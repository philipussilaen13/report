@extends('layout.main')

@section('title')
	Payment
@endsection

@section('css')
	{{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
	Payment List
@endsection

@section('pageDesc')
	List of Customer Transaction
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>Rp {{ number_format($totalPaidAmount) }}</h3>
					<p>Amount Paid Payment</p>
				</div>
				<div class="icon text-white">
					<i class="ion ion-cash"></i>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-xs-6">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>{{ $totalPaidCount }}</h3>
					<p>Total Paid Payment</p>
				</div>
				<div class="icon text-white">
					<i class="ion ion-cash"></i>
				</div>
			</div>
		</div>
	</div>

	<div class="box">
		<div class="box-header">
			<h3 class="box- title">List Payment</h3>
		</div>
		<div class="box-body">
			<form id="form-transaction">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label>Date Range</label>
							<input type="text" name="dateRange" class="form-control" id="dateRange">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Company</label>
							<select class="form-control select2" name="company">
								<option value="">All Company</option>
								@foreach ($companyAccessTokenDb as $item)
									<option value="{{ $item->id }}">{{ $item->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Channel</label>
							<select class="form-control select2" name="channel">
								<option value="">All Channel</option>
								@foreach ($paymentChannelDb as $item)
									<option value="{{ $item->id }}">{{ $item->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Customer Name</label>
							<input type="text" name="customerName" class="form-control">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Status</label>
							<select class="form-control select2" name="status">
								<option value="">All Status</option>
								@foreach ($statusList as $item)
									<option value="{{ $item }}">{{ $item }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<br>
							<button class="btn btn-flat btn-success" id="btn-filter" type="button">Filter</button>
							<button class="btn btn-flat btn-info" id="btn-excel" type="button">Excel</button>
						</div>
					</div>
				</div>
			</form>
			<hr>
			<div class="table-responsive">
				<table class="table table-condensed table-hover no-padding">
				 	<thead>
				 		<tr>
				 			<th>No</th>
				 			<th>Payment ID</th>
				 			<th>Company</th>
				 			<th>VA Number</th>
				 			<th>Channel</th>
				 			<th>Type</th>
				 			<th>Customer Data</th>
				 			<th>Customer Paid Amount</th>
				 			<th>Customer Received</th>
				 			<th>Status</th>
				 			<th>Total Amount</th>
				 			<th>Description</th>
				 			<th>Date</th>
				 		</tr>
				 	</thead>
				 	<tbody>
				 		@foreach ($clientTransactionDetail as $index => $item)
				 			<tr>
				 				<td>{{ $clientTransactionDetail->firstItem() + $index }}</td> 
				 				<td>
				 					<strong>{{ $item->clientTransaction->payment_id }}</strong> <br>
				 					{{ $item->sub_payment_id }}
				 				</td>
				 				<td>{{ $item->clientTransaction->companyAccessToken->name }}</td>
				 				<td>
				 					@if (!empty($item->clientTransaction->bniTransaction))
				 						{{ $item->clientTransaction->bniTransaction->virtual_account_number }}
				 					@elseif (!empty($item->clientTransaction->dokuTransaction))
				 						{{ $item->clientTransaction->dokuTransaction->virtual_number }}
				 					@endif
				 				</td>
				 				<td>{{ $item->clientTransaction->paymentChannel->name }}</td>
				 				<td>
				 					@if ($item->clientTransaction->payment_type == 'open')
				 						<span class="label label-danger">{{ $item->clientTransaction->payment_type }}</span>
				 					@else 
				 						<span class="label label-warning">{{ $item->clientTransaction->payment_type }}</span>
				 					@endif
				 				</td>
				 				<td>
				 					<strong>{{ $item->clientTransaction->customer_name }}</strong>
				 				</td>
				 				<td>
				 					@if (empty($item->paid_amount))
				 						{{ number_format($item->amount) }}
				 					@else
				 						{{ number_format($item->paid_amount) }}
				 					@endif
				 					
				 				</td>
				 				<td>
				 					{{ number_format($item->amount) }}
				 				</td>
				 				<td>
				 					@if ($item->clientTransaction->status == 'CREATED')
				 						<span class="label label-info">CREATED</span>
				 					@elseif ($item->clientTransaction->status == 'PAID')
				 						<span class="label label-success">PAID</span>
				 					@endif
				 				</td>
				 				<td>Rp{{ number_format($item->clientTransaction->total_payment) }}</td>
				 				<td>
				 					{{ $item->clientTransaction->description }}
				 				</td>
				 				<td>{{ date('Y-m-d H:i:s',strtotime($item->updated_at)) }}</td>
				 			</tr>
				 		@endforeach
				 	</tbody>
				</table>
			</div>
		</div>
		{{ $clientTransactionDetail->links() }}
	</div>
@endsection

@section('js')
	{{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

	{{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['status']))
            	$('select[name=status]').val("{{ $parameter['status'] }}").trigger('change');
            @endif
            @if (!empty($parameter['company']))
            	$('select[name="company"]').val({!! json_encode($parameter['company']) !!}).trigger('change');
            @endif
            @if (!empty($parameter['channel']))
            	$('select[name="channel"]').val({!! json_encode($parameter['channel']) !!}).trigger('change');
            @endif
        });
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
            $('#btn-filter').on('click', function(event) {
                $('#form-transaction').attr('method', 'get');
                $('#form-transaction').submit();
            });
            $('#btn-excel').on('click', function(event) {
                $('#form-transaction').attr('method', 'post');
                $('#form-transaction').submit();
            });
        });
    </script>
@endsection