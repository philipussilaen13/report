<h3>Failed Push Transaction</h3>
<p>
    <strong>Reference</strong> : {{ $transaction->reference }}
</p>
<p>
    <strong>Agent</strong> : {{ $locker->locker_name }} {{ $transaction->user->locker_id }}
</p>
<p>
    <strong>Items type</strong> : {{ $item->type }} <br>
    <strong>Item Params</strong> <br>
    @php
        $parameter = json_decode($item->params);
    @endphp
    @foreach($parameter as $key => $param)
    	@if(!is_array($param))
        	{{ $key }} : {{ $param }} <br>
       	@else
            @if($key != 'product_item')
                @foreach ($element as $key_ => $value)
                    {{$key_}} : {{$value}}
                @endforeach
            @endif
        @endif
    @endforeach
</p>
<p>
    <strong>Error Msg</strong> : {{ $error }}
</p>

<p>
    <strong>Action</strong> : {{ $url }} <br>
    <strong>Params Push</strong> : {{ $params }}
</p>