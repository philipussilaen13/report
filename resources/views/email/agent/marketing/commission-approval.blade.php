<!DOCTYPE html>
<html>
<head>
	<title>PopBox Agent Commission Approval </title>
</head>
<style type="text/css">
	.bold {
		font-weight: bold;
	}
</style>
<body>
	<p align="center">
		<h3>PopBox Agent</h3>
		<h3>Commission Schema Approval</h3>
	</p>
	<p>
		Date : {{ date('Y-m-d H:i:s') }}
	</p>
	<table style="border-collapse: collapse;">
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Status</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($schemaDb->status == 1)
					<b style="color: green">Need To Activate</b>
				@elseif ($schemaDb->status == 0)
					<b style="color: red">Need To Disabled</b>
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Amount</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($schemaDb->type == 'fixed')
					Rp {{ number_format($schemaDb->values) }}
				@else
					{{ $schemaDb->values }} {{ $schemaDb->type }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Minimum Amount</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if (empty($schemaDb->min_amount))
					-
				@else 
					Rp {{ number_format($schemaDb->min_amount) }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Maximum Amount</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if (empty($schemaDb->max_amount))
					-
				@else 
					Rp {{ number_format($schemaDb->max_amount) }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Active Date</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{ date('D, j F Y',strtotime($schemaDb->start_date)) }} - {{ date('D, j F Y',strtotime($schemaDb->end_date)) }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Rules</td>
			<td style="border: 1px solid black;padding: 10px;">
				@foreach ($commissionRules as $item)
					<b>Name</b> : {{ $item->parameter_name }} <br>
					<b>Operator</b> : {{ $item->operator }} <br>
					<b>Value</b> : <br>
					@foreach ($item->value_text as $element)
						- {{ $element }} <br>
					@endforeach
					<hr>
				@endforeach
			</td>
		</tr>
	</table>

</body>
</html>