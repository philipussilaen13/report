<!DOCTYPE html>
<html>
<head>
	<title>PopBox Agent Campaign Approval </title>
</head>
<body>
	<p align="center">
		<h3>PopBox Agent</h3>
		<h3>Commission Schema Approval</h3>
	</p>
	<p>
		Date : {{ date('Y-m-d H:i:s') }}
	</p>
	<table style="border-collapse: collapse;">
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Status</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($campaignDb->status == 'enabled')
					<b style="color: green">Need To Activate</b>
				@elseif ($campaignDb->status == 'disabled')
					<b style="color: red">Need To Disabled</b>
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Name</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{ $campaignDb->name }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Description</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{ $campaignDb->description }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Active Date</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{ date('D, j F Y',strtotime($campaignDb->start_time)) }} - {{ date('D, j F Y',strtotime($campaignDb->end_time)) }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Campaign Type</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($campaignDb->promo_type=='topup')
					<span class="label label-success">Top Up</span>
				@else
					<span class="label label-info">{{ strtoupper($campaignDb->promo_type) }}</span>
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Amount</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($campaignDb->type=='fixed')
					<span class="label label-primary">Fixed</span>
					/ Rp {{ number_format($campaignDb->amount) }}
				@elseif($campaignDb->type == 'percent')
					<span class="label label-success">Percentage</span>
					/ {{ $campaignDb->amount }} % <br>
					@if (!empty($campaignDb->max_amount))
						<strong>Max Amnt</strong> : Rp {{ number_format($campaignDb->max_amount) }} <br>
					@endif
					@if (!empty($campaignDb->min_amount))
						<strong>Min Amnt</strong> : Rp {{ number_format($campaignDb->min_amount) }}
					@endif
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Category</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($campaignDb->category=='fixed')
					<span class="label label-primary">Fixed Discount</span>
				@elseif($campaignDb->category == 'cutback')
					<span class="label label-success">Cutback Discount</span>
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Voucher/Limit Type</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($campaignDb->voucher_required=='1')
					<span class="label label-primary">Voucher Required</span> <br>
					<span class="label label-info">{{ count($campaignDb->vouchers) }} Vouchers</span>
				@elseif($campaignDb->voucher_required == '0')
					<span class="label label-success">Voucher Not Required</span>
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Limit Usage</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{-- If Voucher NOT REQUIRED --}}
				@if (empty($campaignDb->limit_usage) || $campaignDb->limit_usage == 0)
					<span class="label label-warning">No Limit Usage</span>
				@else 
					<span class="label label-success">{{ $campaignDb->limit_usage }} Usage Limit</span>
				@endif
				<span class="label label-default">{{ ucfirst($campaignDb->campaign_limit_type) }}</span>
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Rules</td>
			<td style="border: 1px solid black;padding: 10px;">
				@foreach ($campaignRules as $item)
					<b>Name</b> : {{ $item->parameter_name }} <br>
					<b>Operator</b> : {{ $item->operator }} <br>
					<b>Value</b> : <br>
					@foreach ($item->value_text as $element)
						- {{ $element }} <br>
					@endforeach
					<hr>
				@endforeach
			</td>
		</tr>
	</table>
</body>
</html>