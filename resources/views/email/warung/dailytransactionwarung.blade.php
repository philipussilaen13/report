<!DOCTYPE html>
<html>
<head>
	<title>Data transaksi {{ $agent_name }} Tanggal {{ $date }} </title>
</head>
<body>
Hi {{ $agent_name }}, <br><br>

Terlampir rekap seluruh transaksi yang dibuat pada tanggal {{ $date }}. <br><br>

Terima Kasih.
</body>
</html>