<!DOCTYPE html>
<html>
<head>
	<title>Data transaksi {{ $agent_name }} Tanggal {{ $date }} </title>
</head>
<body>
Hi, <br><br>

Berikut report transaksi penjualan {{ $agent_name }} untuk periode {{ $report_date }}.
<br>
Jika ada pertanyaan terkait isi report, silakan hubungi customer service kami di 081297206858 atau email ria@popexpress.id.
<br><br>
Terima Kasih.
</body>
</html>