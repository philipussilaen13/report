@extends('layout.main')

@section('title')
    Detail Transaction
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Detail Transaction
@endsection

@section('pageDesc')
    Detail Transaction
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-3">
                <div class="box box-widget widget-user" style="min-height:900px;">
                    <div class="widget-user-header bg-aqua">
                        <h3 class="widget-user-username">{{ $locker->locker_name }}</h3>
                        <h5 class="widget-user-desc">{{ $locker->locker_id }}</h5>
                    </div>
                    <div class="box-footer">
                        <div class="row" style="margin-left:0px;margin-right:0px;">
                            <strong>Alamat</strong>
                            <p class="text-muted">{{ $locker->address }} {{ $locker->city->city_name }}</p>
                            <strong>Provinsi</strong>
                            <p class="text-muted">{{ $locker->city->province->province_name }}</p>
                            <strong>Alamat Detail</strong>
                            <p class="text-muted">{{ $locker->detail_address }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="box" style="min-height:900px;">
                    <div class="box-header">
                        <h3 class="box-title">Detail Transaction</h3>
                        <table id="gridinfo" class="table table-striped" style="width: 100%">
                        	<tbody></tbody>
                        </table>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive div_content">
                            <table id="gridcontent" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="30">No</th>
                                        <th>Name</th>
                                        <th>SKU</th>
                                        <th>Type</th>
                                        <th>Qty</th>
                                        <th>Price Per Item</th>
                                        <th>Total Price</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="5" style="text-align:right">Grand Total Qty :</th>
                                        <th><span id="id_span_totqty">0</span></th>
                                    </tr>
                                    <tr>
                                        <th colspan="5" style="text-align:right">Grand Total Price :</th>
                                        <th><span id="id_span_totprice">0</span></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function(){
			$('#gridcontent').DataTable({
				'searching' : false,
				'paging'      : false,
				'lengthChange': false,
				'ordering'    : false,
				'info'        : false,
				'autoWidth'   : false,
				"processing": true,
				"serverSide": true,
				
				"ajax": {
					"url": "{{ url('warung/transaction/warung/getdetailtransaction') }}",
					"data": function ( d ) {
						var info = $('#gridcontent').DataTable().page.info();						
						d.transactionid = "{{$transactionid}}";						
					},
					"dataSrc": function(json){
						json.draw = json.payload.draw;

                        $('#id_span_totqty').text(json.payload.totQty);
                        $('#id_span_totprice').text(json.payload.totPrice);

                        var status = json.payload.status === 'VOID' ? 'Void' : 'Success';
						var payment_method = json.payload.paymentMethod;

                        if(json.payload.is_popbox_wallet){
                            $('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Customer Name</td><td>: <span id="user_name"></span></td></tr>');
                            $('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Customer Phone Number</td><td>: <span id="phone"></span></td></tr>');
                            $('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Customer Email</td><td>: <span id="email"></span></td></tr>');
                            
                            $('#user_name').text(json.payload.name);
                            $('#phone').text(json.payload.phone);
                            $('#email').text(json.payload.email);
                        }
                        
                        $('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Payment Method</td><td>: <span id="payment_method"></span></td></tr>');
                        $('#payment_method').text(payment_method);
                        if(payment_method && payment_method.toUpperCase() === 'ottopay'.toUpperCase()){
                    		$('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">PopBox Transaction Reference</td><td>: <span id="transaction_reference_popbox"></span></td></tr>');
                    		$('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Ottopay Transaction Reference</td><td>: <span id="transaction_reference_ottopay"></span></td></tr>');

                            $('#transaction_reference_popbox').text(json.payload.orderId);
                            $('#transaction_reference_ottopay').text(json.payload.transactionId);
                        } else {
                        	$('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Transaction Reference</td><td>: <span id="transaction_reference"></span></td></tr>');
                        	
                            $('#transaction_reference').text(json.payload.transactionReference);
                        }

                        $('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Status Transaction</td><td>: <span id="status_transaction"></span></td></tr>');
                        $('#status_transaction').text(status);
                        
                        if(status == 'Void'){
                            $('#gridinfo > tbody:last-child').append('<tr><td style="width: 25%">Void Reason</td><td>: <span id="description"></span></td></tr>');
                            $('#description').text(json.payload.description);
                        }
                        
						return json.payload.data;
					}
				},
				"columnDefs" : [
					{ "targets": 0, "data": "no" },
					{ "targets": 1, "data": "name" },
					{ "targets": 2, "data": "sku" },
					{ "targets": 3, "data": "type" },
					{ "targets": 4, "data": "qty" },
					{ "targets": 5, "data": "price" },
					{ "targets": 6, "data": null, 
						"render": function ( data, type, row, meta ) {
							var price = isEmpty(data.price) ? 0 : data.price;
							var qty = isEmpty(data.qty) ? 0 : data.qty;
							var totPriceItem = parseFloat(price) * parseFloat(qty);
							return totPriceItem;
						}
					},
				],
			});
        });
    </script>
@endsection