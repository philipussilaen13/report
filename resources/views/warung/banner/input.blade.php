@extends('layout.main') @section('title') Banner @endsection

@section('css') {{-- select 2 --}}
<link rel="stylesheet"
	href="{{ asset('plugins/select2/css/select2.min.css') }}">
{{-- Date Range picker --}}
<link rel="stylesheet"
	href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet"
	href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet"
	href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
<!-- Sweet Alert -->
<link href="{{ asset('plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
@endsection @section('pageTitle') Home Banner Management @endsection

@section('content')
<style>
    .tip{
        font-size: x-small;
        font-style: italic;
        color: red;
        display: none;
        margin-left: 25px;
    }
    table.dataTable tbody td {
        vertical-align: middle;
    }
}
</style>
<section class="content" id="vueApp">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-input-banner">
				<div class="box-body border-radius-none">
					<div class="p-20 m-b-20" id="vueApp" style="padding-left: 0px" v-cloak>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session()->has('success'))
                            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <i class="mdi mdi-check-all"></i>
                                <strong id="success">{{ session()->get('success') }}</strong>
                            </div>
                        @endif
    					 <form role="form" class="form-horizontal" method="POST" action="{{route('store-banner')}}" id="form-message" enctype="multipart/form-data">
                        	{{csrf_field()}}
                            <input type="hidden" name="banner_id" v-model="banner_id">
                        	<h4 class="m-b-20 header-title">Campaign Information</h4>
                        	<table class="table">
        						<tr>
        							<td style="width: 13%;vertical-align: middle;">Input Title<span class="text-danger">*</span></td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
                                			<div class="row col-md-5" style="width: 350px">
                								<input type="text" class="form-control" style="width: 100%;" maxlength="50" id="title" :disabled="is_disabled" value="{{old('title')}}" name="title" v-model="title">
                							</div>
                							<div class="row col-md-4" style="margin-top: 5px">
                								<span id="title_tip" class="tip">This field is required</span>
                							</div>
                							
    								</td>
        						</tr>
        						<tr>
        							<td style="width: 13%;vertical-align: middle;">User Type</td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
        								<input type="radio" id="user_type" name="user_type" value="agent" :disabled="is_disabled" v-model="user_type"> <label for="agent">Agent</label>
        								<input type="radio" id="user_type" name="user_type" value="warung" :disabled="is_disabled" v-model="user_type" style="margin-left: 30px"> <label for="warung">PopWarung</label>
        								<span id="user_type_tip" class="tip">This field is required</span>
        							</td>
        						</tr>
        						<tr>
        							<td style="width: 13%;vertical-align: middle;">Upload Image<span class="text-danger">*</span></td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
        								<label class="btn btn-primary" id="btnImage" :disabled="is_disabled">
                                            Browse Image&hellip; <input id="id_fileupload" type="file" :disabled="is_disabled" name="image" @change="onFileChange" style="display: none;">
                                        </label>
										&nbsp;&nbsp;&nbsp;
                                        <img id="id_img_product" style="max-width:100px;height:auto;" name="img_product">
        								<span id="image_tip" class="tip">This field is required</span>
                                    </td>
        						</tr>
        						<tr>
        							<td style="width: 13%;vertical-align: middle;">Status<span class="text-danger">*</span></td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
        								<input type="radio" id="status_banner" name="status_banner" value="active" v-model="status_banner"> <label for="active">Active</label>
        								<input type="radio" id="status_banner" name="status_banner" value="inactive" v-model="status_banner" style="margin-left: 30px"> <label for="inactive">Inactive</label>
        								<span id="status_tip" class="tip">This field is required</span>
        							</td>
        						</tr>
        					</table>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button id="submitForm" style="width: 20%" type="submit" class="btn btn-primary bg-green pull-right">
                                        Submit
                                    </button>
                                </div>
                            </div>
    					 </form>
					 </div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-detail">
				<div class="box-body border-radius-none">
					<table  id="list-grid-content" class="table table-bordered table-hover">
						<thead>
							<tr class="text-center">
								<th width="30" class="text-center">No</th>
								<th class="text-center">Created By</th>
								<th class="text-center">Last Updated By</th>
								<th class="text-center">Title</th>
								<th class="text-center">Type</th>
								<th class="text-center">Image</th>
								<th class="text-center">Status</th>
								<th class="text-center"></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
        
    
</section>
@endsection 

@section('js') 

@if(App::environment('production'))
	<script src="{{ asset ('assets/js/vue.min.js') }}"></script>
@else
	<script src="{{ asset ('assets/js/vue.js') }}"></script>
@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
{{-- DateRange Picker --}}
<script
	src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/helper.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>

<!-- Sweet-Alert  -->
<script src="{{ asset ('/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset ('assets/js/underscore-min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var app = new Vue({
            el: '#vueApp',
        data: {
            title			: '',
            user_type		: '',
            image_file		: '',
        	status_banner	: 'inactive',
        	banners			: [],
        	is_disabled		: false,
        	banner_id		: '',
        	limit			: 15,
        	start 			: 0,
        },
        computed: {
            
        },
        watch: {
            
        },
        created : function(){
        },
        mounted : function(){
            this.$nextTick(function(){
            	
            	$("#title_tip").hide();
            	$("#user_type_tip").hide();
            	$("#image_tip").hide();
            	$("#status").hide();
                hideLoading('.box-body', 'box-body');
            	
                $('.select2').select2();
                
                var $vm = this;

                $('#title').keypress(function (e) {
                	$vm.alphanumeric(e);
                	$("#title").attr('style', 'width: 100%');
                	$("#title_tip").hide();
                });

                $('input:radio[name="user_type"]').change( function() {
                	if (this.checked) {
                		$("#user_type_tip").hide();
                	}
                });

                $('input:radio[name="status_banner"]').change( function() {
                	if (this.checked) {
                		$("#status_tip").hide();
                	}
                });

                $('#id_fileupload').on('change', function(e) {
                	$("#image_tip").hide();
                    var file = $(this)[0].files[0];

                    if(file != null && file != '' && file != 'undefined'){
    					if(file.type != 'image/png'){ 
    						alert('Please upload using PNG file type only');
    					} else if (file.size > 100000) { 
    						alert('Please upload image not more than 100Kb');
    					} else {
                            var img = new Image();
                            var imgwidth = 0;
                            var imgheight = 0;
                            img.src = URL.createObjectURL(file);
                            img.onload = function() {
                                imgwidth = this.width;
                                imgheight = this.height;
                                var imageType = file.type;
        
                                $('#id_img_product').attr('src', URL.createObjectURL(file));
                            };
    					}
                    }
                });
                
                $.fn.openImage = function(image) {        		
            		window.open(
                        (image), '_blank' 
                  	);
                };
    		
        		$('#list-grid-content').DataTable({
        			'paging'      	: true,
        			'lengthChange'	: false,
        			'ordering'    	: false,
        			'info'        	: true,
        			'autoWidth'   	: false,
        			"processing"	: true,
        			"serverSide"	: true,
        			'searching' 	: false,
        			"pageLength"	: $vm.limit,
        			"ajax": {
        				"url": "{{ route('list-banner') }}",
        				"data": function ( d ) {
        					var info 				= $('#list-grid-content').DataTable().page.info();
        					d.start 				= info.start;
        					d.limit 				= $vm.limit;
        				},
        				"dataSrc": function(json){
        					json.draw = json.draw;
        					json.recordsTotal = json.recordsTotal;
        					json.recordsFiltered = json.recordsTotal;
        					baseUrl = json.base_url;
        	                hideLoading('.box-body', 'box-body');
        	                $vm.banners = json.result;
        					return json.result;
        				},
        			},
        			"columnDefs" : [
        				{ "targets": 0, "className": "text-center", "width": "7%", "data": "no" },
        				{ "targets": 1, "className": "text-center", "width": "10%", "data": "created_by" },
        				{ "targets": 2, "className": "text-center", "width": "13%", "data": function ( data, type, row, meta ) {
        						return data.updated_by ? data.updated_by : '-';
                        	}  
                    	},
        				{ "targets": 3, "className": "text-center", "width": "20%", "data": "title"},
        				{ "targets": 4, "className": "text-center", "width": "10%", "data": function ( data, type, row, meta ) {
                				var label = '';
            					if (data.user_type === 'agent') {
            						label =  '<span class="label bg-blue">Agent</span>'
                                } else {
                                	label =  '<span class="label bg-green">Warung</span>'
                                }
        						return label;
                        	}  
                    	},
        				{ "targets": 5, "className": "text-center", "width": "15%", "data": function ( data, type, row, meta ) {
            					var image = '';
            					image = image.concat(baseUrl, 'banner/', data.image_name);
        						return '<a onclick=$(this).openImage("'+image+'") style="cursor: pointer;"> <img alt="'+image+'" src="'+image+'" style="max-width:50px;height:auto;" ></a>';
                        	}  
                        },
        				{ "targets": 6, "className": "text-center", "width": "10%", "data": function ( data, type, row, meta ) {
                				var label = '';
            					if (data.status === 'active') {
            						label =  '<label style="color: green">Active</label>';
                                } else {
                                	label =  '<label style="color: red">Inactive</label>';
                                }
        						return label;
                        	}  
                    	},
        				{ "targets": 7, "className": "text-center", "width": "7%", "data": function ( data, type, row, meta ) {
        						return '<button class="btn btn-primary" onclick="$(this).edit('+data.id+')">Edit</button>';
                        	}  
                        }
        			]
        		});

        		$.fn.edit = function(id) { 
                  	for(i = 0; i < $vm.banners.length; i++){
                      	if($vm.banners[i].id == id ){
                      		$vm.title = $vm.banners[i].title;
                      		$vm.banner_id = $vm.banners[i].id;
                      		$vm.user_type = $vm.banners[i].user_type;
                      		$vm.image_file = $vm.banners[i].image_name;
                      		$vm.status_banner = $vm.banners[i].status;
                      		$vm.is_disabled = $vm;
                      	}
                  	}
                };

                var form = $("#form-message");
                var isConfirmed = false;
                form.submit(function (event) {
                    
                    // If answered "ok" previously continue submitting the form.
                    if (isConfirmed) {
                        return true;
                    }
                	
                	$("#title_tip").hide();
                	$("#user_type_tip").hide();
                	$("#image_tip").hide();
                	$("#status").hide();

                    if(!$vm.title 
                            || !$vm.user_type 
                            || !$vm.status_banner
                            || !$vm.image_file){
                        
                        if(!$vm.title) {
                            $("#title_tip").show();
                        }

                        if(!$vm.user_type){
							$("#user_type_tip").show();
                        }

                        if(!$vm.image_file){
                            $("#image_tip").show();
                        }

                        if(!$vm.status_banner){ 
                            $("#status_tip").show();
                        }
                        return false;
                    }
                    
                    for(i = 0; i < $vm.banners.length; i++){
                      	if(($vm.banners[i].title.toLowerCase() === $vm.title.toLowerCase()) && !$vm.is_disabled){
                          	alert('Submitted title already exist');
                            return false;
                      	}
                  	}
                    
                    // Confirm is not ok yet so prevent the form from submitting and show the message.
                    event.preventDefault();
                    var popupText = '<strong>Are you sure want to upload this banner?</strong>';

                    swal({
                        text				: popupText,
                        type				: 'warning',
                        showCancelButton	: true,
                        confirmButtonColor	: 'green',
                        cancelButtonColor	: '#d33',
                        confirmButtonText	: 'Proceed',
                        cancelButtonText	: 'Cancel'
                    }).then(function (confirmed) {

                        isConfirmed = confirmed;
                        showLoading('.box-body', 'box-body');
                        $('#submitForm').attr('disabled', 'disabled');
                       	form.submit();
                    }).catch(swal.noop);
                });

            })
        },
        methods : {
            alphanumeric: function(e){
            	var regex = new RegExp("^[a-zA-Z0-9,.!?% ]*$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            },
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                  return;
                this.createImage(files[0]);
          	},
          	createImage(file) {
                var image = new Image();
                var reader = new FileReader();
                var vm = this;

                reader.onload = (e) => {
                  vm.image_file = e.target.result;
                };
                reader.readAsDataURL(file);
          	},
          	removeImage: function (e) {
                this.image_file = '';
          	},
        },


    });
</script>
@endsection
