@extends('layout.main') @section('css') {{-- select 2 --}}
<link rel="stylesheet"
	href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
{{-- Date Range picker --}}
<link rel="stylesheet"
	href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet"
	href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet"
	href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">

<meta name="viewport" content="width=device-width, initial-scale=1">
@stop 

@section('title') 
	Messaging Management
@endsection 
@section('pageTitle') 
	Messaging Management
@endsection 
@section('pageDesc') 

@endsection 

@section('content')
<style>
        
.header-filter-custom {
    border: 1px solid;
}
    
table.dataTable tbody td {
    vertical-align: middle;
}

#maps {
	border: 1px solid red;
	width: 100%;
	height: 700px;
}
</style>
<section class="content">
	{{-- Table --}}
	<div class="row">
		<div class="box box-solid box-filter">
			<div class="box-body border-radius-none">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-3" style="margin-left: 0px; padding-left: 10px;margin-right: 0px; width: 20%">
							<div class="form-group" style="margin-top: 15px;">
    							<label>Select Type &nbsp;</label>
    							<select class="select2 select2-message-type"
    								name="messageType" id="messageType" style="width: 60%;">
    							</select>
							</div>
						</div>
						<div class="col-md-4" style="margin-left: 0px; padding-left: 3px;margin-right: 0px;">
							<div class="form-group" style="margin-right: 0px; margin-top: 15px;">
    							<label>Search Message &nbsp;</label>
    							<input type="text" class="form-control"
											id="keyword-message" name="keyword-message"
											placeholder="Title / Message"
											style="width: 65%; display: inline-block;">
							</div>
						</div>
						<div class="col-md-4" style="margin-left: 0px; padding-left: 3px;margin-right: 0px;">
							<div class="form-group form-inline" style="margin-top: 15px;">
								<div class="row"
									style="margin-right: 0px;padding-left: 10px">
									<label>Created Date &nbsp;</label>
									<div class="input-group" style="width: 60%">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control"
											id="date_range_picked" name="date_range_picked"
											placeholder="Select Date" readonly="readonly"
											style="background-color: white;">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-1" style="width: 10%">
							<div class="form-group">
								<div style="margin-right: 0px; margin-top: 15px;">
									<a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
            <div class="box box-solid box-body">
                <div class="box-header">
                    <div class="form-group">
                        <div class="col-md-4">
                            
                        </div>

                        <div class="col-md-8 no-padding">
                            <button id="btn-export" type="button" class="btn btn-success btn-primary btn-sm pull-right bg-green" onclick="ajaxCreateNewMessage()" style="margin-right: 10px; width: 17%">
                                Create New Message
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
						<table id="list-grid-content" class="table table-bordered table-hover">
							<thead>
								<tr class="text-center">
                                    <th class="text-center" style="vertical-align: middle;">No</th>
                                    <th class="text-center" style="vertical-align: middle;">Create Date</th>
                                    <th class="text-center" style="vertical-align: middle;">Create By</th>
                                    <th class="text-center" style="vertical-align: middle;">Type</th>
                                    <th class="text-center" style="vertical-align: middle;">Region</th>
                                    <th class="text-center" style="vertical-align: middle;">Recipient</th>
                                    <th class="text-center" style="vertical-align: middle;">Title</th>
                                    <th class="text-center" style="vertical-align: middle;">Total Open</th>
                                    <th class="text-center" style="vertical-align: middle;">Conv. Open</th>
                                    <th class="text-center" style="vertical-align: middle;">Total Click</th>
                                    <th class="text-center" style="vertical-align: middle;">Image</th>
								</tr>
							</thead>
						</table>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection @section('js')
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.freezeheader.js') }}"></script>
{{-- DateRange Picker --}}
<script
	src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/helper.js') }}"></script>

{{-- NUMERATOR --}}
<script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>
<script
	src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
<script
	src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">

    	var start = 0;
        var limit = 15;
        var messageTypes = {!! json_encode($message_types) !!};
        var baseUrl = '';
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
        
        $(function(){

            $('.select2').select2();

            $(".select2-message-type").select2({
                  data: messageTypes
            });
            
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                showLoading('.box-filter', 'box-filter');
                showLoading('.box-body', 'box-body');
                $('#list-grid-content').DataTable().ajax.reload();
            });
            
            $.fn.openImage = function(image) {        		
        		window.open(
                    (image), '_blank' 
              	);
            };
		
    		$('#list-grid-content').DataTable({
    			'paging'      	: true,
    			'lengthChange'	: false,
    			'ordering'    	: false,
    			'info'        	: true,
    			'autoWidth'   	: false,
    			"processing"	: true,
    			"serverSide"	: true,
    			'searching' 	: false,
    			
    			"pageLength": limit,
    			"ajax": {
    				"url": "{{ route('list-message') }}",
    				"data": function ( d ) {
    					var info 				= $('#list-grid-content').DataTable().page.info();						
    					d.message_type   		= $('#messageType').val() ? $('#messageType').val() : 'all';
                        d.message_created_at	= date_range_picked;
                        d.keyword        		= $("#keyword-message").val() ? $("#keyword-message").val() : '';
    					d.start 				= info.start;
    					d.limit 				= limit;
    				},
    				"dataSrc": function(json){
    					json.draw = json.draw;
    					json.recordsTotal = json.recordsTotal;
    					json.recordsFiltered = json.recordsTotal;
    					baseUrl = json.base_url;
    	                hideLoading('.box-filter', 'box-filter');
    	                hideLoading('.box-body', 'box-body');
    					return json.result;
    				},
    			},
    			"columnDefs" : [
    				{ "targets": 0, "className": "text-center", "width": "50px", "data": "no" },
    				{ "targets": 1, "className": "text-center", "width": "8%", "data": "created_at" },
    				{ "targets": 2, "className": "text-center", "width": "8%", "data": "created_by" },
    				{ "targets": 3, "className": "text-center", "width": "5%", "data": function ( data, type, row, meta ) {
        				var label = '';
        					if (data.message_type === 'DM') {
        						label =  '<span class="label bg-blue">'+data.message_type+'</span>'
                            } else if (data.message_type === 'Broadcast') {
                            	label =  '<span class="label bg-green">'+data.message_type+'</span>'
                            }
    						return label;
                    	}  
                    },
    				{ "targets": 4, "className": "text-center", "width": "100px", "data": "region" },
    				{ "targets": 5, "className": "text-center", "width": "100px", "data": "recipient" },
    				{ "targets": 6, "className": "text-left", "width": "17%", "data": "title" },
    				{ "targets": 7, "className": "text-center", "width": "50px", "data": "message_opened" },
    				{ "targets": 8, "className": "text-center", "width": "50px", "data": "conv_open" },
    				{ "targets": 9, "className": "text-center", "width": "50px", "data": "url_clicked" },
    				{ "targets": 10, "className": "text-center", "width": "100px", "data": function ( data, type, row, meta ) {
        					var image = '';
        					image = image.concat(baseUrl, 'upload/', data.image_name);
    						return '<a onclick=$(this).openImage("'+image+'") style="cursor: pointer;"> <img alt="'+image+'" src="'+image+'" style="max-width:50px;height:auto;" ></a>';
                    	}  
                    }
    			]
    		});
        });

        function ajaxCreateNewMessage(){
        	window.location = "{{ route('new-message') }}";
        }
    </script>

	{{-- Date Range Picker --}}
    <script type="text/javascript">
        var date_range_picked = '';
        var startDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
        var endDate = moment().format('YYYY-MM-DD');

        var startDateToShow = moment().subtract(29, 'days').format('DD/MM/YYYY');
        var endDateToShow = moment().format('DD/MM/YYYY');
        date_range_picked = startDate + ',' + endDate;

        jQuery(document).ready(function ($) {
            dateRangeTransaction();

            $('#date_range_picked').val(startDateToShow + ' - ' + endDateToShow);

        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange-transaction');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        }

    </script>
@stop
