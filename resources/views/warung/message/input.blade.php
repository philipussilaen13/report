@extends('layout.main') @section('title') New Message @endsection

@section('css') {{-- select 2 --}}
<link rel="stylesheet"
	href="{{ asset('plugins/select2/css/select2.min.css') }}">
{{-- Date Range picker --}}
<link rel="stylesheet"
	href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet"
	href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet"
	href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
<!-- Sweet Alert -->
<link href="{{ asset('plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
@endsection @section('pageTitle') Create New Message @endsection

@section('content')
<style>
    .table thead tr th, .table tbody tr td {
        border: 0;
    }

    .tip{
        font-size: x-small;
        font-style: italic;
        color: red;
        display: none;
        margin-left: 25px;
    }

    .counter-tip{
        font-size: small;
        font-style: italic;
    }
}
</style>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-input-message">
				<div class="box-body border-radius-none">
					<div class="p-20 m-b-20" id="vueApp" style="padding-left: 0px" v-cloak>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session()->has('success'))
                            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <i class="mdi mdi-check-all"></i>
                                <strong id="success">{{ session()->get('success') }}</strong>
                            </div>
                        @endif
    					 <form role="form" class="form-horizontal" method="POST" action="{{route('create-message')}}" id="form-message" enctype="multipart/form-data">
                        	{{csrf_field()}}
                        	
                        	<h4 class="m-b-20 header-title">Personal Data</h4>
                        	<table class="table">
        						<tr style="height: 50px">
        							<td style="width: 13%;vertical-align: top;padding-top: 15px">Select Message Type</td>
        							<td style="width: 1px;vertical-align: top;padding-top: 15px">: </td>
        							<td style="vertical-align: top;">
    									<select id="message_type" class="form-control select2 select2-message-type" name="message_type" v-model="messageType" style="width: 130px;border: 1px solid #a94442;">
        									<option value="" disabled="disabled">-- Pilih --</option>
        									<option v-for="message_type in message_types" :value="message_type.id">@{{ message_type.name }}</option>
        								</select><span id="message_type_tip" class="tip">This field is required</span>
    								</td>
        						</tr>
        						<tr v-show="messageType == 'broadcast'">
        							<td style="width: 13%;vertical-align: middle;">Select Recipient Type</td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
        								<input type="radio" id="recipient_type" name="recipient_type" value="all" v-model="recipient_type"> <label for="all">All User ( Agent + PopWarung )</label>
        								<input type="radio" id="recipient_type" name="recipient_type" value="agent" v-model="recipient_type" style="margin-left: 30px"> <label for="agent">Agent</label>
        								<input type="radio" id="recipient_type" name="recipient_type" value="warung" v-model="recipient_type" style="margin-left: 30px"> <label for="warung">PopWarung</label>
        								<span id="recipient_type_tip" class="tip">This field is required</span>
        							</td>
        						</tr>
        						<tr v-show="messageType == 'broadcast'">
        							<td style="width: 13%;vertical-align: middle;">Select Region<span class="text-danger">*</span></td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
    									<select id="region" class="form-control select2" name="region" v-model="region" style="width: 30%">
        									<option value="" disabled="disabled">-- Pilih --</option>
        									<option v-for="region in regions" :value="region.id">@{{ region.name }}</option>
        								</select><span id="region_tip" class="tip">This field is required</span>
    								</td>
        						</tr>
        						<tr v-show="messageType == 'directmessage'">
        							<td style="width: 13%;vertical-align: middle;">Select Users<span class="text-danger">*</span></td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
                                        <select class="form-control select2 form-control required" id="user_list" name="recipient[]" multiple="multiple" class="lstSelected" style="width: 100%;"></select>
                                        <span id="user_tip" class="tip">This field is required</span>
									</td>
        						</tr>
        						<tr>
        							<td style="width: 13%;padding-top: 15px;">Input Title<span class="text-danger">*</span></td>
        							<td style="width: 1px;padding-top: 15px;">: </td>
        							<td style="vertical-align: middle;">
                            			<div class="row col-md-5" style="width: 403px">
            								<input type="text" class="form-control" style="width: 100%;" maxlength="50" id="message_title" value="{{old('message_title')}}" name="message_title" v-model="message_title">
            								
            								<span id="counter_title_tip" class="counter-tip" style="padding-top: 0px">@{{counter_title}} / 50</span>
            							</div>
            							<div class="row col-md-4" style="margin-top: 5px">
            								<span id="title_tip" class="tip">This field is required</span>
            							</div>
    								</td>
        						</tr>
        						<tr>
        							<td style="width: 13%;vertical-align: middle;">Upload Image<span class="text-danger">*</span></td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
        								<label class="btn btn-primary" id="btnImage">
                                            Browse Image&hellip; <input id="id_fileupload" type="file" name="image" @change="onFileChange" style="display: none;">
                                        </label>
										&nbsp;&nbsp;&nbsp;
                                        <img id="id_img_product" style="max-width:100px;height:auto;" name="img_product">
        								<span id="image_tip" class="tip">This field is required</span>
                                    </td>
        						</tr>
        						<tr>
        							<td style="width: 13%;vertical-align: top;">Input Detail Message<span class="text-danger">*</span></td>
        							<td style="width: 1px;vertical-align: top;">: </td>
        							<td style="vertical-align: middle;">
        								<textarea rows="3" maxlength="1000" style="width: 100%;" id="message" name="message" v-model="message"></textarea>
        								<div class="row col-md-3">
        									<span id="counter_tip" class="counter-tip" style="padding-top: 0px">@{{counter}} / 1000</span>
        									<span id="body_message_tip" class="tip">This field is required</span>
        								</div>
        							</td>
        						</tr>
        						<tr>
        							<td style="width: 13%;vertical-align: middle;">URL Button</td>
                        			<td style="vertical-align: middle;">: </td>
                        			<td style="vertical-align: middle;">
        								<input type="radio" id="link_loc" name="link_loc" value="nodirect" v-model="link_loc"> <label for="all">No redirect</label>
        								<input type="radio" id="link_loc" name="link_loc" value="menuApp" v-model="link_loc" style="margin-left: 30px"> <label for="all">Menu on App</label>
        								<input type="radio" id="link_loc" name="link_loc" value="browser" v-model="link_loc" style="margin-left: 30px"> <label for="agent">Browser</label>
    								</td>
        						</tr>
        						<tr v-show="link_loc == 'menuApp'">
        							<td style="width: 13%;vertical-align: middle;">Select Menu</td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
        							<td style="vertical-align: middle;">
        								<div class="row col-md-3">
        									<select id="link_menu_loc" class="form-control select2 select2-link-menu-loc" name="link_menu_loc" v-model="linkMenuLoc" style="width: 200px">
            									<option value="" disabled="disabled">-- Pilih --</option>
            									<option v-for="item in menuApps" :value="item.id">@{{ item.name }}</option>
            								</select>
        								</div>
        								<div class="row col-md-3">
        									<input type="text" class="form-control" id="btn_name" name="btn_name" v-model="btn_name" style="width: 200px;" maxlength="30" placeholder="Input button name">
        								</div>
        								<span id="menuApp_tip" class="tip" style="margin-top: 50px">This field is required</span>
    								</td>
        						</tr>
        						<tr v-show="link_loc == 'browser'">
        							<td style="width: 13%;vertical-align: middle;">Input URL</td>
        							<td style="width: 1px;vertical-align: middle;">: </td>
                        			<td style="vertical-align: middle;">
                            			<div class="row col-md-5" style="width: 700px">
                    						<input type="text" id="url_direct" name="url_direct" v-model="url_direct" style="width: 100%">
                            			</div>
            							<div class="row col-md-2" style="margin-top: 1px">
            								<span id="url_direct_tip" class="tip">This field is required</span>
            							</div>
                        			</td>
        						</tr>
        					</table>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button id="submitForm" style="width: 20%" type="submit" class="btn btn-primary bg-green pull-right">
                                        Submit
                                    </button>
                                    <input type="button" @click="messageIndex()" class="btn btn-primary bg-red pull-right" style="width: 20%; margin-right: 10px" value="Back">
                                </div>
                            </div>
    					 </form>
					 </div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection 

@section('js') 

@if(App::environment('production'))
	<script src="{{ asset ('assets/js/vue.min.js') }}"></script>
@else
	<script src="{{ asset ('assets/js/vue.js') }}"></script>
@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
{{-- DateRange Picker --}}
<script
	src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/helper.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>

<!-- Sweet-Alert  -->
<script src="{{ asset ('/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset ('assets/js/underscore-min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var app = new Vue({
            el: '#vueApp',
        data: {
            messageType			: '',
            message_types		: {!! json_encode($message_types) !!},
            region				: '',
            regions				: {!! json_encode($regions) !!},
            linkMenuLoc			: '',
            menuApps			: {!! json_encode($menuApps) !!},
            recipient_type 		: '',
            user_list			: [],
            message_title		: '',
            message				: '',
            link_loc			: 'nodirect',
            url_direct			: '',
            image_file			: '',
            total_user			: 0,
            counter				: 0,
            counter_title		: 0,
            btn_name			: '',
        },
        computed: {
            
        },
        watch: {
        	region : function(newVal, oldVal){
                if (!newVal) return;

                if(this.recipient_type){
                	this.countBroadcastUser(this.recipient_type, newVal);
                }
            },
            recipient_type : function(newVal, oldVal){
                if (!newVal) return;

                if(this.region){
                	this.countBroadcastUser(newVal, this.region);
                }
            },
        },
        created : function(){

        },
        mounted : function(){
            this.$nextTick(function(){
            	
            	$("#message_type_tip").hide();
            	$("#recipient_type_tip").hide();
            	$("#body_message_tip").hide();
            	$("#region_tip").hide();
            	$("#title_tip").hide();
            	$("#user_tip").hide();
            	$("#image_tip").hide();
            	$("#menuApp_tip").hide();
            	$("#url_direct_tip").hide();
            	 
                hideLoading('.box-body', 'box-body');
            	
                $('.select2').select2();
                var $vm = this;

                $("#message_type").select2().on('change', function(){
                    $vm.messageType = this.value;
                    if(this.value == 'broadcast'){
						$vm.user_list = [];
                    } else {
                        $vm.region = '';
                        $vm.recipient_type = '';
                    }
                    $("#message_type_tip").hide();
                });

                $("#region").select2().on('change', function(){
                    $vm.region = this.value;
                    $("#select2-region-container").attr('style', '');
                    $("#region_tip").hide();
                });

                $('input:radio[name="recipient_type"]').change( function() {
                	if (this.checked) {
                		$("#recipient_type_tip").hide();
                	}
                });

                $("#link_menu_loc").select2().on('change', function(){
                    $vm.linkMenuLoc = this.value;
                	$("#menuApp_tip").hide();
                });

                $('#message_title').keypress(function (e) {
                	$vm.alphanumeric(e);
                	$("#message_title").attr('style', 'width: 100%');
                	$("#title_tip").hide();
                }).keyup(function(){
                    $vm.counter_title = $vm.message_title.length;
            	}).keydown(function(){
                    $vm.counter_title = $vm.message_title.length;
            	});

                $('#btn_name').keypress(function (e) {
                	$vm.alphanumeric(e);
                	$("#btn_name").attr('style', 'width: 200px');
                	$("#menuApp_tip").hide();
                });

                $('#message').keypress(function (e) {
                	$vm.alphanumeric(e);
                	$("#message").attr('style', 'width: 100%');
                	$("#body_message_tip").hide();
                }).keyup(function(){
                    $vm.counter = $vm.message.length;
            	}).keydown(function(){
                    $vm.counter = $vm.message.length;
            	});

                $('#url_direct').keypress(function (e) {
                	$("#url_direct_tip").hide();
                });

                $('#id_fileupload').on('change', function(e) {
                	$("#image_tip").hide();
                    var file = $(this)[0].files[0];

                    if(file != null && file != '' && file != 'undefined'){
    					if(file.size > 100000){ 
    						alert('Maksimal ukuran gambar 100Kb');
    					} else if (file.type != 'image/png') { 
    						alert('Format gambar harus dalam bentuk .png');
    					} else {
                            var img = new Image();
                            var imgwidth = 0;
                            var imgheight = 0;
                            img.src = URL.createObjectURL(file);
                            img.onload = function() {
                                imgwidth = this.width;
                                imgheight = this.height;
                                var imageType = file.type;
        
                                $('#id_img_product').attr('src', URL.createObjectURL(file));
                            };
    					}
                    }
                });

                $('#user_list').select2({
    				width: '100%',
                    minimumInputLength: 3,
                    ajax: {
                        url: "{{ route('populate-user') }}",
                        method: "GET",
                        dataType: 'JSON',
                        data: function (params) {
                            var query = {
                                search: params.term || '*',
                                type: 'public'
                            }
                            return query;
                        },
                        processResults: function (data, params) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.locker_name,
                                        id: item.locker_id
                                    }
                                })
                            };
                        }
                    }
                }).on("select2:select", function (e) {
					if(e.params.data.selected){
						$vm.user_list.push(e.params.data.id);
                    	$("#user_tip").hide();
					}
                }).on("select2:unselect", function (e) { console.log($vm.user_list)
					if(!e.params.data.selected){
						$vm.user_list.splice($vm.user_list.indexOf(e.params.data.id), 1);
					}
                });;

                var form = $("#form-message");
                var isConfirmed = false;
                form.submit(function (event) {
                    
                    // If answered "ok" previously continue submitting the form.
                    if (isConfirmed) {
                        return true;
                    }
                	
                	$("#message_type_tip").hide();
                	$("#recipient_type_tip").hide();
                	$("#body_message_tip").hide();
                	$("#region_tip").hide();
                	$("#title_tip").hide();
                	$("#user_tip").hide();
                	$("#image_tip").hide();
                	$("#menuApp_tip").hide();
                	$("#url_direct_tip").hide();

                    if(!$vm.messageType 
                            || !$vm.message_title 
                            || ($vm.messageType == 'broadcast' && (!$vm.recipient_type || !$vm.region)) 
                            || !$vm.image_file
                            || !$vm.message
                            || ($vm.link_loc == 'menuApp' && (!$vm.linkMenuLoc || !$vm.btn_name))
                            || ($vm.link_loc == 'browser' && !$vm.url_direct)){
                        if(!$vm.messageType) {
                            $("#message_type_tip").show();
                        }
                    	
                        if($vm.messageType == 'broadcast' && (!$vm.recipient_type || !$vm.region)){
                        	if(!$vm.recipient_type) $("#recipient_type_tip").show();
                            if(!$vm.region) $("#region_tip").show();
                        }

                        if($vm.messageType == 'directmessage' && (user_list.length == 0)){
                        	$("#user_tip").show();
                        }

                        if(!$vm.image_file){
                            $("#image_tip").show();
                        }

                        if(!$vm.message){ 
                            $("#body_message_tip").show();
                        }
                        
                        if(!$vm.message_title){ 
                            $("#title_tip").show();
                        }

                        if(
                            $vm.link_loc == 'menuApp' 
                            && (!$vm.linkMenuLoc || !$vm.btn_name) ){
                        	$("#menuApp_tip").show();
                        }

                        if(($vm.link_loc == 'browser' && !$vm.url_direct)){
                        	$("#url_direct_tip").show();
                        }
                        
                        return false;
                    }
                    
                    if($vm.message_title.substring($vm.message_title.length - 1, $vm.message_title.length) === ' '){
                        alert('Please remove whitespace at the end of title');
                        return false;
                    }

                    if($vm.messageType == 'directmessage'){
                    	$vm.total_user = $vm.user_list.length;
                    }
//                     if($vm.image_file == null || $vm.image_file == '') {
//                         alert('Silakan pilih gambar terlebih dahulu');
//                     	return false;
//                     }
                    
                    // Confirm is not ok yet so prevent the form from submitting and show the message.
                    event.preventDefault();
                    var popupText = '<strong>Total recipient for this message: '+$vm.total_user+' users\nContinue to send message</strong>';

                    swal({
                        text				: popupText,
                        type				: 'warning',
                        showCancelButton	: true,
                        confirmButtonColor	: 'green',
                        cancelButtonColor	: '#d33',
                        confirmButtonText	: 'Proceed',
                        cancelButtonText	: 'Cancel'
                    }).then(function (confirmed) {

                        isConfirmed = confirmed;
                        showLoading('.box-body', 'box-body');
                        $('#submitForm').attr('disabled', 'disabled');
                       	form.submit();
                    }).catch(swal.noop);
                });

            })
        },
        methods : {
            alphanumeric: function(e){
            	var regex = new RegExp("^[a-zA-Z0-9,.!?% ]*$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            },
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                  return;
                this.createImage(files[0]);
          	},
          	createImage(file) {
                var image = new Image();
                var reader = new FileReader();
                var vm = this;

                reader.onload = (e) => {
                  vm.image_file = e.target.result;
                };
                reader.readAsDataURL(file);
          	},
          	removeImage: function (e) {
                this.image_file = '';
          	},
          	disabledForm: function (e){
          		$("#target :input").prop("disabled", true);
          	},
          	countBroadcastUser: function(userType, region){
          		$.get('{{ route('get-total-user') }}', {user_type:userType, region_code:region}).done(function(result){
                    this.total_user = result;
                }.bind(this))
          	},
          	messageIndex: function(){
          		window.location.href = '{{ route('message') }}';
          	}
        },


    });
</script>
@endsection
