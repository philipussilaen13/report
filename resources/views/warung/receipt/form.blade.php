@extends('layout.main')

@section('title')
    Form Delivery Order
@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Form Delivery Order
@endsection

@section('pageDesc')
    Delivery Order
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-left">
                <a href="{{ url('warung/receipt/detail') }}/{{ $receiptid }}" class="btn btn-flat btn-default">Back</a>
            </div>
            <div class="pull-right">
                <a id="id_save" class="btn btn-flat bg-olive">Save</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="font-weight: bold;">Form Delivery Order</h3>
                </div>
                <div class="box-body form_content" style="min-height:600px;">
                    <table id="id_gridcontent" class="table table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Product Name</th>
                                <th style="width: 100px">SKU</th>
                                <th style="width: 100px">Product Code</th>
                                <th style="width: 120px">Qty Order Not Completed</th>
                                <th style="width: 120px">Qty Delivery</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $no=1; @endphp
                        @foreach($items as $r)                            
                            @php $param = json_decode($r->params); @endphp
                            <tr data-transactionitem="{{ $r->transaction_items_id }}" data-gapqty="{{ $r->gap_qty }}">
                                <td>@php echo $no; @endphp</td>
                                <td>{{ $r->name }}</td>
                                <td>@php echo $param->sku @endphp</td>
                                <td>@php echo isset($param->barcode) ? $param->barcode : ''; @endphp</td>
                                <td>{{ $r->gap_qty }}</td>
                                <td><input type="number" name="qty_delivery" value="0" class="input_qty" onkeyup="$(this).onChangeQty(this, '{{ $r->gap_qty }}');"></td>
                            </tr>
                            @php $no++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                    <div id="id_message_qty" class="row pull-right" style="margin:10px 0px 10px 0px;"></div>                    
                </div>
            </div>
        </div>
    </section>   
    <div id="winNotify" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">
                    <p id="id_message"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/fileinput/bootstrap-filestyle.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $("input[type=number]").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            var inputValue = $(this).val().replace(/[^0-9\.]/g,'');
            if(((event.which != 8 && event.which != 13 && event.which != 116) || inputValue.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var receiptid = '{{ $receiptid }}';

            $.fn.onChangeQty = function(e, qty_order) {
                var qty_delivery = $(e).val();
                if(qty_delivery.length > 0) {
                    if( parseInt(qty_delivery) > parseInt(qty_order) ) {
                        $(e).removeClass('input_qty').addClass('input_failure');
                        $('#id_message_qty').html('<span style="color:red">Jumlah pengiriman melebihi qty order</span>');
                    }
                    else{
                        $(e).removeClass('input_failure').addClass('input_qty');
                        $('#id_message_qty').html('');
                    }
                }
                else {
                    $(e).removeClass('input_qty').addClass('input_failure');
                    $('#id_message_qty').html('<span style="color:red">qty delivery wajib diisi</span>');
                }
            };

            $('#id_save').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var grid = $('#id_gridcontent > tbody');
                var dataItems = [];
                var countGAP = 0, countQtyNotCompleted = 0; countQty=0;
                grid.find('tr').each(function (i, rec) {
                    var row = $(this);
                    var qtygap = parseInt($(rec).attr('data-gapqty'));
                    var qtydelivery = parseInt($(this).find('input').val());
                    var temp = {};
                    temp.transactionitem = $(rec).attr('data-transactionitem');
                    temp.qtydelivery = qtydelivery;
                    
                    if(qtydelivery == 0 ) countQty++;
                    if(qtydelivery > qtygap || isNaN(qtydelivery)) countGAP++;
                    if(qtygap == 0) countQtyNotCompleted++;
                    
                    dataItems.push(temp);
                });

                if(grid.find('tr').length == countQty) return;
                if(countGAP > 0) return;
                if(grid.find('tr').length == countQtyNotCompleted) return; 
                
                $.ajax({
                    method: "POST",
                    url: "{{ url('warung/receipt/savedetail') }}",
                    data: {
                        _token: CSRF_TOKEN,
                        receiptid: receiptid,
                        items: JSON.stringify(dataItems)
                    }
                }).done(function(response) {
                    if(response.success) {
                        $('#winNotify').modal('show');
                        $('#id_message').html(response.payload.message);
                        setTimeout(function(){
                            window.location.href = "{{ url('warung/receipt/detail') }}"+'/'+receiptid;
                        }, 1000);
                    }
                    else {
                        $('#winNotify').modal('show');
                        $('#id_message').html(response.payload.message);
                    }
                });
            });
        });
    </script>
    <style>
        .input_qty {
            border: 1px solid #d2d6de;
        }
        .input_failure {
            border: 1px solid red;
        }
        input[type='number'] {
            -moz-appearance:textfield;
        }
        input::-webkit-outer-spin-button, input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }
    </style>
@endsection