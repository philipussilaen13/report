@extends('layout.main')

@section('title')
    Goods Receipt Warung
@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">    
@endsection

@section('pageTitle')
    Goods Receipt Warung
@endsection

@section('pageDesc')
    Goods Receipt Warung
@endsection

@section('content')
    <section class="content">        
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="box" style="margin-bottom:0px;">
                <div class="box-header with-border">
                    <h3 class="box-title">Filter</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <div class="col-md-2" style="padding-left:3px;padding-right:3px;">
                            <div class="form-group" style="margin-bottom:0px;">
                                <label>Start Date</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="id_start_date">
                                </div>
                            </div>                        
						</div>
                        <div class="col-md-2" style="padding-left:3px;padding-right:3px;">
                            <div class="form-group" style="margin-bottom:0px;">
                                <label>End Date</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="id_end_date">
                                </div>
                            </div>                        
						</div>
                        <div class="col-md-2" style="padding-left:3px;padding-right:3px;">
                            <div class="form-group" style="margin-bottom:0px;">
                                <label>Status Delivery</label>
								<div>
                                    <select id="id_status" class="form-control" name="status_delivery" data-width="100%" data-minimum-results-for-search="Infinity">
                                        <option value="-1">All</option>
                                        <option value="1">Belum Dikonfirmasi</option>
                                        <option value="2">Terima Sebagian</option>
                                        <option value="3">Sudah Terima Semua Barang</option>
                                    </select>                                    
								</div>
                            </div>                        
						</div>
                        <div class="col-md-2" style="padding-left:3px;padding-right:3px;">
                            <div class="form-group" style="margin-bottom:0px;">
                                <label>Search Warung</label>
                                <div>
                                    <select id="id_warung" class="form-control" name="warungid"></select>
                                </div>
                            </div>                        
						</div>
                        <div class="col-md-4" style="padding-left:3px;padding-right:3px;">
                            <div class="col-md-8" style="padding-left:0px;padding-right:0px;">
                                <div class="form-group">
                                    <label>Transaction ID</label>
                                    <div>
                                        <input id="id_transaction" class="form-control" name="transaction_id"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left:3px;padding-right:3px;">
                                <div class="form-group">
                                    <div style="margin-top: 25px;">
                                        <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left:0px;padding-right:5px;">
                <div class="info-box" style="margin-bottom:0px;">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-hourglass-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Unprocessed Order</span>
                        <span id="id_span_unprocess" class="info-box-number">0</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left:0px;padding-right:5px;">
                <div class="info-box" style="margin-bottom:0px;">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-hourglass-half"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Partial Delivered</span>
                        <span id="id_span_partial" class="info-box-number">0</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left:0px;padding-right:5px;">
                <div class="info-box" style="margin-bottom:0px;">
                    <span class="info-box-icon bg-aqua"><i class="fa  fa-hourglass"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Fully Delivered</span>
                        <span id="id_span_fully" class="info-box-number">0</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Goods Receipt Warung</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
						<table id="gridcontent" class="table table-bordered table-hover">
							<thead>
								<tr>
                                    <th width="30">No</th>
                                    <th>Transaction Date</th>
                                    <th>Warung Name</th>
                                    <th>Locker ID</th>
                                    <th>ID Transaction</th>
                                    <th>Status DO</th>
                                    <th>Action</th>
								</tr>
							</thead>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary" onclick="this.disabled=true;">Delete</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    <script type="text/javascript">
        var start = 0;
        var limit = 25;

        $(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var startDate = new moment().startOf('month').format("YYYY-MM-DD");
            var endDate = new moment().endOf("month").format("YYYY-MM-DD");

			jQuery.fn.dataTableExt.oApi.fnFilterOnReturn = function (oSettings) {
				var _that = this;

				this.each(function (i) {
					$.fn.dataTableExt.iApiIndex = i;
					var $this = this;
					var anControl = $('input', _that.fnSettings().aanFeatures.f);
					anControl
						.unbind('keyup search input')
						.bind('keypress', function (e) {
							if (e.which == 13) {
								$.fn.dataTableExt.iApiIndex = i;
								_that.fnFilter(anControl.val());
							}
						});
					return this;
				});
				return this;
			};

            $.fn.detail = function(id) {
                window.location = "{{ url('warung/receipt/detail') }}/"+id;
            };			

            $.fn.summaryCountDelivery = function() {
                $.ajax({
                    method: "GET",
                    url: "{{ url('warung/receipt/getsummarydelivery') }}",
                    data: {
                        _token: CSRF_TOKEN,
                    }
                }).done(function(response) {
                    if(response.success) {
                        $('#id_span_unprocess').html(response.payload.data.status_unprocess);
                        $('#id_span_partial').html(response.payload.data.status_partial);
                        $('#id_span_fully').html(response.payload.data.status_completed);
                    }
                });

            };

            $('#id_start_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                toggleActive: true,
            });
            $('#id_start_date').datepicker("setDate", startDate);

            $('#id_end_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                toggleActive: true,
            });
            $('#id_end_date').datepicker("setDate", endDate);

            $('#id_status').select2();

            $('#id_warung').select2({
				width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: "{{ url('warung/transaction/warung/getlistmasterwarung') }}",
                    method: "GET",
                    dataType: 'JSON',
                    data: function (params) {
                        var query = {
                            search: params.term || '*',
                            type: 'public'
                        }
                        return query;
                    },
                    processResults: function (data, params) {
						if(data.isSuccess) {
                            return {
                                results: $.map(data.data.lockerData, function (item) {
                                    return {
                                        region_code: item.region_code,
                                        text: item.locker_name,
                                        id: item.locker_id
                                    }
                                })
                            };
						}
                    }
                }
            });
            
            $.fn.summaryCountDelivery();
            setInterval(function() {
                $.fn.summaryCountDelivery();
            }, 3000);

            $('#gridcontent').DataTable({                
                'paging'      : true,
                'lengthChange': false,
                'ordering'    : false,
                'info'        : true,
                'autoWidth'   : false,
                "processing": true,
                "serverSide": true,
                'searching' : false,                
                "pageLength": limit,
                "ajax": {
                    "url": "{{ url('warung/receipt/getlistreceipt') }}",
                    "data": function ( d ) {
                        var info = $('#gridcontent').DataTable().page.info();						
                        d.lockerid = $('#id_warung').val();
                        d.statusid = $('#id_status').val();
                        d.startdate = moment($('#id_start_date').datepicker('getDate')).format('YYYY-MM-DD');
                        d.enddate = moment($('#id_end_date').datepicker('getDate')).format('YYYY-MM-DD');
                        d.reference = $("#id_transaction").val();
                        d.start = info.start;
                        d.limit = limit;
                    },
                    "dataSrc": function(json){
                        json.draw = json.payload.draw;
                        json.recordsTotal = json.payload.count;
                        json.recordsFiltered = json.payload.count;

                        return json.payload.data;
                    }
                },
                                
                "columnDefs" : [
                    { "targets": 0, "data": "no", "className": "text-center" },
                    { "targets": 1, "data": "transaction_date", "className": "text-center"},
                    { "targets": 2, "data": "locker_name", "className": "text-center" },
                    { "targets": 3, "data": "locker_id", "className": "text-center" },
                    { "targets": 4, "data": "reference", "className": "text-center" },
                    { "targets": 5, "data": "status_receipt", "className": "text-center" },
                    { "targets": 6, "data": null, "className": "text-center",
                        "render": function ( data, type, row, meta ) {
                            return '<a onclick=$(this).detail("'+data.receipt_id+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-external-link"></i></a>';
                        }
                    }
                ]
            });
            $('#gridcontent').dataTable().fnFilterOnReturn();

            $('#id_btn_filter').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $('#gridcontent').DataTable().ajax.reload();
            });
            
        });
    </script>
@endsection