@extends('layout.main')

@section('title')
    Detail Goods Receipt Warung
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Detail Goods Receipt Warung
@endsection

@section('pageDesc')
    Detail Goods Receipt Warung
@endsection

@section('content')
    <section class="content">        
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="box" style="margin-bottom:0px;">
                <div class="box-header with-border">
                    <h3 class="box-title">Filter</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <div class="row">
                            <div class="col-md-2">
                                Agent / Warung Name
                            </div>
                            <div class="col-md-9">
                                {{ $receipt->locker_name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Transaction ID
                            </div>
                            <div class="col-md-9">
                                {{ $receipt->reference }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Transaction Date
                            </div>
                            <div class="col-md-9">
                                {{ $receipt->transaction_date }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Status DO
                            </div>
                            <div class="col-md-9">
                                {{ $receipt->status_receipt }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-left">
                <a href="{{ url('warung/receipt') }}" class="btn btn-flat btn-default">Back</a>
            </div>
            <div class="pull-right">
                <a id="id_new_delivery" href="{{ url('warung/receipt/formdetail') }}/{{ $receiptid }}" class="btn btn-flat bg-olive" type="button">New Delivery Order</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Delivery Order</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @php $statusBatch = null; $i=0; @endphp
                    @foreach ($batch as $r)
                        @php $statusBatch = $r->status_batch_id; @endphp
                        <div class="row content_batch">
                            <div class="row" style="margin-left:0px;margin-right:0px;border-bottom: 1px solid #d2d6de">
                                <div class="pull-left" style="padding-top:3px;padding-left:10px;">
                                    <strong>{{ $r->batch_no }}</strong>
                                </div>
                                <div class="pull-right" style="padding-right:5px;">
                                    <button type="button" class="btn btn-box-tool" data-toggle="collapse" data-target="#content_table{{ $r->receipt_batch_id }}" aria-expanded="false">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="row content_summary">
                                <div class="col-md-6 summary_left">
                                    <div class="row">
                                        <div class="col-md-4">
                                            Created By
                                        </div>
                                        <div class="col-md-8">
                                            {{ $r->user }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            Created Date
                                        </div>
                                        <div class="col-md-8">
                                            {{ $r->receipt_create_batch_date }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 summary_right">
                                    <div class="row">
                                        <div class="col-md-4">
                                            Status Delivery
                                        </div>
                                        <div class="col-md-8">
                                            {{ $r->status_batch }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            Goods Receive Date
                                        </div>
                                        <div class="col-md-8">
                                            {{ $r->receipt_batch_date }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="content_table{{ $r->receipt_batch_id }}" class="row collapse content_table">
                                <table class="table table-condensed">
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Product Name</th>
                                        <th>SKU</th>
                                        <th style="width: 120px">Product Code</th>
                                        <th style="width: 120px">@php echo ($i==0) ? "Qty Order" : "Qty Order Not Completed"; @endphp</th>
                                        <th style="width: 120px">Qty Delivery</th>
                                        <th style="width: 150px">Received By Warung</th>
                                    </tr>
                                    @php $no=1; @endphp
                                    @foreach($r->receipt_item as $item)
                                        @php $param = json_decode($item->params); @endphp
                                        <tr>
                                            <td>@php echo $no; @endphp</td>
                                            <td>{{ $item->name }}</td>
                                            <td>@php echo $param->sku; @endphp</td>
                                            <td>@php echo isset($param->barcode) ? $param->barcode : ''; @endphp</td>
                                            <td>{{ $item->totorder }}</td>
                                            <td>{{ $item->qty_delivery }}</td>
                                            <td>{{ $item->qty_receipt }}</td>
                                        </tr>
                                        @php $no++; @endphp
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        @php $i++; @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    <script type="text/javascript">
        var start = 0;
        var limit = 25;

        $(function() {
            var statusBatch = '{{ $statusBatch }}';
            $('#id_new_delivery').show();
            if(statusBatch == '1') {
                $('#id_new_delivery').hide();
            }

            $('.collapse').on('shown.bs.collapse', function(){
                $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            }).on('hidden.bs.collapse', function(){
                $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
            });            
        });
    </script>
    <style>
        .content_batch {
            margin-left:0px;
            margin-right:0px;
            margin-top:5px;
            margin-bottom:5px;
            border: 1px solid #d2d6de;
        }
        .content_batch .content_summary {
            margin-left:0px;
            margin-right:0px;
        }
        .content_batch .content_summary .summary_left{
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .content_batch .content_summary .summary_right{
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .content_batch .content_table {
            margin-top: 5px;
            margin-bottom: 5px;
            margin-left:0px;
            margin-right:0px;
            padding-left: 5px;
            padding-right: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
        }

    </style>
@endsection