@extends('layout.main')

@section('title')
    Agent Product BOM
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Product BOM
@endsection

@section('pageDesc')
    Agent Product BOM
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <a href="{{ url('warung/catalog/formproductbom/1/0') }}" class="btn btn-flat bg-olive" type="button">Tambah</a>
            </div>
            <div class="clearfix"></div>
        </div>
		<div class="row">
            <div class="box box-filter">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
						<div class="col-md-3">
							<div class="form-group">
								<label>Nama Produk</label>
                                <div>
                                    <input id="product_name" class="form-control" name="product_name"/>
                                </div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label>BOM Code</label>
                                <div>
                                    <input id="bom_code" class="form-control" name="bom_code"/>
                                </div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Region</label>
								<div>
									<select id="region" class="form-control select2-region" name="region"></select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Status</label>
								<div>
									<select id="publish_status" class="form-control select2-publish-status" name="publish_status"></select>
								</div>
							</div>
                        </div>
						
                        <div class="col-md-1">
                            <div class="form-group">
                                <div style="margin-top: 25px">
                                    <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                </div>
                            </div>
        				</div>
    				</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <div class="form-group">
                        <div class="col-md-4">
                    		<h3 class="box-title">Product</h3>
                        </div>

                        <div class="col-md-8 no-padding">
                            <button id="btn-export" type="button" class="btn btn-success btn-primary btn-sm pull-right bg-green" onclick="getAjaxTransactionDownload()" style="margin-right: 10px; width: auto">
                                Download Excel
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
						<table id="id_grid_product" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="10">No</th>
									<th>Product</th>
									<th>Barcode</th>
									<th>SKU</th>
									<th>Weight</th>
									<th>Width</th>
									<th>Stock</th>
									<th width="100"></th>
								</tr>
							</thead>
						</table>					
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    <script type="text/javascript">
        var start = 0;
        var limit = 25;
		var publish_status = {!! json_encode($publish_status) !!};
		var regions = {!! json_encode($regions) !!};
        $(function(){

            $('.select2').select2();

            $(".select2-publish-status").select2({
				width: '100%',
          		data: publish_status
            });

            $(".select2-region").select2({
				width: '100%',
          		data: regions
            });
            
            $.fn.detailProduct = function(productid) {
                window.open(
                    ('{{ route('bom-product-form', ['flag' => '', 'productid' => '']) }}/' + 2 + '/'+ productid),
                    '_blank' 
              	);
            };
            $.fn.winDelContent = function(id) {
				$('#winDelContent').modal('show');
                $('#id_btn_del').one('click',function(){
                    $.ajax({
                        url: "{{ url('warung/catalog/delproductbom') }}",
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}",
                            "productid": id
                        },
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.success) {
                                $('#winDelContent').modal('hide');
                                $('#id_grid_product').DataTable().ajax.reload();
                            }
                        }
                    });
                });				
			};

			$('#id_grid_product').DataTable({
                'searching' : false,
				'paging'      : true,
				'lengthChange': false,
				'ordering'    : false,
				'info'        : true,
				'autoWidth'   : false,
				"processing": true,
				"serverSide": true,
				"pageLength": limit,
				"ajax": {
					"url": "{{ url('warung/catalog/getlistproductbom') }}",
					"data": function ( d ) {
						var info = $('#id_grid_product').DataTable().page.info();
						d.product_name 	= $('#product_name').val();
						d.bom_code		= $('#bom_code').val();
						d.region		= $('#region').val();
						d.publish_stts	= $('#publish_status').val();
						d.start 		= info.start;
						d.limit 		= limit;
					},
					"dataSrc": function(json){
						json.draw = json.payload.draw;
						json.recordsTotal = json.payload.count;
						json.recordsFiltered = json.payload.count;
						return json.payload.data;
					}
				},
				"columnDefs" : [
					{ "targets": 0, "data": "no", "width": "5%", "className": "text-right" },
					{ "targets": 1, "data": "name", "width": "25%" },
					{ "targets": 2, "data": "barcode", "width": "16%" },
					{ "targets": 3, "data": "sku", "width": "17%" },
					{ "targets": 4, "data": "weight", "width": "9%", "className": "text-right" },
					{ "targets": 5, "data": "width", "width": "9%", "className": "text-right" },
					{ "targets": 6, "data": "stock", "width": "9%", "className": "text-right" },
					{ "targets": 7, "data": null,
						"render": function ( data, type, row, meta ) {
							return '<a onclick=$(this).detailProduct("'+data.productid+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a> <a onclick=$(this).winDelContent("'+data.productid+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
						}, "width": "10%", "className": "text-center"
					},
				],
			});

            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                $('#id_grid_product').DataTable().ajax.reload();
            });
        });

        function getAjaxTransactionDownload() {
            if($("#id_grid_product").DataTable().rows().count() == 0){
                alert('No Data');
            } else {

                 showLoading('.box-filter', 'box-filter');
                 showLoading('.box-transaction-table', 'box-transaction-table');
                 
                 $.ajax({
                 	url: '{{ route('get-list-product-bom') }}',
                     data: {
                         _token       	: '{{ csrf_token() }}',
                         product_name	: $('#product_name').val(),
 						 bom_code		: $('#bom_code').val(),
 						 region			: $('#region').val(),
 						 publish_stts	: $('#publish_status').val(),
                         is_download	: true,
                     },
                     type: 'get',
                     responseType: 'blob', // important
                     async : true,
                     success: function (response, textStatus, request) {

                         hideLoading('.box-filter', 'box-filter');
                         hideLoading('.box-transaction-table', 'box-transaction-table');
                         
                         var a = document.createElement("a");
                         a.href = response.file; 
                         a.download = response.name;
                         document.body.appendChild(a);
                         a.click();
                         a.remove();
                     },
                     error: function (ajaxContext) {
                         hideLoading('.box-filter', 'box-filter');
                         hideLoading('.box-transaction-table', 'box-transaction-table');
                         alert('Export error: '+ajaxContext.responseText);
                     }
                 });
                
            }
         }
    </script>
@endsection