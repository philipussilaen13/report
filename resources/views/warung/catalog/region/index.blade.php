@extends('layout.main')

@section('title')
    Agent Region
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Region
@endsection

@section('pageDesc')
    Agent Region
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <a href="{{ url('warung/catalog/crudregion') }}/1/all" class="btn btn-flat bg-olive" type="button">Tambah</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Master Region</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
                        <table id="gridcontent" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Region</th>
                                    <th>List City</th>
                                    <th>Minimum Purchase</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="winContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Region</h4>
                </div>
                <div class="modal-body">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag" value="1">
                        <input type="hidden" name="regionid">
                        <div class="form-group">
                            <label>Region Name</label>
                            <input type="text" class="form-control" name="region" placeholder="Region Name ...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winNotifContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div id="id_content_notif" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            var formContent = $('#id_form_content');
            formContent[0].reset();

            $.fn.loadGridContent = function() {
                showLoading('.div_content', 'div_content');
                $.getJSON(
                    "{{ url('warung/catalog/getlistregion') }}", '',
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr>';
                                html += '<td style="width: 10px">'+(index+1)+'</td>';
                                html += '<td>'+(isEmpty(record.region_name) ? '' : record.region_name)+' ('+(isEmpty(record.region_code) ? '' : record.region_code)+')'+'</td>';
                                html += '<td>'+(isEmpty(record.city_name) ? '' : record.city_name)+'</td>';
                                html += '<td>'+(isEmpty(record.min_purchase) ? '' : record.min_purchase)+'</td>';
                                html += '<td style="width: 110px;">';
                                    html += '<a onclick=$(this).detailForm("2","'+record.id+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a>';
                                    html += '<a onclick=$(this).winDelContent("'+record.id+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
                                html += '</td>';
                            html += '</tr>';
                        });
                        $('#gridcontent > tbody').html(html);
                        hideLoading('.div_content', 'div_content');
                    }
                );
            };

            $.fn.detailForm = function(flag, id) {
                window.location = "{{ url('warung/catalog/crudregion') }}/2/"+id;
            };
            
            $.fn.winDelContent = function(id) {
                $('#winDelContent').modal('show');
                $('#id_btn_del').one('click',function() {
                    $.ajax({
                        url: "{{ url('warung/catalog/delregion') }}",
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}",
                            "regionid": id
                        },
                        success: function(response){
                            if(response.success) {
                                $('#winDelContent').modal('hide');
                                $.fn.loadGridContent();
                            }
                        }
                    });
                });
            };

            $.fn.loadGridContent();
            $('#id_btn_add').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $.fn.winContent('1','','');
            });
        });
    </script>
@endsection