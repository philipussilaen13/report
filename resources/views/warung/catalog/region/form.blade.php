@extends('layout.main')

@section('title')
    Form Region
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Form Region
@endsection

@section('pageDesc')
    Form Region
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-left">
                <a href="{{ url('warung/catalog/region') }}" class="btn btn-flat btn-default">Back</a>
            </div>
            <div class="pull-right">
                <a id="id_save" class="btn btn-flat bg-olive">Save</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="font-weight: bold;">Form City</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body form_content">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag" value="1">
                        <input type="hidden" name="regionid" />

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Region Code *</label>
                                    <input type="text" class="form-control" name="regioncode" placeholder="Region Code ...">
                                </div>
                                <div class="form-group">
                                    <label>Region Name *</label>
                                    <input type="text" class="form-control" name="regionname" placeholder="Region Name ...">
                                </div>
                                <div class="form-group">
                                    <label>Minimum Purchase</label>
                                    <input type="number" class="form-control" name="minpurchase" value="50000" placeholder="Minimum Purchase ...">
                                </div>
                                <hr style="border:1px solid #9b9b9b" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row" style="margin-left:0px;margin-right:0px;">
                                    <div class="pull-right">
                                        <a id="id_add_city" class="btn btn-flat bg-olive">Add</a>
                                    </div>
                                </div>
                                <div class="table-responsive div_content">
                                    <table id="gridcontentcity" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="180">City</th>
                                                <th width="60"></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div id="winContentListCity" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">City</h4>
                </div>
                <div class="modal-body" style="padding:25px;">
                    <div class="row">
                        <div class="pull-left">
                            <div class="input-group">
                                <input id="id_product_keyword" type="text" name="keyword" class="form-control">
                                <a id="id_btn_keyword" class="input-group-addon"><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive div_content">
                            <table id="id_table_listcity" class="table table-striped">
                                <thead>
                                <tr>
                                    <th width="30"></th>
                                    <th width="10">No</th>
                                    <th>City Code</th>
                                    <th>City Name</th>
                                    <th>Region</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_add_city" type="button" class="btn btn-primary">Add</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winNotifContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div id="id_content_notif" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/fileinput/bootstrap-filestyle.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $("input[type=number]").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            var inputValue = $(this).val().replace(/[^0-9\.]/g,'');
            if(((event.which != 8 && event.which != 13 && event.which != 116) || inputValue.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $(function(){
            var formContent = $('#id_form_content');
            var flag = '{{ $vflag }}';
            var regionid = '{{ $vcityid }}';

            formContent[0].reset();
            formContent.validate({
                focusCleanup: true,
                onfocusout: false,
                rules: {
                    regioncode : {required: true},
                    regionname : {required: true},
                    minpurchase: {required: true}
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                },
            });

            $.fn.loadGridListCity = function(keyword) {
                $.ajax({
                    url: "{{ url('warung/catalog/getlistcity') }}",
                    type: 'GET',
                    data: {
                        search: keyword
                    },
                    success: function(response){
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr data-id="'+record.id+'" data-citycode="'+record.city_code+'" data-cityname="'+record.city_name+'">';
                                html += '<td style="width: 10px">';
                                if(isEmpty(record.regions_id)) {
                                    html += '<input type="checkbox" class="styled styled-primary" name="chkQ5" value="option1" aria-label="Single checkbox One">'; 
                                }                                    
                                html += '</td>';
                                html += '<td style="width: 10px">'+(index+1)+'</td>';
                                html += '<td>'+(isEmpty(record.city_code) ? '' : record.city_code)+'</td>';
                                html += '<td>'+record.city_name+'</td>';
                                html += '<td>'+(isEmpty(record.region_name) ? '' : record.region_name)+'</td>';
                            html += '</tr>';
                        });
                        $('#id_table_listcity > tbody').html(html);
                    }
                });

            };

            $.fn.winListCity = function() {
                $('#winContentListCity').modal('show');

                $.fn.loadGridListCity('');

                $('#id_product_keyword').keypress(function(e) {
                    if(e.which == 13) {
                        var keyword = $(this).val();
                        $.fn.loadGridListCity(keyword);
                    }
                });

                $('#id_btn_keyword').off('click').on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    
                    var keyword = $('#id_product_keyword').val();
                    $.fn.loadGridListCity(keyword);
                });

                $('#id_btn_add_city').off('click').on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    var grid = $('#id_table_listcity');
                    var countChecked = 0; var resSelected = [];
                    grid.find('tr').each(function () {
                        var row = $(this);
                        if (row.find('input[type="checkbox"]').is(':checked')) {
                            var tempSelected = {
                                id: row.attr('data-id'),
                                citycode: row.attr('data-citycode'),
                                cityname: row.attr('data-cityname'),
                            };
                            resSelected.push(tempSelected);
                        }
                    });

                    $('#winContentListCity').modal('hide');

                    if(resSelected.length > 0) {
                        var tempGridContentCity = [];
                        if($('#gridcontentcity > tbody').find('tr').length > 0) {
                            $('#gridcontentcity > tbody').find('tr').each(function (i, rec) {
                                tempGridContentCity.push($(rec).attr('data-id'));
                            });
                        }
                        
                        $.each(resSelected, function(i, rec) {
                            $.when( 
                                $.ajax({ url:"{{ url('warung/catalog/checkcity') }}/"+rec.id, type: 'GET', }) 
                            ).done(function( a1, a2 ) {
                                if(a1.success) {                                    
                                    var obj = a1.payload.data;
                                    if(isEmpty(obj.regions_id)) {
                                        if($.inArray(rec.id, tempGridContentCity) == -1) {
                                            $('#gridcontentcity > tbody:last-child').append('<tr data-id="'+rec.id+'"><td>'+rec.cityname+'</td><td><a onclick=$.fn.removeCity("'+rec.id+'") style="cursor:pointer;"><i class="fa fa-fw fa-trash"></i></a></td></tr>');
                                        }                                    
                                    }          
                                }
                            });

                        });                        

                    }
                                        
                });
            };

            $.fn.removeCity = function(id) {
                $('#gridcontentcity > tbody:last-child').find('[data-id="'+id+'"]')[0].remove();
            };

            $('#id_add_city').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                $.fn.winListCity();
            });

            $('#id_save').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                                
                var grid = $('#gridcontentcity > tbody');
                var paramCityID = [];

                if(formContent.valid()) {
                    var formData = new FormData(formContent[0]);
                    
                    if(grid.find('tr').length > 0) {
                        grid.find('tr').each(function (i, rec) {
                            paramCityID.push($(rec).attr('data-id'));
                        });

                        formData.append('cityid', paramCityID.join(','));

                        $.ajax({
                            url: "{{ url('warung/catalog/saveregion') }}",
                            type: 'POST',
                            data: formData,
                            async: false,
                            cache: false,
                            contentType: false,
                            enctype: 'multipart/form-data',
                            processData: false,
                            success: function (response) {
                                if(response.success) {
                                    $('#id_content_notif').html('<p>'+response.payload.response.message+'</p>');
                                    $('#winNotifContent').modal('show');
                                    setTimeout(function(){
                                        $('#winNotifContent').modal('hide');
                                        window.location.href = "{{ url('warung/catalog/region') }}";
                                    }, 1000);                            
                                }                        
                                else {
                                    $('#id_content_notif').html('<p>'+response.payload.response.message+'</p>');
                                    $('#winNotifContent').modal('show');
                                }
                            }
                        });
                    }
                }
            });

            if(flag == '2') {
                var urlParams = "{{ url('warung/catalog/getregionbyid') }}" + '/' + regionid;
                $.getJSON(
                    urlParams, '',
                    function(response) {                        
                        if(response.success) {
                            var obj = response.payload.data;
                            formContent.find('input[name="flag"]').val(flag);
                            formContent.find('input[name="regionid"]').val(regionid);
                            formContent.find('input[name="regioncode"]').val(obj.region_code);
                            formContent.find('input[name="regionname"]').val(obj.region_name);
                            formContent.find('input[name="minpurchase"]').val(obj.min_purchase);

                            $.each(obj.cities, function(i, rec) {
                                $('#gridcontentcity > tbody:last-child').append('<tr data-id="'+rec.id+'"><td>'+rec.city_name+'</td><td><a onclick=$.fn.removeCity("'+rec.id+'") style="cursor:pointer;"><i class="fa fa-fw fa-trash"></i></a></td></tr>');
                            });

                        }
                    }
                );                
            }
            
        });
    </script>
@endsection