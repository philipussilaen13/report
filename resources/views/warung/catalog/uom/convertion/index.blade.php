@extends('layout.main')

@section('title')
    Agent UOM Convertion
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent UOM Convertion
@endsection

@section('pageDesc')
    Agent UOM Convertion
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <a href="{{ url('warung/catalog/formconvertionuom') }}/1/all" class="btn btn-flat bg-olive" type="button">Tambah</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">UOM Convertion</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
                        <table id="gridcontent" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Convertion Name</th>
                                <th>UOM Source</th>
                                <th>UOM Target</th>
                                <th>Multiply Rate</th>
                                <th>Divide Rate</th>
                                <th>Product</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winContentDetailProduct" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detail Product</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive div_detail_product">
                            <table id="id_table_detail_product" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product</th>
                                    <th>SKU</th>
                                    <th>Purchase Price</th>
                                    <th>Selling Price</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            $.fn.loadGridContent = function() {
                showLoading('.div_content', 'div_content');
                $.getJSON(
                    "{{ url('warung/catalog/getlistconvertionuom') }}", '',
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr>';
                            html += '<td style="width: 10px">'+(index+1)+'</td>';
                            html += '<td>'+record.name+'</td>';
                            html += '<td>'+record.uom_source+'</td>';
                            html += '<td>'+record.uom_target+'</td>';
                            html += '<td>'+record.multiply_rate+'</td>';
                            html += '<td>'+record.divide_rate+'</td>';
                            html += '<td style="width: 60px;"><a onclick=$(this).winDetailProduct("'+record.uom_convertion_id+'") style="cursor:pointer;"><i class="fa fa-fw fa-database"></i></a></td>';
                            html += '<td style="width: 110px;">';
                            html += '<a onclick=$(this).detailForm("2","'+record.uom_convertion_id+'") style="cursor:pointer;"><i class="fa fa-fw fa-pencil"></i></a>';
                            html += '<a onclick=$(this).winDelContent("'+record.uom_convertion_id+'") style="cursor:pointer;"><i class="fa fa-fw fa-trash"></i></a>';
                            html += '</td>';
                            html += '</tr>';
                        });
                        $('#gridcontent > tbody').html(html);
                        hideLoading('.div_content', 'div_content');
                    }
                );
            };

            $.fn.detailForm = function(flag, convertionid) {
                window.location = "{{ url('warung/catalog/formconvertionuom/') }}/2/"+convertionid;
            };

            $.fn.winDetailProduct = function(uomconvertionid) {
                $('#winContentDetailProduct').modal('show');

                showLoading('.div_detail_product', 'div_detail_product');
                $.getJSON(
                    "{{ url('warung/catalog/getproductbyconvertionuom') }}/"+uomconvertionid, '',
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr>';
                            html += '<td style="width: 10px">'+(index+1)+'</td>';
                            html += '<td>'+record.name+'</td>';
                            html += '<td>'+record.sku+'</td>';
                            html += '<td>'+record.purchase_price+'</td>';
                            html += '<td>'+record.selling_price+'</td>';
                            html += '</tr>';
                        });

                        $('#id_table_detail_product > tbody').html(html);
                        hideLoading('.div_detail_product', 'div_detail_product');
                    }
                );

            };

            $.fn.winDelContent = function(id) {
                $('#winDelContent').modal('show');
                $('#id_btn_del').on('click',function(e) {
                    if (e.isPropagationStopped()) return;
                    e.stopPropagation();

                    $.ajax({
                        url: "{{ url('warung/catalog/delconvertionuom') }}",
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}",
                            "id": id
                        },
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.success) {
                                $('#winDelContent').modal('hide');
                                $.fn.loadGridContent();
                            }
                        }
                    });
                });
            };

            $.fn.loadGridContent();
        });
    </script>

    <style>
        .clearicon {
            cursor: pointer;
        }

        .hideicon {
            display: none;
        }

    </style>
@endsection