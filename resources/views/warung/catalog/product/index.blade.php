@extends('layout.main')

@section('title')
    Agent Product
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Product
@endsection

@section('pageDesc')
    Agent Product
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <a href="{{ url('warung/catalog/addproduct/1/0') }}" class="btn btn-flat bg-olive" type="button">Tambah</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="box box-filter">
                <div class="box-header with-border">
                    <h3 class="box-title">Filter</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <div class="col-md-6" style="padding-left:15px;padding-right:20px;">
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Nama Produk</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <input id="id_product" type="text" name="productname" class="form-control" placeholder="Nama Produk ..." />
                                </div>
                            </div>
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Category</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <select id="id_category" class="form-control" name="category"></select>
                                </div>
                            </div>
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Sub Category</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <select id="id_subcategory" class="form-control" name="subcategory"></select>
                                </div>
                            </div>
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Sub Sub Category</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <select id="id_subsubcategory" class="form-control" name="subsubcategory"></select>
                                </div>
                            </div>
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Region</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <select id="id_region" class="form-control" name="region"></select>
                                </div>
                            </div>
                            <div class="row" style="margin-top:10px;margin-bottom:10px;">
                                <a id="id_btn_cari" class="btn btn-flat bg-olive" type="button">Cari</a>
                            </div>

                        </div>
                        <div class="col-md-6" style="padding-left:20px;padding-right:15px;">
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Product Code / SKU</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <input id="id_sku" type="text" name="sku" class="form-control" placeholder="Product Code / SKU ..." />
                                </div>
                            </div>
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Mobile Category</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <select id="id_mobile" class="form-control" name="mobilecategory"></select>
                                </div>
                            </div>
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Brand</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <select id="id_brand" class="form-control" name="brand"></select>
                                </div>
                            </div>
                            <div class="row">
                                <h5 style="margin-top:5px;margin-bottom:5px;">Status</h5>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <select id="id_status" class="form-control" name="status">
										<option value="1">Penjualan + Belanja Stok</option>
										<option value="2">Penjualan</option>
										<option value="3">Belanja Stok</option>
                                        <option value="0">Unpublish</option>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box box-transaction-table">
                <div class="box-header">
                    <div class="form-group">
                        <div class="col-md-4">
                    		<h3 class="box-title">Product</h3>
                        </div>

                        <div class="col-md-8 no-padding">
                            <button id="btn-export" type="button" class="btn btn-success btn-primary btn-sm pull-right bg-green" onclick="getAjaxTransactionDownload()" style="margin-right: 10px; width: auto">
                                Download Excel
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
						<table id="id_grid_product" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="10">No</th>
									<th>Product</th>
									<th>Barcode</th>
									<th>SKU</th>
									<th>Thumbnail</th>
									<th>Stock</th>
								</tr>
							</thead>
						</table>					
                    </div>
                </div>
            </div>
        </div>
    </section>
	
    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>
	
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 25;

        $(function(){
            $('#id_product').val('');

            $.fn.detailProduct = function(productid) {
        		window.open(
                    ('{{ route('add-catalog-product', ['flag' => '', 'productid' => '']) }}/' + 2 + '/'+ productid),
                    '_blank' 
              	);
            };
			
            $.fn.winDelContent = function(id) {
                $('#winDelContent').modal('show');
                $('#id_btn_del').one('click',function(){
                    $.ajax({
                        url: "{{ url('warung/catalog/delproduct') }}",
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}",
                            "productid": id
                        },
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.success) {
                                $('#winDelContent').modal('hide');
                                $('#id_grid_product').DataTable().ajax.reload();
                            }
                        }
                    });
                });				
			};
			
			$('#id_grid_product').DataTable({
                'searching' : false,
				'paging'      : true,
				'lengthChange': false,
				'ordering'    : false,
				'info'        : true,
				'autoWidth'   : false,
				"processing": true,
				"serverSide": true,
				"pageLength": limit,
				"ajax": {
					"url": "{{ url('warung/catalog/getlistproduct') }}",
					"data": function ( d ) {
						var info = $('#id_grid_product').DataTable().page.info();
						d.search = $('#id_product').val();
						d.mobileid = $('#id_mobile').val();
						d.categoriesid = $('#id_category').val();
						d.subcategoriesid = $('#id_subcategory').val();
						d.subsubcategoriesid = $('#id_subsubcategory').val();
                        d.brandid = $('#id_brand').val();
                        d.skuid = $('#id_sku').val();
                        d.statusid = $('#id_status').val();
                        d.regionid = $('#id_region').val();
						d.start = info.start;
						d.limit = limit;
					},
					"dataSrc": function(json){
						json.draw = json.payload.draw;
						json.recordsTotal = json.payload.count;
						json.recordsFiltered = json.payload.count;

						return json.payload.data;
					}
				},
				"columnDefs" : [
					{ "targets": 0, "data": "no" },
					{ "targets": 1, "data": "name", "width": "40%" },
					{ "targets": 2, "data": "barcode", "width": "10%" },
					{ "targets": 3, "data": "sku", "width": "10%" },
					{ "targets": 4, "data": "image_url", "width": "15%", "className": "text-center",
						"render": function ( data, type, row, meta ) {
							return '<img alt="'+data+'" src="'+data+'" style="max-width:50%;height:auto;">';
						}
					 },
					{ "targets": 5, "data": "stock", "width": "7%", "className": "text-right" },
					{ "targets": 6, "data": null, "width": "18%",
						"render": function ( data, type, row, meta ) {
							return '<a onclick=$(this).detailProduct("'+data.productid+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a> <a onclick=$(this).winDelContent("'+data.productid+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
						}
					},
				],
			});

            $('#id_category').select2({
                width: '100%',
                placeholder: "Category ...",
                minimumResultsForSearch: -1,
                ajax: {
                    url: "{{ url('warung/catalog/getlistcategory') }}",
                    method: "GET",
                    processResults: function (data, params) {
                        var obj = jQuery.parseJSON(data);

                        var newData = [];
                        var firstData = {
                            id: '0',
                            text: 'All',
                            level: ''
                        };

                        newData.push(firstData);
                        $.map(obj.payload.data, function (item) {
                            var temp = {
                                id: item.id,
                                text: item.name,
                                level: item.level
                            };
                            newData.push(temp);
                        });

                        var result = {
                            results: newData
                        };

                        return result;
                    }
                }
            });

            $('#id_subcategory').select2({
                width: '100%',
                placeholder: "Sub Category ...",
                minimumResultsForSearch: -1,
                allowClear: true
            });

            $('#id_subsubcategory').select2({
                width: '100%',
                placeholder: "Sub Sub Category ...",
                minimumResultsForSearch: -1,
                allowClear: true
            });

            $('#id_category').on('select2:select', function (e) {
                e.stopImmediatePropagation();

                var vParam = e.params.data;
                $('#id_subcategory').html('').select2({data: [{id: '', text: '', level: ''}]});
                $.ajax({
                    type: 'GET',
                    url: "{{ url('warung/catalog/getlistcategory') }}",
                    data: {
                        categoryid: vParam.id,
                        level: 1,
                    }
                }).then(function (response) {
                    var obj = jQuery.parseJSON(response);
                    if(obj.success) {
                        var firstData = new Option('All', '0', false, false);
                        $('#id_subcategory').append(firstData);

                        $.each(obj.payload.data, function(index, record) {
                            var newData = {
                                id: record.id,
                                text: record.name,
                                level: record.level
                            };
                            var newOption = new Option(newData.text, newData.id, false, false);
                            $('#id_subcategory').append(newOption);
                        });
                    }
                });
            });

            $('#id_subcategory').on('select2:select', function (e) {
                e.stopImmediatePropagation();

                var vParam = e.params.data;

                $('#id_subsubcategory').html('').select2({data: [{id: '', text: '', level: ''}]});
                $.ajax({
                    type: 'GET',
                    url: "{{ url('warung/catalog/getlistcategory') }}",
                    data: {
                        categoryid: vParam.id,
                        level: 2,
                    }
                }).then(function (response) {
                    var obj = jQuery.parseJSON(response);

                    var firstData = new Option('All', '0', false, false);
                    $('#id_subsubcategory').append(firstData);

                    $.each(obj.payload.data, function(index, record) {
                        var newData = {
                            id: record.id,
                            text: record.name,
                            level: record.level
                        };
                        var newOption = new Option(newData.text, newData.id, false, false);
                        $('#id_subsubcategory').append(newOption);
                    });
                });
            });

            $('#id_brand').select2({
                width: '100%',
                placeholder: "Brand ...",
                minimumResultsForSearch: -1,
                ajax: {
                    url: "{{ url('warung/catalog/getlistbrand') }}",
                    method: "GET",
                    processResults: function (data, params) {
                        var obj = jQuery.parseJSON(data);

                        var newData = [];
                        var firstData = {
                            id: '0',
                            text: 'All',
                            level: ''
                        };

                        newData.push(firstData);
                        $.map(obj.payload.data, function (item) {
                            var temp = {
                                id: item.id,
                                text: item.name,
                            };
                            newData.push(temp);
                        });

                        var result = {
                            results: newData
                        };

                        return result;
                    }
                }
            });

            $('#id_mobile').select2({
                width: '100%',
                placeholder: "Mobile Category ...",
                minimumResultsForSearch: -1,
                ajax: {
                    url: "{{ url('warung/catalog/getlistcategorymobile') }}",
                    method: "GET",
                    processResults: function (data, params) {
                        var obj = jQuery.parseJSON(data);

                        var newData = [];
                        var firstData = {
                            id: '0',
                            text: 'All',
                            level: ''
                        };

                        newData.push(firstData);
                        $.map(obj.payload.data, function (item) {
                            var temp = {
                                id: item.id,
                                text: item.name,
                            };
                            newData.push(temp);
                        });

                        var result = {
                            results: newData
                        };

                        return result;
                    }
                }
            });

            $('#id_region').select2({
                width: '100%',
                placeholder: "Region ...",
                minimumResultsForSearch: -1,
                ajax: {
                    url: "{{ url('warung/catalog/getmasterregion') }}",
                    method: "GET",
                    processResults: function (data, params) {
                        var newData = [];
                        var firstData = {
                            id: '0',
                            text: 'All',
                            level: ''
                        };
                        newData.push(firstData);
                        $.map(data.payload.data, function (item) {
                            var temp = {
                                id: item.region_code,
                                text: item.region_name,
                            };
                            newData.push(temp);
                        });

                        var result = {
                            results: newData
                        };

                        return result;
                    }
                }
            });

            $('#id_btn_cari').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                $('#id_grid_product').DataTable().ajax.reload();
            });

        });

        function getAjaxTransactionDownload() {
            if($("#id_grid_product").DataTable().rows().count() == 0){
                alert('No Data');
            } else {

                 showLoading('.box-filter', 'box-filter');
                 showLoading('.box-transaction-table', 'box-transaction-table');
                 
                 $.ajax({
                 	url: '{{ route('product-download-excel') }}',
                     data: {
                         _token             : '{{ csrf_token() }}',
 						search 				: $('#id_product').val(),
						mobileid 			: $('#id_mobile').val(),
						categoriesid 		: $('#id_category').val(),
						subcategoriesid 	: $('#id_subcategory').val(),
						subsubcategoriesid 	: $('#id_subsubcategory').val(),
                     	brandid 			: $('#id_brand').val(),
                        skuid 				: $('#id_sku').val(),
                        statusid 			: $('#id_status').val(),
                        regionid 			: $('#id_region').val(),
                        is_download			: true,
                     },
                     type: 'get',
                     responseType: 'blob', // important
                     async : true,
                     success: function (response, textStatus, request) {

                         hideLoading('.box-filter', 'box-filter');
                         hideLoading('.box-transaction-table', 'box-transaction-table');
                         
                         var a = document.createElement("a");
                         a.href = response.file; 
                         a.download = response.name;
                         document.body.appendChild(a);
                         a.click();
                         a.remove();
                     },
                     error: function (ajaxContext) {
                         hideLoading('.box-filter', 'box-filter');
                         hideLoading('.box-transaction-table', 'box-transaction-table');
                         alert('Export error: '+ajaxContext.responseText);
                     }
                 });
                
            }
         }
    </script>
@endsection