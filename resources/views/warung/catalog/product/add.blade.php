@extends('layout.main')

@section('title')
    Agent Product
@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Product
@endsection

@section('pageDesc')
    Agent Product
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <a id="id_reset" class="btn btn-flat btn-default">Reset</a>
                <a id="id_save" class="btn btn-flat bg-olive">Save</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box div_content">
                <div class="box-header">
                    <h3 class="box-title" style="font-weight: bold;">Product</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag" value="1">
                        <input type="hidden" name="productid">
                        <input type="hidden" name="sku">
                        <input type="hidden" name="fileold">
                        <input type="hidden" name="filetypeold">
                        <input type="hidden" name="publishold">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Product Name *</label>
                                    <input type="text" class="form-control" name="productname" placeholder="Product Name ...">
                                </div>
                                <div class="form-group">
                                    <label>Product Code *</label>
                                    <input type="text" class="form-control" name="productcode" placeholder="Product Code ...">
                                </div>
                                <div class="form-group">
                                    <label>Category *</label>
                                    <select id="id_categories" class="form-control" name="categoriesid">
                                        <option value="0">- Choose Category -</option>
                                        @foreach ($categories as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Sub Category</label>
                                    <select id="id_subcategories" class="form-control" name="subcategoriesid">
                                        <option value="0">- Choose Sub Category -</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Sub Sub Category</label>
                                    <select id="id_subsubcategories" class="form-control" name="subsubcategoriesid">
                                        <option value="0">- Choose Sub Sub Category -</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Grouping Mobile *</label>
                                    <select id="id_categoriesmobile" class="form-control" name="categoriesmobile">
                                        <option value="0">- Choose Grouping Mobile -</option>
                                        @foreach ($categoriesMobile as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Brand Name</label>
                                    <select id="id_brand" class="form-control" name="brandid">
                                        <option value="0">- Choose Brand -</option>
                                        @foreach ($brands as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" style="height:145px;">
                                    <label class="file-label">Product Image *</label>
                                    <div class="row" style="margin-left:0px;margin-right:0px;">
                                        <div class="col-md-6" style="padding-left:0px;padding-right:0px;">
                                            <div class="row" style="margin-left:0px;margin-right:0px;">
                                                <label class="btn btn-primary">
                                                    Browse File Image&hellip; <input id="id_fileupload" type="file" name="files" style="display: none;">
                                                </label>
                                            </div>
                                            <div class="row" style="margin-left:0px;margin-right:0px;">
                                                <span class="file-label">Tipe file : JPEG, JPG. Max size 500 kb</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <img id="id_img_product" class="imageproduct">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Product Detail / Description</label>
                                    <textarea id="id_description" class="form-control" rows="3" name="description" placeholder="Product Detail / Description ..."></textarea>
                                </div>
                            </div>
                        </div>

                        <hr style="border:1px solid #9b9b9b" />
                        <div class="row">
                            <div class="box-header" style="padding-left:15px;padding-right:15px;margin-bottom:10px;">
                                <h3 class="box-title" style="font-weight: bold;">Price Information</h3>
                            </div>
                            <div class="col-md-12" style="padding-left:30px;padding-right:30px;">
                                <div class="row">
                                    <div class="pull-right">
                                        <a id="id_btn_add_price" class="btn btn-flat bg-olive">Add</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table id="gridcontentprice" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Region</th>
                                                    <th>Purchase Price</th>
                                                    <th>Selling Price</th>
                                                    <th>Suggest Price</th>
                                                    <th>Discount</th>
                                                    <th>Promo Price</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>

                        <hr style="border:1px solid #9b9b9b" />
                        <div class="row">
                            <div class="box-header" style="padding-left:15px;padding-right:15px;margin-bottom:10px;">
                                <h3 class="box-title" style="font-weight: bold;">Product Unit</h3>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Smallest Unit *</label>
                                    <select id="id_smallestuom" class="form-control" name="smallestuom">
                                        <option value="0">- Choose UOM -</option>
                                        @foreach ($uom as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Weight (gr)</label>
                                    <input type="number" class="form-control" name="weight" placeholder="Input Weight Per Unit ...">
                                </div>
                                <div class="form-group">
                                    <label>Height</label>
                                    <input type="number" class="form-control" name="height" placeholder="Input Height Per Unit ...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="height:60px;"></div>
                                <div class="form-group">
                                    <label>Width (mm)</label>
                                    <input type="number" class="form-control" name="width" placeholder="Input Width Per Unit ...">
                                </div>
                                <div class="form-group">
                                    <label>Volume (ml)</label>
                                    <input type="number" class="form-control" name="volume" placeholder="Input Volume ...">
                                </div>
                            </div>
                        </div>
                        <hr style="border:1px solid #9b9b9b" />
                        <div class="row">
                            <div class="box-header" style="padding-left:15px;padding-right:15px;margin-bottom:10px;">
                                <h3 class="box-title" style="font-weight: bold;">OTHERS</h3>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Alert Quantity *</label>
                                    <input type="number" class="form-control" name="alertqty" placeholder="Alert Quantity ...">
                                </div>
                                <div class="form-group">
                                    <label>Publish Type</label>
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" name="publish" value="1" class="flat-red" checked> Penjualan + Belanja Stok
                                        </label>
                                        <label>
                                            <input type="radio" name="publish" value="2" class="flat-red"> Penjualan
                                        </label>
                                        <label>
                                            <input type="radio" name="publish" value="3" class="flat-red"> Belanja Stok
                                        </label>
                                        <label>
                                            <input type="radio" name="publish" value="0" class="flat-red"> Unpublish
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="margin-top:15px;">
                                        <input type="checkbox" name="ishighlight"> Is Highlight
                                    <label>                                    
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div id="winNotify" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">
                    <p id="id_message"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winContentPrice" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Price</h4>
                </div>
                <div class="modal-body" style="padding:25px;">
                    <form id="id_form_content_price" role="form">
                        <input type="hidden" name="act" />
                        <input type="hidden" name="priceid" />
                        <input type="hidden" name="regioncode" />

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Region *</label>
                                    <select id="id_select_region" class="form-control" name="regionid">
                                        <option value="0">- Choose Region -</option>
                                        @foreach ($regions as $r)
                                            <option value="{{ $r->region_code }}">{{ $r->region_name }}</option>
                                        @endforeach
                                    </select>                                    
                                </div>
                                <div class="form-group">
                                    <label>Purchase Price *</label>
                                    <input type="number" class="form-control" name="purchaseprice" placeholder="eg. 10.000" min="0">
                                </div>
                                <div class="form-group">
                                    <label>Selling Price *</label>
                                    <input type="number" class="form-control" name="sellingprice" placeholder="eg. 10.000">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6" style="margin: 0px;padding: 0px;">
                                        <input type="checkbox" id="id_ispromo" name="id_ispromo"><label>&nbsp;Fix Price</label>
                                        <input type="text" class="form-control" id="promoprice" name="promoprice" placeholder="eg. 10.000" style="width: 80%;" onkeypress="allowNumbersOnly(event)" >
                                    </div>
                                    <div class="col-md-6" style="margin: 0px;padding: 0px;">
                                        <input type="checkbox" id="c_discount" name="c_discount"><label>&nbsp;Discount (Percentage)</label>
                                        <input type="text" class="form-control" id="discount" name="discount" placeholder="eg. 13.7%" style="width: 80%" onkeypress="percentageOnly(event)" >
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 87px">
                                    <label>Suggest Price *</label>
                                    <input type="number" class="form-control" name="suggestprice" placeholder="eg. 10.000">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_save_price" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winDelPrice" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del_price" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/fileinput/bootstrap-filestyle.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $.validator.addMethod('filesize', function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        });

        $(function(){
            var vflag = '{{ $flag }}';
            var vproductID = '{{ $productid }}';
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formContent = $("#id_form_content");
            var formData = new FormData(formContent[0]);
            formContent[0].reset();

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            });

            $("input[type=number]").on("keypress keyup blur",function (event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                var inputValue = $(this).val().replace(/[^0-9\.]/g,'');
                if(((event.which != 8 && event.which != 13 && event.which != 116) || inputValue.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $('#promoprice').attr('disabled', true);
            $('#id_ispromo').click( function(){
                $('#promoprice').val('');
                if( $(this).is(':checked') ){
                    $('#promoprice').removeAttr('disabled');

                    $('#discount').val('');
                    $('#discount').attr('disabled', true);
                    $('#c_discount').prop("checked", false);
                } else {
                    $('#promoprice').attr('disabled', true);
                }
            });
            
            $('#discount').attr('disabled', true);
            $('#c_discount').click( function(){
                $('#discount').val('');
                if( $(this).is(':checked') ){
                    $('#discount').removeAttr('disabled');

                    $('#promoprice').val('');
                    $('#promoprice').attr('disabled', true);
                    $('#id_ispromo').prop("checked", false);
                } else {
                    $('#discount').attr('disabled', true);
                }
            });
            
            formContent.validate({
                ignore: [],
                onfocusout: false,
                rules: {
                    productname : {required: true},
                    productcode : {required: true},
                    categoriesid : {valueNotEquals: "0"},
                    categoriesmobile : {valueNotEquals: "0"},
                    // brandid : {valueNotEquals: "0"},
                    smallestuom: {valueNotEquals: "0"},
                    // weight: {required: true},
                    // height: {required: true},
                    // width: {required: true},
                    // volume: {required: true},
                    alertqty: {required: true},
                    // files: {
                    //     required: true,
                    //     extension: "jpg|jpeg|png",
                    // }
                },
                highlight: function(element) {
                    if($(element).attr('type') != 'file') {
                        $(element).closest('.form-group').addClass('has-error');
                    }
                    else {
                        $('.file-label').addClass('label-red');
                    }
                },
                unhighlight: function(element) {
                    if($(element).attr('type') != 'file') {
                        $(element).closest('.form-group').removeClass('has-error');
                    }
                    else {
                        $('.file-label').removeClass('label-red');
                    }
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if($(element).attr('type') == 'file') return;

                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                },
            });

            $.fn.loadSubCategories = function(selector, categoryid, level) {
                return $.ajax({
                    method: "GET",
                    url: "{{ url('warung/catalog/getlistcategory') }}",
                    data: {
                        categoryid: categoryid,
                        level: level
                    }
                }).done(function(response) {
                    var obj = jQuery.parseJSON(response);
                    var html = '';
                    if(obj.success) {
                        html += '<option value="0">- Choose Value -</option>';
                        $.each(obj.payload.data, function (i, rec) {
                            html += '<option value="'+rec.id+'">'+rec.name+'</option>';
                        });
                        $('#'+selector).html(html);

                        return $('#'+selector);
                    }
                });
            };

            // Load Data Product When first loading
            $.fn.getProductByID = function(productid) {
                showLoading('.div_content', 'div_content');
                $.getJSON(
                    "{{ url('warung/catalog/getproductbyid') }}/"+productid, '',
                    function(response) {
                        if(response.success) {
                            var obj = response.payload.data;

                            formContent.find('input[name="flag"]').val(vflag);
                            formContent.find('input[name="productid"]').val(productid);
                            formContent.find('input[name="productname"]').val(obj.name);
                            formContent.find('input[name="productcode"]').val(obj.barcode);
                            formContent.find('input[name="sku"]').val(obj.sku);
                            formContent.find('input[name="fileold"]').val(obj.picture);
                            formContent.find('input[name="filetypeold"]').val(obj.filetype);
                            $('#id_brand').val(obj.product_brands_id);

                            if( !isEmpty(obj.product_categories_id)) {
                                $('#id_categories').val(obj.product_categories_id);
                            }

                            if( !isEmpty(obj.product_categories_sub_id)) {
                                $.when($.fn.loadSubCategories('id_subcategories',obj.product_categories_id, '1')).done(function(v){
                                    $('#id_subcategories').val(obj.product_categories_sub_id);
                                });
                            }

                            if( !isEmpty(obj.product_categories_sub_sub_id)) {
                                $.when($.fn.loadSubCategories('id_subsubcategories',obj.product_categories_sub_id, '1')).done(function(v){
                                    $('#id_subsubcategories').val(obj.product_categories_sub_sub_id);
                                });
                            }

                            $('#id_categoriesmobile').val(obj.product_categories_mobile_id);
                            $('#id_img_product').attr('src', obj.picture_url);

                            $('#id_smallestuom').val(obj.low_uom_conversion);
                            formContent.find('input[name="weight"]').val(obj.weight);
                            formContent.find('input[name="height"]').val(obj.height);
                            formContent.find('input[name="width"]').val(obj.width);
                            formContent.find('input[name="volume"]').val(obj.volume);
                            formContent.find('input[name="alertqty"]').val(obj.alert_qty);
                            formContent.find('textarea[name="description"]').val(obj.description);
                            
                            formContent.find('input[name="ishighlight"]').prop('checked', parseInt(obj.is_highlight));
                            if(isEmpty(obj.is_highlight)) {
                                formContent.find('input[name="ishighlight"]').prop('checked', 0);
                            }

                            if(obj.publish == 0) {
                                $($('input[name="publish"]')[3]).iCheck('check');
                            }
                            else if(obj.publish == 1) {
                                $($('input[name="publish"]')[0]).iCheck('check');
                            }
                            else if(obj.publish == 2) {
                                $($('input[name="publish"]')[1]).iCheck('check');
                            }
                            else if(obj.publish == 3) {
                                $($('input[name="publish"]')[2]).iCheck('check');
                            }

                            formContent.find('input[name="publishold"]').val(obj.publish);

                            $.fn.loadPriceByRegion(productid);                                                                                    
                            hideLoading('.div_content', 'div_content');
                        }
                    }
                );
            };

            $.fn.loadPriceByRegion = function(productid) {
                $.getJSON(
                    "{{ url('warung/catalog/getproductbyid') }}/"+productid, '',
                    function(response) {
                        if(response.success) {
                            var obj = response.payload.data;
                            var html = '';
                            $.each(obj.price_region, function(i, rec) {
                                var recordJSON = Base64.encode(JSON.stringify(rec));

                                html += '<tr id="'+rec.id+'" data-regionid="'+(isEmpty(rec.regions_id) ? '' : rec.regions_id)+'" data-purchaseprice="'+(isEmpty(rec.purchase_price) ? '' : rec.purchase_price)+'" data-sellingprice="'+(isEmpty(rec.selling_price) ? '' : rec.selling_price)+'" data-suggestprice="'+(isEmpty(rec.suggest_price) ? '' : rec.suggest_price)+'" data-discount="'+(isEmpty(rec.discount) ? '' : rec.discount)+'" data-ispromo="'+rec.is_promo+'" data-promoprice="'+(isEmpty(rec.selling_price_promo) ? '' : rec.selling_price_promo)+'">';
                                    html += '<td>'+rec.regions_name+'</td>';
                                    html += '<td>'+rec.purchase_price+'</td>';
                                    html += '<td>'+rec.selling_price+'</td>';
                                    html += '<td>'+rec.suggest_price+'</td>';
                                    html += '<td>'+rec.discount+'</td>';
                                    html += '<td>'+rec.selling_price_promo+'</td>';
                                    html += '<td>';
                                    html += '<a onclick=$(this).winAddPrice("'+vflag+'","'+vproductID+'","'+recordJSON+'","upd") style="cursor:pointer;"><i class="fa fa-fw fa-pencil"></i></a>';
                                    html += '<a onclick=$.fn.removePrice("'+rec.id+'") style="cursor:pointer;"><i class="fa fa-fw fa-trash"></i></a>';
                                    html += '</td>';
                                html += '</tr>';
                            });
                            $('#gridcontentprice > tbody:last-child').html(html);
                        }
                    }
                );
            }

            $.fn.removeLocalPrice = function(e) {
                $(e).parent().parent().remove();
            };

            $.fn.removePrice = function(priceid) {
                $('#winDelPrice').modal('show');
                $('#id_btn_del_price').off('click').on('click', function(){
                    $.ajax({
                        method: "POST",
                        url: "{{ url('warung/catalog/delprice') }}",
                        data: {
                            _token: CSRF_TOKEN,
                            priceid: priceid,
                        }
                    }).done(function(response) {
                        if(response.success) {
                            $('#winDelPrice').modal('hide');
                            $.fn.loadPriceByRegion(vproductID);                            
                        }
                    });
                });				
            }

            $.fn.winAddPrice = function(vflag=1, vProductID=null, record=null, vact='add') {
                var formContentPrice = $('#id_form_content_price');
                formContentPrice.validate({
                    ignore: [],
                    onfocusout: false,
                    rules: {
                        regionid : {valueNotEquals: "0"},
                        purchaseprice : {required: true},
                        sellingprice : {required: true},
                    },
                    highlight: function(element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if(element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        }
                        else {
                            error.insertAfter(element);
                        }
                    },
                });

                $('#winContentPrice').modal('show');
                $('#winContentPrice').on('hide.bs.modal', function (e) {
                    if (e.isPropagationStopped()) return;
                    e.stopPropagation();
                    
                    formContentPrice[0].reset();
                });

                $('#id_btn_save_price').on('click', function(e) {
                    e.stopImmediatePropagation();
                    
                    if(formContentPrice.valid()) {
                        var price_id = formContentPrice.find('input[name="priceid"]').val(); 
                        var region = formContentPrice.find('select[name="regionid"]').val(); 
                        var regionText = formContentPrice.find('select[name="regionid"] :selected').text(); 
                        var purchase_price = formContentPrice.find('input[name="purchaseprice"]').val(); 
                        var selling_price = formContentPrice.find('input[name="sellingprice"]').val(); 
                        var suggest_price = formContentPrice.find('input[name="suggestprice"]').val(); 
                        var discount = formContentPrice.find('input[name="discount"]').val(); 
                        var ispromo = $('#id_ispromo').is(":checked") == true ? '1' : '0'; 
                        var promo_price = formContentPrice.find('input[name="promoprice"]').val(); 
                        var uuid = generateUUID();
                        
                        if(vflag == '1') {
                            var tempGridContentPrice = [];
                            $('#gridcontentprice > tbody').find('tr').each(function (i, rec) {
                                tempGridContentPrice.push($(rec).attr('data-regionid'));
                            });

                            if($.inArray(region, tempGridContentPrice) == -1) {
                                $('#gridcontentprice > tbody:last-child').append('<tr id="'+uuid+'" data-regionid="'+region+'" data-purchaseprice="'+purchase_price+'" data-sellingprice="'+selling_price+'" data-suggestprice="'+suggest_price+'" data-discount="'+discount+'" data-ispromo="'+ispromo+'" data-promoprice="'+promo_price+'"><td>'+regionText+'</td><td>'+purchase_price+'</td><td>'+selling_price+'</td><td>'+suggest_price+'</td><td>'+discount+'</td><td>'+promo_price+'</td><td><a onclick=$.fn.removeLocalPrice(this) style="cursor:pointer;"><i class="fa fa-fw fa-trash"></i></a></td></tr>');
                            }
                            else {
                                $('#winNotify').modal('show');
                                $('#id_message').html('Data price di region ini sudah ada');
                            }
                        } else {
                            $.ajax({
                                method: "POST",
                                url: "{{ url('warung/catalog/saveprice') }}",
                                data: {
                                    _token: CSRF_TOKEN,
                                    act: vact,
                                    priceid: price_id,
                                    productid: vProductID,
                                    regioncode: region,
                                    purchaseprice: purchase_price,
                                    sellingprice: selling_price,
                                    suggestprice: suggest_price,
                                    discount: discount,
                                    ispromo: ispromo,
                                    promoprice: promo_price,
                                }
                            }).done(function(response) {
                                if(response.success) {
                                    $.fn.loadPriceByRegion(vProductID);
                                }
                            });
                        }
                        
                        $('#winContentPrice').modal('hide'); 
                    }
                });

                formContentPrice.find('input[name="act"]').val(vact);
                if(vact == 'upd') {
                    var record = jQuery.parseJSON(Base64.decode(record));
                    formContentPrice.find('input[name="act"]').val(vact);
                    formContentPrice.find('input[name="priceid"]').val(record.id);
                    formContentPrice.find('select[name="regionid"]').val(record.regions_id);
                    formContentPrice.find('input[name="purchaseprice"]').val(record.purchase_price); 
                    formContentPrice.find('input[name="sellingprice"]').val(record.selling_price);
                    formContentPrice.find('input[name="suggestprice"]').val(record.suggest_price);
                    formContentPrice.find('input[name="discount"]').val(record.discount);
                    
                    $('#id_ispromo').prop('checked', false);
                    if(record.is_promo == '1') {
                        $('#id_ispromo').prop('checked', true);
                        $('#promoprice').removeAttr('disabled');

                        $('#c_discount').prop("checked", false);
                        $('#discount').attr('disabled', true);
                    }

                    if(record.discount){
                        $('#c_discount').prop("checked", true);
                        $('#discount').removeAttr('disabled');

                        $('#id_ispromo').prop('checked', false);
                        $('#promoprice').attr('disabled', true);
                    }
                    formContentPrice.find('input[name="promoprice"]').val(record.selling_price_promo);
                }
            };

            $('#id_fileupload').on('change', function(e) {
                var file = $(this)[0].files[0];

                var img = new Image();
                var imgwidth = 0;
                var imgheight = 0;
                img.src = URL.createObjectURL(file);
                img.onload = function() {
                    imgwidth = this.width;
                    imgheight = this.height;
                    var imageType = file.type;
                    $('#id_img_product').attr('src', URL.createObjectURL(file));
                };
                $('.file-label').removeClass('label-red');
            });

            $('#id_categories').on('change', function(e) {
                var field = $(this);
                if(field.val() != 0) {
                    $('#id_subsubcategories').html('');
                    $.fn.loadSubCategories('id_subcategories',field.val(), '1');
                }
            });

            $('#id_subcategories').on('change', function(e) {
                var field = $(this);
                if(field.val() != 0) {
                    $.fn.loadSubCategories('id_subsubcategories',field.val(), '2');
                }
            });

            $('#id_btn_add_price').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                $.fn.winAddPrice(vflag, vproductID, 'add');
                
                $('#c_discount').prop("checked", false);
                $('#id_ispromo').prop("checked", false);
                $('#discount').val('');
                $('#discount').attr('disabled', true);
                $('#promoprice').val('');
                $('#promoprice').attr('disabled', true);
            });

            $('#id_save').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                if(formContent.valid()) {
                    var formData = new FormData(formContent[0]);

                    if(vflag == '1') {
                        var tempPrice = [];
                        $('#gridcontentprice > tbody').find('tr').each(function (i, rec) {
                            var temp = {};
                            temp.regionid = $(rec).attr('data-regionid');
                            temp.purchaseprice = $(rec).attr('data-purchaseprice');
                            temp.sellingprice = $(rec).attr('data-sellingprice');
                            temp.suggestprice = $(rec).attr('data-suggestprice');
                            temp.discount = $(rec).attr('data-discount');
                            temp.ispromo = $(rec).attr('data-ispromo');
                            temp.promoprice = $(rec).attr('data-promoprice');

                            tempPrice.push(temp);
                        });

                        // if(tempPrice.length == 0) {
                        //     $('#winNotify').modal('show');
                        //     $('#id_message').html('Price must be filled');
                        //     return;
                        // }

                        formData.append('params_price', JSON.stringify(tempPrice));
                    }
                    
                    $.ajax({
                        url: "{{ url('warung/catalog/saveproduct') }}",
                        type: 'POST',
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        success: function (response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.success) {
                                $('#winNotify').modal('show');
                                $('#id_message').html(obj.payload.message);
                                setTimeout(function(){
                                    window.location.href = "{{ url('warung/catalog/product') }}";
                                }, 1000);
                            }
                            else {
                                $('#winNotify').modal('show');
                                $('#id_message').html(obj.payload.message);
                            }
                        }
                    });
                }
            });

            $('#id_reset').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                formContent[0].reset();
                formContent.find('input[name="flag"]').val('1');
                formContent.find('input[name="productid"]').val('');
                formContent.find('input[name="sku"]').val('');
                formContent.find('input[name="fileold"]').val('');
                formContent.find('input[name="filetypeold"]').val('');
                $('#id_simulation').html('');
                $('#id_img_product').attr('src', null);
            });

            if(vflag == '2') {
                $.fn.getProductByID(vproductID);
            }
        });
        function allowNumbersOnly(e) {
            var code = (e.which) ? e.which : e.keyCode;
            if (code > 31 && (code < 48 || code > 57)) {
                e.preventDefault();
            }
        }

        function percentageOnly(e) {
            var code = (e.which) ? e.which : e.keyCode;
            if (code > 31 && (code < 48 || code > 57) && code != 46) {
                e.preventDefault();
            }
        }
    </script>
    <style>
        .imageproduct {
            width: 100%;
            height: 100%;
        }

        .strikethrough {
            position: relative;
        }
        .strikethrough:before {
            position: absolute;
            content: "";
            left: 0;
            top: 50%;
            right: 0;
            border-top: 1px solid;
            border-color: green;
            -webkit-transform:rotate(-5deg);
            -moz-transform:rotate(-5deg);
            -ms-transform:rotate(-5deg);
            -o-transform:rotate(-5deg);
            transform:rotate(-5deg);
        }
        .price {
            margin-left: 10px;
        }

        .label-red {
            color: red;
        }

    </style>
@endsection