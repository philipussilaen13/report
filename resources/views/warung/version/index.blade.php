@extends('layout.main')

@section('title')
    Version Warung
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Version Warung
@endsection

@section('pageDesc')
    Version Warung
@endsection

@section('content')
    <section class="content">        
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <a href="{{ url('warung/version/form/none/none') }}" class="btn btn-flat bg-olive" type="button">Tambah</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Version Warung</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
						<table id="gridcontent" class="table table-bordered table-hover">
							<thead>
								<tr>
                                    <th width="30">No</th>
                                    <th>Version</th>
                                    <th>Create at</th>
                                    <th>Update at</th>
                                    <th>Create By</th>
                                    <th>Update By</th>
                                    <th></th>
								</tr>
							</thead>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary" onclick="this.disabled=true;">Delete</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    <script type="text/javascript">
        var start = 0;
        var limit = 25;

        $(function() {
            $.fn.detailContent = function(id) {
                window.location = "{{ url('warung/version/form/2') }}/"+id;
            };

            $.fn.winDelContent = function(id) {
                $('#winDelContent').modal('show');
                $('#id_btn_del').one('click',function(){
                    $.ajax({
                        url: "{{ url('warung/version/delversion') }}",
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}",
                            "id": id
                        },
                        success: function(response){
//                             var obj = jQuery.parseJSON(response);
//                             if(obj.success) {
							if(response.success) {	
                                $('#winDelContent').modal('hide');
                                $('#gridcontent').DataTable().ajax.reload();
                            }
                        }
                    });
                });				
			};

			$('#gridcontent').DataTable({
                'searching' : false,
				'paging'      : true,
				'lengthChange': false,
				'ordering'    : false,
				'info'        : true,
				'autoWidth'   : false,
				"processing": true,
				"serverSide": true,
				"pageLength": limit,
				"ajax": {
					"url": "{{ url('warung/version/getversion') }}",
					"data": function ( d ) {
                        var info = $('#gridcontent').DataTable().page.info();
						d.start = info.start;
						d.limit = limit;
					},
					"dataSrc": function(json){
						json.draw = json.draw;
						json.recordsTotal = json.count;
						json.recordsFiltered = json.count;

						return json.data;
					}
				},
				"columnDefs" : [
					{ "targets": 0, "data": "no" },
					{ "targets": 1, "data": "version" },
					{ "targets": 2, "data": "created_at" },
					{ "targets": 3, "data": "updated_at" },
					{ "targets": 4, "data": "created_by" },
					{ "targets": 5, "data": "updated_by" },
					{ "targets": 6, "data": null,
						"render": function ( data, type, row, meta ) {
                            return '<a onclick=$(this).detailContent("'+data.id+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a> <a onclick=$(this).winDelContent("'+data.id+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
						}
					},
				],
			});
        });

    </script>

@endsection