@extends('layout.main')

@section('title')
    Form Version
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Form Version
@endsection

@section('pageDesc')
    Form Version
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-left">
                <a href="{{ url('warung/version') }}" class="btn btn-flat btn-default">Back</a>
            </div>
            <div class="pull-right">
                <a id="id_save" class="btn btn-flat bg-olive">Save</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="font-weight: bold;">Form Version</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body form_content" style="min-height:600px;">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag" value="1" />
                        <input type="hidden" name="id" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Version *</label>
                                    <input type="text" class="form-control" name="version" placeholder="Version ...">
                                </div>
                            </div>                                
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>   

    <div id="winNotifContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div id="id_content_notif" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/fileinput/bootstrap-filestyle.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            var formContent = $('#id_form_content');
            var flag = '{{ $vflag }}';
            var versionid = '{{ $vVersionID }}';
            
            formContent[0].reset();

            formContent.validate({
                focusCleanup: true,
                onfocusout: false,
                rules: {
                    version : {required: true},
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                },
            });
            
            $('#id_save').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                if(formContent.valid()) {
                    var formData = new FormData(formContent[0]);
                    $.ajax({
                        url: "{{ url('warung/version/saveversion') }}",
                        type: 'POST',
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        success: function (response) {
                            if(response.success) {
                                $('#id_content_notif').html('<p>'+response.payload.response.message+'</p>');
                                $('#winNotifContent').modal('show');
                                setTimeout(function(){
                                    $('#winNotifContent').modal('hide');
                                    window.location.href = "{{ url('warung/version') }}";
                                }, 1000);                            
                            }                        
                            else {
                                $('#id_content_notif').html('<p>'+response.payload.response.message+'</p>');
                                $('#winNotifContent').modal('show');
                            }
                        }
                    });
                }
            });

            if(flag == '2') {
                var urlParams = "{{ url('warung/version/getversionbyid') }}" + '/' + versionid;
                $.getJSON(
                    urlParams, '',
                    function(response) {                        
                        if(response.success) {
                            var obj = response.payload.data;
                            formContent.find('input[name="flag"]').val(flag);
                            formContent.find('input[name="version"]').val(obj.version);
                            formContent.find('input[name="id"]').val(obj.id);
                        }                        
                    }
                );                
            }

        });
    </script>
@endsection