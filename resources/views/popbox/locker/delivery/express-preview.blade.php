@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Locker Delivery
@endsection

@section('css')
	{{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
	Locker Pickup Request
@endsection

@section('pageDesc')
	List Pickup Request
@endsection

@section('content')
	@if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

	<div class="box box-solid">
		<div class="box-body">
			<form method="post" action="{{ url('popbox/locker/delivery/pickup') }}" id="form-submit">
				<input type="hidden" name="formType" value="excel">
				<table class="table table-condensed table-striped">
					{{ csrf_field() }}
					<thead>
		            	<tr>
		              		<th>No</th>
		              		<th>Order Number</th>
		              		<th>Customer Phone</th>
		            	</tr>
		          	</thead>
		          	<tbody>
		          		@php
		          			$isValid = true;
		          		@endphp
			            @foreach ($orderList as $index => $item)
			              	<tr>
			                	<td>{{ $index+1 }}</td>
			                	<td>
			                		{{ $item->orderNumber }}
			                		@if ($item->orderNumberValidate)
			                			<span class="label label-success">Valid</span>
			                		@else
			                			<span class="label label-danger">Empty or Invalid Format</span>
			                			@php
			                				if ($isValid) {
			                					$isValid = false;
			                				}
			                			@endphp
			                		@endif
			                		<input type="hidden" name="orderNumber[]" value="{{ $item->orderNumber }}">
			                	</td>
			                	<td>
			                		{{ $item->phone }}
			                		@if ($item->phoneValidate)
			                			<span class="label label-success">Valid</span>
			                		@else
			                			<span class="label label-danger">Empty or Invalid Format</span>
			                			@php
			                				if ($isValid) {
			                					$isValid = false;
			                				}
			                			@endphp
			                		@endif
			                		<input type="hidden" name="phone[]" value="{{ $item->phone }}">
			                	</td>
				            </tr>
				        @endforeach
		          	</tbody>
		      	</table>
			</form>
			<a href="{{ url('popbox/locker/delivery/pickup') }}">
				<button class="btn btn-flat btn-info pull-left"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
			</a>
			@if ($isValid)
				<button class="btn btn-flat btn-success pull-right" id="btn-submit">Submit</button>
			@else
				<button class="btn btn-flat btn-danger pull-right" disabled>Submit</button>
			@endif
		</div>
	</div>
	
@endsection

@section('js')
	{{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.select2').select2();
			$('#btn-submit').on('click', function() {
				$('#form-submit').submit();
			});
		});
	</script>
@endsection