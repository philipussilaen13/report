@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Locker Delivery
@endsection

@section('css')
	{{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
	Locker Delivery Activities
@endsection

@section('pageDesc')
	List all delivery activities on Locker
@endsection

@section('content')
	<div class="row">
		{{-- Overdue --}}
        <div class="col-lg-4 col-xs-12">
            <div class="box box-solid bg-maroon" id="collapseOnline">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-maroon btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Overdue <strong><span id="onlineCount"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-maroon" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3 id="online-all">{{ $countOverdue }}</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-clock text-white"></i>
                        </div>
                        <a href="{{ url('popbox/locker/delivery')."?status=OVERDUE" }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        {{-- In Store --}}
        <div class="col-lg-4 col-xs-12">
            <div class="box box-solid bg-blue text-white" id="collapseOffline">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-blue btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">In Store <strong><span id="offlineCount"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-blue" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3 id="offline-all">{{ number_format($countInStore) }}</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-navicon-round text-white"></i>
                        </div>
                        <a href="{{ url('popbox/locker/delivery')."?status=IN_STORE" }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        {{-- Customer Taken --}}
        <div class="col-lg-4 col-xs-12">
            <div class="box box-solid bg-green" id="collapseAll">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-green btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Customer Taken <strong><span id="allCount"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-green" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3 id="all-count">{{ number_format($countCustomerTaken) }}</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark-round text-white"></i>
                        </div>
                        <a href="{{ url('popbox/locker/delivery')."?status=CUSTOMER_TAKEN" }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="box box-solid">
		<div class="box-body">
			<button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modal-default">Filter</button>
			<a href="{{ url('popbox/locker/delivery') }}">
				<button class="btn btn-flat btn-warning">Reset</button>
			</a>
			<hr>
			{{-- Table --}}
			<div class="table-responsive">
				<table class="table table-condensed table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Locker Name</th>
							<th>Courier</th>
							<th>Parcel Number</th>
							<th>Type</th>
							<th>Phone</th>
							<th>Locker Number & Size</th>
							<th>Store Time</th>
							<th>Take Time</th>
							<th>Status</th>
							{{--<th>PIN</th>--}}
							<th>Overdue Time</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php
							$nowDateTime = time()*1000;
						@endphp
						@foreach ($lockerActivitiesDb as $index => $item)
							<tr>
								<td>{{ $lockerActivitiesDb->firstItem() + $index }}</td>
								<td>{{ $item->locker_name }}</td>
								<td>{{ $item->company_name }}</td>
								<td>
									@if ($item->expressType == 'COURIER_STORE')
										{{ $item->expressNumber }}
									@else
										{{ $item->customerStoreNumber }}
									@endif
								</td>
								<td>
									@if ($item->expressType == 'COURIER_STORE')
										<span class="label label-primary">Last Mile</span>
									@elseif ($item->expressType == 'CUSTOMER_REJECT')
										<span class="label label-info">Return</span>
									@elseif ($item->expressType == 'CUSTOMER_STORE')
										<span class="label label-default">PopSend</span>
									@endif
								</td>
								<td>
									@if ($item->expressType == 'COURIER_STORE')
										{{ $item->takeUserPhoneNumber }}
									@else
										{{ $item->storeUserPhoneNumber }}
									@endif
								</td>
								<td>{{ $item->locker_number }} - {{ $item->locker_size }}</td>
								<td>{{ date('Y-m-d H:i:s',round($item->storeTime/1000)) }}</td>
								<td>
									@if (empty($item->takeTime))
										-
									@else
										{{ date('Y-m-d H:i:s',round($item->takeTime/1000)) }}
									@endif
								</td>
								<td>
									@if ($item->status == 'CUSTOMER_TAKEN')
										<span class="label label-success">Customer Taken</span>
									@elseif ($item->status == 'COURIER_TAKEN')
										<span class="label label-warning">Courier Taken</span>
									@elseif ($item->status == 'OPERATOR_TAKEN')
										<span class="label label-warning">Operator Taken</span>
									@elseif($item->status == 'IN_STORE' && $nowDateTime > $item->overdueTime && $item->expressType == 'COURIER_STORE')
										{{-- <span class="label label-info">In Store</span> --}}
										<span class="label label-danger">Overdue</span>
									@elseif($item->status == 'IN_STORE')
										<span class="label label-info">In Store</span>
									@endif
								</td>
								{{--<td>{{ $item->validateCode }}</td>--}}
								<td>{{ date('Y-m-d H:i:s',round($item->overdueTime/1000)) }}</td>
								<td>
									<a href="{{ url('popbox/locker/delivery/detail')."?parcelId=$item->id" }}">
										<span class="label label-primary"><i class="fa fa-edit"></i></span>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				{{ $lockerActivitiesDb->links() }}
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-default">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Filter</h4>
				</div>
				<div class="modal-body">
					<form id="form-filter">
						<div class="form-group">
							<label>Range Date</label>
							<input type="text" name="dateRange" class="form-control" id="dateRange">
						</div>
						<div class="form-group">
							<label>Locker</label>
							<select class="form-control select2" name="locker">
								<option value="">All Locker</option>
								@foreach ($lockerList as $item)
									<option value="{{ $item->id }}">{{ $item->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Type</label>
							<select class="form-control select2" name="expressType">
								<option value="">All Type</option>
								@foreach ($typeList as $index => $element)
									<option value="{{ $index }}">{{ $element }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Parcel Number</label>
							<input type="text" name="parcelNumber" class="form-control">
						</div>
						<div class="form-group">
							<label>PIN</label>
							<input type="text" name="pin" class="form-control">
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="text" name="phone" class="form-control">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select class="form-control select2" name="status">
								<option value="">All Status</option>
								@foreach ($statusList as $index => $element)
									<option value="{{ $index }}">{{ $element }}</option>
								@endforeach
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
				</div>
			</div>
		</div>
	</div>
	
@endsection

@section('js')
	{{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	<script type="text/javascript">
		// Modal on show
		$('#modal-default').on('shown.bs.modal', function(event) {
			// inisiate select2
			$('.select2').select2(); 

			// Inisiate Date Range
			var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });

            // check if param value exist
            @if (!empty($parameter['locker']))
            $('select[name=locker]').val('{{ $parameter['locker'] }}').trigger('change');
            @endif
            @if (!empty($parameter['expressType']))
            $('select[name=expressType]').val('{{ $parameter['expressType'] }}').trigger('change');
            @endif
            @if (!empty($parameter['status']))
            $('select[name=status]').val('{{ $parameter['status'] }}').trigger('change');
            @endif
		});

		$('#btn-filter').on('click', function(event) {
			$('#form-filter').submit();
		});
	</script>
@endsection