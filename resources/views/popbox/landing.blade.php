@extends('layout.main')

@section('title')
    PopBox Summary
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
    PopBox Locker Summary
@endsection

@section('pageDesc')
    PopBox Locker Status and Performance
@endsection

@section('content')
    <section class="content">
        {{-- Summary --}}
        <div class="row">
            {{-- Online --}}
            <div class="col-lg-4 col-xs-12">
                <div class="box box-solid bg-maroon" id="collapseOnline">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn bg-maroon btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <i class="fa fa-map-marker"></i>
                        <h3 class="box-title">Online <strong><span id="onlineCount"></span></strong></h3>
                    </div>
                    <div class="box-body" style="padding: 0px;">
                        <div class="small-box bg-maroon" style="margin-bottom: 0px;">
                            <div class="inner text-white">
                                <h3 id="online-all">{{ $onlineCounter }}</h3>
                                <p><strong id="all-locker">Locker</strong></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-pin text-white"></i>
                            </div>
                            <a href="{{ url('popbox/locker/location') }}?status=1" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Offline --}}
            <div class="col-lg-4 col-xs-12">
                <div class="box box-solid bg-black text-white" id="collapseOffline">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn bg-black btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <i class="fa fa-map-marker"></i>
                        <h3 class="box-title">Offline <strong><span id="offlineCount"></span></strong></h3>
                    </div>
                    <div class="box-body" style="padding: 0px;">
                        <div class="small-box bg-black" style="margin-bottom: 0px;">
                            <div class="inner text-white">
                                <h3 id="offline-all">{{ $offlineCounter }}</h3>
                                <p><strong id="all-locker">Locker</strong></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-pin text-white"></i>
                            </div>
                            <a href="{{ url('popbox/locker/location') }}?status=0" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- All --}}
            <div class="col-lg-4 col-xs-12">
                <div class="box box-solid bg-green" id="collapseAll">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn bg-green btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <i class="fa fa-map-marker"></i>
                        <h3 class="box-title">Terdaftar <strong><span id="allCount"></span></strong></h3>
                    </div>
                    <div class="box-body" style="padding: 0px;">
                        <div class="small-box bg-green" style="margin-bottom: 0px;">
                            <div class="inner text-white">
                                <h3 id="all-count">{{ $allCounter }}</h3>
                                <p><strong id="all-locker">Locker</strong></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-stalker text-white"></i>
                            </div>
                            <a href="{{ url('popbox/locker/location') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Graph --}}
        <div class="box box-solid">
            <div class="box-body">
                {{-- form --}}
                <form id="form-performance">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                               <label>Locker Name</label>
                               <select class="form-control select2" name="lockerId">
                                   <option value="">All Locker</option>
                                   @foreach ($lockerLocationDb as $item)
                                        <option value="{{ $item->locker_id }}">{{ $item->name }}</option>
                                   @endforeach  
                               </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                               <label>Locker Type</label>
                               <select class="form-control select2" name="lockerType">
                                   <option value="">All Type</option>
                                   @foreach ($buildingTypeDb as $item)
                                        <option value="{{ $item->id_building }}">{{ $item->building_type }}</option>
                                   @endforeach
                               </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                               <label>Date Range</label>
                               <input type="text" name="dateRange" class="form-control" id="dateRange">
                            </div>
                        </div>
                        <div class="col-md-3"> <br>
                            <button class="btn btn-flat btn-block btn-info" type="submit" id="btn-form-performance">
                               Filter
                            </button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom" id="box-graph">
                            <ul class="nav nav-tabs pull-right">
                                <li><a href="#tab_1" data-toggle="tab" data-type="daily">Daily</a></li>
                                <li class="active"><a href="#tab_2" data-toggle="tab" data-type="weekly">Weekly</a></li>
                                <li><a href="#tab_3" data-toggle="tab" data-type="monthly">Monthly</a></li>
                                <li class="pull-left header"><i class="fa fa-line-chart"> Locker Activity</i></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="tab_1">
                                    <div class="chart">
                                        <canvas id="performance-daily" style="height: 250px; width: 1072px;"></canvas>
                                    </div>
                                </div>
                                <div class="tab-pane active" id="tab_2">
                                    <div class="chart">
                                        <canvas id="performance-weekly" style="height: 250px; width: 1072px;"></canvas>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    <div class="chart">
                                        <canvas id="performance-monthly" style="height: 250px; width: 1072px;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>No</th>
                            <th>Locker Name</th>
                            <th>Place</th>
                            <th>Building Type</th>
                            <th>Delivery</th>
                            <th>Daily Usage</th>
                        </tr>
                        @foreach ($lockerActivitiesAll as $key => $item)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $item->locker_name }}</td>
                                <td>{{ $item->district }}, {{ $item->province }}</td>
                                <td>{{ $item->building_type }}</td>
                                <td>{{ $item->delivery }}</td>
                                <td>{{ round(($item->delivery / $numberOfDays),2) }}</td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $lockerActivitiesAll->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
     {{-- Morris.js charts --}}
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/morris.js/morris.min.js') }}"></script>
    {{-- ChartJS 2 --}}
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
        });
    </script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['lockerId']))
                $('select[name=lockerId]').val('{{ $parameter['lockerId'] }}').trigger('change');
            @endif
            @if (!empty($parameter['lockerType']))
                $('select[name=lockerType]').val('{{ $parameter['lockerType'] }}').trigger('change');
            @endif
        });
    </script>

    {{-- Chart --}}
    <script type="text/javascript">
        var dailyLabel = [];
        var dailyData = [];
        var daylySumData = [];

        var weeklyLabel = [];
        var weeklyData = [];
        var weeklySumData = [];

        var monthlyLabel = [];
        var monthlyData = [];
        var monthlySumData = [];

        var lockerPerformanceChart;

        /**
         * Create Daily 
         * @return {[type]} [description]
         */
        function createDaily(){
            $('#performance-daily').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Daily Chart*/
            var performanceChartCanvas = $("#performance-daily");
            var performanceChart = new Chart(performanceChartCanvas, {
                type : 'line',
                data : {
                    labels : dailyLabel,
                    datasets :[{
                        label: "Locker Activity Daily",
                        data: dailyData,
                        backgroundColor : 'rgba(216, 27, 96, 0.8)'
                    }]
                },
                steppedLine : false
            });
            performanceChart.clear();
        };

        /**
         * Create Weekly
         * @return {[type]} [description]
         */
        function createWeekly(){
            $('#performance-weekly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Weekly Chart*/
            var performanceChartCanvas = $("#performance-weekly");
            var performanceChart = new Chart(performanceChartCanvas, {
                type : 'line',
                data : {
                    labels : weeklyLabel,
                    datasets :[{
                        label: "Locker Activity Weekly",
                        data: weeklyData,
                        backgroundColor : 'rgba(216, 27, 96, 0.8)'
                    }]
                },
                option: {
                    steppedLine : false
                }
            });
            performanceChart.clear();
        };

        /**
         * Create Monthly
         * @return {[type]} [description]
         */
        function createMonthly(){
            $('#performance-monthly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Monthly Chart*/
            var performanceChartCanvas = $("#performance-monthly");
            var performanceChart = new Chart(performanceChartCanvas, {
                type : 'line',
                data : {
                    labels : monthlyLabel,
                    datasets :[{
                        label: "Locker Activity Monthly",
                        data: monthlyData,
                        backgroundColor : 'rgba(216, 27, 96, 0.8)'
                    }]
                }
            });
            performanceChart.clear();
        };


        function getGraph(){
            var formData = $('#form-performance').serialize();
            showLoading('#box-graph','graph-loading');
            showLoading('.box-parcel','box-loading');
            $.ajax({
                url: '{{ url('popbox/locker/ajax/lockerPerformance') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: formData,
                success: function(data){
                    console.log(data);
                    if (data.isSuccess==true) {
                        dailyLabel = data.dayLabels;
                        dailyData = data.dayCountPerformance;

                        weeklyLabel = data.weekLabels;
                        weeklyData = data.weekCountPerformance;

                        monthlyLabel = data.monthLabels;
                        monthlyData = data.monthCountPerformance;

                        createWeekly();

                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - getParcel");
            })
            .fail(function() {
                console.log("error - getParcel");
            })
            .always(function() {
                hideLoading('#box-graph','graph-loading');
                hideLoading('.box-parcel','box-loading');
                console.log("complete - getParcel");
                console.log('===================')
            });   
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            getGraph();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var type = $(this).data('type');
                if (type=='daily') {
                    createDaily();
                }
                if (type=='weekly') {
                    createWeekly();
                }
                if (type=='monthly') {
                    createMonthly();
                }
            });
            $('#type').on('change', function() {
                type = $(this).val();
                createWeekly();
            });
        });
    </script>
@endsection