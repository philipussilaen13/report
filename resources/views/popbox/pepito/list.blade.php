@extends('layout.main')

@section('title')
    Digital Transaction
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
    Digital Transaction
@endsection

@section('pageDesc')
    List of Digital Transaction
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Top Digital Product</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div id="inner-content-div">
                        <table class="table table-striped">
                            @foreach ($topProductDb as $item)
                                <tr>
                                    <td>{{ $item->label }} (PID: {{ $item->product_id }})</td>
                                    <td>{{ number_format($item->count_transaction) }}</td>
                                    <td>Rp {{ number_format($item->total_transaction) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-solid bg-teal-gradient">
                <div class="box-header">
                    <i class="fa fa-chart-line"></i>
                    <h3 class="box-title">Sales Graph</h3>
                </div>
                <div class="box-body border-radius-none">
                    <canvas class="chart" id="daily-graph" style="height: 250px;"></canvas>
                </div>
            </div>
        </div>
        {{-- Table Transaction --}}
        <div class="col-md-12">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Transaction</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    <form id="form-filter">
                    	{{ csrf_field() }}
                        <div class="row">
                        	<div class="col-md-2">
                            	<label>Transaction Date</label>
	                            <div class="input-group">
	                                <div class="input-group-addon">
	                                    <i class="fa fa-calendar"></i>
	                                </div>
	                                <input type="text" class="form-control" id="dateRange" name="dateRange" placeholder="Tanggal Transaksi">
	                            </div>
	                        </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Tipe</label>
                                    <select class="form-control select2" name="type">
                                        <option value="">Semua Tipe</option>
                                        @foreach ($productList as $element)
                                            @php
                                                $selected = '';
                                                if (!empty($param['type'])) {
                                                    if ($element == $param['type']) $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $element }}" {{ $selected }}>{{ $element }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Transaction Status</label>
                                    <select class="form-control select2" name="status">
                                        <option value="">Semua Status</option>
                                        @foreach ($transactionStatusList as $element)
                                            @php
                                                $selected = '';
                                                if (!empty($param['status'])) {
                                                    if ($element == $param['status']) $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $element }}" {{ $selected }}>{{ $element }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>PopBox Invoice ID</label>
                                    @if (!empty($param['invoice_id']))
                                        <input type="text" name="invoice_id" class="form-control" value="{{ $param['invoice_id'] }}">
                                    @else
                                        <input type="text" name="invoice_id" class="form-control">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Location</label>
                                    <select class="form-control select2" name="location_selected">
                                        <option value="">Semua Lokasi</option>
                                        @foreach ($location as $element)
                                            @php
                                                $selected = '';
                                                if (!empty($param['location_selected'])) {
                                                    if ($element['name'] == $param['location_selected']) $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $element['name'] }}" {{ $selected }}>{{ $element['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                            	<button class="btn btn-success btn-block btn-flat" id="btn-search" type="button">Cari</button>
                            	<button class="btn btn-info btn-block btn-flat" id="btn-export" type="button">Excel</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive no-padding table-bordered">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                            	<th>Invoice ID</th>
                            	<th>Customer Data</th>
                            	<th>Product Type</th>
                            	<th>Product Description</th>
                            	<th>Location</th>
                            	<th>Status</th>
                            	<th>PopBox Price</th>
                            	<th>Transaction Date</th>
                            	<th>Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($transactionDb as $key => $item)
                                <tr>
                                	<td>
                                		{{ $item->invoice_id }}
                                	</td>
                                	{{-- Customer Data --}}
                                	<td>
                                		{{ $item->cust_phone }} <br>
                                		{{ $item->cust_email }}
                                	</td>
                                	{{-- Product Type --}}
                                	<td>
                                		{{ $item->product_type }}
                                	</td>
                                	{{-- Product Description --}}
                                	<td>
                                		<strong>{{ $item->operator }}</strong> <br>
                                		{{ $item->product_description }}
                                	</td>
                                	{{-- Location --}}
                                	<td>
                                		{{ $item->location }}
                                	</td>
                                	{{-- Status --}}
                                	<td>
                                		@if ($item->status == 'success')
                                			<span class="label label-success">Success</span>
                                		@elseif ($item->status == 'failed')
                                			<span class="label label-danger">Failed</span>
                                		@endif <br>

                                		<strong>RC</strong>: <span class="label label-default">{{ $item->rc }}</span>
                                	</td>
                                	{{-- PopBox Price --}}
                                	<td>
                                		{{ number_format($item->popbox_price) }}
                                	</td>
                                	{{-- Transaction Date --}}
                                	<td>
                                		{{ $item->created_at }} / <strong>{{ $item->updated_at }}</strong>
                                	</td>
                                	{{-- Data --}}
                                	<td>
                                		@if (!empty($item->product_data))
                                			@php
                                				$dataJson = $item->product_data;
                                				$data = json_decode($item->product_data);
                                			@endphp
                                			@foreach ($data as $key => $element)
                            					{{ $key }} : {{ $element }} <br>
                            				@endforeach
                                		@endif
                                	</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $transactionDb->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    {{-- SlimScroll --}}
    <script type="text/javascript" src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    {{-- ChartJS 1.0.1 --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>
    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $param['beginDate'] }}';
            var endDate = '{{ $param['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
        	$('#btn-search').on('click', function(event) {
        		console.log('click');
        		$('#form-filter').attr('method', 'get');
        		$('#form-filter').submit();
        	});
        	$('#btn-export').on('click', function(event) {
        		$('#form-filter').attr('method', 'post');
        		$('#form-filter').submit();
        	});
            $('#inner-content-div').slimscroll({});
        });
    </script>
    {{-- Graph --}}
    <script type="text/javascript">
        var dailyLabel = {!! json_encode($dayLabel) !!}
        var dailyData = {!! json_encode($dayCountTransaction) !!}
        function createDaily(){
            /*Daily Chart*/
            var transactionChartCanvas = $("#daily-graph");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : dailyLabel,
                    datasets :[{
                        label: "Transaction",
                        data: dailyData,
                        borderColor: 'rgba(255, 255, 255, 1)'
                    }]
                },
                options: {
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            fontColor: '#fff'
                        }
                    }
                }
            });
        };
        jQuery(document).ready(function($) {
            createDaily();
        });
    </script>
@endsection