@extends('layout.main')

@section('title')
    Sepulsa Location
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('pageTitle')
    Sepulsa Location
@endsection

@section('pageDesc')
    Sepulsa Transaction Location
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Daftar Location</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    <form>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tipe</label>
                                    <select class="form-control select2" name="type">
                                        <option value="">Semua Tipe</option>
                                        @foreach ($typeList as $element)
                                            @php
                                                $selected = '';
                                                if (!empty($param['type'])) {
                                                    if ($element == $param['type']) $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $element }}" {{ $selected }}>{{ $element }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nama Agent/Locker</label>
                                    @if (!empty($param['name']))
                                        <input type="text" name="name" class="form-control"
                                               value="{{ $param['name'] }}">
                                    @else
                                        <input type="text" name="name" class="form-control">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2"> <br>
                                <button class="btn btn-success">Cari</button>
                                <a href="{{ url('popbox/sepulsa/updateLocation') }}">
                                    <button class="btn btn-warning" id="btn-all-update" type="button">Update Location</button>
                                </a>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive no-padding table-bordered">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Nearest Suggestion</th>
                                <th>Alias Locker</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($location as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $item->code }}</td>
                                    <td>
                                        @if ($item->type=='locker')
                                            <span class="label label-success">locker</span>
                                        @else
                                            <span class="label label-info">agent</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $item->name }}
                                        @if ($item->type == 'agent')
                                            <a href="{{ url('dashboard/agent/detail') }}/{{ $item->lockerId }}">{{ $item->lockerId }}</a>
                                        @endif
                                        <br> {{ $item->address }} <br>
                                        {{ $item->cityName }}
                                    </td>
                                    <td>{{ $item->nearestLocker }}</td>
                                    <td>{{ $item->alias_locker }}/{{ $item->alias_city }}</td>
                                    <td>
                                        <button class="btn btn-flat btn-info btn-update" data-id="{{ $item->id }}"
                                                data-code="{{ $item->code }}" data-type="{{ $item->type }}"
                                                data-name="{{ $item->name }}"
                                                data-alias_locker="{{ $item->alias_locker }}">Update
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $location->links() }}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="form-product">
                        {{ csrf_field() }}
                        <input type="hidden" name="form-id">
                        <div class="form-group">
                            <label>Code</label>
                            <input type="text" name="form-code" class="form-control" readonly="true">
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <select class="form-control" name="form-type">
                                @foreach ($typeList as $element)
                                    <option value="{{ $element }}">{{ $element }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="form-name" class="form-control" readonly="true">
                        </div>
                        <div class="form-group">
                            <label>Locker Alias</label>
                            <select class="form-control select2" style="width: 100%;" name="form-locker_alias">
                                <option value="">Empty</option>
                                @foreach ($lockerDb as $locker)
                                    <option value="{{ $locker->name }}">{{ $locker->name }}
                                        - {{ $locker->district }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.btn-update').on('click', function (event) {
                var data = $(this).data();
                console.log(data);
                $('input[name=form-id]').val(data.id);
                $('input[name=form-code]').val(data.code);
                $('input[name=form-name]').val(data.name);
                $('select[name=form-type]').val(data.type);
                $('input[name=form-alias_locker]').val(data.alias_locker);
                $('#myModal').modal('show');
                $('.select2').select2();
            });
            $('#btn-submit').on('click', function (event) {
                $('#form-product').submit();
            });
        });
    </script>
@endsection