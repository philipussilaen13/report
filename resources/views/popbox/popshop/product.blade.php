@extends('layout.main')

@section('title')
    PopShop Product
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('pageTitle')
    PopShop Product
@endsection

@section('pageDesc')
    List of PopShop Product Status and Price
@endsection

@section('content')
	<div class="row">
		<div class="col-md-6">
			 <div class="small-box bg-maroon">
                <div class="inner text-white">
                    <h3>{{ $productsDb->total() }}</h3>
                    <p>Total Product</p>
                </div>
                <div class="icon">
                    <i class="ion ion-close text-white"></i>
                </div>
            </div>
		</div>
		<div class="col-md-6">
			<div class="small-box bg-green">
                <div class="inner text-white">
                    <h3>{{ count($categoriesDb) }}</h3>
                    <p>Total Category</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark text-white"></i>
                </div>
            </div>
		</div>
	</div>
    <div class="row">
    	<div class="col-md-6">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Top Product List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form>
                    </form>
                    <div class="table-responsive no-padding">   
                        <table class="table table-striped table-condensed">
                        	<thead>
                        		<tr>
                        			<th>Product Name</th>
                        			<th>Total</th>
                        			<th>Amount</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		@foreach ($topProduct as $index => $item)
                        			<tr>
                        				<td>{{ $item->name }}</td>
                        				<td>{{ number_format($item->sum) }}</td>
                        				<td>{{ number_format($item->total_order) }}</td>
                        			</tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    	<div class="col-md-6">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Category List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form>
                    </form>
                    <div class="table-responsive no-padding">   
                        <table class="table table-striped table-condensed">
                        	<thead>
                        		<tr>
                        			<th>Category Name</th>
                        			<th>Product</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		@foreach ($categoriesDb as $index => $item)
                        			<tr>
                        				<td>{{ $item->category_name }}</td>
                        				<td>{{ count($item->products) }}</td>
                        			</tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" hidden="true">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Product List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form>
                    </form>
                    <div class="table-responsive no-padding">   
                        <table class="table table-striped table-condensed">
                        	<thead>
                        		<tr>
                        			<th>No</th>
                        			<th>SKU</th>
                        			<th>Name</th>
                        			<th>Categories</th>
                        			<th>Price</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		@foreach ($productsDb as $index => $item)
                        			<tr>
                        				<td>{{ $productsDb->firstItem() + $index }}</td>
                        				<td>{{ $item->sku }}</td>
                        				<td>{{ $item->name }}</td>
                        				<td>
                        					@php
                        						$categories = $item->categories;
                        					@endphp
                        					@foreach ($categories as $category)
                        						<span class="label label-info">{{ $category->category_name }}</span>
                        					@endforeach
                        				</td>
                        				<td>{{ number_format($item->price) }}</td>
                        			</tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                    {{ $productsDb->links() }} <br>
                    <strong>Total: </strong> {{ $productsDb->total() }}
                </div>
            </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-topall-type">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-1">
                            <h3>Filter</h3>
                        </div>
                    </div>
                </div>
                <div id="box-filter" class="box-body border-radius-none">
                    <div class="row col-md-12">
                        <div class="row">
                            <div class="col-md-4" style="padding-left: 15px">
                                <h5>SKU</h5>
                                <div class="row" style="margin-left: 1px">
                                    <div class="form-group">
                                        <input type="text" id="sku" name="sku" class="form-control" placeholder="SKU" style="width: 100%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 15px">
                                <h5>Product Name</h5>
                                <div class="row" style="margin-left: 1px">
                                    <div class="form-group">
                                        <input type="text" id="product-name" name="product-name" class="form-control" placeholder="Product Name" style="width: 100%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="margin-left: -10px; margin-right: 10px; margin-bottom: 45px">
                                <h5>Categories</h5>
                                <div class="row" style="margin-left: 1px">
                                    <div class="form-group">
                                        <select class="form-control select2" id="type-product" name="type-product" multiple="multiple" style="width: 100%">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: -30px; margin-bottom: 20px">
                                <button id="btn-filter-submit" type="button" class="btn btn-primary btn-sm" onclick="getProductList()" style="width: 100px">
                                    Filter
                                </button>
                                <button id="btn-filter-submit" type="button" class="btn btn-danger btn-sm" onclick="resetFilter()" style="width: 100px">
                                    Clear
                                </button>
                                <button id="btn-filter-submit" type="button" class="btn btn-primary btn-sm pull-right" onclick="getDownloandProductList()" style="width: 100px">
                                    Download
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div id="box-product-list" class="box-body">
                            <table id="table-products-list" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>SKU</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Photo</th>
                                    <th>Price</th>
                                    <th>Discount</th>
                                </tr>
                                </thead>
                                {{--<tbody id="tbody-table-agent"></tbody>--}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            getProductCategories();
        });

        function getProductCategories() {
            showLoadingg(true);

            $.ajax({
                url: '{{ url('popbox/popshop/ajax/getProductCategories') }}',
                type: 'GET',
                dataType: 'JSON',
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);
                        insertDataToDropDown(data.data.categoriesDb);
                        showDataToTable(data);

                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showLoadingg(false);
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showLoadingg(false);
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showLoadingg(false);
                })
                .always(function() {
                    showLoadingg(false);
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function getProductList() {
            // showLoadingg(true);
            showLoading('.table-products-list', 'table-products-list');

            var sku = $('#sku')[0].value;
            var name = $('#product-name')[0].value;
            var category_product = $('#type-product').val();

            $.ajax({
                url: '{{ url('popbox/popshop/ajax/getProductList') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    category: category_product,
                    sku: sku,
                    name: name
                },
                type: 'POST',
                dataType: 'JSON',
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);
                        showDataToTable(data);

                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showLoadingg(false);
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showLoadingg(false);
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showLoadingg(false);
                })
                .always(function() {
                    showLoadingg(false);
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function getDownloandProductList() {
            showLoadingg(true);

            var sku = $('#sku')[0].value;
            var name = $('#product-name')[0].value;
            var category_product = $('#type-product').val();

            $.ajax({
                url: '{{ url('popbox/popshop/ajax/getDownloadProductList') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    category: category_product,
                    sku: sku,
                    name: name
                },
                type: 'POST',
                dataType: 'JSON',
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);
                        window.location = '{{ url("ajax/download") }}' + '?file=' + data.data.link;

                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showLoadingg(false);
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showLoadingg(false);
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showLoadingg(false);
                })
                .always(function() {
                    showLoadingg(false);
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function showDataToTable(data) {
            if (data.data.productsDb.length > 0) {
                tableAllType = $('#table-products-list').DataTable({
                    data          : data.data.productsDb,
                    'destroy'     : true,
                    'retreive'    : true,
                    'paging'      : true,
                    'searching'   : true,
                    'ordering'    : true,
                    'autoWidth'   : true,
                    'responsive'  : true,
                    'processing'  : true,
                    'scrollY'     : 500,
                    'deferRender' : true,
                    'scroller'    : true,
                    "pageLength"  : 50,
                    'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    'order': [[ 2, "asc" ]],
                    columnDefs: [
                        {
                            "targets": 0,
                            "data": "download_link",
                            "render": function ( data, type, full, meta ) {
                                return full.sku;
                            }
                        },
                        {
                            "targets": 1,
                            "render": function ( data, type, full, meta ) {
                                return full.name;
                            }
                        },
                        {
                            "targets": 2,
                            "render": function ( data, type, full, meta ) {
                                return full.categories[0].category_name;
                            }
                        },
                        {
                            "targets": 3,
                            "data": "imageUrl",
                            "render": function ( data, type, full, meta ) {
                                var urlImg = "https://shop.popbox.asia/assets/images/uploaded/" + full.image;
                                return '<img height="50" width="50" src="'+urlImg+ '"/>';
                            }
                        },
                        {
                            "targets": 4,
                            "render": function ( data, type, full, meta ) {
                                return numberWithCommas(full.price);
                            }
                        },
                        {
                            "targets": 5,
                            "render": function ( data, type, full, meta ) {
                                return numberWithCommas(full.discount);
                            }
                        }
                    ]
                });

            } else {
                $('#table-products-list').DataTable().clear().draw();
            }
        }

        function resetFilter() {
            $('#sku').val('');
            $('#product-name').val('');
            insertDataToDropDown(dataDropDown);
        }

        var dataDropDown;
        function insertDataToDropDown(data) {
            dataDropDown = data;
            var categoryDataSelect2 = [];
            data.forEach(function(data){
                // console.log(data.type);
                categoryDataSelect2.push({
                    id: data.category_name,
                    text: data.category_name
                });
            });

            $('#type-product').html('').select2({
                data: categoryDataSelect2
            });
        }

        function showLoadingg(isShow) {
            if (isShow) {
                showLoading('.table-products-list', 'table-products-list');
            } else {
                // hideLoading('.table-products-list', 'table-products-list');
            }
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID' });
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

    </script>
@endsection