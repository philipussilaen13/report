@extends('layout.main-error')
@section('css')
@stop

@section('page-header')
    Not Found
@stop

@section('content')
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow">404</h2>
            <div class="error-content">
                <h3>
                    <i class="fa fa-warning text-yellow"></i>
                    Oops! Page Not Found
                </h3>
                <p>
                    Maaf, halaman yang Anda tuju tidak ditemukan.
                </p>
            </div>
        </div>
    </section>
@stop

@section('js')
@stop