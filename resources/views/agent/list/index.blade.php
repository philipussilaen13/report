@extends('layout.main')

@section('title')
    Agent List
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent
@endsection

@section('pageDesc')
    Agent List
@endsection

@section('content')
    <section class="content">
        {{-- Summary --}}
        <div class="row">
            {{-- Waiting --}}
            <div class="col-lg-6 col-xs-6">
                <div class="box box-solid bg-gray" id="collapseWaiting">
                    <div class="box-header text-black">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn bg-gray btn-sm pull-right" data-widget="collapse"
                                    data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <i class="fa fa-map-marker"></i>
                        <h3 class="box-title">Verifikasi</strong></h3>
                    </div>
                    <div class="box-body" style="padding: 0px;">
                        <div class="small-box bg-gray" style="margin-bottom: 0px;">
                            <div class="inner">
                                <h3 id="waitingCount">0</h3>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-pin"></i>
                            </div>
                            <a href="{{ url('agent/list') }}?status=disable" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- All --}}
            <div class="col-lg-6 col-xs-6">
                <div class="box box-solid bg-green" id="collapseAll">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn bg-green btn-sm pull-right" data-widget="collapse"
                                    data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <i class="fa fa-map-marker"></i>
                        <h3 class="box-title">Terdaftar</h3>
                    </div>
                    <div class="box-body" style="padding: 0px;">
                        <div class="small-box bg-green" style="margin-bottom: 0px;">
                            <div class="inner text-white">
                                <h3 id="allCount">0</h3>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-stalker text-white"></i>
                            </div>
                            <a href="{{ url('agent/list') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Graph --}}
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="box-graph">
                    <ul class="nav nav-tabs pull-right">
                        <li><a href="#tab_1" data-toggle="tab" data-type="daily">Daily</a></li>
                        <li class="active"><a href="#tab_2" data-toggle="tab" data-type="weekly">Weekly</a></li>
                        <li><a href="#tab_3" data-toggle="tab" data-type="monthly">Monthly</a></li>
                        <li class="pull-left header"><i class="fa fa-th"> Success Transaction</i></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab_1">
                            <div class="chart">
                                <canvas id="transaction-daily" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                        <div class="tab-pane active" id="tab_2">
                            <div class="chart">
                                <canvas id="transaction-weekly" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="chart">
                                <canvas id="transaction-monthly" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Table --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-info">
                    <div class="box-header with-border">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">Daftar Agen</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i
                                        class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="row">
                            <form id="form-agent">
                                {{ csrf_field() }}
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="dateRange" name="dateRange" placeholder="Tanggal Transaksi">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="typeee">
                                            <option value="">Tipe</option>
                                            <option value="agent">Agent</option>
                                            <option value="warung">Warung</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="province">
                                            <option value="">Semua Provinsi</option>
                                            @foreach ($provinces as $element)
                                                @php
                                                    $selected = '';
                                                    if (isset($parameter['province'])) {
                                                        if ($parameter['province'] == $element['id']) {
                                                            $selected = "selected='selected'";
                                                        }
                                                    }
                                                @endphp
                                                <option value="{{ $element['id'] }}" {{ $selected }}>{{ $element['province_name'] }}
                                                    ({{ $element['province_code'] }})
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="city">
                                            <option value="">Semua Kota</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="status">
                                            <option value="">Semua Status</option>
                                            <option value="disable">Disabled</option>
                                            <option value="enable">Enable</option>
                                            <option value="verified">Verified</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        @php
                                            $value = '';
                                            if (!empty($parameter['lockerName'])) {
                                                $value = $parameter['lockerName'];
                                            }
                                        @endphp
                                        <input type="text" name="lockerName" class="form-control" placeholder="Nama Agen" value="{{ $value }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        @php
                                            $value = '';
                                            if (!empty($parameter['lockerId'])) {
                                                $value = $parameter['lockerId'];
                                            }
                                        @endphp
                                        <input type="text" name="lockerId" class="form-control" placeholder="Agen ID">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2" name="registeredBy">
                                            <option value="">Semua Sales</option>
                                            @foreach ($registeredBy as $element)
                                                <option value="{{ $element->id }}">{{ $element->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-1">
                                    <button class="btn btn-info btn-flat btn-block" type="button" id="btn-search">Cari</button>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-success btn-flat btn-block" type="button" id="btn-export">Excel</button>
                                </div>
                                @if(\App\Models\Privilege::hasAccess('agent/disable'))
                                    <div class="col-md-1">
                                        <button class="btn btn-danger btn-block btn-flat" type="button" id="btn-status">Change Agent</button>
                                    </div>
                                @endif
                            </form>
                        </div>
                        <div class="table-responsive">
                             @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                             @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <form id="form-status" action="{{ url('agent/changeStatus') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="status">
                                <table class="table table-hover" id="transaction-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                                <input type="checkbox" id="check-all">
                                            </th>
                                            <th>Agen ID</th>
                                            <th>Nama Agen</th>
                                            <th>Tipe</th>
                                            <th>Deposit</th>
                                            <th>Alamat</th>
                                            <th>Kota</th>
                                            <th>Jam Operasional</th>
                                            <th>Status</th>
                                            <th>Registered By</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($lockerDb as $key => $element)
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="checkbox-agent" name="lockerId[]" value="{{ $element->locker_id }}" data-label="{{ $element->locker_name }}">
                                                            <strong>{{ $lockerDb->firstItem() + $key }}</strong>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if ($element->type=='agent')
                                                        <a href="{{ url('agent/list/detail') }}/{{ $element->locker_id }}">{{ $element->locker_id }}</a>
                                                    @elseif ($element->type == 'locker')
                                                        {{ $element->locker_id }}
                                                    @elseif ($element->type == 'warung')
                                                        <a href="{{ url('agent/list/detail') }}/{{ $element->locker_id }}">{{ $element->locker_id }}</a>
                                                        @else <a href="{{ url('agent/list/detail') }}/{{ $element->locker_id }}">{{ $element->locker_id }}</a>
                                                    @endif
                                                </td>
                                                <td>{{ $element->locker_name }}</td>
                                                <td>
                                                    @if ($element->type =='agent')
                                                        <span class="label label-info">Agent</span>
                                                    @elseif ($element->type == 'locker')
                                                        <span class="label label-default">Locker</span>
                                                    @elseif ($element->type == 'warung')
                                                        <span class="label label-default">Warung</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($element->type=='agent')
                                                        @php
                                                            $agentLockerDb =\App\Models\Agent\AgentLocker::where('id',$element->locker_id)->first();
                                                        @endphp
                                                        @if($agentLockerDb)
                                                            {{ number_format($agentLockerDb->balance) }}
                                                        @endif
                                                    @endif

                                                </td>
                                                <td>{{ \App\Http\Helpers\Helper::trimText($element->address,50) }}</td>
                                                <td>{{ $element->city->city_name }}</td>
                                                <td>
                                                    @php
                                                        $days =['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
                                                    @endphp
                                                    @foreach ($element->operational_time as $time)
                                                        @php
                                                            $style = '';
                                                            $class = 'bg-gray';
                                                            if(date('w') == $time->day){
                                                                $style = 'font-weight:bold;';
                                                                $class = 'label-success';
                                                            }
                                                            if (empty($time->open_hour) && empty($time->close_hour)) {
                                                                $class = 'label-danger';
                                                            }
                                                        @endphp
                                                        @if (date('w') == $time->day)
                                                            <span class="label {{ $class }}" style="{{ $style }}">
                                                                    {{ $days[$time->day] }}
                                                                @if (!empty($time->open_hour) && !empty($time->close_hour))
                                                                    {{ sprintf("%02d", $time->open_hour) }}.00 -
                                                                    {{ sprintf("%02d", $time->close_hour) }}.00
                                                                @else
                                                                    Tutup
                                                                @endif
                                                                </span>
                                                        @endif

                                                    @endforeach
                                                </td>
                                                <td>
                                                    @if($agentLockerDb)
                                                        @if (empty($agentLockerDb->status_verification) || $agentLockerDb->status_verification == 'enable')
                                                            <span class="label bg-gray">Enabled</span>
                                                        @elseif ($agentLockerDb->status_verification == 'disable')
                                                            <span class="label bg-red">Disabled</span>
                                                        @elseif ($agentLockerDb->status_verification == 'verified')
                                                            <span class="label bg-green">Verified</span>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (empty($element->registeredBy))
                                                        <i>Empty/Not Set</i>
                                                    @else
                                                        {{ $element->registeredBy->name }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        {{ $lockerDb->links() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" id="tmp-status" name="tmp-status">
                                <option value="DISABLE">DISABLE</option>
                                <option value="ENABLE">ENABLE</option>
                                <option value="VERIFIED">VERIFIED</option>
                            </select>
                            <div id="list-agent">
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-change-submit">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    {{-- Morris.js charts --}}
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/morris.js/morris.min.js') }}"></script>
    {{-- ChartJS 1.0.1 --}}
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['status']))
                $('select[name="status"]').val({!! json_encode($parameter['status']) !!}).trigger('change');
            @endif
            @if (!empty($parameter['registeredBy']))
                $('select[name="registeredBy"]').val({!! json_encode($parameter['registeredBy']) !!}).trigger('change');
            @endif
        });
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ empty($parameter['beginDate'])? date('Y-m-1') : date('Y-m-d',strtotime($parameter['beginDate'])) }}';
            var endDate = '{{ empty($parameter['endDate'])? date('Y-m-t') : date('Y-m-d',strtotime($parameter['endDate'])) }};'
            $('#dateRange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
            });
            $('#dateRange').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
          });
            $('#dateRange').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>

    {{-- Chart --}}
    <script type="text/javascript">
        var dailyLabel = [];
        var dailyData = [];
        var weeklyLabel = [];
        var weeklyData = [];
        var monthlyLabel = [];
        var monthlyData = [];


        // Option Chart
        var chartOption = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 10,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: false,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            scaleLabel:
                function(label){return  label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");},
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                },
            }
        }
        function createDaily(){
            $('#transaction-daily').css({
                "height": '180px',
                "width": '1072px'
            });

            /*Daily Chart*/
            var transactionChartCanvas = $("#transaction-daily").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var transactionChart = new Chart(transactionChartCanvas,{
                type : 'line',
                data : {
                    labels : dailyLabel,
                    datasets :[{
                        label: "Agent Register Activity",
                        data: dailyData,
                        backgroundColor : 'rgba(216, 27, 96, 0.8)'
                    }]
                },
                steppedLine : false
            });
        };
        function createWeekly(){
            $('#transaction-weekly').css({
                "height": '180px',
                "width": '1072px'
            });

            /*Weekly Chart*/
            var transactionChartCanvas = $("#transaction-weekly").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var transactionChart = new Chart(transactionChartCanvas,{
                type : 'line',
                data : {
                    labels : weeklyLabel,
                    datasets :[{
                        label: "Agent Register Activity",
                        data: weeklyData,
                        backgroundColor : 'rgba(216, 27, 96, 0.8)'
                    }]
                },
                steppedLine : false
            });
        }
        function createMonthly(){
            $('#transaction-monthly').css({
                "height": '180px',
                "width": '1072px'
            });

            /*Monthly Chart*/
            var transactionChartCanvas = $("#transaction-monthly").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var transactionChart = new Chart(transactionChartCanvas,{
                type : 'line',
                data : {
                    labels : monthlyLabel,
                    datasets :[{
                        label: "Agent Register Activity",
                        data: monthlyData,
                        backgroundColor : 'rgba(216, 27, 96, 0.8)'
                    }]
                },
                steppedLine : false
            });
        };

        $(document).ready(function() {
            createWeekly();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var type = $(this).data('type');
                if (type=='daily') {
                    createDaily();
                }
                if (type=='weekly') {
                    createWeekly();
                }
                if (type=='monthly') {
                    createMonthly();
                }
            });
        });
    </script>

    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function getGraph(){
            var formData = $('#form-transaction').serialize();
            console.log(formData);
            showLoading('#box-graph','graph-loading');
            $.ajax({
                url: '{{ url('agent/list/getAjaxGraph') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: formData,
                success: function(data){
                    if (data.isSuccess==true) {
                       dailyLabel = data.dayLabels;
                        dailyData = data.dayData;

                        weeklyLabel = data.weekLabels;
                        weeklyData = data.weekData;

                        monthlyLabel = data.monthLabels;
                        monthlyData = data.monthData;

                        createWeekly();
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - get Graph");
            })
            .fail(function() {
                console.log("error - get Graph");
            })
            .always(function() {
                hideLoading('#box-graph','graph-loading');
                console.log("complete - get Graph");
                console.log('===================')
            });
        }

        function getSummary(){
            showLoading('#box-graph','graph-loading');
            $.ajax({
                url: '{{ url('agent/list/getAjaxSummary') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: [],
                success: function(data){
                    if (data.isSuccess==true) {
                        $('#allCount').html(data.registeredCounter);
                        $('#waitingCount').html(data.waitingCounter);
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - get Summary");
            })
            .fail(function() {
                console.log("error - get Summary");
            })
            .always(function() {
                hideLoading('#box-graph','graph-loading');
                console.log("complete - get Summary");
                console.log('===================')
            });
        }

        getSummary();

        jQuery(document).ready(function($) {
            getGraph();

            // on search
            $('#btn-search').on('click', function(event) {
                $('#form-agent').attr('method', 'get');
                $('#form-agent').submit();
            });
            // on excel export
            $('#btn-export').on('click', function(event) {
                $('#form-agent').attr('action', '{{ url('agent/list/export') }}');
                $('#form-agent').attr('method', 'post');
                $('#form-agent').submit();
            });
        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var citiesList = {!! json_encode($cities) !!};
            /*Province On Change*/
            $("select[name=province]").on('change', function(event) {
                // get selected value
                var value = $(this).val();
                if (value == '') {
                     var option='<option value="">Semua Kota</option>';
                }
                var selected = null;
                @if (isset($parameter['city']))
                    selected = {{ $parameter['city'] }};
                @endif
                // search on cities List
                $.each(citiesList, function(index, val) {
                    if (value==val.provinces_id) {
                        option += "<option value='"+val.id+"'>"+val.city_name+"</option>";
                       
                    }
                });
                $('select[name=city]').html(option);
                if (selected!= null && selected!= 'all') {
                    $('select[name=city]').val(selected).trigger('change');
                }
            });
            $('select[name=province]').trigger('change');

            $('#check-all').on('change', function(event) {
               var checked = $(this).is(':checked');
               if (checked == true) {
                    $('input[type=checkbox][class=checkbox-agent]').prop('checked', true);
               } else {
                    $('input[type=checkbox][class=checkbox-agent]').prop('checked', false);
               }
            });

            $('#btn-status').on('click', function(event) {
                $('#modalStatus').modal('show');
                var list = '<hr>';
                $('input[type="checkbox"][class=checkbox-agent]:checked').each(function() {
                   var value = this.value;
                   var label = $(this).data('label');

                   list += "Agent ID : "+value+"<br>";
                   list += "Name : "+label+"<hr>";
                });
                $('#list-agent').html(list);
            });
            $('#btn-change-submit').on('click', function() {
                var value = $('#tmp-status').val();
                $('input[type=hidden][name=status]').val(value);
                $('#form-status').submit();
            });
        });
    </script>
@endsection