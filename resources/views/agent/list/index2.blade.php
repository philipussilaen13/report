@extends('layout.main')

@section('title')
    Agent List
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('pageTitle')
    Dashboard Agent
@endsection

@section('pageDesc')
    Agent List
@endsection

@section('content')
    {{-- DISABLE --}}
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <a href="javascript:void(0);" onclick="widgetClick(1)">
                    <div id="box-info-disable" class="info-box bg-red-gradient" >
                        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Disable</span>
                            <span id="total-disable" class="info-box-number">0</span>
                            <div class="progress">
                                <div id="total-disable-progressbar" class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span id="total-disable-percentage" class="progress-description"></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>
            {{-- ENABLE --}}
            <div class="col-md-3">
                <a href="javascript:void(0);" onclick="widgetClick(2)">
                    <div id="box-info-enable" class="info-box bg-light-blue-gradient">
                        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Enable</span>
                            <span id="total-enable" class="info-box-number">0</span>
                            <div class="progress">
                                <div id="total-enable-progressbar" class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span id="total-enable-percentage" class="progress-description"></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>
            {{-- PENDING --}}
            <div class="col-md-3">
                <a href="javascript:void(0);" onclick="widgetClick(3)">
                    <div id="box-info-pending" class="info-box bg-yellow-gradient">
                        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Pending</span>
                            <span id="total-pending" class="info-box-number">0</span>
                            <div class="progress">
                                <div id="total-pending-progressbar" class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span id="total-pending-percentage" class="progress-description"></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>
            {{-- VERIFIED --}}
            <div class="col-md-3">
                <a href="javascript:void(0);" onclick="widgetClick(4)">
                    <div id="box-info-verified" class="info-box bg-green-gradient">
                        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Verified</span>
                            <span id="total-verified" class="info-box-number">0</span>
                            <div class="progress">
                                <div id="total-verified-progressbar" class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span id="total-verified-percentage" class="progress-description"></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-topall-type">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-1">
                            <h3>Filter</h3>
                        </div>
                        <div class="col-md-9">
                            <h3 id="title-filter"></h3>
                        </div>
                        <div class="col-md-2">
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" onclick="showHideBox()" style="margin-top: 20px">
                                Show Filter
                            </button>
                        </div>
                    </div>
                </div>
                <div id="box-filter">
                    {{-- FIRST ROW --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Type</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="type" name="type" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Status</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="status" name="status" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Sales</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="sales" name="sales" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- SECOND ROW --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Province</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="province" name="province" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>City</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="city" name="city" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- TYPE ROW --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-6" style="padding-left: 15px">
                            {{--<h5>Name/Owner Name/Phone</h5>--}}
                            <h5>Store Name</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Name" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Agent ID</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" id="agentid" name="agentid" class="form-control" placeholder="Agent ID" style="width: 100%">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Transaction Date</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="daterange-transactions" name="dateRange-transactions" placeholder="Transaction Date">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Registered Date</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="daterange-registered" name="dateRange-registered" placeholder="Registered Date">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer" style="margin-left: 15px; margin-right: 15px;">
                        <div class="form-group">
                            <button id="btn-filter-submit" type="button" class="btn btn-primary btn-sm" onclick="submitFilter()" style="margin-top: 15px; margin-left: 10px">
                                Filter
                            </button>
                            <button id="btn-reset-filter" type="button" class="btn btn-danger btn-sm" onclick="resetFilter()" style="margin-top: 15px">
                                Reset
                            </button>
                        </div>
                    </div>
                </div>
                {{--<div id="box-filterr" class="row">`--}}
                {{----}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    {{-- Table List --}}
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-topall-type">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-6">
                            <h3>User List</h3>
                        </div>
                        <div class="col-md-6">
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" onclick="getAjaxUserDataDownload()" style="margin-top: 20px;">
                                Download
                            </button>

                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right bg-green" onclick="getAjaxAllUserDataDownload()" style="margin-top: 20px; margin-right: 20px">
                                Download All User
                            </button>
                        </div>
                    </div>
                    <div id="top-all-type" class="box-body">
                        <table id="table-all-type" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Agent ID</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Deposit</th>
                                <th>City</th>
                                <th>Status</th>
                                <th>Registered By</th>
                                <th>Registered Date</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script type="text/javascript">

        var dataCity = [];
        var dataCityFilterByProvince = [];
        var dataDropDown;

        var transactionDateRange = '';
        var registeredDateRange = '';

        var isFirstOpeningPage = 'true';

        var dataFilterToDownload = [];

        var tableAllType;

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "currency-cust-pre": function (a) {
                /* Remove Rp text */
                // var x = String(a).replace("Rp", "");

                /* Remove comma and ,00 */
                // x = x.replace(",00", "");

                var x = OSREC.CurrencyFormatter.parse(String(a), { locale: 'id_ID' });

                x = replaceAll(String(x), ".", "");

                // console.log('currency --> ' + x);

                /* Parse and return */
                return parseFloat(x);
            },

            "currency-cust-asc": function (a, b) {
                return a - b;
            },

            "currency-cust-desc": function (a, b) {
                return b - a;
            }
        });

        jQuery(document).ready(function ($) {
            $('#box-filter').hide(10);
            $('#box-filterr').hide(10);
            $('.select2').select2();

            getAjaxTypeData();
            getAjaxStatusVerificationData();

            $('#province').on('select2:select', function (e) {
                var data = e.params.data;
                updateCityByProvince(data.id);
            });

            dateRangeTransactions();
            dateRangeRegistered();

            $('#btn-reset-filter').on('click', function(event) {

            });
        });

        function submitFilter() {

            isFirstOpeningPage = 'false';
            getDefaultData(getDataForFilter()[0]);
            // tableData();
        }

        function resetFilter() {

            insertDataToDropdownSelect2(dataDropDown);
            $('#name').val('');
            $('#agentid').val('');

            $('#daterange-transactions').val('');
            $('#daterange-registered').val('');

            transactionDateRange = '';
            registeredDateRange = '';

            // console.log(getDataForFilter()[0]);

            resetFilterTitle();
        }

        function widgetClick(value) {

            if (value === 1) {
                $('#status').val('disable').trigger('change');
            } else if (value === 2) {
                $('#status').val('enable').trigger('change');
            } else if (value === 3) {
                $('#status').val('pending').trigger('change');
            } else if (value === 4) {
                $('#status').val('verified').trigger('change');
            }

            submitFilter()

        }

        function getDataForFilter() {

            // console.log($('#type').select2("data")[0].id);
            // console.log($('#status').select2("data")[0].id);
            // console.log($('#sales').select2("data")[0].id);
            // console.log($('#province').select2("data")[0].id);
            // console.log($('#city').select2("data")[0].id);
            // console.log($('#name')[0].value);
            // console.log($('#agentid')[0].value);

            var type = $('#type').select2("data")[0].id;
            var status = $('#status').select2("data")[0].id;
            var sales = $('#sales').select2("data")[0].id;
            var province = $('#province').select2("data")[0].id;
            var city = $('#city').select2("data")[0].id;
            var name = $('#name')[0].value;
            var agentid = $('#agentid')[0].value;

            var result = [];
            result.push({
                token: '{{ csrf_token() }}',
                type : type,
                status : status,
                sales : sales,
                province : province,
                city : city,
                name : name,
                agentid : agentid,
                transactionDateRangee : transactionDateRange,
                registeredDateRangee : registeredDateRange,
                firstOpeningPage : isFirstOpeningPage
            });

            return result;
        }

        function getDefaultData(value) {

            showLoading('.box-topall-type', 'box-topall-type');

            dataFilterToDownload = [];

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxAllType') }}',
                data: {
                    _token : value.token,
                    type : value.type,
                    status : value.status,
                    sales : value.sales,
                    province : value.province,
                    city : value.city,
                    name : value.name,
                    agentid : value.agentid,
                    daterange_transaction : value.transactionDateRangee,
                    daterange_registered : value.registeredDateRangee,
                    firstOpeningPage : value.firstOpeningPage
                },
                type: 'POST',
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);
                        if (data.data.resultList.length > 0) {
                            tableAllType = $('#table-all-type').DataTable({
                                data          : data.data.resultList,
                                'destroy'     : true,
                                'retreive'    : true,
                                'paging'      : true,
                                'searching'   : true,
                                'ordering'    : true,
                                'autoWidth'   : false,
                                'responsive'  : true,
                                'processing'  : true,
                                'scrollY'     : 500,
                                'deferRender' : true,
                                'scroller'    : true,
                                "pageLength"  : 50,
                                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                'order': [[ 1, "asc" ]],
                                'columnDefs': [
                                    { type: 'currency-cust', targets: 3 },
                                    {
                                        "targets": 0,
                                        "data": "download_link",
                                        "render": function ( data, type, full, meta ) {
                                            if(full.type !== 'locker') {
                                                return '<a href=" {{ url('agent/list/detail') }}/'+full.id+'"> '+full.id+' </a>';
                                            }

                                            return full.id;
                                            //     return '<a href="javascript:void(0);" onclick="openInNewTab(+full[0]+)"> '+full[0]+' </a>';
                                            // return openInNewTab(full[0]);
                                        }
                                    },
                                    {
                                        "targets": 1,
                                        "render": function ( data, type, full, meta ) {
                                            return full.name;
                                        }
                                    },
                                    {
                                        "targets": 2,
                                        "render": function ( data, type, full, meta ) {
                                            if (full.type === 'agent') {
                                                return '<span class="label span_type_agent">Agent</span>'
                                            } else if (full.type === 'warung') {
                                                return '<span class="label span_type_warung">Warung</span>'
                                            } else {
                                                return '<span class="label span_type_locker">Locker</span>'
                                            }
                                        }
                                    },
                                    {
                                        "targets": 3,
                                        "render": function ( data, type, full, meta ) {
                                            return numberWithCommas(full.deposit);
                                        }
                                    },
                                    {
                                        "targets": 4,
                                        "render": function ( data, type, full, meta ) {
                                            return full.city;
                                        }
                                    },
                                    {
                                        "targets": 5,
                                        "render": function ( data, type, full, meta ) {
                                            if (full.status === 'disable') {
                                                return '<span class="label span_status_disable">'+full.status+'</span>'
                                            } else if (full.status === 'enable') {
                                                return '<span class="label span_status_enable">'+full.status+'</span>'
                                            } else if (full.status === 'pending') {
                                                return '<span class="label span_status_pending">'+full.status+'</span>'
                                            } else if (full.status === 'verified') {
                                                return '<span class="label span_status_verified">'+full.status+'</span>'
                                            } else {
                                                return full.status
                                            }
                                        }
                                    },
                                    {
                                        "targets": 6,
                                        "render": function ( data, type, full, meta ) {
                                            return full.registeredby;
                                        }
                                    },
                                    {
                                        "targets": 7,
                                        "render": function ( data, type, full, meta ) {
                                            return full.registeredDate;
                                        }
                                    }
                                ]
                            });

                        } else {
                            $('#table-all-type').DataTable().clear().draw(false);
                        }

                    }  else {
                        alert(data.errorMsg);
                    }

                    filterTitle(value);
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        function getAjaxTypeData() {
            showLoading('.box-filter', 'box-filter');
            $.ajax({
                url: '{{ url('agent/ajax/getAjaxFilterDropdownDataUser') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        insertDataToDropdownSelect2(data.data);
                    }  else {
                        alert(data.errorMsg);
                    }

                    getDefaultData(getDataForFilter()[0]);
                    // tableData();

                    var space = '&nbsp&nbsp&nbsp&nbsp';
                    var textTitle = '<span class="label span_filter" style="font-size: 14px">Last 25 New Register</span>';
                    $('#title-filter').html(textTitle);
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        function getAjaxStatusVerificationData() {

            showLoading('.box-info-disable', 'box-info-disable');
            showLoading('.box-info-enable', 'box-info-enable');
            showLoading('.box-info-pending', 'box-info-pending');
            showLoading('.box-info-verified', 'box-info-verified');

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxDataStatusVerification') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        var text = '% of Total';

                        $('#total-disable').html(data.data.total_disable);
                        $('#total-disable-percentage').html(data.data.total_disable_percentage + text);
                        $('#total-disable-progressbar').css('width', data.data.total_disable_percentage.toString().concat("%"));

                        $('#total-enable').html(data.data.total_enable);
                        $('#total-enable-percentage').html(data.data.total_enable_percentage + text);
                        $('#total-enable-progressbar').css('width', data.data.total_enable_percentage.toString().concat("%"));

                        $('#total-pending').html(data.data.total_pending);
                        $('#total-pending-percentage').html(data.data.total_pending_percentage + text);
                        $('#total-pending-progressbar').css('width', data.data.total_pending_percentage.toString().concat("%"));

                        $('#total-verified').html(data.data.total_verified);
                        $('#total-verified-percentage').html(data.data.total_verified_percentage + text);
                        $('#total-verified-progressbar').css('width', data.data.total_verified_percentage.toString().concat("%"));


                    }  else {
                        alert(data.errorMsg);
                    }

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-info-disable', 'box-info-disable');
                    hideLoading('.box-info-enable', 'box-info-enable');
                    hideLoading('.box-info-pending', 'box-info-pending');
                    hideLoading('.box-info-verified', 'box-info-verified');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-info-disable', 'box-info-disable');
                    hideLoading('.box-info-enable', 'box-info-enable');
                    hideLoading('.box-info-pending', 'box-info-pending');
                    hideLoading('.box-info-verified', 'box-info-verified');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-info-disable', 'box-info-disable');
                    hideLoading('.box-info-enable', 'box-info-enable');
                    hideLoading('.box-info-pending', 'box-info-pending');
                    hideLoading('.box-info-verified', 'box-info-verified');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-info-disable', 'box-info-disable');
                    hideLoading('.box-info-enable', 'box-info-enable');
                    hideLoading('.box-info-pending', 'box-info-pending');
                    hideLoading('.box-info-verified', 'box-info-verified');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function getAjaxUserDataDownload() {
            var value = getDataForFilter()[0];

            showLoading('.box-topall-type', 'box-topall-type');

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxUserDataDownload') }}',
                data: {
                    _token : value.token,
                    type : value.type,
                    status : value.status,
                    sales : value.sales,
                    province : value.province,
                    city : value.city,
                    name : value.name,
                    agentid : value.agentid,
                    daterange_transaction : value.transactionDateRangee,
                    daterange_registered : value.registeredDateRangee,
                    firstOpeningPage : value.firstOpeningPage
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        window.location = '{{ url("ajax/download") }}' + '?file=' + data.data.link;

                    }  else {
                        alert(data.errorMsg);
                    }

                    hideLoading('.box-topall-type', 'box-topall-type');
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                }
            })
                .done(function() {
                    console.log("success - download");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .fail(function() {
                    console.log("error - download");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                    console.log("complete - download");
                    console.log('===================');
                });

        }
        
        function getAjaxAllUserDataDownload() {
            var value = getDataForFilter()[0];

            showLoading('.box-topall-type', 'box-topall-type');
                
            $.ajax({
            	url: '{{ route('all-user-download-excel') }}',
                data: {
                    _token : '{{ csrf_token() }}',
                },
                type: 'get',
                responseType: 'blob', // important
                async : true,
                success: function (response, textStatus, request) {

                	hideLoading('.box-topall-type', 'box-topall-type');
                    
                    var a = document.createElement("a");
                    a.href = response.file; 
                    a.download = response.name;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                },
                error: function (ajaxContext) {

                	hideLoading('.box-topall-type', 'box-topall-type');
                    alert('Export error: '+ajaxContext.responseText);
                }
            });
        }

        function tableData() {

            var value = getDataForFilter()[0];

            // "data": function (d) {
            //     // console.log(d);
            //     d._token = value.token;
            //     d.type = value.type;
            //     d.status = value.status;
            //     d.sales = value.sales;
            //     d.province = value.province;
            //     d.city = value.city;
            //     d.name = value.name;
            //     d.agentid = value.agentid;
            //     d.daterange_transaction = value.transactionDateRangee;
            //     d.daterange_registered = value.registeredDateRangee;
            //     d.firstOpeningPage = value.firstOpeningPage;
            // }

            tableAllType = $('#table-all-type').DataTable({
                'destroy'     : true,
                'retreive'    : true,
                'paging'      : true,
                'searching'   : true,
                'ordering'    : true,
                'autoWidth'   : false,
                'responsive'  : true,
                'processing'  : true,
                "serverSide"  : true,
                'scrollY'     : 500,
                'deferRender' : true,
                'scroller'    : true,
                "pageLength"  : 50,
                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                'order': [[ 1, "asc" ]],
                "ajax": {
                    url: '{{ url('agent/ajax/getAjaxAllType') }}',
                    type: 'POST',
                    dataSrc: 'data.resultList',
                    data: {
                        _token : value.token,
                        type : value.type,
                        status : value.status,
                        sales : value.sales,
                        province : value.province,
                        city : value.city,
                        name : value.name,
                        agentid : value.agentid,
                        daterange_transaction : value.transactionDateRangee,
                        daterange_registered : value.registeredDateRangee,
                        firstOpeningPage : value.firstOpeningPage
                    }
                },
                'columnDefs': [
                    { type: 'currency-cust', targets: 3 },
                    {
                        "targets": 0,
                        "data": "download_link",
                        "render": function ( data, type, full, meta ) {
                            return '<a href=" {{ url('agent/list/detail') }}/'+full.id+'"> '+full.id+' </a>';
                            //     return '<a href="javascript:void(0);" onclick="openInNewTab(+full[0]+)"> '+full[0]+' </a>';
                            // return openInNewTab(full[0]);
                        }
                    },
                    {
                        "targets": 1,
                        "render": function ( data, type, full, meta ) {
                            return full.name;
                        }
                    },
                    {
                        "targets": 2,
                        "render": function ( data, type, full, meta ) {
                            if (full.type === 'agent') {
                                return '<span class="label span_type_agent">Agent</span>'
                            } else if (full.type === 'warung') {
                                return '<span class="label span_type_warung">Warung</span>'
                            } else {
                                return ''
                            }
                        }
                    },
                    {
                        "targets": 3,
                        "render": function ( data, type, full, meta ) {
                            return numberWithCommas(full.deposit);
                        }
                    },
                    {
                        "targets": 4,
                        "render": function ( data, type, full, meta ) {
                            return full.city;
                        }
                    },
                    {
                        "targets": 5,
                        "render": function ( data, type, full, meta ) {
                            if (full.status === 'disable') {
                                return '<span class="label span_status_disable">'+full.status+'</span>'
                            } else if (full.status === 'enable') {
                                return '<span class="label span_status_enable">'+full.status+'</span>'
                            } else if (full.status === 'pending') {
                                return '<span class="label span_status_pending">'+full.status+'</span>'
                            } else if (full.status === 'verified') {
                                return '<span class="label span_status_verified">'+full.status+'</span>'
                            } else {
                                return full.status
                            }
                        }
                    },
                    {
                        "targets": 6,
                        "render": function ( data, type, full, meta ) {
                            return full.registeredby;
                        }
                    },
                    {
                        "targets": 7,
                        "render": function ( data, type, full, meta ) {
                            return full.registeredDate;
                        }
                    }
                ]
            });
        }

        function filterTitle(value) {
            if (value.firstOpeningPage === 'false') {


                var type = $('#type').select2("data")[0].text;
                var status = $('#status').select2("data")[0].text;
                var sales = $('#sales').select2("data")[0].text;
                var province = $('#province').select2("data")[0].text;
                var city = $('#city').select2("data")[0].text;
                var name = $('#name')[0].value;
                var agentid = $('#agentid')[0].value;

                var space = '&nbsp&nbsp&nbsp&nbsp';
                var defaultTitle = '<span class="label span_filter" style="font-size: 14px">Type: ' +type+ '</span>' +
                    space + '<span class="label span_filter" style="font-size: 14px">Status: ' +status+ '</span>' +
                    space + '<span class="label span_filter" style="font-size: 14px">Sales: ' +sales+ '</span>';

                if (province != 'Province') {
                    var provinceTitle = space +'<span class="label span_filter" style="font-size: 14px">Province: ' +province+ '</span>';
                } else {
                    var provinceTitle = '';
                }

                if (city != 'City') {
                    var cityTitle = space + '<span class="label span_filter" style="font-size: 14px">City: ' +city+ '</span>';
                } else {
                    var cityTitle = '';
                }

                if (name != '') {
                    var nameTitle = space + '<span class="label span_filter" style="font-size: 14px">Store Name: ' +name+ '</span>';
                } else {
                    var nameTitle = '';
                }

                if (agentid != '') {
                    var agentIdTitle = space + '<span class="label span_filter" style="font-size: 14px">Agent ID: ' +agentid+ '</span>';
                } else {
                    var agentIdTitle = '';
                }

                if (transactionDateRange != '') {
                    var str = transactionDateRange.split(',');
                    var text = moment(str[0]).format('DD MMM YYYY') + ' - ' + moment(str[1]).format('DD MMM YYYY');
                    var transactionDateRangeTitle = space + '<span class="label span_filter" style="font-size: 14px">Transaction Date: ' +text+ '</span>';
                } else {
                    var transactionDateRangeTitle = '';
                }

                if (registeredDateRange != '') {
                    var str = registeredDateRange.split(',');
                    var text = moment(str[0]).format('DD MMM YYYY') + ' - ' + moment(str[1]).format('DD MMM YYYY');
                    var registeredDateRangeTitle = space + '<span class="label span_filter" style="font-size: 14px">Register Date: ' +text+ '</span>';
                } else {
                    var registeredDateRangeTitle = '';
                }

                $('#title-filter').html(defaultTitle + provinceTitle + cityTitle + nameTitle + agentIdTitle + transactionDateRangeTitle + registeredDateRangeTitle);
            }
        }

        function resetFilterTitle() {

            var type = $('#type').select2("data")[0].text;
            var status = $('#status').select2("data")[0].text;
            var sales = $('#sales').select2("data")[0].text;

            var space = '&nbsp&nbsp&nbsp&nbsp';
            var defaultTitle = '<span class="label span_filter" style="font-size: 14px">Type: ' +type+ '</span>' +
                space + '<span class="label span_filter" style="font-size: 14px">Status: ' +status+ '</span>' +
                space + '<span class="label span_filter" style="font-size: 14px">Sales: ' +sales+ '</span>';

            $('#title-filter').html(defaultTitle);
        }

        function dateRangeTransactions() {
            var startDate = moment().format('YYYY-MM-DD');
            var endDate = moment().add(7, 'days').format('YYYY-MM-DD');
            var inputDateRangeTransaction = $('#daterange-transactions');

            console.log(startDate);
            console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }

        function dateRangeRegistered() {
            var startDate = moment().format('YYYY-MM-DD');
            var endDate = moment().add(7, 'days').format('YYYY-MM-DD');
            var inputDateRangeRegistered = $('#daterange-registered');

            console.log(startDate);
            console.log(endDate);

            inputDateRangeRegistered.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: startDate,
                endDate: endDate
            });
            inputDateRangeRegistered.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                registeredDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeRegistered.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                registeredDateRange = '';
            });
        }

        function updateCityByProvince(province_id) {
            console.log(province_id);

            // console.log(dataCity);

            var dataCityUpdated = [];
            dataCityUpdated .push({ id: "", text: 'City' });
            for (i = 0; i < dataCity.length; i++) {
                if (dataCity[i].provinces_id === parseInt(province_id, 10)) {
                    dataCityUpdated.push({
                        id: dataCity[i].id,
                        text: dataCity[i].city_name,
                        province_id: dataCity[i].provinces_id
                    });
                } else if (province_id === '') {
                    dataCityUpdated.push({
                        id: dataCity[i].id,
                        text: dataCity[i].city_name,
                        province_id: dataCity[i].provinces_id
                    });
                }
            }

            // console.log(dataCityUpdated);

            // $('#city').html('');
            $('#city').html('').select2({
                data: dataCityUpdated
            });
        }

        function insertDataToDropdownSelect2(data) {
            dataDropDown = data;

            var typeDataSelect2 = [];
            typeDataSelect2.push({ id: "", text: 'All Type' });
            data.type.forEach(function(data){
                // console.log(data.type);
                typeDataSelect2.push({
                    id: data.type,
                    text: data.type.capitalize()
                })
            });

            $('#type').html('').select2({
                data: typeDataSelect2,
                minimumResultsForSearch: Infinity
            });

            var statusDataSelect2 = [];
            statusDataSelect2.push({ id: "", text: 'All Status' });
            data.status.forEach(function(data){
                // console.log(data.type);
                statusDataSelect2.push({
                    id: data.status_verification,
                    text: data.status_verification.capitalize()
                })
            });

            $('#status').html('').select2({
                data: statusDataSelect2,
                minimumResultsForSearch: Infinity
            });

            var salesDataSelect2 = [];
            salesDataSelect2.push({ id: "", text: 'All Sales' });
            data.sales.forEach(function(data){
                // console.log(data.type);
                salesDataSelect2.push({
                    id: data.id,
                    text: data.name
                })
            });

            $('#sales').html('').select2({
                data: salesDataSelect2,
                minimumResultsForSearch: Infinity
            });

            var provinceDataSelect2 = [];
            provinceDataSelect2.push({ id: "", text: 'Province' });
            data.province.forEach(function(data){
                // console.log(data.type);
                provinceDataSelect2.push({
                    id: data.id,
                    text: data.province_name
                })
            });

            $('#province').html('').select2({
                data: provinceDataSelect2
            });

            dataCity = data.city;
            var cityDataSelect2 = [];
            cityDataSelect2.push({ id: "", text: 'City' });
            data.city.forEach(function(data){
                // console.log(data.type);
                cityDataSelect2.push({
                    id: data.id,
                    text: data.city_name,
                    province_id: data.provinces_id
                })
            });

            $('#city').html('').select2({
                data: cityDataSelect2
            });

            hideLoading('.box-filter', 'box-filter');
        }

        var isBoxHide = true;
        function showHideBox() {
            if (isBoxHide) {
                $('#box-filter').show(500);
                $('#box-filterr').show(500);
                $('#btn-show-hide-filter').html('Hide Filter');
            } else {
                $('#box-filter').hide(500);
                $('#box-filterr').hide(500);
                $('#btn-show-hide-filter').html('Show Filter');
            }
            isBoxHide = !isBoxHide;
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID' });
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }

    </script>

    <style>
        .span_type_agent {
            padding: 5px;
            background-color: #1E88E5;
            display: inline-block;
        }
        .span_type_warung {
            padding: 5px;
            background-color: #8BDF00;
            display: inline-block;
        }
        .span_type_locker {
            padding: 5px;
            background-color: #FFA500;
            display: inline-block;
        }
        .span_status_disable {
            padding: 5px;
            background-color: #F33155;
            display: inline-block;
        }
        .span_status_enable {
            padding: 5px;
            background-color: #41B3F9;
            display: inline-block;
        }
        .span_status_pending {
            padding: 5px;
            background-color: #FFBB44;
            display: inline-block;
        }
        .span_status_verified {
            padding: 5px;
            background-color: #7ACE4C;
            display: inline-block;
        }
        .span_filter {
            padding: 5px;
            background-color: #00c0ef;
            display: inline-block;
        }
    </style>

@endsection