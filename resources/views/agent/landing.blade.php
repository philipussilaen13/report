@extends('layout.main')

@section('title')
    Agent
@endsection

@section('css')
    <meta http-equiv="refresh" content="1800">
    <style type="text/css">
        #maps {
            width: 100%;
            height: 512px;
        }
    </style>
@endsection

@section('pageTitle')
    PopBox Agent
@endsection

@section('pageDesc')
    Description
@endsection

@section('content')
    {{-- Highlight Agent --}}
    <div class="row">
        {{-- This Week --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-maroon" id="collapseOnline">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-maroon btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">This Week <strong><span id="onlineCount"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-maroon" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3>{{ $totalThisWeek }}</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-pin text-white"></i>
                        </div>
                        <a href="{{ url('agent/list') }}?status=1" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        {{-- This Month --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-black text-white" id="collapseOffline">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-black btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">This Month <strong><span id="offlineCount"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-black" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3>{{ $totalThisMonth }}</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-pin text-white"></i>
                        </div>
                        <a href="{{ url('agent/list') }}?status=2" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        {{-- Waiting --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-gray" id="collapseWaiting">
                <div class="box-header text-black">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-gray btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Verifikasi <strong><span id="waitingCount"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-gray" style="margin-bottom: 0px;">
                        <div class="inner">
                            <h3>{{ $totalWaiting }}</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-pin"></i>
                        </div>
                        <a href="{{ url('agent') }}?status=0" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        {{-- All --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-green" id="collapseAll">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-green btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Terdaftar <strong><span id="allCount"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-green" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3>{{ $totalAgent }}</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-stalker text-white"></i>
                        </div>
                        <a href="{{ url('agent') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Highlight Transaction and Parcel --}}
    <div class="row">
        {{-- Total Transaction --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion ion-cash"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Transaksi</span>
                    <span class="info-box-number">Rp <span id="transaction-amount"></span></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span class="start-date"></span> - <span class="end-date"></span>
                    </span>
                </div>
                <div class="overlay transaction-loading hide">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
        {{-- Top Up Amount --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-arrow-graph-up-right"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Top Up</span>
                    <span class="info-box-number">Rp <span id="topup-amount"></span></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span class="start-date"></span> - <span class="end-date"></span>
                    </span>
                </div>
                <div class="overlay transaction-loading hide">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
        {{-- Commission and Reward --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-gray">
                <span class="info-box-icon"><i class="ion ion-calculator"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Komisi & Reward</span>
                    <span class="info-box-number">Rp <span id="reward-amount"></span></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span class="start-date"></span> - <span class="end-date"></span>
                    </span>
                </div>
                <div class="overlay transaction-loading hide">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
        {{-- Delivery --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-cube"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Parcel</span>
                    <span class="info-box-number">Rp <span id="parcel-amount">0</span></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span class="start-date"></span> - <span class="end-date"></span>
                    </span>
                </div>
                <div class="overlay transaction-loading hide">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="box box-solid">
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <div id="maps">

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <div class="pad box-pane-right text-black">
                                <form id="form-map-filter" class="text-center">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="form-control select2" name="buildingType">
                                            <option value="">Semua Tipe</option>
                                            @foreach ($listLockerBuilding as $element)
                                                <option value="{{ $element->id }}">{{ $element->building_types }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Province</label>
                                        <select class="form-control select2" name="province">
                                            <option value="">All Province</option>
                                            @foreach ($listProvinces as $item)
                                                <option value="{{ $item->id }}">{{ $item->province_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" id="map-form-city">
                                        <label>City</label>
                                        <select class="form-control select2" name="city">
                                            <option value="">All City</option>
                                            
                                        </select>
                                    </div>
                                    <button class="btn btn-flat btn-block" type="button" id="button-filter">Filter</button>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="description-block border-right">
                                        <h5 class="description-header" id="map-total-agent"></h5>
                                        <span class="description-text">Total Agent</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="overlay hide" id="map-loading">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    {{-- Map --}}
    <script type="text/javascript">
        var map;
        var locationMap = [];
        var markers = Array();

        function initMap() {

            var map = new google.maps.Map(document.getElementById('maps'), {
              zoom: 11,
              // center: {lat: -3.8752905, lng: 117.2135979}
              center : {lat:-6.2487563,lng:106.8301447}
            });

            var imageList = [
                {
                    url: '{{ asset('img/map/marker-waiting.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('img/map/marker-online.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('img/map/marker-offline.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('img/map/marker-waiting-blue.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                }
            ];
            var infoWin = new google.maps.InfoWindow();
            var markers = locationMap.map(function(location, i) {
                var html = '<div align="center">';
                html += '<strong style="color:#FF6300;font-size:15px;">'+location.locker_name+'</strong><br>'+location.locker_id+'<br>';
                html += '<strong>'+location.address+'</strong><br>';
                html += '<a href="{{ url('dashboard/agent/detail') }}/'+location.locker_id+'"><button class="btn btn-xs btn-primary">Detail</button></a>';
                html+='</div>';

                var marker = new google.maps.Marker({
                    position: location,
                    icon : imageList[location.map_icon],
                    map: map,
                });
                google.maps.event.addListener(marker, 'click', function(evt) {
                    infoWin.setContent(html);
                    infoWin.open(map, marker);
                })
                return marker;
            }); 
            // Add a marker clusterer to manage the markers.
            var markerCluster = new MarkerClusterer(map, markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        }
    </script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijkBYFlPmwsLEAfvljffdrnhM7EStYF4&callback=initMap"></script>
    {{-- Ajax Transaction --}}
    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        function ajaxTransaction(){
            $('.transaction-loading').removeClass('hide');
            $.ajax({
                url: '{{ url('agent/ajax/getTransaction') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {},
                success: function(data){
                   if (data.isSuccess==true) {
                        var data = data.data;
                        $('#transaction-amount').html(numberWithCommas(data.transactionAmount));
                        $('#topup-amount').html(numberWithCommas(data.topupAmount));
                        $('#reward-amount').html(numberWithCommas(data.rewardAmount));
                        $('.start-date').html(data.beginWeekDate);
                        $('.end-date').html(data.endWeekDate);
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - getTransaction");
            })
            .fail(function() {
                console.log("error - getTransaction");
            })
            .always(function() {
                $('.transaction-loading').addClass('hide');
                console.log("complete - getTransaction");
                console.log('===================')
            });   
        }

        ajaxTransaction();
    </script>

    {{-- Ready Document --}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var listCities = {!! json_encode($listCities) !!};

            $('#button-filter').on('click', function(event) {
                $('#map-loading').removeClass('hide');
                var formData = $("#form-map-filter").serialize();
                $.ajax({
                    url: '{{ url('agent/ajax/getMapFilter') }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: formData,
                    success: function(data){
                       if (data.isSuccess==true) {
                            var lockerData = data.data;
                            locationMap = lockerData;
                            initMap();

                            var agentCount = lockerData.length;
                            $('#map-total-agent').html(agentCount);
                        } else {
                            alert(data.errorMsg);
                        }
                    },
                    error: function(data){
                        console.log('error');
                        console.log(data);
                    }
                })
                .done(function() {
                    console.log("success - mapFilter");
                })
                .fail(function() {
                    console.log("error - mapFilter");
                })
                .always(function() {
                    $('#map-loading').addClass('hide');
                    console.log("complete - mapFilter");
                    console.log('===================')
                });
            });
            $('#button-filter').trigger('click');

            $('select[name=province]').on('change', function(event) {
                var value = $(this).val();
                var cityList = [];

                var option = '<option value="">All City</option>';

                if (value != '') {
                    for(index=0, len = listCities.length;index < len;++index){
                        if (listCities[index].provinces_id == value) {
                            option += '<option value="'+listCities[index].id+'">'+listCities[index].city_name+'</option>';
                        }
                    }
                }

                $('select[name=city]').html(option);

            });
        });
    </script>
@endsection