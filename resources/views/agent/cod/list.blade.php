@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    
@stop

@section('title')
    COD Confirmation
@endsection

@section('pageTitle')
    COD Confirmation - Dashboard
@endsection

@section('pageDesc')
    List of All COD Transaction
@endsection

@section('content')
    <section class="content">
        {{-- Table --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body border-radius-none">
                    
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                    
                                        <div class="col-md-3">
                                            <div class="form-group">
                								<div class="row" style="margin-left:0px;margin-right:0px;margin-top: 25px">
                									<div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control" id="daterange-transaction" name="dateRange-transaction" placeholder="Transaction Date" readonly="readonly" style="background-color: white;">
                                                    </div>
                								</div>
                							</div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div style="margin-top: 25px">
                                                    <select id="transaction_status" class="form-control" name="transaction_type">
                                                        @foreach($transaction_status as $status)
                                                            <option value="{{ $status['id'] }}">{{ $status['name'] }}</option>
                                                        @endforeach
                                                    </select>
                								</div>
                                            </div>
                                        </div>
                                        
                						<div class="col-md-3">
                                            <div class="form-group">
                								<div style="margin-top: 25px">
                									<select id="id_warung" class="form-control" name="warungid"></select>
                								</div>
                                            </div>
                						</div>
                						
                						<div class="col-md-3">
                                            <div class="form-group">
                                                <div style="margin-top: 25px">
                                                    <input id="transaction_id" class="form-control" name="transaction_id" placeholder="Transaksi ID"/>
                                                </div>
                                            </div>
                                        </div>
						
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <div style="margin-top: 25px">
                                                    <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                                </div>
                                            </div>
                        				</div>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="box-body">
                    		<div class="table-responsive div_content">

                                <table class="table table-hover table-striped" cellspacing="0" width="100%" id="transaction-summary-grid">
                                    <thead>
                                        <tr style="width: 100%">
                                            <th colspan="2" class="text-center" style="background-color: #D3D3D3">Status Transaction</th>
                                        </tr>
                                        <tr style="width: 100%">
                                            <th class="text-center" style="width: 50%; height: 10%;">
                                            	<span class="fa fa-check-circle" id="closed_transaction" style="color: green; font-size: 30px"></span> <br>Closed Transaction 
                                        	</th>
                                            <th class="text-center" style="width: 50%; height: 10%;">
                                            	 <span class="fa fa-envelope-open-o" id="open_transaction" style="color: red; font-size: 30px"></span> <br>Open Transaction
                                        	</th>
                                        </tr>
                                    </thead>
                                </table>

                                <table class="table table-hover table-striped" cellspacing="0" width="100%" id="transaction-grid">
                                    <thead>
                                        <tr style="width: 100%">
                                            <th width="30">No</th>
                                            <th width="90">Transaction Date</th>
                                            <th width="100">Warung Name</th>
                                            <th width="100">Locker ID</th>
                                            <th width="190">Transaction ID</th>
                                            <th width="50">Status</th>
                                            <th width="80">Last Updated</th>
                                            <th width="100">Updated By</th>
                                            <th width="30"></th>
                                        </tr>
                                    </thead>
                                </table>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 15;
        var isBoxHide = true;
        
        $(function(){
            $.fn.detailTransaction = function(id, lockerid) {
                window.location = "{{ url('agent/transaction/cod/detail') }}/"+id+"/"+lockerid;
            };			
 
			jQuery.fn.dataTableExt.oApi.fnFilterOnReturn = function (oSettings) {
				var _that = this;

				this.each(function (i) {
					$.fn.dataTableExt.iApiIndex = i;
					var $this = this;
					var anControl = $('input', _that.fnSettings().aanFeatures.f);
					anControl
						.unbind('keyup search input')
						.bind('keypress', function (e) {
							if (e.which == 13) {
								$.fn.dataTableExt.iApiIndex = i;
								_that.fnFilter(anControl.val());
							}
						});
					return this;
				});
				return this;
			};

            $('#id_warung').select2({
				width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: "{{ url('warung/transaction/warung/getlistmasterwarung') }}",
                    method: "GET",
                    dataType: 'JSON',
                    data: function (params) {
                        var query = {
                            search: params.term || '*',
                            type: 'public'
                        }
                        return query;
                    },
                    processResults: function (data, params) {
						if(data.isSuccess) {
                            return {
                                results: $.map(data.data.lockerData, function (item) {
                                    return {
                                        region_code: item.region_code,
                                        text: item.locker_name,
                                        id: item.locker_id
                                    }
                                })
                            };
						}
                    }
                }
            });

            $('#id_btn_cont_filter').on('click',function(e) {
                e.stopImmediatePropagation();

                if(isBoxHide) {
                    $('#id_box_body').show(500);
                    $(this).html('Hide Filter');
                    isBoxHide = false;
				}
				else {
                    $('#id_box_body').hide(500);
                    $(this).html('Show Filter');
                    isBoxHide = true;
				}
			});
			
			$('#transaction-grid').DataTable({
                                
				'paging'      : true,
				'lengthChange': false,
				'ordering'    : false,
				'info'        : true,
				'autoWidth'   : false,
				"processing": true,
				"serverSide": true,
				'searching' : false,
				
				"pageLength": limit,
				"ajax": {
					"url": "{{ route('transaction-list') }}",
					"data": function ( d ) {
						var info = $('#transaction-grid').DataTable().page.info();						
						d.locker = $('#id_warung').val();
						d.daterange_transaction = transactionDateRange;
						d.transactionid = $("#transaction_id").val();
						d.transactionstatus = $("#transaction_status").val();
						d.start = info.start;
						d.limit = limit;
					},
					"dataSrc": function(json){
						json.draw = json.parameter.draw;
						json.recordsTotal = json.recordsTotal;
						json.recordsFiltered = json.recordsTotal;

						$('#closed_transaction').text(json.status_closed);
						$('#open_transaction').text(json.status_open);
						return json.allTransaction;
					},
				},
                                
				"columnDefs" : [
					{ "targets": 0, "data": "no", "className": "text-center" },
					{ "targets": 1, "data": "created_at", "className": "text-center",
						"render": function ( data, type, row, meta ) {
                            var momentDateTime = moment(data);
                            return momentDateTime.format('DD-MM-YYYY HH:mm:ss');
                        }
                    },
					{ "targets": 2, "className": "text-center", "data": "user.locker.locker_name" },
					{ "targets": 3, "className": "text-center", "data": "locker_id" },
					{ "targets": 4, "className": "text-center", "data": "reference" },
					{ "targets": 5, "className": "text-center", "data": function ( data, type, row, meta ) {
							var items = data.items;
							var status_items = 'delivered';

							for(var i = 0; i < items.length; i++){
								if(items[i].cod_status === null || items[i].cod_status === '' || items[i].cod_status === 'not_yet_delivery'){
									status_items = items[i].cod_status;
									break;
								}
								
							}
							return ( data.status === 'PAID' && status_items === 'delivered' ) ? '<span class="label label-success">CLOSED</span>' : '<span class="label label-danger">OPEN</span>';
                        } 
                    },
					{ "targets": 6, "className": "text-center", "data": function ( data, type, row, meta ) {
							var histories = data.histories;
                            var size = histories.length;
                            if(size > 0){
                            	return moment(histories[size-1].updated_at).format('DD-MM-YYYY HH:mm:ss');
                            }
                            return data.updated_at;
                        }
                    },
					{ "targets": 7, "className": "text-center", "data": function ( data, type, row, meta ) {
						var histories = data.histories;
                        var size = histories.length;
                        if(size > 0){
                        	return histories[size-1].user;
                        }
                        return data.user.name;
                    }
                },
					{ "targets": 8, "className": "text-center", "data": null,
						"render": function ( data, type, row, meta ) {
							return '<a onclick=$(this).detailTransaction("'+data.id+'","'+data.locker_id+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-external-link"></i></a>';
						}
					}
				]
			});
			$('#transaction-grid').dataTable().fnFilterOnReturn();
			
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                $('#transaction-grid').DataTable().ajax.reload();
            });
        });
        
        function getAjaxTransactionDownload() {
            
           var value = collectParameters();
           
           if($("#transaction-grid").DataTable().rows().count() == 0){
               alert('No Data');
           } else {

                showLoading('.box-filter', 'box-filter');
                showLoading('.box-transaction-table', 'box-transaction-table');
                
                $.ajax({
                	url: '{{ route('download-excel') }}',
                    data: {
                        _token                  : '{{ csrf_token() }}',
                        daterange_transaction   : value.dateRange,
                        id_warung               : value.id_warung,
                        transactionid           : value.transaction_id,
                        transactionstatus       : value.transaction_status
                    },
                    type: 'get',
                    responseType: 'blob', // important
                    async : true,
                    success: function (response, textStatus, request) {

                        hideLoading('.box-filter', 'box-filter');
                        hideLoading('.box-transaction-table', 'box-transaction-table');
                        
                        var a = document.createElement("a");
                        a.href = response.file; 
                        a.download = response.name;
                        document.body.appendChild(a);
                        a.click();
                        a.remove();
                    },
                    error: function (ajaxContext) {
                        hideLoading('.box-filter', 'box-filter');
                        hideLoading('.box-transaction-table', 'box-transaction-table');
                        alert('Export error: '+ajaxContext.responseText);
                    }
                });
               
           }
        }
        
        function collectParameters(){
                        
            var dateRange           = transactionDateRange;
            var id_warung           = $('#id_warung').val();
            var transaction_id      = $("#transaction_id").val();
            var transaction_status    = $("#transaction_status").val();
            
            var params = [];
            
            params.push({
                token               : '{{ csrf_token() }}',
                dateRange           : dateRange,
                id_warung           : id_warung,
                transaction_id      : transaction_id,
                transaction_status    : transaction_status
            });
            
            return params[0];
        }
    </script>
    
    {{-- Date Range Picker --}}
    <script type="text/javascript">
        var transactionDateRange = '';
        var startDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
        var endDate = moment().format('YYYY-MM-DD');

        var startDateToShow = moment().subtract(29, 'days').format('DD/MM/YYYY');
        var endDateToShow = moment().format('DD/MM/YYYY');
        transactionDateRange = startDate + ',' + endDate;

        jQuery(document).ready(function ($) {
            dateRangeTransaction();

            $('#daterange-transaction').val(startDateToShow + ' - ' + endDateToShow);

        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange-transaction');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }

    </script>
@stop