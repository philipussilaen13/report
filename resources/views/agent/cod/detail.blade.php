@extends('layout.main') @section('title') Detail Transaction @endsection

@section('css') {{-- select 2 --}}
<link rel="stylesheet"
	href="{{ asset('plugins/select2/css/select2.min.css') }}">
{{-- Date Range picker --}}
<link rel="stylesheet"
	href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet"
	href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet"
	href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection @section('pageTitle') Detail Transaction @endsection

@section('content')
<section class="content">
	<div id="vue-app" class="row" v-cloak>
		<div class="col-md-12">
			<div class="box box-solid">
				<div class="box-body border-radius-none">

					<table class="table table-striped">
						<tr>
							<td style="width: 30%;">Nama Agent / Warung</td>
							<td>: @{{ agent_name }}</td>
						</tr>
						<tr>
							<td style="width: 30%;">Locker ID</td>
							<td>: @{{ locker_id }}</td>
						</tr>
						<tr>
							<td style="width: 30%;">Transaction No.</td>
							<td>: @{{ reference }}</td>
						</tr>
						<tr>
							<td style="width: 30%;">Transaction Date</td>
							<td>: @{{ created_at }}</td>
						</tr>
						<tr>
							<td style="width: 30%;">Status</td>
                			<td>: 
                				<span class="label label-success" v-if="status === 'PAID'">CLOSED</span>
                				<span class="label label-danger" v-else>OPEN</span>
                			
                			</td>

						</tr>
						<tr>
							<td style="width: 30%;">Last Updated</td>
							<td>: @{{ last_updated }}</td>
						</tr>
						<tr>
							<td style="width: 30%;">Last Updated By</td>
							<td>: @{{ last_updated_by }}</td>
						</tr>
					</table>
					<br>
					<table class="table table-striped"
						style="border-style: solid; width: 50%">
						<tr>
							<td style="width: 60%">Change status all transaction</td>
							<td>
    						  	@if(\App\Models\Privilege::hasAccess('agent/transaction/cod/detail/update-item-status'))
    								<select id="all_status" class="form-control select2" name="set_all_item_status" v-model="set_all_item_status" :disabled="status === 'PAID'">
    									<option disabled="" value="not_yet_delivery">-- Pilih --</option>
    									<option v-for="status in all_status" :value="status.id">@{{ status.name }}</option>
    								</select>
    							@else
    								: <label>@{{ lblStatus(set_all_item_status) }}</label>
    							@endif
							</td>
						</tr>
					</table>

					<div class="box-body">
						<div class="table-responsive div_content">
							<table class="table table-hover table-striped"
								style="width:100%; cellspacing:0" id="gridcontent">
								<thead>
									<tr>
										<th width="30">No</th>
										<th>Product Name</th>
										<th>Product Code</th>
										<th>Qty</th>
										<th>Price Per Item</th>
										<th>Total Price</th>
										<th>Status COD</th>
									</tr>
								</thead>
								<tbody>
									<template v-for="(row, idx) in items">
									<tr>
										<td>@{{ idx+1 }}</td>
										<td>@{{ row.name }}</td>
										<td>@{{ getProductId(row.params) }}</td>
										<td>@{{ getQty(row.params) }}</td>
										<td>@{{ getPricePerItem(row.price, row.params) }}</td>
										<td>@{{ row.price }}</td>
										<td>
                						  	@if(\App\Models\Privilege::hasAccess('agent/transaction/cod/detail/update-item-status'))
    											<select :id="row.id" class="form-control select2" name="transaction_status" v-model="row.cod_status" 
    												v-bind:disabled="row.status_d">
    												<option value="not_yet_delivery" disabled>-- Pilih --</option>
    												<option v-for="status in statuses" :value="status.id">@{{ status.name }}</option>
    											</select>
                							@else
                								<label>@{{ lblStatus(row.cod_status) }}</label>
                							@endif
										</td>
									</tr>
									</template>
								</tbody>
							</table>
							
						  	@if(\App\Models\Privilege::hasAccess('agent/transaction/cod/detail/update-item-status'))
    							<div class="no-padding" align="right">
    								<button class="btn btn-flat btn-success" id="btn_submit"
    									style="width: 200px; margin-right: 30px;" :disabled="status === 'PAID'">Save</button>
    							</div>
							@endif
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection @section('js') 

@if(App::environment('production'))
	<script src="{{ asset ('assets/js/vue.min.js') }}"></script>
@else
	<script src="{{ asset ('assets/js/vue.js') }}"></script>
@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
{{-- DateRange Picker --}}
<script
	src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/helper.js') }}"></script>

<script>
        var app = new Vue({
            
        		el		: "#vue-app",
    			data	: {
                    filters: {
                    	transactionid : '{!! $id !!}',
   						start : 0,
   						limit : 50,
                    },
            		agent_name 			: '{!! $transaction[0]->name !!}',
            		locker_id			: '{!! $transaction[0]->locker_id !!}',
            		reference			: '{!! $transaction[0]->reference !!}',
            		created_at			: '{!! $transaction[0]->created_at !!}',
            		last_updated		: '{!! $last_updated !!}',
            		trx_id				: '{!! $id !!}',
            		last_updated_by		: '{!! $last_updated_by !!}',
            		set_all_item_status	: 'not_yet_delivery',
//             		disabled			: '',
            		all_status			: [{'id' : 'delivered', 'name' : 'Sudah Diterima'}],
            		statuses			: {!! json_encode($statuses) !!},
            		items				: [],
            		items_status		: {},
            		selected			: [],
            	},
        		computed: {
            		status: function(){
                		var stat = '{!! $transaction[0]->status !!}';
                        var $vm = this;
                		     if( stat === 'PAID' ) {
                    		     stat = 'PAID';
                    		     $vm.set_all_item_status = 'delivered';
//                     		     $vm.disabled = 'disabled="disabled"';
                   		     
                		     }
                		     else {
                    		     stat = '';
                    		     $vm.set_all_item_status = 'not_yet_delivery';
                		     }

                		return stat;
            		},
                    queryParams: function () {
                        return JSON.parse(JSON.stringify(this.filters));
                    },
            	},
            	watch: {
                	
            	},
            	created : function(){
                	
            	},
                mounted : function(){
                	this.loadDataItems();
                	this.$nextTick(function(){
                        var $vm = this;

//                         for(var i = 0; i < $vm.items.length; i++){
//                         	$('#'+$vm.items[i].id).select2().on('change', function(){});
//                         }
                        
                        $('#all_status').select2().on('change', function(){
                        	for(var i = 0; i < $vm.items.length; i++){
                            	if($vm.items[i].cod_status == null || $vm.items[i].cod_status == '' || $vm.items[i].cod_status == 'not_yet_delivery'){
                        			$vm.items[i].cod_status = 'delivered';
                            	}
                        	}
                        });
                        
                		$(document).ready(function() {
                			 $('#btn_submit').on('click', function(e) {
                    			 
                				 e.stopImmediatePropagation();

                				 //$vm.disabled_items_status_option();
                				 
                                 $('#btn_submit').attr('disabled', 'disabled');
                                 $('#all_status').attr('disabled', 'disabled');

                                 if($('#all_status').val() === '1'){
                                     for(var i = 0; i < $vm.items.length; i++){

                                         $('#'+$vm.items[i].id).attr('disabled', 'disabled');
                                         
                                         $vm.selected.push({"id": $vm.items[i].id, "status": 'delivered'});
                                     }
                                 } else {
                                     for(var i = 0; i < $vm.items.length; i++){
                                         $('#'+$vm.items[i].id).attr('disabled', 'disabled');
                                         
                                         if($vm.items[i].cod_status !== 'not_yet_delivery'){
                                        	 $vm.selected.push({"id": $vm.items[i].id, "status": $vm.items[i].cod_status});
                                         }
                                     }
                                 }

                                 if($vm.selected.length === 0){
                                     alert('No data');
                                     for(var i = 0; i < $vm.items.length; i++){
                                         $('#'+$vm.items[i].id).removeAttr('disabled', 'disabled');
                                     }
                                     $('#btn_submit').removeAttr('disabled', 'disabled');
                                     $('#all_status').removeAttr('disabled', 'disabled');

                                 }
                                 else {
                                     $.ajax({
                                         url: '{{ route('update-item-status') }}',
                                         data: {
                                             _token               : '{{ csrf_token() }}',
                                             transactionReference : $vm.reference,
                                             lockerId             : $vm.locker_id,
                                             items                : $vm.selected,
                                         },
                                         type: 'post',
                                         success: function (response, textStatus, request) {
                                             window.location.href=window.location.href
                                         },
                                         error: function(data) {console.log(data);
                                             alert(data.responseJSON.error)
                                         },
                                     });
                                 }
                			 });
                		});
                	});
                },
                methods : {
                	loadDataItems: function (page) {
                        return $.get("{{ url('agent/transaction/cod/detail/get-items') }}", this.queryParams)
                                .done(function (result) {
                                	this.items = result.transactionItems;
//                                 	for(var i = 0; i < this.items.length; i++){
//                                     	var key = "key_"+this.items[i].id;
//                                 		var val = this.items[i].cod_status === 'delivered' 		? '1' :
//                                 			this.items[i].cod_status === 'partially_delivered' 	? '2' :
// 											this.items[i].cod_status === 'cancelled' 			? '3' :	'0';
//                                 		this.$set(this.items_status, key, val);
//                                 	}
                                	
                                }.bind(this))
                                .always(function(){
                                    
                                }.bind(this));
                    },
                    getProductId: function(param){
						var obj = JSON.parse(param);
						return obj.product_id;
                    },
                    getQty: function(param){
						var obj = JSON.parse(param);
						return obj.amount;
                    },
                    getPricePerItem: function(price, param){
                    	var obj = JSON.parse(param);
                    	return price / obj.amount;
                    },
                    disabled_items_status_option:function(){
    					for(var i = 0; i < this.items.length; i++){
    						this.disabled_item_status_option(this.items[i].id);
    					}
                    },
                    disabled_item_status_option:function(id){
                    	$('#'+id).attr('disabled', 'disabled');
                    },
                    lblStatus:function(status){
                    	var label = 'Belum Diterima';

                    	this.statuses.forEach(function (value){
							if(status == value.id)
								label = value.name;
                    	});
                    	return label;
                    },
                },
            });
    </script>
@endsection
