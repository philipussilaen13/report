@extends('layout.main')

@section('title')
    Commission Schema
@endsection

@section('css')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
    Agent Article
@endsection

@section('pageDesc')
    Agent Article Management
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Tambah Artikel/ Banner</h3>
                </div>
                <div class="box-body">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Types</label>
                                    <select class="form-control" name="type" required>
                                        <option value="">Pilih Tipe</option>
                                        @foreach ($types as $element)
                                            <option value="{{ $element }}">{{ ucfirst($element) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Value</label>
                                    <input type="text" name="value" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Short Description</label>
                                    <input type="text" name="shortDescription" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Active Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="dateRange" name="dateRange" placeholder="Tanggal Aktif" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" required>
                                        <option value="1">Active</option>
                                        <option value="2">Not Active</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Picture</label>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Long Description</label>
                                    <textarea class="form-control" name="longDescription" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Daftar Artikel / Banner</h3>
                </div>
                <div class="box-body border-radius-none">
                    <div class="table-responsive no-padding table-bordered">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th data-priority="1">Type</th>
                                <th>Title</th>
                                <th>Value</th>
                                <th data-priority="2">Picture</th>
                                <th>Short Description</th>
                                <th>Long Description</th>
                                <th>Active Date</th>
                                <th>Status</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($articles as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>
                                        <span class="label label-info">{{ $item->type }}</span>
                                    </td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->value }}</td>
                                    <td>
                                        @if (!empty($item->image))
                                            <img src="{{ \App\Http\Helpers\Helper::createImgUrlAgent('articles',$item->image) }}" style="width: 50%;">
                                        @else
                                            <img src="{{ $item->image_url }}" style="width: 50%;">
                                        @endif
                                    </td>
                                    <td>{{ $item->short_description }}</td>
                                    <td>{{ $item->long_description }}</td>
                                    <td><strong>{{ date('j F Y',strtotime($item->start_date)) }}</strong> - <strong>{{ date('j F Y',strtotime($item->end_date)) }}</strong></td>
                                    <td>
                                        @if ($item->status==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Not Active</span>
                                        @endif
                                    </td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>
                                        <button class="btn btn-flat btn-info btn-update" data-id="{{ $item->id }}" data-type="{{ $item->type }}" data-value="{{ $item->value }}" data-title="{{ $item->title }}" data-shortdesc="{{ $item->short_description }}" data-longdesc="{{ $item->long_description }}" data-status="{{ $item->status }}" data-begin="{{ $item->start_date }}" data-end="{{ $item->end_date }}">Update</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- bootstrap datepicker -->
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('plugins/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    {{-- DateTime Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            //Date range picker
            $('#dateRange').daterangepicker({
                locale : {
                    format : 'YYYY/MM/DD'
                },
            });
        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.btn-update').on('click', function(event) {
                var data = $(this).data();
                $('input[name=id]').val(data.id);
                $('select[name=type]').val(data.type);
                $('input[name=title]').val(data.title);
                $('input[name=value]').val(data.value);
                $('input[name=shortDescription]').val(data.shortdesc);
                $('textarea[name=longDescription]').val(data.longdesc);
                $('select[name=status]').val(data.status);
                var startDate = data.begin;
                var endDate = data.end;
                $('#dateRange').daterangepicker({
                    locale : {
                        format : 'YYYY/MM/DD'
                    },
                    startDate : startDate,
                    endDate : endDate,
                });

                $('input[name=status]').val(data.status);
                $('#myModal').modal('show');
            });
        });
    </script>
@endsection