@extends('layout.main')

@section('title')
	Commission Schema
@endsection

@section('css')
	<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/dist/fullcalendar.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/dist/fullcalendar.print.min.css') }}" media="print">
  	<!-- bootstrap datepicker -->
  	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  	<!-- daterange picker -->
  	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
	Agent Commission Schema
@endsection

@section('pageDesc')
	Agent Commission Schema Management
@endsection

@section('content')
	<div class="row">
		<div class="col-md-4">
			<div class="box">
            	<div class="box-body">
            		<h3>Add Commission Schema</h3>
            		@if (session('success'))
		                <div class="alert alert-success">
		                    {{ session('success') }}
		                </div>
		            @endif
		            @if (session('error'))
		                <div class="alert alert-danger">
		                    {{ session('error') }}
		                </div>
		            @endif
            		<form method="post">
            			{{ csrf_field() }}
            			<div class="form-group">
	              			<label>Type</label>
	              			<select class="form-control" name="type" required>
	              				<option value="percent">Percentage</option>
	              				<option value="fixed">Fixed</option>
	              			</select>
	              		</div>
	              		<input type="hidden" name="rule" value="publish">
	              		<div class="form-group">
	              			<label>Value</label>
	              			<input type="text" name="values" class="form-control" required>
	              		</div>
	              		<div class="row">
	              			<div class="col-md-6">
	              				<div class="form-group">
			              			<label>Min Amount</label>
			              			<input type="number" name="minAmount" class="form-control">
			              		</div>
	              			</div>
	              			<div class="col-md-6">
	              				<div class="form-group">
			              			<label>Max Amount</label>
			              			<input type="number" name="maxAmount" class="form-control">
			              		</div>
	              			</div>
	              		</div>
	              		<div class="form-group">
	              			<label>Active Date</label>
	              			<input type="text" name="activeDate" class="form-control">
	              		</div>
	              		<div class="form-group">
	              			<label>Priority</label>
	              			<select class="form-control" name="priority">
	              				<option value="1">1</option>
	              				<option value="2">2</option>
	              				<option value="3">3</option>
	              			</select>
	              		</div>
	              		<button class="btn btn-flat btn-info btn-block">Add Commission Schema</button>
            		</form>
            	</div>
          	</div>
		</div>
		<div class="col-md-8">
			<div class="box">
            	<div class="box-body no-padding">
              		<div id="calendar"></div>
            	</div>
          	</div>
		</div>
		<div class="col-md-12">
			<div class="box box-solid">
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>Value</th>
								<th>Rules</th>
								<th>Priority</th>
								<th>Status</th>
								<th>Active Date</th>
								<th>Approval Status</th>
								<th></th>
							</tr>
							@foreach ($commissionDb as $item)
								<tr>
									<td>{{ ($item->values) }} {{ $item->type }}</td>
									<td>
										@php
											$rules = $item->commissionRules;
										@endphp
										@if (count($rules)>0)
											<ul>
												@foreach ($rules as $element)
													<li>
														{{ $element->parameter->description }} {{ $element->operator }} 
														{{ \App\Http\Helpers\Helper::trimText($element->value,50) }}
													</li>
												@endforeach
											</ul>
										@else
											Product : {{ $item->product }}
										@endif
									</td>
									<td>{{ $item->priority }}</td>
									<td>
										@php
											$nowDateTime = date('Y-m-d H:i:s');
										@endphp
										@if ($item->status == '0')
											<span class="label label-danger">Disabled</span>
										@elseif ($item->status == 1 && $nowDateTime >= $item->start_date && $nowDateTime <= $item->end_date)
											<span class="label label-success">Active</span>
										@elseif ($item->status == 1 && $nowDateTime < $item->start_date)
											<span class="label label-primary">Active Soon</span>
										@elseif ($item->status == 1 && $nowDateTime > $item->end_date)
											<span class="label label-warning">Expired</span>
										@endif
									</td>
									<td>
										{{ date('j F Y H:i:s',strtotime($item->start_date)) }} - {{ date('j F Y H:i:s',strtotime($item->end_date)) }}
									</td>
									<td>
										@if ($item->approval_status == 'pending')
											<span class="label label-warning">Pending</span>
										@elseif ($item->approval_status == 'rejected')
											<span class="label label-danger">Rejected</span>
										@elseif ($item->approval_status == 'approved')
											<span class="label label-success">Approved</span> <br>
											{{ $item->approved_by }}
										@endif
									</td>
									<td>
										<a href="{{ url('agent/marketing/commission/rule').'/'.$item->id }}">
											<button class="btn btn-info">Update</button>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<!-- fullCalendar -->
	<script src="{{ asset('plugins/moment/moment.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/dist/fullcalendar.js') }}"></script>
	<!-- bootstrap datepicker -->
	<script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	<!-- date-range-picker -->
	<script src="{{ asset('plugins/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	{{-- Calendar --}}
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			var date = new Date()
		    var d    = date.getDate(),
		        m    = date.getMonth(),
		        y    = date.getFullYear();
		        
		    var eventDb = {!! json_encode($activeList) !!};
			var eventArr = [];
			for (var i = 0; i < eventDb.length; i++) {
				eventArr.push({
					title : eventDb[i].name,
					start : eventDb[i].start_time,
					end : eventDb[i].end_time,
					color : eventDb[i].color,
					created : eventDb[i].created_at,
					priority : eventDb[i].priority,
					allDay : true
				});
			}
		    $('#calendar').fullCalendar({
		      	header    : {
		        	left  : 'prev,next today',
		        	center: 'title',
		        	right : 'month,agendaWeek'
		      	},
		      	buttonText: {
		        	today: 'today',
		        	month: 'month',
		        	week : 'week',
		        	day  : 'day'
		      	},
		      	//Random default events
		      	eventOrder : 'priority,created_at',
		      	events    : eventArr,
    		})
		});
	</script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
		    $('input[name=activeDate]').daterangepicker({ timePicker: false, format: 'YYYY/MM/DD h:mm A' });
		    $('.btn-expand').click(function(event) {
				var p = $(this).prev('p[class=short]');
				var lineheight = parseInt(p.css('line-height'));
				if (parseInt(p.css('height')) == lineheight*3) {
					p.css('height', 'auto');
					$(this).text('Less');
				} else {
					p.css('height', lineheight*3+'px');
					$(this).text('More');
				}
			});
		});	
	</script>
@endsection