<div class="box box-solid bg-gray" id="rule-{{ $newRuleCount }}">
	<div class="box-body">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group">
					<label>Parameter</label>
					<select name="parameterId[{{ $newRuleCount }}]" rule="{{ $newRuleCount }}" class="form-control select2 rule-parameter">
						@foreach ($availableParam as $group)
							<optgroup label="{{ $group['paramCategory'] }}">
								@foreach ($group['paramName'] as $element)
									<option value="{{ $element['id'] }}" name="{{ $element['name'] }}">{{ $element['description'] }} ({{ $element['name'] }})</option>
								@endforeach
							</optgroup>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Operator</label>
					<select name="operator[{{ $newRuleCount }}]" rule="{{ $newRuleCount }}" class="form-control select2 rule-operator">
						@foreach ($operatorList as $key => $element)
							<option value="{{ $key }}">{{ $element }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>
						Value
						<span class="label label-info addValue" rule="{{ $newRuleCount }}">Add</span>
					</label>
					<div class="ruleValueList" rule="{{ $newRuleCount }}">
						<div class="row ruleValueItem" rule="{{ $newRuleCount }}" item="0">
							<div class="col-md-10">
								<input type="text" name="value[{{ $newRuleCount }}][0]" class="form-control rule-value" rule="{{ $newRuleCount }}" item="0">
							</div>
							<div class="col-md-1">
								<span class="btn btn-xs btn-info helpValue" rule="{{ $newRuleCount }}" item="0">Helper</span>
							</div>
							<div class="col-md-1">
								<span class="btn btn-xs btn-warning delValue" rule="{{ $newRuleCount }}" item="0">x</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<button type="button" class="btn btn-flat btn-danger deleteRule" rule="{{ $newRuleCount }}">Delete</button>
</div>