@extends('layout.main')

@section('title')
	Agent Referral
@endsection

@section('css')
	{{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
	<!-- bootstrap datepicker -->
  	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
  	<!-- daterange picker -->
  	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  	 <style type="text/css">
        .border-right{
            border-right: 1px solid #f4f4f4
        }
    </style>
@endsection

@section('pageTitle')
	Agent Referral Campaign
@endsection

@section('pageDesc')
	Agent Referral Management
@endsection

@section('content')
	<div class="box box-solid" id="box-body">
		<div class="box-body">
			<h3>Add Referral Campaign</h3>
			<form method="post">
				{{ csrf_field() }}
				<input type="hidden" name="id">
				@if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
				<div class="row">
					{{-- General --}}
					<div class="col-md-4 border-right">
						<h4 class="text-blue">General</h4>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Active Date</label>
							<input type="text" name="activeDate" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Expired</label>
							<input type="number" name="expired" class="form-control">
							<p class="help-block">Number of Expired on Days</p>
						</div>
						<div class="form-group">
							<label>Limit Usage</label>
							<input type="number" name="limit" class="form-control">
							<p class="help-block">Number of Limit Usage per Referral Code</p>
						</div>
					</div>
					<div class="col-md-4 border-right">
						<h4 class="text-red">Rules</h4>
						<div class="form-group">
							<label>Type</label>
							<select class="form-control" name="type">
								@foreach ($availableType as $element)
									<option value="{{ $element }}">{{ $element }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Rule</label>
							<select class="form-control" name="rule">
								@foreach ($availableOperator as $element)
									<option value="{{ $element }}">{{ $element }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Value</label>
							<input type="text" name="rule-value" class="form-control">
						</div>
					</div>
					<div class="col-md-4">
						<h4 class="text-green">From And To Amount</h4>
						<div class="form-group">
							<label>From Amount</label>
							<input type="number" name="fromAmount" class="form-control" required>
						</div>
						<div class="form-group">
							<label>From Agent</label>
							<select class="select2 form-control" name="fromLockerId">
								<option value="">Not Set</option>
								@foreach($fromLockerList as $item)
									<option value="{{ $item->id }}">{{ $item->locker_name }}</option>
								@endforeach
							</select>
							<p class="help-block">If exist, then only selected from agent will get the from amount. Set if have special code</p>
						</div>
						<div class="form-group">
							<label>To Amount</label>
							<input type="number" name="toAmount" class="form-control" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="status">
								<option value="0">Disabled</option>
								<option value="1">Active</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Special Code</label>
							<input type="text" name="code" class="form-control">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label>Description</label>
							<textarea class="textarea form-control" name="description"></textarea>
						</div>
						<div class="form-group">
							<label>Description To Shared</label>
							<textarea class="form-control" name="descriptionToShared" rows="3"></textarea>
						</div>
					</div>
					<div class="col-md-4">
						<button class="btn btn-block btn-flat btn-info">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="box box-solid">
		<div class="box-body">
			<div class="table-responsive no-padding">
				<table class="table table-hover">
					<tbody>
						<tr>
							<th>Name</th>
							<th>Type</th>
							<th>Active Date</th>
							<th>Status</th>
							<th>Approval Status</th>
							<th>Limit</th>
							<th>From</th>
							<th>To</th>
							<th>Code</th>
							<th>Expired</th>
							<th></th>
						</tr>
						@foreach ($referralCampaignDb as $element)
							<tr>
								<td>{{ $element->name }}</td>
								<td>
									{{ $element->type }} <br>
									@php
										$transactionValue = null;
										$transactionOperator = null;
									@endphp
									@if (!empty($element->rule))
										@php
											$schemaTransactionRule = json_decode($element->rule);
                							$transactionValue = reset($schemaTransactionRule);
                							$transactionOperator = key($schemaTransactionRule);
										@endphp	
										{{ $transactionOperator }} {{ number_format($transactionValue) }}
									@endif
								</td>
								<td>
									{{ $element->start_date }} - {{ $element->end_date }}
								</td>
								<td>
									@if ($element->status == 1)
										<span class="label label-success">Active</span>
									@else
										<span class="label label-danger">Disabled</span>
									@endif
									@if (time() > strtotime($element->end_date))
										<span class="label label-danger">Expired</span>
									@elseif (time() < strtotime($element->start_date))
										<?php
											$diff = strtotime($element->start_date) - time(); 
										?>
										<span class="label label-general">{{ \App\Http\Helpers\Helper::secondsToTime($diff) }}</span>
									@endif
								</td>
								<td>
									@if ($element->approval_status == 'pending')
										<span class="label label-warning">Pending</span>
									@elseif ($element->approval_status == 'rejected')
										<span class="label label-danger">Rejected</span>
									@elseif ($element->approval_status == 'approved')
										<span class="label label-success">Approved</span> <br>
										{{ $element->approved_by }}
									@endif
								</td>
								<td>
									@if (empty($element->limit_usage))
										<span class="label bg-black">No Limit</span>
									@else
										{{ $element->limit_usage }}
									@endif
								</td>
								<td>
									@if (!empty($element->from_locker_id))
										<strong>{{ $element->from_locker_id }}</strong> <span class="label label-danger">From Locker Exist</span> <br>
									@endif
									{{ number_format($element->from_amount) }} <br>
									{!! $element->description !!}
								</td>
								<td>
									{{ number_format($element->to_amount) }} <br>
									{!! $element->description_to_shared !!}
								</td>
								<td>
									{{ $element->code }}
								</td>
								<td>
									{{ $element->expired }} days
								</td>
								<td>
									<button class="btn btn-flat btn-info btn-update"
										data-name="{{ $element->name }}"
										data-type="{{ $element->type }}"
										data-rule="{{ $element->rule }}"
										data-transValue="{{ $transactionValue }}"
										data-transOperator="{{ $transactionOperator }}"
										data-fromLockerId="{{ $element->from_locker_id }}"
										data-fromAmount="{{ $element->from_amount }}"
										data-toAmount="{{ $element->to_amount }}"
										data-code="{{ $element->code }}"
										data-expired="{{ $element->expired }}"
										data-description="{!! $element->description !!}"
										data-descriptionTo="{!! $element->description_to_shared !!}"
										data-id="{{ $element->id }}"
										data-startDate="{{ $element->start_date }}"
										data-endDate="{{ $element->end_date }}"
										data-status="{{ $element->status }}"
										data-limit="{{ $element->limit_usage }}">
										Edit
									</button>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{{ $referralCampaignDb->links() }}
		</div>
	</div>
@endsection

@section('js')
	<!-- bootstrap datepicker -->
	<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
	<!-- date-range-picker -->
	<script src="{{ asset('plugins/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
	{{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.select2').select2();
		    $('input[name=activeDate]').daterangepicker({ timePicker: false, format: 'YYYY/MM/DD h:mm A' });
		    $('textarea[name=description]').wysihtml5();

		    $('.btn-update').on('click', function() {
		    	var data = $(this).data();
		    	console.log(data);

		    	var id = data.id;
		    	var name = data.name;
		    	var startDate = moment(data.startdate);
		    	var endDate = moment(data.enddate);
		    	var type = data.type;
		    	var rule = data.transoperator;
		    	var ruleValue = data.transvalue;
		    	var fromAmount = data.fromamount;
		    	var toAmount = data.toamount;
		    	var code = data.code;
		    	var expired = data.expired;
		    	var status = data.status;
		    	var description = data.description;
		    	var descriptionTo = data.descriptionto;
		    	var fromLockerId = data.fromlockerid;
		    	var limitUsage = data.limit;

		    	$('input[name=id]').val(id);
		    	$('input[name=name]').val(name);
		    	$('input[name=activeDate]').daterangepicker({ 
		    		startDate : startDate,
		    		endDate : endDate,
		    		timePicker: false, 
		    		format: 'YYYY/MM/DD h:mm A' 
		    	});
		    	$('input[name=type]').val(type);
		    	$('select[name=rule]').val(rule);
		    	$('select[name=fromLockerId]').val(fromLockerId).trigger('change');
		    	$('input[name=rule-value]').val(ruleValue);
		    	$('input[name=fromAmount]').val(fromAmount);
		    	$('input[name=toAmount]').val(toAmount);
		    	$('input[name=code]').val(code);
		    	$('input[name=expired]').val(expired);
		    	$('select[name=status]').val(status);
		    	$('textarea[name=description]').val(description);
		    	$('textarea[name=descriptionToShared]').val(descriptionTo);
		    	$('input[name=limit]').val(limitUsage)
		    	$('html, body').animate({
		    			scrollTop: $("#box-body").offset().top
		    		},
		    		1000);
		    });
		});	
	</script>
@endsection