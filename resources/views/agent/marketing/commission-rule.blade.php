@extends('layout.main')

@section('title')
	Commission Rule
@endsection

@section('css')
	<!-- daterange picker -->
  	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
  	{{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('pageTitle')
	Commission Rule
@endsection

@section('pageDesc')
	Commission Rule and Update
@endsection

@section('content')
	<form method="post" onsubmit="return confirm('Do you really want to submit the form?');">
		{{ csrf_field() }}
		<input type="hidden" name="commissionSchemaId" value="{{ $commissionSchemaDb->id }}">
		<div class="box box-solid">
			<div class="box-body">
				<div class="row">
					<div class="col-md-5">
						<div class="form-group">
		          			<label>Type</label>
		          			@php
		          				$type = $commissionSchemaDb->type;
		          			@endphp
		          			<select class="form-control" name="type" required>
		          				@foreach ($typeForm as $element)
		          					@php
		          						$checked = '';
		          						if ($element == $type) $checked = 'selected';
		          					@endphp
		          					<option value="{{ $element }}" {{ $checked }}>{{ strtoupper($element) }}</option>
		          				@endforeach
		          			</select>
		          		</div>
		          		<input type="hidden" name="rule" value="publish">
		          		<div class="form-group">
		          			<label>Value</label>
		          			<input type="text" name="values" class="form-control" required value="{{ $commissionSchemaDb->values }}">
		          		</div>
		          		<div class="row">
		          			<div class="col-md-6">
		          				<div class="form-group">
			              			<label>Min Amount</label>
			              			<input type="number" name="minAmount" class="form-control" value="{{ $commissionSchemaDb->min_amount }}">
			              		</div>
		          			</div>
		          			<div class="col-md-6">
		          				<div class="form-group">
			              			<label>Max Amount</label>
			              			<input type="number" name="maxAmount" class="form-control" value="{{ $commissionSchemaDb->max_amount }}">
			              		</div>
		          			</div>
		          		</div>
		          		<div class="form-group">
		          			<label>Active Date</label>
		          			<input type="text" name="activeDate" class="form-control">
		          		</div>
		          		<div class="form-group">
		          			<label>Priority</label>
		          			<select class="form-control" name="priority">
		      					@php
		      						$checked = '';
		      						if ($element == $commissionSchemaDb->priority) $checked = 'checked';
		      					@endphp
		          				@foreach ($priorityForm as $element)
		          					<option value="{{ $element }}" {{ $checked }}>{{ $element }}</option>
		          				@endforeach
		          			</select>
		          		</div>
		          		<div class="form-group">
		          			<label>Status</label>
		          			<select class="form-control" name="status">
		          				<option value="1">Active</option>
		          				<option value="0">Disable</option>
		          			</select>
		          		</div>
		          		<button class="btn btn-flat btn-block btn-primary">Save</button>
					</div>
					<div class="col-md-7 border-left">
						<div class="row">
							<div class="col-md-12">
								<h4 class="pull-left">Rules</h4>
								<button class="btn btn-success pull-right" type="button" id="add-rules">Add rule</button>
							</div>
						</div>
						@php
							$ruleCount = 0;
						@endphp
						<div class="rule-list">
							@foreach ($commissionRuleDb as $cKey => $value)
								<div class="" style="background-color: #FAFAFA;border-top: 1px solid;margin-top: 5px;" id="rule-{{ $cKey }}">
									{{-- Parameter --}}
									<div class="form-group">
										<label>Parameter</label>
										<select class="form-control select2 rule-parameter" name="parameter_id[{{ $cKey }}]" rule="{{ $cKey }}">
											@foreach ($availableParam as $group)
												<optgroup label="{{ $group['paramCategory'] }}">
													@foreach ($group['paramName'] as $element)
														<option value="{{ $element['id'] }}" {{ ($value->available_parameter_id == $element["id"] ? " selected='selected' " : '' ) }} name="{{ $element['name'] }}">{{ ucfirst($element['description']) }} ({{ $element['name'] }})</option>
													@endforeach
												</optgroup>
											@endforeach
										</select>
									</div>
									{{-- Operator --}}
									<div class="form-group">
										<label>Operator</label>
										<select name='operator[{{ $cKey }}]' rows='5'   class='select2 rule-operator form-control' rule='{{ $cKey }}'>
											@foreach ($availableOperator as $key => $element)
												{!! "<option  value ='$key' ".($value->operator == $key ? " selected='selected' " : '' ).">$element ($key)</option>" !!}
											@endforeach
										</select>
									</div>
									{{-- Value --}}
									<div class="form-group">
										<label>Value <span class="btn btn-xs btn-primary add-value" rule='{{ $cKey }}'>Add</span></label>
										<div class="rule-value-list" rule='{{ $cKey }}'>
											@php
												$multiple = ['between','exist','except'];
											@endphp
											@if (in_array($value->operator, $multiple))
												@php
													$explode = json_decode($value->value);
												@endphp
												@foreach ($explode as $vkey => $element)
													<div class="row rule-value-item" rule="{{ $cKey }}" item="{{ $vkey }}">
														<div class="col-md-10">
															<input type="text" name="value[{{ $cKey }}][{{ $vkey }}]" class="form-control rule-value" value="{{ $element }}" rule='{{ $cKey }}' item="{{ $vkey }}"> 
														</div>
														<div class="col-md-1">
															<span class="btn btn-xs btn-info helper-value" rule="{{ $cKey }}" item="0">Helper</span>
														</div>
														<div class="col-md-1">
															<span class="btn btn-xs btn-warning delete-value" rule="{{ $cKey }}" item="{{ $vkey }}">x</span>
														</div>
													</div>
												@endforeach
											@else
												<div class="row rule-value-item" rule="{{ $cKey }}" item="0">
													<div class="col-md-10">
														<input type="text" name="value[{{ $cKey }}][0]" class="form-control rule-value" value="{{ $value->value }}" rule='{{ $cKey }}' item="0"> 
													</div>
													<div class="col-md-1">
														<span class="btn btn-xs btn-info helper-value" rule="{{ $cKey }}" item="0">Helper</span>
													</div>
													<div class="col-md-1">
														<span class="btn btn-xs btn-warning delete-value" rule="{{ $cKey }}" item="0">x</span>
													</div>
												</div>
											@endif
										</div>
									</div>
									<button type="button" class="btn btn-danger delete-rule" rule="{{ $cKey }}" campaignparamid="{{ $value->id }}">Delete Rule</button>
								</div>
								@php
									$ruleCount++;
								@endphp
							@endforeach
						</div>
						<input type="hidden" name="rule_count" value="{{ $ruleCount }}">
						<div id="ruleDeleted"></div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
    			</div>
    			<div class="modal-body" id="myModalBody">
    				
    			</div>
    			<div class="modal-footer">
    				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    				<button type="button" class="btn btn-primary" id="addValueRule" rule='' item=''>Tambah Value Rule</button>
    			</div>
    		</div>
    	</div>
    </div>
@endsection

@section('js')
	<!-- date-range-picker -->
	<script src="{{ asset('plugins/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	{{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
		    $('input[name=activeDate]').daterangepicker({ 
		    	timePicker: false, 
		    	format: 'YYYY/MM/DD',
		    	startDate : moment('{{ date('Y-m-d',strtotime($commissionSchemaDb->start_date)) }}'),
		    	endDate : moment('{{ date('Y-m-d',strtotime($commissionSchemaDb->end_date)) }}')
		   	})
		});	
	</script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			// add rule
			$('#add-rules').on('click', function() {
				var current_rule_count = parseInt($('input[name=rule_count]').val());
				var new_rule_count = current_rule_count +1;
				var rule_div = '<div class="" style="background-color: #FAFAFA;border-top: 1px solid;margin-top: 5px;" id="rule-'+new_rule_count+'">';
					rule_div+= '<div class="form-group">';
						rule_div += '<label>Parameter</label>';
						rule_div += '<select class="form-control select2 rule-parameter" name="parameter_id['+new_rule_count+']" rule="'+new_rule_count+'">'
							@foreach ($availableParam as $group)
								rule_div += '<optgroup label="{{ $group['paramCategory'] }}">'
									@foreach ($group['paramName'] as $element)
										rule_div += '<option value="{{ $element['id'] }}" name="{{ $element['name'] }}">{{ ucfirst($element['description']) }} ({{ $element['name'] }})</option>';
									@endforeach
								rule_div += '</optgroup>';
							@endforeach
						rule_div += '</select>';
					rule_div += '</div>';
					rule_div += '<div class="form-group">'
						rule_div += '<label>Operator</label>'
						rule_div += "<select name='operator["+new_rule_count+"]' rows='5'   class='select2 rule-operator form-control' rule='"+new_rule_count+"'>";
							@foreach ($availableOperator as $key => $element)
								rule_div += "{!! "<option  value ='$key'>$element ($key)</option>" !!}";
							@endforeach
						rule_div += '</select>';
					rule_div += '</div>';
					rule_div += '<div class="form-group">';
						rule_div += '<label>Value <span class="btn btn-xs btn-primary add-value" rule="'+new_rule_count+'">Add</span></label>';
						rule_div += '<div class="rule-value-list" rule='+new_rule_count+'>';
							rule_div += '<div class="row rule-value-item" rule="'+new_rule_count+'" item="0">';
								rule_div += '<div class="col-md-10">';
									rule_div += '<input type="text" name="value['+new_rule_count+'][0]" class="form-control rule-value" rule='+new_rule_count+' item="0"> ';
								rule_div += '</div>';
								rule_div += '<div class="col-md-1">';
									rule_div += '<span class="btn btn-xs btn-info helper-value" rule="'+new_rule_count+'" item="0">Helper</span>';
								rule_div += '</div>';
								rule_div += '<div class="col-md-1">';
									rule_div += '<span class="btn btn-xs btn-warning delete-value" rule="'+new_rule_count+'" item="0">x</span>';
								rule_div += '</div>';
							rule_div += '</div>';
						rule_div += '</div>';
					rule_div += '</div>';
					rule_div += '<button type="button" class="btn btn-danger delete-rule" rule="'+new_rule_count+'">Delete Rule</button>';
				rule_div += '</div>';

				$('div[class=rule-list]').append(rule_div);
				$('input[name=rule_count]').val(new_rule_count);
			});
			// delete rule
			$('.rule-list').on('click', '.delete-rule', function(event) {
				// get current id
				var id = $(this).attr('rule');
				if ($(this).is('[campaignparamid]')) {
				    console.log('exist');
				    // get campaign param id
				    var idDb = $(this).attr('campaignparamid');
				    // send delete request
				    var url = "{{ URL::to('agent/marketing/commission/ajaxDeleteRule') }}"+'/'+idDb;
				    console.log(url);
				    $.get( url, function( data ) {
						if (data=='ok') {
							$('div[id=rule-'+idDb+']').remove();
						}
					});
				} else {
				    console.log('not')
				}
				$('div[id=rule-'+id+']').remove();
			});

			// add new value on rule 
			$('.rule-list').on('click', '.add-value', function(event) {
				var ruleId = $(this).attr('rule');
				// get all value rule
				var countValueRule = $("input[class*=rule-value][rule="+ruleId+"]");
				// get rule operator
				var operator = $('select[class*=rule-operator][rule="'+ruleId+'"]').val();
				// if <, <= , =,  >=, > allow one rule
				// if between allow 2
				// if exist and not exist allow multiple
				var one = ["<","<=","=",">=",">"];
				var two = ["between"];
				var multiple = ["exist","except"];
				if (one.indexOf(operator)>-1) {
					console.log('one');
					if (countValueRule.length == 1) {
						alert('Only One Value Allowed');
						return;
					}
				} else if (two.indexOf(operator)>-1) {
					console.log('two');
					if (countValueRule.length == 2) {
						alert('Only Two Value Allowed');
						return;
					}
				} else if (multiple.indexOf(operator)>-1) {
					console.log('multiple');
				} else {
					alert('undefined operator');
					return;
				}
				// create element
				var newIndex = countValueRule.length;
				var element ='<div class="row rule-value-item" rule="'+ruleId+'" item="'+newIndex+'">';
						element += '<div class="col-md-10">';
							element += '<input type="text" name="value['+ruleId+']['+newIndex+']" class="form-control rule-value" rule='+ruleId+' item="'+newIndex+'"> ';
						element += '</div>';
						element += '<div class="col-md-1">';
							element += '<span class="btn btn-xs btn-info helper-value" rule="'+ruleId+'" item="'+newIndex+'">Helper</span>';
						element += '</div>';
						element += '<div class="col-md-1">';
							element += '<span class="btn btn-xs btn-warning delete-value" rule="'+ruleId+'" item="'+newIndex+'">x</span>';
						element += '</div>';
					element += '</div>';
				// add to div
				$('div[class*=rule-value-list][rule="'+ruleId+'"]').append(element);
			});

			// remove  value on rule
			$('.rule-list').on('click', '.delete-value', function(event) {
				// get rule id
				var ruleId = $(this).attr('rule');
				// get item id
				var itemId =  $(this).attr('item');
				var countValueRule = $("input[class*=rule-value][rule="+ruleId+"]");
				if (countValueRule.length<=1) {
					alert('Minimum 1 Value');
					return;
				}
				// get rule id
				var ruleId = $(this).attr('rule');
				// get item id
				var itemId =  $(this).attr('item');
				// remove div
				$('div[class*=rule-value-item][rule="'+ruleId+'"][item="'+itemId+'"]').remove();
			});

			// get help value
			$('.rule-list').on('click', '.helper-value', function(event) {
				var ruleNumber = $(this).attr('rule');
				// get selected optgroup
				var selectRule = $('select[class*=rule-parameter][rule='+ruleNumber+']');
				var selected = selectRule.find(':selected');
				var optGroup = selected.parent().attr('label');
				var selectedRuleValue = selected.attr('name');
				console.log(selected);

				// get selected operator
				var selectOperator = $('select[class*=rule-operator][rule='+ruleNumber+']');
				var selectedOperator = selectOperator.find(':selected');
				var valueOperator = selectedOperator.val();
				// get item index
				var itemNumber = $(this).attr('item');
				console.log(optGroup + " - " + valueOperator);

				// show modal based on opt group
				$.ajax({
					url: '{{ url('agent/marketing/commission/ajaxHelper') }}'+"/"+selectedRuleValue,
					type: 'GET',
					dataType: 'json',
					success: function(data){
	                  	var status = data.isSuccess;
	                  	if (status == true) {
	                  		$('#myModalLabel').html('Bantuan Untuk '+selectedRuleValue);
	                  		$('#myModal').modal('show')
	                  		var view = data.view;
	                  		$('#myModalBody').html(view);
	                  		$('.newSelect').select2();
	                  		$('#addValueRule').attr('rule', ruleNumber);
	                  		$('#addValueRule').attr('item', itemNumber);
	                  	} else {
	                  		alert(data.errorMsg);
	                  	}
	                },
	                error: function(data){
	                	console.log('error');
	                }
				})
				.done(function() {
					console.log("success");
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			});

			// add value from helper to value rule
			$('#addValueRule').on('click', function(event) {
				var ruleNumber = $(this).attr('rule');
				var value = $('#helperValue').val();
				var indexNumber = $(this).attr('item');
				console.log('input[class*=rule-value][rule='+ruleNumber+'][item='+indexNumber+']');
				console.log(value);

				$('input[class*=rule-value][rule='+ruleNumber+'][item='+indexNumber+']').val(value);
				$('#myModal').modal('hide');
			});
		});
	</script>
@endsection