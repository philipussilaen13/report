@extends('layout.main')

@section('title')
	Agent Notification
@endsection

@section('css')
	{{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('pageTitle')
	Agent Notification
@endsection

@section('pageDesc')
	Push Agent Notification
@endsection

@section('content')
	<div class="box">
		<div class="box-body">
			@if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
			<form method="post" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-5">
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="typeNotif" value="selected" checked>
											Selected Agent
										</label>
										<p class="help-block">Select one or more Agent</p>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="typeNotif" value="all">
											All Agent
										</label>
										<p class="help-block">All Agent</p>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" id="div-select-agent">
							<label>Agent</label>
							<select class="select2 form-control" multiple="true" name="user[]">
								@foreach ($userDb as $element)
									@if ($element->locker)
										<option value="{{ $element->gcm_token }}">{{ $element->locker->locker_name }} ({{ $element->name }})</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="form-group" method="post" enctype="multipart/form-data">
							<label>Title</label>
							<input type="text" name="title" class="form-control">
						</div>
						<div class="form-group">
							<label>Body</label>
							<input type="text" name="body" class="form-control">
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label>Type</label>
							<select class="form-control" name="type">
								@foreach ($typeList as $element)
									<option value="{{ $element }}">{{ $element }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Image</label>
							<input type="file" name="image">
						</div>
						<div class="form-group">
							<label>Description</label>
							<textarea name="description" class="form-control"></textarea>
						</div>
						<div class="form-group">
							<label>Reference</label>
							<input type="text" name="reference" class="form-control">
						</div>
					</div>
					<div class="col-md-2">
						<button class="btn btn-block btn-flat btn-success">Submit</button>
					</div>
				</div>
			</form>			
		</div>
	</div>
@endsection

@section('js')
	 {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>
    
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('input:radio[name=typeNotif]').change(function(event) {
				var value = $(this).val();
				if (value == 'all') {
					$('#div-select-agent').addClass('hide');
				} else if(value == 'selected') {
					$('#div-select-agent').removeClass('hide');
				}
			});
		});
	</script>
@endsection