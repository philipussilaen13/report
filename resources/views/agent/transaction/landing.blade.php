@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <style type="text/css">
        .border-right{
            border-right: 1px solid #f4f4f4
        }
    </style>
@stop

@section('title')
    Agent Transaksi
@endsection

@section('pageTitle')
    Dashboard Transaksi
@endsection

@section('pageDesc')
    Transaksi
@endsection

@section('content')
    <section class="content">
        <div class="row">
            {{-- Pulsa --}}
            <div class="col-lg-3 col-xs-12">
                <div class="info-box bg-blue box-parcel">
                    <span class="info-box-icon"><i class="ion ion-android-phone-portrait"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Pulsa</span>
                        <span class="info-box-number">Rp <span id="pulsa-sum">0</span></span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
                        <span class="progress-description">
                            <span id="pulsa-count">0</span>
                        </span>
                    </div>
                </div>
            </div>
            {{-- Tagihan --}}
            <div class="col-lg-3 col-xs-12">
                <div class="info-box bg-blue box-parcel">
                    <span class="info-box-icon"><i class="ion ion-android-bulb"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Payment</span>
                        <span class="info-box-number">Rp <span id="payment-sum">0</span></span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
                        <span class="progress-description">
                            <span id="payment-count">0</span>
                        </span>
                    </div>
                </div>
            </div>
            {{-- Belanja --}}
            <div class="col-lg-3 col-xs-12">
                <div class="info-box bg-blue box-parcel">
                    <span class="info-box-icon"><i class="ion ion-android-cart"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">PopShop</span>
                        <span class="info-box-number">Rp<span id="popshop-sum">0</span></span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
                        <span class="progress-description">
                            <span id="popshop-count">0</span>
                        </span>
                    </div>
                </div>
            </div>
            {{-- Payment --}}
            <div class="col-lg-3 col-xs-12">
                <div class="info-box bg-blue box-parcel">
                    <span class="info-box-icon"><i class="ion ion-android-mail"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Delivery</span>
                        <span class="info-box-number">Rp <span id="delivery-sum">0</span></span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
                        <span class="progress-description">
                            <span id="delivery-count">0</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="dateRange" name="dateRange" placeholder="Tanggal Transaksi">
                                    </div>
                                </div>
                                {{-- <div class="col-md-2">
                                    <select class="form-control" id="type">
                                        <option value="count">Number Of</option>
                                        <option value="sum">Amount</option>
                                    </select>
                                </div> --}}
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-flat btn-block" id="btn-submit">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="nav-tabs-custom box-graph">
                    <ul class="nav nav-tabs pull-right">
                        <li><a href="#tab_count_1" data-toggle="tab" data-type="daily">Daily</a></li>
                        <li class="active"><a href="#tab_count_2" data-toggle="tab" data-type="weekly">Weekly</a></li>
                        <li><a href="#tab_count_3" data-toggle="tab" data-type="monthly">Monthly</a></li>
                        <li class="pull-left header"><i class="fa fa-chart-line"></i> Transaction</li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab_count_1">
                            <div class="chart">
                                <canvas id="transaction-daily" style="height: 180px; width: 1072px;"></canvas>
                                <canvas id="sum-daily" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                        <div class="tab-pane active" id="tab_count_2">
                            <div class="chart">
                                <canvas id="transaction-weekly" style="height: 180px; width: 1072px;"></canvas>
                                <canvas id="sum-weekly" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_count_3">
                            <div class="chart">
                                <canvas id="transaction-monthly" style="height: 180px; width: 1072px;"></canvas>
                                <canvas id="sum-monthly" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        {{-- Transaction --}}
                        <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-maroon" id="transaction-count">0</h5>
                                    <span class="description-text text-uppercase text-black">Number Transaction</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header text-maroon">Rp <span id="transaction-sum">0</span></h5>
                                    <span class="description-text text-uppercase text-black">Amount Transaction</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header text-maroon"><span id="transaction-week-count">0</span> / Rp<span id="transaction-week-sum">0</span></h5>
                                    <span class="description-text text-uppercase text-black">Transaction per Week (<span class="week-count"></span>)</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header text-maroon"><span id="transaction-day-count">0</span> / Rp<span id="transaction-day-sum">0</span></h5>
                                    <span class="description-text text-uppercase text-black">Transaction per Day (<span class="day-count"></span>)</span>
                                </div>
                            </div>
                        </div>
                        {{-- TopUp --}}
                        <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-green" id="topup-count">0</h5>
                                    <span class="description-text text-uppercase text-black">Number TopUp</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header text-green">Rp <span id="topup-sum">0</span></h5>
                                    <span class="description-text text-uppercase text-black">Amount TopUp</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header text-green"><span id="topup-week-count">0</span> / Rp<span id="topup-week-sum">0</span></h5>
                                    <span class="description-text text-uppercase text-black">TopUp per Week(<span class="week-count"></span>)</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header text-green"><span id="topup-day-count">0</span> / Rp<span id="topup-day-sum">0</span></h5>
                                    <span class="description-text text-uppercase text-black">TopUp per Day (<span class="day-count"></span>)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-solid bg-blue box-graph">
                    <div class="box-header">
                        <h3 class="box-title">Active Agent</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="donut-active-agent" style="height: 250px; width: 1072px;"></canvas>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="description-block">
                                    <h5 class="description-header text-maroon" id="total-agent">0</h5>
                                    <span class="description-text text-uppercase text-black">Total Agent</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-maroon"><span id="transaction-count-active">0</span> / Rp<span id="transaction-sum-active">0</span></h5>
                                    <span class="description-text text-uppercase text-black">Transaction per Active</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="description-block">
                                    <h5 class="description-header text-green"><span id="topup-count-active">0</span> / Rp<span id="topup-sum-active">0</h5>
                                    <span class="description-text text-uppercase text-black">TopUp per Active</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-maroon"><span id="transaction-count-active-day">0</span> / Rp<span id="transaction-sum-active-day">0</h5>
                                    <span class="description-text text-uppercase text-black">Transaction per Active per Day</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="description-block">
                                    <h5 class="description-header text-green"><span id="topup-count-active-day">0</span> / Rp<span id="topup-sum-active-day">0</h5>
                                    <span class="description-text text-uppercase text-black">TopUp per Active per Day</span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('js')
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- ChartJS 1.0.1 --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    {{-- Animate Number --}}
    <script type="text/javascript" src="{{ asset('plugins/animate-number/jquery.animateNumber.min.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['status']))
            $('select[name=status]').val("{{ $parameter['status'] }}").trigger('change');
            @endif
            @if (!empty($parameter['type']))
            $('select[name="type[]"]').val({!! json_encode($parameter['type']) !!}).trigger('change');
            @endif
        });
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
        });
    </script>

    {{-- Chart --}}
    <script type="text/javascript">
        var dailyLabel = [];
        var dailyData = [];
        var dailySumData = [];

        var weeklyLabel = [];
        var weeklyData = [];
        var weeklySumData = [];

        var monthlyLabel = [];
        var monthlyData = [];
        var monthlySumData = [];

        var allAgent = 0;
        var activeAgent = 0;

        var type = 'count';
        var transactionChart;


        /**
         * Create Daily Chart
         * @return {[type]} [description]
         */
        function createDaily(){
            $('#transaction-daily').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Daily Chart*/
            var transactionChartCanvas = $("#transaction-daily");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : dailyLabel,
                    datasets :[{
                        label: "Daily Transaction by Number",
                        data: dailyData,
                        fill: false,
                        borderColor: '#0073b7'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
            $('#sum-daily').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Daily Chart*/
            var transactionChartCanvas = $("#sum-daily");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : dailyLabel,
                    datasets :[{
                        label: "Daily Transaction by Amount",
                        data: dailySumData,
                        fill: false,
                        borderColor: '#FF0000'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
        };

        /**
         * Create Weekly Charts
         * @return {[type]} [description]
         */
        function createWeekly(){
            $('#transaction-weekly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Weekly Chart*/
            var transactionChartCanvas = $("#transaction-weekly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : weeklyLabel,
                    datasets :[{
                        label: "Weekly Transaction by Number",
                        data: weeklyData,
                        fill: false,
                        borderColor: '#0073b7'
                    }]
                },
                 options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
            $('#sum-weekly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Weekly Chart*/
            var transactionChartCanvas = $("#sum-weekly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : weeklyLabel,
                    datasets :[{
                        label: "Weekly Transaction by Amount",
                        data: weeklySumData,
                        fill: false,
                        borderColor: '#FF0000',
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
        }

        /**
         * Create Monthly Chart
         * @return {[type]} [description]
         */
        function createMonthly(){
            $('#transaction-monthly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Monthly Chart*/
            var transactionChartCanvas = $("#transaction-monthly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : monthlyLabel,
                    datasets :[{
                        label: "Monthly Transaction by Number",
                        data: monthlyData,
                        fill: false,
                        borderColor: '#0073b7'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
            $('#sum-monthly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Monthly Chart*/
            var transactionChartCanvas = $("#sum-monthly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : monthlyLabel,
                    datasets :[{
                        label: "Monthly Transaction by Amount",
                        data: monthlySumData,
                        fill: false,
                        borderColor: '#FF0000'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
        };

        function createDonutActiveAgent(){
            var transactionChartCanvas = $("#donut-active-agent");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'pie',
                data : {
                    datasets: [{
                        data: [activeAgent, (allAgent-activeAgent)],
                        backgroundColor: ['#05FF00','#FF1700']
                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        'Active',
                        'Not Active'
                    ]
                },
                options:{
                    legend: {
                        labels: {
                            fontColor: "white"
                        }
                    },
                }
            });
        }

        $(document).ready(function() {
            createWeekly();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var type = $(this).data('type');
                if (type=='daily') {
                    createDaily();
                }
                if (type=='weekly') {
                    createWeekly();
                }
                if (type=='monthly') {
                    createMonthly();
                }
            });
            $('#type').on('change', function() {
                type = $(this).val();
                createWeekly();
            });
        });
    </script>

    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }

        function getGraph(){
            var comma_separator_number_step = $.animateNumber.numberStepFactories.separator('.')
            var formData = $('#form-transaction').serialize();
            showLoading('.box-graph','graph-loading');
            showLoading('.box-parcel','box-loading');
            $.ajax({
                url: '{{ url('agent/transaction/getAjaxGraph') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: formData,
                success: function(data){
                    if (data.isSuccess==true) {
                        dailyLabel = data.dayLabels;
                        dailyData = data.dayCountTransaction;
                        dailySumData = data.daySumTransaction;

                        weeklyLabel = data.weekLabels;
                        weeklyData = data.weekCountTransaction;
                        weeklySumData = data.weekSumTransaction;

                        monthlyLabel = data.monthLabels;
                        monthlyData = data.monthCountTransaction;
                        monthlySumData = data.monthSumTransaction;

                        allAgent = data.countAllAgent;
                        activeAgent = data.countActiveAgent;

                        createWeekly();
                        createDonutActiveAgent();

                        $('#pulsa-count').animateNumber({
                            number:data.pulsaCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#pulsa-sum').animateNumber({
                            number:data.pulsaSum,
                            numberStep: comma_separator_number_step
                        });

                        $('#payment-count').animateNumber({
                            number:data.paymentCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#payment-sum').animateNumber({
                            number:data.paymentSum,
                            numberStep: comma_separator_number_step
                        });

                        $('#popshop-count').animateNumber({
                            number:data.popshopCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#popshop-sum').animateNumber({
                            number:data.popshopSum,
                            numberStep: comma_separator_number_step
                        });

                        $('#delivery-count').animateNumber({
                            number:data.deliveryCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#delivery-sum').animateNumber({
                            number:data.deliverySum,
                            numberStep: comma_separator_number_step
                        });

                        // calculation transaction
                        $('#transaction-count').animateNumber({
                            number:data.transactionCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#transaction-sum').animateNumber({
                            number:data.transactionAmount,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_count = (data.transactionCount/(data.weekCountTransaction.length));
                        $('#transaction-week-count').animateNumber({
                            number:transaction_week_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_sum = (data.transactionAmount/(data.weekCountTransaction.length));
                        $('#transaction-week-sum').animateNumber({
                            number:transaction_week_sum,
                            numberStep: comma_separator_number_step
                        });

                        var transaction_day_count = (data.transactionCount/(data.dayCountTransaction.length));
                        $('#transaction-day-count').animateNumber({
                            number:transaction_day_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_day_sum = (data.transactionAmount/(data.dayCountTransaction.length));
                        $('#transaction-day-sum').animateNumber({
                            number:transaction_day_sum,
                            numberStep: comma_separator_number_step
                        });

                        // calculation topup
                        $('#topup-count').animateNumber({
                            number:data.topupCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#topup-sum').animateNumber({
                            number:data.topupAmount,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_count = (data.topupCount/(data.weekCountTransaction.length));
                        $('#topup-week-count').animateNumber({
                            number:transaction_week_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_sum = (data.topupAmount/(data.weekCountTransaction.length));
                        $('#topup-week-sum').animateNumber({
                            number:transaction_week_sum,
                            numberStep: comma_separator_number_step
                        });

                        var transaction_day_count = (data.topupCount/(data.dayCountTransaction.length));
                        $('#topup-day-count').animateNumber({
                            number:transaction_day_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_day_sum = (data.topupAmount/(data.dayCountTransaction.length));
                        $('#topup-day-sum').animateNumber({
                            number:transaction_day_sum,
                            numberStep: comma_separator_number_step
                        });

                        $('.day-count').html(data.dayCountTransaction.length);
                        $('.week-count').html(data.weekCountTransaction.length);

                        // calculate transaction based on active
                        var transaction_count_active = (data.transactionCount / data.countActiveAgent);
                        $('#transaction-count-active').animateNumber({
                            number:transaction_count_active,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_sum_active = (data.transactionAmount / data.countActiveAgent);
                        $('#transaction-sum-active').animateNumber({
                            number:transaction_sum_active,
                            numberStep: comma_separator_number_step
                        });

                        // calculate topup based on active
                        var topup_count_active = (data.topupCount / data.countActiveAgent);
                        $('#topup-count-active').animateNumber({
                            number:topup_count_active,
                            numberStep: comma_separator_number_step
                        });
                        var topup_sum_active = (data.topupAmount / data.countActiveAgent);
                        $('#topup-sum-active').animateNumber({
                            number:topup_sum_active,
                            numberStep: comma_separator_number_step
                        });

                        // calculate transaction based on active per day
                        var transaction_count_active_day = ((data.transactionCount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#transaction-count-active-day').animateNumber({
                            number:transaction_count_active_day,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_sum_active_day = ((data.transactionAmount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#transaction-sum-active-day').animateNumber({
                            number:transaction_sum_active_day,
                            numberStep: comma_separator_number_step
                        });

                        // calculate topup based on active per day
                        var topup_count_active_day = ((data.topupCount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#topup-count-active-day').animateNumber({
                            number:topup_count_active_day,
                            numberStep: comma_separator_number_step
                        });
                        var topup_sum_active_day = ((data.topupAmount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#topup-sum-active-day').animateNumber({
                            number:topup_sum_active_day,
                            numberStep: comma_separator_number_step
                        });

                        $('#total-agent').animateNumber({
                            number:allAgent,
                            numberStep: comma_separator_number_step
                        });

                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    alert('Failed');
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - getParcel");
            })
            .fail(function() {
                console.log("error - getParcel");
            })
            .always(function() {
                hideLoading('.box-graph','graph-loading');
                hideLoading('.box-parcel','box-loading');
                console.log("complete - getParcel");
                console.log('===================')
            });   
        }

        jQuery(document).ready(function($) {
            getGraph();
        });
    </script>

    {{-- Button Click --}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#btn-submit').on('click', function(event) {
                $('#form-transaction').attr('method', 'get');
                $('#form-transaction').submit();
            });
            $('#btn-export').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/export') }}');
                $('#form-transaction').attr('method', 'post');
                $('#form-transaction').submit();
            });
        });
    </script>
@stop