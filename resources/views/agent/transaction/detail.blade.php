@extends('layout.main')

@section('title')
    Agent Detail Transaksi
@endsection

@section('css')
    <style type="text/css">
        .changeStatus {
            cursor: pointer;
        }
    </style>
@endsection

@section('pageTitle')
    Dashboard Transaksi
@endsection

@section('pageDesc')
    Detail Transaksi
@endsection

@section('content')
    <section class="content">
        <div class="box box-widget widget-user-2">
            <div class="widget-user-header bg-aqua">
                <h3 class="widget-user-username">
                    <strong><a class="text-white"
                               href="{{ url('agent/list/detail') }}/{{ $transaction->user->locker_id }}">{{ $locker->locker_name }}</a></strong>
                </h3>
                <h5 class="widget-user-desc">{{ $transaction->user->name }}</h5>
                <h5 class="widget-user-desc">{{ $transaction->user->locker_id }}</h5>
            </div>
            <div class="box-body">
                {{-- Transaction Overall --}}
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <ul class="nav nav-stacked">
                    <li>
                        <a href="#" class="clipboard" data-clipboard-text="{{ $locker->address }}"  data-toggle="tooltip" title="Click To Copy Address">
                            Address
                            <span class="pull-right">{{ $locker->address }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="clipboard" data-clipboard-text="{{ $locker->detail_address }}"  data-toggle="tooltip" title="Click To Copy Address">
                            Address Detail
                            <span class="pull-right">{{ $locker->detail_address }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="clipboard" data-clipboard-text="{{ $transaction->reference }}"
                           data-toggle="tooltip" title="Click To Copy Reference">Reference
                            <span class="pull-right badge bg-blue" id="span-reference" >{{ $transaction->reference }}</span>
                        </a>
                    </li>
                    <li><a href="#">Type <span class="pull-right badge bg-aqua">{{ $transaction->type }}</span></a></li>
                    <li><a href="#">Transaction Amount <span
                                    class="pull-right badge bg-green">Rp {{ number_format($transaction->total_price) }}</span></a>
                    </li>
                    <li>
                        <a href="#">
                            Status
                            @if ($transaction->trashed()) 
                                <span class="pull-right badge bg-red">Dibatalkan</span>
                            @elseif ($transaction->status == 'WAITING')
                                <span class="pull-right badge bg-yellow">Menunggu Pembayaran</span>
                            @elseif ($transaction->status == 'UNPAID')
                                <span class="pull-right badge bg-yellow ">Menunggu Metode Pembayaran</span>
                            @elseif ($transaction->status=='PAID')
                                <span class="pull-right badge bg-green">Telah Dibayar</span>
                            @elseif ($transaction->status == 'EXPIRED')
                                <span class="pull-right badge bg-black text-white">Lewat Masa Bayar</span>
                            @elseif ($transaction->status == 'PENDING')
                                <span class="pull-right badge bg-yellow ">Menunggu Konfirmasi</span>
                            @elseif ($transaction->status == 'REFUND')
                                <span class="pull-right badge bg-info">Re-Fund</span>
                            @endif
                        </a>
                    </li>
                    <li><a href="#">Last Update Date <span
                                    class="pull-right badge bg-aqua">{{ date('d F Y H:i:s',strtotime($transaction->updated_at)) }}</span></a>
                    </li>
                    @if (!empty($transaction->payment))
                        <li><a href="#">Payment Method
							<span class="pull-right badge bg-blue">{{ $transaction->payment->method->name }}</span></a>
                        </li>
                    @endif
                    
                    @if (!empty($sales_transaction))
                        <li><a href="{{ route('warung-detail-transaction', ['id' =>'', 'locker_id' =>'']) }}/{{$trx_id}}/{{$locker_id}}">Sales Transaction 
							<span class="pull-right badge bg-blue">{{ $sales_transaction }}</span></a>
                        </li>
                    @endif
                </ul>

                {{-- Payment Detail --}}
                @if (!empty($transaction->payment))
                    @php
                        $dokuVAList = ['doku_mandiri_va','doku_permata_va','doku_alfamart_va'];
                        $paymentVAList = ['bni_va'];
                    @endphp
                    @if ($transaction->payment->method->code == 'transfer')
                        <h4>Payment Detail</h4>
                        <ul class="nav nav-stacked">
                            @php
                                $transferDb = \App\Models\Agent\PaymentTransfer::where('payments_id',$transaction->payment->id)->first();
                            @endphp
                            <li>
                                <a href="#">Destination
                                    <span class="pull-right badge bg-blue">{{ $transferDb->destination_bank }}</span>
                                    <span class="pull-right badge bg-blue">{{ $transferDb->destination_account }}</span>
                                </a>
                            </li>
                            <li><a href="#">Sender Bank<span
                                            class="pull-right badge bg-blue">{{ $transferDb->sender_bank }}</span></a>
                            </li>
                            <li>
                                <a href="#">Sender Name
                                    <span class="pull-right badge bg-blue">{{ $transferDb->sender_name }}</span>
                                    {{ $transferDb->remarks }}
                                </a>
                            </li>
                            @if (!empty($transferDb->picture))
                                <li>
                                    <a href="#">
                                        Picture <br>
                                        <img src="{{ \App\Http\Helpers\Helper::createImg('agent/transfer',$transferDb->picture) }}"
                                             alt="">
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @endif
                    @if ($transaction->payment->method->code == 'doku_va')
                        <h4>Payment Detail</h4>
                        <ul class="nav nav-stacked">
                            @php
                                $dokuVa = \App\Models\Agent\PaymentDokuVA::where('payments_id',$transaction->payment->id)->first();
                                $permataVa = $dokuVa->permata_va;
                                if (empty($permataVa)) {
                                    $permataVa = $dokuVa->va_number;
                                }
                                $splitPermata = str_split($permataVa,4);
                                $alfamartVa = $dokuVa->alfamart_va;
                                $splitAlfamart = str_split($alfamartVa,4);
                            @endphp
                            <li><a href="#">Time Limit<span
                                            class="pull-right badge bg-red">{{ $transaction->payment->time_limit }}</span></a>
                            </li>
                            <li>
                                <a href="#" class="clipboard" data-clipboard-text="{{ $permataVa }}"
                                   data-toggle="tooltip" title="Click To Copy Permata VA Number">Permata VA
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[3] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[2] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[1] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[0] }}</span>
                                </a>
                            </li>
                            @if (!empty($splitAlfamart[3]))
                                <li>
                                    <a href="#" class="clipboard" data-clipboard-text="{{ $alfamartVa }}"
                                       data-toggle="tooltip" title="Click To Alfamart VA Number">Alfamart VA
                                        <span class="pull-right label label-primary"
                                              style="font-size: 120%">{{ $splitAlfamart[3] }}</span>
                                        <span class="pull-right label label-primary"
                                              style="font-size: 120%">{{ $splitAlfamart[2] }}</span>
                                        <span class="pull-right label label-primary"
                                              style="font-size: 120%">{{ $splitAlfamart[1] }}</span>
                                        <span class="pull-right label label-primary"
                                              style="font-size: 120%">{{ $splitAlfamart[0] }}</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @endif
                    @if (in_array($transaction->payment->method->code, $dokuVAList))
                        <h4>Payment Detail</h4>
                        <ul class="nav nav-stacked">
                            @php
                                $dokuVa = \App\Models\Agent\PaymentDokuVA::where('payments_id',$transaction->payment->id)->first();
                                if ($dokuVa) {
                                    $permataVa = $dokuVa->va_number;
                                } else {
                                    $dokuVa = \App\Models\Agent\PaymentVirtualAccount::where('payments_id',$transaction->payment->id)->first();
                                    $permataVa = $dokuVa->va_number;
                                }
                                $splitPermata = str_split($permataVa,4);
                            @endphp
                            <li><a href="#">Time Limit<span
                                            class="pull-right badge bg-red">{{ $transaction->payment->time_limit }}</span></a>
                            </li>
                            <li>
                                <a href="#" class="clipboard" data-clipboard-text="{{ $permataVa }}"
                                   data-toggle="tooltip" title="Click To Copy Permata VA Number">{{ $dokuVa->va_type }}
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[3] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[2] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[1] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitPermata[0] }}</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                    @if (in_array($transaction->payment->method->code, $paymentVAList))
                        <h4>Payment Detail</h4>
                        <ul class="nav nav-stacked">
                            @php
                                $vaDb = \App\Models\Agent\PaymentVirtualAccount::where('payments_id',$transaction->payment->id)->first();
                                $vaNumber = $vaDb->va_number;
                                $splitVaNumber = str_split($vaNumber,4);
                            @endphp
                            <li><a href="#">Time Limit<span
                                            class="pull-right badge bg-red">{{ $transaction->payment->time_limit }}</span></a>
                            </li>
                            <li>
                                <a href="#" class="clipboard" data-clipboard-text="{{ $vaNumber }}"
                                   data-toggle="tooltip" title="Click To Copy VA Number">{{ $vaDb->va_type }}
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitVaNumber[3] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitVaNumber[2] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitVaNumber[1] }}</span>
                                    <span class="pull-right label label-info"
                                          style="font-size: 120%">{{ $splitVaNumber[0] }}</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                @endif
                @if (count($transaction->histories)>0)
                    <h4>
                        Rekam Transaksi
                    </h4>
                    <table class="table">
                        @php
                            $histories = $transaction->histories;
                        @endphp
                        @foreach ($histories as $element)
                            <tr>
                                <td>{{ $element->user }}</td>
                                <td>{{ $element->status }}</td>
                                <td>{{ $element->remarks }}</td>
                                <td>{{ date('Y-m-d H:i:s',strtotime($element->created_at)) }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endif
                <hr>
                <h4>Transaksi Item</h4>
                {{-- Transaction Items --}}
                <table class="table">
                    <thead>
                    <tr>
                        <th>Items</th>
                        <th>Tipe</th>
                        <th>Description</th>
                        <th>Reference</th>
                        <th>Comission</th>
                        <th>Harga</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $isFailed = false;
                    @endphp
                    @foreach ($transaction->items as $item)
                        <tr>
                            <td>
                                <span>{{ $item->name }}</span>
                                @if ($item->type == 'popshop')
                                    <br> <b>Delivery Status</b> 
                                    @if (empty($item->delivery_status))
                                        <span class="label label-default changeStatus" data-id="{{ $item->id }}" data-name="{{ $item->name }}">Not Available</span>
                                    @elseif ($item->delivery_status == 'on_process')
                                        <span class="label label-info changeStatus" data-id="{{ $item->id }}" data-name="{{ $item->name }}">On Process</span>
                                    @elseif ($item->delivery_status == 'undelivered')
                                        <span class="label label-danger changeStatus" data-id="{{ $item->id }}" data-name="{{ $item->name }}">Un-Delviered</span>
                                    @elseif ($item->delivery_status == 'delivered')
                                        <span class="label label-success changeStatus" data-id="{{ $item->id }}" data-name="{{ $item->name }}">Delivered</span>
                                    @endif
                                @endif
                            </td>
                            <td>{{ $item->type }}</td>
                            <td>
                                @if (!empty($item->params))
                                    @php
                                        $description = json_decode($item->params);
                                    @endphp
                                    @foreach ($description as $key=> $element)

                                    	@if(!is_array($element))
                                        	{{ $key }} : {{ $element }} <br>
                                       	@else
                                            @if($key != 'product_item')
                                                @foreach ($element as $key_ => $value)
                                                    {{$key_}} : {{$value}}
                                                @endforeach
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                <span class="label label-primary">{{ $item->status_push }}</span> {{ $item->reference }}
                                <br>
                                @if ($item->status_push =='FAILED')
                                    @php
                                        $isFailed = true;
                                        $data = [];
                                        $data[] = "data-id=$item->id";
                                        if (!empty($item->params)) {
                                            foreach ($description as $key => $value) {
                                                $value = str_replace(' ', '_', $value);
                                                if($key != 'product_item') {
                                                    $data[] = "data-$key=$value";
                                                }
                                            }
                                        }
                                    @endphp
                                    @if ($transaction->status == 'PAID' && ($transaction->type=='purchase' || $transaction->type=='payment'))
                                        <button class="btn btn-flat btn-danger btn-repush" {{ implode(' ', $data) }}>
                                            REPUSH
                                        </button>
                                    @else
                                        <button class="btn btn-flat btn-danger" {{ implode(' ', $data) }} disabled>
                                            REPUSH
                                        </button>
                                    @endif
                                @endif
                                @if (!empty($item->serial_number))
                                    <strong>Serial Number / Token</strong><br>
                                    {{ $item->serial_number }}
                                @endif
                            </td>
                            <td>
                                @if (!empty($item->commission_schemas_id))
                                    @php
                                        $commissionDb = \App\Models\Agent\CommissionSchema::find($item->commission_schemas_id);
                                    @endphp
                                    @if ($commissionDb)
                                        @if ($commissionDb->type == 'fixed')
                                            {{ number_format($commissionDb->values) }}
                                        @elseif($commissionDb->type == 'percent')
                                            {{ $commissionDb->values }} %
                                        @endif
                                    @endif
                                @endif
                            </td>
                            <td>Rp {{ number_format($item->price) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            @if(\App\Models\Privilege::hasAccess('agent/transaction/refund'))
                                @if ($transaction->status == 'PAID' && ($transaction->type=='purchase' || $transaction->type=='payment' || $transaction->type=='delivery'))
                                    <button class="btn btn-flat btn-warning" data-toggle="modal"
                                            data-target="#modalRefund">Refund
                                    </button>
                                @else
                                    <button class="btn btn-flat btn-warning" disabled>Refund</button>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            @if(\App\Models\Privilege::hasAccess('agent/transaction/confirmBankTransfer'))
                                @if (!empty($transaction->payment))
                                    @if ($transaction->payment->method->code == 'transfer' && $transaction->status=='PENDING')
                                        <button class="btn btn-flat btn-success" data-toggle="modal" data-target="#modalConfirm">Confirm
                                        </button>
                                    @else
                                        <button class="btn btn-flat btn-success" disabled>Confirm Transfer</button>
                                    @endif
                                @else 
                                    <button class="btn btn-flat btn-success" disabled>Confirm Transfer</button>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            @if(\App\Models\Privilege::hasAccess('agent/transaction/sendReceipt'))
                                @if ($transaction->status == 'PAID' && ($transaction->type=='payment'))
                                    <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modalSendReceipt">
                                        Send Receipt
                                    </button>
                                @endif
                            @endif
                        </div>
                    </div>
                     <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            @if(\App\Models\Privilege::hasAccess('agent/transaction/changeDeliveryStatus'))
                                <button id="btn-change-all-delivery-status" class="btn btn-flat btn-danger" onclick="changeAllDeliveryStatus()">Change All Delivery Status</button>
                            @endif
                            @if(\App\Models\Privilege::hasAccess('agent/transaction/addRemarks'))
                                <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modalAddRemarks">
                                    Add Remarks
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if ($isFailed && $transaction->status == 'PAID' && ($transaction->type=='purchase' || $transaction->type=='payment'))
        <div class="modal fade" id="modalRepush" tabindex="-1" role="dialog" aria-labelledby="modalRepushLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalRepushLabel">Repush Item</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/transaction/repush') }}" id="form-repush">
                            {{ csrf_field() }}
                            <input type="hidden" name="itemId">
                            <div id="itemDetail">

                            </div>
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea class="form-control" rows="4" name="remarks"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-repush">Repush</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($transaction->status == 'PAID' && ($transaction->type=='purchase' || $transaction->type=='payment' || $transaction->type =='delivery'))
        <div class="modal fade" id="modalRefund" tabindex="-1" role="dialog" aria-labelledby="modalRefundLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalRefundLabel">Refund Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/transaction/refund') }}" id="form-refund">
                            {{ csrf_field() }}
                            <input type="hidden" name="transactionRef" value="{{ $transaction->reference }}">
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea class="form-control" rows="4" name="remarks"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-refund">Refund</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($transaction->status == 'PAID' && ($transaction->type=='payment'))
        <div class="modal fade" id="modalSendReceipt" tabindex="-1" role="dialog" aria-labelledby="modalSendReceiptLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalSendReceiptLabel">Re Send Receipt</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/transaction/sendReceipt') }}" id="form-receipt">
                            {{ csrf_field() }}
                            <input type="hidden" name="transactionRef" value="{{ $transaction->reference }}">
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" name="phone" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-receipt">Send Receipts</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (!empty($transaction->payment))
        @if ($transaction->payment->method->code == 'transfer' && $transaction->status=='PENDING')
            <div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modalConfirmLabel">Confirm Transaction</h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{ url('agent/transaction/confirmBankTransfer') }}"
                                  id="form-confirm">
                                {{ csrf_field() }}
                                <input type="hidden" name="transactionRef" value="{{ $transaction->reference }}">
                                <table class="table">
                                    <tr>
                                        <td><strong>Nama Agent</strong></td>
                                        <td>{{ $locker->locker_name }} ({{ $transaction->user->locker_id }})</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Transaction Amount</strong></td>
                                        <td>Rp {{ number_format($transaction->total_price) }}</td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="btn-confirm">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif

    @if(\App\Models\Privilege::hasAccess('agent/transaction/addRemarks'))
        <div class="modal fade" id="modalAddRemarks" tabindex="-1" role="dialog" aria-labelledby="modelAddRemarksLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modelAddRemarksLabel">Add Remarks</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/transaction/addRemarks') }}" id="form-receipt">
                            {{ csrf_field() }}
                            <input type="hidden" name="transactionRef" value="{{ $transaction->reference }}">
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea name="remarks" class="form-control" rows="5">{{ $transaction->remarks }}</textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-receipt">Add Remarks</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(\App\Models\Privilege::hasAccess('agent/transaction/changeDeliveryStatus'))
        <div class="modal fade" id="modalChangeStatus" tabindex="-1" role="dialog" aria-labelledby="modalChangeStatusLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalChangeStatusLabel">Change Delivery Status</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/transaction/changeDeliveryStatus') }}" id="form-changeStatus">
                            {{ csrf_field() }}
                            <input type="hidden" name="transactionRef" value="{{ $transaction->reference }}">
                            <input type="hidden" name="transactionItemId">
                            <div class="form-group">
                                <label>Product</label>
                                <input type="text" name="itemName" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>Delivery Status</label>
                                <select class="form-control" name="deliveryStatus">
                                    <option value="on_process">On Process</option>
                                    <option value="undelivered">Un-Delivered</option>
                                    <option value="delivered">Delivered</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-changeStatus">Change Status</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade" id="modalChangeAllDeliveryStatus" tabindex="-1" role="dialog" aria-labelledby="modalAllDeliveryLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalRepushLabel">Change All Delivery Status</h4>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" rows="4" id="remarks-status-delivery-item" name="remarks-status-delivery-item"></textarea>
                </div>
                <div class="form-group" style="margin-left: 15px; margin-right: 15px">
                    <label>Delivery Status</label>
                    <select class="form-control" id="dropdown-delivery-status" name="dropdown-delivery-status">
                        <option value="on_process">On Process</option>
                        <option value="undelivered">Un-Delivered</option>
                        <option value="delivered">Delivered</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-update-status" onclick="updateStatusDeliveryItem()">Update Status</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('js/clipboard.min.js') }}"></script>
    {{-- Clipboard JS --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var clipboard = new Clipboard('.clipboard');
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('a[class*="clipboard"]').click(function (event) {
                event.preventDefault();
            });
            $('#btn-refund').on('click', function () {
                $('#form-refund').submit();
            });
            $('#btn-confirm').on('click', function (event) {
                $('#form-confirm').submit();
            });
            $('.btn-repush').on('click', function (event) {
                var data = $(this).data();
                var html = '';
                $.each(data, function (index, val) {
                    html += "<strong>" + index + "</strong> : " + val + "<br>"
                });
                $('#itemDetail').html(html);
                $('input[name=itemId]').val(data.id);
                $('#modalRepush').modal('show');
            });
            $('#btn-repush').on('click', function (event) {
                $('#form-repush').submit();
            });
            $('#btn-receipt').on('click', function(event) {
                $('#form-receipt').submit();
            });

            $('span[class*=changeStatus]').on('click', function(event) {
                $('#modalChangeStatus').modal('show');
                // get data
                var data = $(this).data();
                var productName = data.name;
                var productId = data.id;

                // set value
                $('input[name=itemName]').val(productName);
                $('input[type=hidden][name=transactionItemId]').val(productId);
            });
             $('#btn-changeStatus').on('click', function(event) {
                $('#form-changeStatus').submit();
            });
        });

        var transactionItems = {!! json_encode($transaction->items->toArray()) !!};
        function changeAllDeliveryStatus() {

            var text = '';
            var space = '&nbsp&nbsp&nbsp&nbsp';

            var transactionItemsName = [];
            transactionItems.forEach(function (value) {
                // transactionItemsName.push(value.name);
                // text += '<span class="label span_privilege_module" style="margin-top: 10px; font-size: 14px">' + value.name +'</span>';
                // text += space;
                text += value.name;
                text += ', '
            });

            $('#remarks-status-delivery-item').html(text);

            $('#modalChangeAllDeliveryStatus').modal('show');

            console.log(transactionItemsName);
        }
        
        function updateStatusDeliveryItem() {
            var transaction_id = $('#span-reference')[0].innerText;
            var transaction_delivery_status = $('#dropdown-delivery-status')[0].value;

            console.log(transaction_id);
            console.log(transaction_delivery_status);

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxChangeAllDeliveryStatus') }}',
                data: {
                    _token : '{{ csrf_token() }}',
                    transaction_ref : transaction_id,
                    transaction_status : transaction_delivery_status
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        window.location.reload();

                    }  else {
                        alert(data.errorMsg);
                    }

                    hideLoading('.content', 'content');
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                }
            })
                .done(function() {
                    console.log("success - download");
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                })
                .fail(function() {
                    console.log("error - download");
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                    console.log("complete - download");
                    console.log('===================');
                });
        }
        
    </script>

@endsection