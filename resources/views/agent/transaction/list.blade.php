@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@stop

@section('title')
    Agent Transaction
@endsection

@section('pageTitle')
    Transaction Dashboard
@endsection

@section('pageDesc')
    List of All Agent Transactions
@endsection

@section('content')
    <section class="content">
        {{-- Table --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body border-radius-none">
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" id="dateRange" name="dateRange" placeholder="Tanggal Transaksi">
                                            </div>
                                        </div>
                                        {{--<div class="col-md-3">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select class="form-control select2" name="typeeee">--}}
                                                    {{--<option value="">Tipe</option>--}}
                                                    {{--<option value="agent">Agent</option>--}}
                                                    {{--<option value="warung">Warung</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="form-control select2" name="locker">
                                                    <option value="">Semua Agen</option>
                                                    @foreach ($lockerData as $element)
                                                        @php
                                                            $selected = '';
                                                            if (isset($parameter['locker'])) {
                                                                if ($parameter['locker'] == $element->locker_id) {
                                                                    $selected = "selected='selected'";
                                                                }
                                                            }
                                                        @endphp
                                                        <option value="{{ $element->locker_id }}" {{ $selected }}>{{ $element->locker_name }} ({{ $element->locker_id }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="form-control select2" name="status">
                                                    <option value="">Semua Status</option>
                                                    <option value="PAID">Telah Dibayar</option>
                                                    <option value="WAITING">Menunggu Pembayaran</option>
                                                    <option value="PENDING">Menunggu Konfirmasi</option>
                                                    <option value="REFUND">Re-Fund</option>
                                                    <option value="EXPIRED">Lewat Masa Bayar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" name="transactionId" class="form-control" value="{{$transactionId}}"
                                                       placeholder="Transaksi ID">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" name="p_sales_transaction" class="form-control" value="{{$p_sales_transaction}}"
                                                       placeholder="Sales Transaction">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="form-control select2" name="city">
                                                    <option value="">Semua Kota</option>
                                                    @foreach ($cities as $city)
                                                        @php
                                                            $selected = '';
                                                            if (isset($parameter['city'])) {
                                                                if ($parameter['city'] == $city->id) {
                                                                    $selected = "selected='selected'";
                                                                }
                                                            }
                                                        @endphp
                                                        <option value="{{ $city->id }}" {{ $selected }}>{{ $city->city_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control select2" name="type[]" multiple="multiple">
                                                    <optgroup label="Pembelian">
                                                        <option value="pulsa">Pulsa</option>
                                                        <option value="popshop">PopShop</option>
                                                    </optgroup>
                                                    <optgroup label="Tagihan">
                                                        <option value="electricity">PLN Token</option>
                                                        <option value="electricity_postpaid">PLN Pasca Bayar</option>
                                                        <option value="bpjs_kesehatan">BPJS Kesehatan</option>
                                                        <option value="telkom_postpaid">Telkom PostPaid</option>
                                                        <option value="pdam">PDAM</option>
                                                    </optgroup>
                                                    <optgroup label="Reward">
                                                        <option value="commission">Komisi</option>
                                                        <option value="reward">Reward</option>
                                                        <option value="referral">Referral</option>
                                                    </optgroup>
                                                    <optgroup label="Deposit">
                                                        <option value="refund">Re-Fund</option>
                                                        <option value="topup">Top Up</option>
                                                        <option value="deduct">Manual Deduct</option>
                                                    </optgroup>
                                                    <option value="delivery">Pengiriman</option>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse" id="collapseExample">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="itemReference" class="form-control"
                                                           placeholder="Item Reference">
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="itemParam" class="form-control"
                                                           placeholder="Phone Number, SKU">
                                                    <label>
                                                        <small>Pencarian Dengan Nomor HP atau SKU akan memakan waktu
                                                            lama
                                                        </small>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="col-md-6 no-padding">
                                        <button class="btn btn-info btn-flat btn-block" type="button" id="btn-submit">Cari</button>
                                    </div>
                                    <div class="col-md-6 no-padding">
                                        <button class="btn btn-success btn-flat btn-block" type="button" id="btn-export">Excel</button>
                                    </div>
                                    <button class="btn btn-primary btn-flat btn-block" type="button" data-toggle="collapse"
                                            data-target="#collapseExample" aria-expanded="false"
                                            aria-controls="collapseExample">Advance Search
                                    </button>
                                    <div class="col-md-6 no-padding">
                                        <button class="btn btn-success btn-flat" type="button" id="btn-export-performance">Performance</button>
                                    </div>
                                    <div class="col-md-6 no-padding">
                                        <button class="btn btn-flat bg-orange" type="button" id="btn-export-non-group">Non Group</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover table-striped" cellspacing="0" width="100%" id="transaction-table">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Transaksi ID</th>
                                    <th data-priority="1">Nama Agen</th>
                                    <th>Kota</th>
                                    <th>Tipe</th>
                                    <th>Sales Transaction</th>
                                    <th>Item</th>
                                    <th>Status</th>
                                    <th data-priority="2">Tanggal Transaksi</th>
                                    <th data-priority="3">Jumlah Transaksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $lockerData = collect($lockerData);
                                @endphp
                                @foreach ($allTransaction as $key => $element)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            <a href="{{ url('agent/transaction/detail') }}/{{ $element->reference }}">
                                                {{ $element->reference }}
                                            </a>
                                        </td>
                                        <td>
                                            <strong>{{ $element->user->locker->locker_name }}</strong><br>
                                            <a href="{{ url('agent/list/detail') }}/{{ $element->user->locker_id }}">{{ $element->user->locker_id }}</a>
                                        </td>
                                        <td>
                                            {{ $element->city_name }}<br>
                                        </td>
                                        <td>
                                            @php
                                                $types = [];
                                                foreach ($element->items as $item){
                                                    if (!in_array($item->type, $types)) $types[] = $item->type;
                                                }
                                            @endphp
                                            @if (!empty($types))
                                                {{ implode(',', $types) }}
                                            @else
                                                {{ $element->type }}
                                            @endif
                                        </td>
                                        <td>
                                        	@if( $sales_transaction[$element->reference] !== '-' )
                                        		<a href="{{ route('warung-detail-transaction', ['id' =>'', 'locker_id' =>'']) }}/{{$sales_transaction[$element->reference][1]}}/{{$sales_transaction[$element->reference][2]}}">
                                        		{{ $sales_transaction[$element->reference][0] }}
                                        		</a>
                                        	@else
                                               {{ $sales_transaction[$element->reference] }}
                                            @endif
                                        </td>
                                        <td>
                                            <ul>
                                                @foreach ($element->items as $item)
                                                    <li>
                                                        <strong>{{ \App\Http\Helpers\Helper::trimText($item->name,25) }}</strong>
                                                        Rp {{ number_format($item->price) }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>
                                            @if ($element->trashed())
                                                <span class="label label-danger">Dibatalkan</span>
                                            @elseif ($element->status == 'WAITING')
                                                <span class="label label-warning">Menunggu Pembayaran</span>
                                            @elseif ($element->status == 'UNPAID')
                                                <span class="label label-warning">Menunggu Metode Pembayaran</span>
                                            @elseif ($element->status == 'PENDING')
                                                <span class="label label-warning">Menunggu Konfirmasi</span>
                                            @elseif ($element->status=='PAID')
                                                <span class="label label-success">Telah Dibayar</span>
                                            @elseif ($element->status == 'EXPIRED')
                                                <span class="label bg-black text-white">Lewat Masa Bayar</span>
                                            @elseif ($element->status == 'PENDING')
                                                <span class="label label-warning">Menunggu Konfirmasi</span>
                                            @elseif ($element->status == 'REFUND')
                                                <span class="label label-warning">Re-Fund</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ date('d F Y H:i:s',strtotime($element->created_at)) }}
                                        </td>
                                        <td>Rp {{ number_format($element->total_price) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $allTransaction->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('js')
    {{-- Morris.js charts --}}
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/morris.js/morris.min.js') }}"></script>
    {{-- ChartJS 1.0.1 --}}
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['status']))
            $('select[name=status]').val("{{ $parameter['status'] }}").trigger('change');
            @endif
            @if (!empty($parameter['type']))
            $('select[name="type[]"]').val({!! json_encode($parameter['type']) !!}).trigger('change');
            @endif
        });
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
        });
    </script>

    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

    </script>

    {{-- Button Click --}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#btn-submit').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/list') }}');
                $('#form-transaction').attr('method', 'get');
                $('#form-transaction').submit();
            });
            $('#btn-export').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/export') }}');
                $('#form-transaction').attr('method', 'post');
                $('#form-transaction').submit();
            });
            $('#btn-export-performance').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/exportAgentPerformance') }}');
                $('#form-transaction').attr('method', 'post');
                $('#form-transaction').submit();
            });
            $('#btn-export-non-group').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/exportNonGroup') }}');
                $('#form-transaction').attr('method', 'post');
                $('#form-transaction').submit();
            });
        });
    </script>
@stop