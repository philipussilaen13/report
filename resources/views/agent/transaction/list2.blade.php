@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@stop

@section('title')
    Agent Transaction
@endsection

@section('pageTitle')
    Transaction Dashboard
@endsection

@section('pageDesc')
    List of All Agent Transactions
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-filter">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-1">
                            <h3>Filter</h3>
                        </div>
                        <div class="col-md-10">
                            <h3 id="title-filter"></h3>
                        </div>
                        <div class="col-md-1 pull-right">
                            <button id="btn-show-hide-filter" type="button" class="btn btn-danger btn-lg pull-right" onclick="showHideBox()" style="margin-top: 20px">
                                Show Filter
                            </button>
                        </div>
                    </div>
                </div>
                <div id="box-filter">
                    {{-- FIRST ROW --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>User Type</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="type-agent-warung" name="type-agent-warung" style="width: 100%">
                                        <option value="">All Type</option>
                                        <option value="agent">Agent</option>
                                        <option value="warung">Warung</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Store Name/Agent ID</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="store-agentid-name" name="store-agentid-name" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Status</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="status" name="status" style="width: 100%">
                                        <option value="">All Status</option>
                                        <option value="PAID">Telah Dibayar</option>
                                        <option value="WAITING">Menunggu Pembayaran</option>
                                        <option value="PENDING">Menunggu Konfirmasi</option>
                                        <option value="REFUND">Re-Fund</option>
                                        <option value="EXPIRED">Lewat Masa Bayar</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- SECOND ROW --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Transaction Date</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="daterange-transaction" name="dateRange-transaction" placeholder="Transaction Date">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Transaction ID</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="transaction-id" name="transaction-id" placeholder="Transaction ID" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Kota</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="city" name="city" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Transaction Type --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Transaction Type</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="type-transaction" name="transaction-type" multiple="multiple" style="width: 100%">
                                        <optgroup label="Pembelian">
                                            <option value="pulsa">Pulsa</option>
                                            <option value="popshop">PopShop</option>
                                        </optgroup>
                                        <optgroup label="Tagihan">
                                            <option value="electricity">PLN Token</option>
                                            <option value="electricity_postpaid">PLN Pasca Bayar</option>
                                            <option value="bpjs_kesehatan">BPJS Kesehatan</option>
                                            <option value="telkom_postpaid">Telkom PostPaid</option>
                                            <option value="pdam">PDAM</option>
                                        </optgroup>
                                        <optgroup label="Reward">
                                            <option value="commission">Komisi</option>
                                            <option value="reward">Reward</option>
                                            <option value="referral">Referral</option>
                                        </optgroup>
                                        <optgroup label="Deposit">
                                            <option value="refund">Re-Fund</option>
                                            <option value="topup">Top Up</option>
                                            <option value="deduct">Manual Deduct</option>
                                        </optgroup>
                                        <option value="delivery">Pengiriman</option>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Sales Transaction</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="sales-transaction" name="sales-transaction" placeholder="Sales Transaction" style="width: 100%">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Item Reference --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Item Reference</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" id="item-reference" name="item-reference" class="form-control" placeholder="Input reference code from other system" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-left: 15px">
                            <h5>Phone Number/Meter Number</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" id="item-param" name="item-param" class="form-control" placeholder="Phone Number/Meter Number" style="width: 100%">
                                </div>
                            </div>
                            <div class="row" style="padding-left: 15px; margin-top: -20px">
                                <h5>Searching using Phone Number or SKU will take a long time</h5>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer" style="margin-left: 15px; margin-right: 15px;">
                        <div class="form-group">
                            <button id="btn-filter-submit" type="button" class="btn btn-primary btn-sm" onclick="submitFilter()" style="margin-top: 15px; margin-left: 10px">
                                Filter
                            </button>
                            <button id="btn-reset-filter" type="button" class="btn btn-danger btn-sm" onclick="resetFilter()" style="margin-top: 15px">
                                Reset
                            </button>
                        </div>
                    </div>
                </div>
                {{--<div id="box-filterr" class="row">`--}}
                {{----}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    {{-- Table List --}}
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-transaction-table">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-4">
                            <h3>Transaction List</h3>
                        </div>
                        <div class="form-group">
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#modal-download-nongroup" style="margin-top: 20px">
                                Non-Group
                            </button>
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#modal-download-performance" style="margin-top: 20px; margin-right: 10px">
                                Performance
                            </button>
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" onclick="getAjaxTransactionDownload()" style="margin-top: 20px; margin-right: 10px">
                                Download
                            </button>
                        </div>
                    </div>
                    <div id="top-all-type" class="box-body">
                        <table id="table-transactions-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>Transaction ID</th>
                                <th>Agent Name</th>
                                <th>Kota</th>
                                <th>Type Product</th>
                                <th>Sales Transaction</th>
                                <th>Item Product</th>
                                <th>Status</th>
                                <th>Transaction Date</th>
                                <th>Transaction Amount</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-download-performance" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Download Transaction Performance</h3>
                </div>
                <div class="modal-body">
                    <form id="form-transaction">
                        {{ csrf_field() }}
                        <div>
                            <h3>Please Select Transaction Date Range</h3>
                        </div>
                        <div style="margin-top: 30px">
                            <div>
                                <h5>Start Date - End Date</h5>
                                <div style="margin-left: 1px">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="daterange-transaction-performance" name="daterange-transaction-performance" placeholder="Date">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="btn-export-performance" type="button" class="btn btn-primary btn-sm pull-right" onclick="getAjaxDownloadPerformance()" style="margin-top: 20px; margin-right: 10px">
                        Download
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-download-nongroup" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Download Transaction Non-Group</h3>
                </div>
                <div class="modal-body">
                    <form id="form-transaction">
                        {{ csrf_field() }}
                        <div>
                            <h3>Please Select Transaction Date Range</h3>
                        </div>
                        <div style="margin-top: 30px">
                            <div>
                                <h5>Start Date - End Date</h5>
                                <div style="margin-left: 1px">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="daterange-transaction-nongroup" name="daterange-transaction-nongroup" onclick="" placeholder="Date">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="btn-export-non-group" type="button" class="btn btn-primary btn-sm pull-right" onclick="getAjaxDownloadNonGroup()" style="margin-top: 20px; margin-right: 10px">
                        Download
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>


    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            // initFilterData();
            getAjaxFilterDropdownDataTransaction();

            // // Hide the Modal
            // $("#btn-export-performance").click(function(){
            //     $("#modal-download-performance").modal("hide");
            // });
            //
            // // Hide the Modal
            // $("#btn-export-non-group").click(function(){
            //     $("#modal-download-nongroup").modal("hide");
            // });
        });

        function submitFilter() {
            getAjaxFilterTransaction(getDataForFilter());
            // console.log(getDataForFilter());
        }

        function resetFilter() {

            transactionDateRange = '';

            $('#type-agent-warung').val('').trigger('change');

            // $('#store-agentid-name').val("").trigger('change');
            insertDataToDropdownSelect2(dataDropDown);

            $('#status').val('').trigger('change');

            $('#daterange-transaction').val('');
            $('#transaction-id').val('');
            $('#sales-transaction').val('');
            $('#type-transaction').val('val').trigger('change');
            $('#item-reference').val('');
            $('#item-param').val('');

            resetFilterTitle();

        }

        function getAjaxDownloadPerformance() {
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-transaction-table', 'box-transaction-table');

            // $('#modal-download-performance').modal('hide');

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxTransactionDownloadPerformance') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    dateRange : transactionPerformanceDateRange
                },
                type: 'POST',
                success: function(data){
                    if (data.isSuccess === true) {
                        // console.log(data.data);

                        window.location = '{{ url("ajax/download") }}' + '?file=' + data.data.link;

                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function getAjaxDownloadNonGroup() {
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-transaction-table', 'box-transaction-table');

            // $('#modal-download-nongroup').modal('hide');

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxTransactionDownloadNonGroup') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    dateRange : transactionNonGroupDateRange
                },
                type: 'POST',
                success: function(data){
                    if (data.isSuccess === true) {
                        // console.log(data.data);

                        window.location = '{{ url("ajax/download") }}' + '?file=' + data.data.link;

                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function getAjaxFilterDropdownDataTransaction() {
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-transaction-table', 'box-transaction-table');

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxFilterDropdownDataTransaction') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        insertDataToDropdownSelect2(data.data);
                    }  else {
                        alert(data.errorMsg);
                    }

                    getAjaxFilterTransaction(getDataForFilter());
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    // hideLoading('.box-filter', 'box-filter');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    // hideLoading('.box-filter', 'box-filter');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    // hideLoading('.box-filter', 'box-filter');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    // hideLoading('.box-filter', 'box-filter');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        var tableAllType;
        function getAjaxFilterTransaction(value) {
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-transaction-table', 'box-transaction-table');

            $.ajax({
                url: '{{ url('agent/ajax/getAjaxFilterTransaction') }}',
                data: {
                    _token: value.token,
                    type_agent_warung 		: value.type_agent_warungg,
                    store_agentid_name 		: value.store_agentid_namee,
                    status 					: value.statuss,
                    daterange_transaction 	: value.daterange_transactionn,
                    transaction_id 			: value.transaction_idd,
                    sales_transaction 		: value.sales_transaction,
                    city_id			 		: value.city,
                    type_transaction 		: value.type_transactionn,
                    item_reference 			: value.item_referencee,
                    item_param 				: value.item_paramm
                },
                type: 'POST',
                success: function(data){
                    if (data.isSuccess === true) {
                        // console.log(data.data);

                        if (data.data.resultList.length > 0) {
                            tableAllType = $('#table-transactions-list').DataTable({
                                data          : data.data.resultList,
                                'destroy'     : true,
                                'retreive'    : true,
                                'paging'      : true,
                                'searching'   : true,
                                'ordering'    : false,
                                'autoWidth'   : false,
                                'responsive'  : true,
                                'processing'  : true,
                                'scrollY'     : 500,
                                'deferRender' : true,
                                'scrollX'     : true,
                                "pageLength"  : 50,
                                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                'order': [[ 1, "asc" ]],
                                columnDefs: [
                                    {
                                        "targets": 0,
                                        "data": "download_link",
                                        "render": function ( data, type, full, meta ) {
                                            // console.log(data);
                                            // console.log('full');
                                            // console.log(full);
                                            return '<a href=" {{ url('agent/transaction/detail') }}/'+full.reference+'"> '+full.reference+' </a>';
                                        }
                                    },
                                    {
                                        "targets": 1,
                                        "render": function ( data, type, full, meta ) {
                                            return '<b>'+full.user.locker.locker_name+'</b><br><a href=" {{ url('agent/list/detail') }}/' +full.locker_id +' "> '+ full.locker_id +' </a>'
                                        }
                                    },
                                    {
                                        "targets": 2,
                                        "render": function ( data, type, full, meta ) {
                                            return full.city_name;
                                        }
                                    },
                                    {
                                        "targets": 3,
                                        "render": function ( data, type, full, meta ) {
                                            var types = [];
                                            var tempTypes = '';
                                            if (full.items.length > 0) {
                                                full.items.forEach(function(data){
                                                    if (tempTypes === '') {
                                                        tempTypes = data.type;
                                                        types.push(data.type);
                                                    } else {
                                                        if (data.type !== tempTypes) {
                                                            types.push(data.type);
                                                        }
                                                    }
                                                });
                                            } else {
                                                types.push(full.type);
                                            }

                                            var typesUnique = types.filter( onlyUnique );
                                            return types;
                                        }
                                    },
                                    {
                                        "targets": 4,
                                        "render": function ( data, type, full, meta ) {
                                            if (full.sales_transaction.length > 0 && full.sales_transaction[0] !== '-') {
                                                return "<a href='{{ route("warung-detail-transaction", ['id' =>'', 'locker_id' =>'']) }}/"+full.sales_transaction[1]+"/"+full.sales_transaction[2]+"'>"+ full.sales_transaction[0] +"</a>";
                                            }
                                            return '-';
                                        }
                                    },
                                    {
                                        "targets": 5,
                                        "render": function ( data, type, full, meta ) {
                                            if (full.items.length > 0) {
                                                var ColumnBuilder = "<ul>";
                                                full.items.forEach(function(data){
                                                    ColumnBuilder += "<li><strong>" + data.name + "</strong></li>";
                                                    ColumnBuilder +=  numberWithCommas(data.price);
                                                });
                                                return ColumnBuilder + "</ul>"
                                            }
                                            return ''
                                            // return '<ul> + full.items.foreach(function(data){ "<li><strong> data.name </strong> data.price </li>" }) + </ul>'
                                        }
                                    },
                                    {
                                        "targets": 6,
                                        "render": function ( data, type, full, meta ) {
                                            if (full.status === 'WAITING') {
                                                return '<span class="label label-warning">Menunggu Pembayaran</span>'
                                            } else if (full.status === 'UNPAID') {
                                                return '<span class="label label-warning">Menunggu Metode Pembayaran</span>'
                                            }  else if (full.status === 'PENDING') {
                                                return '<span class="label label-warning">Menunggu Konfirmasi</span>'
                                            }  else if (full.status === 'PAID') {
                                                return '<span class="label label-success">Telah Dibayar</span>'
                                            }  else if (full.status === 'EXPIRED') {
                                                return '<span class="label bg-black text-white">Lewat Masa Bayar</span>'
                                            }  else if (full.status === 'REFUND') {
                                                return '<span class="label label-warning">Re-Fund</span>'
                                            }
                                        }
                                    },
                                    {
                                        "targets": 7,
                                        "render": function ( data, type, full, meta ) {
                                            return moment(full.created_at).format('DD MMM YYYY')
                                        }
                                    },
                                    {
                                        "targets": 8,
                                        "render": function ( data, type, full, meta ) {
                                            return numberWithCommas(full.total_price)
                                        }
                                    }
                                ]
                            });

                            dataFilterToDataDownload(data.data.resultList);

                        } else {
                            $('#table-transactions-list').DataTable().clear().draw();
                        }

                    }  else {
                        alert(data.errorMsg);
                    }

                    filterTitle(value);

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-transaction-table', 'box-transaction-table');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        function dataFilterToDataDownload(data) {
            dataFilterToDownload = [];
            var array = [];

            data.forEach(function (value) {
                array.push({
                    transaction_id : value.reference,
                    agent_name : value.user.locker.locker_name,
                    types : getDataFilterDownloadTypes(value),
                    status : value.status,
                    created_at : moment(value.created_at).format('DD MMM YYYY'),
                    total_price : value.total_price,
                    items : getDataFilterDownloadItems(value)
                })
            });

            dataFilterToDownload = array;
        }

        function getDataFilterDownloadTypes(data) {
            var type = '';

            if (data.items.length > 0) {

                data.items.forEach(function(value){

                    var str = '';
                    str += value.type;
                    str += ',';
                    type += str;
                });

                return type.slice(0, -1);
            } else {
                return data.type
            }

        }

        function getDataFilterDownloadItems(data) {
            var item = '';

            if (data.items.length > 0) {

                data.items.forEach(function (value) {

                    var str = '';
                    str += value.name;
                    str += ' --> ';
                    str += numberWithCommas(value.price).toString();
                    str += ' || ';
                    item += str;

                });

                return item.slice(0, -4);
            } else {
                return '';
            }
        }

        var dataFilterToDownload = [];
        function getAjaxTransactionDownload() {

            var value = getDataForFilter();

            if (dataFilterToDownload.length == 0) {
                alert('No Data');
            } else {

                showLoading('.box-filter', 'box-filter');
                showLoading('.box-transaction-table', 'box-transaction-table');

                $.ajax({
                    url: '{{ url('agent/ajax/getAjaxTransactionDownload') }}',
                    data: {
                        _token: value.token,
                        type_agent_warung 		: value.type_agent_warungg,
                        store_agentid_name 		: value.store_agentid_namee,
                        status 					: value.statuss,
                        daterange_transaction 	: value.daterange_transactionn,
                        transaction_id 			: value.transaction_idd,
                        sales_transaction 		: value.sales_transaction,
                        city_id			 		: value.city,
                        type_transaction 		: value.type_transactionn,
                        item_reference 			: value.item_referencee,
                        item_param 				: value.item_paramm,
                        daterange 				: value.daterange_transactionn
                    },
                    type: 'get',
                    responseType: 'blob', // important
                    async : true,
                    success: function(response, textStatus, request){
                        
                        var a = document.createElement("a");
                        a.href = response.file; 
                        a.download = response.name;
                        document.body.appendChild(a);
                        a.click();
                        a.remove();

                        hideLoading('.box-filter', 'box-filter');
                        hideLoading('.box-transaction-table', 'box-transaction-table');
                    },
                    error: function (ajaxContext) {
                        // showHideLoading(type, 'hide');
                        hideLoading('.box-filter', 'box-filter');
                        hideLoading('.box-transaction-table', 'box-transaction-table');
                        alert('Export error: '+ajaxContext.responseText);
                    }
                });
            }
        }

    </script>

    {{-- Insert Locker Data to DropDown --}}
    <script type="text/javascript">

        function filterTitle() {
            var type_agent_warung = $('#type-agent-warung').select2("data")[0].text;
            var lockerId = $('#store-agentid-name').select2("data")[0].text;
            var city = $('#city').select2("data")[0].text;
            var status = $('#status').select2("data")[0].text;

            var dateRange = transactionDateRange;
            var transactionId = $('#transaction-id')[0].value;
            var sales_transaction = $('#sales-transaction')[0].value;

            var type_transaction = $('#type-transaction').select2("data");
            var itemReference = $('#item-reference')[0].value;
            var itemParam = $('#item-param')[0].value;

            var space = '&nbsp&nbsp&nbsp&nbsp';
            var defaultTitle = '<span class="label span_filterr" style="font-size: 14px">Type: ' +type_agent_warung+ '</span>' +
            space + '<span class="label span_filterr" style="font-size: 14px">Store/Agent: ' +lockerId+ '</span>' +
            space + '<span class="label span_filterr" style="font-size: 14px">Kota: ' +city+ '</span>' +
                space + '<span class="label span_filterr" style="font-size: 14px">Status: ' +status+ '</span>';

            if (transactionDateRange != '') {
                var str = transactionDateRange.split(',');
                var text = moment(str[0]).format('DD MMM YYYY') + ' - ' + moment(str[1]).format('DD MMM YYYY');
                var transactionDateRangeTitle = space + '<span class="label span_filterr" style="font-size: 14px">Transaction Date: ' +text+ '</span>';
            } else {
                var transactionDateRangeTitle = '';
            }

            if (transactionId != '') {
                var transactionIdTitle = space + '<span class="label span_filterr" style="font-size: 14px">Transaction ID: ' +transactionId+ '</span>';
            } else {
                var transactionIdTitle = '';
            }

            if (type_transaction.length > 0) {
                var text = '';
                type_transaction.forEach(function (data) {
                    text += data.text;
                    text += ', '
                });

                var textt = text.slice(0, -2);
                console.log(textt)
                var transactionTypeTitle = space + '<span class="label span_filterr" style="font-size: 14px">Transaction Type: ' +textt+ '</span>';
            } else {
                var transactionTypeTitle = '';
            }

            if (sales_transaction != '') {
                var sales_transactionTitle = space + '<span class="label span_filterr" style="font-size: 14px">Sales Transaction: ' +sales_transaction+ '</span>';
            } else {
                var sales_transactionTitle = '';
            }

            if (itemReference != '') {
                var itemReferenceTitle = space + '<span class="label span_filterr" style="font-size: 14px">Item Reference: ' +itemReference+ '</span>';
            } else {
                var itemReferenceTitle = '';
            }

            if (itemParam != '') {
                var itemParamTitle = space + '<span class="label span_filterr" style="font-size: 14px">Item Param: ' +itemParam+ '</span>';
            } else {
                var itemParamTitle = '';
            }

            $('#title-filter').html(defaultTitle + transactionDateRangeTitle + transactionIdTitle +
                transactionTypeTitle + sales_transactionTitle + itemReferenceTitle + itemParamTitle);
        }

        function resetFilterTitle() {

            var type_agent_warung = $('#type-agent-warung').select2("data")[0].text;
            var lockerId = $('#store-agentid-name').select2("data")[0].text;
            var city = $('#city').select2("data")[0].text;
            var status = $('#status').select2("data")[0].text;

            var space = '&nbsp&nbsp&nbsp&nbsp';
            var defaultTitle = '<span class="label span_filterr" style="font-size: 14px">Type: ' +type_agent_warung+ '</span>' +
            space + '<span class="label span_filterr" style="font-size: 14px">Store/Agent: ' +lockerId+ '</span>' +
            space + '<span class="label span_filterr" style="font-size: 14px">Kota: ' +city+ '</span>' +
                space + '<span class="label span_filterr" style="font-size: 14px">Status: ' +status+ '</span>';

            $('#title-filter').html(defaultTitle);
        }

        function getDataForFilter() {

            // console.log($('#type').select2("data")[0].id);
            // console.log($('#status').select2("data")[0].id);
            // console.log($('#sales').select2("data")[0].id);
            // console.log($('#province').select2("data")[0].id);
            // console.log($('#city').select2("data")[0].id);
            // console.log($('#name')[0].value);
            // console.log($('#agentid')[0].value);

            var type_agent_warung = $('#type-agent-warung').select2("data")[0].id;
            var lockerId = $('#store-agentid-name').select2("data")[0].locker_id;
            var status = $('#status').select2("data")[0].id;
			var city = $('#city').select2("data")[0].id;
			
            var dateRange = transactionDateRange;
            var transactionId = $('#transaction-id')[0].value;
            var sales_transaction = $('#sales-transaction')[0].value;
            var type_transaction = $('#type-transaction').val();
            var itemReference = $('#item-reference')[0].value;
            var itemParam = $('#item-param')[0].value;

            var result = [];
            result.push({
                token: '{{ csrf_token() }}',
                type_agent_warungg 		: type_agent_warung,
                store_agentid_namee 	: lockerId,
                statuss 				: status,
                daterange_transactionn 	: dateRange,
                transaction_idd 		: transactionId,
                sales_transaction 		: sales_transaction,
                city					: city,
                type_transactionn 		: type_transaction,
                item_referencee 		: itemReference,
                item_paramm 			: itemParam
            });

            return result[0];
        }

        var dataDropDown;
        function insertDataToDropdownSelect2(data) {
            dataDropDown = data;

            // ========= Data locker ========= //
            var lockerDataSelect2 = [];
            lockerDataSelect2.push({ id: "", locker_id: "", text: 'All Agent' });
            data.lockerData.forEach(function(data){
                // console.log(data.type);
                lockerDataSelect2.push({
                    id: data.id,
                    locker_id: data.locker_id,
                    text: data.locker_name + ' [' + data.locker_id + ']'
                })
            });

            $('#store-agentid-name').html('').select2({
                data: lockerDataSelect2
            });

            // ========= Data cities ========= //
            var cities = [];
            cities.push({ id: "", locker_id: "", text: 'Semua Kota' });
            data.cities.forEach(function(data){
                cities.push({
                    id			: data.id,
                    city_name	: data.city_name,
                    text		: data.city_name,
                })
            });

            $('#city').html('').select2({
                data: cities
            });
            
            hideLoading('.box-filter', 'box-filter');
        }
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        var transactionDateRange = '';
        var transactionPerformanceDateRange = '';
        var transactionNonGroupDateRange = '';
        var startDate = moment().startOf('month').format('YYYY-MM-DD');
        var endDate = moment().endOf('month').format('YYYY-MM-DD');

        var startDateToShow = moment().startOf('month').format('DD/MM/YYYY');
        var endDateToShow = moment().endOf('month').format('DD/MM/YYYY');
        transactionDateRange = startDate + ',' + endDate;
        transactionPerformanceDateRange  = startDate + ',' + endDate;

        jQuery(document).ready(function ($) {
            dateRangeTransaction();
            performanceDateRangeTransaction();
            nonGroupDateRangeTransaction();

            $('#daterange-transaction').val(startDateToShow + ' - ' + endDateToShow);
            $('#daterange-transaction-nongroup').val(startDateToShow + ' - ' + endDateToShow);
            $('#daterange-transaction-performance').val(startDateToShow + ' - ' + endDateToShow);

        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange-transaction');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }

        function performanceDateRangeTransaction() {
            var inputDateRangeperformanceTransaction = $('#daterange-transaction-performance');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeperformanceTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeperformanceTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionPerformanceDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeperformanceTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionPerformanceDateRange = '';
            });
        }

        function nonGroupDateRangeTransaction() {
            var inputDateRangeNonGroupTransaction = $('#daterange-transaction-nongroup');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeNonGroupTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeNonGroupTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionNonGroupDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeNonGroupTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionNonGroupDateRange = '';
            });
        }

    </script>


    <script type="text/javascript">

        jQuery(document).ready(function ($) {
            $('#box-filter').hide(10);
            $('#box-filterr').hide(10);
            $('.select2').select2();
        });

        function initFilterData() {

            var typeDataSelect2 = [];
            typeDataSelect2.push({ id: "", text: 'All Type' });
            typeDataSelect2.push({ id: "agent", text: 'Agent' });
            typeDataSelect2.push({ id: "warung", text: 'Warung' });


            $('#type-agent-warung').html('').select2({
                data: typeDataSelect2,
                minimumResultsForSearch: Infinity
            });

            var statusDataSelect2 = [];
            statusDataSelect2.push({ id: "", text: 'All Status' });
            statusDataSelect2.push({ id: "PAID", text: 'Telah Dibayar' });
            statusDataSelect2.push({ id: "WAITING", text: 'Menunggu Pembayaran' });
            statusDataSelect2.push({ id: "PENDING", text: 'Menunggu Konfirmasi' });
            statusDataSelect2.push({ id: "REFUND", text: 'Re-Fund' });
            statusDataSelect2.push({ id: "EXPIRED", text: 'Lewat Masa Bayar' });

            $('#status').html('').select2({
                data: statusDataSelect2,
                minimumResultsForSearch: Infinity
            });

        }

        var isBoxHide = true;
        function showHideBox() {
            if (isBoxHide) {
                $('#box-filter').show(500);
                $('#box-filterr').show(500);
                $('#btn-show-hide-filter').html('Hide Filter');
            } else {
                $('#box-filter').hide(500);
                $('#box-filterr').hide(500);
                $('#btn-show-hide-filter').html('Show Filter');
            }
            isBoxHide = !isBoxHide;
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID' });
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }

        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }

    </script>

    <style>
        .span_filterr {
            padding: 5px;
            background-color: #00c0ef;
            display: inline-block;
        }
    </style>

@stop