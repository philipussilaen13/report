@extends('layout.main')

@section('title')
    Agent
@endsection

@section('css')
    <meta http-equiv="refresh" content="1800">
    <style type="text/css">
        #maps {
            width: 100%;
            height: 512px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/paginationjs/dist/pagination.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('pageTitle')
    PopBox Agent
@endsection

@section('pageDesc')
    Top Sales
@endsection

@section('content')
    <div class="row" style="padding-left: 15px; padding-right: 15px">
        <div class="col-md-6">
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header" style="background-color: #686868">
                    <div class="col-md-6">
                        <h1 id="today-transactions" class="widget-user-username" style="color: white; font-weight: bolder; margin-left: -20px; margin-top: -10px">Rp0</h1>
                    </div>
                    <div class="col-md-6">
                        <h3 id="date-transactions" class="widget-user-username pull-right" style="color: white; margin-right: -15px; margin-top: -10px"></h3>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="widget-user-username" style="color: white; margin-left: -5px; margin-top: 10px">Transaction</h3>
                        </div>
                        <div class="col-md-4 " style="text-align: right">
                            {{--<div style="display: block; float: left; border: 1px solid black;">--}}
                            {{--<div style="height: 30px; border: 1px solid green; float: left">--}}
                            {{--<span id="today-percentage-icon-up"><i class="fa fa-sort-up" style="color: green; font-size: 40px; position: absolute; top: 0px;"></i></span>--}}
                            {{--</div>--}}
                            {{--<div style="height: 30px; border: 1px solid blue; float: left">--}}
                            {{--<span id="today-percentage-icon-down"><i class="fa fa-sort-down" style="color: red; font-size: 40px; position: absolute; top: 0px;"></i></span>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <span id="today-percentage" style="color: red; font-size: 24px;">0</span><br><span style="color: white">from yesterday</span>
                        </div>
                    </div>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href="#">Topup<span id="today-topup" class="pull-right badge bg-blue">Rp0</span></a></li>
                        <li><a href="#">Current Balance<span id="today-floating_money"  class="pull-right badge bg-aqua">Rp0</span></a></li>
                        <li><a href="#">Non-Digital<span id="today-non-digital" class="pull-right badge bg-green">Rp0</span></a></li>
                        <li><a href="#">Digital<span id="today-digital" class="pull-right badge bg-red">Rp0</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="padding-right: -150px !important;">
            <div class="box box-solid box-topselling-chart">
                <div class="box-header with-border">
                    <div class="col-md-6">
                        <h3 style="margin-left: -3px; margin-top: -3px">Overview Product</h3>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <input id="switch-chart" type="checkbox" checked data-toggle="toggle"
                                   data-on="Total" data-off="Amount" data-onstyle="default" data-size="small">
                            <div class="btn-group" style="margin-left: 30px">
                                <button id="b" type="button" class="btn btn-default btn-sm" onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, statusTimeChartSalesNow)" >
                                    Refresh
                                </button>
                                {{--<button id="b" type="button" class="btn btn-default btn-sm" onclick="notify('Test Title', 'Test Body')" >--}}
                                    {{--Refresh--}}
                                {{--</button>--}}
                                <button id="btnDropDownChart" type="button" class="btn btn-default btn-sm">Today</button>
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, lastSevenDays)">Last 7 Days</a></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, lastThirtyDays)">Last 30 Days</a></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, thisMonth)">This Month</a></li>
                                    <li class="divider"></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, today)">Today</a></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, allTime)">All Time</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="top-selling-graph" class="box-body">
                    <canvas id="myChartt" height= auto></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left: 15px; padding-right: 15px">
        {{-- Top Selling Offline --}}
        <div class="col-md-6">
            <div class="box box-solid box-topselling-offline">
                <div class="box-header with-border">
                    <div class="col-md-6">
                        <h3 style="margin-top: -3px; margin-left: -5px">Top Non Digital Product By Sales</h3>
                    </div>
                    <div class="col-md-6">
                        {{--<button type="button" class="btn btn-warning btn-flat"><i class="fa fa-refresh"></i> </button>--}}
                        <div class="btn-group pull-right">
                            <button id="b" type="button" onclick="topSellingOfflinefilterRange(offline, statusTimeOfflineNow)" class="btn btn-default btn-sm">Refresh</button>
                            <button id="btnDropDownOffline" type="button" class="btn btn-default btn-sm">Today</button>
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a onclick="topSellingOfflinefilterRange(offline, lastSevenDays)">Last 7 Days</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(offline, lastThirtyDays)">Last 30 Days</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(offline, thisMonth)">This Month</a></li>
                                <li class="divider"></li>
                                <li><a onclick="topSellingOfflinefilterRange(offline, today)">Today</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(offline, allTime)">All Time</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="text-align: right; color: #419641">
                    <h4>Non-Digital:
                        <span id="total-non-digital">
                        0
                    </span>
                    </h4>
                </div>
                <div id="top-selling-offline-product" class="box-body">
                    <table id="table-topselling-offline" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Total</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody id="tbody-table-topselling-offline">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- Top Selling Online --}}
        <div class="col-md-6">
            <div class="box box-solid box-topselling-online">
                <div class="box-header with-border">
                    <div class="col-md-6">
                        <h3 style="margin-left: -3px; margin-top: -3px">Top Digital Product By Sales</h3>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group pull-right">
                            <button id="a" type="button" onclick="topSellingOfflinefilterRange(online, statusTimeOnlineNow)" class="btn btn-default btn-sm">Refresh</button>
                            <button id="btnDropDownOnline" type="button" class="btn btn-default btn-sm">Today</button>
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a onclick="topSellingOfflinefilterRange(online, lastSevenDays)">Last 7 Days</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(online, lastThirtyDays)">Last 30 Days</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(online, thisMonth)">This Month</a></li>
                                <li class="divider"></li>
                                <li><a onclick="topSellingOfflinefilterRange(online, today)">Today</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(online, allTime)">All Time</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="text-align: right; color: #419641">
                    <h4>Digital:
                        <span id="total-digital">
                        0
                    </span>
                    </h4>
                </div>
                <div id="top-selling-online-product" class="box-body">
                    <table id="table-topselling-online" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Operator</th>
                            <th>Total</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody id="tbody-table-topselling-online">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        {{-- Top Selling Agent --}}
        <div class="col-md-12" style="padding-left: 30px; padding-right: 30px; !important;">
            <div class="box box-solid box-topselling-agent">
                <div class="box-header with-border">
                    <div class="col-md-6">
                        <h3 style="margin-left: -3px; margin-top: -3px">Top Agent Performance</h3>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group pull-right">
                            <button id="b" type="button" class="btn btn-default btn-sm" onclick="topSellingOfflinefilterRange(agent, statusTimeAgentNow)" >
                                Refresh
                            </button>
                            <button id="btnDropDownAgent" type="button" class="btn btn-default btn-sm">Today</button>
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a onclick="topSellingOfflinefilterRange(agent, lastSevenDays)">Last 7 Days</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(agent, lastThirtyDays)">Last 30 Days</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(agent, thisMonth)">This Month</a></li>
                                <li class="divider"></li>
                                <li><a onclick="topSellingOfflinefilterRange(agent, today)">Today</a></li>
                                <li><a onclick="topSellingOfflinefilterRange(agent, allTime)">All Time</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="text-align: right; color: #419641">
                    <h4>Agent:
                        <span id="total-agent">
                        0
                    </span>
                    </h4>
                </div>
                <div id="top-selling-agent" class="box-body">
                    <table id="table-topselling-agent" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Non-Digital</th>
                            <th>Non-Digital Amount</th>
                            <th>Digital</th>
                            <th>Digital Amount</th>
                            <th>Total Transaction</th>
                            <th>Total Amount</th>
                        </tr>
                        </thead>
                        <tbody id="tbody-table-agent">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" hidden="true">
        <div class="col-md-12" style="padding-left: 30px; padding-right: 30px; !important;">
            <div class="box box-solid box-topselling-chart">
                <div class="box-header with-border">
                    <div class="col-md-6">
                        <h3 style="margin-left: -3px; margin-top: -3px">Transaction Summary</h3>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <input id="switch-chart" type="checkbox" checked data-toggle="toggle"
                                   data-on="Total" data-off="Amount" data-onstyle="default" data-size="small">
                            <div class="btn-group" style="margin-left: 30px">
                                <button id="b" type="button" class="btn btn-default btn-sm" onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, statusTimeChartSalesNow)" >
                                    Refresh
                                </button>
                                <button id="btnDropDownChart" type="button" class="btn btn-default btn-sm">Today</button>
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, lastSevenDays)">Last 7 Days</a></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, lastThirtyDays)">Last 30 Days</a></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, thisMonth)">This Month</a></li>
                                    <li class="divider"></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, today)">Today</a></li>
                                    <li><a onclick="topSellingOfflinefilterRange(chartOfflineOnlineSales, allTime)">All Time</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="top-selling-graph" class="box-body">
                    <canvas id="myChart" height= auto></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.js')}}"></script>
    <script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('plugins/paginationjs/dist/pagination.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="{{ asset('plugins/bootstrap-notify-3.1.3/dist/bootstrap-notify.min.js')}}"></script>
    <script type="text/javascript">

        //'Today'       : [moment(), moment()],
        //'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        //'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        //'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        var online = 'online';
        var offline = 'offline';
        var agent = 'agent';
        var chartOfflineOnlineSales = 'chartOfflineOnlineSales';
        var todayTransactions = 'todayTransactions';

        var today = 'today';
        var lastSevenDays = 'lastSevenDays';
        var lastThirtyDays = 'lastThirtyDays';
        var thisMonth = 'month';
        var allTime = 'allTime';

        var chartSalesIsTotal = true;

        var statusTimeOfflineNow = '';
        var statusTimeOnlineNow = '';
        var statusTimeAgentNow = '';
        var statusTimeChartSalesNow = '';

        var btnDropDownOffline = '#btnDropDownOffline';
        var btnDropDownOnline = '#btnDropDownOnline';
        var btnDropDownAgent = '#btnDropDownAgent';
        var btnDropDownChart = '#btnDropDownChart';

        var chartSalesData;
        var chartt;

        var transactionText = 'Transaction ';
        var totalText = ' by Total';
        var amountText = ' by Amount';

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "currency-cust-pre": function (a) {
                /* Remove Rp text */
                // var x = String(a).replace("Rp", "");

                /* Remove comma and ,00 */
                // x = x.replace(",00", "");

                var x = OSREC.CurrencyFormatter.parse(String(a), { locale: 'id_ID' });

                x = replaceAll(String(x), ".", "");

                // console.log('currency --> ' + x);

                /* Parse and return */
                return parseFloat(x);
            },

            "currency-cust-asc": function (a, b) {
                return a - b;
            },

            "currency-cust-desc": function (a, b) {
                return b - a;
            }
        });

        jQuery(document).ready(function ($) {

            $('#today-percentage').hide();

            var ajax_call = function () {

                topSellingOfflinefilterRange(offline, statusTimeOfflineNow);
                topSellingOfflinefilterRange(online, statusTimeOnlineNow);
                topSellingOfflinefilterRange(agent, statusTimeAgentNow);
                topSellingOfflinefilterRange(chartOfflineOnlineSales, statusTimeChartSalesNow);
                topSellingOfflinefilterRange(todayTransactions, lastSevenDays);
            };

            var interval = 1000 * 300;
            setInterval(ajax_call, interval);

            $('#switch-chart').change(function () {
                chartSalesIsTotal = $(this).prop('checked');
                if (chartSalesIsTotal) {
                    updateChart(totalText, chartSalesData);
                } else {
                    updateChart(amountText, chartSalesData);
                }
            });

            ajaxTopSellingOffline();
            ajaxTopSellingOnline();
            ajaxTopSellingAgent();
            topSellingOfflinefilterRange(chartOfflineOnlineSales, lastSevenDays);
            topSellingOfflinefilterRange(todayTransactions, lastSevenDays);

            $('#date-transactions').html(moment().format('DD MMM YYYY'));
        });

        function getChart(charData, title) {
            var ctxx = document.getElementById("myChartt");
            return myCharttt = new Chart(ctxx, {
                type: 'bar',
                data: charData,
                options: {
                    title: {
                        display: true,
                        text: title
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Range Date'
                            }
                        }],
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    if (chartSalesIsTotal) {
                                        return value;
                                    }
                                    return numberWithCommas(value);
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Transactions by Total'
                            }
                        }]
                    }
                }
            });
        }

        function chart(chartData, title) {
            var mmyChartt = getChart(chartData, title);
            chartt = mmyChartt;
        }

        function createChartData(data) {
            var arr_date = [];
            var arr_non_digital = [];
            var arr_digital = [];
            for (i = 0; i < data.topProduct.length; i++) {
                arr_date.push(moment(data.topProduct[i].created_at).format('D MMM Y'));
                if (chartSalesIsTotal) {
                    arr_non_digital.push(parseInt(data.topProduct[i].nondigital));
                    arr_digital.push(parseInt(data.topProduct[i].digital));
                } else {
                    arr_non_digital.push(parseInt(data.topProduct[i].nondigital_price));
                    arr_digital.push(parseInt(data.topProduct[i].digital_price));
                }
            }

            return barChartData = {
                labels: arr_date,
                datasets: [{
                    label: 'Digital',
                    backgroundColor: '#A582C5',
                    data: arr_digital
                }, {
                    label: 'Non-Digital',
                    backgroundColor: '#DB82A1',
                    data: arr_non_digital
                }]

            };
        }

        function updateChart(label, data) {
            var title = 'Transactions ' + moment(data.startDate).format('D MMM Y') + " - " + moment(data.endDate).format('D MMM Y') + label;
            removeData(chartt);
            chartt.options.title.text = title;
            chartt.options.scales.yAxes[0].scaleLabel.labelString = 'Transactions ' + label;
            chartt.data = createChartData(data);
            chartt.update();
        }

        function removeData(chart) {
            chart.data.labels.pop();
            chart.data.datasets.pop();
            chart.update();
        }

        function topSellingOfflinefilterRange(type, time) {
            showHideLoading(type, 'show');

            $.ajax({
                url: '{{ url('agent/ajax/getFilter/{type}/{time}') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {
                    type: type,
                    time: time
                },
                success: function(data){
                    if (data.isSuccess === true) {
                        if (data.data.topProduct.length > 0) {
                            // console.log(data.data);
                            showDataToView(type, data.data);
                            changetBtnDropDownText(type, time);
                            showHideLoading(type, 'hide');
                        } else if (data.data.topProduct.length < 1) {
                            clearDataTable(type);
                            changetBtnDropDownText(type, time);
                            showHideLoading(type, 'hide');
                        }
                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showHideLoading(type, 'hide');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showHideLoading(type, 'hide');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showHideLoading(type, 'hide');
                })
                .always(function() {
                    showHideLoading(type, 'hide');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function clearDataTable(type) {
            console.log('CLEAR ' + type);
            if (type === online) {
                $('#table-topselling-online').DataTable().clear().draw();
            } else if (type === offline) {
                $('#table-topselling-offline').DataTable().clear().draw();
            } else if (type === agent) {
                $('#table-topselling-agent').DataTable().clear().draw();
            } else if (type === chartOfflineOnlineSales) {
                $('#myChart').hide();
            }
        }

        function showDataToView(type, data) {
            if (type === online) {
                if (data.latest !== '') countTopSellingOfflienAjax = 1;
                var countTotal = data.countProductDb;
                $('#total-digital').html(countTotal);

                var tableOnline = $('#table-topselling-online').DataTable({
                    'destroy'      : true,
                    'retreive'      : true,
                    'paging'      : true,
                    'searching'   : true,
                    'ordering'    : true,
                    'autoWidth'   : false,
                    'responsive'  : true,
                    'processing'  : true,
                    'scrollY'     : 200,
                    'deferRender' : true,
                    'scroller'    : true,
                    'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    'order': [[ 2, "desc" ]],
                    columnDefs: [
                        { type: 'currency-cust', targets: 3 }
                    ]
                });

                tableOnline.clear();
                for($i = 0; $i < data.topProduct.length; $i++) {
                    tableOnline.row.add([
                        $i + 1,
                        data.topProduct[$i].label,
                        data.topProduct[$i].count_transaction,
                        numberWithCommas(data.topProduct[$i].total_transaction)
                    ]).draw(false);
                }

            } else if (type === offline) {
                if (data.latest !== '') countTopSellingOfflienAjax = 1;
                $('#total-non-digital').html(data.countSku);

                var tableOffline = $('#table-topselling-offline').DataTable({
                    'destroy'    : true,
                    'retrieve'    : true,
                    'paging'      : true,
                    'searching'   : true,
                    'ordering'    : true,
                    'autoWidth'   : false,
                    'responsive'  : true,
                    'processing'  : true,
                    'scrollY'     : 200,
                    'deferRender' : true,
                    'scroller'    : true,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    'order': [[ 2, "desc" ]],
                    columnDefs: [
                        { type: 'currency-cust', targets: 3 }
                    ]
                });

                tableOffline.clear();
                for($i = 0; $i < data.topProduct.length; $i++) {
                    tableOffline.row.add([
                        $i + 1,
                        data.topProduct[$i].name,
                        data.topProduct[$i].sum,
                        numberWithCommas(data.topProduct[$i].total_order)
                    ]).draw(false);
                }

            } else if (type === agent) {
                if (data.latest !== '') countTopSellingAgentAjax = 1;
                $('#total-agent').html(data.totalAgent);

                var tableAgent = $('#table-topselling-agent').DataTable({
                    'destroy'    : true,
                    'retrieve'    : true,
                    'paging'      : true,
                    'searching'   : true,
                    'ordering'    : true,
                    'autoWidth'   : false,
                    'responsive'  : true,
                    'processing'  : true,
                    'scrollY'     : 200,
                    'deferRender' : true,
                    'scroller'    : true,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    'order': [[ 6, "desc" ]],
                    'buttons': [
                        'copy', 'excel', 'pdf'
                    ],
                    columnDefs: [
                        { type: 'currency-cust', targets: 3 },
                        { type: 'currency-cust', targets: 5 },
                        { type: 'currency-cust', targets: 7 }
                    ]
                });

                tableAgent.clear();
                for($i = 0; $i < data.topProduct.length; $i++) {
                    tableAgent.row.add([
                        $i + 1,
                        data.topProduct[$i].locker_name,
                        data.topProduct[$i].nondigital,
                        numberWithCommas(data.topProduct[$i].nondigital_price),
                        data.topProduct[$i].digital,
                        numberWithCommas(data.topProduct[$i].digital_price),
                        data.topProduct[$i].total_jumlah,
                        numberWithCommas(data.topProduct[$i].total_price)
                    ]).draw(false);
                }

            } else if (type === chartOfflineOnlineSales) {
                $('#myChart').show();
                chartSalesData = data;
                if (statusTimeChartSalesNow === '') {
                    var title = 'Transaction ' + moment(data.startDate).format('D MMM Y') + " - " + moment(data.endDate).format('D MMM Y') + ' by Total';
                    chart(createChartData(data), title);
                } else {
                    if (chartSalesIsTotal) {
                        updateChart(totalText, data);
                    } else {
                        updateChart(amountText, data);
                    }
                }

            } else if (type === todayTransactions) {
                console.log(data);
                var topProduct = data.topProduct;
                var transactionYesterday = topProduct[topProduct.length - 2].total_price;
                var transactionToday = topProduct[topProduct.length - 1].total_price;

                var value = transactionToday - transactionYesterday;
                var percentValue = (value / transactionYesterday) * 100;

                if (percentValue < 0) {
                    // $('#today-percentage-icon').html('<i class="fa fa-sort-down" style="color: red; font-size: 40px; position: absolute; top: 0px;"></i>');

                    var todayPercentage = document.getElementById('today-percentage');
                    todayPercentage.style.color = "red";
                } else {
                    // $('#today-percentage-icon').html('<i class="fa fa-sort-up" style="color: green; font-size: 40px; position: absolute; top: 0px"></i>');

                    var todayPercentage = document.getElementById('today-percentage');
                    todayPercentage.style.color = "green";
                }
                $('#today-transactions').html(numberWithCommas(transactionToday));

                $('#today-percentage').html(percentValue.toFixed(2) + '%').show();

                $('#today-floating_money').html(numberWithCommas(data.floating_money));
                $('#today-topup').html(numberWithCommas(data.topup_amount));
                $('#today-non-digital').html(numberWithCommas(topProduct[topProduct.length - 1].nondigital_price));
                $('#today-digital').html(numberWithCommas(topProduct[topProduct.length - 1].digital_price));

            }

        }

        function changetBtnDropDownText(type, time) {
            var btn = '';
            if (type === online) {
                btn = btnDropDownOnline;
                statusTimeOnlineNow = time;
            } else if (type === offline) {
                btn = btnDropDownOffline;
                statusTimeOfflineNow = time;
            } else if (type === agent) {
                btn = btnDropDownAgent;
                statusTimeAgentNow = time;
            } else if (type === chartOfflineOnlineSales) {
                btn = btnDropDownChart;
                statusTimeChartSalesNow = time;
            }

            if (time === today) {
                $(btn).text('Today');
            } else if (time === lastSevenDays) {
                $(btn).text('Last 7 Days');
            } else if (time === lastThirtyDays) {
                $(btn).text('Last 30 Days');
            } else if (time === thisMonth) {
                $(btn).text('This Month');
            } else if (time === allTime) {
                $(btn).text('All Time');
            }
        }

        function showHideLoading(type, showHide) {
            if (showHide === 'show') {
                if (type === online) {
                    showLoading('.box-topselling-online', 'box-topselling-online');
                } else if (type === offline) {
                    showLoading('.box-topselling-offline', 'box-topselling-offline');
                } else if (type === agent) {
                    showLoading('.box-topselling-agent', 'box-topselling-agent');
                } else if (type === chartOfflineOnlineSales) {
                    showLoading('.box-topselling-chart', 'box-topselling-chart');
                }
            } else if (showHide === 'hide') {
                if (type === online) {
                    hideLoading('.box-topselling-online', 'box-topselling-online');
                } else if (type === offline) {
                    hideLoading('.box-topselling-offline', 'box-topselling-offline');
                } else if (type === agent) {
                    hideLoading('.box-topselling-agent', 'box-topselling-agent');
                } else if (type === chartOfflineOnlineSales) {
                    hideLoading('.box-topselling-chart', 'box-topselling-chart');
                }
            }
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID' });
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

        function ajaxChartSalesSelling() {
            $.ajax({
                url: '{{ url('agent/ajax/getTopChartSelling') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {},
                success: function(data){
                    if (data.isSuccess === true) {
                        showDataToView(agent, data.data);
                        changetBtnDropDownText('agent', 'today');
                        showHideLoading('agent', 'hide');
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showHideLoading('agent', 'hide');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showHideLoading('agent', 'hide');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showHideLoading('agent', 'hide');
                })
                .always(function() {
                    showHideLoading('agent', 'hide');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        var countTopSellingAgentAjax = 0;
        function ajaxTopSellingAgent() {
            showHideLoading('agent', 'show');
            $.ajax({
                url: '{{ url('agent/ajax/getTopSellingAgent') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {},
                success: function(data){
                    if (data.isSuccess === true) {
                        showDataToView(agent, data.data);
                        changetBtnDropDownText(agent, lastSevenDays);
                        showHideLoading('agent', 'hide');
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showHideLoading('agent', 'hide');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showHideLoading('agent', 'hide');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showHideLoading('agent', 'hide');
                })
                .always(function() {
                    showHideLoading('agent', 'hide');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        var countTopSellingOnlineAjax = 0;
        function ajaxTopSellingOnline() {
            showHideLoading('online', 'show');
            $.ajax({
                url: '{{ url('agent/ajax/getTopSellingOnline') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {},
                success: function(data){
                    if (data.isSuccess === true) {
                        showDataToView(online, data.data);
                        changetBtnDropDownText(online, lastSevenDays);
                        showHideLoading('hide', 'online')
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showHideLoading('online', 'hide');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showHideLoading('online', 'hide');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showHideLoading('online', 'hide');
                })
                .always(function() {
                    showHideLoading('online', 'hide');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        var countTopSellingOfflienAjax = 0;
        function ajaxTopSellingOffline(){
            showHideLoading('offline', 'show');
            $.ajax({
                url: '{{ url('agent/ajax/getTopSellingOffline') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {},
                success: function(data){
                    if (data.isSuccess === true) {
                        showDataToView(offline, data.data);
                        changetBtnDropDownText(offline, lastSevenDays);
                        showHideLoading('offline', 'hide')
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showHideLoading('offline', 'hide');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showHideLoading('offline', 'hide');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showHideLoading('offline', 'hide');
                })
                .always(function() {
                    showHideLoading('offline', 'hide');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function notify(title, body) {
            // $.notify({
            //     title: title,
            //     message: body
            // });

            $.notify("Enter: Fade In and RightExit: Fade Out and Right", {
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight',
                    delay: 5000
                }
            });
        }

        function ajaxGetFloatingMoney(){
            // showHideLoading('offline', 'show');
            $.ajax({
                url: '{{ url('agent/ajax/getFloatingMoney') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {},
                success: function(data){
                    if (data.isSuccess === true) {
                        $('#floating_money').html(numberWithCommas(data.data.floating_money));
                        // showDataToView(offline, data.data);
                        // changetBtnDropDownText(offline, lastSevenDays);
                        // showHideLoading('offline', 'hide')
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showHideLoading('offline', 'hide');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    showHideLoading('offline', 'hide');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    showHideLoading('offline', 'hide');
                })
                .always(function() {
                    showHideLoading('offline', 'hide');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }
    </script>

@endsection