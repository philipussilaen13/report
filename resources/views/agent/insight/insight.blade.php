@extends('layout.main')

@section('title')
    Agent
@endsection

@section('css')
    <meta http-equiv="refresh" content="1800">
    <style type="text/css">
        #maps {
            width: 100%;
            height: 512px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/paginationjs/dist/pagination.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('pageTitle')
    PopBox Agent
@endsection

@section('pageDesc')
    User Insight
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-body border-radius-none">
                        <div class="box-header" style="margin-top: -30px">
                            <h3>Filter</h3>
                        </div>
                        <div class="row" style="margin-left: 5px">
                            <div class="col-md-3" style="padding-left: 15px">
                                <h5>Measured By</h5>
                            </div>
                            <div class="col-md-3" style="padding-left: 15px">
                                <h5>Period</h5>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 5px">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control select2" id="measuredBy" name="measuredBy">
                                        <option value="registeredDate">Register Date</option>
                                        {{--<option value="activatedDate">Activated Date</option>--}}
                                        <option value="transactionDate">Transaction</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control select2" id="periodBy" name="periodBy">
                                        <option value="lastSevenDays">Last 7 Days</option>
                                        <option value="lastThirtyDays">Last 30 Days</option>
                                        <option value="month">This Month</option>
                                        <option value="today">Today</option>
                                        <option value="allTime">All Time</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button id="btn-transaction" class="btn btn-primary btn-block" type="button" onclick="getInsightFilter()">Filter</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="javascript:void(0);" onclick="changeView(1)">
                                    <div id="box-allType" class="box box-solid box-insight-allType" style="margin-left: 20px; background-color: #dadee5">
                                        <div class="box-body border-radius-none">
                                            <div><h4>All Type</h4></div>
                                            <div class="row" style="margin-top: -15px; margin-left: 15px;">
                                                <h1 id="countAll" style="color: #1c2529; font-size: 65px">0</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="javascript:void(0);" onclick="changeView(2)">
                                    <div id="box-agent" class="box box-solid box-insight-agent" style="margin-left: 20px;">
                                        <div class="box-body border-radius-none">
                                            <div><h4>Agent</h4></div>
                                            <div class="row" style="margin-top: -15px; margin-left: 15px;">
                                                <h1 id="countAgent" style="color: #1c2529; font-size: 65px">0</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="javascript:void(0);" onclick="changeView(3)">
                                    <div id="box-warung" class="box box-solid box-insight-warung" style="margin-left: 20px;">
                                        <div class="box-body border-radius-none">
                                            <div><h4>Warung</h4></div>
                                            <div class="row" style="margin-top: -15px; margin-left: 15px;">
                                                <h1 id="countWarung" style="color: #1c2529; font-size: 65px">0</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <div class="box-body border-radius-none">
                                    <div class="box-header with-border" style="margin-top: -30px">
                                        <div><h3 id="header-chart"></h3></div>
                                    </div>
                                </div>
                                <div id="data-graph" class="box-body">
                                    <canvas id="myChart" height= auto></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-body border-radius-none">
                        <div class="box-header" style="margin-top: -30px">
                            <h3>Filter</h3>
                        </div>
                        <div class="row" style="margin-left: 5px">
                            <div class="col-md-6" style="padding-left: 15px">
                                <h5>Period</h5>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 5px">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control select2" id="periodByCount" name="periodByCount">
                                        <option value="lastSevenDays">Last 7 Days</option>
                                        <option value="lastThirtyDays">Last 30 Days</option>
                                        <option value="month">This Month</option>
                                        <option value="today">Today</option>
                                        <option value="allTime">All Time</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button id="btn-transaction-count" class="btn btn-primary btn-block" type="button" onclick="getTransactionCount()">Filter</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="javascript:void(0);" onclick="changeViewCount(1)">
                                    <div id="box-allType-count" class="box box-solid box-insight-allType-count" style="margin-left: 20px; background-color: #dadee5">
                                        <div class="box-body border-radius-none">
                                            <div><h4>All Type</h4></div>
                                            <div class="row" style="margin-top: -15px; margin-left: 15px;">
                                                <h1 id="countAllCount" style="color: #1c2529; font-size: 65px">0</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="javascript:void(0);" onclick="changeViewCount(2)">
                                    <div id="box-agent-count" class="box box-solid box-insight-agent-count" style="margin-left: 20px;">
                                        <div class="box-body border-radius-none">
                                            <div><h4>Agent</h4></div>
                                            <div class="row" style="margin-top: -15px; margin-left: 15px;">
                                                <h1 id="countAgentCount" style="color: #1c2529; font-size: 65px">0</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="javascript:void(0);" onclick="changeViewCount(3)">
                                    <div id="box-warung-count" class="box box-solid box-insight-warung-count" style="margin-left: 20px;">
                                        <div class="box-body border-radius-none">
                                            <div><h4>Warung</h4></div>
                                            <div class="row" style="margin-top: -15px; margin-left: 15px;">
                                                <h1 id="countWarungCount" style="color: #1c2529; font-size: 65px">0</h1>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <div class="box-body border-radius-none">
                                    <div class="box-header with-border" style="margin-top: -30px">
                                        <div class="col-md-10">
                                            <h3 id="header-chart-count"></h3>

                                        </div>
                                        <div class="col-md-2" style="margin-top: 20px">
                                            <input id="switch-chart" type="checkbox" checked data-toggle="toggle"
                                                   data-on="Total" data-off="Amount" data-onstyle="default" data-size="small">
                                        </div>
                                    </div>
                                </div>
                                <div id="data-graph" class="box-body">
                                    <canvas id="myChartCount" height= auto></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="box box-solid">--}}
                    {{--<div class="box-body border-radius-none">--}}
                        {{--<div class="box-header with-border" style="margin-top: -30px">--}}
                            {{--<div><h3 id="header-chart"></h3></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div id="data-graph" class="box-body">--}}
                        {{--<canvas id="myChart" height= auto></canvas>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.js')}}"></script>
    <script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>

    <script type="text/javascript">

        var today = 'today';
        var lastSevenDays = 'lastSevenDays';
        var lastThirtyDays = 'lastThirtyDays';
        var thisMonth = 'month';
        var allTime = 'allTime';

        var registeredDate = 'registeredDate';
        var transactionDate = 'transactionDate';
        var transactionCount = 'transactionCount';

        var boxCount = 'boxCount';

        var boxAllType = document.getElementById('box-allType');
        var boxAgentType = document.getElementById('box-agent');
        var boxWarungType = document.getElementById('box-warung');

        var boxAllTypeCount = document.getElementById('box-allType-count');
        var boxAgentTypeCount = document.getElementById('box-agent-count');
        var boxWarungTypeCount = document.getElementById('box-warung-count');

        var chartSalesData;
        var chartSalesIsTotal = true;

        var chartTypeData;
        var chartt;
        var charttCount;

        //'Today'       : [moment(), moment()],
        //'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        //'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        //'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        $('.select2').select2();
        $('#myChart').hide();
        $('#myChartCount').hide();

        jQuery(document).ready(function ($) {

            var ajax_call = function () {

                getInsightFilter()
            };

            var interval = 1000 * 300;
            setInterval(ajax_call, interval);

            $('#measuredBy').select2({
                minimumResultsForSearch: Infinity
            });

            $('#periodBy').select2({
                minimumResultsForSearch: Infinity
            });

            $('#periodBy').val(thisMonth).trigger('change');
            $('#periodByCount').val(thisMonth).trigger('change');

            $('#switch-chart').change(function () {
                chartSalesIsTotal = $(this).prop('checked');
                if (chartSalesIsTotal) {
                    updateChart(chartSalesData, transactionCount);
                } else {
                    updateChart(chartSalesData, transactionCount);
                }
            });

            getInsightFilter();
            getTransactionCount();

        });

        function changeView(value) {
            if (value === 1) {
                boxAllType.style.backgroundColor = '#dadee5';
                boxAgentType.style.backgroundColor = 'white';
                boxWarungType.style.backgroundColor = 'white';

                // chartt.data.labels.pop();
                // chartt.data.labels.push(arr_date);
                chartt.data.datasets[0].hidden = false;
                chartt.data.datasets[1].hidden = false;

            } else if (value === 2) {
                boxAllType.style.backgroundColor = 'white';
                boxAgentType.style.backgroundColor = '#dadee5';
                boxWarungType.style.backgroundColor = 'white';

                // chartt.data.labels.pop();
                // chartt.data.labels.push(arr_date_transaction_agent);
                chartt.data.datasets[0].hidden = false;
                chartt.data.datasets[1].hidden = true;

            } else if (value === 3) {
                boxAllType.style.backgroundColor = 'white';
                boxAgentType.style.backgroundColor = 'white';
                boxWarungType.style.backgroundColor = '#dadee5';

                // chartt.data.labels.pop();
                // chartt.data.labels.push(arr_date_transaction_warung);
                chartt.data.datasets[0].hidden = true;
                chartt.data.datasets[1].hidden = false;
            }

            chartt.update();
        }

        function changeViewCount(value) {
            if (value === 1) {
                boxAllTypeCount.style.backgroundColor = '#dadee5';
                boxAgentTypeCount.style.backgroundColor = 'white';
                boxWarungTypeCount.style.backgroundColor = 'white';

                // chartt.data.labels.pop();
                // chartt.data.labels.push(arr_date);
                charttCount.data.datasets[0].hidden = false;
                charttCount.data.datasets[1].hidden = false;

            } else if (value === 2) {
                boxAllTypeCount.style.backgroundColor = 'white';
                boxAgentTypeCount.style.backgroundColor = '#dadee5';
                boxWarungTypeCount.style.backgroundColor = 'white';

                // chartt.data.labels.pop();
                // chartt.data.labels.push(arr_date_transaction_agent);
                charttCount.data.datasets[0].hidden = false;
                charttCount.data.datasets[1].hidden = true;

            } else if (value === 3) {
                boxAllTypeCount.style.backgroundColor = 'white';
                boxAgentTypeCount.style.backgroundColor = 'white';
                boxWarungTypeCount.style.backgroundColor = '#dadee5';

                // chartt.data.labels.pop();
                // chartt.data.labels.push(arr_date_transaction_warung);
                charttCount.data.datasets[0].hidden = true;
                charttCount.data.datasets[1].hidden = false;
            }

            charttCount.update();
        }

        function getInsightFilter() {
            var type = '';
            var time = '';

            type = $('#measuredBy').select2('data')[0].id;
            time = $('#periodBy').select2('data')[0].id;

            // console.log('adasdas');
            //console.log(type + " --> " + time);
            //console.log();

            showHideLoading(boxCount, 'show');

            $.ajax({
                url: '{{ url('agent/ajax/getInsightFilter/{type}/{time}') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {
                    type: type,
                    time: time
                },
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        $('#countAll').html(data.data.countAll);
                        $('#countAgent').html(data.data.countAgent);
                        $('#countWarung').html(data.data.countWarung);

                        responseChart(data, $('#measuredBy').select2('data')[0].id);

                        changeView(1);

                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    showHideLoading(boxCount, 'hide');
                    responseAjaxError()
                }
            })
                .done(function() {
                    //console.log("success - getInsightFilter");
                    showHideLoading(boxCount, 'hide');
                })
                .fail(function() {
                    //console.log("error - getInsightFilter");
                    showHideLoading(boxCount, 'hide');
                    responseAjaxError()
                })
                .always(function() {
                    showHideLoading(boxCount, 'hide');
                    //console.log("complete - getInsightFilter");
                    //console.log('===================');
                });
        }

        function getTransactionCount() {
            var type = transactionCount;
            var time = '';

            time = $('#periodByCount').select2('data')[0].id;

            //console.log('adasdas');
            //console.log(type + " --> " + time);
            //console.log();

            // showHideLoading(boxCount, 'show');

            $.ajax({
                url: '{{ url('agent/ajax/getInsightFilter/{type}/{time}') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {
                    type: type,
                    time: time
                },
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        $('#countAllCount').html(data.data.countAll);
                        $('#countAgentCount').html(data.data.countAgent);
                        $('#countWarungCount').html(data.data.countWarung);

                        responseChart(data, transactionCount);
                        //
                        // changeView(1);

                    }  else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(boxCount, 'hide');
                    // responseAjaxError()
                }
            })
                .done(function() {
                    //console.log("success - getInsightFilter");
                    // showHideLoading(boxCount, 'hide');
                })
                .fail(function() {
                    //console.log("error - getInsightFilter");
                    // showHideLoading(boxCount, 'hide');
                    // responseAjaxError()
                })
                .always(function() {
                    // showHideLoading(boxCount, 'hide');
                    //console.log("complete - getInsightFilter");
                    //console.log('===================');
                });
        }

        function showHideLoading(type, showHide) {
            if (showHide === 'show') {
                if (type === boxCount) {
                    showLoading('.box-insight-allType', 'box-insight-allType');
                    showLoading('.box-insight-agent', 'box-insight-agent');
                    showLoading('.box-insight-warung', 'box-insight-warung');
                    showLoading('.data-graph', 'data-graph');
                }
            } else if (showHide === 'hide') {
                if (type === boxCount) {
                    hideLoading('.box-insight-allType', 'box-insight-allType');
                    hideLoading('.box-insight-agent', 'box-insight-agent');
                    hideLoading('.box-insight-warung', 'box-insight-warung');
                    hideLoading('.data-graph', 'data-graph');
                }
            }
        }

        function responseAjaxError() {
            $('#countAll').html(0);
            $('#countAgent').html(0);
            $('#countWarung').html(0);
            $('#myChart').hide();
        }

        function responseChart(data, type) {

            if (type === transactionCount) {
                chartSalesData = data;
                if ($('#myChartCount').is(':hidden')) {
                    $('#myChartCount').show();
                    chart(createChartData(data.data.chart, type), getTitleChart(data), type);
                } else {
                    updateChart(data, type);
                }

                $('#header-chart-count').html(getTitleHeaderBoxChart(data));

            } else {

                if ($('#myChart').is(':hidden')) {
                    $('#myChart').show();
                    chart(createChartData(data.data.chart, type), getTitleChart(data), type);
                } else {
                    updateChart(data, type);
                }

                chartt.options.scales.xAxes[0].scaleLabel.labelString = getXAxexTitle(data);
                $('#header-chart').html(getTitleHeaderBoxChart(data));

            }

        }

        function getTitleHeaderBoxChart(data, type) {
            var title = '';
            var startMonthDate = '';
            var endMonthDate = '';

            if ($('#periodBy').select2('data')[0].id === allTime) {

                startMonthDate = moment(data.data.chart[0]).format('D MMM Y');
                endMonthDate = moment(data.data.chart[data.data.chart.length - 1]).format('D MMM Y');

                //console.log(startMonthDate + ' --> ' + endMonthDate + ' --> ' + data.data.chart.length.toString());

            } else {

                startMonthDate = moment(data.data.startDate).format('D MMM Y');
                endMonthDate = moment(data.data.endDate).format('D MMM Y');

            }

            if ($('#measuredBy').select2('data')[0].id === registeredDate) {
                title = 'Register ' + startMonthDate + " - " + endMonthDate;
            } else if ($('#measuredBy').select2('data')[0].id === transactionDate) {
                title = 'Transaction ' + startMonthDate + " - " + endMonthDate;
            } else {
                title = 'Transaction ' + startMonthDate + " - " + endMonthDate;
            }


            return title
        }

        function getTitleChart(data) {
            return getTitleHeaderBoxChart(data);
        }

        function getXAxexTitle(data) {
            var title = '';

            if ($('#periodBy').select2('data')[0].id === allTime) {

                var startMonth = moment(data.data.chart[0]).format('MMM Y');
                var endMonth = moment(data.data.chart[data.data.chart.length - 1]).format('MMM Y');

                if (startMonth === endMonth) {
                    title = moment(data.data.chart[0]).format('MMM Y');
                } else {
                    title = moment(data.data.chart[0]).format('MMM') + ' - ' + moment(data.data.chart[data.data.chart.length - 1]).format('MMM Y');
                }

            } else {

                var startMonth = moment(data.data.startDate).format('MMM');
                var endMonth = moment(data.data.endDate).format('MMM');

                if (startMonth === endMonth) {
                    title = moment(data.data.startDate).format('MMM Y');
                } else {
                    title = moment(data.data.startDate).format('MMM') + ' - ' + moment(data.data.endDate).format('MMM Y');
                }

            }

            return title;
        }

        function getYAxesTitle() {
            var title = '';

            if ($('#measuredBy').select2('data')[0].id === registeredDate) {
                title = 'Total Acquisition'
            } else if ($('#measuredBy').select2('data')[0].id === transactionDate) {
                title = 'Total Transaction'
            } else if ($('#measuredBy').select2('data')[0].id === transactionCount) {
                title = 'Transaction Count'
            }

            return title
        }

        function getChart(charData, title, type) {

            if (type === transactionCount) {

                var ctxx = document.getElementById("myChartCount");
                return myCharttt = new Chart(ctxx, {
                    type: 'bar',
                    data: charData,
                    options: {
                        title: {
                            display: true,
                            text: title
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false
                        },
                        responsive: true,
                        maintainAspectRatio: true,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: ''
                                }
                            }],
                            yAxes: [{
                                stacked: true,
                                ticks: {
                                    // Include a dollar sign in the ticks
                                    callback: function(value, index, values) {
                                        if (chartSalesIsTotal) {
                                            return value;
                                        }
                                        return numberWithCommas(value);
                                    }
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Count'
                                }
                            }]
                        }
                    }
                });

            } else {

                var ctxx = document.getElementById("myChart");
                return myCharttt = new Chart(ctxx, {
                    type: 'bar',
                    data: charData,
                    options: {
                        title: {
                            display: true,
                            text: title
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false
                        },
                        responsive: true,
                        maintainAspectRatio: true,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: ''
                                }
                            }],
                            yAxes: [{
                                stacked: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Total Acquisition'
                                }
                            }]
                        }
                    }
                });

            }
        }

        function chart(chartData, title, type) {
            if (type === transactionCount) {
                var mmyChartt = getChart(chartData, title, type);
                charttCount = mmyChartt;
            } else {
                var mmyChartt = getChart(chartData, title, type);
                chartt = mmyChartt;
            }
        }

        function createChartData(data, type) {
            var arr_date = [];

            // console.log('kkljljljlljlkj');
            // //console.log(data);

            var chrt = data;

            if (type === registeredDate) {

                var arr_agent = [];
                var arr_warung = [];

                for (i = 0; i < chrt.length; i++) {
                    arr_date.push(moment(chrt[i].date_registered).format('D MMM Y'));
                    arr_agent.push(chrt[i].total_agent);
                    arr_warung.push(chrt[i].total_warung);
                }

                return barChartData = {
                    labels: arr_date,
                    datasets: [{
                        label: 'Agent',
                        backgroundColor: '#1E88E5',
                        data: arr_agent
                    },{
                        label: 'Warung',
                        backgroundColor: '#8BDF00',
                        data: arr_warung
                    }]

                };

            } else if (type === transactionDate) {

                var arr_total_transaction_agent = [];
                var arr_total_transaction_warung = [];

                for (i = 0; i < chrt.length; i++) {
                    arr_date.push(chrt[i].date);
                    arr_total_transaction_agent.push(chrt[i].agent);
                    arr_total_transaction_warung.push(chrt[i].warung);
                }


                return barChartData = {
                    labels: arr_date,
                    datasets: [{
                        label: 'Agent',
                        backgroundColor: '#1E88E5',
                        data: arr_total_transaction_agent
                    },{
                        label: 'Warung',
                        backgroundColor: '#8BDF00',
                        data: arr_total_transaction_warung
                    }]

                };

            } else if (type === transactionCount) {

                var arr_total_agent_transaction_count = [];
                var arr_total_warung_transaction_count = [];

                for (i = 0; i < chrt.length; i++) {
                    arr_date.push(moment(chrt[i].created_at).format('D MMM Y'));
                    if (chartSalesIsTotal) {
                        arr_total_agent_transaction_count.push(chrt[i].agent_transactions);
                        arr_total_warung_transaction_count.push(chrt[i].warung_transactions);
                    } else {
                        arr_total_agent_transaction_count.push(chrt[i].total_price_agent);
                        arr_total_warung_transaction_count.push(chrt[i].total_price_warung);
                    }
                }

                return barChartData = {
                    labels: arr_date,
                    datasets: [{
                        label: 'Agent',
                        backgroundColor: '#1E88E5',
                        data: arr_total_agent_transaction_count
                    },{
                        label: 'Warung',
                        backgroundColor: '#8BDF00',
                        data: arr_total_warung_transaction_count
                    }]

                };

            }

        }

        function updateChart(data, type) {
            if (type === transactionCount) {

                if (chartSalesIsTotal) {
                    $('#countAllCount').html(data.data.countAll).css("font-size", "65px");
                    $('#countAgentCount').html(data.data.countAgent).css("font-size", "65px");
                    $('#countWarungCount').html(data.data.countWarung).css("font-size", "65px");
                } else {
                    $('#countAllCount').html(numberWithCommas(data.data.amountAll)).css("font-size", "20px");
                    $('#countAgentCount').html(numberWithCommas(data.data.amountAgent)).css("font-size", "20px");
                    $('#countWarungCount').html(numberWithCommas(data.data.amountWarung)).css("font-size", "20px");
                }

                removeData(charttCount);
                charttCount.options.title.text = getTitleChart(data);
                charttCount.options.scales.yAxes[0].scaleLabel.labelString = 'Count';

                charttCount.data = createChartData(data.data.chart, type);
                charttCount.update();

            } else {

                removeData(chartt);
                chartt.options.title.text = getTitleChart(data);
                chartt.options.scales.yAxes[0].scaleLabel.labelString = getYAxesTitle();

                chartt.data = createChartData(data.data.chart, type);
                chartt.update();

            }
        }

        function removeData(chart) {
            chart.data.labels.pop();
            chart.data.datasets.pop();
            chart.update();
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID' });
        }

    </script>

@endsection