<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Agen</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-3 border-right">
                <form method="post" action="{{ url('agent/sales/agentTopup') }}" id="form-balance">
                    {{ csrf_field() }}
                    <input type="hidden" name="lockerId" value="{{ $lockerId }}">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" name="amount" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea class="form-control" name="remark" rows="4" required></textarea>
                    </div>
                    <button class="btn btn-success" type="button" id="btn-balance">Submit</button>
                </form>
            </div>
            <div class="col-md-3 border-right">
                <div class="form-group">
                    <label>Agent</label>
                    <p>
                        <strong>{{ $locker->locker_name }}</strong> <br>
                        {{ $locker->locker_id }}
                    </p>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <p>{{ $locker->address }}</p>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <p>
                        @if ($locker->status == 1)
                            <span class="label bg-green">Online</span>
                        @elseif ($locker->status == 2)
                            <span class="label bg-black text-white">Offline</span>
                        @elseif ($locker->status == 0)
                            <span class="label bg-gray">Menunggu Aktivasi</span>
                        @endif
                    </p>
                </div>
                <div class="form-group">
                    <label>Balance Deposit</label>
                    <p>Rp {{ number_format($agentLocker->balance) }}</p>
                </div>
                <div class="form-group">
                    <label>User</label>
                    <p>
                        <strong>{{ $agentUser->name }}</strong> <br>
                        {{ $agentUser->phone }}
                    </p>
                </div>
            </div>
            <div class="col-md-6 table-responsive">
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <td>Reference</td>
                        <td>Status</td>
                        <td>Items</td>
                        <td>Harga</td>
                        <td>Tanggal</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($transaction as $element)
                        <tr>
                            <td>{{ $element->reference }}</td>
                            <td>
                                @if ($element->trashed())
                                    <span class="label label-danger">Dibatalkan</span>
                                @elseif ($element->status == 'WAITING')
                                    <span class="label label-warning">Menunggu Pembayaran</span>
                                @elseif ($element->status == 'UNPAID')
                                    <span class="label label-warning">Menunggu Pembayaran</span>
                                @elseif ($element->status=='PAID')
                                    <span class="label label-success">Telah Dibayar</span>
                                @elseif ($element->status == 'EXPIRED')
                                    <span class="label bg-black text-white">Lewat Masa Bayar</span>
                                @elseif ($element->status == 'REFUND')
                                    <span class="label label-warning">Re-Fund</span>
                                @endif
                            </td>
                            <td>
                                <dl>
                                    @foreach ($element->items as $item)
                                        <dt>{{ $item->name }} ({{ number_format($item->price) }})</dt>
                                        <dd>
                                            @if (!empty($item->params))
                                                @php
                                                    $params = $item->params;
                                                    $params = json_decode($params);
                                                @endphp
                                                @foreach ($params as $key => $param)
                                                    {{ $key }} : {{ $param }} <br>
                                                @endforeach
                                            @endif
                                        </dd>
                                    @endforeach
                                </dl>
                            </td>
                            <td>{{ number_format($element->total_price) }}</td>
                            <td>{{ date('j M Y H:i',strtotime($element->created_at)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>