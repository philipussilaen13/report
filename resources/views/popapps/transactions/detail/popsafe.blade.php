<div class="box box-widget widget-user-2">
	<div class="widget-user-header bg-yellow">
		<a href="{{ url('popapps/popsafe/getDetailPage') }}/{{ $popsafe->invoice_code }}">
			<h3 class="widget-user-username" style="color: white">
				<strong>PopSafe</strong>
				<br>{{ $popsafe->invoice_code }}
			</h3>
		</a>
		<h4 class="widget-user-desc">{{ strtoupper($popsafe->locker_name) }}</h4>
	</div>
	<div class="box box-body">
		<table class="table table-striped">
			<tr>
				<td>Status</td>
				<td>
					@if ($popsafe->status == 'PAID')
                        <span class="label label-success">PAID</span>
                    @elseif ($popsafe->status == 'CREATED')
                        <span class="label label-info">CREATED</span>
                    @else
                        <span class="label label-primary">{{ $popsafe->status }}</span>
                    @endif
				</td>
			</tr>
			<tr>
				<td>Locker</td>
				<td>
					<strong>{{ $popsafe->locker_name }}</strong> <br>
					{{ $popsafe->locker_address }} <br>
					{{ $popsafe->locker_address_detail }} <br>
					{{ $popsafe->operational_hours }}
				</td>
			</tr>
			<tr>
				<td>Locker Size / Locker Number</td>
				<td>
					<strong>{{ $popsafe->locker_size }}</strong>
					@if (empty($popsafe->locker_number))
						/ <span class="label label-default">empty</span>
					@else
						/ <span class="label label-success">{{ $popsafe->locker_number }}</span>
					@endif
				</td>
			</tr>
			<tr>
				<td>Transaction Date</td>
				<td>
					{{ date('D, j F Y H:i:s',strtotime($popsafe->transaction_date)) }}
				</td>
			</tr>
			<tr>
				<td>Cancellation Time</td>
				<td>{{ date('D, j F Y H:i:s',strtotime($popsafe->cancellation_time)) }}</td>
			</tr>
			<tr>
				<td>Expired Date</td>
				<td>{{ date('D, j F Y H:i:s',strtotime($popsafe->expired_time)) }}</td>
			</tr>
			<tr>
				<td>Auto Extend</td>
				<td>
					@if ($popsafe->auto_extend == 0)
						<span class="label label-danger">FALSE</span>
					@elseif ($popsafe->auto_extend == 1)
						<span class="label label-success">TRUE</span>
					@endif
					@if (!empty($popsafe->last_extended_datetime))
						<strong>Last Extended Time</strong> : {{ $popsafe->last_extended_datetime }}
					@endif
				</td>
			</tr>
			<tr>
				<td>PIN</td>
				<td>
					@if (empty($popsafe->pin))
					 	<span class="label label-default">empty</span>
					@else
						<strong>{{ $popsafe->pin }}</strong>
					@endif
				</td>
			</tr>
			@if (!empty($popsafe->item_photo))
				<tr>
					<td>Item Photo</td>
					<td colspan="2">
						<img src="{{ env('POPSEND_URL') }}/{{ $popsafe->item_photo }}" height="300px">
					</td>
				</tr>
			@endif
			<tr>
				<td>Histories</td>
				<td>
					<table class="table table-bordered">
						<thead>
							<tr>
								<td>User</td>
								<td>Status</td>
								<td>Remarks</td>
								<td>Date</td>
							</tr>
						</thead>
						<tbody>
							@foreach ($popsafe->histories as $element)
								<tr>
									<td>{{ $element->user }}</td>
									<td>
										<span class="label label-primary">{{ $element->status }}</span>
									</td>
									<td>{{ $element->remarks }}</td>
									<td>{{ date('Y-m-d H:i:s',strtotime($element->created_at)) }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</td>
			</tr>
        </table>
	</div>
</div>