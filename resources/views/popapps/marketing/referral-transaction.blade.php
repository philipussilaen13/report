@extends('layout.main')

@section('title')
	Agent Referral Transaction
@endsection

@section('css')
	{{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
	Agent Referral Transaction
@endsection

@section('pageDesc')
	
@endsection

@section('content')
	<div class="box box-solid">
		<div class="box-body">
			<form>
				<div class="row">
					<div class="col-md-2">
						<label>Date Range</label>
						<input type="text" name="dateRange" class="form-control" id="dateRange">
					</div>
					<div class="col-md-2">
						<label>Code</label>
						<input type="text" name="code" class="form-control">
					</div>
					<div class="col-md-2">
						<label>From Agent</label>
						<select class="form-control select2" name="fromAgent">
							<option value="">All Agent</option>
							@foreach ($fromLockerDb as $item)
								<option value="{{ $item->id }}">{{ $item->locker_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-2">
						<label>To Agent</label>
						<select class="form-control select2" name="toAgent">
							<option value="">All Agent</option>
							@foreach ($toLockerDb as $item)
								<option value="{{ $item->id }}">{{ $item->locker_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-2">
						<label>Type</label>
						<select class="form-control select2" name="type">
							<option value="">All Type</option>
							@foreach ($typeList as $item)
								<option value="{{ $item }}">{{ $item }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-1">
						<label>Status</label>
						<select class="form-control select2" name="status">
							<option value="">All Status</option>
							@foreach ($statusList as $item)
								<option value="{{ $item }}">{{ $item }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-1">
						<br>
						<button class="btn btn-success btn-flat">Filter</button>
					</div>
				</div>
			</form>
			<div class=" table-responsive">
				<table class="table table-condensed table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Campaign</th>
							<th>Code</th>
							<th>From Agent</th>
							<th>To Agent</th>
							<th>Type</th>
							<th>From Amount</th>
							<th>To Amount</th>
							<th>Status</th>
							<th>Submit Date / Used Date</th>
							<th>Expired Date</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($referralTransactionDb as $index => $item)
							<tr>
								<td>{{ $referralTransactionDb->firstItem() + $index }}</td>
								<td>{{ $item->referralCampaign->name }}</td>
								<td>{{ strtoupper($item->code) }}</td>
								<td>
									@if (!empty($item->from_locker_id))
										<a href="{{ url('agent/list/detail')."/$item->from_locker_id" }}">{{ $item->from_locker_id }}</a> <br>
										{{ $item->fromAgent->locker_name }}
									@endif
								</td>
								<td>
									@if (!empty($item->to_locker_id))
										<a href="{{ url('agent/list/detail')."/$item->to_locker_id" }}">{{ $item->to_locker_id }}</a> <br>
										{{ $item->toAgent->locker_name }}
									@endif
								</td>
								<td>
									<span class="label label-info">{{ $item->type }}</span> <br>
									{{ $item->referralCampaign->rule }}
								</td>
								<td>{{ number_format($item->from_amount) }}</td>
								<td>{{ number_format($item->to_amount) }}</td>
								<td>
									@if ($item->status == 'PENDING')
										<span class="label label-default">{{ $item->status }}</span>
									@elseif ($item->status == 'USED')
										<span class="label label-success">{{ $item->status }}</span>
									@elseif ($item->status == 'FAILED')
										<span class="label label-danger">{{ $item->status }}</span>
									@endif
								</td>
								<td>
									{{ date('Y-m-d H:i:s',strtotime($item->submit_date)) }} <br>
									@if (!empty($item->used_date))
										{{ date('Y-m-d H:i:s',strtotime($item->used_date)) }}
									@endif
								</td>
								<td>{{ date('Y-m-d H:i:s',strtotime($item->expired_date)) }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{{ $referralTransactionDb->links() }}
			<strong>Total : </strong> {{ $referralTransactionDb->total() }}
		</div>
	</div>
@endsection

@section('js')
	{{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['status']))
            	$('select[name=status]').val("{{ $parameter['status'] }}").trigger('change');
            @endif
            @if (!empty($parameter['type']))
            	$('select[name="type[]"]').val({!! json_encode($parameter['type']) !!}).trigger('change');
            @endif
        });
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
        });
    </script>
@endsection