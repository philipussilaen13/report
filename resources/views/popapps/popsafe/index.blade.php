@extends('layout.main')

@section('title')
    PopSafe List
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('pageTitle')
    @if (in_array($user, $groupUser)) 
        Parcel List
    @else
        Dashboard PopSafe
    @endif
@endsection

@section('pageDesc')
@if (in_array($user, $groupUser)) 
        List parcel has been created at locker
    @else
        PopSafe List
    @endif
@endsection

@section('content')
    {{-- DISABLE --}}
    <div class="row">
        <div class="col-md-12">

        <div class="box box-solid box-topall-type">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-1">
                            <h3>Summary</h3>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                <div class="row col-md-12" style="margin-left: 5px">
                    <h4>Transaction Date : <span id="transaction-date"></span></h4>
            {{-- CREATED --}}
            <div class="col-md-2">
                <a href="javascript:void(0);" onclick="widgetClick(1)">
                    <div id="box-created" class="box box-solid box-created" style="background-color: #B5BBC8; height: 135px">
                        <div class="box-body border-radius-none" style="height: 135px;">
                            <div style="margin-top: 0px; margin-left: 20px; color: white">CREATED</div>
                            <div id="created-count" style="margin-top: -5px; margin-left: 20px; font-size: 35px; color: white">0</div>
                            <div style="margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                            <div style="font-size: 12px; margin-top: 10px; margin-left: 20px; color: white">Created</div>
                        </div>
                    </div>
                </a>
            </div>
            {{-- EXPIRED --}}
            <div class="col-md-2">
                <a href="javascript:void(0);" onclick="widgetClick(2)">
                    <div id="box-expired" class="box box-solid box-expired" style="background-color: #001F3F; height: 135px">
                        <div class="box-body border-radius-none" style="height: 135px;">
                            <div style="margin-top: 0px; margin-left: 20px; color: white">Expired</div>
                            <div id="expired-count" style="margin-top: -5px; margin-left: 20px; font-size: 35px; color: white">0</div>
                            <div style="margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                            <div style="font-size: 12px; margin-top: 10px; margin-left: 20px; margin-right: 10px; color: white">not yet put in the locker, exceed the time limit</div>
                        </div>
                    </div>
                </a>
            </div>
            {{-- CANCEL --}}
            <div class="col-md-2">
                <a href="javascript:void(0);" onclick="widgetClick(3)">
                    <div id="box-cancel" class="box box-solid box-cancel" style="background-color: #DB8B17; height: 135px">
                        <div class="box-body border-radius-none" style="height: 135px;">
                            <div style="margin-top: 0px; margin-left: 20px; color: white">CANCEL</div>
                            <div id="cancel-count" style="margin-top: -5px; margin-left: 20px; font-size: 35px; color: white">0</div>
                            <div style="margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                            <div style="font-size: 12px; margin-top: 10px; margin-left: 20px; color: white">< 1 hour and cancelled by user</div>
                        </div>
                    </div>
                </a>
            </div>
            {{-- IN STORE --}}
            <div class="col-md-2">
                <a href="javascript:void(0);" onclick="widgetClick(4)">
                    <div id="box-instore" class="box box-solid box-instore" style="background-color: #347CA5; height: 135px">
                        <div class="box-body border-radius-none" style="height: 135px;">
                            <div style="margin-top: 0px; margin-left: 20px; color: white">IN STORE</div>
                            <div id="instore-count" style="margin-top: -5px; margin-left: 20px; font-size: 35px; color: white">0</div>
                            <div style="margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                            <div style="font-size: 12px; margin-top: 10px; margin-left: 20px; color: white">parcel in the locker</div>
                        </div>
                    </div>
                </a>
            </div>
            {{-- OVERDUE --}}
            <div class="col-md-2">
                <a href="javascript:void(0);" onclick="widgetClick(5)">
                    <div id="box-overdue" class="box box-solid box-overdue" style="background-color: #D33724; height: 135px">
                        <div class="box-body border-radius-none" style="height: 135px;">
                            <div style="margin-top: 0px; margin-left: 20px; color: white">OVERDUE</div>
                            <div id="overdue-count" style="margin-top: -5px; margin-left: 20px; font-size: 35px; color: white">0</div>
                            <div style="margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                            <div style="font-size: 12px; margin-top: 10px; margin-left: 20px; color: white">parcel in the locker and need to extend</div>
                        </div>
                    </div>
                </a>
            </div>
            {{-- COMPLETE --}}
            <div class="col-md-2">
                <a href="javascript:void(0);" onclick="widgetClick(6)">
                    <div id="box-complete" class="box box-solid box-complete" style="background-color: #018D4C; height: 135px">
                        <div class="box-body border-radius-none" style="height: 135px;">
                            <div style="margin-top: 0px; margin-left: 20px; color: white">COMPLETE</div>
                            <div id="complete-count" style="margin-top: -5px; margin-left: 20px; font-size: 35px; color: white">0</div>
                            <div style="margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                            <div style="font-size: 12px; margin-top: 10px; margin-left: 20px; color: white">has been taken by user</div>
                        </div>
                    </div>
                </a>
            </div>
            </div></div></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-topall-type">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-1">
                            <h3>Filter</h3>
                        </div>
                        <div class="col-md-9">
                            <h3 id="title-filter"></h3>
                        </div>
                        <div class="col-md-2">
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" onclick="showHideBox()" style="margin-top: 20px">
                                Show Filter
                            </button>
                        </div>
                    </div>
                </div>
                <div id="box-filter">
                    {{-- FIRST ROW --}}
                    <div class="row col-md-12" style="margin-left: 5px">
                       @if (!in_array($user, $groupUser)) 
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Country</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="country" name="country" style="width: 100%">
                                        <option value="">All Country</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="MY">Malaysia</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif
                       
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Status</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="status" name="status" style="width: 100%">
                                        <option value="">All Status</option>
                                        <option value="created">CREATED</option>
                                        <option value="expired">EXPIRED</option>
                                        <option value="cancel">CANCEL</option>
                                        <option value="in store">IN STORE</option>
                                        <option value="overdue">OVERDUE</option>
                                        <option value="complete">COMPLETE</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>User</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="user" name="user" style="width: 100%">

                                    </select>
                                </div>
                            </div>
                        </div>
                        @if (in_array($user, $groupUser))
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Location</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <select class="form-control select2" id="country" name="country" style="width: 100%">
                                        <option value="">Locker Location</option>
                                        @foreach($lockerLocation as $value)
                                            <option value="{{ $value->locker_name }} ">{{$value->locker_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div> 
                        @endif
                    </div>
                    
                    <div class="row col-md-12" style="margin-left: 5px">
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Transaction Date</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="daterange-transactions" name="dateRange-transactions" placeholder="Transaction Date">
                                </div>
                            </div>
                        </div>
                        @if (!in_array($user, $groupUser))
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Promo</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" id="promocode" name="promocode" class="form-control" placeholder="Promo Code" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-4" style="padding-left: 15px">
                            <h5>Inovice ID</h5>
                            <div class="row" style="margin-left: 1px">
                                <div class="form-group">
                                    <input type="text" id="invoiceid" name="invoiceid" class="form-control" placeholder="Inovice ID" style="width: 100%">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer" style="margin-left: 15px; margin-right: 15px;">
                        <div class="form-group">
                            <button id="btn-filter-submit" type="button" class="btn btn-primary btn-sm" onclick="submitFilter()" style="margin-top: 15px; margin-left: 10px">
                                Filter
                            </button>
                            <button id="btn-reset-filter" type="button" class="btn btn-danger btn-sm" onclick="resetFilter()" style="margin-top: 15px">
                                Reset
                            </button>
                        </div>
                    </div>
                </div>
                {{--<div id="box-filterr" class="row">`--}}
                    {{----}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    {{-- Table List --}}
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-topall-type">
                <div class="box-body border-radius-none">
                    <div class="box-header with-border" style="margin-top: -30px">
                        <div class="col-md-6">
                            @if (in_array($user, $groupUser)) 
                                <h3>Parcel List</h3>
                            @else 
                                <h3>PopSafe List</h3>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" onclick="getAjaxDownloadPopsafe()" style="margin-top: 20px">
                                Download
                            </button>
                        </div>
                    </div>
                    <div id="top-all-type" class="box-body">
                        <table id="table-all-type" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Country</th>
                                <th>Invoice ID</th>
                                <th>Status</th>
                                <th>User</th>
                                <th>Location</th>
                                <th>Locker</th>
                                <th>Transaction Date</th>
                                <th>Price</th>
                                <th>Promo Amount</th>
                                <th>Paid Amount</th>
                                <th>Campaign Name</th>
                                <th>Promo Code</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    {{-- NUMERATOR --}}
    <script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>

    <script type="text/javascript">

        // GROUPNAME OF USER 
        var userGroup = "<?php echo $user ?>";
        var groupUser = <?php echo json_encode($groupUser) ?>;

        var dataDropDown;

        var transactionDateRange = '';

        var isFirstOpeningPage = 'true';

        var dataFilterToDownload = [];

        var tableAllType;

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "currency-cust-pre": function (a) {
                /* Remove Rp text */
                // var x = String(a).replace("Rp", "");

                /* Remove comma and ,00 */
                // x = x.replace(",00", "");

                var x = OSREC.CurrencyFormatter.parse(String(a), { locale: 'id_ID' });

                x = replaceAll(String(x), ".", "");

                // console.log('currency --> ' + x);

                /* Parse and return */
                return parseFloat(x);
            },

            "date-cust-pre": function (a) {

                var x = moment(a).format('YYYY-MM-DD HH:mm:ss');

                return x;
            },

            "currency-cust-asc": function (a, b) {
                return a - b;
            },

            "currency-cust-desc": function (a, b) {
                return b - a;
            },

            "date-cust-asc": function (a, b) {
                return a.diff(b);
            },

            "date-cust-desc": function (a, b) {
                return b.diff(a);
            }
        });

        jQuery(document).ready(function ($) {
            $('#box-filter').hide(10);
            $('#box-filterr').hide(10);
            $('.select2').select2();

            getAjaxAllUser();
            dateRangeTransactions();

        });

        function submitFilter() {

            isFirstOpeningPage = 'false';
            // getDefaultData(getDataForFilter()[0]);
            // tableData();

            getDefaultData();
            getAjaxPopsafeLiveStatus();
            filterTitle();
        }

        function resetFilter() {

            $('#country').val('').trigger('change');
            $('#status').val('').trigger('change');

            // $('#user').val('').trigger('change');
            insertDataToDropdownSelect2(dataDropDown);

            $('#promocode').val('');
            $('#invoiceid').val('');

            $('#daterange-transactions').val('');
            transactionDateRange = '';

            resetFilterTitle();
        }

        function widgetClick(value) {

            if (value === 1) {
                $('#status').val('created').trigger('change');
            } else if (value === 2) {
                $('#status').val('expired').trigger('change');
            } else if (value === 3) {
                $('#status').val('cancel').trigger('change');
            } else if (value === 4) {
                $('#status').val('in store').trigger('change');
            } else if (value === 5) {
                $('#status').val('overdue').trigger('change');
            }  else if (value === 6) {
                $('#status').val('complete').trigger('change');
            }

            submitFilter();

        }

        function getDataForFilter() {

            // console.log($('#type').select2("data")[0].id);
            // console.log($('#status').select2("data")[0].id);
            // console.log($('#sales').select2("data")[0].id);
            // console.log($('#province').select2("data")[0].id);
            // console.log($('#city').select2("data")[0].id);
            // console.log($('#name')[0].value);
            // console.log($('#agentid')[0].value);
            
            var country = '';
            var promocode = '';
            if ($("#country").length == 0) {
                country = 'ID';
            } else {
                country = $('#country').select2("data")[0].id;
            }

            var status = $('#status').select2("data")[0].id;
            var user = $('#user').select2("data")[0].id;
            var invoiceId = $('#invoiceid')[0].value;

            if (groupUser.includes(userGroup) == false) {
                promocode = $('#promocode')[0].value;
            }
            
            var result = [];
            result.push({
                token: '{{ csrf_token() }}',
                country : country,
                status : status,
                user : user,
                promocode : promocode,
                invoiceid : invoiceId,
                transactionDateRangee : transactionDateRange
            });

            return result[0];
        }

        function getDefaultData() {

            var value = getDataForFilter();

            showLoading('.box-topall-type', 'box-topall-type');

            dataFilterToDownload = [];

            $.ajax({
                url: '{{ url('popapps/popsafe/ajax/getAjaxFilterPopsafe') }}',
                data: {
                    _token : value.token,
                    country : value.country,
                    status : value.status,
                    user : value.user,
                    promocode : value.promocode,
                    invoiceid : value.invoiceid,
                    transaction_date : value.transactionDateRangee,
                    user_groupname : userGroup
                },
                type: 'POST',
                success: function(data){
                    if (data.isSuccess === true) {
                        // console.log('FILTER POPSAFE');
                        if (data.data.resultList.length > 0) {
                            tableAllType = $('#table-all-type').DataTable({
                                data          : data.data.resultList,
                                'destroy'     : true,
                                'retreive'    : true,
                                'paging'      : true,
                                'searching'   : true,
                                'ordering'    : true,
                                'autoWidth'   : false,
                                'responsive'  : true,
                                'processing'  : true,
                                'scrollY'     : 500,
                                'deferRender' : true,
                                'scroller'    : true,
                                "pageLength"  : 50,
                                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                'order': [[ 6, "desc" ]],
                                'columnDefs': [
                                    { type: 'date-cust', targets: 6 },
                                    { type: 'currency-cust', targets: 7 },
                                    { type: 'currency-cust', targets: 8 },
                                    { type: 'currency-cust', targets: 9 },
                                    {
                                        "targets": 0,
                                        "data": "download_link",
                                        "render": function ( data, type, full, meta ) {
                                            if (full.country === 'ID') {
                                                return 'Indonesia'
                                            } else {
                                                return 'Malaysia'
                                            }
                                        }
                                    },
                                    {
                                        "targets": 1,
                                        "render": function ( data, type, full, meta ) {
                                            if (groupUser.includes(userGroup) == true) {
                                                return '<a href=" {{ url('angkasapura/parcelist/getDetailPage') }}/'+full.invoice_code+'"> '+full.invoice_code+' </a>';
                                                
                                            } else {
                                                return '<a href=" {{ url('popapps/popsafe/getDetailPage') }}/'+full.invoice_code+'"> '+full.invoice_code+' </a>';
                                            }
                                        }
                                    },
                                    {
                                        "targets": 2,
                                        "render": function ( data, type, full, meta ) {
                                            if (full.status === 'CREATED') {
                                                return '<span class="label span_type_created">'+full.status+'</span>'
                                            } else if (full.status === 'EXPIRED') {
                                                return '<span class="label span_type_expired">'+full.status+'</span>'
                                            }  else if (full.status === 'CANCEL') {
                                                return '<span class="label span_status_cancel">'+full.status+'</span>'
                                            }  else if (full.status === 'IN STORE') {
                                                return '<span class="label span_status_instore">'+full.status+'</span>'
                                            }  else if (full.status === 'OVERDUE') {
                                                return '<span class="label span_status_overdue">'+full.status+'</span>'
                                            }  else if (full.status === 'COMPLETE') {
                                                return '<span class="label span_status_complete">'+full.status+'</span>'
                                            } else {
                                                return full.status;
                                            }
                                        }
                                    },
                                    {
                                        "targets": 3,
                                        "render": function ( data, type, full, meta ) {
                                            var string = full.name + '<br>' + full.phone;
                                            return '<a href=" {{ url('popapps/user/detail') }}/'+full.member_id+'"> '+string+' </a>';
                                        }
                                    },
                                    {
                                        "targets": 4,
                                        "render": function ( data, type, full, meta ) {
                                            return full.locker_name;
                                        }
                                    },
                                    {
                                        "targets": 5,
                                        "render": function ( data, type, full, meta ) {
                                            var a = full.locker_size + '/' + full.locker_number;
                                            return a;
                                        }
                                    },
                                    {
                                        "targets": 6,
                                        "render": function ( data, type, full, meta ) {
                                            return moment(full.created_at).format('DD MMM YYYY HH:mm:ss');
                                            // return full.created_at;
                                        }
                                    },
                                    {
                                        "targets": 7,
                                        "render": function ( data, type, full, meta ) {
                                            return numberWithCommas(full.total_price);
                                        }
                                    },
                                    {
                                        "targets": 8,
                                        "render": function ( data, type, full, meta ) {
                                            return numberWithCommas(full.promo_amount);
                                        }
                                    },
                                    {
                                        "targets": 9,
                                        "render": function ( data, type, full, meta ) {
                                            return numberWithCommas(full.paid_amount);
                                        }
                                    },
                                    {
                                        "targets": 10,
                                        "render": function ( data, type, full, meta ) {
                                                var campaignName = '';
                                                if (full.campaign_name != null) {
                                                    campaignName = '<a href="{{ url('popapps/marketing/campaign/edit') }}/'+full.campaign_id+'" target="_blank"> '+full.campaign_name+' </a>';
                                                } 
                                                return campaignName; 
                                        }
                                    },
                                    {
                                        "targets": 11,
                                        "render": function ( data, type, full, meta ) {
                                            if (full.code === null) {
                                                return '';
                                            } else {
                                                return full.code;
                                            }
                                        }
                                    }
                                ]
                            });

                        } else {
                            $('#table-all-type').DataTable().clear().draw(false);
                        }

                    }  else {
                        alert(data.errorMsg);
                    }

                    filterTitle();
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-topall-type', 'box-topall-type');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        function getAjaxPopsafeLiveStatus() {
            showLoading('.box-created', 'box-created');
            showLoading('.box-expired', 'box-expired');
            showLoading('.box-cancel', 'box-cancel');
            showLoading('.box-instore', 'box-instore');
            showLoading('.box-overdue', 'box-overdue');
            showLoading('.box-complete', 'box-complete');

            var value = getDataForFilter();

            $.ajax({
                url: '{{ url('popapps/popsafe/ajax/getLiveStatus') }}',
                data: {
                    _token : value.token,
                    transaction_date : value.transactionDateRangee,
                    user_groupname: userGroup
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        hideLoading('.box-created', 'box-created');
                        hideLoading('.box-expired', 'box-expired');
                        hideLoading('.box-cancel', 'box-cancel');
                        hideLoading('.box-instore', 'box-instore');
                        hideLoading('.box-overdue', 'box-overdue');
                        hideLoading('.box-complete', 'box-complete');

                        $('#created-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.created });

                        $('#expired-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.expired });

                        $('#cancel-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.cancel });

                        $('#instore-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.in_store });

                        $('#overdue-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.overdue });

                        $('#complete-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.complete });

                    }  else {
                        alert(data.errorMsg);
                    }

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');
                })
                .always(function() {
                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        function getAjaxAllUser() {

            showLoading('.box-filter', 'box-filter');
            showLoading('.box-filterr', 'box-filterr');
            showLoading('.box-topall-type', 'box-topall-type');

            showLoading('.box-created', 'box-created');
            showLoading('.box-expired', 'box-expired');
            showLoading('.box-cancel', 'box-cancel');
            showLoading('.box-instore', 'box-instore');
            showLoading('.box-overdue', 'box-overdue');
            showLoading('.box-complete', 'box-complete');

            $.ajax({
                url: '{{ url('popapps/popsafe/ajax/getAllUser') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log('data_user: ' + data.data.allUser.user);
                        
                        insertDataToDropdownSelect2(data.data.allUser);

                    }  else {
                        alert(data.errorMsg);
                    }

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');
                })
                .always(function() {
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function getAjaxDownloadPopsafe() {
            var value = getDataForFilter();

            showLoading('.box-filter', 'box-filter');
            showLoading('.box-filterr', 'box-filterr');
            showLoading('.box-topall-type', 'box-topall-type');

            showLoading('.box-created', 'box-created');
            showLoading('.box-expired', 'box-expired');
            showLoading('.box-cancel', 'box-cancel');
            showLoading('.box-instore', 'box-instore');
            showLoading('.box-overdue', 'box-overdue');
            showLoading('.box-complete', 'box-complete');

            $.ajax({
                url: '{{ url('popapps/popsafe/ajax/getAjaxDownloadPopsafe') }}',
                data: {
                    _token : value.token,
                    country : value.country,
                    status : value.status,
                    user : value.user,
                    userGroup : userGroup,
                    groupUser : groupUser,
                    promocode : value.promocode,
                    transaction_date : value.transactionDateRangee
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        window.location = '{{ url("/popapps/popsafe/ajax/download") }}' + '?file=' + data.data.link;

                    }  else {
                        alert(data.errorMsg);
                    }

                    hideLoading('.box-topall-type', 'box-topall-type');
                },
                error: function(data){
                    console.log('error');
                    console.log(data);

                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');

                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');
                }
            })
                .done(function() {
                    console.log("success - download");

                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');

                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');
                })
                .fail(function() {
                    console.log("error - download");

                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');

                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');
                })
                .always(function() {
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-filterr', 'box-filterr');
                    hideLoading('.box-topall-type', 'box-topall-type');

                    hideLoading('.box-created', 'box-created');
                    hideLoading('.box-expired', 'box-expired');
                    hideLoading('.box-cancel', 'box-cancel');
                    hideLoading('.box-instore', 'box-instore');
                    hideLoading('.box-overdue', 'box-overdue');
                    hideLoading('.box-complete', 'box-complete');

                    console.log("complete - download");
                    console.log('===================');
                });

        }

        function tableData() {

            var value = getDataForFilter()[0];

            // "data": function (d) {
            //     // console.log(d);
            //     d._token = value.token;
            //     d.type = value.type;
            //     d.status = value.status;
            //     d.sales = value.sales;
            //     d.province = value.province;
            //     d.city = value.city;
            //     d.name = value.name;
            //     d.agentid = value.agentid;
            //     d.daterange_transaction = value.transactionDateRangee;
            //     d.daterange_registered = value.registeredDateRangee;
            //     d.firstOpeningPage = value.firstOpeningPage;
            // }

            tableAllType = $('#table-all-type').DataTable({
                'destroy'     : true,
                'retreive'    : true,
                'paging'      : true,
                'searching'   : true,
                'ordering'    : true,
                'autoWidth'   : false,
                'responsive'  : true,
                'processing'  : true,
                "serverSide"  : true,
                'scrollY'     : 500,
                'deferRender' : true,
                'scroller'    : true,
                "pageLength"  : 50,
                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                'order': [[ 1, "asc" ]],
                "ajax": {
                    url: '{{ url('agent/ajax/getAjaxAllType') }}',
                    type: 'POST',
                    dataSrc: 'data.resultList',
                    data: {
                        _token : value.token,
                        type : value.type,
                        status : value.status,
                        sales : value.sales,
                        province : value.province,
                        city : value.city,
                        name : value.name,
                        agentid : value.agentid,
                        daterange_transaction : value.transactionDateRangee,
                        daterange_registered : value.registeredDateRangee,
                        firstOpeningPage : value.firstOpeningPage
                    }
                },
                'columnDefs': [
                    { type: 'currency-cust', targets: 3 },
                    {
                        "targets": 0,
                        "data": "download_link",
                        "render": function ( data, type, full, meta ) {
                            return '<a href=" {{ url('agent/list/detail') }}/'+full.id+'"> '+full.id+' </a>';
                            //     return '<a href="javascript:void(0);" onclick="openInNewTab(+full[0]+)"> '+full[0]+' </a>';
                            // return openInNewTab(full[0]);
                        }
                    },
                    {
                        "targets": 1,
                        "render": function ( data, type, full, meta ) {
                            return full.name;
                        }
                    },
                    {
                        "targets": 2,
                        "render": function ( data, type, full, meta ) {
                            if (full.type === 'agent') {
                                return '<span class="label span_type_agent">Agent</span>'
                            } else if (full.type === 'warung') {
                                return '<span class="label span_type_warung">Warung</span>'
                            } else {
                                return ''
                            }
                        }
                    },
                    {
                        "targets": 3,
                        "render": function ( data, type, full, meta ) {
                            return numberWithCommas(full.deposit);
                        }
                    },
                    {
                        "targets": 4,
                        "render": function ( data, type, full, meta ) {
                            return full.city;
                        }
                    },
                    {
                        "targets": 5,
                        "render": function ( data, type, full, meta ) {
                            if (full.status === 'disable') {
                                return '<span class="label span_status_disable">'+full.status+'</span>'
                            } else if (full.status === 'enable') {
                                return '<span class="label span_status_enable">'+full.status+'</span>'
                            } else if (full.status === 'pending') {
                                return '<span class="label span_status_pending">'+full.status+'</span>'
                            } else if (full.status === 'verified') {
                                return '<span class="label span_status_verified">'+full.status+'</span>'
                            } else {
                                return full.status
                            }
                        }
                    },
                    {
                        "targets": 6,
                        "render": function ( data, type, full, meta ) {
                            return full.registeredby;
                        }
                    },
                    {
                        "targets": 7,
                        "render": function ( data, type, full, meta ) {
                            return full.registeredDate;
                        }
                    }
                ]
            });
        }

        function filterTitle() {
            var country = '';
            if ($("#country").length == 0) {
                country = 'ID';
            } else {
                country = $('#country').select2("data")[0].id;
            }

            var status = $('#status').select2("data")[0].text;
            var user = $('#user').select2("data")[0].text;

            var promocode = '';
            
            if (groupUser.includes(userGroup) == false) {
                promocode = $('#promocode')[0].value;
            }


            var space = '&nbsp&nbsp&nbsp&nbsp';
            var defaultTitle = '<span class="label span_filter" style="font-size: 14px">Type: ' +country+ '</span>' +
                space + '<span class="label span_filter" style="font-size: 14px">Status: ' +status+ '</span>' +
                space + '<span class="label span_filter" style="font-size: 14px">User: ' +user+ '</span>';

            if (promocode != '') {
                var promocodeTitle = space + '<span class="label span_filter" style="font-size: 14px">Promo Code: ' +promocode+ '</span>';
            } else {
                var promocodeTitle = '';
            }

            var text = '';
            if (transactionDateRange != '') {
                var str = transactionDateRange.split(',');
                var text = moment(str[0]).format('DD MMM YYYY') + ' - ' + moment(str[1]).format('DD MMM YYYY');
                var transactionDateRangeTitle = space + '<span class="label span_filter" style="font-size: 14px">Transaction Date: ' +text+ '</span>';
            } else {
                var transactionDateRangeTitle = '';
            }

            $('#title-filter').html(defaultTitle + transactionDateRangeTitle + promocodeTitle);
            $('#transaction-date').html(text);
        }

        function resetFilterTitle() {

            var country = $('#country').select2("data")[0].text;
            var status = $('#status').select2("data")[0].text;
            var user = $('#user').select2("data")[0].text;

            var space = '&nbsp&nbsp&nbsp&nbsp';
            var defaultTitle = '<span class="label span_filter" style="font-size: 14px">Type: ' +country+ '</span>' +
                space + '<span class="label span_filter" style="font-size: 14px">Status: ' +status+ '</span>' +
                space + '<span class="label span_filter" style="font-size: 14px">User: ' +user+ '</span>';

            $('#title-filter').html(defaultTitle);
        }

        function dateRangeTransactions() {
            var startDate = moment().startOf('month').format('YYYY-MM-DD');
            var endDate = moment().endOf('month').format('YYYY-MM-DD');
            var inputDateRangeTransaction = $('#daterange-transactions');
            transactionDateRange = startDate + ',' + endDate;

            var startDateToShow = moment().startOf('month').format('DD/MM/YYYY');
            var endDateToShow = moment().endOf('month').format('DD/MM/YYYY');
            $('#daterange-transactions').val(startDateToShow + ' - ' + endDateToShow);

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }

        function dateRangeRegistered() {
            var startDate = moment().format('YYYY-MM-DD');
            var endDate = moment().add(7, 'days').format('YYYY-MM-DD');
            var inputDateRangeRegistered = $('#daterange-registered');

            console.log(startDate);
            console.log(endDate);

            inputDateRangeRegistered.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: startDate,
                endDate: endDate
            });
            inputDateRangeRegistered.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                registeredDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeRegistered.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                registeredDateRange = '';
            });
        }

        function insertDataToDropdownSelect2(data) {
            dataDropDown = data;

            var userDataSelect2 = [];
            userDataSelect2.push({ id: "", text: "All User" });
            data.forEach(function (value) {
                userDataSelect2.push({
                    id: value.id,
                    text: value.name + ' (' + value.phone + ')'
                })
            });

            $('#user').html('').select2({
                data: userDataSelect2
            });


            getDefaultData();
            getAjaxPopsafeLiveStatus();

            hideLoading('.box-filter', 'box-filter');
        }

        var isBoxHide = true;
        function showHideBox() {
            if (isBoxHide) {
                $('#box-filter').show(500);
                $('#box-filterr').show(500);
                $('#btn-show-hide-filter').html('Hide Filter');
            } else {
                $('#box-filter').hide(500);
                $('#box-filterr').hide(500);
                $('#btn-show-hide-filter').html('Show Filter');
            }
            isBoxHide = !isBoxHide;
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID', pattern: '!#,##0' });
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }

    </script>

<style>

    .span_type_created {
        padding: 5px;
        background-color: #B5BBC8;
        display: inline-block;
    }
    .span_type_expired {
        padding: 5px;
        background-color: #001F3F;
        display: inline-block;
    }
    .span_status_cancel {
        padding: 5px;
        background-color: #DB8B17;
        display: inline-block;
    }
    .span_status_instore {
        padding: 5px;
        background-color: #347CA5;
        display: inline-block;
    }
    .span_status_overdue {
        padding: 5px;
        background-color: #D33724;
        display: inline-block;
    }
    .span_status_complete {
        padding: 5px;
        background-color: #018D4C;
        display: inline-block;
    }
    .span_filter {
        padding: 5px;
        background-color: #00c0ef;
        display: inline-block;
    }
</style>

@endsection