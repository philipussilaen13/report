@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
    Transactions Detail
@endsection

@section('css')
    <style type="text/css">
        .changeStatus {
            cursor: pointer;
        }
    </style>
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('pageTitle')
    Transactions Detail
@endsection

@section('pageDesc')
    Transactions Detail
@endsection

@section('content')
<!-- sample modal content -->
<div id="pinModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">PopSafe PIN</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
    <div class="col-md-12">
        <div class="col-md-4 box-popsafe-detail widget-user-2">
            <div class="widget-user-header bg-yellow">
                <h3 id="title-widget" class="widget-user-username"></h3>
                <a href="javascript:void(0);">
                    <h4 id="desc-widget" class="widget-user-desc" style="color: white"></h4>
                </a>
            </div>
            <div class="box box-body">
                <table id="table-popsafe" class="table table-striped">

                </table>
            </div>
        </div>
        <div class="col-md-8">
            <div class="col-md-12 box-popsafe-histories widget-user-2">
                <div class="widget-user-header bg-gray">
                    <h3 class="widget-user-username">Tracking</h3>
                </div>
                <div class="box box-body">
                    <table id="table-histories-popsafe" class="table table-striped">
                        <thead>
                        <tr>
                            <td>User</td>
                            <td>Status</td>
                            <td>Remarks</td>
                            <td>Date</td>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="col-md-12 box-popsafe-histories widget-user-2">
                <div class="widget-user-header bg-gray">
                    <h3 class="widget-user-username">Transactions History</h3>
                </div>
                <div class="box box-body">
                    <table id="table-transactions-popsafe" class="table table-striped">
                        <thead>
                        <tr>
                            <td>Invoice ID</td>
                            <td>Status</td>
                            <td>Description</td>
                            <td>Total Price</td>
                            <td>Paid Amount</td>
                            <td>Promo Amount</td>
                            <td>Refund Amount</td>
                            <td>Created At</td>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script>
    
    </script>
    <script type="text/javascript">
        var userGroup = "{{$user}}";
        var groupUser = '<?php echo json_encode($groupUser) ?>';
        console.log(groupUser, 'groupuser');
        var popsafeId = '';

        $(document).ready(function () {

            var code = '{{$invoice_code}}';
            console.log(code);

            getDetailPopsafe(code);
        });

        function getDetailPin() {
                $.ajax({
                    url: "{{ url('angkasapura/parcelist/getDetailPin').'/'.$invoice_code }}",
                    success: function(response){    
                        console.log(response);
                        if(response.permission == true) {
                            if(response.code_pin[0] != null) {
                                $(".modal-body").html("<p>Here is the code to open the locker<br>PopSafe ID : <b>"+response.invoice_code+"</b><br>PIN : <h4 style='text-align: center; font-size: 35px;'><b>"+ response.code_pin[0] +"</b></h4></p>");
                            } else {
                                $(".modal-body").html(" <b>Sorry, the order's PIN isn't available.</b>");
                            }
                        } else {
                            $(".modal-body").html(" <b>Sorry, you don't have permission to view the order's PIN.</b>");
                        }
                    },
                    error: function(e) {
                        alert('Error: ' + e);
                    }
                });
        }

        function getDetailPopsafe(value) {
            showLoading('.box-popsafe-detail', 'box-popsafe-detail');
            
            $.ajax({
                url: '{{ url('popapps/popsafe/ajax/getDetailPopsafe') }}',
                data: {
                    _token : '{{ csrf_token() }}',
                    invoice_id : value
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        var popsafe = data.data.popsafe;
                        popsafeId = popsafe.id;

                        getDetailHistoriesPopsafe();
                        getDetailTransactionPopsafe();

                        var invoiceCode = popsafe.invoice_code;
                        var name = popsafe.name + '<br>' + popsafe.phone ;
                        {{--var urlName = '<a href="{{ url('popapps/user/detail') }}/{{ popsafe.member_id }}"> '+ name +' </a>';--}}

                        $('#title-widget').html('<strong>PopSafe</strong>\n' + '<br>' + invoiceCode);

                        $('#desc-widget').html(name);
                        $('#desc-widget').click(function () {
                            var win = window.open('{{url('popapps/user/detail')}}/'+popsafe.member_id, '_blank');
                            win.focus();
                        });

                        var transactionDate = moment(popsafe.transaction_date).format('ddd, DD MMM YYYY HH:mm:ss');
                        var cancellationTime = moment(popsafe.cancellation_time).format('ddd, DD MMM YYYY HH:mm:ss');
                        var expiredDate = moment(popsafe.expired_time).format('ddd, DD MMM YYYY HH:mm:ss');

                        // '<tr><td height="40px">PIN</td><td><span class="label label-default">'+ popsafe.code_pin +'</span></td></tr>'
                        $('#table-popsafe').html(
                            '<tr><td width="200px" height="40px">Status</td><td><span class="label label-primary">'+ popsafe.status +'</span></td></tr>'
                            + '<tr><td height="40px">Locker</td><td><strong>'+ popsafe.locker_name+ '</strong>'
                            + '<br>' + popsafe.locker_address + '<br>' + popsafe.locker_address_detail + '<br>' + popsafe.operational_hours + '</td></tr>'
                            + '<tr><td height="40px">Locker Size/Locker Number</td><td>'+ popsafe.locker_size + ' / ' + popsafe.locker_number +'</td></tr>'
                            + '<tr><td height="40px">Transaction Date</td><td>'+ transactionDate +'</td></tr>'
                            + '<tr><td height="40px">Cancellation Time</td><td>'+ cancellationTime +'</td></tr>'
                            + '<tr><td height="40px">Expired Date</td><td>'+ expiredDate +'</td></tr>'
                            + '<tr><td height="40px">Auto Extend</td><td><span class="label label-danger">FALSE</span></td></tr>'
                            + '<tr><td height="40px">PIN</td><td><button id="btnPinModal" onClick="getDetailPin()" class="btn waves-effect waves-light btn-warning" role="button" data-toggle="modal" data-target="#pinModal">Show PIN</button></td></tr>'
                        )

                    }  else {
                        alert(data.errorMsg);
                    }

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    hideLoading('.box-popsafe-detail', 'box-popsafe-detail');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    hideLoading('.box-popsafe-detail', 'box-popsafe-detail');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    hideLoading('.box-popsafe-detail', 'box-popsafe-detail');
                })
                .always(function() {
                    hideLoading('.box-popsafe-detail', 'box-popsafe-detail');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function getDetailHistoriesPopsafe() {

            showLoading('.box-popsafe-histories', 'box-popsafe-histories');

            $.ajax({
                url: '{{ url('popapps/popsafe/ajax/getDetailHistoriesPopsafe') }}',
                data: {
                    _token : '{{ csrf_token() }}',
                    popsafe_id : popsafeId
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        $('#table-histories-popsafe').DataTable({
                            data          : data.data.popsafeHistories,
                            'destroy'     : true,
                            'retreive'    : true,
                            'paging'      : false,
                            'searching'   : false,
                            'ordering'    : false,
                            'autoWidth'   : false,
                            'processing'  : false,
                            'responsive'  : true,
                            'columnDefs': [
                                {
                                    "targets": 0,
                                    "render": function ( data, type, full, meta ) {
                                        return full.user;
                                    }
                                },
                                {
                                    "targets": 1,
                                    "render": function ( data, type, full, meta ) {
                                        // return '<span class="label label-primary">'+full.status+'</span>';
                                        if (full.status === 'CREATED') {
                                            return '<span class="label span_type_created">'+full.status+'</span>'
                                        } else if (full.status === 'EXPIRED') {
                                            return '<span class="label span_type_expired">'+full.status+'</span>'
                                        }  else if (full.status === 'CANCEL') {
                                            return '<span class="label span_status_cancel">'+full.status+'</span>'
                                        }  else if (full.status === 'IN STORE') {
                                            return '<span class="label span_status_instore">'+full.status+'</span>'
                                        }  else if (full.status === 'OVERDUE') {
                                            return '<span class="label span_status_overdue">'+full.status+'</span>'
                                        }  else if (full.status === 'COMPLETE') {
                                            return '<span class="label span_status_complete">'+full.status+'</span>'
                                        } else {
                                            return full.status;
                                        }
                                    }
                                },
                                {
                                    "targets": 2,
                                    "render": function ( data, type, full, meta ) {
                                        if (full.remarks === null) {
                                            return '';
                                        } else {
                                            return full.remarks;
                                        }
                                    }
                                },
                                {
                                    "targets": 3,
                                    "render": function ( data, type, full, meta ) {
                                        return moment(full.updated_at).format('ddd, D MMM YYYY HH:mm:ss');
                                    }
                                }
                            ]
                        });



                    }  else {
                        alert(data.errorMsg);
                    }

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                })
                .always(function() {
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        function getDetailTransactionPopsafe() {

            showLoading('.box-popsafe-histories', 'box-popsafe-histories');

            $.ajax({
                url: '{{ url('popapps/popsafe/ajax/getDetailTransactionsPopsafe') }}',
                data: {
                    _token : '{{ csrf_token() }}',
                    popsafe_id : popsafeId
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        $('#table-transactions-popsafe').DataTable({
                            data          : data.data.popsafeTransactions,
                            'destroy'     : true,
                            'retreive'    : true,
                            'paging'      : false,
                            'searching'   : false,
                            'ordering'    : false,
                            'autoWidth'   : false,
                            'processing'  : false,
                            'responsive'  : true,
                            'columnDefs': [
                                {
                                    "targets": 0,
                                    "render": function ( data, type, full, meta ) {
                                        if (groupUser.includes(userGroup)) {
                                            return '<a href=" {{ url('angkasapura/transaction/detail') }}/'+full.invoice_id+'"> '+full.invoice_id+' </a>';
                                        } else {
                                            return '<a href=" {{ url('popapps/transaction/detail') }}/'+full.invoice_id+'"> '+full.invoice_id+' </a>';
                                        }
                                    }
                                },
                                {
                                    "targets": 1,
                                    "render": function ( data, type, full, meta ) {
                                        return '<span class="label label-primary">'+full.status+'</span>';
                                    }
                                },
                                {
                                    "targets": 2,
                                    "render": function ( data, type, full, meta ) {
                                        return full.description;
                                    }
                                },
                                {
                                    "targets": 3,
                                    "render": function ( data, type, full, meta ) {
                                        return numberWithCommas(full.total_price);
                                    }
                                },
                                {
                                    "targets": 4,
                                    "render": function ( data, type, full, meta ) {
                                        return numberWithCommas(full.paid_amount);
                                    }
                                },
                                {
                                    "targets": 5,
                                    "render": function ( data, type, full, meta ) {
                                        return numberWithCommas(full.promo_amount);
                                    }
                                },
                                {
                                    "targets": 6,
                                    "render": function ( data, type, full, meta ) {
                                        return numberWithCommas(full.refund_amount);
                                    }
                                },
                                {
                                    "targets": 7,
                                    "render": function ( data, type, full, meta ) {
                                        return moment(full.created_at).format('ddd, D MMM YYYY HH:mm:ss');
                                    }
                                }
                            ]
                        });



                    }  else {
                        alert(data.errorMsg);
                    }

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                })
                .always(function() {
                    hideLoading('.box-popsafe-histories', 'box-popsafe-histories');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID' });
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }

    </script>

    <style>

        .span_type_created {
            padding: 5px;
            background-color: #B5BBC8;
            display: inline-block;
        }
        .span_type_expired {
            padding: 5px;
            background-color: #001F3F;
            display: inline-block;
        }
        .span_status_cancel {
            padding: 5px;
            background-color: #DB8B17;
            display: inline-block;
        }
        .span_status_instore {
            padding: 5px;
            background-color: #347CA5;
            display: inline-block;
        }
        .span_status_overdue {
            padding: 5px;
            background-color: #D33724;
            display: inline-block;
        }
        .span_status_complete {
            padding: 5px;
            background-color: #018D4C;
            display: inline-block;
        }
        .span_filter {
            padding: 5px;
            background-color: #00c0ef;
            display: inline-block;
        }
    </style>

@endsection