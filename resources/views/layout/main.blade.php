<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PopBox Dashboard | @yield('title')</title>
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="a384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

@yield('css')
	<style>
        [v-cloak] {
            display: none;
        }
        .card-box {
            background-color: #ffffff;
            border: 1px solid rgba(98, 103, 115, 0.2);
            margin-bottom: 20px;
            border-radius: 9px;
            border-color: rgba(98, 103, 115, 0.3); 
            padding: 5px;
        }
        
        .header-title {
            margin-top: 0;
            font-weight: bold;
            border-bottom: 1px solid gainsboro;
            padding-bottom: 12px;
        }
        
        .header-filter {
            border: 1px solid;
            margin: 5px;
        }
        
        .info-icon-tooltip{
            font-size:20px;
            position:absolute;
            top:7px;
            right:21px;
        }
    </style>
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('css/skins/skin-blue.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/global.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('layout.header')
    
    @include('layout.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('pageTitle')
                <small>@yield('pageDesc')</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            PopBox IT Team
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 17 <a href="https://www.popbox.asia">PopBox Asia Services</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 3 -->
<script src="{{ asset('plugins/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<script type="text/javascript">
    function showLoading(divId, name) {
        if ($("." + name + "_loading").length) {
            $("." + name + "_loading").css({display: "block"});
        }
        else {
            $("<div class='" + name + "_loading'></div>").css({
                position: "absolute",
                display: "block",
                width: "100%",
                height: "100%",
                top: 0,
                left: 0,
                background: "rgba(255,255,255,.57) url('{{ asset('img/ripple.gif') }}') 50% 50% no-repeat",
                "z-index": 1000
            }).css('background-size', '50px 50px').appendTo($(divId).css("position", "relative"));
        }
    }

    function hideLoading(divId, name) {
        $("." + name + "_loading").css({display: "none"});
    }
</script>

@yield('js')
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

</body>
</html>