@extends('layout.main')

@section('title')
	Privileges
@endsection

@section('css')
	{{-- select 2 --}}
	<link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
	<!-- DataTables -->
  	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('pageTitle')
	Privileges Management
@endsection

@section('pageDesc')
	Change Privileges for Group
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="form-group">
					<select class="form-control select2" id="groupName" name="groupName" style="width: 100%">

					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-topall-type">
				<div class="box-body border-radius-none">
					<div class="box-header with-border" style="margin-top: -30px">
						<div class="col-md-6">
							<h3 id="privileges-title">Privileges: </h3>
						</div>
					</div>
					<div id="privileges-list-label" class="row col-md-12" style="margin-top: 15px">

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
	{{-- Select 2 --}}
	<script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<!-- SlimScroll -->
	<script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{ asset('plugins/moment/min/moment.min.js')}}"></script>

	<script type="text/javascript">

		var dataListGroupModule;

        jQuery(document).ready(function ($) {
            $('.select2').select2();

            console.log('dasdasdad');
            getDataPrivileges();
        });
        
        function getDataPrivileges() {

            $.ajax({
                url: '{{ url('privileges/privilege/v2') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        dataListGroupModule = data.data.groupModuleDB;

                        var listGroupData = data.data.listGroupDB;
                        var listGroupDataSelect2 = [];
                        listGroupData.forEach(function(data){
                            // console.log(data.type);
                            listGroupDataSelect2.push({
                                id: data.id,
                                text: data.name
                            })
                        });

                        $('#groupName').html('').select2({
                            data: listGroupDataSelect2,
                            minimumResultsForSearch: Infinity
                        });

                        updatePrivilegesLabelView(6)

                    }  else {
                        alert(data.errorMsg);
                    }

                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                }
            })
                .done(function() {
                    // console.log("success - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                })
                .fail(function() {
                    // console.log("error - getTopSelling");
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.box-filter', 'box-filter');
                    // console.log("complete - getTopSelling");
                    // console.log('===================');
                });

        }

        function updatePrivilegesLabelView(groupId) {
            var text = '';
            var space = '&nbsp&nbsp&nbsp&nbsp';

            dataListGroupModule.forEach(function (data) {
                if (data.group_id === groupId) {
                    text += '<span class="label span_privilege_module" style="margin-top: 10px; font-size: 14px">' + data.module_group + ": " + data.name +'</span>';
                    text += space;
				}
            });

            $('#privileges-list-label').html(text);
        }

	</script>

	<style>
		.span_privilege_module {
			padding: 5px;
			background-color: #1E88E5;
			display: inline-block;
		}

@endsection