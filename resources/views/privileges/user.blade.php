@extends('layout.main')

@section('title')
	User
@endsection

@section('css')
	<!-- DataTables -->
  	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('pageTitle')
	User Management
@endsection

@section('pageDesc')
	Add / Update User
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session('success'))
	                    <div class="alert alert-success">
	                        {{ session('success') }}
	                    </div>
	                @endif
	                @if (session('error'))
	                    <div class="alert alert-danger">
	                        {{ session('error') }}
	                    </div>
	                @endif
					<table id="group-table" class="table table-borderd table-striped">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email / Phone</th>
								<th>Group</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($users as $element)
								<tr>
									<td>{{ $element->name }}</td>
									<td>
										{{ $element->email }} <br>
										{{ $element->phone }}
									</td>
									<td>
										<strong>{{ $element->group->name }}</strong>
									</td>
									<td>
										@if ($element->status == 'enable')
											<span class="label label-success">Active</span>
										@else 
											<span class="label label-danger">{{ strtoupper($element->status) }}</span>
										@endif
									</td>
									<td>
										<button class="btn btn-flat btn-warning btn-update-modal" data-name="{{ $element->name }}" data-email="{{ $element->email }}" data-phone="{{ $element->phone }}" data-group="{{ $element->group->id }}" data-status="{{ $element->status }}" data-id="{{ $element->id }}">
											Update
										</button>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-info pull-right" id="btn-modal-create">Add User</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-default">
    	<div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  		<span aria-hidden="true">&times;</span>
                  	</button>
                	<h4 class="modal-title"><span id="modal-header">Create </span>Group</h4>
              	</div>
              	<div class="modal-body">
                	<form id="user-form" method="post">
                		{{ csrf_field() }}
                		<input type="hidden" name="userId">
                		<div class="form-group">
                			<label>Name</label>
                			<input type="text" name="name" class="form-control" required>
                		</div>
                		<div class="form-group">
                			<label>Email</label>
                			<input type="text" name="email" class="form-control" required>
                		</div>
                		<div class="form-group">
                			<label>Phone</label>
                			<input type="text" name="phone" class="form-control" required>
                		</div>
                		<div class="form-group">
                			<label>Password</label>
                			<input type="text" name="password" class="form-control" required>
                		</div>
                		<div class="form-group">
                			<label>Status</label>
                			<select class="form-control" name="status">
                				<option value="enable">Enabled</option>
                				<option value="disable">Disabled</option>                				
                			</select>
                		</div>
                		<div class="form-group">
                			<label>Group</label>
                			<select class="form-control" name="group">
                				@foreach ($groups as $element)
                					<option value="{{ $element->id }}">{{ $element->name }}</option>
                				@endforeach
                			</select>
                		</div>
                	</form>
              	</div>
              	<div class="modal-footer">
                	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                	<button type="button" class="btn btn-primary" id="btn-form">Save changes</button>
              	</div>
            </div>
        </div>
    </div>
@endsection

@section('js')
	<!-- DataTables -->
	<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#group-table').DataTable();

			// Button click show create modal
			$('#btn-modal-create').on('click', function(event) {
				$('#modal-default').modal('show');
				//set empty first 
				$('input[name=userId]').val('');
				$('input[name=name]').val('');
				$('input[name=email]').val('');
				$('input[name=phone]').val('');
				$('input[name=password]').val('');
			});
			$('#group-table').on('click', '.btn-update-modal', function(event) {
				var data = $(this).data();
				$('#modal-default').modal('show');

				//set empty first 
				$('input[name=userId]').val('');
				$('input[name=name]').val('');
				$('input[name=email]').val('');
				$('input[name=phone]').val('');
				$('input[name=password]').val('');

				// set value to form
				$('input[name=userId]').val(data.id);
				$('input[name=name]').val(data.name);
				$('input[name=email]').val(data.email);
				$('input[name=phone]').val(data.phone);
				$('select[name=group]').val(data.group);
				$('select[name=group]').trigger('change');
				$('select[name=status]').val(data.status);
				$('select[name=status]').trigger('change');
			});


			// Modal Click Save then submit form
			$('#btn-form').on('click', function(event) {
				$('#user-form').submit();
			});
		});
	</script>
@endsection