@extends('layout.main')

@section('title')
	Privileges
@endsection

@section('css')
	<!-- DataTables -->
  	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('pageTitle')
	Module Management
@endsection

@section('pageDesc')
	Add / Edit Module
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session('success'))
	                    <div class="alert alert-success">
	                        {{ session('success') }}
	                    </div>
	                @endif
	                @if (session('error'))
	                    <div class="alert alert-danger">
	                        {{ session('error') }}
	                    </div>
	                @endif
					<table id="module-table" class="table table-borderd table-striped">
						<thead>
							<tr>
								<th>Group</th>
								<th>Module Name</th>
								<th>Description</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($routeLists as $element)
								<tr>
									<td>
										<span class="label label-info">{{ $element->group }}</span>
									</td>
									<td>{{ $element->name }}</td>
									<td>{{ $element->description }}</td>
									<td>
										@if (empty($element->id))
											<button class="btn btn-flat btn-success btn-add" data-url="{{ $element->name }}" data-group="{{ $element->group }}">Add</button>
										@else
											<button class="btn btn-flat btn-info btn-edit" data-id="{{ $element->id }}" data-url="{{ $element->name }}" data-description="{{ $element->description }}" data-group="{{ $element->group }}">Edit</button>
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="box">
		<div class="box-header">
            <i class="fa fa-th"></i>
            <h3 class="box-title">Insert Manual</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
		<div class="box-body">
			<form method="post">
				{{ csrf_field() }}
        		<div class="form-group">
        			<label>Group</label>
        			<input type="text" name="moduleGroup" class="form-control" required>
        		</div>
        		<div class="form-group">
        			<label>Name</label>
        			<input type="text" name="moduleName" class="form-control" required>
        		</div>
        		<div class="form-group">
        			<label>Description</label>
        			<input type="text" name="moduleDescription" class="form-control" required>
        		</div>
        		<button class="btn btn-info btn-flat">Submit</button>
			</form>
		</div>
	</div>

	<div class="modal fade" id="modal-default">
    	<div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  		<span aria-hidden="true">&times;</span>
                  	</button>
                	<h4 class="modal-title"><span id="modal-header">Create </span>Group</h4>
              	</div>
              	<div class="modal-body">
                	<form id="module-form" method="post">
                		{{ csrf_field() }}
                		<input type="hidden" name="moduleId">
                		<div class="form-group">
                			<label>Group</label>
                			<input type="text" name="moduleGroup" class="form-control" required>
                		</div>
                		<div class="form-group">
                			<label>Name</label>
                			<input type="text" name="moduleName" class="form-control" required readonly>
                		</div>
                		<div class="form-group">
                			<label>Description</label>
                			<input type="text" name="moduleDescription" class="form-control" required>
                		</div>
                	</form>
              	</div>
              	<div class="modal-footer">
                	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                	<button type="button" class="btn btn-primary" id="btn-form">Save changes</button>
              	</div>
            </div>
        </div>
    </div>
@endsection

@section('js')
	<!-- DataTables -->
	<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#module-table').DataTable({
				"order" : [[1,"asc"]],
                'scrollY'     : 500,
                'deferRender' : true,
                'scroller'    : true
			});
			// Button click show create modal
            $('#module-table').on('click', '.btn-add', function (event) {
				var data = $(this).data();
				var name = data.url;
				var group = data.group;
				// show modal
				$('#modal-default').modal('show');
				$('input[name=moduleName]').val('');
				$('input[name=moduleId]').val('');
				$('input[name=moduleDescription]').val('');
				$('input[name=moduleGroup]').val('');
				
				// set value URL Name
				$('input[name=moduleName]').val(name);
				$('input[name=moduleGroup]').val(group);
			});
			// Button click update 
            $('#module-table').on('click', '.btn-edit', function (event) {
				var data = $(this).data();
				var name = data.url;
				var id = data.id;
				var description = data.description;
				var group = data.group;

				// show modal
				$('#modal-default').modal('show');
				$('input[name=moduleName]').val('');
				$('input[name=moduleId]').val('');
				$('input[name=moduleDescription]').val('');
				$('input[name=moduleGroup]').val('');

				// set value URL Name
				$('input[name=moduleName]').val(name);
				$('input[name=moduleId]').val(id);
				$('input[name=moduleDescription]').val(description);
				$('input[name=moduleGroup]').val(group);
			});

			// Modal Click Save then submit form
			$('#btn-form').on('click', function(event) {
				$('#module-form').submit();
			});
		});
	</script>
@endsection