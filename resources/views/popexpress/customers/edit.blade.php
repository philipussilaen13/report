@extends('layout.main')

@section('title')
    Edit Customer
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('pageTitle')
    Customer
@endsection

@section('pageDesc')
    Edit Customer
@endsection

@section('content')
    <form method="post" method="post" action="{{ url('/popexpress/customers/store') }}">
        <input type="hidden" value="{{ $customer->id }}" name="id">
        {{ csrf_field() }}
        <div class="box box-solid">
            <div class="box-body">
                <h3>Details</h3>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="form-group">
                            <label class="mandatory">Code</label>
                            <input type="text" name="code" class="form-control" value="{{ $customer->code }}" required>
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Email</label>
                            <input type="text" name="email" class="form-control" value="{{ $user->email }}" required>
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Phone</label>
                            <input type="text" name="phone" class="form-control" value="{{ $user->phone }}" required>
                        </div>
                        <div class="form-group">
                            <label>Line ID</label>
                            <input type="text" name="id_line" class="form-control" value="{{ $customer->id_line }}">
                        </div>
                        <div class="form-group">
                            <label>Instagram ID</label>
                            <input type="text" name="id_instagram" class="form-control" value="{{ $customer->id_instagram }}">
                        </div>
                        <div class="form-group">
                            <label>Pickup Location</label>
                            <select class="form-control select2" name="pickup_location">
                                <option value="">Select</option>
                                @foreach ($destinations as $destination)
                                    <option value="{{ $destination->id }}" {!! ($destination->id == $customer->pickup_location ? 'selected="selected"' : '' ) !!}>{{ $destination->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pickup Latitude</label>
                            <input type="text" name="pickup_latitude" class="form-control" value="{{ $customer->pickup_latitude }}">
                        </div>
                        <div class="form-group">
                            <label>Pickup Longitude</label>
                            <input type="text" name="pickup_longitude" class="form-control" value="{{ $customer->pickup_longitude }}">
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Terms of Payment</label>
                            <select class="form-control" name="terms_of_payment" required>
                                <option value="">Select</option>
                                @foreach ($top as $top)
                                    <option value="{{ $top }}" {!! ($customer->terms_of_payment == $top ? 'selected="selected"' : '') !!}>{{ $top }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="tax" value="1" {!! ($customer->tax == 1 ? 'checked="checked"' : '') !!}> Tax
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Join Date</label>
                            <input type="text" name="join_date" id="join_date" class="form-control" value="{{ $join_date }}" required>
                        </div>
                        <div class="form-group">
                            <label>Weight Rounding</label>
                            <input type="text" name="weight_rounding" id="weight_rounding" class="form-control" value="{{ $customer->weight_rounding }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" value="{{ $user->username }}">
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Password</label>
                            @if($accessAdd)
                                <br><a href="#" id="change-password-link" data-toggle="modal" data-target="#modal-default">Change Password</a>
                            @else
                                <input type="password" name="password" class="form-control" value="*****" disabled>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Status</label>
                            <select class="form-control" name="status" required>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status }}" {!! ($user->status == $status ? 'selected="selected"' : '') !!}>{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Customer Type</label>
                            <select class="form-control" name="customer_type" required>
                                <option value="">Select</option>
                                @foreach ($types as $key => $type)
                                    <option value="{{ $key }}" {!! ($customer->customer_type == $key ? 'selected="selected"' : '') !!} >{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Billing Address</label>
                            <textarea class="form-control" name="billing_address" rows="4" required>{{ $customer->billing_address }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Pickup Address</label>
                            <textarea class="form-control" name="pickup_address" rows="4">{{ $customer->pickup_address }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Sales</label>
                            <select class="form-control" name="sales">
                                <option value="">Select</option>
                                @foreach ($sales as $salesPerson)
                                    <option value="{{ $salesPerson['id'] }}"  {!! ($customer->sales == $salesPerson['id'] ? 'selected="selected"' : '') !!}>{{ $salesPerson['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Credit Limit</label>
                            <input type="text" name="credit_limit" value="{{ intval($customer->credit_limit) }}" class="form-control" required onkeypress="return isNumber(event);" >
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="suspend" value="1" {!! ($customer->suspend == 1 ? 'checked="checked"' : '') !!}> Suspend
                        </div>
                        <div class="form-group">
                            <label>Remark</label>
                            <textarea class="form-control" name="remark">{{ $customer->remark }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ url('popexpress/customers') }}"><button class="btn btn-flat btn-warning">Back</button></a>
            </div>
        </div>
    </form>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <form autocomplete="off" method="post" action="{{ url('/popexpress/customers/changePassword') }}">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" id="user_id"  value="{{ $user->id }}" class="form-control">
                        <input type="hidden" name="id" id="id"  value="{{ $customer->id }}" class="form-control">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" name="password" id="password" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Change New Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Save" id="btn-add">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/popexpress.core.js') }} ?>"></script>
    <script src="{{ asset('plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".select2").select2();
            $("#join_date").datepicker();
            $("#weight_rounding").keypress(function(event){
                var inputValue = event.which;
                if((inputValue >= 48 && inputValue <= 57) || inputValue == 46 || inputValue == 13 || inputValue == 8) {
                    return true;
                } else {
                    event.preventDefault();
                }
            });
        });
    </script>
@endsection