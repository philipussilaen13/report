@extends('layout.main')

@section('title')
    Detail Destination
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    <style type="text/css">
        #maps {
            width: 100%;
            height: 384px;
        }
        .view-label {
            margin-top: 7px;
        }
        .center {
            text-align: center;
        }
        .margin-bottom-10 {
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('pageTitle')
    Destination
@endsection

@section('pageDesc')
    Detail Destination
@endsection

@section('content')
    <section class="content">
        <div class="row margin-bottom-10">
            <div class="col-md-12 center">
                <button class="btn btn-flat btn-info btn-small btn-update"
                        data-id = "{{ $destination->id }}"
                        data-origin_id = "{{ $destination->origin_id }}"
                        data-airport_code = "{{ $destination->airport_code }}"
                        data-detail_code = "{{ $destination->detail_code }}"
                        data-province = "{{ $destination->province }}"
                        data-county = "{{ $destination->county }}"
                        data-district = "{{ $destination->district }}"
                        data-is_locker = "{{ $destination->is_locker }}"
                        data-locker_name = "{{ $destination->locker_name }}"
                        data-locker_id = "{{ $destination->locker_id }}"
                        data-latitude = "{{ $destination->latitude }}"
                        data-longitude = "{{ $destination->longitude }}"
                >
                    EDIT
                </button>
                <button class="btn btn-flat btn-danger btn-small"  data-toggle="modal" id="btn-add-branch" data-target="#modal-delete-destination">
                    DELETE
                </button>
                <a href="{{ url('/popexpress/destinations') }}" class="btn btn-flat btn-success btn-small">BACK</a>
            </div>
        </div>
        <div class="row margin-bottom-10">
            <div class="col-lg-3">
                <div class="box-body box-profile bg-aqua">
                    <span class="info-box-icon bg-aqua">
                        <i class="ion ion-android-pin text-white"></i>
                    </span>
                    <h3 style="vertical-align: middle; line-height: 50px;">Destination</h3>
                </div>
                <div class="box box-widget widget-user">
                    <div class="box-footer" style="padding-top: 10px;">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" method="POST">
                                    <input type="hidden" id="origin_id_label" value="{{ $destination->origin_id }}">
                                    <input type="hidden" id="airport_code_label" value="{{ $destination->airport_code }}">
                                    <input type="hidden" id="locker_name_label" value="{{ $destination->locker_name }}">
                                    <input type="hidden" id="locker_id_label" value="{{ $destination->locker_id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Origin:</label>
                                    <div class="col-md-8 view-label" id="origin_label">
                                        {{ $destination->origin_name }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Detail Code:</label>
                                    <div class="col-md-8 view-label" id="detail_code_label">
                                        {{ $destination->detail_code }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Province:</label>
                                    <div class="col-md-8 view-label" id="province_label">
                                        {{ $destination->province }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Kabupaten:</label>
                                    <div class="col-md-8 view-label" id="county_label">
                                        {{ $destination->county }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Kecamatan:</label>
                                    <div class="col-md-8 view-label" id="district_label">
                                        {{ $destination->district }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Locker:</label>
                                    <div class="col-md-8 view-label" id="is_locker_label">
                                        {{ ($destination->is_locker == 1 ? 'Yes' : 'No') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Latitude:</label>
                                    <div class="col-md-8 view-label" id="latitude_label">
                                        {{ $destination->latitude }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Longitude:</label>
                                    <div class="col-md-8 view-label" id="longitude_label">
                                        {{ $destination->longitude }}
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="box box-primary">
                    <div id="map" style="height: 500px;"></div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2>List Pickup Branches</h2>
                        <button class="btn btn-flat btn-success pull-right" data-toggle="modal" id="btn-add-branch" data-target="#modal-add">Add Branch</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="table-branch-pickup">

                                </tbody>
                            </table>
                            <button type="button" class="btn btn-default disabled pull-right" data-dismiss="modal"><strong>Total : {{ count($branchPickups) }}</strong></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>

    <div class="modal fade" id="modal-add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Branch</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="destination_id" id="destination_id" value="{{ $destination->id }}">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <div class="form-group">
                        <label class="mandatory">Branch</label>
                        <select class="form-control select2" name="branch_id" id="branch_id" required>
                            <option value=""></option>
                            @foreach($branches as $branch)
                                <option value="{{ $branch->id }}">{{ $branch->code.' - '.$branch->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-add" onclick="addBranch()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-delete-new">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Branch</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id_delete" class="form-control">
                    <input type="hidden" name="destination_id" id="destination_id_delete" class="form-control">
                    Apakah anda yakin ingin menghapus branch ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="btn-add" onclick="deleteBranch()">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-delete-destination">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Branch</h4>
                </div>
                <div class="modal-body">
                    <form id="form-delete" method="post" action="{{ url('/popexpress/destinations/store') }}">
                        {{ csrf_field() }}
                    <input type="hidden" name="id" id="id_delete_destination" value="{{ $destination->id }}" class="form-control">
                        <input type="hidden" name="delete" id="delete" value="1" class="form-control">
                    </form>
                    Apakah anda yakin ingin menghapus destination ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="btn-delete-destination">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit-destination">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Destination</h4>
                </div>
                <div class="modal-body">
                    <form id="form-update-destination" method="post" action="{{ url('/popexpress/destinations/store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id"  id="id_update" class="form-control">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Origin</label>
                                    <select class="form-control" name="origin_id" id="origin_id" required>
                                        <option value=""></option>
                                        @foreach($origins as $origin)
                                            <option value="{{ $origin->id }}">{{ $origin->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">Airport Code</label>
                                    <input type="text" name="airport_code" id="airport_code" class="form-control" required>
                                </div>
                                <div class="form-group" id="detailcodepart">
                                    <label class="mandatory">Detail Code</label>
                                    <input type="text" name="detail_code" id="detail_code" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">Province</label>
                                    <input type="text" name="province" id="province"  class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="latitude" id="latitude"  class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Locker</label>
                                    <select class="form-control" name="is_locker"  id="is_locker" required>
                                        <option value="0">NO</option>
                                        <option value="1">YES</option>
                                    </select>
                                </div>
                                <div class="form-group" id="lockernamepart">
                                    <label class="mandatory">Locker Name</label>
                                    <input type="text" name="locker_name" id="locker_name" class="form-control" required>
                                    <input type="hidden" class="form-control" id="locker_id" name="locker_id"/>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">County</label>
                                    <input type="text" name="county" id="county" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">District</label>
                                    <input type="text" name="district" id="district"  class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="longitude" id="longitude"  class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-update-destination">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js') }}"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYRNe9Dgft0H5c-cos_y6-rQ2L2nEqMBI&callback=initMap"></script>
    <script type="text/javascript">
        var defaultMarker = {lat: {{$destination->latitude}}, lng: {{$destination->longitude}}};

        var map;
        function initMap(){
            var emptyMarker = false;
            if(defaultMarker.lat == 0 || defaultMarker.lng == 0){
                defaultMarker = {lat: -6.203852, lng: 106.796650};
                emptyMarker = true;
            }

            map = new google.maps.Map(document.getElementById('map'), {
                center: defaultMarker,
                zoom: 10
            });

            if(!emptyMarker){
                var marker = new google.maps.Marker({
                    position: defaultMarker,
                    map: map
                });

                var destinationCircle = new google.maps.Circle({
                    strokeColor: 'white',
                    strokeWeight: 0,
                    fillColor: 'red',
                    fillOpacity: 0.2,
                    map: map,
                    center: defaultMarker
                });
            } else {
                $("#map").html("<iframe\n" +
                    "  width=\"100%\"\n" +
                    "  height=\"500\"\n" +
                    "  frameborder=\"0\" style=\"border:0\"\n" +
                    "  src=\"https://www.google.com/maps/embed/v1/place?key=AIzaSyD85eopJ4INcD6vnPl7JOW_0nvH-h1L2Rs\n" +
                    "    &q={{ $place }}\" allowfullscreen>\n" +
                    "</iframe>");
            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#name").keypress(function(event){
                var inputValue = event.which;
                if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0 && inputValue != 8)) {
                    event.preventDefault();
                }
            });
        });

        $( document ).ready(function() {
            $('#btn-delete-destination').on('click', function(event) {
                $('#delete').val(1);
                $('#form-delete').submit();
            });

            loadBranchPickup();
        });

        var unsaved = false;
        $(function () {
            var tujuanSource = {!! $ArrLockerNames !!};
            $('#locker_name').typeahead({
                source: tujuanSource,
                items: 10,
                triggerLength: 1,
                onSelect: function(item){
                    $('#locker_id').val(item.value);
                    unsaved=true;
                }
            });
        });

        $('.btn-update').on('click', function() {
            var data = $(this).data();
            $('#modal-edit-destination').modal('show');
            $("#modal-edit-destination").on('shown.bs.modal', function(){
                $('#id_update').val(data.id);
                $('#origin_id').val($.trim($('#origin_id_label').val()));
                $('#airport_code').val($.trim($('#airport_code_label').val()));
                $('#detail_code').val($.trim($('#detail_code_label').text()));
                $('#province').val($.trim($('#province_label').text()));
                $('#is_locker').val(($.trim($('#is_locker_label').text()) == 'Yes' ? 1 : 0));
                $('#locker_name').val($.trim($('#locker_name_label').val()));
                $('#locker_id').val($.trim($('#locker_id_label').val()));
                $('#county').val($.trim($('#county_label').text()));
                $('#district').val($.trim($('#district_label').text()));
                $('#latitude').val($.trim($('#latitude_label').text()));
                $('#longitude').val($.trim($('#longitude_label').text()));

                if($.trim($('#is_locker_label').text()) == 'Yes') {
                    $('#detailcodepart').hide();
                    $('#lockernamepart').show();
                    $('#locker_name').val($.trim($('#detail_code_label').text()));
                    $('#detail_code').val('');
                } else {
                    $('#detailcodepart').show();
                    $('#lockernamepart').hide();
                    $('#locker_name').val('');
                }
            });
        });

        $('#btn-update-destination').on('click', function(event) {
            $.LoadingOverlay("show");
            $.ajax({
                url: '{{ url("/popexpress/destinations/update_destination") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: $('#id_update').val(),
                    origin_id: $('#origin_id').val(),
                    airport_code: $('#airport_code').val(),
                    detail_code: $('#detail_code').val(),
                    province: $('#province').val(),
                    is_locker: $('#is_locker').val(),
                    locker_name: $('#locker_name').val(),
                    locker_id: $('#locker_id').val(),
                    county: $('#county').val(),
                    district: $('#district').val(),
                    latitude: $('#latitude').val(),
                    longitude: $('#longitude').val()
                },
                success: function (data) {
                    if(data.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('#origin_id_label').html(data.data.origin_id);
                        $('#origin_label').html(data.data.origin);
                        $('#detail_code_label').html(data.data.detail_code);
                        $('#province_label').html(data.data.province);
                        $('#county_label').html(data.data.county);
                        $('#district_label').html(data.data.district);
                        $('#is_locker_label').html((data.data.is_locker == 1 ? 'Yes' : 'No'));
                        $('#latitude_label').html(data.data.latitude);
                        $('#longitude_label').html(data.data.longitude);
                        $('#origin_id_label').val(data.data.origin_id);
                        $('#airport_code_label').val(data.data.airport_code);
                        $('#locker_name_label').val(data.data.locker_name);
                        $('#locker_id_label').val(data.data.locker_id);
                        $("#modal-edit-destination .close").click();


                        if(data.data.latitude == '' || data.data.latitude == '0' || data.data.longitude == '' ||  data.data.longitude == '0') {
                            initMap();
                        } else {
                            var map = new google.maps.Map(document.getElementById('map'), {
                                center: {lat: data.data.latitude, lng: data.data.longitude},
                                zoom: 10
                            });

                            var marker = new google.maps.Marker({
                                position: {lat: data.data.latitude, lng: data.data.longitude},
                                map: map
                            });

                            var destinationCircle = new google.maps.Circle({
                                strokeColor: 'white',
                                strokeWeight: 0,
                                fillColor: 'red',
                                fillOpacity: 0.2,
                                map: map,
                                center: {lat: data.data.latitude, lng: data.data.longitude}
                            });
                        }

                    } else {
                        $("#modal-edit-destination .close").click();
                        $(".error-title").html('Error');
                        $(".error-body").html(data.message);
                        $("#modal-error").modal("show");
                    }
                    $.LoadingOverlay("hide");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        });

        $('#is_locker').on('change', function(event) {
            if($('#is_locker').val() == 1) {
                $('#detailcodepart').hide();
                $('#lockernamepart').show();
            } else if($('#is_locker').val() == "") {
                $('#detailcodepart').show();
                $('#lockernamepart').show();
                $('#locker_name').val('');
                $('#locker_id').val('');
            } else if($('#is_locker').val() == 0) {
                $('#detailcodepart').show();
                $('#lockernamepart').hide();
                $('#locker_name').val('');
                $('#locker_id').val('');
            }
        });

        $('#modal-add').on('shown.bs.modal', function(event) {
            $('#branch_id').val('');
            $('#branch_id').attr('placeholder', '').select2();
        });

        function addBranch() {
            $.LoadingOverlay("show");
            var branch_id = $('#branch_id').val();
            var destination_id = $('#destination_id').val();
            $.ajax({
                url: '{{ url("/popexpress/destinations/add_branch_pickup") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    branch_id: branch_id,
                    destination_id: destination_id
                },
                success: function (data) {
                    if(data == false) {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Branch sudah terdaftar untuk destination ini.');
                        $("#modal-error").modal("show");
                    } else {
                        $("#modal-add .close").click();
                        loadBranchPickup();
                    }
                    $.LoadingOverlay("hide");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function deleteBranch() {
            $.LoadingOverlay("show");
            var id = $('#id_delete').val();
            var destination_id = $('#destination_id_delete').val();
            $.ajax({
                url: '{{ url("/popexpress/destinations/delete_branch_pickup") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                    destination_id: destination_id
                },
                success: function (data) {
                    if(data == false) {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Branch gagal dihapus.');
                        $("#modal-error").modal("show");
                    } else {
                        $("#modal-delete-new .close").click();
                        loadBranchPickup();
                    }
                    $.LoadingOverlay("hide");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function loadBranchPickup() {
            $.LoadingOverlay("show");
            var destination_id = $('#destination_id').val();
            $('#table-branch-pickup').html('');
            $.ajax({
                url: '{{ url("/popexpress/destinations/load") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    destination_id: destination_id
                },
                success: function (data) {
                    if(data !== false) {
                        console.log(data);
                        var number = 1;
                        for(var no=0; no < data.length; no++) {
                            $('#table-branch-pickup').append(
                                '<tr>\n' +
                                ' <td>' + number + '</td>\n' +
                                ' <td>' + data[no].code + '</td>\n' +
                                ' <td>' + data[no].name + '</td>\n' +
                                ' <td>' + data[no].type.toUpperCase() + '</td>\n' +
                                ' <td>' + data[no].description + '</td>\n' +
                                ' <td>\n' +
                                '     <button class="btn btn-flat btn-danger btn-small btn-delete-new"\n' +
                                //'  data-toggle="modal" id="btn-delete-branch" data-target="#modal-delete" ' +
                                '             data-id = "' + data[no].branches_pickup_id + '"\n' +
                                '             data-branchid = "' + data[no].branch_id + '"\n' +
                                '             data-destinationid = "' + data[no].destination_id + '"\n' +
                                '     >\n' +
                                '         <i class="fa fa-trash"></i>\n' +
                                '     </button>\n' +
                                ' </td>\n' +
                                '</tr>'
                            );
                            number++;
                        }
                        $('.btn-delete-new').on('click', function() {
                            var data = $(this).data();
                            $('#modal-delete-new').modal('show');
                            $("#modal-delete-new").on('shown.bs.modal', function(){
                                $('#id_delete').val(data.id);
                                $('#destination_id_delete').val(data.destinationid);
                            });
                        });
                    }
                    $.LoadingOverlay("hide");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

    </script>
@endsection