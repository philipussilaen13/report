@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
    List all destinations
@endsection

@section('css')
@endsection

@section('pageTitle')
    Destinations
@endsection

@section('pageDesc')
    List all destinations
@endsection

@section('content')
    <div class="box box-solid">
        <div class="box-body">
            <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modal-default">Filter</button>
            <a href="{{ url('popexpress/destinations') }}"><button class="btn btn-flat btn-warning">Reset Filter</button></a>
            <div class="pull-right">
                <button class="btn btn-flat btn-primary" data-toggle="modal" id="export">Export</button>
                <button class="btn btn-flat btn-primary" data-toggle="modal" id="export_locker">Export PopBox Locker</button>
                <a href="{{ url('popexpress/destinations/import') }}"><button class="btn btn-flat btn-primary">Import</button></a>
                <button class="btn btn-flat btn-success" data-toggle="modal" id="btn-add-destination" data-target="#modal-add">Add Destination</button>
            </div>
            <hr>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Table --}}
            <div class="table-responsive">
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Origin</th>
                        <th>Airport Code</th>
                        <th>Detail Code</th>
                        <th>Province</th>
                        <th>County</th>
                        <th>District</th>
                        <th>Locker</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($destinations as $index => $destination)
                        <tr>
                            <td>{{ $destinations->firstItem() + $index }}</td>
                            <td>{{ $destination->origin_name }}</td>
                            <td>{{ $destination->airport_code }}</td>
                            <td><a href="{{ url('/popexpress/destinations/detail/'.$destination->id) }}"> {{ $destination->detail_code }} </a></td>
                            <td>{{ $destination->province }}</td>
                            <td>{{ $destination->county }}</td>
                            <td>{{ $destination->district }}</td>
                            <td>{{ ($destination->is_locker == 1 ? 'Yes' : 'No') }}</td>
                            <td>
                                <a href="{{ url('/popexpress/destinations/detail/'.$destination->id) }}">
                                    <button class="btn btn-flat btn-info btn-small">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $destinations->appends($_GET)->links() }}
                <button type="button" class="btn btn-default disabled pull-right" data-dismiss="modal"><strong>Total : {{ $destinations->total() }}</strong></button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <div class="modal-body">
                    <form id="form-filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Origin</label>
                                    <select class="form-control" name="origin_id" id="origin_id_filter" required>
                                        <option value="">All</option>
                                        @foreach($origins as $origin)
                                            <option value="{{ $origin->id }}">{{ $origin->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Airport Code</label>
                                    <input type="text" name="airport_code" id="airport_code_filter" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Detail Code</label>
                                    <input type="text" name="detail_code" id="detail_code_filter" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Province</label>
                                    <input type="text" name="province" id="province_filter" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Locker</label>
                                    <select class="form-control" name="is_locker"  id="is_locker_filter" required>
                                        <option value="">Any</option>
                                        <option value="0">NO</option>
                                        <option value="1">YES</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Locker Name</label>
                                    <input type="text" name="locker_name" id="locker_name_filter" class="form-control" required>
                                    <input type="hidden" name="locker_id" class="form-control" id="locker_id_filter"/>
                                </div>
                                <div class="form-group">
                                    <label>County</label>
                                    <input type="text" name="county" id="county_filter" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>District</label>
                                    <input type="text" name="district" id="district_filter" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-filter">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add/Update Destination</h4>
                </div>
                <div class="modal-body">
                    <form id="form-add" method="post" action="{{ url('/popexpress/destinations/store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" class="form-control">
                        <input type="hidden" name="delete" id="delete" class="form-control">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Origin</label>
                                    <select class="form-control" name="origin_id" id="origin_id" required>
                                        <option value=""></option>
                                        @foreach($origins as $origin)
                                            <option value="{{ $origin->id }}">{{ $origin->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">Airport Code</label>
                                    <input type="text" name="airport_code" id="airport_code" class="form-control" required>
                                </div>
                                <div class="form-group" id="detailcodepart">
                                    <label class="mandatory">Detail Code</label>
                                    <input type="text" name="detail_code" id="detail_code" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">Province</label>
                                    <input type="text" name="province" id="province" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Locker</label>
                                    <select class="form-control" name="is_locker"  id="is_locker" required>
                                        <option value="0">NO</option>
                                        <option value="1">YES</option>
                                    </select>
                                </div>
                                <div class="form-group" id="lockernamepart">
                                    <label class="mandatory">Locker Name</label>
                                    <input type="text" name="locker_name" id="locker_name" class="form-control" required>
                                    <input type="hidden" class="form-control" id="locker_id" name="locker_id"/>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">County</label>
                                    <input type="text" name="county" id="county" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">District</label>
                                    <input type="text" name="district" id="district" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="btn-delete" style="display: none;">Delete</button>
                    <button type="button" class="btn btn-primary" id="btn-add">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#name,#name_filter").keypress(function(event){
                var inputValue = event.which;
                if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
                    event.preventDefault();
                }
            });
            $("#modal-add").on('shown.bs.modal', function(){
                $("#is_locker").val('0');
                $('#lockernamepart').hide();
            });
            $("#modal-default").on('shown.bs.modal', function(){
                $("#is_locker_filter").val('');
            });
        });

        var unsaved = false;
        $(function () {
            var tujuanSource = {!! $ArrLockerNames !!};
            $('#locker_name').typeahead({
                source: tujuanSource,
                items: 10,
                triggerLength: 1,
                onSelect: function(item){
                    $('#locker_id').val(item.value);
                    unsaved=true;
                }
            });
        });

        $(function () {
            var tujuanSourceFilter = {!! $ArrLockerNames !!};
            $('#locker_name_filter').typeahead({
                source: tujuanSourceFilter,
                items: 10,
                triggerLength: 1,
                onSelect: function(item){
                    $('#locker_id_filter').val(item.value);
                    unsaved=true;
                }
            });
        });

        $('#btn-filter').on('click', function(event) {
            $('#form-filter').submit();
        });

        $('#btn-add').on('click', function(event) {
            $('#btn-delete').hide();
            $('#form-add').submit();
        });

        $('#is_locker').on('change', function(event) {
            if($('#is_locker').val() == 1) {
                $('#detailcodepart').hide();
                $('#lockernamepart').show();
            } else if($('#is_locker').val() == "") {
                $('#detailcodepart').show();
                $('#lockernamepart').show();
                $('#locker_name').val('');
                $('#locker_id').val('');
            } else if($('#is_locker').val() == 0) {
                $('#detailcodepart').show();
                $('#lockernamepart').hide();
                $('#locker_name').val('');
                $('#locker_id').val('');
            }
        });

        $('#btn-add-destination').on('click', function() {
            $("#modal-add").on('shown.bs.modal', function(){
                $('#btn-delete').hide();
                $('#id').val('');
                $('#origin_id').val('');
                $('#airport_code').val('');
                $('#detail_code').val('');
                $('#province').val('');
                $('#is_locker').val('0');
                $('#locker_name').val('');
                $('#locker_id').val('');
                $('#county').val('');
                $('#district').val('');
                $('#delete').val(0);
                $('#detailcodepart').show();
                $('#lockernamepart').hide();
            });
        });

        $('.btn-update').on('click', function() {
            var data = $(this).data();
            $('#btn-delete').show();
            $('#modal-add').modal('show');
            $("#modal-add").on('shown.bs.modal', function(){
                $('#id').val(data.id);
                $('#origin_id').val(data.origin_id);
                $('#airport_code').val(data.airport_code);
                $('#detail_code').val(data.detail_code);
                $('#province').val(data.province);
                $('#is_locker').val(data.is_locker);
                $('#locker_name').val(data.locker_name);
                $('#locker_id').val(data.locker_id);
                $('#county').val(data.county);
                $('#district').val(data.district);
                $('#delete').val(0);

                if(data.is_locker == 1) {
                    $('#detailcodepart').hide();
                    $('#lockernamepart').show();
                    $('#locker_name').val(data.detail_code);
                    $('#detail_code').val('');
                } else {
                    $('#detailcodepart').show();
                    $('#lockernamepart').hide();
                    $('#locker_name').val('');
                }
            });
        });

        $('#btn-delete').on('click', function(event) {
            $('#delete').val(1);
            $('#form-add').submit();
        });

        $('#export_locker').on('click', function(event) {
            $.LoadingOverlay("show");

            $.ajax({
                url: '{{ url("/popexpress/destinations/export_locker") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        $.LoadingOverlay("hide");
                        window.location = '{{ url("/popexpress/destinations/download") }}' + '?file=' + feedback['link'];
                    } else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Informasi');
                        $(".error-body").html(feedback['message']);
                        $("#modal-error").modal("show");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        });

        $('#export').on('click', function(event) {
            var queries = {};
            var currentUrl = document.location.search;
            if(currentUrl.indexOf('?') != -1) {
                $.each(document.location.search.substr(1).split('&'),function(c,q){
                    var i = q.split('=');
                    queries[i[0].toString()] = i[1].toString();
                });
            }
            queries['_token'] = "{{ csrf_token() }}";
            $.LoadingOverlay("show");

            $.ajax({
                url: '{{ url("/popexpress/destinations/export") }}',
                type: 'POST',
                data: queries,
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        $.LoadingOverlay("hide");
                        window.location = '{{ url("/popexpress/destinations/download") }}' + '?file=' + feedback['link'];
                    } else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Informasi');
                        $(".error-body").html(feedback['message']);
                        $("#modal-error").modal("show");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        });

    </script>
@endsection