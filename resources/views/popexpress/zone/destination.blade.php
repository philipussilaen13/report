@extends('layout.main')

@section('title')
    Destination
@endsection

@section('css')
    <style type="text/css">
        #maps {
            width: 100%;
            height: 384px;
        }
    </style>
@endsection

@section('pageTitle')
    ZONA
@endsection

@section('pageDesc')
    Destination
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-3">
                <div class="box-body box-profile bg-aqua">
                    <span class="info-box-icon bg-aqua">
                        <i class="ion ion-android-pin text-white"></i>
                    </span>
                    <h3 style="vertical-align: middle; line-height: 50px;">Destination</h3>
                </div>
                <div class="box box-widget widget-user">
                    <div class="box-footer" style="padding-top: 10px;">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" method="POST" action="{{url('/popexpress/zone/destination/'.$destination->id.'/update')}}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Province:</label>
                                        <div class="col-sm-8 view-label">
                                            {{ $destination->province }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Kabupaten:</label>
                                        <div class="col-sm-8 view-label">
                                            {{ $destination->county }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Kecamatan:</label>
                                        <div class="col-sm-8 view-label">
                                            {{ $destination->district }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Latitude:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="latitude" name="latitude" placeholder="Masukan latitude" value="{{ $destination->latitude }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Longitude:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="longitude" name="longitude" placeholder="Masukan longitude" value="{{ $destination->longitude }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Coverage:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="coverage" name="coverage" placeholder="Masukan coverage" value="{{ $destination->coverage_size }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4"></label>
                                        <div class="col-sm-8">
                                            <button type="submit" class="btn btn-default">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-9">
                <div class="box box-primary">
                    <div id="map" style="height: 500px;"></div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('js')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYRNe9Dgft0H5c-cos_y6-rQ2L2nEqMBI&callback=initMap"></script>
    <script type="text/javascript">
        var defaultMarker = {lat: {{$destination->latitude}}, lng: {{$destination->longitude}}, coverage: {{$destination->coverage_size}}};

        var map;
        function initMap(){
            var emptyMarker = false;
            if(defaultMarker.lat == 0 && defaultMarker.lng == 0){
                defaultMarker = {lat: -6.203852, lng: 106.796650};
                emptyMarker = true;
            }

            map = new google.maps.Map(document.getElementById('map'), {
                center: defaultMarker,
                zoom: 10
            });

            if(!emptyMarker){
                var marker = new google.maps.Marker({
                    position: defaultMarker,
                    map: map
                });

                var destinationCircle = new google.maps.Circle({
                    strokeColor: 'white',
                    strokeWeight: 0,
                    fillColor: 'red',
                    fillOpacity: 0.2,
                    map: map,
                    center: defaultMarker,
                    radius: Math.sqrt(defaultMarker.coverage) * 1000
                });
            }
        }
    </script>
@endsection