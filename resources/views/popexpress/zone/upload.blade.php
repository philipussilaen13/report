@extends('layout.main')

@section('title')
    Upload Zona
@endsection

@section('css')
    <style>
        .form-group.required .control-label:after {
            content:" *";
            color: red;
        }
    </style>
@endsection

@section('pageTitle')
    ZONA
@endsection

@section('pageDesc')
    Upload Zona
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Upload Zona</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="{{ url('/popexpress/zone/upload') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="file_upload" class="col-sm-4 control-label">File Upload:</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control-file" name="file_upload" id="file_upload" data-allowed-file-extensions='["xls", "xlsx"]'>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="button" class="btn btn-success" onclick="kembali()">Kembali</button>
                                    <button type="button" class="btn btn-primary" onclick="template()">Template</button>
                                    <button type="submit" class="btn btn-info" id="btn_upload">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(count($RecordError)>0)
                    <div class="container">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-body table-responsive" style=" padding: 5px;height: 200px;">
                                    <table id="table-error-list" class="table-bordered table-striped"
                                           style="width: 100%;font-size: 12px;">
                                        <thead>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Kode Detil</th>
                                        <th>Provinsi</th>
                                        <th>Kabupaten</th>
                                        <th>Kecamatan</th>
                                        <th>Zona</th>
                                        <th>Regular</th>
                                        <th>One Day</th>
                                        <th>Keterangan</th>
                                        </thead>
                                        <tbody>
                                        @php
                                            $nourut = 1;
                                            foreach ($RecordError as $record)
                                            {
                                                print "<tr>";
                                                print "<td>".$nourut++."</td>";
                                                print "<td>".$record->kode."</td>";
                                                print "<td>".$record->kode_detil."</td>";
                                                print "<td>".$record->provinsi."</td>";
                                                print "<td>".$record->kabupaten."</td>";
                                                print "<td>".$record->kecamatan."</td>";
                                                print "<td>".$record->zona."</td>";
                                                print "<td>".$record->regular."</td>";
                                                print "<td>".$record->oneday."</td>";
                                                print "<td>Gagal diupload</td>";
                                                print "</tr>";
                                            }
                                        @endphp
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('form').submit(function () {
                alert('Proses Upload sedang dilakukan...');
            })
        });

        function template() {
            $.ajax({
                url: '{{ url("/popexpress/zone/excel") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    filter_airport_code: $('#filter_airport_code').val(),
                    filter_detail_code: $('#filter_detail_code').val(),
                    filter_province: $('#filter_province').val(),
                    filter_county: $('#filter_county').val(),
                    filter_district: $('#filter_district').val()
                },
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        window.location = '{{ url("/popexpress/zone/download") }}' + '?file=' + feedback['link'];
                    } else {
                        alert(feedback['message']);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        alert('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                    }
                    else {
                        alert('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                    }
                }
            });
        }

        function kembali() {
            window.location = '{{ url('/popexpress/zone') }}';
        }
    </script>
@endsection