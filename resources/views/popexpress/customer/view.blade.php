@extends('layout.main')

@section('title')
    Detail Pelanggan
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <style>
        table { table-layout: fixed; }
        td {
            overflow: hidden;
            word-wrap:break-word;
        }
        .form-group.required .control-label:after {
            content:" *";
            color: red;
        }
        .pad-20 {
            padding: 20px;
        }
    </style>
@endsection

@section('pageTitle')
    PELANGGAN
@endsection

@section('pageDesc')
    Detail Pelanggan
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Detail Pelanggan</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <form class="form-horizontal" method="POST" action="{{ url('/popexpress/customer').'/'.$customerData->id.'/update' }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Kode Pelanggan:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="customer_code" name="customer_code" placeholder="Masukan kode pelanggan" value="{{ (old('customer_code') == '' ? $customerData->customer_code: old('customer_code')) }}">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Nama Pelanggan:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Masukan nama pelanggan" value="{{ (old('name') == '' ? $customerData->user->name: old('name')) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Cabang:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="branch" name="branch">
                                                @foreach($branches as $branch)
                                                    <option value="{{ $branch['id'] }}" {{ ($selectedBranch == $branch['id'] ? "selected":"") }}>{{ $branch['code']." - ".$branch['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Email:</label>
                                        <div class="col-md-8">
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Masukan email" value="{{ (old('email') == '' ? $customerData->user->email: old('email')) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Password:</label>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Ganti password disini">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Telepon:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Masukan nomor telepon" value="{{ (old('telephone') == '' ? $customerData->telephone: old('telephone')) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Alamat:</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" rows="5" placeholder="Masukan alamat" id="address" name="address">{{ (old('address') == '' ? $customerData->address: old('address')) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Line ID:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="line" name="line" placeholder="Masukan Line ID" value="{{ (old('line') == '' ? $customerData->line_id: old('line')) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Akun Instagram:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Masukan akun Instagram" value="{{ (old('instagram') == '' ? $customerData->ig_account: old('instagram')) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Keterangan:</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" rows="5" placeholder="Masukan keterangan" id="remark" name="remark">{{ (old('remark') == '' ? $customerData->remark: old('remark')) }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Tanggal Bergabung:</label>
                                        <div class="col-md-8">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" name="join_date" value="{{ $selectedJoinDate }}" placeholder="Masukan tanggal bergabung">
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">SMS Notifikasi:</label>
                                        <div class="col-md-8" style="padding: 6px 15px 0;">
                                            <input type="checkbox" value="1" name="sms" {{ ($selectedSMS == '1' ? "checked":"") }}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tipe:</label>
                                        <div class="col-md-8" style="padding: 6px 15px 0;">
                                            <input type="radio" value="1" name="corporate" {{ ($selectedType == '1' ? "checked":"") }}> Corporate
                                            <input type="radio" value="0" name="corporate" {{ ($selectedType == '0' ? "checked":"") }}> Social Seller
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Pajak:</label>
                                        <div class="col-md-8" style="padding: 6px 15px 0;">
                                            <input type="checkbox" value="1" name="tax" {{ ($selectedTax == '1' ? "checked":"") }}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Sales:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="sales" name="sales" data-live-search="true">
                                                <option value="">Tidak tersedia</option>
                                                @foreach($employees as $employee)
                                                    <option value="{{ $employee->id }}" {{ ($selectedEmployee == $employee->id ? "selected":"") }}>{{ $employee->name." (".$employee->employee_code.")" }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Referensi:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="customer_reference" name="customer_reference" data-live-search="true">
                                                <option value="">Tidak tersedia</option>
                                                @php $selectedReference = ""; if(old('customer_reference')){$selectedReference = old('customer_reference');}else{$selectedReference = $customerData->customer_reference;} @endphp
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}" {{ ($selectedReference == $customer->id ? "selected":"") }}>{{ $customer->name." (".$customer->customer_code.")" }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Terms of Payment:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="top" name="top">
                                                <option value="">Pilih</option>
                                                @foreach($terms as $term)
                                                    <option value="{{ $term }}" {{ ($selectedTerm == $term ? "selected" : "") }}>{{ $term }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Status Priority:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="priority" name="priority">
                                                <option value="">Pilih</option>
                                                @foreach($priorities as $priority)
                                                    <option value="{{ strtolower($priority) }}" {{ ($selectedPriority == strtolower($priority) ? "selected" : "") }}>{{ $priority }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(count($errors))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row" style="text-align: center; margin-bottom: 20px;">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                <button type="button" class="btn btn-info" onclick="window.location = '{{ url('/popexpress/customer') }}';">Kembali</button>
                            </div>
                        </form>
                    </div>
                    <div class="row pad-20">
                        <div class="col-md-12">
                            <div>
                                <h3 align="center">DAFTAR AKUN PELANGGAN</h3>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" name="akun" class="form-control" placeholder="Akun" id="akun">
                                </div>
                            </div>
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success" id="search">Cari</button>
                                <button type="button" class="btn btn-info" onclick="window.location = '{{ url("/popexpress/customer/".$customerData->id."/account/create") }}';">Tambah</button>
                            </div>
                            <table class="table table-hover cell-border" id="data-table" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>
                                        <button type="button" id="btn-trash" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                    </th>
                                    <th>No</th>
                                    <th>Akun</th>
                                    <th>Diskon</th>
                                    <th>Diskon 3PL</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Akhir</th>
                                    <th>Akses Semua Cabang</th>
                                </tr>
                                </thead>
                            </table>

                            <div class="modal fade" id="modal-default">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header alert-info">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah anda yakin akan menghapus data yang dipilih ?</p>
                                        </div>
                                        <div class="modal-footer" style="text-align: center;">

                                            <button type="button" class="btn btn-info" id="confirm-delete">Ya</button>
                                            <button type="button" class="btn btn-info" data-dismiss="modal">Tidak</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection

@section('js')
    <script src="{{ asset('js/popexpress.core.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({format: 'dd/mm/yyyy', allowInputToggle: true});
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            var deletedId = [];

            loadTable();

            $('#search').on('click', function(ev, picker) {
                reloadTable();
            });

            $('#code, #akun, #nama, #email, #branch').on('keyup', function (e){
                if(e.keyCode === 13){
                    reloadTable();
                }
            });

            $("#btn-trash").on("click", function (event) {
                $('input.selected-data:checkbox:checked').each(function () {
                    deletedId.push($(this).val());
                });
                if(deletedId.length == 0){
                    alert("Silahkan pilih data yang ingin di hapus terlebih dahulu.");
                } else {
                    $("#modal-default").modal("show");
                }
            });

            $("#confirm-delete").on("click", function (event) {
                console.log(deletedId);
                $.ajax( {
                    url: '{{ url("/popexpress/customer")."/".$customerData->id."/account/delete" }}',
                    data: {
                        id: deletedId,
                        _token : '{{ csrf_token() }}'
                    },
                    type: 'POST',
                    success: function(response){
                        $("#modal-default").modal("hide");
                        alert(response.message);
                        reloadTable();
                        deletedId = [];
                    },
                    failure: function(response){
                        alert("Terdeteksi masalah koneksi ke server. Mohon dicoba lagi.");
                    }
                } );
            });

            function reloadTable() {
                $('#data-table').DataTable().destroy();
                loadTable();
            }

            function loadTable() {
                var DataTable = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    lengthChange: false,
                    paging: true,
                    pageLength: 30,
                    bAutoWidth: false,
                    scrollX: true,
                    scrollY: '50vh',
                    scrollCollapse: true,
                    order: [[ 2, "asc" ]],
                    ajax: {
                        url: "{{ url("/popexpress/customer")."/".$customerData->id."/account/grid" }}",
                        data: {
                            "akun": $("#akun").val(),
                            "length": 30,
                            "_token": "{{ csrf_token() }}"
                        }
                    },
                    columns: [
                        {
                            data: "id",
                            name: "id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return '<input type="checkbox" name="selected_data" value="'+data+'" class="form-input selected-data">';
                            }
                        },
                        {
                            data: "id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            data: "account_name",
                            name: "account_name",
                            width: "15%",
                            render: function (data, type, row, meta) {
                                return '<a href="{{ url('/popexpress/customer/'.$customerData->id) }}/account/'+row.id+'">'+data+'</a>';
                            }
                        },
                        {
                            data: "discount_internal",
                            name: "discount_internal",
                            width: "8%",
                            render: function (data, type, row, meta) {
                                return number_format(data, 2, ",", ".")+" %";

                            }
                        },
                        {
                            data: "discount_external",
                            name: "discount_external",
                            width: "8%",
                            render: function (data, type, row, meta) {
                                return number_format(data, 2, ",", ".")+" %";
                            }
                        },
                        { data: "start_date", name: "start_date", width: "8%" },
                        { data: "end_date", name: "end_date", width: "8%" },
                        {
                            data: "all_branch",
                            name: "all_branch",
                            width: "10%",
                            render: function (data, type, row, meta) {
                                if(data == "1"){
                                    return "Ya";
                                }else{
                                    return "Tidak";
                                }
                            }
                        },
                    ]
                });
            }
        });

    </script>
@endsection