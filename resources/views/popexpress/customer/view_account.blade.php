@extends('layout.main')

@section('title')
    {{ $account->account_name }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <style>
        .form-group.required .control-label:after {
            content:" *";
            color: red;
        }
    </style>
@endsection

@section('pageTitle')
    PELANGGAN
@endsection

@section('pageDesc')
    {{ $account->account_name }}
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">{{ $account->account_name }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <form class="form-horizontal" method="POST" action="{{ url('/popexpress/customer').'/'.$customer->id.'/account/'.$account->id }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Akun:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="account" name="account" placeholder="Masukan nama akun" value="{{ old('account') == '' ? $account->account_name: old('account') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Akses semua cabang:</label>
                                        <div class="col-md-8" style="padding: 6px 15px 0;">
                                            <input type="checkbox" value="1" name="all_branch" {{ ($selectedAllBranch == '1' ? "checked":"") }}>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Diskon:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" style="padding-right: 40px;" id="discount" name="discount" placeholder="Masukan diskon" value="{{ number_format($account->discount_internal, 2, ',', '') }}" onkeypress="return isDecimal(event, this, ',');">
                                            <span style="top: -27px; margin-bottom: -27px; position: relative; right: 10px; float:right;">%</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Diskon 3PL:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" style="padding-right: 40px;" id="discount_3pl" name="discount_3pl" placeholder="Masukan diskon 3PL" value="{{ number_format($account->discount_external, 2, ',', '') }}" onkeypress="return isDecimal(event, this, ',');">
                                            <span style="top: -27px; margin-bottom: -27px; position: relative; right: 10px; float:right;">%</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tanggal Mulai:</label>
                                        <div class="col-md-8">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" name="start_date" value="{{ $selectedStartDate }}" placeholder="Masukan tanggal mulai">
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tanggal Akhir:</label>
                                        <div class="col-md-8">
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' class="form-control" name="end_date" value="{{ $selectedEndDate }}" placeholder="Masukan tanggal akhir">
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="menu-title">AKUN - CABANG</div>
                            <div class="row">
                                @foreach($branches as $branch)
                                    @php
                                        $selectedAccountBranches = "";
                                        if(in_array($branch->id, $accountBranches)){$selectedAccountBranches = "checked";}
                                    @endphp
                                    <div class="col-xs-3">
                                        <input type="checkbox" id="account_branch" name="account_branch[]" value="{{ $branch->id }}" {{ $selectedAccountBranches }}> {{ $branch->code.' - '.$branch->name }}
                                    </div>
                                @endforeach
                            </div>
                            @if(count($errors))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row" style="text-align: center; padding-top: 20px; margin-bottom: 20px;">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                <button type="button" class="btn btn-info" onclick="window.location = '{{ url('/popexpress/customer/'.$customer->id) }}';">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1, #datetimepicker2').datepicker({format: 'dd/mm/yyyy', allowInputToggle: true});
        });
    </script>
@endsection