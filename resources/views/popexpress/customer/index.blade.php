@extends('layout.main')

@section('title')
    Daftar Pelanggan
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.css') }}">
    <style>
        table { table-layout: fixed; }
        td {
            overflow: hidden;
            word-wrap:break-word;
        }
    </style>
@endsection

@section('pageTitle')
    PELANGGAN
@endsection

@section('pageDesc')
    Daftar Pelanggan
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-info">
                    <div class="box-header with-border">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">PELANGGAN</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="row">
                            <form id="form-transaction">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" name="code" class="form-control" placeholder="Kode" id="code">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" name="akun" class="form-control" placeholder="Akun" id="akun">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="nama" class="form-control" placeholder="Nama" id="nama">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control" placeholder="Email" id="email">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control" name="branch" id="branch" data-width="150px" data-live-search="true">
                                            <option value="">Semua cabang</option>
                                            @foreach($branches as $branch)
                                                <option value="{{ $branch['id'] }}" {{ (old('branch') == $branch['id'] ? "selected":"") }}>{{ $branch['code']." - ".$branch['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-info" id="search">Cari</button>
                                    <a href="{{ url('/popexpress/customer/create') }}" class="btn btn-primary">Tambah</a>
                                    <button type="button" class="btn btn-success" id="excel" onclick="unduh_excel()">Unduh Excel</button>
                                </div>
                            </form>
                        </div>
                        <table class="table table-hover cell-border" id="data-table" cellspacing="0">
                            <thead>
                            <tr>
                                <th>
                                    <button type="button" id="btn-trash" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                </th>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Akun</th>
                                <th>Nama</th>
                                <th>Cabang</th>
                                <th>Email</th>
                                <th>Status Priority</th>
                                <th>Tanggal Gabung</th>
                                <th>Transaksi Terakhir</th>
                                <th>Transaksi Terakhir ( Hari )</th>
                            </tr>
                            </thead>
                        </table>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header alert-info">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Konfirmasi</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Apakah anda yakin akan menghapus data yang dipilih ?</p>
                                    </div>
                                    <div class="modal-footer" style="text-align: center;">

                                        <button type="button" class="btn btn-info" id="confirm-delete">Ya</button>
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            var deletedId = [];

            loadTable();

            $('#search').on('click', function(ev, picker) {
                reloadTable();
            });

            $('#code, #akun, #nama, #email, #branch').on('keyup', function (e){
                if(e.keyCode === 13){
                    reloadTable();
                }
            });

            $("#btn-trash").on("click", function (event) {
                $('input.selected-data:checkbox:checked').each(function () {
                    deletedId.push($(this).val());
                });
                if(deletedId.length == 0){
                    alert("Silahkan pilih data yang ingin di hapus terlebih dahulu.");
                } else {
                    $("#modal-default").modal("show");
                }
            });

            $("#confirm-delete").on("click", function (event) {
                console.log(deletedId);
                $.ajax( {
                    url: '{{ url("/popexpress/customer/deactive") }}',
                    data: {
                        id: deletedId,
                        _token : '{{ csrf_token() }}'
                    },
                    type: 'POST',
                    success: function(response){
                        $("#modal-default").modal("hide");
                        alert(response.message);
                        reloadTable();
                        deletedId = [];
                    },
                    failure: function(response){
                        alert("Terdeteksi masalah koneksi ke server. Mohon dicoba lagi.");
                    }
                } );
            });

            function reloadTable() {
                $('#data-table').DataTable().destroy();
                loadTable();
            }

            function loadTable() {
                var DataTable = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    lengthChange: false,
                    paging: true,
                    pageLength: 30,
                    bAutoWidth: false,
                    scrollX: true,
                    scrollY: '50vh',
                    scrollCollapse: true,
                    order: [[ 2, "asc" ]],
                    ajax: {
                        url: "{{ url("/popexpress/customer/grid") }}",
                        data: {
                            "code": $("#code").val(),
                            "akun": $("#akun").val(),
                            "nama": $("#nama").val(),
                            "email": $("#email").val(),
                            "branch": $("#branch").val(),
                            "length": 30,
                            "_token": "{{ csrf_token() }}"
                        }
                    },
                    columns: [
                        {
                            data: "user_id",
                            name: "user_id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return '<input type="checkbox" name="selected_data" value="'+data+'" class="form-input selected-data">';
                            }
                        },
                        {
                            data: "id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            data: "customer_code",
                            name: "customer_code",
                            width: "8%",
                            render: function (data, type, row) {
                                return '<a href="{{ url('/popexpress/customer/') }}/'+row.id+'">'+data+'</a>';
                            }
                        },
                        {
                            data: "account_name",
                            name: "account_name",
                            width: "8%",
                            render: function (data, type, row) {
                                if(data){
                                    return '<a href="{{ url('/popexpress/customer/') }}/'+row.id+'/account/'+row.account_id+'">'+data+'</a>';
                                } else {
                                    return '';
                                }
                            }
                        },
                        { data: "user_name", name: "user_name", width: "15%" },
                        {
                            data: "branch_code",
                            name: "branch_code",
                            width: "8%",
                            render: function (data, type, row) {
                                return data+' - '+row.branch_name;
                            }
                        },
                        { data: "email", name: "email", width: "15%" },
                        { data: "priority", name: "priority", width: "8%" },
                        { data: "join_date", name: "join_date", width: "8%" },
                        { data: "last_pickup", name: "last_pickup", width: "8%" },
                        { data: "not_active_day", name: "not_active_day", width: "15%" },
                    ]
                });
            }
        });

        function unduh_excel() {

            $.ajax({
                url: '{{ url("/popexpress/customer/excel") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "code": $("#code").val(),
                    "akun": $("#akun").val(),
                    "nama": $("#nama").val(),
                    "email": $("#email").val(),
                    "branch": $("#branch").val()
                },
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        window.location = '{{ url("/popexpress/customer/download") }}' + '?file=' + feedback['link'];
                    } else {
                        alert(feedback['message']);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        alert('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                    }
                    else {
                        alert('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                    }
                }
            });

        }

    </script>
@endsection