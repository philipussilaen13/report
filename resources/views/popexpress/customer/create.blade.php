@extends('layout.main')

@section('title')
    Tambah Pelanggan
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <style>
        .form-group.required .control-label:after {
            content:" *";
            color: red;
        }
    </style>
@endsection

@section('pageTitle')
    PELANGGAN
@endsection

@section('pageDesc')
    Tambah Pelanggan
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Tambah Pelanggan</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <form class="form-horizontal" method="POST" action="{{ url('/popexpress/customer/create') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Kode Pelanggan:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="customer_code" name="customer_code" placeholder="Masukan kode pelanggan" value="{{ old('customer_code') }}">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Nama Pelanggan:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Masukan nama pelanggan" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Cabang:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="branch" name="branch">
                                                @foreach($branches as $branch)
                                                    <option value="{{ $branch['id'] }}" {{ (old('branch') == $branch['id'] ? "selected":"") }}>{{ $branch['code']." - ".$branch['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Email:</label>
                                        <div class="col-md-8">
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Masukan email" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Password:</label>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Masukan password">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Telepon:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Masukan nomor telepon" value="{{ old('telephone') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Alamat:</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" rows="5" placeholder="Masukan alamat" id="address" name="address">{{ old('address') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Line ID:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="line" name="line" placeholder="Masukan Line ID" value="{{ old('line') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Akun Instagram:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Masukan akun Instagram" value="{{ old('instagram') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Keterangan:</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" rows="5" placeholder="Masukan keterangan" id="remark" name="remark">{{ old('remark') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="control-label col-md-4">Tanggal Bergabung:</label>
                                        <div class="col-md-8">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" name="join_date" value="{{ old('join_date') }}" placeholder="Masukan tanggal bergabung">
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">SMS Notifikasi:</label>
                                        <div class="col-md-8" style="padding: 6px 15px 0;">
                                            <input type="checkbox" value="1" name="sms" {{ (old('sms') == '1' ? "checked":"") }}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tipe:</label>
                                        <div class="col-md-8" style="padding: 6px 15px 0;">
                                            <input type="radio" value="1" name="corporate" {{ (old('corporate') == "1" ? "checked" : "") }}> Corporate
                                            <input type="radio" value="0" name="corporate" {{ (old('corporate') == "0" ? "checked":"") }}> Social Seller
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Pajak:</label>
                                        <div class="col-md-8" style="padding: 6px 15px 0;">
                                            <input type="checkbox" value="1" name="tax" {{ (old('tax') == '1' ? "checked":"") }}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Sales:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="sales" name="sales" data-live-search="true">
                                                <option value="">Tidak tersedia</option>
                                                @foreach($employees as $employee)
                                                    <option value="{{ $employee->id }}" {{ (old('sales') == $employee->id ? "selected":"") }}>{{ $employee->name." (".$employee->employee_code.")" }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Referensi:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="customer_reference" name="customer_reference" data-live-search="true">
                                                <option value="">Tidak tersedia</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}" {{ (old('customer_reference') == $customer->id ? "selected":"") }}>{{ $customer->name." (".$customer->customer_code.")" }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Terms of Payment:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="top" name="top">
                                                <option value="">Pilih</option>
                                                @foreach($terms as $term)
                                                    <option value="{{ $term }}" {{ (old('top') == $term ? "selected" : "") }}>{{ $term }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Status Priority:</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="priority" name="priority">
                                                <option value="">Pilih</option>
                                                @foreach($priorities as $priority)
                                                    <option value="{{ strtolower($priority) }}" {{ (old('priority') == strtolower($priority) ? "selected" : "") }}>{{ $priority }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(count($errors))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row" style="text-align: center; margin-bottom: 20px;">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                <button type="button" class="btn btn-info" onclick="window.location = '{{ url('/popexpress/customer') }}';">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({format: 'dd/mm/yyyy', allowInputToggle: true});
        });
    </script>
@endsection