@extends('layout.main')

@section('title')
    ORIGIN {{ $origin->name }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.css') }}">
    <style>
        table { table-layout: fixed; }
        td {
            overflow: hidden;
            word-wrap:break-word;
        }
    </style>
@endsection

@section('pageTitle')
    ORIGIN
@endsection

@section('pageDesc')
    ORIGIN {{ $origin->name }}
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-info">
                    <div class="box-header with-border">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">ORIGIN {{ $origin->name }} </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="row">
                            <form id="form-transaction">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" name="code" class="form-control" placeholder="Kode" id="code">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="kota" class="form-control" placeholder="Kota" id="kota">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" id="kecamatan">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-info" id="search">Cari</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location = '{{ URL('/popexpress/origin/'.$origin->id.'/price/create')}}'">Tambah</button>
                                    <button type="button" class="btn btn-danger" onclick="window.location = '{{ URL('/popexpress/origin/price/upload/'.$origin->id)}}'">Upload</button>
                                    <button type="button" class="btn btn-success" onclick="excel()">Unduh Excel</button>
                                    <button type="button" class="btn btn-info" onclick="window.location = '{{ url('/popexpress/origin') }}';">Kembali</button>
                                </div>
                            </form>
                        </div>
                        <table class="table table-hover cell-border" id="data-table" cellspacing="0">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Kode Detail</th>
                                <th>Kota</th>
                                <th>Kecamatan</th>
                                <th>One Day</th>
                                <th>Regular</th>
                                <th>Estimasi Awal</th>
                                <th>Estimasi Akhir</th>
                            </tr>
                            </thead>
                        </table>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header alert-info">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Konfirmasi</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Apakah anda yakin akan menghapus data yang dipilih ?</p>
                                    </div>
                                    <div class="modal-footer" style="text-align: center;">

                                        <button type="button" class="btn btn-info" id="confirm-delete">Ya</button>
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modal-edit">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header alert-info">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Edit Origin Price</h4>
                                    </div>
                                    <div class="modal-body">

                                    </div>
                                    <div class="alert-edit">

                                    </div>
                                    <div class="modal-footer" style="text-align: center;">
                                        <button type="button" class="btn btn-info" id="confirm-update">Simpan</button>
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            var deletedId = [];

            loadTable();

            $('#search').on('click', function(ev, picker) {
                reloadTable();
            });

            $('#code, #kota, #kecamatan').on('keyup', function (e){
                if(e.keyCode === 13){
                    reloadTable();
                }
            });

            function reloadTable() {
                $('#data-table').DataTable().destroy();
                loadTable();
            }

            function loadTable() {
                var DataTable = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    lengthChange: false,
                    paging: true,
                    pageLength: 30,
                    bAutoWidth: false,
                    scrollX: true,
                    scrollY: '50vh',
                    scrollCollapse: true,
                    order: [[ 2, "asc" ]],
                    ajax: {
                        url: "{{ url("/popexpress/origin/".$origin->id."/price/grid") }}",
                        data: {
                            "code": $("#code").val(),
                            "kota": $("#kota").val(),
                            "kecamatan": $("#kecamatan").val(),
                            "length": 30,
                            "_token": "{{ csrf_token() }}"
                        }
                    },
                    columns: [
                        {
                            data: "id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { data: "airport_code", name: "airport_code", width: "8%" },
                        { data: "detail_code", name: "detail_code", width: "8%" },
                        { data: "county", name: "county", width: "15%" },
                        { data: "district", name: "district", width: "15%" },
                        { data: "one_day", name: "one_day", width: "8%" },
                        { data: "regular", name: "regular", width: "8%" },
                        { data: "estimation_early", name: "estimation_early", width: "8%" },
                        { data: "estimation_late", name: "estimation_late", width: "8%" }
                    ]
                });

                $('#data-table tbody').on('dblclick', 'tr', function () {
                    var data = DataTable.row( this ).data();
                    $("#id_edit").val(data.id);
                    $("#code_edit").val(data.code);
                    $("#name_edit").val(data.name);
                    $("#area_edit").val(data.area);
                    $("#city_edit").val(data.city);
                    $("#telephone_edit").val(data.telephone);
                    $("#address_edit").val(data.address);
                    $(".alert-edit").html('');
                    $("#modal-edit").modal("show");
                });
            }

            $("#confirm-update").on("click", function (event) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: '{{ url("/origin/".$origin->id."/price/update") }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: $("#id_edit").val(),
                        code: $("#code_edit").val(),
                        name: $("#name_edit").val(),
                        area: $("#area_edit").val(),
                        city: $("#city_edit").val(),
                        telephone: $("#telephone_edit").val(),
                        address: $("#address_edit").val()

                    },
                    success: function (response) {
                        if(!response.error){
                            $(".success-title").html('Informasi');
                            $(".success-body").html(response.message);
                            $("#modal-success").modal("show");
                            $("#modal-edit").modal("hide");
                            $(".alert-edit").html('');
                            reloadTable();
                        } else {
                            $(".alert-edit").html('<div class="alert alert-danger">' + response.message + '</div>');
                        }
                        $.LoadingOverlay("hide", true);
                    },
                    failure: function(response){
                        $(".error-title").html('Error');
                        $(".error-body").html(response.message);
                        $("#modal-error").modal("show");
                        $.LoadingOverlay("hide");
                    }
                });
            });

        });

        function excel() {
            var code = $('#code').val();
            var kota = $('#kota').val();
            var kecamatan = $('#kecamatan').val();

            $.LoadingOverlay("show");

            $.ajax({
                url: '{{ url("/popexpress/origin/price/excel") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    origin_id: '{{ $origin->id }}',
                    code: code,
                    kota: kota,
                    kecamatan: kecamatan
                },
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        $.LoadingOverlay("hide");
                        window.location = '{{ url("/popexpress/origin/price/download") }}' + '?file=' + feedback['link'];
                    } else {
                        $(".error-title").html('Error');
                        $(".error-body").html(feedback['message']);
                        $("#modal-error").modal("show");
                        $.LoadingOverlay("hide");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $(".error-title").html('Error');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                        $.LoadingOverlay("hide");
                    }
                    else {
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                        $.LoadingOverlay("hide");
                    }
                }
            });

        }

    </script>
@endsection