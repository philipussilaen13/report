@extends('layout.main')

@section('title')
    Daftar 3PL
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.css') }}">
    <style>
        table { table-layout: fixed; }
        td {
            overflow: hidden;
            word-wrap:break-word;
        }
    </style>
@endsection

@section('pageTitle')
    3PL
@endsection

@section('pageDesc')
    Daftar 3PL
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-info">
                    <div class="box-header with-border">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">3PL</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="row">
                            <form id="form-transaction">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" name="code" class="form-control" placeholder="Kode" id="code">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" name="nama" class="form-control" placeholder="Nama" id="nama">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="area" class="form-control" placeholder="Area" id="area">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="kota" class="form-control" placeholder="Kota" id="kota">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-info" id="search">Cari</button>
                                    <a href="{{ url('/popexpress/3pl/create') }}" class="btn btn-primary">Tambah</a>
                                </div>
                            </form>
                        </div>
                        <table class="table table-hover cell-border" id="data-table" cellspacing="0">
                            <thead>
                            <tr>
                                <th>
                                    <button type="button" id="btn-trash" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                </th>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Telephone</th>
                                <th>Area</th>
                                <th>Kota</th>
                                <th>Alamat</th>
                            </tr>
                            </thead>
                        </table>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header alert-info">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Konfirmasi</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Apakah anda yakin akan menghapus data yang dipilih ?</p>
                                    </div>
                                    <div class="modal-footer" style="text-align: center;">

                                        <button type="button" class="btn btn-info" id="confirm-delete">Ya</button>
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modal-edit">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header alert-info">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Edit 3PL</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal" method="POST" action="{{ url('/popexpress/3pl/create') }}">
                                            <input type="hidden" id="id_edit">
                                            {{ csrf_field() }}
                                            <div class="form-group required">
                                                <label class="control-label col-sm-4">Kode:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="code_edit" name="code" placeholder="Masukan kode" value="{{ old('code') }}">
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="control-label col-sm-4">Nama:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="name_edit" name="name" placeholder="Masukan nama" value="{{ old('name') }}">
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="control-label col-sm-4">Telepon:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="telephone_edit" name="telephone" placeholder="Masukan nomor telepon" value="{{ old('telephone') }}">
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="control-label col-sm-4">Area:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="area_edit" name="area" placeholder="Masukan area" value="{{ old('area') }}">
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="control-label col-sm-4">Kota:</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" id="city_edit" name="city" data-live-search="true">
                                                        @foreach($CitiesStore as $city)
                                                            <option value="{{ $city->name }}" {{ (old('city') == $city->name ? "selected":"") }}>{{ ucwords($city->name) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="control-label col-sm-4">Alamat:</label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control" rows="5" placeholder="Masukan alamat" id="address_edit" name="address">{{ old('address') }}</textarea>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="alert-edit">

                                    </div>
                                    <div class="modal-footer" style="text-align: center;">
                                        <button type="button" class="btn btn-info" id="confirm-update">Simpan</button>
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            var deletedId = [];

            loadTable();

            $('#search').on('click', function(ev, picker) {
                reloadTable();
            });

            $('#code, #nama, #area, #kota').on('keyup', function (e){
                if(e.keyCode === 13){
                    reloadTable();
                }
            });

            $("#btn-trash").on("click", function (event) {
                $('input.selected-data:checkbox:checked').each(function () {
                    deletedId.push($(this).val());
                });
                if(deletedId.length == 0){
                    alert("Silahkan pilih data yang ingin di hapus terlebih dahulu.");
                } else {
                    $("#modal-default").modal("show");
                }
            });

            $("#confirm-delete").on("click", function (event) {
                console.log(deletedId);
                $.ajax( {
                    url: '{{ url("/popexpress/3pl/delete") }}',
                    data: {
                        id: deletedId,
                        _token : '{{ csrf_token() }}'
                    },
                    type: 'POST',
                    success: function(response){
                        $("#modal-default").modal("hide");
                        alert(response.message);
                        reloadTable();
                        deletedId = [];
                    },
                    failure: function(response){
                        alert("Terdeteksi masalah koneksi ke server. Mohon dicoba lagi.");
                    }
                } );
            });

            function reloadTable() {
                $('#data-table').DataTable().destroy();
                loadTable();
            }

            function loadTable() {
                var DataTable = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    lengthChange: false,
                    paging: true,
                    pageLength: 30,
                    bAutoWidth: false,
                    scrollX: true,
                    scrollY: '50vh',
                    scrollCollapse: true,
                    order: [[ 2, "asc" ]],
                    ajax: {
                        url: "{{ url("/popexpress/3pl/grid") }}",
                        data: {
                            "code": $("#code").val(),
                            "nama": $("#nama").val(),
                            "area": $("#area").val(),
                            "kota": $("#kota").val(),
                            "length": 30,
                            "_token": "{{ csrf_token() }}"
                        }
                    },
                    columns: [
                        {
                            data: "id",
                            name: "id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return '<input type="checkbox" name="selected_data" value="'+data+'" class="form-input selected-data">';
                            }
                        },
                        {
                            data: "id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { data: "code", name: "code", width: "8%" },
                        { data: "name", name: "name", width: "15%" },
                        { data: "telephone", name: "telephone", width: "15%" },
                        { data: "area", name: "area", width: "15%" },
                        { data: "city", name: "city", width: "15%" },
                        { data: "address", name: "address", width: "15%" },
                    ]
                });

                $('#data-table tbody').on('dblclick', 'tr', function () {
                    var data = DataTable.row( this ).data();
                    $("#id_edit").val(data.id);
                    $("#code_edit").val(data.code);
                    $("#name_edit").val(data.name);
                    $("#area_edit").val(data.area);
                    $("#city_edit").val(data.city);
                    $("#telephone_edit").val(data.telephone);
                    $("#address_edit").val(data.address);
                    $(".alert-edit").html('');
                    $("#modal-edit").modal("show");
                });
            }

            $("#confirm-update").on("click", function (event) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: '{{ url("/popexpress/3pl/update") }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: $("#id_edit").val(),
                        code: $("#code_edit").val(),
                        name: $("#name_edit").val(),
                        area: $("#area_edit").val(),
                        city: $("#city_edit").val(),
                        telephone: $("#telephone_edit").val(),
                        address: $("#address_edit").val()

                    },
                    success: function (response) {
                        if(!response.error){
                            $(".success-title").html('Informasi');
                            $(".success-body").html(response.message);
                            $("#modal-success").modal("show");
                            $("#modal-edit").modal("hide");
                            $(".alert-edit").html('');
                            reloadTable();
                        } else {
                            $(".alert-edit").html('<div class="alert alert-danger">' + response.message + '</div>');
                        }
                        $.LoadingOverlay("hide", true);
                    },
                    failure: function(response){
                        $(".error-title").html('Error');
                        $(".error-body").html(response.message);
                        $("#modal-error").modal("show");
                        $.LoadingOverlay("hide");
                    }
                });
            });

        });
    </script>
@endsection