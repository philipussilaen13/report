@extends('layout.main')

@section('title')
    Tambah 3PL
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <style>
        .form-group.required .control-label:after {
            content:" *";
            color: red;
        }
    </style>
@endsection

@section('pageTitle')
    3PL
@endsection

@section('pageDesc')
    Tambah 3PL
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Tambah 3PL</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <form class="form-horizontal" method="POST" action="{{ url('/popexpress/3pl/create') }}">
                            {{ csrf_field() }}
                            <div class="form-group required">
                                <label class="control-label col-sm-4">Kode:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="code" name="code" placeholder="Masukan kode" value="{{ old('code') }}">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="control-label col-sm-4">Nama:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Masukan nama" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="control-label col-sm-4">Telepon:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Masukan nomor telepon" value="{{ old('telephone') }}">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="control-label col-sm-4">Area:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="area" name="area" placeholder="Masukan area" value="{{ old('area') }}">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="control-label col-sm-4">Kota:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="city" name="city" data-live-search="true">
                                        @foreach($Cities as $city)
                                            <option value="{{ $city->name }}" {{ (old('city') == $city->name ? "selected":"") }}>{{ ucwords($city->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="control-label col-sm-4">Alamat:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="5" placeholder="Masukan alamat" id="address" name="address">{{ old('address') }}</textarea>
                                </div>
                            </div>
                            @if(count($errors))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <button type="button" class="btn btn-info" onclick="window.location = '{{ url('/popexpress/3pl') }}';">Kembali</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')

@endsection