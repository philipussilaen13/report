@extends('layout.main')

@section('title')
    Import Price
@endsection

@section('css')
@endsection

@section('pageTitle')
    Price
@endsection

@section('pageDesc')
    Import Price
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Import Price</h3>
                    </div>
                    <div class="box-body border-radius-none">
                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="{{ url('/popexpress/prices/import') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="file_upload" class="col-md-4 control-label">File Import:</label>
                                <div class="col-md-8">
                                    <input type="file" class="form-control-file" name="file_upload" id="file_upload" data-allowed-file-extensions='["xls", "xlsx"]' required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="button" class="btn btn-success" onclick="kembali()">Kembali</button>
                                    <button type="button" class="btn btn-primary" onclick="template()">Template</button>
                                    <button type="submit" class="btn btn-info" id="btn_upload">Import</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-3">
                                    @if(isset($total))
                                        <div class="alert alert-info">
                                            <strong>Importation log:</strong><br><br>

                                            Cannot import data at {{ $row }} row.<br>
                                            Total importation {{ $total }}, new {{ $new }}, update {{ $update }}, skip {{ $skip }}. <br>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </form>

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if(count($errors))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </section>
    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('form').submit(function () {
                $.LoadingOverlay("show");
            })
        });

        function template() {
            var queries = {};
            queries['_token'] = "{{ csrf_token() }}";
            $.LoadingOverlay("show");

            $.ajax({
                url: '{{ url("/popexpress/prices/template") }}',
                type: 'POST',
                data: queries,
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        $.LoadingOverlay("hide");
                        window.location = '{{ url("/popexpress/prices/download") }}' + '?file=' + feedback['link'];
                    } else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Informasi');
                        $(".error-body").html(feedback['message']);
                        $("#modal-error").modal("show");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function kembali() {
            window.location = '{{ url('/popexpress/prices') }}';
        }
    </script>
@endsection