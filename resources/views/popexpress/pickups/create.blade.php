@extends('layout.main')

@section('title')
    Add Pickup
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('pageTitle')
    Pickups
@endsection

@section('pageDesc')
    Add Pickup
@endsection

@section('content')
    <form method="post" method="post" action="{{ url('/popexpress/pickups/store') }}">
        {{ csrf_field() }}
        <div class="box box-solid">
            <div class="box-body">
                <h3>Add Pickup</h3>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="form-group">
                            <label>Branch</label>
                            <select class="form-control" name="branch_id" required>
                                <option value="">Select</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->code.' - '.$branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Time</label>
                            <select class="form-control" name="pickup_time" required>
                                <option value="">Select</option>
                                @foreach ($times as $time)
                                    <option value="{{ $time }}">{{ ucwords($time) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Location</label>
                            <select class="form-control select2" name="pickup_location" id="pickup_location" required>
                                <option value="">Select</option>
                                @foreach ($locations as $location)
                                    <option value="{{ $location->id }}">{{ $location->detail_code.' - '.$location->district.', '.$location->county.', '.$location->province }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" name="pickup_address" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Expected Total Items</label>
                            <input type="text" name="expected_total_items" id="expected_total_items" maxlength="10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Customer Name</label>
                            <input type="text" name="pickup_customer_name" id="pickup_customer_name" maxlength="255" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Customer Email</label>
                            <input type="email" name="pickup_customer_email" id="pickup_customer_email" maxlength="255" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Customer Phone</label>
                            <input type="text" name="pickup_customer_phone" id="pickup_customer_phone" maxlength="20" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Accounts</label>
                            <select class="form-control select2" name="account_id" id="account_id" required onchange="getCustomer()">
                                <option value="">Select</option>
                                @foreach ($accounts as $account)
                                    <option value="{{ $account->id }}">{{ $account->account_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <select class="form-control" id="pickup_type" name="pickup_type" required onchange="getLocation()">
                                <option value="">Select</option>
                                @foreach ($types as $type)
                                    <option value="{{ $type }}">{{ ucwords(str_replace('-',' ', $type)) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Source</label>
                            <select class="form-control" name="pickup_source" required>
                                <option value="">Select</option>
                                @foreach ($sources as $source)
                                    <option value="{{ $source }}">{{ ucwords($source) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Alternative Address</label>
                            <textarea class="form-control" name="pickup_alternative_address"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Remark</label>
                            <textarea class="form-control" name="pickup_remarks"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ url('popexpress/pickups') }}" class="btn btn-flat btn-warning">Back</a>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script src="{{ asset('js/popexpress.core.js') }} ?>"></script>
    <script src="{{ asset('plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.select2').select2();
            $("#expected_total_items,#pickup_customer_phone").keypress(function(event){
                var inputValue = event.which;
                if((inputValue >= 48 && inputValue <= 57) || inputValue == 13 || inputValue == 8) {
                    return true;
                } else {
                    event.preventDefault();
                }
            });
        });
        
        function getLocation() {
            $.LoadingOverlay("show");
            var type = $('#pickup_type').val();
            $('#pickup_location').empty();
            $.ajax({
                url: '{{ url("/popexpress/pickups/get_location") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    type: type
                },
                success: function (data) {
                    for(var no=0; no < data.length; no++) {
                        $('#pickup_location').append('<option value="' + data[no].id + '">' + data[no].detail_code + ' - ' + data[no].district + ', ' + data[no].county + ', ' + data[no].province + '</option>');
                    }
                    $("#pickup_location").select2({
                        placeholder: "Select"
                    });
                    $.LoadingOverlay("hide");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function getCustomer() {
            $.LoadingOverlay("show");
            var account = $('#account_id option:selected').val();
            $('#pickup_customer_name').val('');
            $('#pickup_customer_email').val('');
            $('#pickup_customer_phone').val('');
            $.ajax({
                url: '{{ url("/popexpress/pickups/get_customer") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    account: account
                },
                success: function (data) {
                    console.log(data);
                    if(data !== false) {
                        $('#pickup_customer_name').val(data.name);
                        $('#pickup_customer_email').val(data.email);
                        $('#pickup_customer_phone').val(data.phone);
                    }
                    $.LoadingOverlay("hide");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }
    </script>
@endsection