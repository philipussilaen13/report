@extends('layout.main')

@section('title')
    Edit Employee
@endsection

@section('css')
@endsection

@section('pageTitle')
    Employee
@endsection

@section('pageDesc')
    Edit Employee
@endsection

@section('content')
    <form method="post" method="post" action="{{ url('/popexpress/employees/store') }}">
        <input type="hidden" value="{{ $employee->report_users_id }}" name="id">
        {{ csrf_field() }}
        <div class="box box-solid">
            <div class="box-body">
                <h3>Details</h3>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="form-group">
                            <label>Code</label>
                            <input type="text" name="code" class="form-control" maxlength="30" value="{{ $employee->code }}" required {{ (!$access ? 'disabled' : '') }}>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" maxlength="255" value="{{ $users[$employee->report_users_id]['name'] }}" required {{ (!$access ? 'disabled' : '') }}>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" maxlength="255" value="{{ $users[$employee->report_users_id]['email'] }}" required {{ (!$access ? 'disabled' : '') }}>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" class="form-control" maxlength="20" value="{{ $users[$employee->report_users_id]['phone'] }}" required {{ (!$access ? 'disabled' : '') }}>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" maxlength="255" value="{{ $users[$employee->report_users_id]['username'] }}" {{ (!$access ? 'disabled' : '') }}>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            @if($access)
                                <br><a href="#" id="change-password-link" data-toggle="modal" data-target="#modal-default">Change Password</a>
                            @else
                                <input type="password" name="password" class="form-control" value="*****" disabled>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status" {{ (!$access ? 'disabled' : '') }}>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status }}" {!! ($status == $users[$employee->report_users_id]['status'] ? 'selected="selected"' : '') !!}>{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Branch</label>
                            <select class="form-control" name="branch" {{ (!$access ? 'disabled' : '') }}>
                                <option value="">Select</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" {!! ($branch->id == $employee->branch_id ? 'selected="selected"' : '') !!}>{{ $branch->code.' - '.$branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="all_branch" {!! ($employee->all_branch == 1 ? 'checked="checked"' : '') !!}> Access All Branch
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-body">
                <h3>Groups</h3>
                <div class="row">
                    @foreach($groups as $group)
                        @php
                            $selectedGroup = "";
                            if(in_array($group->id, explode(',', $users[$employee->report_users_id]['groupiduser'] ))){$selectedGroup = "checked";}
                        @endphp
                        <div class="col-md-6">
                            <input type="checkbox" id="employee_group" name="employee_group[]" value="{{ $group->id }}" {{ $selectedGroup }}> {{ strtoupper(str_replace('_', ' ', $group->name)) }}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                @if($access)
                    <input type="submit" value="Save" class="btn btn-primary" >
                @endif
                <a href="{{ url('popexpress/employees') }}"><button class="btn btn-flat btn-warning">Back</button></a>
            </div>
        </div>
    </form>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <form autocomplete="off" method="post" action="{{ url('/popexpress/employees/changePassword') }}">
                <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id"  value="{{ $employee->report_users_id }}" class="form-control">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" name="password" id="password" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Change New Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" maxlength="100" class="form-control" required>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save" id="btn-add">
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection