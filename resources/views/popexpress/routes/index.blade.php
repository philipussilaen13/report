@extends('layout.main')

@section('title')
    List Routes
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <style>
        .table th {
            text-align: center;
        }
        .list-routes li {
            display: inline-block;
            margin: 10px;
            text-align: center;
            font-weight: bold;
            vertical-align: middle;
        }
    </style>
@endsection

@section('pageTitle')
    Routes
@endsection

@section('pageDesc')
    List Routes
@endsection

@section('content')
    <div class="box box-solid">
        <div class="box-body">
            <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modal-default">Filter</button>
            <a href="{{ url('popexpress/routes') }}"><button class="btn btn-flat btn-warning">Reset Filter</button></a>

            @if($accessAdd)
            <div class="pull-right">
                <a href="{{ url('popexpress/routes/create') }}"><button class="btn btn-flat btn-primary">Add Routes</button></a>
            </div>
            @endif

            <hr>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Table --}}
            <div class="table-responsive">
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Routes</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($routes as $index => $route)
                        <tr>
                            <td width="75" align="center">{{ $routes->firstItem() + $index }}</td>
                            <td>
                                <strong>{{ $route->branch_name }}</strong><br>
                                <strong>
                                    {{ (!empty($route->airport_code) ? 'Airport Code : '.$route->airport_code.', '  : '') }}
                                    {{ (!empty($route->detail_code) ? 'Detail Code : '.$route->detail_code.', '  : '') }}
                                    {{ (!empty($route->province) ? 'Province : '.$route->province.', '  : '') }}
                                    {{ (!empty($route->county) ? 'County : '.$route->county.', '  : '') }}
                                    {{ (!empty($route->district) ? 'District : '.$route->district.', '  : '') }}
                                    {{ ($route->is_locker == '1' ? 'Locker'  : '') }}
                                </strong>
                                @php $routings = json_decode($route->routing); @endphp
                                <br>
                                <ul class="list-routes">
                                    @foreach($routings as $key => $routing)
                                        @php $branch = $branchList[$routing]; @endphp
                                        <li><img src="{{ url('/img/popexpress/'.($branch['type'] == '3pl' ? 'icon_3pl' : 'icon_branch').'.png') }}"><br>{{ $branch['label'] }}</li>
                                        @if($routing !== end($routings))
                                            <li><img src="{{ url('/img/popexpress/icon_next.png') }}"></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                            <td align="center">
                                @if($accessAdd)
                                <a href="{{ url('/popexpress/routes/'.$route->id) }}">
                                    <button class="btn btn-flat btn-info btn-small btn-update">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    @if(count($routes) == 0)
                        <tr><td colspan="8" align="center">Please assign filter to display route list.</td></tr>
                    @endif
                    </tbody>
                </table>
                {{ (!empty($routes) ? $routes->appends($_GET)->links() : '')}}
                <button type="button" class="btn btn-default disabled pull-right" data-dismiss="modal"><strong>Total : {{ (!empty($routes) ? $routes->total() : '0')}}</strong></button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <div class="modal-body">
                    <form id="form-filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Origin Branch</label>
                                    <select class="form-control" id="branch_id" name="branch_id" required>
                                        <option value="">Select</option>
                                        @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->code.' - '.$branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Airport Code</label>
                                    <input type="text" name="airport_code" id="airport_code" class="form-control"  required>
                                </div>
                                <div class="form-group">
                                    <label>Province</label>
                                    <input type="text" name="province" id="province" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>District</label>
                                    <input type="text" name="district" id="district" class="form-control" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Destination Branch</label>
                                    <select class="form-control" name="branch_destination_id" id="branch_destination_id_filter" >
                                        <option value="">Select</option>
                                        @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->code.' - '.$branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Detail Code</label>
                                    <input type="text" name="detail_code" id="detail_code" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>County</label>
                                    <input type="text" name="county" id="county" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <br>
                                    <input type="checkbox" name="is_locker" id="is_locker" value="1" > Locker
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-filter">Save</button>
                </div>
            </div>
        </div>
    </div>

    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('js/popexpress.core.js') }}>"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#start_date, #end_date').datepicker({ format: 'dd/mm/yyyy' });
        });

        $('#btn-filter').on('click', function(event) {
            $('#form-filter').submit();
        });

        $('#btn-add').on('click', function(event) {
            $('#form-add').submit();
        });

        $('.btn-update').on('click', function() {
            var data = $(this).data();
            $('#modal-add').modal('show');
            $('#id').val(data.id);
            $('#origin_id').val(data.originid);
            $('#account_id').val(data.accountid);
            $('#type').val(data.type);
            $('#value').val(parseInt(data.value));
            $('#start_date').val(data.startedit);
            $('#end_date').val(data.endedit);
            $('#airport_code').val(data.airport_code);
            $('#detail_code').val(data.detail_code);
            $('#province').val(data.province);
            $('#county').val(data.county);
            $('#district').val(data.district);
            if(data.is_locker == 1) {
                $('#is_locker').prop('checked', true);
            } else {
                $('#is_locker').prop('checked', false);
            }

            if(data.override == 'Yes') {
                $('#override').prop('checked', true);
            } else {
                $('#override').prop('checked', false);
            }
        });

        $('#btn-delete').on('click', function(event) {
            $('#form-add').submit();
        });

        $('#export').on('click', function(event) {
            var queries = {};
            var currentUrl = document.location.search;
            if(currentUrl.indexOf('?') != -1) {
                $.each(document.location.search.substr(1).split('&'),function(c,q){
                    var i = q.split('=');
                    queries[i[0].toString()] = i[1].toString();
                });
            }
            queries['_token'] = "{{ csrf_token() }}";
            $.LoadingOverlay("show");

            $.ajax({
                url: '{{ url("/popexpress/price_list/export") }}',
                type: 'POST',
                data: queries,
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        $.LoadingOverlay("hide");
                        window.location = '{{ url("/popexpress/destinations/download") }}' + '?file=' + feedback['link'];
                    } else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Informasi');
                        $(".error-body").html(feedback['message']);
                        $("#modal-error").modal("show");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        });
    </script>
@endsection