@extends('layout.main')

@section('title')
    Edit Routes
@endsection

@section('css')
@endsection

@section('pageTitle')
    Routes
@endsection

@section('pageDesc')
    Edit Routes
@endsection

@section('content')
    <form method="post" method="post" action="{{ url('/popexpress/routes/store') }}">
        <input type="hidden" name="id" value="{{ $route->id }}">
        {{ csrf_field() }}
        <div class="box box-solid">
            <div class="box-body">
                <h3>Edit Routes</h3>
                {{ csrf_field() }}
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="form-group">
                            <label>Original Branch</label>
                            <select class="form-control" name="firstmile_branch_id" required id="firstmile_branch_id" onchange="setBranchId()">
                                <option value="">Select</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" {{ ($route->firstmile_branch_id == $branch->id ? 'selected="selected"' : '') }}>{{ $branch->code.' - '.$branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Destination Airport Code</label>
                            <input type="text" name="airport_code" class="form-control" value="{{ $route->airport_code }}">
                        </div>
                        <div class="form-group">
                            <label>Destination Province</label>
                            <input type="text" name="province" class="form-control" value="{{ $route->province }}">
                        </div>
                        <div class="form-group">
                            <label>Destination District</label>
                            <input type="text" name="district" class="form-control" value="{{ $route->district }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <br><br>
                            <input type="checkbox" name="is_locker" value="1" {{ ($route->is_locker == 1 ? 'checked="checked"' : '') }}> Locker
                        </div>
                        <div class="form-group">
                            <label>Destination Detail Code</label>
                            <input type="text" name="detail_code" class="form-control" value="{{ $route->detail_code }}">
                        </div>
                        <div class="form-group">
                            <label>Destination County</label>
                            <input type="text" name="county" class="form-control" value="{{ $route->county }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-body">
                <h3>Routing</h3>
                <a class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-default">Add Branch</a><br><br>
                <table class="table table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th width="100" align="center">No.</th>
                        <th align="center">Account</th>
                        <th width="200" align="center">Action</th>
                    </tr>
                    </thead>
                    <tbody id="routing-panel">
                        @foreach($routes as $key => $item)
                            <tr>
                                <td> {{ $key+1 }} </td>
                                <td> {{ $branchList[$item]['label'] }} </td>
                                <td>
                                    @if($key > 1)
                                        <a href="javascript:void(0)" onclick="moveTo( {{ $key }} , {{ ($key-1) }} )"><img src="{{ url('/img/popexpress/icon_up.png') }}"></a>
                                    @endif
                                    @if($key > 0 && $key < end($routes))
                                        <a href="javascript:void(0)" onclick="moveTo( {{ $key }} , {{ ($key+1) }} )"><img src="{{ url('/img/popexpress/icon_down.png') }}"></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <input type="hidden" name="routing" id="routing" value="{{ $routing }}">
                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ url('popexpress/routes') }}" class="btn btn-flat btn-warning">Back</a>
            </div>
        </div>
    </form>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Branch</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Origin Branch</label>
                                <select class="form-control" id="destination_branch_id" name="destination_branch_id" required>
                                    <option value="">Select</option>
                                    @foreach ($branches as $branch)
                                        <option value="{{ $branch->id }}">{{ $branch->code.' - '.$branch->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-add-branch" onclick="addBranchId()">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/popexpress.core.js') }}>"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#start_date, #end_date').datepicker({ format: 'dd/mm/yyyy' });
        });

        function setBranchId() {
            $.LoadingOverlay("show");
            $('#routing-panel').html('');
            $('#routing').val('');

            var firstmile_branch_id = $('#firstmile_branch_id').val();
            var firstmile_branch_text = $('#firstmile_branch_id option:selected').text();
            var routing = $('#routing').val();

            $('#routing').val(firstmile_branch_id + ',' + routing);
            $('#routing-panel').append(
                '<tr>' +
                '<td>1</td>' +
                '<td>' + firstmile_branch_text + '</td>' +
                '<td>' +
                '<a href="#"><img src="{{ url('/img/popexpress/icon_up.png') }}"> </a> ' +
                '<a href="#"><img src="{{ url('/img/popexpress/icon_down.png') }}"> </a> ' +
                '</td>' +
                '</tr>'
            );
            $("#destination_branch_id option").each(function() {
                $("#destination_branch_id option[value='" + $(this).val() + "']").attr('disabled', false );
            });
            $("#destination_branch_id option[value='" + firstmile_branch_id + "']").attr('disabled', true );
            $.LoadingOverlay("hide");
        }

        function addBranchId() {
            $.LoadingOverlay("show");
            var listRoutings = [];
            var rows = 1;
            $('#routing-panel>tr').each(function () {
                var tdIndex = 1;
                var listTd = [];
                $('td', this).each(function () {
                    listTd.push($(this).text());
                    tdIndex++;
                });
                listRoutings.push(listTd);
                rows++;
            });
            console.log(listRoutings);
            var destination_branch_id = $('#destination_branch_id').val();
            var destination_branch_text = $('#destination_branch_id option:selected').text();
            listRoutings.push([
                destination_branch_id,
                destination_branch_text
            ]);
            console.log(listRoutings);

            $('#routing-panel').html('');
            $('#routing').val('');
            var number = 1;
            var routing = $('#routing').val();
            var last = listRoutings.length-1;

            for(var no=0; no<=listRoutings.length-1; no++) {
                routing += listRoutings[no][0] + ',';
                $('#routing-panel').append(
                    '<tr>' +
                    '<td>' + number + '</td>' +
                    '<td>' + listRoutings[no][1] + '</td>' +
                    '<td>' +
                    (no > 1 ? '<a href="javascript:void(0)" onclick="moveTo(' + no + ', ' + (no - 1) + ')"><img src="{{ url('/img/popexpress/icon_up.png') }}"> </a> ' : '') +
                    (no > 0 && no < last ? '<a href="javascript:void(0)" onclick="moveTo(' + no + ', ' + (no + 1) + ')"><img src="{{ url('/img/popexpress/icon_down.png') }}"> </a> ' : '' ) +
                    '</td>' +
                    '</tr>'
                );
                number++;
            }
            var all_routes = routing + destination_branch_id + ',';
            $('#routing').val(all_routes);

            console.log(routing);
            var arrRoutings = all_routes.split(',');
            $.each( arrRoutings, function( key, value ) {
                if(value != '') {
                    $("#destination_branch_id option[value='" + value + "']").attr('disabled', true );
                }
            });

            $('#modal-default').modal('hide');
            $.LoadingOverlay("hide");
        }

        function moveTo(oldIndex, newIndex) {
            var listRoutings = [];
            var rows = 1;
            $('#routing-panel>tr').each(function () {
                var tdIndex = 1;
                var listTd = [];
                $('td', this).each(function () {
                    listTd.push($(this).text());
                    tdIndex++;
                });
                listRoutings.push(listTd);
                rows++;
            });
            console.log("Base : " + listRoutings);
            var destination_branch_id = $('#destination_branch_id').val();
            var destination_branch_text = $('#destination_branch_id option:selected').text();
            listRoutings.push([
                destination_branch_id,
                destination_branch_text
            ]);

            if (newIndex >= listRoutings.length) {
                newIndex = listRoutings.length - 1;
            }
            listRoutings.splice(newIndex, 0, listRoutings.splice(oldIndex, 1)[0]);
            var arrRoutings = listRoutings;
            console.log("Move Result : " + arrRoutings);

            $('#routing-panel').html('');
            var number = 1;
            var routingList = '';
            var last = arrRoutings.length-1;

            for(var no=0; no<=arrRoutings.length-1; no++) {
                console.log(arrRoutings[no][0]);
                if(arrRoutings[no][0] !== null && arrRoutings[no][0] !== '') {
                    routingList += arrRoutings[no][0] + ',';
                    $('#routing-panel').append(
                        '<tr>' +
                        '<td>' + number + '</td>' +
                        '<td>' + arrRoutings[no][1] + '</td>' +
                        '<td>' +
                        (no > 1 ? '<a href="javascript:void(0)" onclick="moveTo(' + no + ', ' + (no - 1) + ')"><img src="{{ url('/img/popexpress/icon_up.png') }}"> </a> ' : '') +
                        (no > 0 && no < last ? '<a href="javascript:void(0)" onclick="moveTo(' + no + ', ' + (no + 1) + ')"><img src="{{ url('/img/popexpress/icon_down.png') }}"> </a> ' : '' ) +
                        '</td>' +
                        '</tr>'
                    );
                    number++;
                }
            }
            console.log(routingList);
            var newRoutings = $('#routing').val();
            var arrRouting = newRoutings.replace(/(^,)|(,$)/g, "").split(',');
            if (newIndex >= arrRouting.length) {
                newIndex = arrRouting.length - 1;
            }
            arrRouting.splice(newIndex, 0, arrRouting.splice(oldIndex, 1)[0]);
            $('#routing').val(arrRouting.toString());
            console.log(arrRouting);
        }

    </script>
@endsection