@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Time picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    {{-- Morris CSS --}}
  	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@stop

@section('title')
    Executive Summary
@endsection

@section('pageTitle')
    Executive Summary Verified PopWarung
@endsection

@section('pageDesc')
    
@endsection

@section('content')
<style>
 .header-title-custom{
     background-color: #D3D3D3;
     border-radius: 15px; 
     padding-left: 15px; 
     padding-bottom: 2px;
     height: 30px;
 }
 #graph {
    width: 800px;
    height: 250px;
    margin: 20px auto 0 auto;
 }
 pre {
    height: 250px;
    overflow: auto;
 }
</style>
    <section class="content">
        {{-- Table --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-filter">
                    <div class="box-body border-radius-none">
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row header-filter">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-city" name="city" id="city">
                                                    <option value="all">Semua Kota</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3" style="display: none">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-user-type" name="userType" id="userType">
                                                
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-monthly-1" name="start-month" id="start-month"></select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-monthly-2" name="end-month" id="end-month"></select>
                                            </div>
                                        </div>
                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div style="margin-right:0px;margin-top: 15px;">
                                                    <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <br>
                        
                        <!-- Line chart Belanja Stok v.s Penjualan PopWarung (fast_sales dan popshop) -->
                        
                        <div class="row box-info">
                			<div class="col-sm-12">
                                <div class="card-box text-center">
                                    <h4 class="m-t-0">Total transaction amount of "Stock Purchase" vs POS "Penjualan &amp; Penjualan Cepat"</h4>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#458bc4"></i> Belanja Stock</h5>
                                            </li>
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#23b195"></i> Penjualan</h5>
                                            </li>
                                        </ul>
                                    </div>
            
                                    <div id="dashboard-line-stacked" style="height: 300px;"></div>
            
                                </div>
                            </div>
                        </div> 
                        
                        <!-- Bar chart Belanja Stok Active User v.s Verified User -->
                        
                        <div class="row box-info">
                			<div class="col-sm-12">
                                <div class="card-box text-center">
                                    <h4 class="m-t-0">Number of verified user PopWarung make stock purchase</h4>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#23b195"></i> Perform stock purchase</h5>
                                            </li>
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#f98009"></i> Non perform stock purchase</h5>
                                            </li>
                                        </ul>
                                    </div>
            
                                    <div id="dashboard-bar-sales-stock-user" style="height: 300px;"></div>
            
                                </div>
                            </div>
                        </div> 
                        
                        <!-- Bar chart Belanja Stok v.s Sales Stock by Active User -->
                        
                        <div class="row box-info">
                			<div class="col-sm-12">
                                <div class="card-box text-center">
                                    <h4 class="m-t-0">Comparison number of verified user PopWarung that made sales transaction (Penjualan &amp; Penjualan Cepat)</h4>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#23b195"></i> Using POS</h5>
                                            </li>
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#f98009"></i> Not using POS</h5>
                                            </li>
                                        </ul>
                                    </div>
            
                                    <div id="dashboard-bar-pos-user" style="height: 300px;"></div>
            
                                </div>
                            </div>
                        </div> 
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    
    {{-- NUMERATOR --}}
    <script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    {{-- Morris Chart --}}
	<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 15;
        var isBoxHide = true;
        var userTypes = {!! json_encode($user_types) !!};
        var cities =  {!! json_encode($cities) !!};
        var monthly =  {!! json_encode($monthly) !!};
        var lineChart = null;
        var barChart_sales_stock_user = null;
        var barChart_pos_user = null;
        var month = {{ $month }};

        $(function(){

            $(".select2-user-type").select2({
                  data: userTypes
            }).val('warung').trigger('change')
            ;

            $(".select2-city").select2({
                  data: cities
            })
            ;

            $(".select2-monthly-2").select2({
                  data: monthly
            }).val(month).trigger('change');

            // First, set data and "on change" for start-month
            $(".select2-monthly-1").select2({
                  data: monthly
            }).on('change', function(){

				var end_month = parseInt($('#end-month').val()); // get latest end-month index
				
                // start by setting everything to enabled
                $('select[name="end-month"] option').prop('disabled',false);

                // disabled previous options
				for(i = 1; i < $('#start-month').val(); i++){
					$('#end-month>option[value='+i+']').attr('disabled','disabled');
				}
				
				var start_month = parseInt($('#start-month').val()); // get latest start-month index

				setTimeout(function () {
    				$('select').select2();
    				//if latest end-month index smaller than start-month index, set new end-month index
    				if(start_month > end_month){
    					$('select[name="end-month"]').val(start_month+1).trigger('change');
    				}
				});
            });

            // Second, set val for start-month
            if(month >= 3) {
            	month = month - 2;
            } else if(month = 2) {
            	month = month - 1;
            }
            
        	$(".select2-monthly-1").val(month).trigger('change');
            
        	lineChart = new Morris.Line({
                element				: 'dashboard-line-stacked',
                xkey				: 'week',
                ykeys				: ['total_trx_belanja_stock', 'total_trx_pos'],
                ymin				: 0,
                labels				: ['Belanja Stock', 'Penjualan'],
                fillOpacity			: ['0.1'],
                pointStrokeColors	: ['#999999'],
                behaveLikeLine		: true,
                gridLineColor		: '#eef0f2',
                hideHover			: 'auto',
                lineWidth			: '3px',
                pointSize			: 0,
                parseTime			: false,
                resize				: true, //defaulted to true
                lineColors			: ['#458bc4', '#23b195'],
                gridIntegers		: true,
                hoverCallback		: function(index, options, content) {
                    return	"<div class='morris-hover-row-label'> "+options.data[index].date_range+"</div>" +
                            "<div class='morris-hover-point' style='color: #458bc4'>" +
                            options.labels[0]+": " +
                            formatPriceTopPurchased(options.data[index].total_trx_belanja_stock) +
                            "</div><div class='morris-hover-point' style='color: #23b195'>" +
                            options.labels[1]+": " +
                            formatPriceTopPurchased(options.data[index].total_trx_pos) +
                            "</div>";
                }
            });
            
        	barChart_sales_stock_user = new Morris.Bar({
                element				: 'dashboard-bar-sales-stock-user',
                xkey				: 'week',
                ykeys				: ['active_user','verified_user'],
                ymin				: 0,
                labels				: ['Perform stock purchase','Non perform stock purchase'],
                hideHover			: 'auto',
                resize				: true, //defaulted to true
                barColors			: ['#23b195', '#f98009'],
                stacked				: false,
                hoverCallback		: function(index, options, content) {
                    return	"<div class='morris-hover-row-label'> "+options.data[index].date_range+"</div>" +
                            "<div class='morris-hover-point' style='color: #458bc4'>" +
                            options.labels[0]+": " +
                            formatPriceTopPurchased(options.data[index].active_user) +
                            "</div><div class='morris-hover-point' style='color: #23b195'>" +
                            options.labels[1]+": " +
                            formatPriceTopPurchased(options.data[index].verified_user) +
                            "</div>";
                }
            });
            
        	barChart_pos_user = new Morris.Bar({
                element				: 'dashboard-bar-pos-user',
                xkey				: 'week',
                ykeys				: ['active_user','verified_user'],
                ymin				: 0,
                labels				: ['Using POS','Non using POS'],
                hideHover			: 'auto',
                resize				: true, //defaulted to true
                barColors			: ['#23b195', '#f98009'],
                hoverCallback		: function(index, options, content) {
                    return	"<div class='morris-hover-row-label'> "+options.data[index].date_range+"</div>" +
                            "<div class='morris-hover-point' style='color: #458bc4'>" +
                            options.labels[0]+": " +
                            formatPriceTopPurchased(options.data[index].active_user) +
                            "</div><div class='morris-hover-point' style='color: #23b195'>" +
                            options.labels[1]+": " +
                            formatPriceTopPurchased(options.data[index].verified_user) +
                            "</div>";
                }
            });

            dashboard();
            
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                dashboard();
            });
        });

        function dashboard(){
            
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-info', 'box-info');

            $.ajax({
                url: "{{ route('summary-of-executive') }}",
                data: {
                    _token                  : '{{ csrf_token() }}',
                    city                    : $('#city').val() ? $('#city').val() : 'all',
                    start_month 			: $('#start-month').val(),
                    end_month 				: $('#end-month').val(),
                    user_type             	: $("#userType").val() ? $("#userType").val() : 'all',
                },
                type: 'GET',
                success: function(data){
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-info', 'box-info');

                	lineChart.options['ymax'] = data.payload.chart_line_y_max*1;
                	lineChart.setData(data.payload.chart_line);

                	barChart_sales_stock_user.options['ymax'] = data.payload.sales_stock_users_bar_chart_y_max*1;
                	barChart_sales_stock_user.setData(data.payload.sales_stock_users_bar_chart);

                	barChart_pos_user.options['ymax'] = data.payload.pos_users_bar_chart_y_max*1;
                	barChart_pos_user.setData(data.payload.pos_users_bar_chart);
                }
            });
        }
        
        function formatPrice(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return 'Rp '+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
        function formatPriceTopPurchased(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        function getMonths(weekNumber) {
    	  	var beginningOfWeek = moment().week(weekNumber).startOf('week');
    	  	var endOfWeek = moment().week(weekNumber).startOf('week').add(6, 'days');
    	  	console.log(beginningOfWeek.format('YYYY-MM-DD'));
    	  	console.log(endOfWeek.format('YYYY-MM-DD'));
    	}
        
    </script>
@stop