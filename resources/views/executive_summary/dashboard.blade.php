@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    {{-- Morris CSS --}}
  	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
@stop

@section('title')
    Executive Summary
@endsection

@section('pageTitle')
    Stock Purchase Summary
@endsection

@section('pageDesc')
    
@endsection

@section('content')
<style>
 .header-title-custom{
     background-color: #D3D3D3;
     border-radius: 15px; 
     padding-left: 15px; 
     padding-bottom: 2px;
     height: 30px;
 }

.header-title-custom + .tooltip > .tooltip-inner {
    background-color: #000000; 
    color: #FFFFFF; 
    border: 1px solid black; 
    padding: 15px;
    font-size: 12px;
    max-width: 100%;
    text-align: left;
}

 #graph {
    width: 800px;
    height: 250px;
    margin: 20px auto 0 auto;
 }
 pre {
    height: 250px;
    overflow: auto;
 }
 
 tbody:hover,
 tr.hover,
 th.hover,
 td.hover,
 tr.hoverable:hover {
    background-color: #DCDCDC;
 }

</style>
    <section class="content">
        {{-- Table --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-filter">
                    <div class="box-body border-radius-none">
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row header-filter">
                                        
                                        <div class="col-md-3" style="display: none;">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-locker" name="locker" id="locker"></select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-user-type" name="userType" id="userType">
                                                
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-md-3 text-right">
                                            <div class="form-group">
                                                <div class="row" style="margin-right:0px;margin-top: 15px;padding-left: 10px">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control" id="daterange-transaction" name="dateRange-transaction" placeholder="Transaction Date" readonly="readonly" style="background-color: white;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div style="margin-right:0px;margin-top: 15px;">
                                                    <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <br>

                        <div class="row box-info">
                        	<div class="header-title-custom" style="margin-bottom: 10px; width: 98%; margin-left: 12px" data-toggle="tooltip" data-placement="bottom" 
                        			data-html="true" title="{{trans('executive_summary.tooltip.stock_purchase_summary')}}">
                        		<h4 class="m-b-20" style="padding-top: 5px"><strong>Summary</strong></h4>
                        		<p class="glyphicon glyphicon-info-sign info-icon-tooltip" style="padding-top: 8px; margin-right: 1%"></p>
                        	</div>
                        </div>
                        
                        <div class="row box-info">
                        	<div class="col-lg-12">
                                <div class="table-responsive div_content">
                                    <table id="gridcontent" class="table">
                                        <thead>
                                            <tr style="color: #f1f1f1;background: rgb(23,139,193);">
                                                <th class="text-center"><strong>{{trans('executive_summary.summary.date')}}</strong></th>
                                                <th class="text-left"><strong>{{trans('executive_summary.summary.detail_item')}}</strong></th>
                                                <th class="text-right"><strong>{{trans('executive_summary.summary.7_days')}}</strong></th>
                                                <th class="text-right"><strong>{{trans('executive_summary.summary.14_days')}}</strong></th>
                                                <th class="text-right"><strong>{{trans('executive_summary.summary.30_days')}}</strong></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.freezeheader.js') }}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    
    {{-- NUMERATOR --}}
    <script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    {{-- Morris Chart --}}
	<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 15;
        var lockers =  {!! json_encode($lockers) !!};
        var userTypes = {!! json_encode($user_types) !!};
        var executed = false;
        var top_performing_agent_dataSet = [];
        var lineChart = null;
        
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });

        $(document).ready(function () {
            $("#gridcontent").freezeHeader();
        });
        
        $(function(){

            $('.select2').select2();

            $(".select2-locker").select2({
                  data: lockers
            });

            $(".select2-user-type").select2({
                  data: userTypes
            }).val('warung').trigger('change');

            dashboard();
            
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                dashboard();
            });
        });

        function dashboard(){
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-info', 'box-info');
            
            $.ajax({
                url: "{{ route('summary-of-dashboard') }}",
                data: {
                    _token                  : '{{ csrf_token() }}',
                    locker_id              	: $('#locker').val() ? $('#locker').val() : 'all',
                    daterange_transaction 	: transactionDateRange,
                    user_type             	: $("#userType").val() ? $("#userType").val() : 'all',
                },
                type: 'GET',
                success: function(data){
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-info', 'box-info');
                	result = data.payload.data;
                	i = 0;
                	removeField('gridcontent', 1);
                	
                	$.each(result, function(index, value){

                        $('#gridcontent').append(
                                '<tbody> <tr>' +
                                '<td style="width: 10%;vertical-align: middle;" class="text-center" rowspan="5">'+index+'</td>' +
                                '<td style="width: 25%;vertical-align: middle" class="text-left" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.active_warung')}}">'+result[index].active_user.detail_item+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+result[index].active_user.past_7_days+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+result[index].active_user.past_14_days+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+result[index].active_user.past_30_days+'</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="width: 25%;vertical-align: middle" class="text-left" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.total_sales')}}">'+result[index].total_sales.detail_item+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].total_sales.past_7_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].total_sales.past_14_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].total_sales.past_30_days)+'</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="width: 25%;vertical-align: middle" class="text-left">'+result[index].percentage_active.detail_item+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+percentageFormat(result[index].percentage_active.past_7_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+percentageFormat(result[index].percentage_active.past_14_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+percentageFormat(result[index].percentage_active.past_30_days)+'</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="width: 25%;vertical-align: middle" class="text-left" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.avg_sales_verified_warung')}}">'+result[index].avg_sales_by_verified.detail_item+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].avg_sales_by_verified.past_7_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].avg_sales_by_verified.past_14_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].avg_sales_by_verified.past_30_days)+'</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="width: 25%;vertical-align: middle" class="text-left" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.avg_sales_active_warung')}}">'+result[index].avg_sales_by_active.detail_item+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].avg_sales_by_active.past_7_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].avg_sales_by_active.past_14_days)+'</td>' +
                                '<td style="width: 15%;vertical-align: middle" class="text-right">'+formatPrice(result[index].avg_sales_by_active.past_30_days)+'</td>' +
                                '</tr></tbody>'
                        );
                	});
                }
            });
        }

        function removeField(tableId, index){
        	if(index == '' || index == null) index = 1; 
	    	var rowCount = $('#'+tableId+' tr').length;
	    	for(i = index; i < rowCount; i++){
	    		document.getElementById(tableId).deleteRow(1);
	    	}
	    }
        
        function formatPrice(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return 'Rp. '+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
        function percentageFormat(value) {
            var len = value.length;
            var val = value;
            if(value.substring(len-3, len) == '.00') val = value.substring(0, len-3)
            
            return val+'%';
        }
        
        function formatPriceTopPurchased(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
    </script>
    
    {{-- Date Range Picker --}}
    <script type="text/javascript">
    	var transactionDateRange = '';
        var startDate = moment().subtract(7, 'days').format('YYYY-MM-DD');
        var endDate = moment().subtract(1, 'days').format('YYYY-MM-DD');

        var startDateToShow = moment().subtract(6, 'days').format('DD/MM/YYYY');
        var endDateToShow = moment().subtract(1, 'days').format('DD/MM/YYYY');
        transactionDateRange = startDate + ',' + endDate;

        jQuery(document).ready(function ($) {
            dateRangeTransaction();

            $('#daterange-transaction').val(startDateToShow + ' - ' + endDateToShow);

        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange-transaction');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate	: startDate,
                endDate		: endDate,
                minDate		: new Date(2018, 6 - 1, 01),
                maxDate		: moment().subtract(1, 'days').format('YYYY-MM-DD'),
                ranges   	: {
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(7, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(30, 'days'), moment()],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }
    </script>
@stop