@extends('layout.main') @section('css') {{-- select 2 --}}
<link rel="stylesheet"
	href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
{{-- Date Range picker --}}
<link rel="stylesheet"
	href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet"
	href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet"
	href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">

<meta name="viewport" content="width=device-width, initial-scale=1">
@stop @section('title') Executive Summary @endsection

@section('pageTitle') Map Location from All User (Agent/PopWarung)
@endsection @section('pageDesc') @endsection @section('content')
<style>
.header-title-custom {
	background-color: #D3D3D3;
	border-radius: 15px;
	padding-left: 15px;
	padding-bottom: 2px;
	height: 30px;
}

#maps {
	border: 1px solid red;
	width: 100%;
	height: 700px;
}
</style>
<section class="content">
	{{-- Table --}}
	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-filter">
				<div class="box-body border-radius-none">
					<form id="form-transaction">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-12">
								<div class="row header-filter">

									<div class="col-md-3" style="display: none;">
										<div class="form-group"
											style="margin-right: 0px; margin-top: 15px;">
											<select class="form-control select2 select2-locker"
												name="locker" id="locker"></select>
										</div>
									</div>

									<div class="col-md-3" style="width: 20%">
										<div class="form-group"
											style="margin-right: 0px; margin-top: 15px;">
											<label>Select Type &nbsp;</label>
											<select class="form-control select2 select2-user-type"
												name="userType" id="userType" style="width: 55%;">
											</select>
										</div>
									</div>

									<div class="col-md-3" style="width: 20%">
										<div class="form-group"
											style="margin-right: 0px; margin-top: 15px;">
											<label>User Status &nbsp;</label>
											<select class="form-control select2 select2-status"
												name="status" id="status" style="width: 55%;">
											</select>
										</div>
									</div>

									<div class="col-md-3" style="width: 20%">
										<div class="form-group"
											style="margin-right: 0px; margin-top: 15px">
											<label>Locker Status &nbsp;</label>
											<select class="form-control select2 select2-locker-status"
												name="locker_status" id="locker_status" style="width: 45%;">
											</select>
										</div>
									</div>

									<div class="col-md-3" style="width: 30%">
										<div class="form-group form-inline">
											<div class="row"
												style="margin-right: 0px; margin-top: 15px; padding-left: 10px">
												<label>Register Date &nbsp;</label>
												<div class="input-group" style="width: 70%">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control daterange"
														id="daterange" name="daterange"
														placeholder="Select Date" readonly="readonly"
														style="background-color: white;">
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-3" style="width: 10%">
										<div class="form-group">
											<div style="margin-right: 0px; margin-top: 15px;">
												<a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</form>

					<br>

					<div class="header-title-custom header-summary-user" style="margin-bottom: 10px">
						<h4 class="m-b-20" style="padding-top: 5px">
							<strong>{{trans('executive_summary.map.user_summary')}}</strong>
						</h4>
					</div>
					<div class="row box-info box-summary-user">
						<div class="col-lg-2 col-sm-6" style="width: 20%">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-users"></i> <label class="counter"
										id="total_register_user"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.user_summary.total_reg')}}</p>
							</div>
						</div>

						<div class="col-lg-2 col-sm-6" style="width: 20%">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-users" style="color: #800000;"></i> <label
										class="counter" id="total_disable_user"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.user_summary.disable')}}</p>
							</div>
						</div>

						<div class="col-lg-2 col-sm-6" style="width: 20%">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-users" style="color: #4db8ff;"></i> <label
										class="counter" id="total_enable_user"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.user_summary.enable')}}</p>
							</div>
						</div>

						<div class="col-lg-2 col-sm-6" style="width: 20%">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-users" style="color: orange;"></i> <label
										class="counter" id="total_pending_user"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.user_summary.pending')}}</p>
							</div>
						</div>

						<div class="col-lg-2 col-sm-6" style="width: 20%">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-users" style="color: green;"></i> <label
										class="counter" id="total_verified_user"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.user_summary.verified')}}</p>
							</div>
						</div>
					</div>

					<br>

					<div class="header-title-custom header-summary-locker" style="margin-bottom: 10px">
						<h4 class="m-b-20" style="padding-top: 5px">
							<strong>{{trans('executive_summary.map.locker_summary')}}</strong>
						</h4>
					</div>
					<div class="row box-info box-summary-locker">
						<div class="col-md-4">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-dot-circle"></i> <label class="counter"
										id="total_lockers"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.map.total_locker')}}</p>
							</div>
						</div>

						<div class="col-md-4">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-dot-circle" style="color: #800000;"></i> <label
										class="counter" id="total_offline"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.map.offline_locker')}}</p>
							</div>
						</div>

						<div class="col-md-4">
							<div class="card-box text-center">
								<h3 class="m-t-0">
									<i class="fa fa-dot-circle" style="color: green;"></i> <label
										class="counter" id="total_online"></label>
								</h3>
								<p class="text-muted">{{trans('executive_summary.map.online_locker')}}</p>
							</div>
						</div>
					</div>

					<div id="maps"></div>

				</div>
			</div>
		</div>
	</div>
</section>
@endsection @section('js')
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script
	src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.freezeheader.js') }}"></script>
{{-- DateRange Picker --}}
<script
	src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/helper.js') }}"></script>

{{-- NUMERATOR --}}
<script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>
<script
	src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
<script
	src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

{{-- Map --}}
<script type="text/javascript" src="{{ asset('js/jquery.gmap.js') }}"></script>
<script type="text/javascript">
	var map;
    var locationMap = [];
	var markers = Array();
	var marker;
    
    function initMap() {

        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 11,
          // center: {lat: -3.8752905, lng: 117.2135979}
          center : {lat:-6.2487563,lng:106.8301447}
        });

        var infoWin = new google.maps.InfoWindow();
        var markers = locationMap.map(function(location, i) {
            
            var html = '<div align="center">';
            if(location.item_type !== 'locker'){
                html += '<strong style="color:#FF6300;font-size:15px;">'+location.locker_name+'</strong><br>'+location.locker_id+'<br>';
            	html += '<a onclick=$(this).agentDetail("'+location.locker_id+'")><button class="btn btn-xs btn-primary">Detail</button></a>';
            } else {
				html += '<strong style="color:#FF6300;font-size:15px;">'+location.locker_name+'</strong>';
            }
            html += '</div>';

            var marker = new google.maps.Marker({
                position	: {lat: parseFloat(location.latitude), lng:parseFloat(location.longitude) },
                icon 		: {
                                url: '{{ asset('img/map/map-marker_') }}'+location.icon_type+'.png',
                                // This marker is 20 pixels wide by 32 pixels high.
                                size: new google.maps.Size(30, 49),
                                // The origin for this image is (0, 0).
                                origin: new google.maps.Point(0, 0),
                                // The anchor for this image is the base of the flagpole at (0, 32).
                                anchor: new google.maps.Point(19, 34)
                        	  },
                map			: map,
                title 		: location.locker_name
            });
//             google.maps.event.addListener(marker, 'click', function(evt) {
//                 infoWin.setContent(html);
//                 infoWin.open(map, marker);
//             });
            google.maps.event.addListener(marker, 'mouseover', function(evt) {
                infoWin.setContent(html);
                infoWin.open(map, marker);
            })
            return marker;
        }); 
        // Add a marker clusterer to manage the markers.
        //var markerCluster = new MarkerClusterer(map, markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }
</script>

<script async defer
	src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&callback=initMap"></script>
<script
	src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

<script type="text/javascript">

    	var start = 0;
        var limit = 15;
        var userTypes = {!! json_encode($user_types) !!};
        var statusVerification = {!! json_encode($status_verification) !!};
        var lockerStatus = {!! json_encode($locker_status) !!};
        
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
        
        $(function(){

            $('.select2').select2();

            $(".select2-user-type").select2({
                  data: userTypes
            }).on('change', function(){
                if($('#userType').val() == 'locker'){
                	$("#status").attr('disabled', 'disabled');
                	$("#locker_status").removeAttr('disabled', 'disabled');

                	$(".header-summary-user").hide();
                	$(".box-summary-user").hide();
                	
                	$(".header-summary-locker").show();
                	$(".box-summary-locker").show();

                	$(".daterange").attr('disabled', 'disabled');
                	
                } else if($('#userType').val() == 'agent' || $('#userType').val() == 'warung') {
                	$("#status").removeAttr('disabled', 'disabled');
                	$("#locker_status").attr('disabled', 'disabled');

                	$(".header-summary-user").show();
                	$(".box-summary-user").show();
                	
                	$(".header-summary-locker").hide();
                	$(".box-summary-locker").hide();

                	$(".daterange").removeAttr('disabled', 'disabled');
                } else {
                	$("#status").removeAttr('disabled', 'disabled');
                	$("#locker_status").removeAttr('disabled', 'disabled');

                	$(".header-summary-user").show();
                	$(".box-summary-user").show();
                	
                	$(".header-summary-locker").show();
                	$(".box-summary-locker").show();
                	
                	$(".daterange").removeAttr('disabled', 'disabled');
                }
            });

            $(".select2-status").select2({
                  data: statusVerification
            });

            $(".select2-locker-status").select2({
                  data: lockerStatus
            }).on('change', function(){
                if($('#userType').val() == 'locker'){
                	$("#status").attr('disabled', 'disabled');
                } else {
                	$("#status").removeAttr('disabled', 'disabled');
                }
            });

            dashboard();
            
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                dashboard();
            });
            
            $.fn.agentDetail = function(lockerid) {        		
        		window.open(
                    ('{{ route('agent-detail', ['lockerid' => '']) }}/' + lockerid),
                    '_blank' 
              	);
            };
        });

        function dashboard(){
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-info', 'box-info');
            
            $.ajax({
                url: "{{ route('list-of-mitra') }}",
                data: {
                    _token                  : '{{ csrf_token() }}',
                    locker_id              	: $('#locker').val() ? $('#locker').val() : 'all',
                    daterange_transaction 	: transactionDateRange,
                    user_type             	: $("#userType").val() ? $("#userType").val() : 'all',
                    status	             	: $("#status").val() ? $("#status").val() : 'all',
                    locker_status	      	: $("#locker_status").val() ? $("#locker_status").val() : 'all',
                },
                type: 'GET',
                success: function(data){
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-info', 'box-info');
                	refresh_dasboard(data.payload);
                	
                    locationMap = data.payload.locationsMap;
                    initMap();
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            });
        }
        
        function refresh_dasboard(data){
            $('#total_register_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.total_register_user 
            });
            
            $('#total_disable_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.total_disable_user 
            });
            
            $('#total_enable_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.total_enable_user 
            });
            
            $('#total_pending_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.total_pending_user 
            });
            
            $('#total_verified_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.total_verified_user 
            });
            
            $('#total_lockers').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.locker_summary.total_locker 
            });
            
            $('#total_offline').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.locker_summary.total_offline 
            });
            
            $('#total_online').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.locker_summary.total_online 
            });
          }
    </script>

{{-- Date Range Picker --}}
<script type="text/javascript">
    	var transactionDateRange = '';

        var startDateToShow = moment().subtract(6, 'days').format('DD/MM/YYYY');
        var endDateToShow = moment().subtract(1, 'days').format('DD/MM/YYYY');

        jQuery(document).ready(function ($) {
            dateRangeTransaction();
        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange');

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                ranges   	: {
                	'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }
    </script>
@stop
