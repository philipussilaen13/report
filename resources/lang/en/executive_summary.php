<?php

return [

    /*
    |--------------------------------------------------------------------------
    | All Related to i18n of the Global Menu
    |--------------------------------------------------------------------------
    |
    */

    'main' => [
        
    ],
    /*
    |--------------------------------------------------------------------------
    | Topup & Digital
    |--------------------------------------------------------------------------
    */
    'mitra_registration' => [
        'title'                 => 'Mitra Registration',
        'total_registered'      => 'Total Registered',
        'total_verified'        => 'Total Verified',
        'total_active_user'     => 'Total Active User',
        'total_inactive_user'   => 'Total Inactive User',
    ],
    
    'user_summary' => [
        'warung'        => 'Warung User Summary',
        'agent'         => 'Agent User Summary',
        'total_reg'     => 'Total Registration',
        'disable'       => 'Disable',
        'enable'        => 'Enable',
        'pending'       => 'Pending',
        'verified'      => 'Verified',
    ],
    
    'topup_deposit' => [
        'title'                 => 'Topup Deposit',
        'gross_amount'          => 'Gross Amount',
        'nett_amount'           => 'Nett Amount',
        'successfull_topup'     => 'Successful Topup Attempt',
        'unique_user'           => 'Unique User',
    ],
    
    'digital_product' => [
        'title'                 => 'Digital Product - Admin Fee, Pulsa, Paket Data, BJPS, PDAM, PLN (Prepaid/Postpaid), Telkom',
        'gross_amount'          => 'Gross Amount',
        'nett_amount'           => 'Nett Amount',
        'gross_order'           => 'Gross Order (number of transaction)',
        'nett_order'            => 'Unique User (number of transaction)',
        'gross_unique_user'     => 'Unique User (Gross Transaction)',
        'nett_unique_user'      => 'Unique User (Nett Transaction)',
    ],
    
    'performing_warung' => [
        'title'                 => 'Top 10 Performing PopWarung',
        'locker_id'             => 'Locker ID',
        'warung_name'           => 'Warung Name',
        'digital_product'       => 'Digital Product',
        'non_digital_product'   => 'Non Digital Product',
        'total_transaction'     => 'Total Transaction',
    ],
    
    'performing_agent' => [
        'title'                 => 'Top 10 Performing Agent',
        'locker_id'             => 'Locker ID',
        'agent_name'            => 'Agent Name',
        'digital_product'       => 'Digital Product',
        'non_digital_product'   => 'Non Digital Product',
        'total_transaction'     => 'Total Transaction',
    ],
    
    'alias_column' => [
        'total_registered'      => 'total_registered_user',
        'total_verified'        => 'total_verified_user',
        'total_active_user'     => 'total_active_user',
        'total_inactive_user'   => 'total_inactive_user',
        'gross_amount'          => 'gross_amount',
        'nett_amount'           => 'nett_amount',
        'successfull_topup'     => 'successfull_topup',
        'unique_user'           => 'unique_user',
    ],
    
    
    /*
     |--------------------------------------------------------------------------
     | Tooltip
     |--------------------------------------------------------------------------
     */
    
    'tooltip' => [
        
        /*
         |--------------------------------------------------------------------------
         | Topup & Digital
         |--------------------------------------------------------------------------
         */
        
        'total_registered'      => 'Total seluruh user yang melakukan registrasi',
        'total_verified'        => 'Total seluruh user yang berstatus verified',
        'total_active_user'     => 'Total user dengan status verified dan melakukan transaksi apapun minimal 1x dalam rentang tanggal yang dipilih.',
        'total_inactive_user'   => 'Total user dengan status verified tetapi belum melakukan transaksi apapun dalam rentang tanggal yang dipilih.',
        'gross_amount'          => 'Total nominal topup yang di create (baik sukses, gagal, atau lewat masa bayar)',
        'nett_amount'           => 'Total nominal topup yang di create dan berhasil',
        'successfull_topup'     => 'Jumlah/total transaksi topup yang berhasil',
        'unique_user'           => 'Jumlah unique user/mitra yang melakukan topup',
        
        'digital_product' => [
            'gross_amount'          => 'Total nilai penjualan yang dilakukan (termasuk didalamnya transaksi yang berhasil maupun gagal)',
            'nett_amount'           => 'Total nilai penjualan yang sukses dilakukan',
            'gross_order'           => 'Total jumlah transaksi yang dilakukan (termasuk didalamnya transaksi yang berhasil maupun gagal)',
            'nett_order'            => 'Total jumlah transaksi yang sukses dilakukan',
            'gross_unique_user'     => 'Jumlah unique user yang melakukan transaksi (termasuk didalamnya transaksi yang berhasil maupun gagal)',
            'nett_unique_user'      => 'Jumlah unique user yang melakukan transaksi digital product yang berhasil dilakukan',
        ],
        
        
        /*
         |--------------------------------------------------------------------------
         | Sales Stock
         |--------------------------------------------------------------------------
         */
        
        'product_stock'   => [
            'total_stock_transaction'   => 'Total nilai transaksi belanja stock yang dilakukan oleh warung dan agent',
            'deposit_sales'             => 'Total nilai transaksi belanja stock yang menggunakan metode pembayaran deposit',
            'cod_sales'                 => 'Total nilai transaksi belanja stock yang menggunakan metode pembayaran COD',
        ],
        
        'verified_user' => [
            'avg_sales_per_store'           => 'Total nilai transaksi :type dibagi jumlah seluruh verified user',
            'avg_sales_per_store_per_day'   => '(Total :type dibagi jumlah seluruh verified user) dibagi jumlah hari pada date range'
        ],
        
        'active_user' => [
            'avg_sales_per_store'           => 'Total nilai transaksi :type dibagi total active user (verified user)',
            'avg_sales_per_store_per_day'   => '(Total :type dibagi total active user) dibagi jumlah hari pada date range (verified user)'
        ],
        
        'purchase_popagent' => 'Total transaksi digital (tidak termasuk topup, refund dan komisi) dan non digital (belanja stok dengan deposit+COD) yang sukses dilakukan',
        
        /*
         |--------------------------------------------------------------------------
         | Point of Sales
         |--------------------------------------------------------------------------
         */
        'penjualan_cepat'   => 'Total nilai transaksi yang dibuat oleh PopWarung melalui menu Penjualan Cepat (SKU tidak terdaftar)',
        'penjualan'         => 'Total nilai transaksi yang dibuat oleh PopWarung melalui menu Penjualan (SKU terdaftar)',
        'total_penjualan'   => 'Total nilai transaksi penjualan baik deri menu Penjualan maupun Penjualan Cepat',
        'by_active_user'    => 'Total active user yang menggunakan fitur penjualan dan penjualan cepat',
        
        /*
         |--------------------------------------------------------------------------
         | Outline / Stock Purchase Summary
         |--------------------------------------------------------------------------
         */
        'stock_purchase_summary'    => 'Active Warung = No. of Warung Purchased from us within the time period <br> <br>
                    
                    Total Sales = Total amount of Warung Purchase <br> <br>
                    
                    Average Sales/Total Verified Warung = Average sales Warung (stock purchase) within the time period divided by No. of verified Warung from beginning  <br> <br>
                    
                    Average sales/Active Warung = Average Sales Warung (stock purchase) divided by No. of verified Warung (unique) that purchased from us - both within the time period',
        
        
    ],
    
    /*
     |--------------------------------------------------------------------------
     | Sales Stock
     |--------------------------------------------------------------------------
     */
    
    'product_stock' => [
        'title'                     => 'Product Stock Purchase',
        'total_stock_transaction'   => 'Total Sales Transaction',
        'deposit_sales'             => 'Deposit Sales',
        'cod_sales'                 => 'COD Sales',
    ],
    
    'verified_user' => [
        'title'                         => 'Verified User Stock Purchase',
        'avg_sales_per_store'           => 'Avg. Sales per Store',
        'avg_sales_per_store_per_day'   => 'Avg. Sales per Store per Day'
    ],
    
    'active_user' => [
        'title'                         => 'Active User Stock Purchase',
        'avg_sales_per_store'           => 'Avg. Sales per Store',
        'avg_sales_per_store_per_day'   => 'Avg. Sales per Store per Day'
    ],
    
    'purchase_popwarung' => [
        'title'         => 'Top 20 Most Purchased SKU - PopWarung',
        'sku'           => 'Barcode',
        'product_name'  => 'Product Name',
        'qty'           => 'Qty',
        'amount'        => 'Total Selling Amount',
    ],
    
    'purchase_popagent' => [
        'title'         => 'Top 20 Most Purchased SKU - PopAgent',
        'locker_id'     => 'Locker ID',
        'sku'           => 'SKU',
        'product_name'  => 'Product Name',
        'locker_name'   => 'Warung Name',
        'qty'           => 'Qty',
        'amount'        => 'Total Selling Amount',
        'total_amount'  => 'Amount',
    ],
    
    /*
     |--------------------------------------------------------------------------
     | Point of Sales
     |--------------------------------------------------------------------------
     */
    
    'pos' => [
        'warung'        => 'Warung',
        'product_sales' => 'Product Sales',
        'product_sales_box' => [
            'penjualan'         => 'From "Penjualan" Menu',
            'penjualan_cepat'   => 'From "Penjualan Cepat" Menu',
            'total'             => 'Total Product Sales',
            'active_user'       => 'Total Active User',
        ],
        'verified_user' => 'Product Sales from All Verified User',
        'active_user'   => 'Product Sales from Active User'
    ],
    
    'best_selling'  => [
        'title'         => 'Top 20 Most Sold Products from "Penjualan" Menu',
        'sku'           => 'SKU',
        'product_name'  => 'Product Name',
        'qty'           => 'Qty',
        'amount'        => 'Amount',
    ],
    
    'top_product_sales'  => [
        'title'         => 'Top 10 PopWarung based on Total Selling Amount',
        'sku'           => 'SKU',
        'product_name'  => 'Product Name',
        'qty'           => 'Qty',
        'amount'        => 'Amount',
    ],
    
    'summary'   => [
        'date'          => 'Date',
        'detail_item'   => 'Matrix Report',
        '7_days'        => 'Past 7 days',
        '14_days'       => 'Past 14 days',
        '30_days'       => 'Past 30 days'
    ],
    
    /*
     |--------------------------------------------------------------------------
     | Map Location
     |--------------------------------------------------------------------------
     */
    
    'map'   => [
        'user_summary'      => 'User Summary',
        'locker_summary'    => 'Locker Summary',
        'total_locker'      => 'Total Locker',
        'offline_locker'    => 'Offline',
        'online_locker'     => 'Online',
    ],
];
