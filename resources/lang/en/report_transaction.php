<?php

return [

    'page' => [
        'configuration'             => 'Konfigurasi',
        'id_warung'                 => 'Pilih Agent/Warung',
        'receiver'                  => 'Email Penerima',
        'place_holder_receiver'     => 'Input email penerima, gunakan tanda koma (,) sebagai pemisah jika penerima lebih dari satu',
        'period'                    => 'Jangka Waktu',
        'date_send'                 => 'Pilih Tanggal dan Waktu Pengiriman Report',
        'place_holder_date_send'    => 'Tanggal pengiriman',
        'hour_send'                 => 'Pilih Jam Pengiriman Peport',
    ],
    
    'filter' => [
        'report_type'               => 'Tipe Report',
        'id_warung'                 => 'Agent/Warung',
        'status'                    => 'Status',
    ],
];