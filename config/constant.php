<?php
/**
 * Created by PhpStorm.
 * User: Hendy
 * Date: 28-Aug-18
 * Time: 3:13 PM
 */

return [
    'popwarung' => [
        'api_url' => env('POPWARUNG_API_URL'),
    ],
    'outline_report'  => [
        'time_implemented'  => env('TIME_IMPLEMENTED', '08:00'),
    ],
    'agent_api'  => [
        'url'  => env('AGENT_URL'),
    ],
];