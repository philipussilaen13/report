<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_response', function (Blueprint $table) {
            $table->increments('id');
            $table->string('api_url', 255)->nullable();
            $table->text('api_send_data')->nullable();
            $table->text('api_response')->nullable();
            $table->dateTime('response_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_response');
    }
}
