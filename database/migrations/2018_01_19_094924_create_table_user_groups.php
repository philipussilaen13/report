<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableUserGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->timestamps();
            $table->softDeletes();
            $table->timestamp('server_timestamp')->nullable();
        });

        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'popexpress_api_key')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->string('username', 255)->after('group_id');
                    $table->string('popexpress_api_key', 10)->nullable()->after('username');
                    $table->timestamp('server_timestamp')->nullable()->after('popexpress_api_key');
                });
            }
        }

        if (Schema::hasTable('groups')) {
            if (!Schema::hasColumn('groups', 'server_timestamp')) {
                Schema::table('groups', function (Blueprint $table) {
                    $table->timestamp('server_timestamp')->nullable()->after('deleted_at');
                });
            }
        }

        $popexpressGroups = ['popexpress_admin','popexpress_hr','popexpress_branch_head','popexpress_finance','popexpress_courier_head','popexpress_courier','popexpress_operation_head','popexpress_customer_service','popexpress_sales'];
        foreach ($popexpressGroups as $popexpressGroup) {
            $check = DB::table('groups')->where('name', $popexpressGroup)->count();
            if(empty($check)) {
                DB::table('groups')->insert(['name' => $popexpressGroup, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')]);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_groups');

        if (Schema::hasTable('users'))
        {
            if (Schema::hasColumn('users', 'popexpress_api_key')) {
                Schema::table('users', function (Blueprint $table) {
                    if (Schema::hasColumn('users', 'popexpress_api_key')) {
                        $table->dropColumn('popexpress_api_key');
                    }
                });
            }

            if (Schema::hasColumn('users', 'server_timestamp')) {
                Schema::table('users', function (Blueprint $table) {
                    if (Schema::hasColumn('users', 'server_timestamp')) {
                        $table->dropColumn('server_timestamp');
                    }
                });
            }
        }

        if (Schema::hasTable('groups'))
        {
            if (Schema::hasColumn('groups', 'server_timestamp')) {
                Schema::table('groups', function (Blueprint $table) {
                    if (Schema::hasColumn('groups', 'server_timestamp')) {
                        $table->dropColumn('server_timestamp');
                    }
                });
            }
        }

    }
}
