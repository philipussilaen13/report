<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function () {
    Route::get('login', '\App\Http\Controllers\Auth\LoginController@getLogin');
    Route::post('login', '\App\Http\Controllers\Auth\LoginController@postLogin');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'LandingController@getLanding');
    /*Ajax Landing*/
    Route::group(['prefix' => 'ajax'], function () {
        Route::any('getLocker', 'LandingController@ajaxGetLocker');
        Route::any('getParcel', 'LandingController@ajaxGetParcel');
        Route::any('getTransaction', 'LandingController@ajaxGetTransaction');
        Route::any('getDelivery', 'LandingController@ajaxGetDelivery');
        Route::get('download', 'Agent\AgentController@download');
    });

    /*Privilege Management*/
    Route::group(['prefix' => 'privileges'], function () {
        Route::get('group', 'PrivilegeController@getGroup');
        Route::post('group', 'PrivilegeController@postGroup');
        Route::get('module', 'PrivilegeController@getModule');
        Route::post('module', 'PrivilegeController@postModule');
        Route::get('privilege', 'PrivilegeController@getPrivilege');
        Route::post('privilege', 'PrivilegeController@postPrivilege');
        Route::get('user', 'PrivilegeController@getUser');
        Route::post('user', 'PrivilegeController@postUser');

        /*------------------V2------------------*/
        Route::get('privilege/landing', 'PrivilegeController@getLanding');
        Route::get('privilege/v2', 'PrivilegeController@getPrivilegesGroupList');
    });

    /*Log Access*/
    Route::group(['prefix' => 'log'], function () {
        Route::get('agent', 'LogAccessController@getAgentAccess');
    });

    /*PopBox Dashboard*/
    Route::group(['prefix' => 'popbox'], function () {
        Route::get('/', 'Popbox\LockerController@landing');
        // ===================================== Locker =======================================
        Route::group(['prefix' => 'locker'], function () {
            Route::get('performance', 'Popbox\LockerController@getLockerPerformance');
            Route::get('ajax/lockerPerformance', 'Popbox\LockerController@ajaxLockerPerformance');
            Route::get('location', 'Popbox\LockerController@getLocation');
            Route::group(['prefix' => 'delivery'], function () {
                Route::get('/', 'Popbox\LockerController@getDeliveryActivity');
                Route::get('detail', 'Popbox\LockerController@getDeliveryDetailActivity');
                Route::post('postChangePhone', 'Popbox\LockerController@postChangePhone');
                Route::post('postResendSMS', 'Popbox\LockerController@postResendSMS');
                Route::post('postChangeOverdue', 'Popbox\LockerController@postChangeOverdue');
                Route::post('postOpenDoor', 'Popbox\LockerController@postOpenDoor');

                Route::get('users', 'Popbox\LockerController@getUsers');
                Route::post('users', 'Popbox\LockerController@postUsers');

                Route::get('pickup', 'Popbox\LockerController@getPickupList');
                Route::get('pickup/downloadExample', 'Popbox\LockerController@downloadExample');
                Route::post('pickup', 'Popbox\LockerController@postExpress');
                Route::post('pickup/excel', 'Popbox\LockerController@postExcelExpress');
            });
        });
        // ======================================================================================
        // ===================================== Sepulsa =======================================
        Route::group(['prefix' => 'sepulsa'], function () {
            Route::get('product', 'Popbox\SepulsaController@getProduct');
            Route::post('product', 'Popbox\SepulsaController@postSepulsaProduct');
            Route::get('updateStatus', 'Popbox\SepulsaController@getSepulsaUpdateStatus');
            Route::post('closeProduct', 'Popbox\SepulsaController@postCloseProduct');
            Route::get('location', 'Popbox\SepulsaController@getSepulsaLocation');
            Route::post('location', 'Popbox\SepulsaController@postSepulsaLocation');
            Route::get('updateLocation', 'Popbox\SepulsaController@postUpdateLocation');
            Route::get('transactions', 'Popbox\SepulsaController@getSepulsaTransaction');
            Route::post('transactions', 'Popbox\SepulsaController@postExportSepulsaTransaction');

            Route::group(['prefix' => 'ajax'], function () {
                Route::post('getSepulsaProductDownload', 'Popbox\SepulsaController@getSepulsaProductDownload');
            });
        });
        // ======================================================================================
        // ===================================== PopShop =======================================
        Route::group(['prefix' => 'popshop'], function () {
            Route::get('product', 'Popbox\PopshopController@getProduct');
            Route::group(['prefix' => 'ajax'], function () {
                Route::get('getProductCategories', 'Popbox\PopshopController@getProductCategories');
                Route::post('getProductList', 'Popbox\PopshopController@getProductList');
                Route::post('getDownloadProductList', 'Popbox\PopshopController@getDownloadProductList');
            });
        });
        // ======================================================================================
		// ===================================== Pepito =======================================
        Route::group(['prefix' => 'pepito'], function () {
            Route::get('transactions', 'Popbox\PepitoController@getTransaction')->name('transaction');
            Route::post('transactions', 'Popbox\PepitoController@postExportSepulsaTransaction');
            
        });
    });

    /*PopSend Dashboard*/
    Route::group(['prefix' => 'popapps'], function () {

        Route::group(['prefix' => 'popsafe'], function () {
            Route::get('/','PopApps\PopsafeController@getLanding');
            Route::get('getDetailPage/{invoice_code}','PopApps\PopsafeController@getDetailPage');
            Route::group(['prefix' => 'ajax'], function () {
                Route::post('getLiveStatus', 'PopApps\PopsafeController@getAjaxLiveStatus');
                Route::get('getAllUser', 'PopApps\PopsafeController@getAllUser');
                Route::post('getAjaxFilterPopsafe', 'PopApps\PopsafeController@getAjaxFilterPopsafe');
                Route::post('getDetailPopsafe','PopApps\PopsafeController@getDetailPopsafe');
                Route::post('getDetailHistoriesPopsafe','PopApps\PopsafeController@getDetailHistoriesPopsafe');
                Route::post('getDetailTransactionsPopsafe','PopApps\PopsafeController@getDetailTransactionsPopsafe');
                Route::post('getAjaxDownloadPopsafe','PopApps\PopsafeController@getAjaxDownloadPopsafe');                
                Route::get('download', 'PopApps\PopsafeController@download');
            });
        });

        Route::group(['prefix' => 'transaction'], function () {
            Route::get('/','PopApps\TransactionController@getTransactionList');
            Route::get('v2','PopApps\TransactionController@getLanding');
            Route::get('detail/{invoiceId}','PopApps\TransactionController@getTransactionDetail');
            Route::post('refund','PopApps\TransactionController@postRefundTransaction');
            Route::post('rePushDelivery','PopApps\TransactionController@postRePushDelivery');
            Route::post('cancelrefund','PopApps\TransactionController@postCancelRefundTransaction');
            Route::get('extendPopsafe/{transactionId}','PopApps\TransactionController@extendPopsafe');
            Route::group(['prefix' => 'ajax'], function () {
                Route::any('getDefaultData','PopApps\TransactionController@getDefaultData');
                Route::post('getFilterData','PopApps\TransactionController@getFilterData');
                Route::post('getAjaxDownloadTransaction','PopApps\TransactionController@getAjaxDownloadTransaction');
                Route::get('download', 'PopApps\PopsafeController@download');
            });
        });
        Route::group(['prefix'=>'user'],function (){
            Route::get('list','PopApps\UserController@getList');
            Route::get('list/v2','PopApps\UserController@getLandingV2');
            Route::get('detail/{memberId}','PopApps\UserController@getUserDetail');
            Route::get('detailId/{userId}','PopApps\UserController@getUserDetailByID');
            Route::get('getAjaxUser','PopApps\UserController@getAjaxUser');
            Route::group(['prefix' => 'ajax'], function () {
                Route::post('getListV2', 'PopApps\UserController@getListV2');
                Route::post('getAjaxDownloadList','PopApps\UserController@getAjaxDownloadList');
                Route::get('download', 'PopApps\UserController@download');
            });
        });
        Route::group(['prefix'=>'finance'],function (){
            Route::post('ajaxUser', 'PopApps\FinanceController@ajaxUserDetail');
            Route::get('credit','PopApps\FinanceController@getCreditManual');
            Route::post('credit','PopApps\FinanceController@postCreditManual') ;
            Route::get('debit','PopApps\FinanceController@getDebitManual');
            Route::post('debit','PopApps\FinanceController@postDebitManual') ;
        });
        Route::group(['prefix'=>'marketing'],function (){
            Route::get('ruleHelper/{type}','PopApps\MarketingController@getHelperRule');
            Route::group(['prefix'=>'campaign'],function () {
                Route::get('/','PopApps\MarketingController@getCampaign');
                Route::get('add','PopApps\MarketingController@getAddCampaign');
                Route::get('edit/{id}','PopApps\MarketingController@getEditCampaign');
                Route::get('ajaxAddRule','PopApps\MarketingController@getAjaxRule');
                Route::get('ajaxDeleteRule','PopApps\MarketingController@deleteAjaxRule');
                Route::post('add','PopApps\MarketingController@postAddCampaign');
                Route::get('addVoucher/{campaignId}','PopApps\MarketingController@getAddVoucher');
                Route::post('addVoucher/{campaignId}','PopApps\MarketingController@postAddVoucher');
            });
            Route::group(['prefix' => 'referral'], function () {
                Route::get('/', 'PopApps\MarketingController@getReferral');
                Route::post('/', 'PopApps\MarketingController@postReferral');
                Route::get('transaction', 'PopApps\MarketingController@getReferralTransaction');
            });
        });
        Route::group(['prefix' => 'freeGift'],function (){
           Route::get('list','PopApps\FreeGiftController@getListUsage');
           Route::get('transaction','PopApps\FreeGiftController@getListTransaction');
           Route::post('transaction', 'PopApps\FreeGiftController@updatestatusTransaction');
           Route::get('category/{sort?}','PopApps\FreeGiftController@getListCategory');
           
           Route::post('category', 'PopApps\FreeGiftController@storeCategory');
           Route::post('question', 'PopApps\FreeGiftController@storeQuestion');
           Route::get('detail/{memberId}','PopApps\UserController@getUserDetail') ;
           Route::get('transactionQuestionDetail/{categoryId}/{sort?}','PopApps\FreeGiftController@listQuestion');
           Route::get('categoryDetail/{categoryId}','PopApps\FreeGiftController@getCategoryDetail');
           Route::get('update/{categoryId}','PopApps\FreeGiftController@getUpdateCategory');

           Route::any('landing', 'PopApps\FreeGiftController@getLanding');
            Route::group(['prefix' => 'ajax'], function () {
                Route::post('getFreeGiftListUser', 'PopApps\FreeGiftController@getFreeGiftListUser');
                Route::get('download', 'PopApps\PopsafeController@download');
            });
        });
        Route::group(['prefix' => 'delivery'],function (){
            Route::get('/','PopApps\DeliveryController@getDeliveryList');
            Route::post('/','PopApps\DeliveryController@DownloadExcel');
        });
        Route::group(['prefix' => 'article'],function (){
            Route::get('list','PopApps\ArticleController@getListArticle');
            Route::get('create','PopApps\ArticleController@createNewArticle');
            Route::post('save','PopApps\ArticleController@saveNewArticle');
            Route::get('edit/{articleId}','PopApps\ArticleController@editArticle');
            Route::post('editExisting/{articleId}','PopApps\ArticleController@editArticleById');
            Route::get('changeStatus/{articleId}/{status}','PopApps\ArticleController@changeStatusArticle');
            Route::any('notification','PopApps\ArticleController@pushNotification');
        });

        Route::group(['prefix' => 'notification'],function (){
            Route::get('article/{articleId}','PopApps\ArticleController@pushArticle');
            Route::post('send','NotificationController@sendPushNotification');            
        });
    });

    /*PopShop Dashboard*/
    Route::group(['prefix' => 'popshop'], function () {
        Route::get('/', 'Popshop\LandingController@getLanding');
    });

    /*Agent Dashboard*/
    Route::group(['prefix' => 'agent'], function () {
        // ===================================== Landing =======================================
        Route::get('/', 'Agent\LandingController@getLanding');
        Route::get('topsales', 'Agent\LandingController@getLanding2');
        Route::get('userinsight', 'Agent\LandingController@getUserInsight');
        Route::group(['prefix' => 'ajax'], function () {
            Route::get('getTransaction', 'Agent\LandingController@ajaxGetTransaction');
            Route::get('getMapFilter', 'Agent\LandingController@getMapFilter');
            Route::get('getProduct', 'Agent\LandingController@ajaxGetProduct');
            Route::get('getTopSellingOffline', 'Agent\LandingController@ajaxGetTopSellingOffline');
            Route::get('getTopSellingOnline', 'Agent\LandingController@ajaxGetTopSellingOnline');
            Route::get('getTopSellingAgent', 'Agent\LandingController@ajaxGetTopSellingAgent');
            Route::get('getTopChartSelling', 'Agent\LandingController@ajaxGetTopSellingAgent');
            Route::get('getFloatingMoney', 'Agent\LandingController@ajaxGetFloatingMoney');
            Route::get('getFilter/{type}/{time}', 'Agent\LandingController@ajaxGetFilter');
            Route::get('getInsightFilter/{type}/{time}', 'Agent\LandingController@getInsightAjaxFilter');
            Route::get('getInsightType', 'Agent\LandingController@getInsightAllType');
            Route::any('getAjaxAllType', 'Agent\AgentController@getAjaxAllType');
            Route::any('getAjaxAllTypeV2', 'Agent\AgentController@getAjaxAllTypeV2');
            Route::any('getAjaxFilterDropdownDataUser', 'Agent\AgentController@getAjaxFilterDropdownDataUser');
            Route::post('getAjaxUserDataDownload', 'Agent\AgentController@getAjaxUserDataDownload');
            Route::get('getAjaxAllUserDataDownload', 'Agent\AgentController@getAjaxAllUserDataDownload')->name('all-user-download-excel');
            Route::any('getAjaxDataStatusVerification', 'Agent\AgentController@getAjaxDataStatusVerification');
            Route::get('populate-user', 'Agent\AgentController@populateUser')->name('populate-user');

            // ======================================================================================
            // ===================================== Agent Transaction Ajax =====================================
            Route::any('getAjaxFilterDropdownDataTransaction', 'Agent\TransactionController@getAjaxFilterDropdownDataTransaction');
            Route::any('getAjaxFilterTransaction', 'Agent\TransactionController@getAjaxFilterTransaction');
            Route::get('getAjaxTransactionDownload', 'Agent\TransactionController@getAjaxTransactionDownload');
            Route::post('getAjaxTransactionDownloadPerformance', 'Agent\TransactionController@getAjaxPerformanceTransactionDownload');
            Route::post('getAjaxTransactionDownloadNonGroup', 'Agent\TransactionController@getAjaxNonGroupTransactionDownload');
            Route::post('getAjaxChangeAllDeliveryStatus', 'Agent\TransactionController@getAjaxChangeAllDeliveryStatus');

            Route::post('getDetailSales', 'Agent\AgentController@getDetailSales');

            // ======================================================================================
            // ===================================== Ajax Download =====================================
			Route::get('download', 'Agent\AgentController@download');
        });
        // ======================================================================================
        // ===================================== Agent List =====================================
        Route::group(['prefix' => 'list'], function () {
            Route::get('/', 'Agent\AgentController@getLanding');
            Route::get('userlist', 'Agent\AgentController@getLandingV2');
            Route::any('getAjaxGraph', 'Agent\AgentController@getAjaxGraph');
            Route::any('getAjaxSummary', 'Agent\AgentController@getAjaxSummary');
            Route::get('detail/{locker_id}', 'Agent\AgentController@getDashboardAgentDetail')->name('agent-detail');
            Route::post('export', 'Agent\AgentController@postExportExcel');
        });
        Route::post('activate', 'Agent\AgentController@postActivateAgent');
        Route::post('update', 'Agent\AgentController@postUpdateAgent');
        Route::post('activateExpress', 'Agent\AgentController@postActivateExpress');
        Route::post('changeStatus', 'Agent\AgentController@postChangeStatusAgent');
        Route::get('changeType/{locker_id}', 'Agent\AgentController@getChangeTypeAgent');
        // ========================================================================================
        // ==================================== Transaction =======================================
        Route::group(['prefix' => 'transaction'], function () {
            Route::get('/', 'Agent\TransactionController@getLanding');
            Route::get('list', 'Agent\TransactionController@getList');
            Route::get('list/v2', 'Agent\TransactionController@getListV2');
            Route::get('report','Agent\TransactionController@getTransactionReport');
            Route::post('export', 'Agent\TransactionController@postExport');
            Route::post('exportNonGroup','Agent\TransactionController@postExportNonGrouping');
            Route::get('getAjaxGraph', 'Agent\TransactionController@getAjaxGraph');
            Route::get('detail/{transactionRef}', 'Agent\TransactionController@getDetail');
            Route::post('refund', 'Agent\TransactionController@postRefund');
            Route::post('confirmBankTransfer', 'Agent\TransactionController@postConfirm');
            Route::post('repush', 'Agent\TransactionController@postRepush');
            Route::any('exportAgentPerformance', 'Agent\TransactionController@exportAgentPerformance');
            Route::post('sendReceipt', 'Agent\TransactionController@postReSendReceipt');
            Route::post('addRemarks','Agent\TransactionController@postAddRemarks');
            Route::post('changeDeliveryStatus','Agent\TransactionController@postChangeDeliveryStatus');			
            Route::group(['prefix' => 'cod'], function () {
                Route::get('/', 'Agent\CodController@index')->name('cod');
                Route::get('/getList', 'Agent\CodController@list')->name('transaction-list');
                Route::get('/detail/{id}/{lockerId}', 'Agent\CodController@show')->name('detail-cod-transaction');
                Route::get('/detail/get-items', 'Agent\CodController@detail');
                Route::post('/detail/update-item-status', 'Agent\CodController@update')->name('update-item-status');
            });
            Route::group(['prefix' => 'ajax'], function () {
                Route::get('/get-transaction-warung/{transactionRef}', 'Agent\TransactionController@getAjaxTrxWarung')->name('get-transaction-warung');
            });            
        });
        // =======================================================================================
        // ======================================= Finance =======================================
        Route::group(['prefix' => 'finance'], function () {
            Route::get('credit', 'Agent\FinanceController@getCreditManual');
            Route::post('credit', 'Agent\FinanceController@postCreditManual');
            Route::get('debit', 'Agent\FinanceController@getDebitManual');
            Route::post('debit', 'Agent\FinanceController@postDebitManual');
            Route::post('ajaxAgent', 'Agent\FinanceController@ajaxAgentDetail');
            // report
            Route::any('member', 'Agent\FinanceController@MemberSummary');
            Route::any('member/v2', 'Agent\FinanceController@MemberSummaryV2');
            Route::any('pulsa', 'Agent\FinanceController@PulsaSummary');
            Route::any('payment', 'Agent\FinanceController@paymentSummary');
            Route::any('popshop', 'Agent\FinanceController@PopshopSummary');
            // sales feature
            Route::get('salesPending', 'Agent\FinanceController@getSalesPending');
            Route::post('salesPending', 'Agent\FinanceController@postSalesPending');
            Route::post('confirmSalesPending', 'Agent\FinanceController@postConfirmSalesPending');
            Route::post('ajaxGetAgent', 'Agent\FinanceController@postAjaxGetAgent');
        });
        // ====================================== Marketing ======================================
        Route::group(['prefix' => 'marketing'], function () {
            Route::group(['prefix'=>'approval'],function (){
                Route::get('/','Agent\MarketingController@getPendingApproval');
                Route::get('/{type}/{id}','Agent\MarketingController@getPendingApprovalDetail');
                Route::post('/{type}/{id}','Agent\MarketingController@postPendingApprovalDetail');
            });
            Route::group(['prefix'=>'campaign'],function (){
                Route::get('/','Agent\MarketingController@getCampaign');
                Route::get('add','Agent\MarketingController@getAddCampaign');
                Route::get('edit/{id}','Agent\MarketingController@getEditCampaign');
                Route::get('ajaxAddRule','Agent\MarketingController@getAjaxRule');
                Route::get('ajaxDeleteRule','Agent\MarketingController@deleteAjaxRule');
                Route::post('add','Agent\MarketingController@postAddCampaign');
                Route::get('addVoucher/{campaignId}','Agent\MarketingController@getAddVoucher');
                Route::post('addVoucher/{campaignId}','Agent\MarketingController@postAddVoucher');
            });
            Route::group(['prefix' => 'commission'], function () {
                Route::get('/', 'Agent\MarketingController@getCommission');
                Route::post('/', 'Agent\MarketingController@postCommission');
                Route::get('rule/{id}', 'Agent\MarketingController@getCommissionRule');
                Route::post('rule/{id}', 'Agent\MarketingController@postCommissionRule');
                Route::any('ajaxDeleteRule/{id}', 'Agent\MarketingController@deleteCommissionRule');
                Route::any('ajaxHelper/{type}', 'Agent\MarketingController@getCommissionHelper');
            });
            Route::group(['prefix' => 'referral'], function () {
                Route::get('/', 'Agent\MarketingController@getReferral');
                Route::post('/', 'Agent\MarketingController@postReferral');
                Route::get('transaction', 'Agent\MarketingController@getReferralTransaction');
            });
            Route::group(['prefix' => 'notification'], function () {
                Route::get('/', 'Agent\MarketingController@getNotification');
                Route::post('/', 'Agent\MarketingController@postNotification');
            });
            Route::group(['prefix' => 'article'], function () {
                Route::get('/', 'Agent\MarketingController@getArticle');
                Route::post('/', 'Agent\MarketingController@postArticle');
            });
        });
        // ======================================================================================
        // ======================================== Sales ========================================
        Route::group(['prefix' => 'sales'], function () {
            Route::get('addSales', 'Agent\SalesController@getAddSalesAgent');
            Route::post('addSales', 'Agent\SalesController@postAddSalesAgent');
            // sales function
            Route::get('agentTopup', 'Agent\SalesController@getAgentTopup');
            Route::post('agentTopup', 'Agent\SalesController@postAgentTopup');
            Route::post('ajaxAgent', 'Agent\SalesController@getAgentData');
            Route::get('requestTopUp', 'Agent\SalesController@getRequestTopUp');
            Route::post('requestTopUp', 'Agent\SalesController@postRequestTopUp');
        });
        // =======================================================================================
    });
	
    /*Agent Warung*/
    Route::group(['prefix' => 'warung'], function () {
        // ======================================== Catalog ========================================
        Route::group(['prefix' => 'catalog'], function () {
            Route::get('category', 'Warung\CategoryController@category');
            Route::get('getlistcategory', 'Warung\CategoryController@getListCategory');
            Route::get('getlistcategorymobile', 'Warung\CategoryController@getListCategoryMobile');
            Route::post('crudcategory', 'Warung\CategoryController@crudCategory');
            Route::post('delcategory', 'Warung\CategoryController@delCategory');

            Route::get('brand', 'Warung\BrandController@brand');
            Route::get('getlistbrand', 'Warung\BrandController@getListBrand');
			Route::get('checkproductbybrand/{brandid}', 'Warung\BrandController@checkProductByBrand');
            Route::post('crudbrand', 'Warung\BrandController@crudBrand');
            Route::post('delbrand', 'Warung\BrandController@delBrand');

            Route::get('product', 'Warung\ProductController@product');
            Route::get('getlistproduct', 'Warung\ProductController@getListProduct');
            Route::get('getproductbyid/{id}', 'Warung\ProductController@getProductByID');
            Route::get('addproduct/{flag}/{id}', 'Warung\ProductController@addProduct')->name('add-catalog-product');
            Route::post('saveproduct', 'Warung\ProductController@saveProduct');
            Route::post('delproduct', 'Warung\ProductController@delProduct');
            Route::get('getmasterregion', 'Warung\ProductController@getMasterRegion');
            Route::get('test', 'Warung\ProductController@test');

            Route::get('convertionuom', 'Warung\UOMController@convertion');
            Route::get('formconvertionuom/{flag}/{uomconvertionid}', 'Warung\UOMController@formConvertion');

            Route::get('getlistconvertionuom', 'Warung\UOMController@getListConvertionUOM');
            Route::get('getconvertionuombyid/{uomconvertionid}', 'Warung\UOMController@getConvertionUOMByID');
            Route::get('getproductbyconvertionuom/{uomconvertionid}', 'Warung\UOMController@getProductByConvertationUOM');
            Route::post('crudconvertionuom', 'Warung\UOMController@crudConvertionUOM');
            Route::post('delconvertionuom', 'Warung\UOMController@delConvertionUOM');

            Route::get('uom', 'Warung\UOMController@uom');
            Route::get('getlistuom', 'Warung\UOMController@getListUOM');
            Route::post('cruduom', 'Warung\UOMController@crudUOM');
            Route::post('deluom', 'Warung\UOMController@delUOM');

            Route::get('region', 'Warung\RegionController@region');
            Route::get('getlistregion', 'Warung\RegionController@getListRegion');
            Route::get('getregionbyid/{id}', 'Warung\RegionController@getRegionByID');
            Route::get('getlistcity', 'Warung\RegionController@getListCity');
            Route::get('checkcity/{id}', 'Warung\RegionController@checkCity');
            Route::get('crudregion/{flag}/{id}', 'Warung\RegionController@crudRegion');            
            Route::post('saveregion', 'Warung\RegionController@saveRegion');
            Route::post('delregion', 'Warung\RegionController@delRegion');

            Route::get('stock', 'Warung\StockController@stockOpname');
            Route::get('getliststock', 'Warung\StockController@getListStock');
            Route::get('getsuggestproduct', 'Warung\StockController@getSuggestProduct');
            Route::get('getgroupproductuom/{productid}', 'Warung\StockController@getGroupProductUOM');
            Route::post('addstockopname', 'Warung\StockController@addStockOpname');

            Route::get('productbom', 'Warung\ProductController@productbom');
            Route::get('getlistproductbom', 'Warung\ProductController@getListProductBOM')->name('get-list-product-bom');
            Route::get('getproductbombyid/{id}', 'Warung\ProductController@getProductBOMByID');
            Route::get('formproductbom/{flag}/{id}', 'Warung\ProductController@formProductBOM')->name('bom-product-form');
            Route::post('saveproductbom', 'Warung\ProductController@saveProductBOM');            
            Route::post('delproductbom', 'Warung\ProductController@delProductBOM');            
            Route::get('getsuggestionproduct', 'Warung\ProductController@getSuggestionProduct');
            Route::post('addproductitembom', 'Warung\ProductController@addProductItemBOM');
            Route::post('updproductitembom', 'Warung\ProductController@updProductItemBOM');
            Route::post('delproductitembom', 'Warung\ProductController@delProductItemBOM');

            Route::post('saveprice', 'Warung\ProductController@savePriceByRegion');                        
            Route::post('delprice', 'Warung\ProductController@delPriceByRegion');                        
        });
        // ==================================== Transaction =======================================
        Route::group(['prefix' => 'transaction'], function () {
			Route::get('warung', 'Warung\TransactionController@getTransactionWarung');
			Route::get('warung/getlisttransaction', 'Warung\TransactionController@getListTransactionWarung');
			Route::get('warung/detail/{id}/{lockerid}', 'Warung\TransactionController@detailTransactionWarung')->name('warung-detail-transaction');
			Route::get('warung/getdetailtransaction', 'Warung\TransactionController@getDetailTransactionWarung');			
			Route::get('warung/getlistmasterwarung', 'Warung\TransactionController@getListMasterWarung');
        });
        // ==================================== Version =======================================
        Route::group(['prefix' => 'version'], function () {
            Route::get('/', 'Warung\VersionController@version');
            Route::get('getversion', 'Warung\VersionController@getVersion');
            Route::get('getversionbyid/{id}', 'Warung\VersionController@getVersionByID');
            Route::get('form/{flag}/{id}', 'Warung\VersionController@formVersion');
            Route::post('saveversion', 'Warung\VersionController@saveVersion');
            Route::post('delversion', 'Warung\VersionController@delVersion');
        });

        // ==================================== Good Receipt =======================================
        Route::group(['prefix' => 'receipt'], function () {
            Route::get('/', 'Warung\ReceiptController@receipt');
            Route::get('getlistreceipt', 'Warung\ReceiptController@getListReceipt');
            Route::get('getsummarydelivery', 'Warung\ReceiptController@getSummaryDelivery');
            Route::get('detail/{receiptid}', 'Warung\ReceiptController@detailReceipt');
            Route::get('getreceiptbybatch', 'Warung\ReceiptController@getReceiptByBatch');
            Route::get('formdetail/{receiptid}', 'Warung\ReceiptController@formDetail');
            Route::post('savedetail', 'Warung\ReceiptController@saveDetail');
        });        
        
        // ==================================== Ajax ==============================================
        Route::group(['prefix' => 'ajax'], function () {
            Route::get('downloadFile', 'Warung\TransactionController@downloadFile')->name('download-excel');
            Route::get('product-download-excel', 'Warung\ProductController@downloadFile')->name('product-download-excel');
        });
            
        // ==================================== Report Config ==============================================
        Route::group(['prefix' => 'report'], function () {
            Route::get('transaction', 'Warung\ReportConfiguration@index')->name('report-config');
            Route::post('submit-report-config', 'Warung\ReportConfiguration@submit')->name('submit-report-config');
            Route::get('report-config-list', 'Warung\ReportConfiguration@list')->name('report-config-list');
            Route::post('activate-report-config', 'Warung\ReportConfiguration@update')->name('activate-report-config');
            Route::post('deactivate-report-config', 'Warung\ReportConfiguration@update')->name('deactivate-report-config');
            Route::post('remove-report-config', 'Warung\ReportConfiguration@update')->name('remove-report-config');
        });
    });
	
    /* Executive Summary */
    Route::group(['prefix' => 'executive-summary'], function () {
        Route::get('', 'ExecutiveSummary\ExecutiveController@index')->name('executive-summary');
        Route::get('sales-stock-summary', 'ExecutiveSummary\ExecutiveController@summary')->name('summary-of-executive');
    });
        
    Route::group(['prefix' => 'purchase-summary'], function () {
        Route::get('', 'ExecutiveSummary\DashboardController@index')->name('purchase-summary');
        Route::get('summary-of-dashboard', 'ExecutiveSummary\DashboardController@summary')->name('summary-of-dashboard');
    });
        
    Route::group(['prefix' => 'topup-digital-registrasi'], function () {
        Route::get('', 'ExecutiveSummary\TopupDigitalRegistController@index')->name('topup-digital-regist');
        Route::get('topup-digital-summary', 'ExecutiveSummary\TopupDigitalRegistController@summary')->name('topup-digital-summary');
        Route::get('top-performing-popwarung-list', 'ExecutiveSummary\TopupDigitalRegistController@popwarung')->name('top-performing-popwarung-list');
        Route::get('top-performing-agent-list', 'ExecutiveSummary\TopupDigitalRegistController@agent')->name('top-performing-agent-list');
    });
        
    Route::group(['prefix' => 'sales-stock'], function () {
        Route::get('', 'ExecutiveSummary\SalesStockController@index')->name('sales-stock');
        Route::get('sales-stock-summary', 'ExecutiveSummary\SalesStockController@summary')->name('sales-stock-summary');
    });
        
    Route::group(['prefix' => 'point-of-sales'], function () {
        Route::get('', 'ExecutiveSummary\PointOfSalesController@index')->name('point-of-sales');
        Route::get('point-of-sales-summary', 'ExecutiveSummary\PointOfSalesController@summary')->name('point-of-sales-summary');
    });
        
    Route::group(['prefix' => 'map'], function () {
        Route::get('', 'ExecutiveSummary\DashboardController@map')->name('map');
        Route::get('list-of-mitra', 'ExecutiveSummary\DashboardController@list')->name('list-of-mitra');
    });
    
    Route::group(['prefix' => 'warung/message-management'], function () {
        Route::get('/','Warung\MessageServiceController@index')->name('message');
        Route::get('/new','Warung\MessageServiceController@newMessage')->name('new-message');
        Route::post('/create','Warung\MessageServiceController@create')->name('create-message');
        Route::get('/list','Warung\MessageServiceController@list')->name('list-message');
        Route::get('/get-total-user','Warung\MessageServiceController@totalUser')->name('get-total-user');
    });
        
    Route::group(['prefix' => 'warung/banner'], function () {
        Route::get('/','Warung\BannerController@index')->name('banner');
        Route::get('/list','Warung\BannerController@list')->name('list-banner');
        Route::post('/store','Warung\BannerController@store')->name('store-banner');
    });
    
    /*Payment Dashboard*/
    Route::group(['prefix' => 'payment'], function () {
        Route::group(['prefix' => 'transaction'], function () {
            Route::get('/', 'Payment\TransactionController@getList');
            Route::post('/', 'Payment\TransactionController@postExcel');
        });
        Route::group(['prefix' => 'channel'],function (){
            Route::get('/','Payment\ChannelController@getListChannel');
            Route::get('edit/{channelCode}','Payment\ChannelController@getEditChannel');
            Route::post('edit/{channelCode}','Payment\ChannelController@postChannel');
        });
        Route::group(['prefix' => 'client'],function (){
            Route::get('/','Payment\ChannelController@getListClient');
            Route::get('detail/{clientId}','Payment\ChannelController@getDetailClient');
            Route::get('changeStatus/{clientChannelId}','Payment\ChannelController@getChangeClientChannelStatus');
        });
    });

    /*Profile*/
    Route::group(['prefix' => 'profile'], function () {
        Route::get('changePassword', '\App\Http\Controllers\Auth\ProfileController@getChangePassword');
        Route::post('changePassword', '\App\Http\Controllers\Auth\ProfileController@postChangePassword');
    });

    /*PopExpress Dashboard*/
    Route::group(['prefix' => 'popexpress'], function () {

        // ==================================== Origin ==========================================
        Route::group(['prefix' => 'origins'], function () {
            Route::get('/', 'Popexpress\OriginController@index');
            Route::post('store', 'Popexpress\OriginController@store');
        });
        // =======================================================================================

        // ==================================== Audit Trails =====================================
        Route::group(['prefix' => 'audit_trails'], function () {
            Route::get('/', 'Popexpress\AuditTrailController@index');
        });
        // =======================================================================================

        // ==================================== Destinations =====================================
        Route::group(['prefix' => 'destinations'], function () {
            Route::get('/', 'Popexpress\DestinationController@index');
            Route::post('store', 'Popexpress\DestinationController@store');
            Route::post('export', 'Popexpress\DestinationController@export');
            Route::post('export_locker', 'Popexpress\DestinationController@exportLocker');
            Route::get('import', 'Popexpress\DestinationController@import');
            Route::post('import', 'Popexpress\DestinationController@importData');
            Route::get('download', 'Popexpress\DestinationController@download');
            Route::post('template', 'Popexpress\DestinationController@template');
            Route::get('detail/{destinationId}', 'Popexpress\DestinationController@detail');
            Route::post('add_branch_pickup', 'Popexpress\DestinationController@addBranchPickup');
            Route::post('delete_branch_pickup', 'Popexpress\DestinationController@deleteBranchPickup');
            Route::post('update_destination', 'Popexpress\DestinationController@updateDestination');
            Route::post('load', 'Popexpress\DestinationController@load');
        });
        // ======================================================================================

        // ==================================== Prices ==========================================
        Route::group(['prefix' => 'prices'], function () {
            Route::get('/', 'Popexpress\PriceController@index');
            Route::post('get_destination', 'Popexpress\PriceController@getDestination');
            Route::post('store', 'Popexpress\PriceController@store');
            Route::post('export', 'Popexpress\PriceController@export');
            Route::get('import', 'Popexpress\PriceController@import');
            Route::post('import', 'Popexpress\PriceController@importData');
            Route::get('download', 'Popexpress\PriceController@download');
            Route::post('template', 'Popexpress\PriceController@template');
        });
        // ======================================================================================

        // ==================================== Branches ========================================
        Route::group(['prefix' => 'branches'], function () {
            Route::get('/', 'Popexpress\BranchController@index');
            Route::post('store', 'Popexpress\BranchController@store');
            Route::post('export', 'Popexpress\BranchController@export');
            Route::get('detail/{branchId}', 'Popexpress\BranchController@detail');
            Route::post('update_branch', 'Popexpress\BranchController@updateBranch');
            Route::post('load', 'Popexpress\BranchController@load');
            Route::get('grid_destinations', 'Popexpress\BranchController@grid');
            Route::post('add_branch_pickup', 'Popexpress\BranchController@addBranchPickup');
        });
        // ======================================================================================

        // ==================================== Employees =======================================
        Route::group(['prefix' => 'employees'], function () {
            Route::get('/', 'Popexpress\EmployeeController@index');
            Route::get('create', 'Popexpress\EmployeeController@create');
            Route::post('store', 'Popexpress\EmployeeController@store');
            Route::post('export', 'Popexpress\EmployeeController@export');
            Route::get('edit/{id}', 'Popexpress\EmployeeController@edit');

            Route::post('changePassword','Popexpress\EmployeeController@changePassword');
            Route::get('transfer', 'Popexpress\EmployeeController@transfer');
            Route::get('transfer/{id}', 'Popexpress\EmployeeController@editTransfer');
            Route::post('store_transfer','Popexpress\EmployeeController@storeTransfer');
        });
        // ======================================================================================

        // ==================================== Customers =======================================
        Route::group(['prefix' => 'customers'], function () {
            Route::get('/', 'Popexpress\CustomerController@index');
            Route::get('create', 'Popexpress\CustomerController@create');
            Route::post('store', 'Popexpress\CustomerController@store');
            Route::post('export', 'Popexpress\CustomerController@export');
            Route::get('edit/{id}/{user_id}', 'Popexpress\CustomerController@edit');
            Route::post('changePassword', 'Popexpress\CustomerController@changePassword');
            Route::get('transfer', 'Popexpress\CustomerController@transfer');
            Route::get('transfer/{id}', 'Popexpress\CustomerController@storeTransfer');
        });
        // ======================================================================================

        // ==================================== Accounts ========================================
        Route::group(['prefix' => 'accounts'], function () {
            Route::get('/', 'Popexpress\AccountController@index');
            Route::post('store', 'Popexpress\AccountController@store');
            Route::post('export', 'Popexpress\AccountController@export');
        });
        // ======================================================================================

        // ==================================== Discounts =======================================
        Route::group(['prefix' => 'discounts', 'middleware' => 'popexpress_permission:popexpress_admin|popexpress_finance'], function () {
            Route::get('/', 'Popexpress\DiscountController@index');
            Route::post('store', 'Popexpress\DiscountController@store');
        });
        // ======================================================================================

        // ==================================== Price List ======================================
        Route::group(['prefix' => 'price_list'], function () {
            Route::get('/', 'Popexpress\PriceListController@index');
            Route::post('export','Popexpress\PriceListController@export');
        });
        // ======================================================================================

        // ==================================== Prices ==========================================
        Route::group(['prefix' => 'branches_pickups', 'middleware' => 'popexpress_permission:popexpress_admin|popexpress_operation_head'], function () {
            Route::get('/', 'Popexpress\BranchPickupController@index');
            Route::post('store','Popexpress\BranchPickupController@store');
        });
        // ======================================================================================

        // ==================================== Routes ==========================================
        Route::group(['prefix' => 'routes', 'middleware' => 'popexpress_permission:popexpress_admin|popexpress_operation_head|popexpress_courier_head|popexpress_branch_head'], function () {
            Route::get('/', 'Popexpress\RouteController@index');
            Route::get('create', 'Popexpress\RouteController@create')->middleware('popexpress_permission:popexpress_admin|popexpress_operation_head');
            Route::get('/{id}', 'Popexpress\RouteController@edit')->middleware('popexpress_permission:popexpress_admin|popexpress_operation_head');
            Route::post('store', 'Popexpress\RouteController@store')->middleware('popexpress_permission:popexpress_admin|popexpress_operation_head');
        });
        // ======================================================================================

        // ==================================== Pickups ==========================================
        Route::group(['prefix' => 'pickups'], function () {
            Route::get('/', 'Popexpress\PickupController@index');
            Route::get('create', 'Popexpress\PickupController@create');
            Route::post('store', 'Popexpress\PickupController@store');
            Route::post('get_location', 'Popexpress\PickupController@getLocation');
            Route::post('get_customer', 'Popexpress\PickupController@getCustomer');
            Route::get('view/{id}', 'Popexpress\PickupController@view');
            Route::get('print/{id}', 'Popexpress\PickupController@print');
            Route::get('list_pickup_details', 'Popexpress\PickupDetailController@index');
            Route::post('get_destination_price', 'Popexpress\PickupDetailController@get_destination_price');
            Route::post('get_account', 'Popexpress\PickupDetailController@get_account');
            Route::post('pickup_details/store', 'Popexpress\PickupDetailController@store');
            Route::get('list_pickup_tasks', 'Popexpress\PickupTaskController@index');
            Route::post('pickup_tasks/store', 'Popexpress\PickupTaskController@store');
        });
        // ======================================================================================
    });

    /* ANGKASAPURA 1 Dashboard*/
Route::group(['prefix' => 'angkasapura'], function () {
    Route::get('/','PopApps\PopsafeController@getLanding');

    Route::group(['prefix' => 'parcelist'], function () {
        Route::get('/','PopApps\PopsafeController@getLanding');
        Route::get('getDetailPage/{invoice_code}','PopApps\PopsafeController@getDetailPage');
        Route::get('getDetailPin/{invoice_code}','PopApps\PopsafeController@getDetailPin');
    });

    Route::group(['prefix' => 'locker'], function () {
        Route::get('location', 'Popbox\LockerController@CustomeLockerLocation');
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::get('summary', 'Popbox\LockerController@getPaymentTransactions');
        Route::group(['prefix' => 'ajax'], function () {
            Route::any('getAjaxGraph', 'Popbox\LockerController@getAjaxGraph');            
        });
    });

    Route::group(['prefix' => 'transaction'], function () {
        Route::get('/','PopApps\TransactionController@getTransactionList');
        Route::get('v2','PopApps\TransactionController@getLanding');
        Route::get('detail/{invoiceId}','PopApps\TransactionController@getTransactionDetail');
        Route::post('refund','PopApps\TransactionController@postRefundTransaction');
        Route::post('rePushDelivery','PopApps\TransactionController@postRePushDelivery');
        Route::post('cancelrefund','PopApps\TransactionController@postCancelRefundTransaction');
        Route::get('extendPopsafe/{transactionId}','PopApps\TransactionController@extendPopsafe');
        Route::group(['prefix' => 'ajax'], function () {
            Route::any('getDefaultData','PopApps\TransactionController@getDefaultData');
            Route::post('getFilterData','PopApps\TransactionController@getFilterData');
            Route::post('getAjaxDownloadTransaction','PopApps\TransactionController@getAjaxDownloadTransaction');
            Route::get('download', 'PopApps\PopsafeController@download');
        });
    });

});

});
