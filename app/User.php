<?php

namespace App;

use App\Models\Company;
use App\Models\Group;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*Relationship*/
    public function group(){
        return $this->belongsTo(Group::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }
}
