<?php

namespace App\Http\Helpers;

class SanitizeHelper{
    
    static function cleansingNull($data, $page_position = 0) {
        $result = [];
        $no=1;
        foreach($data as $key => $value) {
            $value['no'] = $no + $page_position;
            foreach($value as $key1 => $val1) {
                $value[$key1] = is_null($val1) ? '' : $val1;
            }
            $result[] = $value;
            $no++;
        }
        return $result;
    }
    
    static function addCounterNumber($data, $page_position = 0) {
        $result = [];
        $no=1;
        foreach($data as $row) {
            $value = (array) $row;
            $value['no'] = $no + $page_position;
            
            $result[] = $value;
            $no++;
        }
        return $result;
    }
    
    
}