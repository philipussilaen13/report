<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 05/03/2018
 * Time: 10.20
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class GoogleAPI
{
    private $id = null;
    private $apiKey = '';
    private $mapsApiUrl = 'https://maps.googleapis.com/maps/api/';

    public function __construct()
    {
        $this->id = uniqid();
        $this->apiKey = env('GOOGLE_MAP_KEY');
    }

    /**
     * File Get Content
     * @param $request
     * @param array $param
     * @return bool|string
     */
    private function fileGetContent($request,$param=[]){
        $unique = $this->id;
        $param['key'] = $this->apiKey;

        $paramQuery = http_build_query($param);
        $url = $request.$paramQuery;

        // check on DB
        $logApi = DB::table('companies_response')->where('api_url',$url)->orderBy('id','desc')->first();
        if ($logApi){
            $dataString = $logApi->api_response;
            $tmp = json_decode($dataString);
            if ($tmp->status == 'OK'){
                return $dataString;
            }
        }

        // log to file
        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url\n";
        $user = get_current_user();
        $f = fopen(storage_path().'/logs/api/google.'.$date.'.'.$user.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        $dataString = file_get_contents($url);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $dataString\n";
        $user = get_current_user();
        $f = fopen(storage_path().'/logs/api/google.'.$date.'.'.$user.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        // log to DB
        $insert = DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_response' => $dataString,
                'response_date' => date('Y-m-d H:i:s')
            ]);

        return $dataString;
    }

    /**
     * Reverse Geocode by Address
     * @param $address
     * @return bool|mixed|string
     */
    public function reverseGeocodeAddress($address){
        $param = [];
        $param['address'] = "$address";
        $request = $this->mapsApiUrl.'geocode/json?';
        $response = $this->fileGetContent($request,$param);
        $response = json_decode($response);
        return $response;
    }
}