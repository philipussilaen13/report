<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 11/01/2017
 * Time: 22:05
 */

namespace App\Http\Helpers;

use App\Models\Payment\ClientTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;


class Helper
{
    /**
     * generate random string
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10)
    {
        $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    /**
     * get start date and end date on week
     * @param $week
     * @param $year
     * @return mixed
     */
    public static function getStartAndEndDate($week, $year, $format = 'Y-m-d')
    {
        $dto = new \DateTime();
        $ret = [];
        $ret['week_start'] = $dto->setISODate($year, $week)->format($format);
        $ret['week_end'] = $dto->modify('+6 days')->format($format);
        return $ret;
    }
    
    /**
     * get Date From Day of Year
     * @param $dayOfYear
     * @param $year
     * @return bool|\DateTime
     */
    public static function getDateFromDay($dayOfYear, $year)
    {
        $date = \DateTime::createFromFormat('Y z', strval($year) . ' ' . strval($dayOfYear));
        return $date;
    }
    
    /**
     * Create Date From Month Number
     * @param $monthOfYear
     * @return bool|\DateTime
     */
    public static function getDateFromMonth($monthOfYear)
    {
        $date = \DateTime::createFromFormat('!m', $monthOfYear);
        return $date;
    }
    
    /**
     * Create Month Name from Month Number
     * @param $monthOfYear
     * @return bool|\DateTime
     */
    public static function getNameFromMonth($monthOfYear)
    {
        $date = \DateTime::createFromFormat('!m', $monthOfYear);
        $monthName = $date->format('F');
        return $monthName;
    }
    
    /**
     * trims text to a space then adds ellipses if desired
     * @param string $input text to trim
     * @param int $length in characters to trim to
     * @param bool $ellipses if ellipses (...) are to be added
     * @param bool $stripHtml if html tags are to be stripped
     * @return string
     */
    public static function trimText($input, $length, $ellipses = true, $stripHtml = true)
    {
        //strip tags, if desired
        if ($stripHtml) {
            $input = strip_tags($input);
        }
        
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }
        
        //find last space within length
        $lastSpace = strrpos(substr($input, 0, $length), ' ');
        $trimmedText = substr($input, 0, $lastSpace);
        
        //add ellipses (...)
        if ($ellipses) {
            $trimmedText .= '...';
        }
        
        return $trimmedText;
    }
    
    /**
     * create image from base64
     * @param $folder
     * @param $filename
     * @return bool|string
     */
    public static function createImg($folder, $filename)
    {
        $server = $_SERVER['DOCUMENT_ROOT'];
        $tmpPath = $server . env('AGENT_LOC') . $folder . '/' . $filename;
        $path = realpath($tmpPath);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = @file_get_contents($path);
        if (empty($data)) return false;
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }
    
    public static function createImgUrlAgent($folder,$filename){
        $publicUrl = env('AGENT_PUBLIC_URL');
        $imageUrl = $publicUrl.$folder."/".$filename;
        return $imageUrl;
    }
    
    /**
     * Calculate distance in mile
     * @param array $latLong1
     * @param array $latLong2
     * @return float
     */
    public static function calculateDistance($latLong1 = [], $latLong2 = [])
    {
        list($lat1, $lon1) = $latLong1;
        list($lat2, $lon2) = $latLong2;
        
        $lat1 = (float)$lat1;
        $lon1 = (float)$lon1;
        $lat2 = (float)$lat2;
        $lon2 = (float)$lon2;
        
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return $miles;
    }
    
    public static function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        return $dtF->diff($dtT)->format('in %a days, %h hours, %i minutes');
    }
    
    public static function getColor($number){
        $hash = md5('color'.$number);
        return [
            hexdec(substr($hash,0,2)),
            hexdec(substr($hash,2,2)),
            hexdec(substr($hash,4,2))
        ];
    }
    
    public static function getHexColor($number){
        return '#' . str_pad(dechex($number), 6, '0', STR_PAD_LEFT);
    }
    
    public static function formatDateRange($dateRange, $delimiter) {
        $result = new \stdClass();
        
        $tmpDate = explode($delimiter, $dateRange);
        $startDate = $tmpDate[0] . " 00:00:00";
        $endDate = $tmpDate[1] . " 23:59:59";
        
        $result->startDate = $startDate;
        $result->endDate = $endDate;
        
        return $result;
    }
    
    public static function download(Request $request) {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            echo "File not found";
        }
    }
    
    public static function removeReportFile($file_path) {
        
        $directory = '';
        $files = Storage::disk($file_path)->allFiles($directory);
        
        foreach($files as $r) {
            $res = Storage::disk('email_attach_file')->delete($r);
            if($res) {
                //Log::info('Report Transaction Warung ' . $r . ' has been deleted');
            }
            else {
                //Log::info('Failure when remove Report Transaction Warung ' . $r);
            }
        }
        
    }
    
    public static function delete_file($dir, $max_age = 3600*24*1) {
        $list = array();
        
        $limit = time() - $max_age;
        
        $dir = realpath($dir);
        
        if (!is_dir($dir)) {
            return;
        }
        
        $dh = opendir($dir);
        if ($dh === false) {
            return;
        }
        
        while (($file = readdir($dh)) !== false) {
            $file = $dir . '/' . $file;
            if (!is_file($file)) {
                continue;
            }
            
            if (filemtime($file) < $limit) {
                File::delete($file);
            }
            
        }
        
        closedir($dh);
        
        return $list;
    }
    
    public static function range_between_two_date($date1, $date2) {
        $earlier = new \DateTime($date1);
        $later = new \DateTime($date2);
        
        return $later->diff($earlier)->format("%a") + 1;
    }
    
    public static function rendering_excel($filename, $payload, $headers_attribute, $file_path = ''){
        
        $myFile = Excel::create("$filename", function ($excel) use ($payload, &$headers_attribute) {
            $excel->setTitle('Export File');
            $excel->setCreator('Dashboard Report')->setCompany('PopBox Agent');
            $excel->setDescription('Export File');
            $excel->sheet('sheet1', function ($sheet) use ($payload, &$headers_attribute) {
                $row = 1;
                
                $headers = collect($headers_attribute)->pluck('name')->all();
                $columns = collect($headers_attribute)->pluck('column')->all();
                
                $sheet->row($row, $headers);
                foreach($payload as $item){
                    $row++;
                    foreach ($columns as $index => $column){
                        $sheet->cell($column.$row, function ($cell) use ($index, &$item, &$headers_attribute) {
                            $cell->setValue($item[$headers_attribute[$index]['field']])->setAlignment(
                                isset($headers_attribute[$index]['align']) && !empty($headers_attribute[$index]['align']) ?
                                    $headers_attribute[$index]['align'] : "left"
                                );
                        });
                            
                        if(isset($headers_attribute[$index]['data_type']) && $headers_attribute[$index]['data_type'] == 'type_to_string'){
                            $sheet->setCellValueExplicit($column.$row, $item[$headers_attribute[$index]['field']], \PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                        
                        if(isset($headers_attribute[$index]['column_format']) && !empty($headers_attribute[$index]['column_format'])){
                            $sheet->setColumnFormat([$column.$row => $headers_attribute[$index]['column_format']]);
                        }
                    }
                }
            });
                
        });
            
        if(!empty($file_path)){
            // file is store on serve
            $myFile->store('xls', storage_path($file_path));
            return true;
        } else {
            $myFile = $myFile->string('xlsx');//change xlsx for the format you want, default is xls
            
            $response =  array(
                'name' => $filename, //no extention needed
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
            );
            
            return $response;
        }
        
        
    }
    
    public static function findLocker($id_warung){
        $locker_name = DB::connection('popbox_virtual')->table('lockers')->where('locker_id', $id_warung)->value('locker_name');
        if ($locker_name) {
            return $locker_name;
        }
        return '';
    }
    
    public static function detailClientTransactionByPaymentId($payment_id){
        
        $result =  ClientTransaction::leftJoin('popbox_payment.ottopay_transactions', 'popbox_payment.ottopay_transactions.client_transactions_id', '=', 'popbox_payment.client_transactions.id')
        ->where('popbox_payment.client_transactions.payment_id', '=', $payment_id)
        ->select('popbox_payment.ottopay_transactions.param_response')
        ->get();
        
        $response = [];
        if(sizeof($result) == 1){
            $p_response = $result[0]->param_response;
            $response = json_decode($p_response, true);
        }
        
        return $response;
    }
    
    public static function populate_week_number($date){
        
        $year = date('Y'); // get current year
        
        $date = new \DateTime($date);
        $week = $date->format("W");
        
        if($week < 2) $year = $year + 1; // masuk ke tahun berikutnya bila week number kecil dari 2 karena telah masuk akhir pekan dibulan desember
        
        $weekly = [];
        
        for($i = 1; $i <= 52; $i++){
            // $result = self::getStartAndEndDate($i, $year, 'd-m-Y');
            
            // $weekly[] = ['id' => $i, 'text' => '#'.$i.' | '.$result['week_start'].' s/d '.$result['week_end']];
            $weekly[] = ['id' => $i, 'text' => '#'.$i];
        }
        
        return $weekly;
    }
    
    public static function getWeekFromDate($date){
        return date("W", strtotime($date));
    }
    
}