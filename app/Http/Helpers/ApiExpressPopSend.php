<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 18/04/2018
 * Time: 14.54
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class ApiExpressPopSend
{
    private function cUrl($url, $param = array()){
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;

        if (is_array($param)){
            if (isset($param['api_key'])) $token = $param['api_key'];
        } else {
            if (isset($param->api_key)) $token = $param->api_key;
        }
        if (empty($token)){
            $token = env('POP_EXPRESS_KEY');
        }

        $header = [];
        $header[] = 'Content-Type:application/json';
        $header[] = 'api-key:'.$token;

        $json = json_encode($param);
        $currentUser = get_current_user();

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $f = fopen(storage_path()."/logs/api/express.$currentUser.$date.log",'a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        curl_setopt($ch, CURLINFO_HEADER_OUT,true);
        // exec
        $output = curl_exec($ch);

        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path()."/logs/api/express.$currentUser.$date.log",'a');
        fwrite($f,$msg);
        fclose($f);

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $json,
                'api_response'  => $output,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        return $output;
    }

    public function rePushPickup($url,$param){
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }
}