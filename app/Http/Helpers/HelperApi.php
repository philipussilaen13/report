<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 13/02/2017
 * Time: 14:09
 */

namespace App\Http\Helpers;



class HelperApi
{
    /**
     * @param string $endpoint
     * @param array $param
     * @return mixed
     */
    private function cUrl($endpoint, $param = array()){
        
        $host = config('constant.agent_api.url');
        $token = env('AGENT_TOKEN');
        
        if(substr($host, strlen($host) - 1) != '/'){
            $url = $host.'/'.$endpoint;
        } else {
            $url = $host.$endpoint;
        }
        
        $param['token'] = $token;
        $json = json_encode($param);
        
        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type:application/json'));
        $output = curl_exec($ch);
        curl_close($ch);
        
        return $output;
    }
    
    public function postSendMessage($message_id, $module){
        $endpoint = 'api/service/message/send';
        $param = [];
        $param['message_id'] = $message_id;
        $param['module'] = $module;
        $result = $this->cUrl($endpoint, $param);
        $result = json_decode($result);   
        return $result;
    }
    
    public function postStoreFile($fileName, $image, $subFolder){
        $endpoint = 'api/service/message/store-file';
        $param = [];
        $param['image'] = $image;
        $param['filename'] = $fileName;
        $param['dir_path'] = $subFolder;
        
        $result = $this->cUrl($endpoint, $param);
        
        $result = json_decode($result);
        return $result;
    }
}