<?php

namespace App\Http\Helpers;

use App\Models\PopApps\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\PopApps\PaymentMethods;

class CustomeLocker
{
    public function lockerLocations($userGroup)
    {
        $company = $this->getCompany($userGroup);
        $dataLocker = DB::connection('popsend')
                    ->table('transactions')
                    ->select('popsafes.locker_name')
                    ->join('popsafes', 'popsafes.id', '=', 'transactions.transaction_id_reference')
                    ->where('transactions.client_id_companies', $company)
                    ->groupBy('popsafes.locker_name')
                    ->get();

        return $dataLocker->toArray();
    }

    public function getAllUserCustome($userGroup)
    {
        $company = $this->getCompany($userGroup);
        $getUser = Transaction::with('user')->where('client_id_companies', $company)->groupBy('user_id')->get();
        $user = [];
        foreach ($getUser->toArray() as $key => $value) {
            $user[] = $value['user'];
        }
        return $user;
    }

    public function getCompany($groupname)
    {
        $groupCompany = null;
        if (isset($groupname) && $groupname != 'POPBOX') {
            $dataGroup = DB::connection('popsend')->table('companies')->where('code', $groupname)->first();
            $groupCompany = $dataGroup->client_id;
        }
        return $groupCompany;
    }

    public function paymentMethods()
    {
        $data = PaymentMethods::all();
        return $data;
    }

    public static function getGroupUser()
    {
        $data = DB::connection('popbox_report')
                        ->table('groups')
                        ->select('groupname')
                        ->where('groupname', '!=', 'POPBOX')
                        ->groupBy('groupname')
                        ->get()
                        ->toArray();
        $groups = [];
        foreach ($data as $key => $value) {
            $groups[] = $value->groupname;
        }
        return $groups;
    }

}

?>