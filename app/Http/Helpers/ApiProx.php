<?php
/**
 * Created by PhpStorm.
 * User: jrpikong
 * Date: 13/02/18
 * Time: 16:37
 */

namespace App\Http\Library;


use App\Http\Models\ApiResponse;
use FastRoute\RouteParser\Std;

class ApiProx
{
    private $id = null;
    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = array()){
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;

        $host = env('URL_PROX');
        $token = env('URL_PROX_TOKEN');
        $header = [];
        $header[] = 'Content-Type:application/json';
        $header[] = 'userToken:'.$token;

        $url = $host.'/'.$request;
        $param['api_key'] = $token;
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $f = fopen(storage_path().'/logs/api/prox.'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        $output = curl_exec($ch);
        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path().'/logs/api/prox.'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        // log to DB
        $logApi = new ApiResponse();
        $logApi->api_url = $url;
        $logApi->request = $json;
        $logApi->response = $output;
        $logApi->save();

        return $output;
    }

    public function importPopSafe($data)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $url = 'express/staffImportCustomerStoreExpress';
        $companyId = "161e5ed1140f11e5bdbd0242ac110001";

        $param = [];
        $param['account_name'] = 'Popsend';
        $param['groupName'] = 'POPDEPOSIT';

        $param['logisticsCompany']           = ['id' => $companyId]; // id group baru
        $param['takeUserPhoneNumber']        = $data['userPhone'];
        $param['customerStoreNumber']        = $data['invoiceCode'];
        $param['chargeType']                 = 'NOT_CHARGE';
        $param['recipientName']              = $data['userName'];
        $param['recipientUserPhoneNumber']   = $data['userPhone'];
        $param['endAddress']                 = 'PopBox @ '.$data['address'].' ]==)> PopSafe @ PopBox '. $data['address'];

        // post to pr0x
        $charac = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $inv = '';
        $max = strlen($charac) - 1;
        for ($i = 0; $i < 5; $i++) {
            $inv .= $charac[mt_rand(0, $max)];
        }
        $idm = (time()*1000).$inv;
        $param["id"] = hash('haval128,5',$idm);
        $param["logistic_id"] = $companyId;

        $result = $this->cUrl($url,$param);
        $result = json_decode($result);

        $response->isSuccess = true;
        $response->data = $result->data;

        return $response;
    }

    /**
     * @param string $country
     * @param $userPhone
     * @param $userName
     * @param $invoiceCode
     * @param $lockerName
     * @param $endAddress
     * @return \stdClass
     */
    public function orderPopSend($country = 'ID',$userPhone,$userName,$invoiceCode,$lockerName, $endAddress){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $url = 'express/staffImportCustomerStoreExpress';
        $companyId = "161e5ed1140f11e5bdbd0242ac110001";

        if ($country == 'MY'){
            $companyId = '4028808c57d5e92c0157dd3ea6373fd8';
        }

        $param = [];
        $param['account_name'] = 'Popsend';

        $param['logisticsCompany']           = ['id' => $companyId]; // id group baru
        $param['takeUserPhoneNumber']        = $userPhone;
        $param['customerStoreNumber']        = $invoiceCode;
        $param['chargeType']                 = 'NOT_CHARGE';
        $param['recipientName']              = $userName;
        $param['recipientUserPhoneNumber']   = $userPhone;
        $param['endAddress']                 = $endAddress;

        // post to pr0x
        $character = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $inv = '';
        $max = strlen($character) - 1;
        for ($i = 0; $i < 5; $i++) {
            $inv .= $character[mt_rand(0, $max)];
        }
        $idm = (time()*1000).$inv;
        $param["id"] = hash('haval128,5',$idm);
        $param["logistic_id"] = $companyId;

        $result = $this->cUrl($url,$param);
        $result = json_decode($result);

        $response->isSuccess = true;
        $response->data = $result->data;

        return $response;
    }

    public function orderProxFreeGift ($params)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $url = 'express/import';
        $result = $this->cUrl($url,$params);
        $result = json_decode($result);
        $resultRes = $result->response;

        if ($resultRes->code != 200) {
            $php_errormsg = 'Order To Prox Error';
            return $response->errorMsg = $php_errormsg;
        }

        $response->isSuccess = true;
        $response->data = $result->data;
        return $response;
    }

    /**
     * Change Overdue Time by Parcel ID
     * @param $parcelId
     * @param $unixOverdueDateTime
     * @return mixed
     */
    public function changeOverdueDateTime($parcelId, $unixOverdueDateTime){
        $url = 'task/express/resetExpress';
        $param = [];
        $param['id'] = $parcelId;
        $param['overdueTime'] = $unixOverdueDateTime;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Delete Imported Data
     * @param $expressId
     * @return mixed
     */
    public function deleteImport($expressId){
        $url = 'express/deleteImportedExpress/'.$expressId;
        $param = [];
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }
}