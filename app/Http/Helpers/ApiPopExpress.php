<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 27/10/2017
 * Time: 14.27
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class ApiPopExpress
{
    private $id = null;
    private $token = null;

    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = array()){
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;

        $host = env('POP_EXPRESS_URL');
        $token = $this->token;
        if (empty($token)){
            $token = env('POP_EXPRESS_KEY');
        }

        $header = [];
        $header[] = 'Content-Type:application/json';
        $header[] = 'api-key:'.$token;

        $url = $host.'/'.$request;
        $param['api_key'] = $token;
        $json = json_encode($param);
        $currentUser = get_current_user();

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $f = fopen(storage_path()."/logs/api/express.$currentUser.$date.log",'a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        curl_setopt($ch, CURLINFO_HEADER_OUT,true);
        // exec
        $output = curl_exec($ch);

        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path()."/logs/api/express.$currentUser.$date.log",'a');
        fwrite($f,$msg);
        fclose($f);

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $json,
                'api_response'  => $output,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        return $output;
    }

    /**
     * Get City Origin
     * @return mixed
     */
    public function getCityOrigin(){
        $url = 'list/origin';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get Province Destination
     * @return mixed
     */
    public function getProvinceDestination(){
        $url = 'list/province';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get City Destination
     * @param $provinceName
     * @return mixed
     */
    public function getCityDestination($provinceName){
        $url = 'list/county';
        $param = [];
        $param['province'] = $provinceName;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get District Destination
     * @param $cityName
     * @return mixed
     */
    public function getDistrictDestination($cityName){
        $url = 'list/district';
        $param = [];
        $param['county'] = $cityName;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get Tariff
     * @param $origin
     * @param $province
     * @param $city
     * @param $district
     * @return mixed
     */
    public function getTariff($origin,$province,$city,$district){
        $url = 'tariff';
        $param = [];
        $param['origin'] = $origin;
        $param['destination']['province'] = $province;
        $param['destination']['county'] = $city;
        $param['destination']['district'] = $district;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Register Agent for PopExpress
     * @param $agentName
     * @param $province
     * @param $city
     * @param $phone
     * @param $address
     * @param $userName
     * @param $email
     * @param $latitude
     * @param $longitude
     * @return mixed
     */
    public function registerAgent($agentName,$province,$city,$origin,$phone,$address,$userName,$email,$latitude,$longitude){
        $url = 'pickup_request/popagent/register';
        $param = [];
        $param['agent_name'] = $agentName;
        $param['province'] = $province;
        $param['city'] = $city;
        $param['origin'] = $origin;
        $param['contact_person'] = $userName;
        $param['phone'] = $phone;
        $param['address'] = $address;
        $param['email'] = $email;
        $param['latitude'] = $latitude;
        $param['longitude'] = $longitude;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Cancel Pickup
     * @param $pickupNumber
     * @param string $remarks
     * @return mixed
     */
    public function cancelPickup($pickupNumber,$remarks=""){
        $this->token = env('POP_EXPRESS_KEY_POPSEND');
        $url = 'pickup_request/cancel';
        $param = [];
        $param['pickup_number'] = $pickupNumber;
        $param['cancel_remarks'] = $remarks;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }
}