<?php
namespace App\Http\Helpers;

use App\Models\Popexpress\AuditTrail;
use App\Models\Popexpress\Discount;
use App\Models\Popexpress\Pickup;
use App\Models\Popexpress\Price;
use App\Models\UserGroup;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Doctrine\Common;

class CommonPopExpressHelper
{
    public static function helperExample()
    {
        return "This is helper example";
    }

    public static function listAirportCode()
    {
        return DB::table('international_codes')->select('airport_code')->distinct()->orderBy('airport_code', 'asc')->get();
    }

    public static function getMenuPermission($group)
    {
        $checkType = substr($group, 0, 1);
        switch ($checkType) {
            case "!":
                $permission = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                                ->where('user_groups.deleted_at', null)
                                ->where('user_groups.user_id', Auth::id())
                                ->whereNotIn('groups.name', explode('|', substr($group, 1)))
                                ->first();
                return (!is_null($permission) ? true : false);
                break;
            default:
                $permission = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                    ->where('user_groups.deleted_at', null)
                    ->where('user_groups.user_id', Auth::id())
                    ->whereIn('groups.name', explode('|', $group))
                    ->first();
                return (!is_null($permission) ? true : false);
                break;
        }
    }

    public static function getAPIAccess($key, $type)
    {
        $api = DB::connection('pop_express')->table('api')
            ->select('api.id')
            ->where('active', 1)->where('type', $type)->where('api_key', $key)->first();
        return $api;
    }

    public static function generateRandomString($length, $upperCase = true, $lowerCase = true, $numeric = true){
        $characters = '';
        if($numeric){
            $characters .= "0123456789";
        }
        if($lowerCase){
            $characters .= "abcdefghijklmnopqrstuvwxyz";
        }
        if($upperCase){
            $characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateUniqueId($prefix, $suffix)
    {
        return $prefix.time().$suffix;
    }

    public static function insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark)
    {
        $auditTrail = new AuditTrail();
        $auditTrail->id = CommonPopExpressHelper::generateUniqueId("ADT", CommonPopExpressHelper::generateRandomString(5, true, false, false));
        $auditTrail->key = $key;
        $auditTrail->module = $module;
        $auditTrail->type = $type;
        $auditTrail->json_before = $jsonBefore;
        $auditTrail->json_after = $jsonAfter;
        $auditTrail->remark = $remark;
        $auditTrail->created_at = date('Y-m-d H:i:s');
        $auditTrail->created_by = Auth::user()->id;
        $auditTrail->server_timestamp = date('Y-m-d H:i:s');
        $auditTrail->save();
    }

    public static function calcPrice($param)
    {
        $output = ["price_per_kg" => 0, "insurance_price" => 0, "rounded_weight" => 0, "discount_internal" => 0, "discount_external" => 0, "third_party_id" => 0, "ori_total_price" => 0, "destination_international_code" => ""];

        $discount = 0;

        $accountData = DB::connection('pop_express')->table('accounts')
            ->join('customers', function($join) use($param){
                $join->on('customers.id', '=', 'accounts.customer_id');
                $join->on('customers.user_id', '=', DB::raw($param['user_id']));
            })
            ->select('accounts.*')
            ->addSelect('customers.branch_id')
            ->where('accounts.id', '=', $param['account_id'])
            ->first();
        if($accountData){
            if(isset($param['branch_id'])){
                $db = DB::connection('pop_express')->table('prices')
                    ->join('branches', 'branches.origin_id', '=', 'prices.origin_id')
                    ->select('prices.*')
                    ->where('prices.destination_id', '=', $param['destination_id'])->where('branches.id', '=', $param['branch_id'])
                    ->first();
            }else{
                $db = DB::connection('pop_express')->table('prices')
                    ->join('branches', 'branches.origin_id', '=', 'prices.origin_id')
                    ->select('prices.*')
                    ->where('prices.destination_id', '=', $param['destination_id'])->where('branches.id', '=', $accountData->branch_id)
                    ->first();
            }

            if($db){
                if($param["service_type"] == "one_day"){
                    $output['price_per_kg'] = $db->one_day;
                }else{
                    $output['price_per_kg'] = $db->regular;
                }

                $param["weight"] = str_replace(",", ".", $param["weight"]);
                $output['rounded_weight'] = floor($param["weight"]);
                if(number_format($param["weight"] - $output['rounded_weight'], 2, ".", "") > 0.3){
                    $output['rounded_weight']++;
                }
                if($output['rounded_weight'] < 1){
                    $output['rounded_weight'] = 1;
                }

                if($param["insurance"] == "1"){
                    $output['insurance_price'] = number_format(0.002 * $param["price"], 2, ".", "") + 5000;
                }

                $zoneHandler = \App\Models\Popexpress\Zone::select('*')->where('international_code_id', '=', $param['destination_id'])->first();
                $handler = "";
                if($param["service_type"] == "one_day"){
                    $handler = $zoneHandler->one_day_handler;
                }else{
                    $handler = $zoneHandler->regular_handler;
                }

                if($handler != "0"){
                    $output['third_party_id'] = $handler;
                    $thirdParty = \App\Models\Popexpress\ThirdParty::find($output['third_party_id']);
                    $output['destination_international_code'] = $thirdParty->code;
                }else{
                    $internationalCode = \App\Models\Popexpress\InternationalCode::find($param['destination_id']);
                    $output['destination_international_code'] = $internationalCode->airport_code;
                }

                $today = date("Y-m-d");
                if($accountData->start_date != null && $accountData->end_date != null && $accountData->start_date <= $today && $accountData->end_date >= $today){
                    if($handler == "0"){
                        $discount = $accountData->discount_internal;
                        $output['discount_internal'] = $discount;
                    }else{
                        $discount = $accountData->discount_external;
                        $output['discount_external'] = $discount;
                    }
                }
            }
        }
        $output['total_price'] = number_format($output['price_per_kg']*$output['rounded_weight'], 2, ".", "");
        $output['ori_total_price'] = $output['total_price'];
        if($discount != 0){
            $output['total_price'] = number_format($output['total_price'] - ($output['total_price'] * $discount / 100), 2, ".", "");
        }
        $output['total_price'] += number_format($output['insurance_price'], 2, ".", "");
        $output['total_price'] = number_format($output['total_price'], 0, ".", "");
        $output['ori_total_price'] += number_format($output['insurance_price'], 2, ".", "");
        $output['ori_total_price'] = number_format($output['ori_total_price'], 0, ".", "");
        return $output;
    }

    public static function getBranchTimezone($branchId)
    {
        $output = "+07:00";
        $branch = \App\Models\Popexpress\Branch::find($branchId);
        if($branch){
            $output = $branch->timezone;
        }
        return CommonPopExpressHelper::formatTimezoneValue($output);
    }

    public static function formatTimezoneValue($timezone)
    {
        return str_replace("+", "", $timezone);
    }

    public static function getTimezoneText($timezone = "")
    {
        $output = "";
        if($timezone == ""){
            $timezone = session('timezone');
        }
        if($timezone == "+07:00"){
            $output = " WIB";
        }else if($timezone == "+08:00"){
            $output = " WITA";
        }else if($timezone == "+09:00"){
            $output = " WIT";
        }
        return $output;
    }

    public static function getUTCNowToTimezone($timezone = "07:00")
    {
        $tmpDate = Carbon::now("UTC");
        if($timezone != "00:00"){
            $arrTimezone = explode(":", $timezone);
            $tmpDate->addHours($arrTimezone[0])->addMinutes($arrTimezone[1]);
        }
        return $tmpDate;
    }

    public static function convertTimezone($date, $from, $to)
    {
        $tmpDate = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        if($from != $to){
            $arrFrom = explode(":", $from);
            $tmpDate->subHours($arrFrom[0])->subMinutes($arrFrom[1]);

            $arrTo = explode(":", $to);
            $tmpDate->addHours($arrTo[0])->addMinutes($arrTo[1]);
        }
        return $tmpDate;
    }

    public static function getFakeNowToTimezone($from, $to = "07:00")
    {
        $tmpDate = CommonPopExpressHelper::getUTCNowToTimezone($from);
        $totalMinute = ($tmpDate->hour*60) + $tmpDate->minute;
        $limitMinute = 5*60;
        if($totalMinute <= $limitMinute){
            $tmpDate->subDay();
            $tmpDate = Carbon::createFromFormat('Y-m-d H:i:s', $tmpDate->format('Y-m-d')." 23:59:59");
        }
        $tmpDate = CommonPopExpressHelper::convertTimezone($tmpDate->format('Y-m-d H:i:s'), $from, $to);
        return $tmpDate;
    }

    public static function getPrice($originId, $destinationId)
    {
        $price = Price::where('origin_id', $originId)
                    ->where('destination_id', $destinationId)
                    ->where('deleted_at', null)
                    ->orderBy('created_at', 'DESC')
                    ->first();
        return $price;
    }

    public static function getDiscount($accountId, $originId , $destinations)
    {
        $price = Price::find($destinations['price_id']);

        $checkDiscount = Discount::where('account_id', $accountId)
                            ->where('origin_id', $originId)
                            ->where('deleted_at', null)
                            ->orderBy('created_at', 'DESC')
                            ->orderBy('override', 'DESC')
                            ->first();

        $currentDate = date('Y-m-d');
        if(!is_null($checkDiscount)) {
            $airport_code = $checkDiscount->airport_code;
            $detail_code = $checkDiscount->detail_code;
            $province = $checkDiscount->province;
            $county = $checkDiscount->county;
            $district = $checkDiscount->district;

            $discount = Discount::where('account_id', $accountId)
                ->where('origin_id', $originId)
                ->where('deleted_at', null)
                ->where('is_locker', $destinations['is_locker'])
                ->whereDate('start_date', '<=', $currentDate)
                ->whereDate('end_date', '>=', $currentDate)
                ->when($airport_code, function ($query) use ($destinations) {
                    return $query->where('airport_code', 'LIKE', "%".$destinations['airport_code']."%");
                })
                ->when($detail_code, function ($query) use ($destinations) {
                    return $query->where('detail_code', 'LIKE', "%".$destinations['detail_code']."%");
                })
                ->when($province, function ($query) use ($destinations) {
                    return $query->where('province', 'LIKE', "%".$destinations['province']."%");
                })
                ->when($county, function ($query) use ($destinations) {
                    return $query->where('county', 'LIKE', "%".$destinations['county']."%");
                })
                ->when($district, function ($query) use ($destinations) {
                    return $query->where('district', 'LIKE', "%".$destinations['district']."%");
                })
                ->orderBy('created_at', 'DESC')
                ->orderBy('override', 'DESC')
                ->first();

            if(isset($discount->value)) {
                if($discount->type == 'percentage') {
                    $discountRegular = $price->price_regular * ($discount->value / 100);
                    $discountOne = $price->price_one * ($discount->value / 100);

                    $regular = $price->price_regular - $discountRegular;
                    $one = $price->price_one - $discountOne;
                } else {
                    $regular = $price->price_regular - $discount->value;
                    $one = $price->price_one - $discount->value;
                }
            } else {
                $regular = $price->price_regular;
                $one = $price->price_one;
            }

            $result = [
                'regular' => $regular,
                'one' => $one
            ];
        } else {

            $accountDiscount = Discount::where('account_id', $accountId)
                                ->where('deleted_at', null)
                                ->orderBy('created_at', 'DESC')
                                ->orderBy('override', 'DESC')
                                ->first();

            if($accountDiscount) {
                $airport_code = $accountDiscount->airport_code;
                $detail_code = $accountDiscount->detail_code;
                $province = $accountDiscount->province;
                $county = $accountDiscount->county;
                $district = $accountDiscount->district;
                $is_locker = $accountDiscount->is_locker;

                $discount = Discount::where('account_id', $accountId)
                    ->where('deleted_at', null)
                    ->whereDate('start_date', '<=', $currentDate)
                    ->whereDate('end_date', '>=', $currentDate)
                    ->when($airport_code, function ($query) use ($airport_code) {
                        return $query->where('airport_code', 'LIKE', "%".$airport_code."%");
                    })
                    ->when($detail_code, function ($query) use ($detail_code) {
                        return $query->where('detail_code', 'LIKE', "%".$detail_code."%");
                    })
                    ->when($province, function ($query) use ($province) {
                        return $query->where('province', 'LIKE', "%".$province."%");
                    })
                    ->when($county, function ($query) use ($county) {
                        return $query->where('county', 'LIKE', "%".$county."%");
                    })
                    ->when($district, function ($query) use ($district) {
                        return $query->where('district', 'LIKE', "%".$district."%");
                    })
                    ->when($is_locker, function ($query) use ($is_locker) {
                        return $query->where('is_locker', 'LIKE', "%".$is_locker."%");
                    })
                    ->orderBy('created_at', 'DESC')
                    ->orderBy('override', 'DESC')
                    ->first();

                if(isset($discount->value)) {
                    if($discount->type == 'percentage') {
                        $discountRegular = $price->price_regular * ($discount->value / 100);
                        $discountOne = $price->price_one * ($discount->value / 100);

                        $regular = $price->price_regular - $discountRegular;
                        $one = $price->price_one - $discountOne;
                    } else {
                        $regular = $price->price_regular - $discount->value;
                        $one = $price->price_one - $discount->value;
                    }
                } else {
                    $regular = $price->price_regular;
                    $one = $price->price_one;
                }

                $result = [
                    'regular' => $regular,
                    'one' => $one
                ];
            } else {
                $result = [
                    'regular' => $price->price_regular,
                    'one' => $price->price_one
                ];
            }

        }

        return $result;

    }

    public static function generatePickupNo()
    {
        $yearMonth = date('ym');
        $count = Pickup::where('pickup_no','LIKE', "%".$yearMonth."%")->count();
        return $yearMonth.sprintf("%'.05d", ($count+1));
    }

    public static function generatePickupId()
    {
        $prefix = 'WEB';
        $time = time();
        $rand = self::generateRandomString(2, true, false, false);
        $id = $prefix.$time.$rand;
        while(Pickup::where('id', $id)->count() > 0) {
            self::generatePickupId();
        }
        return $id;
    }



}

?>