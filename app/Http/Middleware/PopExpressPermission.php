<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Helpers\CommonPopExpressHelper;

class PopExpressPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $group)
    {
        if(!CommonPopExpressHelper::getMenuPermission($group)){
            abort(404);
        }
        return $next($request);
    }
}
