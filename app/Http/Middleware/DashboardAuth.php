<?php

namespace App\Http\Middleware;

use App\Models\DashboardLog;
use App\Models\Module;
use App\Models\Privilege;
use Closure;
use Illuminate\Support\Facades\Auth;

class DashboardAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // ignored path
        $ignored = ['auth/login','auth/logout'];
        $module = $request->path();
        // if not exist in ignored, then must login
        if (!in_array($module,$ignored)) {
            if (Auth::guest()) return redirect('auth/login');
        }
        if (in_array($module,$ignored)){
            return $next($request);
        }

        // create default privileges
        $isAllowed = false;
        // get user group
        $groupName = Auth::user()->group->name;
        if ($groupName=='superadmin'){
            return $next($request);
        }
        // if ajax request, no checking
        if ($request->ajax()) {
            return $next($request);
        }
        if ($module == 'agent/list/getAjaxSummary'){
            return $next($request);
        }

        $checkAjax = strpos($module, 'ajax');
        if ($checkAjax !== false) {
            return $next($request);
        }

        if ($groupName == 'admin'){
            if (strpos($module,'privilege') === false){
                return $next($request);
            }
        }

        $userId = Auth::user()->id;
        $moduleDb = Module::where('name','LIKE',$module)->first();
        if (!$moduleDb){
            $moduleName = null;
            $tmp = null;
            $moduleArray = explode('/',$module);
            for ($i = count($moduleArray); $i >= 0; $i--) {
                if (count($moduleArray) > 2){
                    if ($i <= 2) break;
                }
                $tmpModule = implode("/", $moduleArray);
                $moduleDb = Module::where('name', $tmpModule)->first();
                if ($moduleDb){
                    $moduleName = $tmpModule;
                    break;
                }
                unset($moduleArray[$i-1]);
            }
            $moduleDb = Module::where('name',$moduleName)->first();
            if (!$moduleDb) abort(404, "Module Not Found");
            else $module = $moduleName;
        }
        $moduleId = $moduleDb->id;
        if (!$isAllowed){
            // check module
            $email = Auth::user()->email;
            $groupId = Auth::user()->group->id;
            $checkPrivileges = Privilege::checkPrivileges($email, $module);
            if (!$checkPrivileges->isSuccess){
                // check on available module
                $privilegesDb = Privilege::where('group_id',$groupId)->first();
                if ($privilegesDb){
                    $moduleFoundId = $privilegesDb->module_id;
                    // find module
                    $moduleDb = Module::find($moduleFoundId);
                    if ($moduleDb){
                        $moduleName = $moduleDb->name;
                        return redirect($moduleName);
                    }
                }

                abort(401, 'Unauthorized. You have no right to access this Module');
            }
            // log to DB
            $userId = $checkPrivileges->userId;
            $moduleId = $checkPrivileges->moduleId;
        }
        $logId = DashboardLog::insertRequest($userId,$moduleId,$request);

        // process
        $response =  $next($request);

        //Log::updateResponse($logId,$response);
        return $response;
    }
}
