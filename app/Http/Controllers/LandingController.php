<?php

namespace App\Http\Controllers;

use App\Models\Agent\Transaction;
use App\Models\NewLocker\Box;
use App\Models\Popbox\LockerActivitiesAll;
use App\Models\Virtual\Locker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LandingController extends Controller
{
    public function getLanding(Request $request){
        return view('landing');
    }

    /**
     * Ajax Get Locker Online, Offline, Agent Total, and SUM of all
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxGetLocker(Request $request){
        // set default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // get locker from newLocker
        $boxDb = Box::getAllLocker();

        $lockerOnline = 0;
        $lockerOffline = 0;
        $agentTotal = 0;
        $pointTotal = 0;

        foreach ($boxDb as $locker){
            if ($locker->statusText == 'online') $lockerOnline++;
            elseif ($locker->statusText == 'offline') $lockerOffline++;
            $pointTotal++;
        }

        // get agent list
        $agentDb = Locker::where('type','agent')
            ->where('status','<>','0')
            ->get();

        $agentTotal = $agentDb->count();
        $pointTotal = $pointTotal + $agentTotal;

        $data = new \stdClass();
        $data->lockerOnline = $lockerOnline;
        $data->lockerOffline = $lockerOffline;
        $data->agentTotal = $agentTotal;
        $data->pointTotal = $pointTotal;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    /**
     * Ajax Get Parcel Total
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxGetParcel(Request $request){
        $count = $request->input('count',0);
        // set default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $beginWeekDate = date('Y-m-d',strtotime('this week monday'));
        $endWeekDate = date('Y-m-d',strtotime('this week sunday'));

        $lockerActivitiesDb = new LockerActivitiesAll();
        $lockerActivitiesData = $lockerActivitiesDb->whereBetween('storetime',[$beginWeekDate,$endWeekDate])
            ->orderBy('storetime', 'desc')
            ->get();

        $oneMinute = date('Y-m-d H:i:s',strtotime('-1 minute'));
        $nowDate = date('Y-m-d H:i:s');

        if ($count == 0){
            $latest = $lockerActivitiesData->take(5);
            $latest = $latest->all();
        } else {
            $latest = $lockerActivitiesData->filter(function ($value,$key) use($oneMinute,$nowDate){
                return $value->storetime > $oneMinute && $value->storetime <= $nowDate;
            });
            $latest = $latest->all();
        }
        $string = '';
        foreach ($latest as $parcel) {
            $string .= "<div class='item' style='border-bottom: 1px solid #f4f4f4;'>";
            $string .= "<div class='col-xs-7'>";
            $string .= "<p class='message'> $parcel->barcode </p>";
            $string .= "</div>";
            $string .= "<div class='col-xs-5'>";
            $string .= "<p class='message'> <span class='label label-success'>$parcel->locker_name</span></p>";
            $string .= "</div>";
            $string .= "</div>";
        }

        $parcelTotal = $lockerActivitiesData->count();

        $data = new \stdClass();
        $data->parcelTotal = $parcelTotal;
        $data->latest = $string;
        $data->beginWeekDate = date('j M Y',strtotime($beginWeekDate));
        $data->endWeekDate = date('j M Y',strtotime($endWeekDate));

        $response->isSuccess = true;
        $response->data = $data;

        return response()->json($response);
    }

    /**
     * Ajax Get Delivery
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxGetDelivery(Request $request){
        $count = $request->input('count',0);
        // set default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $beginWeekDate = date('Y-m-d',strtotime('this week monday'));
        $endWeekDate = date('Y-m-d',strtotime('this week sunday'));

        $expressDeliveryDb = DB::connection('pop_express')
            ->table('courier_ins')
            ->join('pickup_details','courier_ins.receipt_number','=','pickup_details.receipt_number')
            ->join('international_codes','pickup_details.destination_id','=','international_codes.id')
            ->whereBetween('courier_ins.date',[$beginWeekDate,$endWeekDate])
            ->orderBy('courier_ins.date','desc')
            ->select('courier_ins.receipt_number','international_codes.county','courier_ins.status','courier_ins.date')
            ->get();

        $oneMinute = date('Y-m-d H:i:s',strtotime('-1 minute'));
        $nowDate = date('Y-m-d H:i:s');

        if ($count == 0){
            $latest = $expressDeliveryDb->take(5);
            $latest = $latest->all();
        } else {
            $latest = $expressDeliveryDb->filter(function ($value,$key) use($oneMinute,$nowDate){
                return $value->date > $oneMinute && $value->date <= $nowDate;
            });
            $latest = $latest->all();
        }
        $string = '';
        foreach ($latest as $delivery) {
            $string .= "<div class='item' style='border-bottom: 1px solid #f4f4f4;'>";
            $string .= "<div class='col-xs-7'>";
            $string .= "<p class='message'> $delivery->receipt_number <br>  $delivery->county </p>";
            // $string .= "<p class='message'> </p>";
            $string .= "</div>";
            $string .= "<div class='col-xs-5'>";
            if ($delivery->status == 'DITERIMA'){
                $string .= "<p class='message'> <span class='label label-success'>$delivery->status</span></p>";
            } else {
                $string .= "<p class='message'> <span class='label label-warning'>$delivery->status</span></p>";
            }
            $string .= "</div>";
            $string .= "</div>";
        }

        $deliveryTotal = $expressDeliveryDb->count();

        $data = new \stdClass();
        $data->parcelTotal = $deliveryTotal;
        $data->latest = $string;
        $data->beginWeekDate = date('j M Y',strtotime($beginWeekDate));
        $data->endWeekDate = date('j M Y',strtotime($endWeekDate));

        $response->isSuccess = true;
        $response->data = $data;

        return response()->json($response);
    }

    /**
     * Ajax Get Transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxGetTransaction(Request $request){
        $countTransaction = $request->input('countTransaction',0);
        $countTopUp = $request->input('countTopUp',0);

        // set default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $beginWeekDate = date('Y-m-d',strtotime('this week monday'));
        $endWeekDate = date('Y-m-d',strtotime('this week sunday'));

        $purchaseTransactionCount = 0;
        $purchaseTransactionSum = 0;
        $topupTransactionCount = 0;
        $topupTransactionSum = 0;
        $rewardTransactionSum = 0;

        $paidTransaction = Transaction::with('items')
            ->where('status', 'PAID')
            ->whereBetween('updated_at', [$beginWeekDate, $endWeekDate])
            ->orderBy('updated_at', 'desc')
            ->get();

        $latestTransaction = [];
        $latestDigital = [];
        $latestShop=[];
        $latestTopUp = [];

        $transactionShop = [];


        $oneMinute = date('Y-m-d H:i:s',strtotime('-3 minute'));
        $nowTime = date('Y-m-d H:i:s');

        foreach ($paidTransaction as $transaction) {
            if ($transaction->type == 'purchase' || $transaction->type == 'payment') {
                $purchaseTransactionSum += $transaction->total_price;
                $purchaseTransactionCount++;
                // new latest
                if ($countTransaction == 0){
                    if (count($latestTransaction) <= 5) $latestTransaction[] = $transaction;
                } else {
                    if ($transaction->created_at >= $oneMinute && $transaction->created_at <= $nowTime) $latestTransaction[] = $transaction;
                }

                // get shop only and new latest
                $tmpTransaction = $transaction->items()->where('type','popshop')->get();
                if (count($tmpTransaction) > 0){
                    foreach ($tmpTransaction as $item) {
                        if ($countTransaction == 0){
                            if (count($latestShop) <= 5) $latestShop[] = $item;
                        } else {
                            if ($item->created_at >= $oneMinute && $item->created_at <= $nowTime) $latestShop[] = $item;
                        }
                    }
                } else {
                    if ($countTransaction == 0){
                        if (count($latestDigital) <= 5) $latestDigital[] = $transaction;
                    } else {
                        if ($transaction->created_at >= $oneMinute && $transaction->created_at <= $nowTime) $latestDigital[] = $transaction;
                    }
                }
            }
        }

        foreach ($paidTransaction as $transaction) {
            if ($transaction->type == 'topup') {
                $topupTransactionSum += $transaction->total_price;
                $topupTransactionCount++;
                if ($countTopUp == 0){
                    if (count($latestTopUp) <= 5) $latestTopUp[] = $transaction;
                } else {
                    if ($transaction->created_at >= $oneMinute && $transaction->created_at <= $nowTime) $latestTopUp[] = $transaction;
                }
            }
        }

        foreach ($paidTransaction as $transaction) {
            if ($transaction->type == 'commission' || $transaction->type == 'reward' || $transaction->type == 'referral') {
                $rewardTransactionSum += $transaction->total_price;
            }
        }

        $stringLatestTransaction = '';
        foreach ($latestTransaction as $item) {
            $lockerName = $item->user->locker->locker_name;
            $stringLatestTransaction .= "<div class='item' style='border-bottom: 1px solid #f4f4f4;'>";
                $stringLatestTransaction .= "<div class='col-xs-8'>";
                    $stringLatestTransaction .= "<p class='message'> $item->description </p>";
                $stringLatestTransaction .= "</div>";
                $stringLatestTransaction .= "<div class='col-xs-4'>";
                    $stringLatestTransaction .= "<p class='message'> <span class='label label-info'>$lockerName</span></p>";
                $stringLatestTransaction .= "</div>";
            $stringLatestTransaction .= "</div>";
        }

        $stringLatestTopUp = '';
        foreach ($latestTopUp as $item) {
            $lockerName = $item->user->locker->locker_name;
            $stringLatestTopUp .= "<div class='item' style='border-bottom: 1px solid #f4f4f4;'>";
            $stringLatestTopUp .= "<div class='col-xs-8'>";
            $stringLatestTopUp .= "<p class='message'> $item->description </p>";
            $stringLatestTopUp .= "</div>";
            $stringLatestTopUp .= "<div class='col-xs-4'>";
            $stringLatestTopUp .= "<p class='message'> <span class='label label-info'>$lockerName</span></p>";
            $stringLatestTopUp .= "</div>";
            $stringLatestTopUp .= "</div>";
        }

        $stringLatestShop = '';
        foreach ($latestShop as $item) {
            $lockerName = $item->transaction->user->locker->locker_name;
            $stringLatestShop .= "<div class='item' style='border-bottom: 1px solid #f4f4f4;'>";
            $stringLatestShop .= "<div class='col-xs-8'>";
            $stringLatestShop .= "<p class='message'> $item->name ".number_format($item->price)."</p>";
            $stringLatestShop .= "</div>";
            $stringLatestShop .= "<div class='col-xs-4'>";
            $stringLatestShop .= "<p class='message'> <span class='label label-info'>$lockerName</span></p>";
            $stringLatestShop .= "<p class='message'> <span class='label label-default'>$item->created_at</span></p>";
            $stringLatestShop .= "</div>";
            $stringLatestShop .= "</div>";
        }

        $stringLatestDigital='';
        foreach ($latestDigital as $item) {
            $lockerName = $item->user->locker->locker_name;
            $stringLatestDigital .= "<div class='item' style='border-bottom: 1px solid #f4f4f4;'>";
            $stringLatestDigital .= "<div class='col-xs-8'>";
            $stringLatestDigital .= "<p class='message'> $item->description </p>";
            $stringLatestDigital .= "</div>";
            $stringLatestDigital .= "<div class='col-xs-4'>";
            $stringLatestDigital .= "<p class='message'> <span class='label label-info'>$lockerName</span></p>";
            $stringLatestDigital .= "</div>";
            $stringLatestDigital .= "</div>";
        }

        $data = new \stdClass();
        $data->purchaseTransactionCount = $purchaseTransactionCount;
        $data->purchaseTransactionSum = $purchaseTransactionSum;
        $data->topupTransactionCount = $topupTransactionCount;
        $data->topupTransactionSum = $topupTransactionSum;
        $data->latestTransaction = $stringLatestTransaction;
        $data->latestShop = $stringLatestShop;
        $data->latestTopUp = $stringLatestTopUp;
        $data->latestDigital = $stringLatestDigital;
        $data->beginWeekDate = date('j M Y',strtotime($beginWeekDate));
        $data->endWeekDate = date('j M Y',strtotime($endWeekDate));

        $response->isSuccess = true;
        $response->data = $data;

        return response()->json($response);
    }
}
