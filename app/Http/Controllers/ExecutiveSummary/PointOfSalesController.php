<?php

namespace App\Http\Controllers\ExecutiveSummary;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helper;
use App\Http\Helpers\SanitizeHelper;
use App\Models\Virtual\City;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PointOfSalesController extends Controller{
    
    public function index(Request $request){
        $user_types = [
            ['id' => 'all',     'text' => 'ALL'],
            ['id' => 'agent',   'text' => 'Agent'],
            ['id' => 'warung',  'text' => 'Warung'],
        ];
        
        $cities = City::select('id as id', 'city_name as text')->orderByRaw('city_name ASC')->get();
        $city = 'all';
        return view('executive_summary.pointofsale.list', compact('user_types', 'cities', 'city'));
    }
    
    public function summary(){
        $city = request('city');
        $userType = request('user_type');
        $dateRange = request('daterange_transaction');
        
        $startDate = '';
        $endDate = '';
        $fast_sales = '';
        $popshop = '';
        $total_transaction = '';
        $number_of_verified_user = '';
        $number_of_active_user = '';
        $lockerid = 'all';
        $popular_product = array();
        $popular_popwarung = array();
        $avg_sales_per_store_per_active_user = "0";
        $avg_sales_per_store_per_active_user_per_day = "0";
        
        $params = [
            'where_location'                => '',
            'where_transaction_created_at'  => '',
            'where_user_type'               => '',
            'where_payment_method_id'       => '',
            'status_verification'           => '',
        ];
        if(!empty($dateRange)) {
            $startDate = substr(Helper::formatDateRange($dateRange, ',')->startDate, 0, 10);
            $endDate = substr(Helper::formatDateRange($dateRange, ',')->endDate, 0, 10);
        }
        
        $date_diff = Helper::range_between_two_date($startDate, $endDate);
        
        if(!empty($userType) && $userType != 'all'){
            $params['where_user_type'] = "and l.type = '".$userType."'";
        } else {
            $params['where_user_type'] = "and l.type in ('agent', 'warung')";
        }
        
        if(!empty($startDate) && !empty($endDate)){
            $params['where_transaction_created_at'] = "and tc.created_at between '".$startDate." 00:00:00' and '".$endDate." 23:59:59'";
        }
        
        if(!empty($city) && $city != 'all'){
            $params['where_location'] = "and l.cities_id = ".$city;
        }
        
        try{
            // All user
            $all_user = DB::connection('popbox_agent')->select($this->user_query($params));
            if(!empty($all_user)){
                $lockerid = collect($all_user)->map(function ($item){
                    $item = (array) $item;
                    return $item['locker_id'];
                })->toArray();
                
                $result = DB::connection('product_service')->select($this->pointofsales_activity($lockerid, $startDate, $endDate));
                $fast_sales = $result[0]->fast_sales;
                $popshop = $result[0]->popshop;
                $total_transaction = $fast_sales + $popshop;
                
                // Verified user
                $params['status_verification'] = " and lk.status_verification = 'verified'";
                $verified_user = DB::connection('popbox_agent')->select($this->user_query($params));
                $number_of_verified_user = count($verified_user);
                $verified_lockerid = collect($verified_user)->map(function ($item){
                    $item = (array) $item;
                    return $item['locker_id'];
                })->toArray();
                $result = DB::connection('product_service')->select($this->active_user($startDate, $endDate, $verified_lockerid));
                $number_of_active_user = $result[0]->number_of_users;
                
                
                // Top 20 Best Selling Product - PopWarung / PopAgent
                $popular_product = DB::connection('product_service')->select($this->popular_product($lockerid, $startDate, $endDate));
                
                // Top 10 PopWarung
                $result = DB::connection('product_service')->select($this->popular_popwarung($lockerid, $startDate, $endDate));
                foreach ($result as $r){
                    $r = (array) $r;
                    $r['name'] = Helper::findLocker($r['locker_id']);
                    array_push($popular_popwarung, $r);
                }
                
                if($number_of_active_user > 0){
                    $avg_sales_per_store_per_active_user = round(($total_transaction/$number_of_active_user), 2);
                    $avg_sales_per_store_per_active_user_per_day = round(($total_transaction/$number_of_active_user) / $date_diff, 2);
                } else {
                    $avg_sales_per_store_per_active_user = 0;
                    $avg_sales_per_store_per_active_user_per_day = 0;
                }
            }
            
        } catch (\Exception $e){
            Log::error($e->getMessage());
            Log::error($e->getTrace());
        }
        
        $data = [];
        $data['payload']['fast_sales'] = $fast_sales;
        $data['payload']['popshop'] = $popshop;
        $data['payload']['total_transaction'] = $total_transaction;
        $data['payload']['active_user'] = $number_of_active_user;
        $data['payload']['avg_sales_per_store_per_verified_user'] = $number_of_verified_user > 0 ? round(($total_transaction/$number_of_verified_user), 2) : 0;
        $data['payload']['avg_sales_per_store_per_verified_user_per_day'] = $number_of_verified_user > 0 ? round(($total_transaction/$number_of_verified_user) / $date_diff, 2) : 0;
        $data['payload']['avg_sales_per_store_per_active_user'] = $avg_sales_per_store_per_active_user;
        $data['payload']['avg_sales_per_store_per_active_user_per_day'] = $avg_sales_per_store_per_active_user_per_day;
        $data['payload']['popular_product'] = SanitizeHelper::addCounterNumber($popular_product);
        $data['payload']['popular_popwarung'] = SanitizeHelper::addCounterNumber($popular_popwarung);
        
        return $data;
    }
    
    private function user_query($params){
        return "select distinct l.locker_id, l.locker_name, l.type, lk.status_verification
                from
                    users u
                    join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    join popbox_agent.lockers lk on lk.id = l.locker_id
                where
                    l.`deleted_at` is null
                    ".$params['status_verification']."
                    ".$params['where_user_type']."
                    ".$params['where_location'];
    }
    
    private function pointofsales_activity($lockerid, $startDate, $endDate) {
        $whereCondition = 'AND tc.status not in ("VOID")';
        if(!empty($lockerid) && $lockerid != 'all') {
            if(is_array($lockerid)){
                $locker_id = '';
                foreach ($lockerid as $id){
                    $locker_id .= "'".$id."',";
                }
                $whereCondition .= " AND tc.locker_id in (".substr($locker_id, 0, strlen($locker_id)-1).")";
            } else{
                $whereCondition .= " AND tc.locker_id = '".$lockerid."' ";
            }
        }
        
        if(!empty($startDate) && !empty($endDate) && $startDate != 'all' && $endDate != 'all') {
            $whereCondition .= " AND DATE(tc.`date_transaction`) BETWEEN DATE('".$startDate." 00:00:00') AND DATE('".$endDate." 23:59:59') ";
        }
        
        return "
            select (
               select sum(price) as 'sum_trx'
               from (
                    select tcd.sku, tcd.name, tcd.qty, tcd.price
                    from transaction_customer_detail tcd
                    join transaction_customer tc on tc.id = tcd.transaction_customer_id
                    where 1=1
                         and tcd.type = 'fast_sales' ".$whereCondition."
                    order by tcd.name, tcd.sku
               ) as temp_fast_sales
            ) as 'fast_sales',
            (
               select sum(price) as 'sum_trx'
               from (
                    select distinct tc.reference, tc.total_amount as 'price'
                    from transaction_customer_detail tcd
                    join transaction_customer tc on tc.id = tcd.transaction_customer_id
                    where 1=1
                         and tcd.type = 'popshop' ".$whereCondition."
                    order by tcd.name, tcd.sku
               ) as temp_popshop
            ) as 'popshop'
        ";
    }
    
    private function active_user($startDate, $endDate, $verified_lockerid) {
        $whereCondition = 'AND tc.status not in ("VOID")';
        
        if(!empty($verified_lockerid) && $verified_lockerid != 'all') {
            if(is_array($verified_lockerid)){
                $locker_id = '';
                foreach ($verified_lockerid as $id){
                    $locker_id .= "'".$id."',";
                }
                $whereCondition .= " AND tc.locker_id in (".substr($locker_id, 0, strlen($locker_id)-1).")";
            } else{
                $whereCondition .= " AND tc.locker_id = '".$locker_id."' ";
            }
        }
        
        if(!empty($startDate) && !empty($endDate) && $startDate != 'all' && $endDate != 'all') {
            $whereCondition .= " AND DATE(tc.`date_transaction`) BETWEEN DATE('".$startDate." 00:00:00') AND DATE('".$endDate." 23:59:59') ";
        }
        
        return "
               select count(user) as 'number_of_users'
               from (
                    select distinct tc.locker_id as 'user'
                    from transaction_customer_detail tcd
                    join transaction_customer tc on tc.id = tcd.transaction_customer_id
                    where 1=1
                         and tcd.type in ('popshop', 'fast_sales') ".$whereCondition."
                    order by tc.locker_id
               ) as number_of_users
        ";
    }
    
    private function popular_product($lockerid, $startDate, $endDate){
        $whereCondition = 'AND tc.status not in ("VOID")';
        if(!empty($lockerid) && $lockerid != 'all') {
            if(is_array($lockerid)){
                $locker_id = '';
                foreach ($lockerid as $id){
                    $locker_id .= "'".$id."',";
                }
                $whereCondition .= " AND tc.locker_id in (".substr($locker_id, 0, strlen($locker_id)-1).")";
            } else{
                $whereCondition .= " AND tc.locker_id = '".$lockerid."' ";
            }
        }
        
        if(!empty($startDate) && !empty($endDate) && $startDate != 'all' && $endDate != 'all') {
            $whereCondition .= " AND DATE(tc.`date_transaction`) BETWEEN DATE('".$startDate." 00:00:00') AND DATE('".$endDate." 23:59:59') ";
        }
        
        return "
                select qty as 'purchased', product_name, sku, sum(amount) as 'amount'
                  from (
                        select product_name, sku, sum(qty) as 'qty', sum(price*qty) as 'amount'
                        from (
                                   select p.barcode as 'sku', tcd.name as 'product_name', tcd.qty, tcd.price
                                   from transaction_customer_detail tcd
                                   join transaction_customer tc on tc.id = tcd.transaction_customer_id
                                   join products p on p.id = tcd.products_id
                                   where 1=1
                                        and tcd.type in ('popshop', 'fast_sales')  ".$whereCondition."
                            order by tcd.name asc
                        ) as temp
                        group by product_name, sku
                        order by product_name, sku
                  ) as temp1
                  group by product_name, sku
                  order by qty desc,
                  product_name asc
                        limit 0,20
            ";
    }
    
    private function popular_popwarung($lockerid, $startDate, $endDate){
        $whereCondition = 'AND tc.status not in ("VOID")';
        if(!empty($lockerid) && $lockerid != 'all') {
            if(is_array($lockerid)){
                $locker_id = '';
                foreach ($lockerid as $id){
                    $locker_id .= "'".$id."',";
                }
                $whereCondition .= " AND tc.locker_id in (".substr($locker_id, 0, strlen($locker_id)-1).")";
            } else{
                $whereCondition .= " AND tc.locker_id = '".$lockerid."' ";
            }
        }
        
        if(!empty($startDate) && !empty($endDate) && $startDate != 'all' && $endDate != 'all') {
            $whereCondition .= " AND DATE(tc.`date_transaction`) BETWEEN DATE('".$startDate." 00:00:00') AND DATE('".$endDate." 23:59:59') ";
        }
        
        return "
                select locker_id, amount
                from (
                    select locker_id, sum(total_amount) as 'amount'
                    from (
                              select distinct tc.locker_id, tc.total_amount, tc.date_transaction
                              from transaction_customer_detail tcd
                              join transaction_customer tc on tc.id = tcd.transaction_customer_id
                              where 1=1
                                   and tcd.type in ('popshop', 'fast_sales') ".$whereCondition."
                        order by tc.locker_id asc
                    ) as temp
                    group by locker_id
                    order by locker_id asc
                ) as temp1
                order by amount desc
                limit 0,10
        ";
    }
}