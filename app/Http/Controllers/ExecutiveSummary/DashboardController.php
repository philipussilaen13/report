<?php

namespace App\Http\Controllers\ExecutiveSummary;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;
use App\Models\OutlineReport;

class DashboardController extends Controller{
    
    public function index(Request $request){
        $user_types = [
            ['id' => 'agent',   'text' => 'Agent'],
            ['id' => 'warung',  'text' => 'Warung'],
        ];
        
        $lockers = [
            ['id' => '0', 'text' => '-- Semua --']
        ];
        $locker = 0;
        
        return view('executive_summary.dashboard', compact('lockers', 'locker', 'user_types'));
    }
    
    public function summary(){
//         $locker_id = request('locker_id');
        $dateRange = request('daterange_transaction');
        $userType = request('user_type');
        
        $data = [];
        if(!empty($dateRange)) {
            $from = Helper::formatDateRange($dateRange, ',')->startDate;
            $to = Helper::formatDateRange($dateRange, ',')->endDate;
        }
        
        $outline = OutlineReport::whereBetween('report_date', [$from, $to]);
        $result = '';
        if($userType == 'agent'){
            $result = $outline->select('agent_data', 'report_date')->get();
        } else {
            $result = $outline->select('warung_data', 'report_date')->get();
        }
        
        $rows = [];
        foreach ($result as $r){
            if($userType == 'agent'){
                $row = json_decode($r->agent_data, true);
            } else {
                $row = json_decode($r->warung_data, true);
            }
            $key = date('d-M-Y', strtotime($r->report_date));
            $rows[$key] = $row[$key];
        }
        
        $data['payload']['data'] = $rows;
        return $data;
    }
    
    public function map(Request $request){
        $group_name = strtoupper(Auth::user()->group->name);
        
        $agent_opt  = [strtoupper('superadmin'), 'BD', 'CS', strtoupper('CS - Add permission view detail agent'), strtoupper('BD + Activation Agent/Warung'), strtoupper('BD + COD Confirmation')];
        $warung_opt = [strtoupper('superadmin'), 'BD', 'CS', strtoupper('CS - Add permission view detail agent'), strtoupper('BD + Activation Agent/Warung'), strtoupper('BD + COD Confirmation')];
        $locker_opt = [strtoupper('superadmin'), ];
        $all_opt    = [strtoupper('superadmin'), ];
        
        $user_types = [];
        if(in_array($group_name, $all_opt)){
            array_push($user_types, ['id' => 'all', 'text' => 'All User']);
        }
        
        if(in_array($group_name, $locker_opt)){
            array_push($user_types, ['id' => 'locker', 'text' => 'Locker']);
        } 
        
        if(in_array($group_name, $agent_opt)){
            array_push($user_types, ['id' => 'agent', 'text' => 'Agent']);
        }
        
        if(in_array($group_name, $warung_opt)){
            array_push($user_types, ['id' => 'warung', 'text' => 'Warung']);
        }
        
        $status_verification = [
            ['id' => 'all',         'text' => '- All -'],
            ['id' => 'disable',     'text' => 'Disable'],
            ['id' => 'enable',      'text' => 'Enable'],
            ['id' => 'pending',     'text' => 'Pending'],
            ['id' => 'verified',    'text' => 'Verified'],
        ];
        
        $locker_status = [
            ['id' => 'all',     'text' => '- All -'],
            ['id' => 'offline', 'text' => 'Offline'],
            ['id' => 'online',  'text' => 'Online'],
        ];
        return view('executive_summary.mitra', compact('user_types', 'status_verification', 'locker_status'));
    }
    
    public function list(Request $request){
//         $locker_id = request('locker_id');
        $dateRange = request('daterange_transaction');
        $userType = request('user_type');
        $status = request('status');
        $lockerStatus = request('locker_status');
        
        $city = '';
        $from = '';
        $to = '';
        
        if(!empty($dateRange)) {
            $from = Helper::formatDateRange($dateRange, ',')->startDate;
            $to = Helper::formatDateRange($dateRange, ',')->endDate;
        }
        
        $user_summary = [
            'total_disable_user'    => 0,
            'total_enable_user'     => 0,
            'total_pending_user'    => 0,
            'total_verified_user'   => 0,
            'total_register_user'   => 0,
        ];
        
        $locker_summary = [
            'total_locker'     => 0,
            'total_online'     => 0,
            'total_offline'    => 0,
        ];
        
        $locationsMap = [];
        
        $where_user_type = " and l.type in ('agent', 'warung')";
        $where_location = '';
        $where_user_created_at = '';
        //$where_transaction_created_at = '';
        $status_verification = '';
        $locker_status = '';
        
        if(!empty($city) && $city != 'all'){
            $where_location = "and l.cities_id = ".$city;
        }
        
        if(!empty($userType) && $userType != 'all'){
            $where_user_type = "and l.type = '".$userType."'";
        }
        
        $data = [];
        if($userType != 'locker'){
            if(!empty($from) && !empty($to)){
                //$where_transaction_created_at = "and t.created_at between '".$from."' and '".$to."'";
                $where_user_created_at = "and u.created_at between '".$from."' and '".$to."'";
            }
            
            // ===============================================================================================================================================================
            // ================================================= Get data from agent and virtual dbs.
            // ================================================= User Summary
            // ===============================================================================================================================================================
            $rows = DB::connection('popbox_agent')->select(self::populate_user($where_user_type, $where_location, $where_user_created_at));
            foreach ($rows as $row){
                $user_summary['total_disable_user'] = ($status == 'all' || $status == 'disable') ? $row->total_disable : 0;
                $user_summary['total_enable_user'] = ($status == 'all' || $status == 'enable') ? $row->total_enable : 0;
                $user_summary['total_pending_user'] = ($status == 'all' || $status == 'pending') ? $row->total_pending : 0;
                $user_summary['total_verified_user'] = ($status == 'all' || $status == 'verified') ? $row->total_verified : 0;
                
                $user_summary['total_register_user'] = $user_summary['total_disable_user'] + $user_summary['total_enable_user'] + $user_summary['total_pending_user'] + $user_summary['total_verified_user'];
            }
            
            if(!empty($status) && $status != 'all'){
                $status_verification = "and lk.status_verification = '".$status."'";
            }
            $locationsMap = collect(DB::connection('popbox_agent')->select("
                        select distinct l.locker_id, 
                                        l.locker_name, 
                                        l.latitude, 
                                        l.longitude, 
                                        IFNULL(lk.status_verification, 'disable') as 'status_verification', 
                                        l.type
                        from
                        	users u
                        	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                        	join popbox_virtual.cities c on l.cities_id = c.id
                        	join popbox_agent.lockers lk on lk.id = l.locker_id
                        where
                        	l.`deleted_at` is null $status_verification $where_user_type $where_location $where_user_created_at
                        order by l.locker_name asc"))->map(function($item){
                            
                            return [
                                'locker_id'     => $item->locker_id, 
                                'locker_name'   => $item->locker_name,
                                'latitude'      => $item->latitude,
                                'longitude'     => $item->longitude,
                                'icon_type'     => $item->status_verification."_".$item->type,
                                'item_type'     => "non-locker",
                            ];
                        });
        }
        
        if(!in_array($userType, ['agent', 'warung'])){
            // ===============================================================================================================================================================
            // ===============================================================================================================================================================
            // ================================================= Get data from popbox db
            // ================================================= Locker Summary
            // ===============================================================================================================================================================
            $rows = DB::connection('popbox_db')->select(self::populate_locker());
            foreach ($rows as $row){
                $locker_summary['total_offline'] = ($lockerStatus == 'all' || $lockerStatus == 'offline') ? $row->total_offline : 0;
                $locker_summary['total_online'] = ($lockerStatus == 'all' || $lockerStatus == 'online') ? $row->total_online : 0;
                
                $locker_summary['total_locker'] = $locker_summary['total_offline'] + $locker_summary['total_online'];
            }
            
            if(!empty($lockerStatus) && $lockerStatus != 'all'){
                $lockerStatus = $lockerStatus == 'online' ? 1 : 0;
                $locker_status = "and l.isPublished = '".$lockerStatus."'";
            }
            $lockerLocation = collect(DB::connection('popbox_db')->select("
                        select distinct l.locker_id,
                                        l.name,
                                        l.latitude,
                                        l.longitude,
                                        CASE WHEN l.isPublished = 1 THEN 'online' ELSE 'offline' END as status
                        from
                        	locker_locations l                    
                        where
                        	1=1 $locker_status
                    	order by l.name asc"))->map(function($item){
                    	
                    	return [
                    	    'locker_id'     => $item->locker_id,
                    	    'locker_name'   => $item->name,
                    	    'latitude'      => $item->latitude,
                    	    'longitude'     => $item->longitude,
                    	    'icon_type'     => "locker_".$item->status,
                    	    'item_type'     => "locker",
                    	];
                	});
            
            if(!empty($locationsMap) && (count($locationsMap) > 0)){
                $locationsMap = $locationsMap->merge($lockerLocation);
            } else {
                $locationsMap = $lockerLocation;
            }
        }
        $data['payload']['user_summary'] = $user_summary;
        $data['payload']['locationsMap'] = $locationsMap;
        $data['payload']['locker_summary'] = $locker_summary;
        
        return $data;
    }
    
    private function populate_user($where_user_type, $where_location, $where_user_created_at){
        return "
		select
        (
				select
					count(locker_id) as 'total_disable'
				from (
                    select distinct l.locker_id, l.locker_name , l.type, lk.status_verification, c.city_name
                    from
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where
                    	l.`deleted_at` is null and lk.status_verification = 'disable' ".$where_user_type." ".$where_location." ".$where_user_created_at."
                    order by l.locker_name asc
                ) as temp_total_disable
        ) as 'total_disable',
        (
				select
					count(locker_id) as 'total_enable'
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where
                    	l.`deleted_at` is null and lk.status_verification = 'enable' ".$where_user_type." ".$where_location." ".$where_user_created_at."
                    order by l.locker_name asc
                ) as temp_total_enable
        ) as 'total_enable',
        (
				select
					count(locker_id) as 'total_pending'
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where
                    	l.`deleted_at` is null and lk.status_verification = 'pending' ".$where_user_type." ".$where_location." ".$where_user_created_at."
                    order by l.locker_name asc
                ) as temp_total_pending
        ) as 'total_pending',
        (
				select
					count(locker_id) as 'total_verified'
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where
                    	l.`deleted_at` is null and lk.status_verification = 'verified' ".$where_user_type." ".$where_location." ".$where_user_created_at."
                    order by l.locker_name asc
                ) as temp_total_verified
        ) as 'total_verified'
        ";
    }
    
    private function populate_locker($where_location = ""){
        return "
                select
                (
    				select
    					count(locker_id) as 'total_disable'
    				from (
                        select distinct l.locker_id, l.name
                        from
                        	locker_locations l
                        where 1=1 and l.isPublished = 0 $where_location
                        order by l.name asc
                    ) as temp_offline
                ) as 'total_offline',
                (
    				select
    					count(locker_id) as 'total_disable'
    				from (
                        select distinct l.locker_id, l.name
                        from
                        	locker_locations l
                        where 1=1 and l.isPublished = 1 $where_location
                        order by l.name asc
                    ) as temp_online
                ) as 'total_online'
        ";
    }
}