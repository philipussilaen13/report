<?php

namespace App\Http\Controllers\ExecutiveSummary;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiPopWarung;
use App\Http\Helpers\Helper;
use App\Http\Helpers\SanitizeHelper;
use App\Models\Virtual\City;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TopupDigitalRegistController extends Controller{
    
    public function index(Request $request){
        $user_types = [
            ['id' => 'all',     'text' => 'ALL'],
            ['id' => 'agent',   'text' => 'Agent'],
            ['id' => 'warung',  'text' => 'Warung'],
        ];
        
        $cities = City::select('id as id', 'city_name as text')->orderByRaw('city_name ASC')->get();
        $city = 132;
        return view('executive_summary.topup_digital.list', compact('user_types', 'cities', 'city'));
    }
    
    public function summary(){
        $city = request('city');
        $userType = request('user_type');
        $dateRange = request('daterange_transaction');
        
        $startDate = '';
        $endDate = '';
        
        $where_user_created_at = '';
        $where_transaction_created_at = '';
        $where_user_type = '';
        $where_location = '';
        
        if(!empty($dateRange)) {
            $startDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRange, ',')->endDate;
        }
        
        if(!empty($userType) && $userType != 'all'){
            $where_user_type = "and l.type = '".$userType."'";
        } else {
            $where_user_type = "and l.type in ('agent', 'warung')";
        }
        
        if(!empty($startDate) && !empty($endDate)){
            $where_transaction_created_at = "and t.created_at between '".$startDate."' and '".$endDate."'";
            $where_user_created_at = "and u.created_at between '".$startDate."' and '".$endDate."'";
        }
        
        if(!empty($city) && $city != 'all'){
            $where_location = "and l.cities_id = ".$city;
        }
        
        // Mitra Registration
        $mitra_registration_result = DB::connection('popbox_agent')->select($this->mitra_registration_query($where_user_type, $where_location, $where_transaction_created_at, $where_user_created_at));
        
        // Topup Deposit
        $topup_deposit_result = DB::connection('popbox_agent')->select($this->topup_deposit_query($where_user_type, $where_location, $where_transaction_created_at));
        
        // Digital Product
        $gross_digital_product = DB::connection('popbox_agent')->select($this->gross_digital_product_summarize_query($where_user_type, $where_location, $where_transaction_created_at));
        $nett_digital_product = DB::connection('popbox_agent')->select($this->nett_digital_product_summarize_query($where_user_type, $where_location, $where_transaction_created_at));
        
        // User Summary
        $rows = DB::connection('popbox_agent')->select(self::populate_user($where_location));
        
        $user_summary = [];
        foreach ($rows as $row){
            if($row->userType == 'agent'){
                $user_summary['agent_total_register_user'] = $row->total_register;
                $user_summary['agent_total_disable_user'] = $row->total_disable;
                $user_summary['agent_total_enable_user'] = $row->total_enable;
                $user_summary['agent_total_pending_user'] = $row->total_pending;
                $user_summary['agent_total_verified_user'] = $row->total_verified;
            } else {
                $user_summary['warung_total_register_user'] = $row->total_register;
                $user_summary['warung_total_disable_user'] = $row->total_disable;
                $user_summary['warung_total_enable_user'] = $row->total_enable;
                $user_summary['warung_total_pending_user'] = $row->total_pending;
                $user_summary['warung_total_verified_user'] = $row->total_verified;
            }
        }
        
        $data = [];
        $data['payload']['mitra_registration'] = $mitra_registration_result[0];
        $data['payload']['topup_deposit'] = $topup_deposit_result[0];
        $data['payload']['nett_digital_product'] = $nett_digital_product[0];
        $data['payload']['gross_digital_product'] = $gross_digital_product[0];
        $data['payload']['topup_deposit'] = $topup_deposit_result[0];
        $data['payload']['user_summary'] = $user_summary;
        
        return $data;
    }
    
    public function agent(){
        
        $data = [];
        $data['payload']['data'] = [];
        $data['payload']['count'] = 0;
        
        $params = [
            'where_location'                => '',
            'where_transaction_created_at'  => '',
            'where_user_type'               => '',
            'where_trx_item_type'           => '',
            
        ];
        $city = request('city');
        $dateRange = request('daterange_transaction');
        $draw = request('draw', 1);
        $userType = request('user_type');
        
        $startDate = '';
        $endDate = '';
        
        if(!empty($dateRange)) {
            $startDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRange, ',')->endDate;
        }
        
        if(!empty($userType)){
            if($userType == 'warung'){
                return $data;
            } else {
                $params['where_user_type'] = "and l.type = 'agent'";
            }
        }
        
        if(!empty($startDate) && !empty($endDate)){
            $params['where_transaction_created_at'] = "and t.created_at between '".$startDate."' and '".$endDate."'";
        }
        
        if(!empty($city) && $city != 'all'){
            $params['where_location'] = "and l.cities_id = ".$city;
        }
        
        // Top 10 Performing Agent
        $params['group_by'] = "group by l.id, l.locker_id, l.locker_name, ti.type";
        $rows_transaction_product_agent = DB::connection('popbox_agent')->select($this->transaction_product_summary($params));
        $transaction_product_agent = SanitizeHelper::addCounterNumber($rows_transaction_product_agent, 0);
        
        $data['payload']['data'] = $transaction_product_agent;
        $data['payload']['draw'] = $draw;
        $data['payload']['count'] = count($data['payload']['data']);
        
        return $data;
    }
    
    public function popwarung(){
        $params = [
            'where_location'                => '',
            'where_transaction_created_at'  => '',
            'where_user_type'               => '',
            'where_trx_item_type'           => '',
        ];
        
        $data = [];
        $data['payload']['data'] = [];
        $data['payload']['count'] = 0;
        
        $city = request('city');
        $dateRange = request('daterange_transaction');
        $draw = request('draw', 1);
        $userType = request('user_type');
        
        $startDate = '';
        $endDate = '';
        
        if(!empty($dateRange)) {
            $startDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRange, ',')->endDate;
        }
        
        if(!empty($userType)){
            if($userType == 'agent'){
                return $data;
            } else {
                $params['where_user_type'] = "and l.type = 'warung'";
            }
        }
        
        if(!empty($startDate) && !empty($endDate)){
            $params['where_transaction_created_at'] = "and t.created_at between '".$startDate."' and '".$endDate."'";
        }
        
        if(!empty($city) && $city != 'all'){
            $params['where_location'] = "and l.cities_id = ".$city;
        }
        // Top 10 Performing PopWarung
        $params['group_by'] = "group by l.id, l.locker_id, l.locker_name, ti.type";
        $rows_transaction_product_warung =  DB::connection('popbox_agent')->select($this->transaction_product_summary($params));
        $transaction_product_warung = SanitizeHelper::addCounterNumber($rows_transaction_product_warung, 0);
        
        $data['payload']['data'] = $transaction_product_warung;
        $data['payload']['draw'] = $draw;
        $data['payload']['count'] = count($data['payload']['data']);
        
        return $data;
    }
    
    private function transaction_product_summary($params){
        return "select
                    id, 
                    locker_id, 
                    locker_name, 
                    sum(total_transaction_digital) as 'total_transaction_digital',
                    sum(total_transaction_non_digital) as 'total_transaction_non_digital',
                    sum(total_transaction_digital) + sum(total_transaction_non_digital) as 'total_both_type_transaction'
                from (
                    select 
                        id, 
                        locker_id, 
                        locker_name, 
                        (case when type_prod = 'digital' then sum(total_transaction_digital) else 0 END) as 'total_transaction_digital',
                        sum(total_transaction_non_digital) as 'total_transaction_non_digital'
                    from (
                        select distinct
                           l.id,
                           l.locker_id,
                           l.locker_name,      
                           t.reference,
                           (case when ti.type != 'popshop' then 'digital' else 'non_digital' end)  as 'type_prod',
                           (case when ti.type in ('pulsa', 'electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'telkom_postpaid', 'pdam', 'admin_fee') then t.total_price else 0 end)  as 'total_transaction_digital'
                           ,(case when ti.type = 'popshop' then t.total_price else 0 end)  as 'total_transaction_non_digital'
                        from
                            transactions t
                        left join transaction_items ti on t.id = ti.transaction_id
                        left join users u on t.users_id = u.id
                        left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                        where 1=1
                                and t.deleted_at is null
                                and t.status in ('WAITING','PAID')
                                ".$params['where_trx_item_type']."
                                ".$params['where_user_type']."
                                ".$params['where_transaction_created_at']."
                                ".$params['where_location']."
                    ) as temp
                    group by temp.type_prod,
                        temp.id, 
                        temp.locker_id, 
                        temp.locker_name
                ) as tmp
                group by
                        tmp.id, 
                        tmp.locker_id, 
                        tmp.locker_name
                order by 
                        sum(total_transaction_digital) + sum(total_transaction_non_digital) desc
                limit 0, 10 ";
    }
    
    private function topup_deposit_query($where_user_type, $where_location, $where_transaction_created_at){
        return "select *
                from (
                        select (
                            select sum(t.total_price)
                            from transaction_items ti
                            join transactions t on t.id = ti.transaction_id
                            join users u on u.id = t.users_id
                            left join popbox_virtual.lockers l on u.`locker_id` = l.`locker_id`
                            where 1=1
                            -- and t.status IN ('PAID', 'EXPIRED', 'UNPAID')
                            and upper(ti.type) = upper('topup')
                            ".$where_user_type." ".$where_location."
                            ".$where_transaction_created_at."
                        ) as 'gross_amount',
                        (
                            select sum(t.total_price)
                            from transaction_items ti
                            join transactions t on t.id = ti.transaction_id
                            join users u on u.id = t.users_id
                            left join popbox_virtual.lockers l on u.`locker_id` = l.`locker_id`
                            where 1=1
                            and t.status = 'PAID'
                            and upper(ti.type) = upper('topup')
                            ".$where_user_type." ".$where_location."
                            ".$where_transaction_created_at."
                        ) as 'net_amount',
                        (
                            select count(t.id)
                            from transaction_items ti
                            join transactions t on t.id = ti.transaction_id
                            join users u on u.id = t.users_id
                            left join popbox_virtual.lockers l on u.`locker_id` = l.`locker_id`
                            where 1=1
                            and t.status = 'PAID'
                            and upper(ti.type) = upper('topup')
                            ".$where_user_type." ".$where_location."
                            ".$where_transaction_created_at."
                        ) as 'successful_topup_attempt',
                        (
                            select count(distinct t.users_id)
                            from transactions t
                            join transaction_items ti on t.id = ti.transaction_id
                            join users u on u.id = t.users_id
                            left join popbox_virtual.lockers l on u.`locker_id` = l.`locker_id`
                            where 1=1
                            and upper(ti.type) = upper('topup')
                            ".$where_user_type." ".$where_location."
                            ".$where_transaction_created_at."
                        ) as 'unique_user'
                ) as temp;";
    }
    
    private function gross_digital_product_summarize_query($where_user_type, $where_location, $where_transaction_created_at){
        return "select *
                from (
                        select (
                                select sum(`total_price`)  as 'total_order_transaction_digital' 
                                from (
                    				select distinct 
                    					l.locker_id,
                                   		l.locker_name, 
                                   		t.reference,
                                   		t.total_price
                                	from
                                    	transactions t
                                	left join transaction_items ti on t.id = ti.transaction_id
                                	left join users u on t.users_id = u.id
                                	left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                                	where 1=1
                                      	and t.deleted_at is null
                                        and l.type in ('agent', 'warung')
                                        and ti.type in ('pulsa', 'electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'telkom_postpaid', 'pdam', 'admin_fee') 
                                        ".$where_user_type." ".$where_location."
                                        ".$where_transaction_created_at."
                           	    ) as temp
                        ) as 'total_transaction_digital',
                        (
                            select
                                count(distinct t.id) as 'total_order_transaction_digital'
                            from
                                transactions t
                                left join transaction_items ti on t.id = ti.transaction_id
                                left join users u on t.users_id = u.id
                                left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                                where 1=1
                                    and t.deleted_at is null
                                    and l.type in ('agent', 'warung')
                                    and ti.type in ('pulsa', 'electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'telkom_postpaid', 'pdam', 'admin_fee') 
                                    ".$where_user_type." ".$where_location."
                                    ".$where_transaction_created_at."
                        ) as 'total_order_transaction_digital',
                        (
                            select
                                count(distinct t.users_id) as 'total_order_transaction_digital'
                            from
                                transactions t
                                left join transaction_items ti on t.id = ti.transaction_id
                                left join users u on t.users_id = u.id
                                left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                                where 1=1
                                    and t.deleted_at is null
                                    and l.type in ('agent', 'warung')
                                    and ti.type in ('pulsa', 'electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'telkom_postpaid', 'pdam', 'admin_fee') 
                                    ".$where_user_type." ".$where_location."
                                    ".$where_transaction_created_at."
                        ) as 'unique_user_order_transaction_digital'
                ) as temp;";
    }
    
    private function nett_digital_product_summarize_query($where_user_type, $where_location, $where_transaction_created_at){
        return "select *
                from (
                        select (
                                select sum(`total_price`)  as 'total_order_transaction_digital' 
                                from (
                    				select distinct 
                    					l.locker_id,
                                   		l.locker_name, 
                                   		t.reference,
                                   		t.total_price
                                	from
                                    	transactions t
                                	left join transaction_items ti on t.id = ti.transaction_id
                                	left join users u on t.users_id = u.id
                                	left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                                	where 1=1
                                      	and t.deleted_at is null
                                        and ti.type in ('pulsa', 'electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'telkom_postpaid', 'pdam', 'admin_fee') 
                                        and t.status = 'PAID'
                                        ".$where_user_type." ".$where_location."
                                        ".$where_transaction_created_at."
                               	) as temp
                        ) as 'total_transaction_digital',
                        (
                            select
                                count(distinct t.id) as 'total_order_transaction_digital'
                            from
                                transactions t
                                left join transaction_items ti on t.id = ti.transaction_id
                                left join users u on t.users_id = u.id
                                left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                                where 1=1
                                    and t.deleted_at is null
                                    and ti.type in ('pulsa', 'electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'telkom_postpaid', 'pdam', 'admin_fee')  
                                    and t.status = 'PAID'
                                    ".$where_user_type." ".$where_location."
                                    ".$where_transaction_created_at."
                        ) as 'total_order_transaction_digital',
                        (
                            select
                                count(distinct t.users_id) as 'total_order_transaction_digital'
                            from
                                transactions t
                                left join transaction_items ti on t.id = ti.transaction_id
                                left join users u on t.users_id = u.id
                                left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                                where 1=1
                                    and t.deleted_at is null
                                    and ti.type in ('pulsa', 'electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'telkom_postpaid', 'pdam', 'admin_fee')  
                                    and t.status = 'PAID'
                                    ".$where_user_type." ".$where_location."
                                    ".$where_transaction_created_at."
                        ) as 'unique_user_order_transaction_digital'
                ) as temp;";
    }
    
    private function mitra_registration_query($where_user_type, $where_location, $where_transaction_created_at, $where_user_created_at){
        return "select *, total_verified_user - total_active_user as total_inactive_user
                from (
                        select (
                                select count(u.id)
                                from users u
                                join popbox_virtual.lockers l on u.`locker_id` = l.`locker_id`
                                join popbox_agent.`lockers` lk on lk.id = l.`locker_id`
                                where u.`deleted_at` is null ".$where_user_created_at." ".$where_user_type." ".$where_location."
                        ) as total_registered_user,
                        (
                                select count(u.id)
                                from users u
                                left join popbox_virtual.lockers l on u.`locker_id` = l.`locker_id`
                                left join popbox_agent.`lockers` lk on lk.id = l.`locker_id`
                                where l.`deleted_at` is null and lk.status_verification = 'verified' ".$where_user_created_at." ".$where_user_type." ".$where_location."
                        ) as total_verified_user,
                        (
                                select count(distinct t.users_id)
                                from users u
                                join popbox_agent.`transactions` t on t.`users_id` = u.id
                                left join popbox_virtual.lockers l on u.`locker_id` = l.`locker_id`
                                left join popbox_agent.`lockers` lk on lk.id = u.`locker_id`
                                where l.`deleted_at` is null and lk.status_verification = 'verified' ".$where_user_created_at." ".$where_user_type." ".$where_location."
                                ".$where_transaction_created_at."
                        ) as total_active_user
                ) as temp;";
    }
    
    private function populate_non_digital_product_from_api($digital_product_warung, $startDate, $endDate, $start = '', $limit = '', $tglUpdate = ''){
        
        $arr_locker_id = [];
        foreach ($digital_product_warung as $product){
            array_push($arr_locker_id, $product['locker_id']);
        }
        
        $tglUpdate = 'all';
        $params = [
            'lockerid'  => $arr_locker_id,
            'startDate' => $startDate,
            'endDate'   => $endDate,
            'tglUpdate' => $tglUpdate,
            'start'     => $start,
            'limit'     => $limit,
        ];
        $url = config('constant.popwarung.api_url').'product/gettransactiondetailsbyparams';
        $result = ApiPopWarung::callAPI('GET', $url, $params);
        
        return (array) json_decode($result, true)['payload']['data'];
        
    }
    
    private function populate_user($where_location){
        return "
		select (select 'agent' as 'userType') as 'userType', 
		(
				select 
					count(locker_id) as 'total_register' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and l.type = 'agent' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_register
        ) as 'total_register',
        (
				select 
					count(locker_id) as 'total_disable' 
				from (
                    select distinct l.locker_id, l.locker_name , l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'disable' and l.type = 'agent' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_disable
        ) as 'total_disable',
        (
				select 
					count(locker_id) as 'total_enable' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'enable' and l.type = 'agent' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_enable
        ) as 'total_enable',
        (
				select 
					count(locker_id) as 'total_pending' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'pending' and l.type = 'agent' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_pending
        ) as 'total_pending',
        (
				select 
					count(locker_id) as 'total_verified' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'verified' and l.type = 'agent' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_verified
        ) as 'total_verified'
        
        union
        
        select (select 'warung' as 'userType') as 'userType', 
		(
				select 
					count(locker_id) as 'total_register' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and l.type = 'warung' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_register
        ) as 'total_register',
        (
				select 
					count(locker_id) as 'total_disable' 
				from (
                    select distinct l.locker_id, l.locker_name , l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'disable' and l.type = 'warung' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_disable
        ) as 'total_disable',
        (
				select 
					count(locker_id) as 'total_enable' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'enable' and l.type = 'warung' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_enable
        ) as 'total_enable',
        (
				select 
					count(locker_id) as 'total_pending' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'pending' and l.type = 'warung' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_pending
        ) as 'total_pending',
        (
				select 
					count(locker_id) as 'total_verified' 
				from (
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null and lk.status_verification = 'verified' and l.type = 'warung' ".$where_location."
                    order by l.locker_name asc
                ) as temp_total_verified
        ) as 'total_verified'
        ";
    }
    
}