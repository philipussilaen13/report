<?php

namespace App\Http\Controllers\Warung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiPopWarung;

class CategoryController extends Controller
{
    /**
     * get agent landing dashboard
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(Request $request)
    {
        return view('warung.catalog.category.index', []);
    }

    public function getListCategory(Request $request)
    {
        $categoryid = $request->input('categoryid','all');
        $level = (int) $request->input('level','0');

        $url = config('constant.popwarung.api_url').'category/getlistcategories/'.$categoryid.'/'.$level;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function crudCategory(Request $request)
    {
        $flag = $request->input('flag','');
        $categoryid = $request->input('categoryid','');
        $params = [
            'categoryid' => $categoryid,
            'name' => $request->input('category',''),
            'level' => $request->input('level',''),
            'prefixcode' => $request->input('prefixcode',''),
        ];

        if(in_array($flag,['1','2','3'])) {
            $url = config('constant.popwarung.api_url').'category/addcategory';
        }
        else {
            unset($params['level']);
            $url = config('constant.popwarung.api_url').'category/updcategory';
        }

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));

        return $mresult;
    }

    public function delCategory(Request $request)
    {
        $params = [
            'categoryid' => $request->input('categoryid', '')
        ];

        $url = config('constant.popwarung.api_url').'category/delcategory';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));

        return $mresult;
    }

    public function getListCategoryMobile(Request $request)
    {
        $url = config('constant.popwarung.api_url').'category/getlistcategoriesmobile';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }
}
