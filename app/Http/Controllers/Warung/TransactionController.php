<?php

namespace App\Http\Controllers\Warung;

use App\Http\Helpers\ApiPopWarung;
use App\Models\Agent\Transaction;
use App\Http\Helpers\Helper;
use App\Models\Payment\ClientTransaction;
use App\Models\Virtual\Locker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class TransactionController extends Controller
{
    public $locker_id;
    public $transactionDate;
    public $fileNameTransactionDigital;
    public $fileNameTransactionNonDigital;
    
    public function getTransactionWarung(Request $request)
    {
        $url = config('constant.popwarung.api_url').'transaction/gettransactiontypes';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        
        $transaction_types = [
            ['id' => 'all', 'text' => '-- Semua --'],
        ];
        
        foreach((array) json_decode($mresult, true)['payload']['data']['data'] as $data){
            if($data['type'] !== 'transaction_manual') {
                $transaction_types[] = [
                    'id'    => $data['type'],
                    'text'  => ucwords(str_replace('_', ' ', $data['type'])),
                ];
            }
        }
        return view('warung.transaction.warung', compact('transaction_types'));
    }
    
    public function getListTransactionWarung(Request $request) {
        return $this->callApi($request);
    }
    
    public function detailTransactionWarung(Request $request, $id, $lockerid)
    {
        $lockerDb = Locker::where('locker_id', $lockerid)->first();
        if (!$lockerDb) {
            $request->session()->flash('error', 'Locker ID not found');
            return back();
        }
        
        $data = array();
        $data['transactionid'] = $id;
        $data['locker'] = $lockerDb;
        return view('warung.transaction.detailwarung', $data);
    }
    
    public function getDetailTransactionWarung(Request $request)
    {
        $transactionId = $request->input('transactionid', '');
        
        $rows = $this->callApiDetailTransactionWarung($transactionId);
        
        return json_encode($rows);
    }
    
    private function callApiDetailTransactionWarung($transactionId){
        
        $url = config('constant.popwarung.api_url').'transaction/getdetailtransactioncustomer/'.$transactionId;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        
        $rows = json_decode($mresult, true);
        
        $is_popboxWallet = $rows['payload']['paymentMethod'] == 'popbox_wallet';
        
        if(!$is_popboxWallet){
            foreach ($rows['payload']['data'] as $item){
                if($item['type'] == 'self_service'){
                    $is_popboxWallet = true;
                    break;
                }
            }
        }
        
        $rows['payload']['is_popbox_wallet'] = $is_popboxWallet;
        
        if(strtoupper($rows['payload']['paymentMethod']) === strtoupper('OTTOPay') && (strpos($rows['payload']['payment_id'], 'OTTO-QR') !== false)){
            
            $response = $this->detailClientTransactionByPaymentId($rows['payload']['payment_id']);
            
            $rows['payload']['orderId'] = $response['orderId'];
            $rows['payload']['transactionId'] = $response['transactionId'];
        }
        
        return $rows;
    }
    
    private function detailClientTransactionByPaymentId($payment_id){
        
        $result =  ClientTransaction::leftJoin('popbox_payment.ottopay_transactions', 'popbox_payment.ottopay_transactions.client_transactions_id', '=', 'popbox_payment.client_transactions.id')
        ->where('popbox_payment.client_transactions.payment_id', '=', $payment_id)
        ->select('popbox_payment.ottopay_transactions.param_response')
        ->get();
        
        $response = [];
        if(sizeof($result) == 1){
            $p_response = $result[0]->param_response;
            $response = json_decode($p_response, true);
        }
        
        return $response;
    }
    
    public function getListMasterWarung(Request $request) {
        $keyword = $request->input('search','');
        
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        
        $lockerDb = Locker::leftJoin('popbox_virtual.cities','popbox_virtual.lockers.cities_id','=','popbox_virtual.cities.id')
        ->leftJoin('popbox_virtual.regions','popbox_virtual.cities.regions_id','=','popbox_virtual.regions.id')
        ->where('status', '<>', '0')
        ->where('type', '!=', 'locker')
        ->where("locker_name", 'like', '%'.strtoupper($keyword).'%')
        ->orWhere('locker_id', 'like', '%'.strtoupper($keyword).'%')
        ->select('popbox_virtual.lockers.locker_id', 'popbox_virtual.lockers.locker_name', 'popbox_virtual.regions.region_code')
        ->get();
        
        $data = [];
        $data['lockerData'] = $lockerDb;
        
        $response->data = $data;
        
        $response->isSuccess = true;
        return response()->json($response);
    }
    
    public function downloadFile(Request $request){
        
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $lockerID = $request->input('id_warung', 'all');
        $lockerID = empty($lockerID) ? 'all' : $lockerID;
        
        $dateRange = $request->input('daterange_transaction', null);
        
        if (!empty($dateRange)) {
            $startDate = substr(Helper::formatDateRange($dateRange, ',')->startDate, 0, 10);
            $endDate = substr(Helper::formatDateRange($dateRange, ',')->endDate, 0, 10);
        }
        
        $transaction_id = $request->input('transactionid', '');
        $transaction_id = empty($transaction_id) ? 'all' : $transaction_id;
        
        $transaction_type = $request->input('transactiontype', '');
        $transaction_type = empty($transaction_type) ? 'all' : $transaction_type;
        
        $tglUpdate = 'all';
        $payload = self::getData($lockerID, $startDate, $endDate, $tglUpdate, $transaction_id, $transaction_type);
        
        $filename = "$startDate s/d $endDate Warung Transaction";
        $response = Helper::rendering_excel($filename, $payload, self::non_digital_headers());
        
        return response()->json($response);
    }
    
    public function getData($lockerID, $startDate, $endDate, $tglUpdate, $transaction_id, $transaction_type){
        
        $payload = [];
        try{
            
            $params = [
                'iswarung'              => 0,
                'lockerid'              => $lockerID,
                'startdate'             => $startDate,
                'enddate'               => $endDate,
                'tglupdate'             => $tglUpdate,
                'transactionid'         => $transaction_id,
                'transactiontype'       => $transaction_type,
            ];
            
            $url = config('constant.popwarung.api_url').'product/gettransactiondetailsbyparams';
            $mresult = ApiPopWarung::callAPI('GET', $url, $params);
            
            $mresult = (array) json_decode($mresult, true);
            foreach($mresult['payload']['data'] as $index => $item){
                $payment_id = ( isset($item['payment_id']) && strtoupper((string)$item['payment_type']) == strtoupper('OTTOPay') ) ? (string) $item['payment_id'] : '-';
                $ottopay_ref = '-';
                if(strtoupper((string)$item['payment_type']) == strtoupper('OTTOPay') && isset($item['payment_id']) && !empty($item['payment_id'])){
                    $response = Helper::detailClientTransactionByPaymentId((string) $item['payment_id']);
                    
                    $ottopay_ref = (sizeof($response) > 0) ? (string) $response['transactionId'] : $ottopay_ref;
                }
                
                $payload[] = [
                    'no'                => $index+1,
                    'date_transaction'  => date('d-m-Y', strtotime($item['date_transaction'])),
                    'time_transaction'  => date('H:i:s', strtotime($item['date_transaction'])),
                    'reference'         => $item['reference'],
                    'locker_name'       => Helper::findLocker($item['locker_id']),
                    'locker_id'         => $item['locker_id'],
                    'payment_type'      => strtoupper($item['payment_type']),
                    'payment_id'        => strtoupper($payment_id),
                    'ottopay_ref'       => $ottopay_ref,
                    'trx_status'        => strtoupper($item['status']) === strtoupper('Terbayar') ? 'Success' : 'Void',
                    'description'       => $item['description'],
                    'sku'               => $item['sku'],
                    'product_code'      => $item['product_code'],
                    'product_name'      => $item['product_name'],
                    'selling_price'     => isset($item['selling_price']) ? $item['selling_price'] : 0,
                    'qty'               => $item['qty'],
                    'price'             => $item['price'],
                    'total_trx'         => $item['total_price'],
                    'total_cost'        => $item['selling_price'] * $item['qty'],
                    'profit'            => $item['total_price'] - ($item['selling_price'] * $item['qty']),
                    'customer_name'     => ($item['payment_type'] == 'PopBox Wallet') ? $item['customer_name'] : '-',
                    'customer_phone'    => ($item['payment_type'] == 'PopBox Wallet') ? $item['users_name'] : '-',
                    'customer_email'    => ($item['payment_type'] == 'PopBox Wallet') ? $item['customer_email'] : '-',
                    
                ];
            }
        } catch (\Exception $e){
            Log::error($e->getMessage());
            Log::error($e->getTrace());
        }
        
        return $payload;
    }
    
    public static function non_digital_headers(){
        
        $header = [
            ["column"  => "A", "name" => "No",                     "field" => "no",                 'align' => 'right'],
            ["column"  => "B", "name" => "Transaction Date",       "field" => "date_transaction",   'align' => 'left'],
            ["column"  => "C", "name" => "Transaction Time",       "field" => "time_transaction",   'align' => 'left'],
            ["column"  => "D", "name" => "Transaction ID",         "field" => "reference",          'align' => 'left'],
            ["column"  => "E", "name" => "Warung Name",            "field" => "locker_name",        'align' => 'left'],
            ["column"  => "F", "name" => "Warung/Locker ID",       "field" => "locker_id",          'align' => 'left'],
            ["column"  => "G", "name" => "Payment Type",           "field" => "payment_type",       'align' => 'left'],
            ["column"  => "H", "name" => "PB Trx Ref",             "field" => "payment_id",         'align' => 'left'],
            ["column"  => "I", "name" => "OTTOPay Trx Ref",        "field" => "ottopay_ref",        'align' => 'left'],
            ["column"  => "J", "name" => "Transaction Status",     "field" => "trx_status",         'align' => 'left'],
            ["column"  => "K", "name" => "Void Reason",            "field" => "description",        'align' => 'left'],
            ["column"  => "L", "name" => "SKU",                    "field" => "sku",                'align' => 'left',  'data_type' => 'type_to_string'],
            ["column"  => "M", "name" => "Product Code",           "field" => "product_code",       'align' => 'left',  'data_type' => 'type_to_string'],
            ["column"  => "N", "name" => "Product Name",           "field" => "product_name",       'align' => 'left',  'data_type' => 'type_to_string'],
            ["column"  => "O", "name" => "Purchase Price",         "field" => "selling_price",      'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "P", "name" => "Selling Qty",            "field" => "qty",                'align' => 'right'],
            ["column"  => "Q", "name" => "Selling Price Per Item", "field" => "price",              'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "R", "name" => "Total Selling Price",    "field" => "total_trx",          'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "S", "name" => "Total Cost",             "field" => "total_cost",         'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "T", "name" => "Sales Profit",           "field" => "profit",             'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "U", "name" => "Customer Name",          "field" => "customer_name",      'align' => 'left'],
            ["column"  => "V", "name" => "Customer Phone",         "field" => "customer_phone",     'align' => 'left'],
            ["column"  => "W", "name" => "Customer Email",         "field" => "customer_email",     'align' => 'left'],
        ];
        
        return $header;
    }
    
    public function callApi($request){
        $userid = $request->input('userid', 'all');
        $userid = empty($userid) ? 'all' : $userid;
        
        $dateRange = $request->input('daterange_transaction', null);
        
        if (!empty($dateRange)) {
            $startDate = substr(Helper::formatDateRange($dateRange, ',')->startDate, 0, 10);
            $endDate = substr(Helper::formatDateRange($dateRange, ',')->endDate, 0, 10);
        }
        
        $transaction_id = $request->input('transactionid', '');
        $transaction_id = empty($transaction_id) ? 'all' : $transaction_id;
        
        $transaction_type = $request->input('transactiontype', '');
        $transaction_type = empty($transaction_type) ? 'all' : $transaction_type;
        
        $tglUpdate = 'all';
        
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 25);
        
        $url = config('constant.popwarung.api_url').'transaction/getlisttransactioncustomer/'.$userid.'/'.$startDate.'/'.$endDate.'/'.$tglUpdate.'/'.$transaction_id.'/'.$transaction_type.'/'.$start.'/'.$limit;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        
        // $mresult is string value and need to parse to json object
        $data = array();
        $rows = (array) json_decode($mresult, true);
        
        $data['payload']['count'] = $rows['payload']['count'];
        $data['payload']['data'] = [];
        foreach($rows['payload']['data'] as $row){
            $row['locker_name'] = Helper::findLocker($row['locker_id']);
            array_push($data['payload']['data'], $row);
        }
        return $data;
    }
    
    function DailyTransactionDigital()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $mresult = Transaction::leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
        ->leftJoin('users', 'transactions.users_id', '=', 'users.id')
        ->leftJoin('popbox_virtual.lockers', 'users.locker_id', '=', 'popbox_virtual.lockers.locker_id')
        ->whereNull('transactions.deleted_at')
        ->whereNotIn('transaction_items.type', ['popshop'])
        ->whereRaw("date(transactions.created_at) = str_to_date(?,'%Y-%m-%d')", [$this->transactionDate])
        ->where('popbox_virtual.lockers.locker_id', '=', $this->locker_id)
        ->select(
            'transactions.id', 'transactions.reference', 'transactions.created_at', 'transactions.status', 'users.locker_id',
            'transaction_items.name', 'transaction_items.params',
            'popbox_virtual.lockers.locker_name', 'transaction_items.type', 'transaction_items.price', 'transactions.total_price'
            )
            ->get();
            
            $myFile = Excel::create($this->fileNameTransactionDigital, function ($excel) use ($mresult) {
                $excel->setTitle('Transaction');
                $excel->setCreator('PopBox')->setCompany('PopBox');
                $excel->setDescription('transaction file');
                $excel->sheet('transaction', function ($sheet) use ($mresult) {
                    
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('No');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('Transaction ID');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('Transaction Date');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('Transaction Time');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('Status');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('Warung Name');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('Product Type');
                    });
                    $sheet->cell('H1', function ($cell) {
                        $cell->setValue('Items');
                    });
                    $sheet->cell('I1', function ($cell) {
                        $cell->setValue('Qty');
                    });
                    $sheet->cell('J1', function ($cell) {
                        $cell->setValue('Product Price');
                    });
                    $sheet->cell('K1', function ($cell) {
                        $cell->setValue('Total Transaction');
                    });
                        
                    $cellNumberStart = 2;
                    
                    foreach($mresult as $item){
                        $sheet->cell("A$cellNumberStart", function ($cell) use ($cellNumberStart, $item) {
                            $cell->setValue($cellNumberStart-1);
                        });
                            
                        $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->reference)->setAlignment('center');
                        });
                            
                        $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue(date('d-m-Y', strtotime($item->created_at)))->setAlignment('center');
                        });
                            
                        $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue(date('H:i:s', strtotime($item->created_at)))->setAlignment('center');
                        });
                            
                        $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->status)->setAlignment('center');
                        });
                            
                        $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->locker_name);
                        });
                            
                        $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->type)->setAlignment('center');
                        });
                            
                        $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->name);
                        });
                            
                        $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue('1')->setAlignment('center');
                        });
                            
                        $sheet->cell("J$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue("Rp " . number_format($item->price))->setAlignment('right');
                        });
                            
                        $sheet->cell("K$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue("Rp " . number_format($item->total_price))->setAlignment('right');
                        });
                            
                        $cellNumberStart++;
                    }
                });
                    
            })->store('xls', storage_path('exports'));
            return $myFile;
    }
    
    function DailyTransactionNonDigital()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $userid = $this->locker_id;
        $startDate = $this->transactionDate;
        $endDate = $this->transactionDate;
        $transaction_id = 'all';
        $transaction_type = 'all';
        $tglUpdate = 'all';
        
        $url = config('constant.popwarung.api_url').'product/gettransactiondetailbyparams/'.$userid.'/'.$startDate.'/'.$endDate.'/'.$tglUpdate.'/'.$transaction_id.'/'.$transaction_type;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        
        $payload = (array) json_decode($mresult, true);
        
        $myFile = Excel::create($this->fileNameTransactionNonDigital, function ($excel) use ($payload) {
            $excel->setTitle('Transaction');
            $excel->setCreator('Dashboard Warung')->setCompany('PopBox Agent');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($payload) {
                
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Transaction Date');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Transaction Time');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Transaction ID');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Warung Name');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Warung/Locker ID');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Payment Type');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('SKU');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Product Code');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Product Name');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Qty');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Price per Item');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Total Price');
                });
                $sheet->cell('N1', function ($cell) {
                    $cell->setValue('COGS');
                });
                $sheet->cell('O1', function ($cell) {
                    $cell->setValue('BSMNU');
                });
                $sheet->cell('P1', function ($cell) {
                    $cell->setValue('KIMONU');
                });
                    
                $cellNumberStart = 2;
                
                foreach($payload['payload']['data'] as $item){
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($cellNumberStart, $item) {
                        $cell->setValue($cellNumberStart-1);
                    });
                        
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue(date('d-m-Y', strtotime($item['date_transaction'])));
                    });
                        
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue(date('H:i:s', strtotime($item['date_transaction'])));
                    });
                        
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['reference']);
                    });
                        
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue((string)Helper::findLocker($item['locker_id']));
                    });
                    $sheet->getStyle("E$cellNumberStart")->getAlignment()->setWrapText(true);
                    
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['locker_id']);
                    });
                        
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                        $value = (string) $item['payment_type'];
                        $cell->setValue(strtoupper($value));
                    });
                        
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue((string)$item['sku'])->setAlignment('center');
                    });
                    $sheet->setColumnFormat(["H$cellNumberStart" => '0']);
                    
                    $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['product_code'])->setAlignment('center');
                    });
                    $sheet->setColumnFormat(["I$cellNumberStart" => '0']);
                    
                    $sheet->cell("J$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['product_name']);
                    });
                        
                    $sheet->cell("K$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['qty']);
                    });
                        
                    $sheet->cell("L$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['price']);
                    });
                    $sheet->setColumnFormat(["L$cellNumberStart" => "Rp #,##0.00_-"]);
                    
                    $sheet->cell("M$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['total_price']);
                    });
                    $sheet->setColumnFormat(["M$cellNumberStart" => "Rp #,##0.00_-"]);
                    
                    $sheet->cell("N$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['cogs_price']);
                    });
                    $sheet->setColumnFormat(["N$cellNumberStart" => "Rp #,##0.00_-"]);
                    
                    $sheet->cell("O$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['profit_bsmnu']);
                    });
                    $sheet->setColumnFormat(["O$cellNumberStart" => "Rp #,##0.00_-"]);
                    
                    $sheet->cell("P$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['profit_kimonu']);
                    });
                    $sheet->setColumnFormat(["P$cellNumberStart" => "Rp #,##0.00_-"]);
                    
                    $cellNumberStart++;
                }
            });
                
        })->store('xls', storage_path('exports'));
        return $myFile;
    }
}
