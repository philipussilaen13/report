<?php

namespace App\Http\Controllers\Warung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiPopWarung;

class UOMController extends Controller
{
    /**
     * get warung landing dashboard
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function uom(Request $request)
    {
        return view('warung.catalog.uom.uom.index');
    }

    public function getListConvertionUOM(Request $request)
    {
        $url = config('constant.popwarung.api_url').'product/getlistconvertuom';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function crudConvertionUOM(Request $request)
    {
        $flag = $request->input('flag', '1');
        $uomConvertionID = $request->input('uomconvertionid', null);
        $convertionName = $request->input('convertionname', null);
        $uomSourceID = $request->input('uomsourceid', null);
        $uomTargetID = $request->input('uomtargetid', null);
        $targeType = $request->input('targetype', null);
        $valueRate = $request->input('valuerate', null);
        $productID = $request->input('productid', null);
        $productExp = explode(',', $productID);

        $multiplyRate = null; $divideRate = null;
        if($targeType == '1') {
            $multiplyRate = $valueRate;
        }
        else if($targeType == '2') {
            $divideRate = $valueRate;
        }

        $params = [
            'convertuomid' => $uomConvertionID,
            'name' => $convertionName,
            'uomsource' => $uomSourceID,
            'uomtarget' => $uomTargetID,
            'multiplyrate' => $multiplyRate,
            'dividerate' => $divideRate,
            'productid' => $productExp
        ];

        if($flag == '1') {
            unset($params['convertuomid']);
            $url = config('constant.popwarung.api_url').'product/addconvertuom';
        }
        else {
            $url = config('constant.popwarung.api_url').'product/updconvertuom';
        }

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        return $mresult;
    }

    public function delConvertionUOM(Request $request)
    {
        $params = [
            'convertuomid' => $request->input('id', '')
        ];

        $url = config('constant.popwarung.api_url').'product/delconvertuom';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));

        return $mresult;
    }

    public function getListUOM(Request $request)
    {
        $url = config('constant.popwarung.api_url').'product/getlistproductuom';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function getConvertionUOMByID(Request $request, $uomconvertionid)
    {
        $url = config('constant.popwarung.api_url').'product/getconvertuombyid/' . $uomconvertionid;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function getProductByConvertationUOM(Request $request, $uomconvertionid)
    {
        $url = config('constant.popwarung.api_url').'product/getlistproductbyconvertion/' . $uomconvertionid;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function crudUOM(Request $request)
    {
        $flag = $request->input('flag', '1');
        $params = [
            'productuomid' => $request->input('uomid', ''),
            'name' => $request->input('uom', '')
        ];

        if($flag == '1') {
            unset($params['productuomid']);
            $url = config('constant.popwarung.api_url').'product/addproductuom';
        }
        else {
            $url = config('constant.popwarung.api_url').'product/updproductuom';
        }

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        return $mresult;
    }

    public function delUOM(Request $request)
    {
        $params = [
            'productuomid' => $request->input('uomid', '')
        ];
        $url = config('constant.popwarung.api_url').'product/delproductuom';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));

        return $mresult;
    }

    public function convertion(Request $request)
    {
        $data['uom'] = $this->getMasterUOM()->payload->data;
        return view('warung.catalog.uom.convertion.index', $data);
    }

    public function formConvertion(Request $request, $flag, $convertionid)
    {
        $data['uom'] = $this->getMasterUOM()->payload->data;
        $data['vflag'] = $flag;
        $data['vconvertionid'] = $convertionid;
        return view('warung.catalog.uom.convertion.form', $data);
    }

    public function getMasterUOM()
    {
        $url = config('constant.popwarung.api_url').'product/getlistproductuom';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return json_decode($mresult);
    }
}
