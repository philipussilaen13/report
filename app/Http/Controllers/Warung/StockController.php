<?php

namespace App\Http\Controllers\Warung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiPopWarung;

class StockController extends Controller
{
    /**
     * get agent landing dashboard
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stockOpname(Request $request)
    {
        return view('warung.catalog.stock.index');
    }

    public function getGroupProductUOM(Request $request, $productid)
    {
        $url = config('constant.popwarung.api_url').'product/getgroupproductuom/'.$productid;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function getListStock(Request $request)
    {
        $url = config('constant.popwarung.api_url').'stock/getliststockopname';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function getSuggestProduct(Request $request)
    {
        $search = $request->input('search', null);
        
        $url = config('constant.popwarung.api_url').'product/getlistproduct/0/all/all/all/all/all/all/all/1/all/'.$search.'/none/0/25/none';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function addStockOpname(Request $request)
    {
        $params = [
            'dateopname' => $request->input('dateopname', null),
            'productid' => $request->input('productid', null),
            'lowuomproduct' => $request->input('lowuomconversion', null),
            'productuomid' => $request->input('uomid', null),
            'qty' => $request->input('qtystock', null),
            'description' => $request->input('remark', null),
        ];
		
        $url = config('constant.popwarung.api_url').'stock/addstockopname';

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        return $mresult;
    }
}
