<?php

namespace App\Http\Controllers\Warung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiPopWarung;

class BrandController extends Controller
{
    /**
     * get agent landing dashboard
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function brand(Request $request)
    {
        return view('warung.catalog.brand.index', []);
    }

    public function getListBrand(Request $request)
    {
        $url = config('constant.popwarung.api_url').'brand/getlistbrand/all';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function checkProductByBrand(Request $request, $brandid)
    {
        $url = config('constant.popwarung.api_url').'brand/checkproductbybrand/'.$brandid;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;
    }

    public function crudBrand(Request $request)
    {
        $flag = $request->input('flag', '1');
        $params = [
            'brandid' => $request->input('brandid', ''),
            'name' => $request->input('brand', '')
        ];

        if($flag == '1') {
            unset($params['brandid']);
            $url = config('constant.popwarung.api_url').'brand/addbrand';
        }
        else {
            $url = config('constant.popwarung.api_url').'brand/updbrand';
        }

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        return $mresult;
    }

    public function delBrand(Request $request)
    {
        $params = [
            'brandid' => $request->input('brandid', '')
        ];

        $url = config('constant.popwarung.api_url').'brand/delbrand';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));

        return $mresult;
    }
}
