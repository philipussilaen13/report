<?php

namespace App\Http\Controllers\Warung;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiPopWarung;
use App\Http\Helpers\Helper;
use App\Models\Virtual\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * get warung landing dashboard
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function product(Request $request)
    {
        return view('warung.catalog.product.index', []);
    }

    public function getListProduct(Request $request)
    {
		$iswarung = $request->input('warungid', '0');
        $warungid = 'all';
        $mobileid = empty($request->input('mobileid', '')) ? 'all' : $request->input('mobileid', '');
        $categoriesid = empty($request->input('categoriesid', '')) ? 'all' : $request->input('categoriesid', '');
        $subcategoriesid = empty($request->input('subcategoriesid', '')) ? 'all' : $request->input('subcategoriesid', '');
        $subsubcategoriesid = empty($request->input('subsubcategoriesid', '')) ? 'all' : $request->input('subsubcategoriesid', '');
        $brandid = empty($request->input('brandid', '')) ? 'all' : $request->input('brandid', '');
        $skuid = empty($request->input('skuid', '')) ? 'all' : $request->input('skuid', '');
        $statusid = $request->input('statusid', '1');
        $regioncode = empty($request->input('regionid', '')) ? 'all' : $request->input('regionid', '');
		$keyword = empty($request->input('search', '')) ? 'all' : $request->input('search', '');
		$draw = $request->input('draw', 0);
		$start = $request->input('start', 0);
        $limit = $request->input('limit', 25);
        
//         $url = config('constant.popwarung.api_url').'product/getlistproduct/'.$iswarung.'/'.$warungid.'/'.$mobileid.'/'.$categoriesid.'/'.$subcategoriesid.'/'.$subsubcategoriesid.'/'.$brandid.'/'.urlencode($skuid).'/'.$statusid.'/'.$regioncode.'/'.urlencode($keyword).'/none/'.$start.'/'.$limit.'/none';
//         $mresult = json_decode(ApiPopWarung::callAPI('GET', $url, false), true);
        $params = [
            'iswarung'           => $iswarung,
            'warungid'           => $warungid,
            'mobileid'           => $mobileid,
            'categoriesid'       => $categoriesid,
            'subcategoriesid'    => $subcategoriesid,
            'subsubcategoriesid' => $subsubcategoriesid,
            'brandid'            => $brandid,
            'skuid'              => $skuid,
            'statusid'           => $statusid,
            'regioncode'         => $regioncode,
            'keyword'            => $keyword,
            'tglupdate'          => "none",
            'start'              => $start,
            'limit'              => $limit,
            'page'               => "none",
            'requestedby'        => 'report',
        ];
        
        $url = config('constant.popwarung.api_url').'product/getproduct';
        $response = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        $mresult = json_decode($response, true);
		
		$result = [
			'success' => $mresult['success'],
			'payload' => [
				'data' => $mresult['payload']['data'],
				'draw' => $draw,
				'count' => $mresult['payload']['count']
			],
		];
		
        return response()->json($result);
    }

    public function getProductByID(Request $request, $id=null)
    {
        $url = config('constant.popwarung.api_url').'product/getproductbyid/'.$id;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        $mresult = json_decode($mresult);

        $response = new \stdClass();

        if($mresult->success) {
            $response->success = true;

            $data = $mresult->payload->data;            
            if(sizeof($data->price_region) > 0) {
                $dataPrice = [];
                foreach($data->price_region as $r) {
                    $temp = new \stdClass();
                    $temp->id = $r->id;
                    $temp->products_id = $r->products_id;
                    $temp->regions_id = $r->regions_id;
    
                    $mresRegion = Region::where('region_code', $r->regions_id);
                    $temp->regions_name = null;
                    if($mresRegion->count() > 0) {
                        $temp->regions_name = $mresRegion->first()->region_name;
                    }
                    $temp->purchase_price = $r->purchase_price;
                    $temp->selling_price = $r->selling_price;
                    $temp->suggest_price = $r->suggest_price;
                    $temp->discount = $r->discount;
                    $temp->is_promo = $r->is_promo;
                    $temp->selling_price_promo = $r->selling_price_promo;
    
                    $dataPrice[] = $temp;
                }    
                $data->price_region = $dataPrice;    
            } 

            $response->payload = [
                'data' => $data
            ];
        }
        else {
            $response->success = false;
            $response->payload = [
                'data' => []
            ];
        }

        return response()->json($response);
    }

    public function addProduct(Request $request, $flag='1', $id=null)
    {
        $categoriesid = 'all';
        $level = '0';

        $mresRegion = Region::all();        

        $data['flag'] = $flag;
        $data['productid'] = $id;
        $data['brands'] = $this->getMasterBrand()->payload->data;
        $data['categories'] = $this->getMasterCategory($categoriesid, $level)->payload->data;
        $data['uom'] = $this->getMasterProductUOM()->payload->data;
        $data['regions'] = $mresRegion;
        $data['categoriesMobile'] = $this->getMasterCategoriesMobile()->payload->data;
        return view('warung.catalog.product.add', $data);
    }

    public function saveProduct(Request $request)
    {
		$flag = $request->input('flag', '1');
		$productID = $request->input('productid', '');
        $productName = $request->input('productname', '');
        $productCode = $request->input('productcode', '');
        $categoriesid = $request->input('categoriesid', null) == '0' ? null : $request->input('categoriesid', null);
        $subCategoriesid = $request->input('subcategoriesid', null) == '0' ? null : $request->input('subcategoriesid', null);
        $subSubCategoriesid = $request->input('subsubcategoriesid', null) == '0' ? null : $request->input('subsubcategoriesid', null);
		$fileOld = $request->input('fileold','');
		$fileTypeOld = $request->input('filetypeold','');
        $isHighlight = $request->input('ishighlight', '0') == 'on' ? '1' : $request->input('ishighlight', '0');
        $sku = $request->input('sku', '');
        $publish = $request->input('publish', '0') == 'on' ? '1' : $request->input('publish', '0');
        $publish_old = $request->input('publishold', '0');
        $params_price = json_decode($request->input('params_price', ''));

        $files = $request->file('files');

        $newFile = null; $newFileType = null;
        if( !empty($files)) {
            $newFile = base64_encode(file_get_contents($files));
            $newFileType = $files->getMimeType();
        }

        $params = [
			'productid' => $productID,
            'name' => $request->input('productname', ''),
            'barcode' => $request->input('productcode', ''),
            'description' => $request->input('description', ''),
            'product_categories_sub_id' => $subCategoriesid,
            'product_categories_sub_sub_id' => $subSubCategoriesid,
            'picture' => $newFile,
            'picture_type' => $newFileType,
            'picture_old' => $fileOld,
            'picture_type_old' => $fileTypeOld,
            'weight' => $request->input('weight', ''),
            'width' => $request->input('width', ''),
            'height' => $request->input('height', ''),
            'volume' => $request->input('volume', ''),
            'low_stock_conversion' => null,
            'low_uom_conversion' => $request->input('smallestuom', ''),
            'alert_qty' => $request->input('alertqty', ''),
            'is_warung' => '0', // 0=barang popbox, 1 = barang warung
            'product_types_id' => 'f01d5ee1-a6aa-11e8-a2df-1c1b0dae80ab',
            'product_brands_id' => $request->input('brandid', null),
            'product_categories_id' => $categoriesid,
            'warung_id' => $request->input('warungid', null),
            'is_highlight' => $isHighlight,
            'mobilegrouping' => $request->input('categoriesmobile', null),
            'publish' => $publish
        ];

		if($flag == '1') {
            $v_price = [];
            foreach($params_price as $r) {
                $temp = [];
                $temp['regions_id'] = $r->regionid;
                $temp['purchase_price'] = $r->purchaseprice;
                $temp['selling_price'] = $r->sellingprice;
                $temp['suggest_price'] = $r->suggestprice;
                $temp['discount'] = $r->discount;
                $temp['is_promo'] = $r->ispromo;
                $temp['price_promo'] = $r->promoprice;
                $v_price[] = $temp;
            }    
            $params['price_region'] = $v_price;

			unset($params['productid']);
			unset($params['picture_old']);
			unset($params['picture_type_old']);
			$url = config('constant.popwarung.api_url').'product/addproduct';            
        }
        else {
            // $synch_date = null;
            // if($publish_old == 3 || $publish_old == 0) {
            //     if($publish != $publish_old ) {
            //         $synch_date = date('Y-m-d H:i:s');
            //     }
            // }
    
		    $params['sku'] = $sku;
		    // $params['synch_date'] = $synch_date;
			$url = config('constant.popwarung.api_url').'product/updproduct';
        }

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        return $mresult;
    }
	
	public function delProduct(Request $request) 
	{
        $params = [
            'productid' => $request->input('productid', null)
        ];		
        $url = config('constant.popwarung.api_url').'product/delproduct';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));

        return $mresult;
	}

    public function getMasterBrand()
    {
        $url = config('constant.popwarung.api_url').'brand/getlistbrand/all';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return json_decode($mresult);
    }

    public function getMasterCategory($id, $level)
    {
        $url = config('constant.popwarung.api_url').'category/getlistcategories/'.$id.'/'.$level;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return json_decode($mresult);
    }

    public function getMasterProductUOM()
    {
        $url = config('constant.popwarung.api_url').'product/getlistproductuom';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return json_decode($mresult);
    }

    public function getMasterCategoriesMobile()
    {
        $url = config('constant.popwarung.api_url').'category/getlistcategoriesmobile';
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return json_decode($mresult);
    }

    public function getMasterRegion() 
    {
        $mresult = Region::orderBy('region_name', 'asc')->get();
        
        $response = new \stdClass();
        $response->success = true;
        $response->payload = [
            'data' => $mresult
        ];
        return response()->json($response);
    }

    public function productbom(Request $request)
    {   
        $publish_status = [
            ['id' => 'all',             'text' => '- Semua -'],
            ['id' => 'unpublish',       'text' => 'Unpublish'],
            ['id' => 'stock_purchase',  'text' => 'Belanja Stock'],
        ];
        
        $regions = [
            ['id' => 0, 'text' => '- Semua -'],
        ];
        
        $result = Region::whereNull('deleted_at')->orderBy('region_name', 'asc')->get();
        foreach ($result as $r){
            $regions[] = ['id' => $r->region_code, 'text' => $r->region_name];
        }
        return view('warung.catalog.productbom.index', compact('publish_status', 'regions'));
    }

    public function getListProductBOM(Request $request)
    {
        $is_download = $request->input('is_download', false);
        $url = '';
        $url_method = 'POST';
        $product_name = $request->input('product_name');
        $bom_code = $request->input('bom_code');
        $region = $request->input('region');
        $publish_status = $request->input('publish_stts');
        
        $params = [
            'product_name'      => $product_name,
            'bom_code'          => $bom_code,
            'region'            => !empty($region) ? $region : '',
            'publish_status'    => $publish_status,
        ];
        if(!$is_download){
    		$draw = $request->input('draw', 0);
    		$params['start'] = $request->input('start', 0);
    		$params['limit'] = $request->input('limit', 25);
    		$url = config('constant.popwarung.api_url').'product/getlistproductbom';  
    		$params = json_encode($params);
        } else {
            $url = config('constant.popwarung.api_url').'product/download-list-product-bom';
            $url_method = 'GET';
        }
        $payload = ApiPopWarung::callAPI($url_method, $url, $params);
        $payload = json_decode($payload, true);
        
        if(!$is_download){
    		$result = [
    		    'success' => $payload['success'],
    			'payload' => [
    			    'data' => $payload['payload']['data'],
    				'draw' => $draw,
    			    'count' => $payload['payload']['count']
    			],
            ];
            
            return response()->json($result);
        } else {
            $rows = [];
            
            $arr_region = collect($this->getRegion())->pluck('region_name', 'region_code')->all();
            
            foreach ($payload['payload']['data'] as $index => $item){
                $item['region_item'] = array_key_exists($item['region_item'], $arr_region) ? $arr_region[$item['region_item']]: '-';
                $item['region_product'] = array_key_exists($item['region_product'], $arr_region) ? $arr_region[$item['region_product']]: '-';
                $publish_type = '-';
                if(0 == $item['publish_type']){ 
                    $publish_type = 'Unpublish';
                } elseif(1 == $item['publish_type']) {
                    if(App::environment('production')){
                        $publish_type = 'Penjualan + Belanja Stok';
                    } else {
                        $publish_type = 'Publish';
                    }
                } elseif(2 == $item['publish_type']){
                    $publish_type = 'Penjualan';
                } elseif(3 == $item['publish_type']){
                    $publish_type = 'Belanja Stok';
                }
                $rows[] = [
                    'no'                        => $index+1,
                    'created_at'                => $item['created_at'],
                    'create_by'                 => $item['create_by'],
                    'updated_at'                => $item['updated_at'],
                    'update_by'                 => $item['update_by'],
                    'product_name'              => $item['product_name'],
                    'product_code'              => $item['product_code'],
                    'item_name'                 => $item['item_name'],
                    'item_code'                 => $item['item_code'],
                    'region_item'               => $item['region_item'],
                    'qty_item'                  => $item['qty_item'],
                    'region_product'            => $item['region_product'],
                    'purchase_price_product'    => $item['purchase_price_product'],
                    'selling_price_product'     => $item['selling_price_product'],
                    'suggest_price_product'     => $item['suggest_price_product'],
                    'discount_product'          => $item['discount_product'],
                    'promo_price_product'       => $item['promo_price_product'],
                    'publish_type'              => $publish_type
                ];
            }
            
            $filename = "List of BOM product";
            $response = Helper::rendering_excel($filename, $rows, self::product_bom_headers());
            return response()->json($response);
        }
    }
    
    public static function product_bom_headers(){
        
        $header = [
            ["column"  => "A", "name" => "No",                  "field" => "no",                        'align' => 'center'],
            ["column"  => "B", "name" => "Created Date & Time", "field" => "created_at",                'align' => 'left'],
            ["column"  => "C", "name" => "Created By",          "field" => "create_by",                 'align' => 'left'],
            ["column"  => "D", "name" => "Updated Date & Time", "field" => "updated_at",                'align' => 'left'],
            ["column"  => "E", "name" => "Updated By",          "field" => "update_by",                 'align' => 'left'],
            ["column"  => "F", "name" => "BOM Name",            "field" => "product_name",              'align' => 'left'],
            ["column"  => "G", "name" => "BOM Code",            "field" => "product_code",              'align' => 'left',  'data_type' => 'type_to_string'],
            ["column"  => "H", "name" => "Product Name",        "field" => "item_name",                 'align' => 'left'],
            ["column"  => "I", "name" => "Barcode Product",     "field" => "item_code",                 'align' => 'left',  'data_type' => 'type_to_string'],
            ["column"  => "J", "name" => "Region Product",      "field" => "region_item",               'align' => 'left'],
            ["column"  => "K", "name" => "Qty Product",         "field" => "qty_item",                  'align' => 'right'],
            ["column"  => "L", "name" => "Region BOM",          "field" => "region_product",            'align' => 'left'],
            ["column"  => "M", "name" => "Purchase Price BOM",  "field" => "purchase_price_product",    'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "N", "name" => "Selling Price BOM",   "field" => "selling_price_product",     'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "O", "name" => "Suggest Price",       "field" => "suggest_price_product",     'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "P", "name" => "Discount BOM",        "field" => "discount_product",          'align' => 'right',],
            ["column"  => "Q", "name" => "Promo Price BOM",     "field" => "promo_price_product",       'align' => 'right', 'column_format' => 'Rp #,##0.00_-'],
            ["column"  => "R", "name" => "Publish Type",        "field" => "publish_type",              'align' => 'left'],
        ];
        
        return $header;
    }

    public function getProductBOMByID(Request $request, $id=null)
    {
        $response = new \stdClass();
        $response->success = false;
        $response->payload = [];

        $url = config('constant.popwarung.api_url').'product/getproductbombyid/'.$id;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        $mresult = json_decode($mresult);

        $response = new \stdClass();
        if($mresult->success) {
            $response->success = true;

            $data = $mresult->payload->data;            
            if(sizeof($data->price_region) > 0) {
                $dataPrice = [];
                foreach($data->price_region as $r) {
                    $temp = new \stdClass();
                    $temp->id = $r->id;
                    $temp->products_id = $r->products_id;
                    $temp->regions_id = $r->regions_id;
    
                    $mresRegion = Region::where('region_code', $r->regions_id);
                    $temp->region_name = null;

                    if($mresRegion->count() > 0) {
                        $temp->region_name = $mresRegion->first()->region_name;
                    }
                    
                    $temp->purchase_price = $r->purchase_price;
                    $temp->selling_price = $r->selling_price;
                    $temp->suggest_price = $r->suggest_price;
                    $temp->discount = $r->discount;
                    $temp->is_promo = $r->is_promo;
                    $temp->selling_price_promo = $r->selling_price_promo;
    
                    $dataPrice[] = $temp;
                }    
                $data->price_region = $dataPrice;    
            } 

            if( sizeof($data->productitems) > 0) {
                $productItems = [];
                foreach($data->productitems as $r) {
                    $mresRegion = Region::where('region_code', $r->region_code);
                    $r->region_name = '';
                    if($mresRegion->count() > 0) {
                        $r->region_name = $mresRegion->first()->region_name;
                    }

                    $productItems[] = $r;
                }
            }

            $response->payload['data'] = $data;
        }

        return response()->json($response);
    }

    public function formProductBOM(Request $request, $flag='1', $id=null)
    {
        $categoriesid = 'all';
        $level = '0';

        $mresRegion = Region::all();        

        $data['flag'] = $flag;
        $data['productid'] = $id;
        $data['brands'] = $this->getMasterBrand()->payload->data;
        $data['categories'] = $this->getMasterCategory($categoriesid, $level)->payload->data;
        $data['uom'] = $this->getMasterProductUOM()->payload->data;
        $data['regions'] = $mresRegion;
        $data['categoriesMobile'] = $this->getMasterCategoriesMobile()->payload->data;

        return view('warung.catalog.productbom.add', $data);
    }

    public function saveProductBOM(Request $request)
    {
		$flag = $request->input('flag', '1');
        $productID = $request->input('productid', '');
        $productName = $request->input('productname', '');
        $productCode = $request->input('productcode', '');
        $categoriesid = $request->input('categoriesid', null) == '0' ? null : $request->input('categoriesid', null);
        $subCategoriesid = $request->input('subcategoriesid', null) == '0' ? null : $request->input('subcategoriesid', null);
        $subSubCategoriesid = $request->input('subsubcategoriesid', null) == '0' ? null : $request->input('subsubcategoriesid', null);
		$fileOld = $request->input('fileold','');
		$fileTypeOld = $request->input('filetypeold','');
        $isHighlight = $request->input('ishighlight', '0') == 'on' ? '1' : $request->input('ishighlight', '0');
        $sku = $request->input('sku', '');
        $publish = $request->input('publish', '0') == 'on' ? '1' : $request->input('publish', '0');
        $params_price = json_decode($request->input('params_price', ''));
        $productItems = json_decode($request->input('product_items', ''));

        $files = $request->file('files');

        $newFile = null; $newFileType = null;
        if( !empty($files)) {
            $newFile = base64_encode(file_get_contents($files));
            $newFileType = $files->getMimeType();
        }

        $params = [
			'productid' => $productID,
            'name' => $request->input('productname', ''),
            'barcode' => $request->input('productcode', ''),
            'description' => $request->input('description', ''),
            'product_categories_sub_id' => $subCategoriesid,
            'product_categories_sub_sub_id' => $subSubCategoriesid,
            'picture' => $newFile,
            'picture_type' => $newFileType,
            'picture_old' => $fileOld,
            'picture_type_old' => $fileTypeOld,
            'weight' => $request->input('weight', ''),
            'width' => $request->input('width', ''),
            'height' => $request->input('height', ''),
            'volume' => $request->input('volume', ''),
            'low_stock_conversion' => null,
            'low_uom_conversion' => $request->input('smallestuom', ''),
            'alert_qty' => $request->input('alertqty', ''),
            'is_warung' => '0', // 0=barang popbox, 1 = barang warung
            'product_types_id' => '02c5f9ac-f7a5-11e8-a9bd-e2e2c63acc52',
            'product_brands_id' => $request->input('brandid', null),
            'product_categories_id' => $categoriesid,
            'warung_id' => $request->input('warungid', null),
            'is_highlight' => $isHighlight,
            'mobilegrouping' => $request->input('categoriesmobile', null),
            'publish' => $publish
        ];

		if($flag == '1') {
            $v_price = [];
            foreach($params_price as $r) {
                $temp = [];
                $temp['regions_id'] = $r->regionid;
                $temp['purchase_price'] = $r->purchaseprice;
                $temp['selling_price'] = $r->sellingprice;
                $temp['suggest_price'] = $r->suggestprice;
                $temp['discount'] = $r->discount;
                $temp['is_promo'] = $r->ispromo;
                $temp['price_promo'] = $r->promoprice;
                $v_price[] = $temp;
            }
    
            $v_productItems = [];
            foreach($productItems as $r) {
                $temp = [];
                $temp['productid'] = $r->productid;
                $temp['regioncode'] = $r->regioncode;
                $temp['qty'] = $r->qty;
                $v_productItems[] = $temp;
            }
    
            $params['price_region'] = $v_price;
            $params['productitems'] = $v_productItems;    

			unset($params['productid']);
			unset($params['picture_old']);
			unset($params['picture_type_old']);
			$params['create_by'] = Auth::user()->name;
			$url = config('constant.popwarung.api_url').'product/addproductbom';
        }
        else {
            $params['update_by'] = Auth::user()->name;
		    $params['sku'] = $sku;
			$url = config('constant.popwarung.api_url').'product/updproductbom';
        }

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        return $mresult;        
    }

	public function delProductBOM(Request $request) 
	{
        $params = [
            'productid' => $request->input('productid', null)
        ];		
        $url = config('constant.popwarung.api_url').'product/delproductbom';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));

        return $mresult;
	}

    public function getSuggestionProduct(Request $request)
    {
        $response = new \stdClass();
        $response->success = false;
        $response->payload = [];

        $params = [
            'regioncode' => '',
            'iswarung' => '0',
            'warungid' => '',
            'keyword' => $request->input('search',''),
            'start' => 0,
            'limit' => 25                        
        ];

        $url = config('constant.popwarung.api_url').'product/getsuggestproduct';        
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        $mresult = json_decode($mresult, true);      
        
        if($mresult['success']) {
            $data = [];
            if($mresult['payload']['count'] > 0) {
                foreach($mresult['payload']['data'] as $item) {
                    $mresRegion = Region::where('region_code', $item['regions_id']);
                    $item['region_name'] = '';
                    if($mresRegion->count() > 0) {
                        $item['region_name'] = $mresRegion->first()->region_name;
                    }
                    $data[] = $item;
                }    
            }

            $response->success = true;
            $response->payload['response'] = $mresult['payload']['response'];
            $response->payload['data'] = $data;
            $response->payload['count'] = $mresult['payload']['count'];
        }
        
        return response()->json($response);
    }

    public function addProductItemBOM(Request $request)
    {
        $params = [
            'parentid'      => $request->input('parentid',''),
            'productid'     => $request->input('productid',''),
            'qty'           => $request->input('qty',''),
            'regioncode'    => $request->input('regioncode',''),
            'create_by'     => Auth::user()->name,
            'update_by'     => Auth::user()->name,
        ];

        $url = config('constant.popwarung.api_url').'product/addproductitembom';        
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        $mresult = json_decode($mresult, true);        

        return response()->json($mresult);
    }

    public function updProductItemBOM(Request $request)
    {
        $params = [
            'productitembomid' => $request->input('productitembomid',''),
            'parentid' => $request->input('parentid',''),
            'productid' => $request->input('productid',''),
            'qty' => $request->input('qty',''),
            'regioncode' => $request->input('regioncode',''),
            'update_by'     => Auth::user()->name,
        ];

        $url = config('constant.popwarung.api_url').'product/updproductitembom';        
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        $mresult = json_decode($mresult, true);        

        return response()->json($mresult);
    }

    public function delProductItemBOM(Request $request)
    {
        $params = [
            'productitembomid'  => $request->input('productitembomid',''),
            'update_by'         => Auth::user()->name,
        ];

        $url = config('constant.popwarung.api_url').'product/delproductitembom';        
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        $mresult = json_decode($mresult, true);        

        return response()->json($mresult);
    }

    public function savePriceByRegion(Request $request)
    {
        $act = $request->input('act', '');
        $params = [
            'priceid' => $request->input('priceid', ''),
            'productid' => $request->input('productid', ''),
            'regionid' => $request->input('regioncode', ''),
            'purchaseprice' => $request->input('purchaseprice',''),
            'sellingprice' => $request->input('sellingprice',''),
            'suggestprice' => $request->input('suggestprice',''),
            'discount' => $request->input('discount',''),
            'ispromo' => $request->input('ispromo',''),
            'sellingpricepromo' => $request->input('promoprice',''),
        ];

        if($act == 'add') {
            $params['createby'] = Auth::user()->name;
            $url = config('constant.popwarung.api_url').'product/addprice';
            unset($params['priceid']);        
        }
        else {
            $params['updateby'] = Auth::user()->name;
            $url = config('constant.popwarung.api_url').'product/updprice';        
        }

        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        $mresult = json_decode($mresult, true);        

        return response()->json($mresult);
    }

    public function delPriceByRegion(Request $request)
    {
        $params = [
            'priceid' => $request->input('priceid','')
        ];

        $url = config('constant.popwarung.api_url').'product/delprice';        
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        $mresult = json_decode($mresult, true);        

        return response()->json($mresult);
    }
    
    public function downloadFile(Request $request){
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $iswarung = $request->input('warungid', '0');
        $warungid = 'all';
        $mobileid = empty($request->input('mobileid', '')) ? 'all' : $request->input('mobileid', '');
        $categoriesid = empty($request->input('categoriesid', '')) ? 'all' : $request->input('categoriesid', '');
        $subcategoriesid = empty($request->input('subcategoriesid', '')) ? 'all' : $request->input('subcategoriesid', '');
        $subsubcategoriesid = empty($request->input('subsubcategoriesid', '')) ? 'all' : $request->input('subsubcategoriesid', '');
        $brandid = empty($request->input('brandid', '')) ? 'all' : $request->input('brandid', '');
        $skuid = empty($request->input('skuid', '')) ? 'all' : $request->input('skuid', '');
        $statusid = $request->input('statusid', '1');
        $regioncode = empty($request->input('regionid', '')) ? 'all' : $request->input('regionid', '');
        $keyword = empty($request->input('search', '')) ? 'all' : $request->input('search', '');
        
        $params = [
            'iswarung'              => $iswarung == 'all' ? null : $iswarung,
            'warungid'              => $warungid == 'all' ? null : $warungid,
            'mobileid'              => $mobileid == 'all' ? null : $mobileid,
            'categoriesid'          => $categoriesid == 'all' ? null : $categoriesid,
            'subcategoriesid'       => $subcategoriesid == 'all' ? null : $subcategoriesid,
            'subsubcategoriesid'    => $subsubcategoriesid == 'all' ? null : $subsubcategoriesid,
            'brandid'               => $brandid == 'all' ? null : $brandid,
            'skuid'                 => $skuid == 'all' ? null : $skuid,
            'statusid'              => $statusid == 'all' ? null : $statusid,
            'regioncode'            => $regioncode,
            'keyword'               => $keyword == 'all' ? '' : $keyword,
        ];
        
        $url = config('constant.popwarung.api_url').'product/download-list-product';
        $result = ApiPopWarung::callAPI('GET', $url, $params);
        
        $payload = (array) json_decode($result, true);
        
        $rows = [];

        $arr_region = collect($this->getRegion())->pluck('region_name', 'region_code')->all();

        foreach ($payload['payload']['data'] as $row){
            $row['region_name'] = array_key_exists($row['region_code'], $arr_region) ? $arr_region[$row['region_code']]: '-';
            
            $rows[] = $row;
        }
        $filename = "List of Product";
        $myFile = Excel::create("$filename", function ($excel) use ($rows) {
            $excel->setTitle('Products');
            $excel->setCreator(Auth::user()->name)->setCompany('PopBox Agent');
            $excel->setDescription('List of Product');
            $excel->sheet('Products', function ($sheet) use ($rows) {
                $sheet->setColumnFormat(array(
                    "F" => "@", // display SKU with full length 
                ));
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No.');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('ID');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Name');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Brand');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Barcode');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('SKU');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Category');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Sub Category');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Sub Sub_Category');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Categories Mobile');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Region Name');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Region Code');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Purchase Price');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('N1', function ($cell) {
                    $cell->setValue('Selling Price');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('O1', function ($cell) {
                    $cell->setValue('Suggest Price');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('P1', function ($cell) {
                    $cell->setValue('Smallest Unit');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('Q1', function ($cell) {
                    $cell->setValue('Weight');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('R1', function ($cell) {
                    $cell->setValue('Height');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('S1', function ($cell) {
                    $cell->setValue('Width');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('T1', function ($cell) {
                    $cell->setValue("Volume ( ml )");
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('U1', function ($cell) {
                    $cell->setValue('Alert Qty');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });

                $cellNumberStart = 2;

                foreach($rows as $item){
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($cellNumberStart, $item) {
                        $cell->setValue($cellNumberStart-1);
                        $cell->setAlignment('center');
                    });

                    $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['productid']);
                    });

                    $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['name']);
                    });

                    $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                        $value = $item['brand'] ? $item['brand'] : '-';
                        $cell->setValue($value);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['barcode']);
                    });
                    $sheet->setCellValueExplicit("E$cellNumberStart", $item['barcode'], \PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['sku']);
                    });
                    $sheet->setCellValueExplicit("F$cellNumberStart", $item['sku'], \PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                        $value = (string) $item['prod_category'];
                        $cell->setValue(strtoupper($value));
                    });

                    $sheet->cell("H$cellNumberStart", function ($cell) use ($cellNumberStart, $item) {
                        $cell->setValue($item['sub_category']);
                    });

                    $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['sub_sub_category']);
                    });

                    $sheet->cell("J$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['product_categories_mobile']);
                    });

                    $sheet->cell("K$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['region_name']);
                    });

                    $sheet->cell("L$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['region_code']);
                        $cell->setAlignment('center');
                    });

                    $sheet->cell("M$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['purchase_price']);
                    });

                    $sheet->cell("N$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue(strtoupper($item['selling_price']));
                    });

                    $sheet->cell("O$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['suggest_price']);
                    });

                    $sheet->cell("P$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['uom_name']);
                        $cell->setAlignment('center');
                    });
                    $sheet->cell("Q$cellNumberStart", function ($cell) use ($item) {
                        $value = $item['weight'] ? $item['weight'] : 0;
                        $cell->setValue($value);
                    });
                    $sheet->cell("R$cellNumberStart", function ($cell) use ($item) {
                        $value = $item['height'] ? $item['height'] : 0;
                        $cell->setValue($value);
                    });
                    $sheet->cell("S$cellNumberStart", function ($cell) use ($item) {
                        $value = $item['width'] ? $item['width'] : 0;
                        $cell->setValue($value);
                    });
                    $sheet->cell("T$cellNumberStart", function ($cell) use ($cellNumberStart, $item) {
                        $value = $item['volume'] ? $item['volume'] : 0;
                        $cell->setValue($value);
                    });

                    $sheet->cell("U$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['alert_qty']);
                    });

                    $cellNumberStart++;
                }
            });
        })->string('xlsx');//change xlsx for the format you want, default is xls
        
        $response =  array(
            'name' => $filename, //no extention needed
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        
        return response()->json($response);
    }
    
    private function getRegion(){
        return Region::withTrashed()->get();
    }
}
