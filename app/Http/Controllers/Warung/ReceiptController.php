<?php

namespace App\Http\Controllers\Warung;

use App\Models\Warung\Receipt;
use App\Models\Warung\ReceiptBatch;
use App\Models\Warung\ReceiptItem;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Helpers\SanitizeHelper;

class ReceiptController extends Controller
{   
    public function receipt(Request $request)
    {       
        return view('warung.receipt.index');
    }	
    public function getListReceipt(Request $request)
    {       
        $response = new \stdClass();
        $response->success = false;
        $response->payload = [];

        $params = [
            'start_date' => $request->input('startdate', null),
            'end_date' => $request->input('enddate',null),
            'statusid' => $request->input('statusid',null) == -1 ? null : $request->input('statusid',null),
            'lockerid' => $request->input('lockerid',null),
            'reference' => $request->input('reference',''),
            'start' => $request->input('start',0),
            'limit' => $request->input('limit', 25)
        ];

        $receiptDb = Receipt::getListReceipt($params);
        
        $response->success = true;
        $response->payload['data'] = SanitizeHelper::cleansingNull($receiptDb['data'], $request->input('start',0));
        $response->payload['count'] = $receiptDb['count'];

        return response()->json($response);
    }	
    public function getSummaryDelivery(Request $request) 
    {
        $response = new \stdClass();
        $response->success = false;
        $response->payload = [];

        $receiptDb = Receipt::getSummaryDelivery();
        $response->success = true;
        $response->payload['data'] = $receiptDb;

        return response()->json($response);
    }
    public function detailReceipt(Request $request, $receiptid) 
    {
        $data['receiptid'] = $receiptid;
        $data['receipt'] = Receipt::getReceiptByID($receiptid);
        $data['batch'] = self::getReceiptByBatch($receiptid);
        return view('warung.receipt.detail', $data);
    }
    public function getReceiptByBatch($receiptid)
    {       
        $receiptBatchDb = ReceiptBatch::getListReceiptBatch($receiptid);        
        $data = [];
        foreach($receiptBatchDb as $r) {
            $user = null;
            $userReportDb = \App\User::find($r['users_id']);
            if ($userReportDb){
                $user = $userReportDb->name;
            }
            $r['user'] = $user;

            $receiptItemDb = ReceiptItem::getListReceiptItem($receiptid, $r['receipt_batch_id']);

            $r['receipt_item'] = $receiptItemDb;

            $data[] = $r;
        }

        return $data;
    }
    public function getReceiptItems(Request $request) 
    {
        $receiptItemDb = ReceiptItem::getListReceiptItem();
        return $receiptItemDb;
    }
    public function formDetail(Request $request, $receiptid) 
    {
        $data['receiptid'] = $receiptid;
        $data['items'] = Receipt::getReceiptSuggestDelivery($receiptid);        
        return view('warung.receipt.form', $data);
    }
    public function saveDetail(Request $request) 
    {        
        $receiptid = $request->input('receiptid', '');
        $items = json_decode($request->input('items', ''));
        $userVerificationID = Auth::user()->id;

        $response = new \stdClass();
        $response->success = true;
        $response->payload = null;

        $params = [
            'receipt_id' => $receiptid,
            'status_batch' => '1',
            'users_id' => $userVerificationID,
        ];

        $batchNo = null;
        if( empty($params['receipt_id'])) {
            $response->success = false;
            $response->payload = [
                'message' => 'Receipt ID is null'
            ];
            return response()->json($response);
        }

        \DB::connection('popbox_agent')->beginTransaction();

        $receiptBatchDb = ReceiptBatch::where('receipt_id', $params['receipt_id'])
            ->select(
                \DB::raw("
                    IFNULL(MAX(CAST(SUBSTRING_INDEX(batch_no,'-',-1) AS UNSIGNED)),0)+1 AS batch_no
                ")
            )
            ->first();
        
        if( !$receiptBatchDb) {
            \DB::connection('popbox_agent')->rollback();
            $response->success = false;
            $response->payload = [
                'message' => 'Batch Number not created'
            ];
            return response()->json($response);
        }

        if( !empty($receiptBatchDb->batch_no) ) {
            $batchNo = 'BATCH-'.$receiptBatchDb->batch_no;
        }

        $receiptBatchData = [
            'receipt_id' => $params['receipt_id'],
            'batch_no' => $batchNo,
            'status_batch' => $params['status_batch'],
            'users_id' => $params['users_id'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => null
        ];
        $receiptBatchDb = \DB::connection('popbox_agent')->table('receipt_batch')->insertGetId($receiptBatchData); 

        if( !$receiptBatchDb) {
            \DB::connection('popbox_agent')->rollback();
            $response->success = false;
            $response->payload = [
                'message' => 'Receipt Batch not created'
            ];
            return response()->json($response);
        }

        $o=0;
        foreach($items as $r) {
            $receiptItemsData = [
                'receipt_batch_id' => $receiptBatchDb,
                'transaction_items_id' => $r->transactionitem,
                'qty_delivery' => $r->qtydelivery,
                'qty_receipt' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')        
            ];

            $receiptItemsDb = \DB::connection('popbox_agent')->table('receipt_items')->insert($receiptItemsData);
            
            if( !$receiptItemsDb) {
                \DB::connection('popbox_agent')->rollback();
                $o++;
            }
        }
        
        \DB::connection('popbox_agent')->commit();

        if($o > 0) {
            $response->success = false;
            $response->payload = [
                'message' => 'Data has been failure'
            ];    
            return response()->json($response);
        }

        $response->payload = [
            'message' => 'Data has been success'
        ];    
        return response()->json($response);
    }
}
