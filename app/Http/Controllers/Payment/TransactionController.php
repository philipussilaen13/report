<?php

namespace App\Http\Controllers\Payment;

use App\Models\Payment\ClientTransactionDetail;
use App\Models\Payment\CompanyAccessToken;
use App\Models\Payment\PaymentChannel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TransactionController extends Controller
{
    public function getList(Request $request){
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $companyId = $request->input('company',null);
        $channelId = $request->input('channel',null);
        $customerName = $request->input('customerName',null);
        $status = $request->input('status',null);

        // get client transaction order by updated at
        $clientTransactionDetailDb = ClientTransactionDetail::with(['clientTransaction','clientTransaction.paymentChannel','clientTransaction.companyAccessToken','clientTransaction.dokuTransaction','clientTransaction.bniTransaction'])
            ->whereBetween('updated_at',[$startDate,$endDate])
            ->when($companyId,function($query) use($companyId){
                return $query->whereHas('clientTransaction',function($query2) use($companyId){
                    $query2->where('company_access_tokens_id',$companyId);
                });
            })->when($channelId,function($query) use($channelId){
                return $query->whereHas('clientTransaction',function($query2) use($channelId){
                    $query2->where('payment_channels_id',$channelId);
                });
            })->when($customerName,function($query) use($customerName){
                return $query->whereHas('clientTransaction',function($query2) use($customerName){
                    $query2->where('customer_name','LIKE',"%$customerName%");
                });
            })->when($status,function($query) use($status){
                return $query->whereHas('clientTransaction',function($query2) use($status){
                    $query2->where('status','LIKE',"%$status%");
                });
            })
            ->orderBy('updated_at','desc')
            ->paginate(20);

        // dd($clientTransactionDetailDb);

        // create for summary
        $openPaidAmount = 0;
        $fixedPaidAmount = 0;
        $openPaidCount = 0;
        $fixedPaidCount = 0;

        $openPaidTransactionDb = DB::connection('popbox_payment')
            ->table('client_transaction_details')
            ->join('client_transactions','client_transactions.id','=','client_transaction_details.client_transactions_id')
            ->where('client_transactions.status','PAID')
            ->where('client_transactions.payment_type','open')
            ->whereBetween('client_transaction_details.updated_at',[$startDate,$endDate])
            ->get();

        $fixedPaidTransactionDb = DB::connection('popbox_payment')
            ->table('client_transactions')
            ->where('status','PAID')
            ->where('payment_type','fixed')
            ->whereBetween('updated_at',[$startDate,$endDate])
            ->get();

        foreach ($openPaidTransactionDb as $item){
            $openPaidAmount += $item->amount;
            $openPaidCount++;
        }
        foreach ($fixedPaidTransactionDb as $item) {
            $fixedPaidAmount += $item->total_payment;
            $fixedPaidCount++;
        }

        $totalPaidAmount = $fixedPaidAmount + $openPaidAmount;
        $totalPaidCount = $fixedPaidCount + $openPaidCount;

        // Get company access token
        $companyAccessTokenDb = CompanyAccessToken::get();

        // get payment channel
        $paymentChannelDb = PaymentChannel::get();

        // status list
        $statusList = ['PAID','CREATED'];


        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        $data = [];
        $data['clientTransactionDetail'] = $clientTransactionDetailDb->appends($request->input());;
        $data['totalPaidAmount'] = $totalPaidAmount;
        $data['totalPaidCount'] = $totalPaidCount;
        $data['parameter'] = $param;
        $data['companyAccessTokenDb'] = $companyAccessTokenDb;
        $data['paymentChannelDb'] = $paymentChannelDb;
        $data['statusList'] = $statusList;

        // dd($clientTransactionDetailDb);

        return view('payment.list',$data);
    }

    public function postExcel(Request $request){
        set_time_limit(300);
        ini_set('memory_limit', '20480M');

        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $companyId = $request->input('company',null);
        $channelId = $request->input('channel',null);
        $customerName = $request->input('customerName',null);
        $status = $request->input('status',null);

        // get client transaction order by updated at
        $clientTransactionDetailDb = ClientTransactionDetail::with(['clientTransaction','clientTransaction.paymentChannel','clientTransaction.paymentChannel.paymentVendor','clientTransaction.companyAccessToken','clientTransaction.dokuTransaction','clientTransaction.bniTransaction'])
            ->whereBetween('created_at',[$startDate,$endDate])
            ->when($companyId,function($query) use($companyId){
                return $query->whereHas('clientTransaction',function($query2) use($companyId){
                    $query2->where('company_access_tokens_id',$companyId);
                });
            })->when($channelId,function($query) use($channelId){
                return $query->whereHas('clientTransaction',function($query2) use($channelId){
                    $query2->where('payment_channels_id',$channelId);
                });
            })->when($customerName,function($query) use($customerName){
                return $query->whereHas('clientTransaction',function($query2) use($customerName){
                    $query2->where('customer_name','LIKE',"%$customerName%");
                });
            })->when($status,function($query) use($status){
                return $query->whereHas('clientTransaction',function($query2) use($status){
                    $query2->where('status','LIKE',"%$status%");
                });
            })
            ->orderBy('id','desc')
            ->get();

        $nowDate = date('Y-m-d');
        $input = $request->except('_token');
        $filter = "";
        foreach ($input as $key => $item) {
            if (!empty($item)){
                $filter .= "$key-$item ";
            }
        }

        $filename = "Payment Transaction List per $nowDate $filter";

        Excel::create("$filename", function ($excel) use ($clientTransactionDetailDb) {
            $excel->setTitle('Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Agent');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($clientTransactionDetailDb) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Payment ID');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Company');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Channel');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('VA Number');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Bank Settlement');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Type');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Customer Data');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Amount');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Admin Gateway');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Bank Received');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Admin Fee');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Customer');
                });
                $sheet->cell('N1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('O1', function ($cell) {
                    $cell->setValue('Description');
                });
                $sheet->cell('P1', function ($cell) {
                    $cell->setValue('Date');
                });

                $cellNumberStart = 2;
                foreach ($clientTransactionDetailDb as $index => $detailItem) {
                    $clientTransactionDb = $detailItem->clientTransaction;
                    $paymentChannelDb = $clientTransactionDb->paymentChannel;

                    $paymentId = $clientTransactionDb->payment_id;
                    $subPaymentId = $detailItem->sub_payment_id;
                    $company = $clientTransactionDb->companyAccessToken->name;
                    $channel = $paymentChannelDb->name;
                    $vaNumber = null;
                    if ($clientTransactionDb->bniTransaction){
                        $vaNumber = $clientTransactionDb->bniTransaction->virtual_account_number;
                    } elseif ($clientTransactionDb->dokuTransaction){
                        $vaNumber = $clientTransactionDb->dokuTransaction->virtual_number;
                    }

                    $bankSettlement = null;
                    if (!empty($paymentChannelDb->bank_settlement)){
                        $bankSettlement = $paymentChannelDb->bank_settlement;
                    } else {
                        $bankSettlement = $paymentChannelDb->paymentVendor->bank_settlement;
                    }
                    $paymentType = $clientTransactionDb->payment_type;
                    $customerName = $clientTransactionDb->customer_name;
                    $paidAmount = $detailItem->paid_amount;
                    if (empty($paidAmount)){
                        $paidAmount = $detailItem->amount;
                    }
                    $adminGateway = 0;
                    if (!empty($detailItem->channel_admin_fee)){
                        $adminGateway = $detailItem->channel_admin_fee;
                    } else {
                        $adminGateway = $paymentChannelDb->admin_fee;
                    }
                    $bankReceived = $paidAmount - $adminGateway;
                    $customerAdminFee = 0;
                    if (!empty($detailItem->customer_admin_fee)){
                        $customerAdminFee = $detailItem->customer_admin_fee;
                    }
                    $customerAmount = $detailItem->amount;
                    $status = $clientTransactionDb->status;
                    $description = $clientTransactionDb->description;
                    $updatedAt = $detailItem->updated_at;

                    $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                        $cell->setValue($index + 1);
                    });
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($paymentId,$subPaymentId) {
                        $cell->setValue("$paymentId\n$subPaymentId");
                    });
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($company) {
                        $cell->setValue($company);
                    });
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($channel) {
                        $cell->setValue($channel);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($vaNumber) {
                        $cell->setValue($vaNumber);
                    });
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($bankSettlement) {
                        $cell->setValue($bankSettlement);
                    });
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($paymentType) {
                        $cell->setValue($paymentType);
                    });
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($customerName) {
                        $cell->setValue($customerName);
                    });
                    $sheet->cell("I$cellNumberStart", function ($cell) use ($paidAmount) {
                        $cell->setValue($paidAmount);
                    });
                    $sheet->cell("J$cellNumberStart", function ($cell) use ($adminGateway) {
                        $cell->setValue($adminGateway);
                    });
                    $sheet->cell("K$cellNumberStart", function ($cell) use ($bankReceived) {
                        $cell->setValue($bankReceived);
                    });
                    $sheet->cell("L$cellNumberStart", function ($cell) use ($customerAdminFee) {
                        $cell->setValue($customerAdminFee);
                    });
                    $sheet->cell("M$cellNumberStart", function ($cell) use ($customerAmount) {
                        $cell->setValue($customerAmount);
                    });
                    $sheet->cell("N$cellNumberStart", function ($cell) use ($status) {
                        $cell->setValue($status);
                    });
                    $sheet->cell("O$cellNumberStart", function ($cell) use ($description) {
                        $cell->setValue($description);
                    });
                    $sheet->cell("P$cellNumberStart", function ($cell) use ($updatedAt) {
                        $cell->setValue($updatedAt);
                    });
                    $cellNumberStart++;
                }
            });
        })->download('xlsx');

    }
}
