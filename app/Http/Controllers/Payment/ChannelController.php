<?php

namespace App\Http\Controllers\Payment;

use App\Models\Payment\CompanyAccessToken;
use App\Models\Payment\PaymentChannel;
use App\Models\Payment\TokenChannelPrivilege;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChannelController extends Controller
{
    /**
     * Get List Channel Payment
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getListChannel(Request $request){
        // get channel list
        $channelDb = PaymentChannel::with('paymentVendor')->paginate(15);

        $data = [];
        $data['channelDb'] = $channelDb;

        return view('payment.channel.list',$data);
    }

    /**
     * Get Edit Channel
     * @param Request $request
     * @param $channelCode
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEditChannel(Request $request,$channelCode){
        if (empty($channelCode)){
            $request->session()->flash('error','Empty Channel Code');
            return back();
        }

        // get channel DB
        $channelDb = PaymentChannel::where('code',$channelCode)->first();
        if (!$channelDb){
            $request->session()->flash('error','Channel Code not Found');
            return back();
        }

        $data = [];
        $data['channelDb'] = $channelDb;

        return view('payment.channel.form',$data);
    }

    /**
     * Post Add / Edit Channel
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postChannel(Request $request){
        $channelId = $request->input('channelId',null);
        $type = $request->input('type');
        $name = $request->input('name');
        $text = $request->input('text');
        $description = $request->input('description');
        $imageFile = $request->file('image');
        $image = $request->input('image');
        $status = $request->input('status');

        // update DB
        $channelDb = PaymentChannel::find($channelId);
        if (!$channelDb){
            $request->session()->flash('error','Channel Payment Not Found');
            return back();
        }

        $channelDb->type = $type;
        $channelDb->name = $name;
        $channelDb->text = $text;
        $channelDb->description = $description;
        $channelDb->status = $status;
        $imageUrl = null;
        if (!empty($imageFile)){
            $ftpHost = env('SERVER_PAYMENT_IP');
            $ftpUser = env('SERVER_PAYMENT_USER');
            $ftpPass = env('SERVER_PAYMENT_PASS');
            // filename
            $extension = $imageFile->getClientOriginalExtension();
            $fileName = $channelDb->code.".$extension";
            $imageUrl = "files/channel/".$fileName;
            $remotePath = env('PAYMENT_PATH').$imageUrl;
            $connection = ssh2_connect($ftpHost,22);
            if ($connection === false){
                $request->session()->flash('error','Failed to Connect FTP');
                return back();
            }

            $state = ssh2_auth_password($connection,$ftpUser,$ftpPass);
            if ($state === false){
                $request->session()->flash('error','Failed to Authenticate FTP');
                return back();
            }

            $state = ssh2_scp_send($connection,$imageFile,$remotePath,0777);
            if ($state === false){
                $request->session()->flash('error','Failed to Upload FTP');
                return back();
            }
            $channelDb->image_url = $imageUrl;
        }
        $channelDb->save();

        $request->session()->flash('success','Success Update');
        return redirect('payment/channel');
    }

    /**
     * Get List Client
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getListClient(Request $request){
        // get list client
        $clientDb = CompanyAccessToken::with('channels')->get();

        $data = [];
        $data['clientDb'] = $clientDb;

        return view('payment.client.list',$data);
    }

    /**
     * Get Detail Client
     * @param Request $request
     * @param $clientId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDetailClient(Request $request,$clientId){
        if (empty($clientId)){
            $request->session()->flash('error','Empty Client');
            return back();
        }

        // get client
        $clientDb = CompanyAccessToken::with('channels')->find($clientId);
        if (!$clientDb){
            $request->session()->flash('error','Client Not Found');
            return back();
        }
        $data = [];
        $data['clientDb'] = $clientDb;

        return view('payment.client.detail',$data);
    }

    /**
     * Change Client Status
     * @param Request $request
     * @param $clientChannelId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getChangeClientChannelStatus(Request $request, $clientChannelId){
        // get client ID
        $tokenChannelDb = TokenChannelPrivilege::find($clientChannelId);
        if (!$tokenChannelDb){
            $request->session()->flash('error','Invalid Client Channel');
            return back();
        }

        $currentStatus = $tokenChannelDb->status;
        if ($currentStatus == 'OPEN'){
            $newStatus = 'CLOSE';
        } else {
            $newStatus = 'OPEN';
        }

        // update
        $tokenChannelDb->status = $newStatus;
        $tokenChannelDb->save();

        $request->session()->flash('success',"Success change to $newStatus for this client");
        return back();
    }
}
