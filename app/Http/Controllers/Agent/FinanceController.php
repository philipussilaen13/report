<?php

namespace App\Http\Controllers\Agent;

use App\Models\Agent\AgentLocker;
use App\Models\Agent\BalanceRecord;
use App\Models\Agent\CommissionSchema;
use App\Models\Agent\Payment;
use App\Models\Agent\PaymentTransfer;
use App\Models\Agent\Transaction;
use App\Models\Agent\TransactionHistory;
use App\Models\Agent\User;
use App\Models\Virtual\Locker;
use App\Models\Virtual\SalesAgent;
use App\Models\Virtual\SalesTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class FinanceController extends Controller
{
    /**
     * Get Credit Page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreditManual(Request $request)
    {
        // get agent locker Only
        $lockerDb = Locker::all();

        // parsing data to view
        $data = [];
        $data['agents'] = $lockerDb;
        return view('agent.finance.credit', $data);
    }

    /**
     * post credit manual
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreditManual(Request $request)
    {
        // create rules
        $rules = [
            'amount' => 'required|numeric',
            'remark' => 'required'
        ];
        $this->validate($request, $rules);

        $creditAmount = $request->input('amount', 0);
        $lockerId = $request->input('lockerId');
        $transactionType = $request->input('type');
        $remark = $request->input('remark');
        $method = $request->input('paymentType', null);
        $bankId = $request->input('availableBank', null);
        $senderName = $request->input('senderName', null);
        $senderBank = $request->input('senderBank', null);

        // get agent user
        $agentUserDb = User::where('locker_id', $lockerId)->first();
        $userId = $agentUserDb->id;

        DB::connection('popbox_agent')->beginTransaction();
        $creditTransactionDb = Transaction::createCreditTransaction($transactionType, $userId, $remark, $creditAmount);
        if (!$creditTransactionDb->isSuccess) {
            $request->session()->flash('error', 'Failed Transaction');
            return back();
        }
        $transactionId = $creditTransactionDb->transactionId;
        $transactionRef = $creditTransactionDb->transactionRef;
        if ($transactionType == 'topup') {
            $paymentDb = Payment::addPayment($transactionRef, $method, 'topup');
            if (!$paymentDb->isSuccess) {
                DB::connection('popbox_agent')->rollback();
                $message = $paymentDb->errorMsg;
                $request->session()->flash('error', $message);
                return back();
            }
            $paymentId = $paymentDb->paymentId;

            if ($method == 'transfer') {
                // insert payment transfer
                $paymentTransferDb = PaymentTransfer::createPaymentTransfer($paymentId, $bankId, $senderName, $senderBank);
                if (!$paymentTransferDb->isSuccess) {
                    DB::connection('popbox_agent')->rollback();
                    $message = $paymentTransferDb->errorMsg;
                    $request->session()->flash('error', $message);
                    return back();
                }
            }
        }

        $balanceRecordDb = BalanceRecord::creditDeposit($lockerId, $creditAmount, $transactionId);
        if (!$balanceRecordDb->isSuccess) {
            $request->session()->flash('error', 'Failed Credit Transaction');
            return back();
        }

        // change to PAID
        $transactionDb = Transaction::find($transactionId);
        $transactionDb->status = 'PAID';
        $transactionDb->save();
        if ($transactionType == 'topup') {
            $paymentDb = Payment::find($transactionDb->payment->id);
            $paymentDb->status = 'PAID';
            $paymentDb->save();
        }

        $transactionId = $transactionDb->id;
        $username = Auth::user()->name;
        $remarks = "Confirmed by $username";
        $historyDb = TransactionHistory::createNewHistory($transactionId, $username, 'PAID', $remarks);
        if (!$historyDb->isSuccess) {
            $request->session()->flash('error', 'Failed To create Transaction History');
            return back();
        }

        DB::connection('popbox_agent')->commit();
        $request->session()->flash('success', 'Success Credit Transaction');
        return back();
    }

    /**
     * Get Deduct Page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDebitManual(Request $request)
    {
        // get agent locker Only
		$lockerDb = Locker::all();

        // parsing data to view
        $data = [];
        $data['agents'] = $lockerDb;
        return view('agent.finance.deduct', $data);
    }

    /**
     * post credit manual
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDebitManual(Request $request)
    {
        // create rules
        $rules = [
            'amount' => 'required|numeric',
            'remark' => 'required'
        ];
        $this->validate($request, $rules);

        $creditAmount = $request->input('amount', 0);
        $lockerId = $request->input('lockerId');
        $transactionType = $request->input('type');
        $remark = $request->input('remark');

        // get agent user
        $agentUserDb = User::where('locker_id', $lockerId)->first();
        $userId = $agentUserDb->id;

        DB::connection('popbox_agent')->beginTransaction();
        $creditTransactionDb = Transaction::createDebitTransaction($transactionType, $userId, $remark, $creditAmount);
        if (!$creditTransactionDb->isSuccess) {
            $request->session()->flash('error', 'Failed Transaction');
            return back();
        }
        $transactionId = $creditTransactionDb->transactionId;

        $balanceRecordDb = BalanceRecord::deductDeposit($lockerId, $creditAmount, $transactionId);
        if (!$balanceRecordDb->isSuccess) {
            $request->session()->flash('error', 'Failed Deduct Transaction');
            return back();
        }

        DB::connection('popbox_agent')->commit();
        $request->session()->flash('success', 'Success Deduct Transaction');
        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function ajaxAgentDetail(Request $request)
    {
        // create default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $lockerId = $request->input('lockerId', null);
        $type = $request->input('type', null);

        if (empty($lockerId) || empty($type)) {
            $response->errorMsg = 'Agent or Type is Empty';
            return response()->json($response);
        }
        $agentUserDb = User::where('locker_id', $lockerId)->first();
        $lockerDb = Locker::where('locker_id', $lockerId)->first();
        $lastTransactionDb = Transaction::where('users_id', $agentUserDb->id)
            ->where('type', 'topup')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        $agentLockerDb = AgentLocker::find($lockerId);

        // define type
        $transactionType = [];
        $paymentType = [];

        $availableBanks = PaymentTransfer::getAvailableBanks();

        if ($type == 'credit') {
            $transactionType = ['topup', 'refund', 'reward', 'commission'];
            $paymentType = [
                'cash' => 'Cash / Tunai',
                'transfer' => 'Bank Transfer'
            ];
        } else {
            $transactionType = ['deduct'];
            $paymentType = [
                'deposit' => 'Deposit Agent'
            ];
        }

        // get agent Detail
        $data = [];
        $data['agentUser'] = $agentUserDb;
        $data['locker'] = $lockerDb;
        $data['transaction'] = $lastTransactionDb;
        $data['agentLocker'] = $agentLockerDb;
        $data['type'] = $type;
        $data['lockerId'] = $lockerId;
        $data['transactionType'] = $transactionType;
        $data['paymentType'] = $paymentType;
        $data['availableBanks'] = $availableBanks;
        $view = view('agent.finance._agentDetail', $data)->render();

        $response->isSuccess = true;
        $response->view = $view;
        $response->data = $data;
        return response()->json($response);
    }

    /* ======================Sales ======================*/

    /**
     * Get Sales Pending Transaction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSalesPending()
    {
        // get sales topup
        $salesTransactionDb = SalesTransaction::where('type', 'sales-topup')
            ->orderBy('created_at', 'desc')
            ->get();

        $salesDb = SalesAgent::get();

        // parse data to view
        $data = [];
        $data['salesTransactionDb'] = $salesTransactionDb;
        $data['salesDb'] = $salesDb;

        return view('agent.finance.sales-pending', $data);
    }

    /**
     * Top Up Sales Manual
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSalesPending(Request $request)
    {
        $salesId = $request->input('salesId', null);
        $amount = $request->input('amount', null);

        if (empty($salesId) || empty($amount)) {
            $request->session()->flash('error', 'Sales Id or Amount cant be Empty');
            return back();
        }

        if (!is_numeric($amount)) {
            $request->session()->flash('error', 'Amount must number');
            return back();
        }

        $salesDb = SalesAgent::find($salesId);
        if (!$salesDb) {
            $request->session()->flash('error', 'Sales not found');
            return back();
        }

        $type = 'sales-topup';
        $remarks = "Top Up Sales Agent $amount";

        DB::beginTransaction();

        $salesTransactionDb = new SalesTransaction();
        $result = $salesTransactionDb->createNewSalesTransaction($salesDb->id, $type, $amount, 'PENDING', $remarks);
        if (!$result->isSuccess) {
            DB::rollback();
            $request->session()->flash('error', $result->errorMsg);
            return back();
        }
        $transactionId = $result->transactionId;
        $salesTransactionDb = SalesTransaction::find($transactionId);
        $salesTransactionDb->status = 'PAID';
        $salesTransactionDb->save();

        $salesAgentDb = new SalesAgent();
        $topup = $salesAgentDb->topUpSales($salesId, $amount);
        if (!$topup->isSuccess) {
            DB::rollback();
            $request->session()->flash('error', $topup->errorMsg);
            return back();
        }

        DB::commit();
        $request->session()->flash('success', 'Success Top Up Sales Agent');
        return back();
    }

    /**
     * Post Paid Sales Pending Transaction
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postConfirmSalesPending(Request $request)
    {
        $salesTransactionId = $request->input('salesTransactionId', null);

        if (empty($salesTransactionId)) {
            $request->session()->flash('error', 'Empty Transaction Id');
            return back();
        }
        // get sales transaction
        $salesTransactionDb = SalesTransaction::find($salesTransactionId);
        if (!$salesTransactionDb) {
            $request->session()->flash('error', 'Sales Transaction not Found');
            return back();
        }
        $status = $salesTransactionDb->status;
        $type = $salesTransactionDb->type;
        $amount = $salesTransactionDb->amount;
        $salesId = $salesTransactionDb->sales_agent_id;

        if ($status == 'PAID') {
            $request->session()->flash('error', 'Sales Transaction already PAID');
            return back();
        }

        if ($type != 'sales-topup') {
            $request->session()->flash('error', 'Transaction Type not sales Top Up');
            return back();
        }

        DB::beginTransaction();
        $salesTransactionDb->status = 'PAID';
        $salesTransactionDb->save();

        $salesAgentDb = new SalesAgent();
        $topup = $salesAgentDb->topUpSales($salesId, $amount);
        if (!$topup->isSuccess) {
            DB::rollback();
            $request->session()->flash('error', 'Failed to Top Up Sales');
            return back();
        }
        DB::commit();

        $request->session()->flash('success', 'Success Paid Agent Top Up');
        return back();
    }

    /**
     * Get Sales Agent Data
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAjaxGetAgent(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $salesId = $request->input('salesId', null);
        if (empty($salesId)) {
            $response->errorMsg = 'Sales Id is Null';
            return response()->json($response);
        }

        // find sales data
        $salesDb = SalesAgent::find($salesId);
        if (!$salesDb) {
            $response->errorMsg = 'Sales Not Found';
            return response()->json($response);
        }

        $data = new \stdClass();
        $data->salesId = $salesDb->id;
        $data->salesName = $salesDb->user->name;
        $data->salesLimit = "Rp " . number_format($salesDb->credit_limit);
        $data->salesBalance = "Rp " . number_format($salesDb->balance);
        $data->unpaidBalance = "Rp " . number_format($salesDb->credit_limit - $salesDb->balance);

        $response->data = $data;
        $response->isSuccess = true;
        return response()->json($response);
    }

    /*====================== Finance ======================*/

    /**
     * Get Agent and Export Agent Transactional Report
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function MemberSummary(Request $request)
    {
        // define start and end date
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $agentSummary = [];

        $transactionList = DB::connection('popbox_agent')
            ->table('transactions')
            ->join('users','users.id','=','transactions.users_id')
            ->leftJoin('lockers', 'lockers.id', '=', 'users.locker_id')
            ->leftJoin('balance_records','balance_records.transactions_id','=','transactions.id')
            ->leftJoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftJoin('payment_methods', 'payment_methods.id', '=', 'payments.payment_methods_id')
            ->whereIn('transactions.status',['PAID','REFUND'])
            ->whereBetween('transactions.updated_at', [$startDate, $endDate])
            ->select('transactions.type', 'transactions.total_price', 'balance_records.*', 'lockers.locker_name', 'payment_methods.code','users.id as user_id')
            ->orderBy('transactions.created_at','asc')
            ->get();

        //$typeTransaction = ['popshop', 'pulsa', 'electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid', 'delivery'];
        $typeTransaction = ['payment', 'purchase'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward', 'commission'];

        $transactionGroup = $transactionList->groupBy('lockers_id');
        foreach ($transactionGroup as $lockerId => $transaction) {
            $topUpAmount = 0;
            $transactionAmount = 0;
            $rewardAmount = 0;
            $otherAmount = 0;
            $deductAmount = 0;
            $firstDeposit = 0;
            $lastBalance = 0;
            $refundAmount = 0;
            $adminAmount = 0;
            $receivedMoney = 0;
            $lockerName = null;

            $admin25 = ['bni_va'];
            $admin45 = ['doku_va', 'doku_mandiri_va', 'doku_permata_va', 'doku_bca_va'];
            $admin50 = ['doku_alfamart_va'];

            foreach ($transaction as $item) {
                if (in_array($item->type, $typeTransaction)) {
                    $transactionAmount += $item->total_price;
                } elseif (in_array($item->type, $typeTopUp)) {
                    $topUpAmount += $item->total_price;
                    $paymentMethodCode = $item->code;
                    if ($paymentMethodCode == 'cash' || $paymentMethodCode == 'transfer') {
                        $receivedMoney += $item->total_price;
                    } elseif (in_array($paymentMethodCode, $admin25)) {
                        $adminAmount += 2500;
                        $receivedMoney += ($item->total_price - 2500);
                    } elseif (in_array($paymentMethodCode, $admin45)) {
                        $adminAmount += 4500;
                        $receivedMoney += ($item->total_price - 4500);
                    } elseif (in_array($paymentMethodCode, $admin50)) {
                        $adminAmount += 5000;
                        $receivedMoney += ($item->total_price - 5000);
                    }
                } elseif (in_array($item->type, $typeReward)) {
                    $rewardAmount += $item->total_price;
                } elseif ($item->type == 'deduct') {
                    $deductAmount += $item->total_price;
                } elseif ($item->type == 'refund') {
                    $refundAmount += $item->total_price;
                }
                $lastBalance = $item->balance;
                $lockerName = $item->locker_name;
            }

            // get first Deposit
            //DB::connection('popbox_agent')->enableQueryLog();
            $balanceRecordDb = BalanceRecord::where('updated_at','<',$startDate)
                ->where('lockers_id', $lockerId)
                ->orderBy('id','desc')
                ->first();
            if ($balanceRecordDb) {
                $firstDeposit = $balanceRecordDb->balance;
            }
            //dd(DB::connection('popbox_agent')->getQueryLog());

            $tmp = new \stdClass();
            $tmp->agentId = $lockerId;
            $tmp->agentName = $lockerName;
            $tmp->firstDeposit = $firstDeposit;
            $tmp->topupAmount = $topUpAmount - $deductAmount;
            $tmp->transactionAmount = $transactionAmount;
            $tmp->rewardAmount = $rewardAmount;
            $tmp->lastBalance = $lastBalance;
            $tmp->refundAmount = $refundAmount;
            $tmp->otherAmount = $otherAmount;
            $tmp->adminAmount = $adminAmount;
            $tmp->receivedMoney = $receivedMoney;

            $agentSummary[] = $tmp;
        }

        $parameter = [];
        $parameter['beginDate'] = $startDate;
        $parameter['endDate'] = $endDate;

        $data = [];
        $data['parameter'] = $parameter;
        $data['agentSummary'] = $agentSummary;

        if ($request->method() == 'POST') {
            $fileName = "Agent Rekap " . $startDate . " sd " . $endDate;
            Excel::create($fileName, function ($excel) use ($agentSummary, $fileName) {
                $excel->setTitle($fileName);
                $excel->setCreator('Report PopBox')->setCompany('PopBox Asia');
                $excel->setDescription('Report Agent Summary');
                $excel->sheet('summary', function ($sheet) use ($agentSummary) {
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('No');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('Agent Id');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('Deposit Awal');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('Jumlah TopUp');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('Nilai Pemakaian');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('Bonus');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('Refund');
                    });
                    $sheet->cell('H1', function ($cell) {
                        $cell->setValue('Sisa Deposit');
                    });
                    $sheet->cell('I1', function ($cell) {
                        $cell->setValue('Biaya Admin');
                    });
                    $sheet->cell('J1', function ($cell) {
                        $cell->setValue('Uang Diterima');
                    });

                    $cellNumberStart = 2;
                    foreach ($agentSummary as $index => $item) {
                        $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                            $cell->setValue($index + 1);
                        });
                        $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->agentId . " | $item->agentName");
                        });
                        $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->firstDeposit);
                        });
                        $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->topupAmount);
                        });
                        $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->transactionAmount);
                        });
                        $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->rewardAmount);
                        });
                        $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->refundAmount);
                        });
                        $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->lastBalance);
                        });
                        $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->adminAmount);
                        });
                        $sheet->cell("J$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->receivedMoney);
                        });
                        $cellNumberStart++;
                    }
                });
            })->download('xlsx');
        }

        return view('agent.finance.report-member', $data);
    }

    public function MemberSummaryV2(Request $request)
    {
        // define start and end date
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $agentSummary = [];

        $rawQuery = 'SELECT transactions.type, transactions.total_price, balance_records.*, lockers.locker_name, payment_methods.code, users.id AS user_id FROM users LEFT JOIN transactions ON transactions.users_id = users.id AND transactions.updated_at >= date("2018-08-01") AND transactions.updated_at <= date("2018-08-30") AND transactions.status IN ("PAID", "REFUND") LEFT JOIN lockers ON lockers.id = users.locker_id LEFT JOIN balance_records ON balance_records.transactions_id = transactions.id LEFT JOIN payments ON payments.transactions_id = transactions.id LEFT JOIN payment_methods ON payment_methods.id = payments.payment_methods_id ORDER BY transactions.created_at';

        $transactionList = DB::connection('popbox_agent')->select(DB::raw($rawQuery));

        //$typeTransaction = ['popshop', 'pulsa', 'electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid', 'delivery'];
        $typeTransaction = ['payment', 'purchase'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward', 'commission'];

        $transactionGroup = collect($transactionList)->groupBy('locker_name');

        foreach ($transactionGroup as $lockerId => $transaction) {
            $topUpAmount = 0;
            $transactionAmount = 0;
            $rewardAmount = 0;
            $otherAmount = 0;
            $deductAmount = 0;
            $firstDeposit = 0;
            $lastBalance = 0;
            $refundAmount = 0;
            $adminAmount = 0;
            $receivedMoney = 0;
            $lockerName = null;

            $admin25 = ['bni_va'];
            $admin45 = ['doku_va', 'doku_mandiri_va', 'doku_permata_va', 'doku_bca_va'];
            $admin50 = ['doku_alfamart_va'];

            foreach ($transaction as $item) {
                if (in_array($item->type, $typeTransaction)) {
                    $transactionAmount += $item->total_price;
                } elseif (in_array($item->type, $typeTopUp)) {
                    $topUpAmount += $item->total_price;
                    $paymentMethodCode = $item->code;
                    if ($paymentMethodCode == 'cash' || $paymentMethodCode == 'transfer') {
                        $receivedMoney += $item->total_price;
                    } elseif (in_array($paymentMethodCode, $admin25)) {
                        $adminAmount += 2500;
                        $receivedMoney += ($item->total_price - 2500);
                    } elseif (in_array($paymentMethodCode, $admin45)) {
                        $adminAmount += 4500;
                        $receivedMoney += ($item->total_price - 4500);
                    } elseif (in_array($paymentMethodCode, $admin50)) {
                        $adminAmount += 5000;
                        $receivedMoney += ($item->total_price - 5000);
                    }
                } elseif (in_array($item->type, $typeReward)) {
                    $rewardAmount += $item->total_price;
                } elseif ($item->type == 'deduct') {
                    $deductAmount += $item->total_price;
                } elseif ($item->type == 'refund') {
                    $refundAmount += $item->total_price;
                }
                $lastBalance = $item->balance;
                $lockerName = $item->locker_name;
            }

            // get first Deposit
            //DB::connection('popbox_agent')->enableQueryLog();
            $balanceRecordDb = BalanceRecord::where('updated_at','<',$startDate)
                ->where('lockers_id', $lockerId)
                ->orderBy('id','desc')
                ->first();
            if ($balanceRecordDb) {
                $firstDeposit = $balanceRecordDb->balance;
            }
            //dd(DB::connection('popbox_agent')->getQueryLog());

            $tmp = new \stdClass();
            $tmp->agentId = $lockerId;
            $tmp->agentName = $lockerName;
            $tmp->firstDeposit = $firstDeposit;
            $tmp->topupAmount = $topUpAmount - $deductAmount;
            $tmp->transactionAmount = $transactionAmount;
            $tmp->rewardAmount = $rewardAmount;
            $tmp->lastBalance = $lastBalance;
            $tmp->refundAmount = $refundAmount;
            $tmp->otherAmount = $otherAmount;
            $tmp->adminAmount = $adminAmount;
            $tmp->receivedMoney = $receivedMoney;

            $agentSummary[] = $tmp;
        }

        $parameter = [];
        $parameter['beginDate'] = $startDate;
        $parameter['endDate'] = $endDate;

        $data = [];
        $data['parameter'] = $parameter;
        $data['agentSummary'] = $agentSummary;

        if ($request->method() == 'POST') {
            $fileName = "Agent Rekap " . $startDate . " sd " . $endDate;
            Excel::create($fileName, function ($excel) use ($agentSummary, $fileName) {
                $excel->setTitle($fileName);
                $excel->setCreator('Report PopBox')->setCompany('PopBox Asia');
                $excel->setDescription('Report Agent Summary');
                $excel->sheet('summary', function ($sheet) use ($agentSummary) {
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('No');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('Agent Id');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('Deposit Awal');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('Jumlah TopUp');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('Nilai Pemakaian');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('Bonus');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('Refund');
                    });
                    $sheet->cell('H1', function ($cell) {
                        $cell->setValue('Sisa Deposit');
                    });
                    $sheet->cell('I1', function ($cell) {
                        $cell->setValue('Biaya Admin');
                    });
                    $sheet->cell('J1', function ($cell) {
                        $cell->setValue('Uang Diterima');
                    });

                    $cellNumberStart = 2;
                    foreach ($agentSummary as $index => $item) {
                        $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                            $cell->setValue($index + 1);
                        });
                        $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->agentId . " | $item->agentName");
                        });
                        $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->firstDeposit);
                        });
                        $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->topupAmount);
                        });
                        $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->transactionAmount);
                        });
                        $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->rewardAmount);
                        });
                        $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->refundAmount);
                        });
                        $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->lastBalance);
                        });
                        $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->adminAmount);
                        });
                        $sheet->cell("J$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->receivedMoney);
                        });
                        $cellNumberStart++;
                    }
                });
            })->download('xlsx');
        }

        return view('agent.finance.report-member2', $data);
    }

    /**
     * Get and Export Agent Pulsa Report
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function PulsaSummary(Request $request)
    {
        // define start and end date
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $transactionList = DB::connection('popbox_agent')
            ->table('transactions')
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->leftJoin('lockers', 'lockers.id', '=', 'users.locker_id')
            ->where('transactions.status', '=', 'PAID')
            ->where('transaction_items.type', 'pulsa')
            ->whereBetween('transactions.updated_at', [$startDate, $endDate])
            ->select('transaction_items.*', 'users.locker_id', 'lockers.locker_name')
            ->orderBy('transactions.created_at', 'asc')
            ->get();

        $transactionGroup = $transactionList->groupBy('locker_id');
        $agentSummary = [];
        foreach ($transactionGroup as $lockerId => $transaction) {
            $customerPrice = 0;
            $dppCustomerPrice = 0;
            $ppn = 0;
            $bonus = 0;
            $tmpSepulsaId = [];
            $tmpCommission = [];
            $agentName = null;
            foreach ($transaction as $item) {
                $customerPrice += $item->price;
                $commissionId = $item->commission_schemas_id;
                $price = $item->price;
                if (!empty($item->reference)) $tmpSepulsaId[] = $item->reference;
                $tmpCommission[$commissionId][] = $price;
                $agentName = $item->locker_name;
            }

            foreach ($tmpCommission as $schemaId => $price) {
                $schemaCalculate = CommissionSchema::calculateByIdArrPrice($schemaId, $tmpCommission[$schemaId]);
                if ($schemaCalculate->isSuccess) {
                    $bonus += $schemaCalculate->commission;
                }
            }

            // get partner price
            $sepulsaTransaction = DB::connection('popbox_db')
                ->table('tb_sepulsa_transaction')
                ->whereIn('invoice_id', $tmpSepulsaId)
                ->get();

            $partnerPrice = 0;
            $dppPartnerPrice = 0;
            $ppnPartner = 0;
            foreach ($sepulsaTransaction as $item) {
                $partnerPrice += $item->product_price;
            }

            $dppCustomerPrice = ceil($customerPrice / 1.1);
            $ppn = $customerPrice - $dppCustomerPrice;

            $dppPartnerPrice = ceil($partnerPrice / 1.1);
            $ppnPartner = $partnerPrice - $dppPartnerPrice;

            $tmp = new \stdClass();
            $tmp->agentId = $lockerId;
            $tmp->agentName = $agentName;
            $tmp->customerPrice = $customerPrice;
            $tmp->dppCustomerPrice = $dppCustomerPrice;
            $tmp->ppn = $ppn;
            $tmp->partnerPrice = $partnerPrice;
            $tmp->dppPartnerPrice = $dppPartnerPrice;
            $tmp->ppnPartner = $ppnPartner;
            $tmp->bonus = $bonus;
            $agentSummary[] = $tmp;
        }

        $parameter = [];
        $parameter['beginDate'] = $startDate;
        $parameter['endDate'] = $endDate;

        $data = [];
        $data['parameter'] = $parameter;
        $data['agentSummary'] = $agentSummary;

        if ($request->method() == 'POST') {
            $fileName = "Agent Pulsa Rekap " . $startDate . " sd " . $endDate;
            Excel::create($fileName, function ($excel) use ($agentSummary, $fileName) {
                $excel->setTitle($fileName);
                $excel->setCreator('Report PopBox')->setCompany('PopBox Asia');
                $excel->setDescription('Report Agent Summary');
                $excel->sheet('summary', function ($sheet) use ($agentSummary) {
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('No');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('Agent Id');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('Customer Price');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('DPP Customer Price');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('PPN');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('Partner Price');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('DPP Partner Price');
                    });
                    $sheet->cell('H1', function ($cell) {
                        $cell->setValue('PPN Partner');
                    });
                    $sheet->cell('I1', function ($cell) {
                        $cell->setValue('Bonus');
                    });

                    $cellNumberStart = 2;
                    foreach ($agentSummary as $index => $item) {
                        $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                            $cell->setValue($index + 1);
                        });
                        $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->agentId);
                        });
                        $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->customerPrice);
                        });
                        $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->dppCustomerPrice);
                        });
                        $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->ppn);
                        });
                        $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->partnerPrice);
                        });
                        $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->dppPartnerPrice);
                        });
                        $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->ppnPartner);
                        });
                        $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->bonus);
                        });
                        $cellNumberStart++;
                    }
                });
            })->download('xlsx');
        }

        return view('agent.finance.report-pulsa', $data);
    }

    /**
     * Get and Export Agent Payment Report
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paymentSummary(Request $request)
    {
        // define start and end date
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $agentSummary = [];
        $transactionType = ['electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid'];

        foreach ($transactionType as $type) {
            $transactionList = DB::connection('popbox_agent')
                ->table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                ->where('transactions.status', '=', 'PAID')
                ->where('transactions.type', 'payment')
                ->whereBetween('transactions.updated_at', [$startDate, $endDate])
                ->whereIn('transactions.id', function ($query) use ($type) {
                    $query->select('transaction_id')->from('transaction_items')->where('type', $type);
                })->select('transaction_items.*', 'users.locker_id', 'transactions.total_price')
                ->orderBy('transactions.created_at', 'asc')
                ->get();

            $transactionGroup = $transactionList->groupBy('locker_id');
            foreach ($transactionGroup as $lockerId => $transaction) {
                $basePrice = 0;
                $adminFee = 0;
                $customerPrice = 0;
                $sepulsaPrice = 0;
                $adminFeePartner = 0;
                $bonus = 0;
                $tmpSepulsaId = [];
                $tmpCommission = [];

                foreach ($transaction as $item) {
                    if ($item->type == $type) {
                        $basePrice += $item->price;
                        // get data for commission / bonus calculation
                        $commissionId = $item->commission_schemas_id;
                        $price = $item->price;
                        if (!empty($item->reference)) $tmpSepulsaId[] = $item->reference;
                        $tmpCommission[$commissionId][] = $price;
                    } elseif ($item->type == 'admin_fee') {
                        $adminFee += $item->price;
                    }
                    $customerPrice += $item->price;
                }
                foreach ($tmpCommission as $schemaId => $price) {
                    $schemaCalculate = CommissionSchema::calculateByIdArrPrice($schemaId, $tmpCommission[$schemaId]);
                    if ($schemaCalculate->isSuccess) {
                        $bonus += $schemaCalculate->commission;
                    }
                }

                $sepulsaTransaction = DB::connection('popbox_db')
                    ->table('tb_sepulsa_transaction')
                    ->where('product_type', $type)
                    ->whereIn('invoice_id', $tmpSepulsaId)
                    ->get();
                if ($type == 'electricity_postpaid' || $type == 'pdam' || $type == 'telkom_postpaid' || $type == 'bpjs_kesehatan') {
                    foreach ($sepulsaTransaction as $item) {
                        $sepulsaPrice += $item->product_amount + $item->product_price;
                        $adminFeePartner += $item->product_price;
                    }
                } else {
                    foreach ($sepulsaTransaction as $item) {
                        $sepulsaPrice += $item->product_price;
                    }
                    $adminFeePartner = $customerPrice - $sepulsaPrice;
                }

                $tmp = new \stdClass();
                $tmp->basePrice = $basePrice;
                $tmp->adminFee = $adminFee;
                $tmp->customerPrice = $customerPrice;
                $tmp->sepulsaPrice = $sepulsaPrice;
                $tmp->adminFeePartner = $adminFeePartner;
                $tmp->bonus = $bonus;
                $agentSummary[$lockerId][$type] = $tmp;
            }
        }

        $parameter = [];
        $parameter['beginDate'] = $startDate;
        $parameter['endDate'] = $endDate;

        $data = [];
        $data['parameter'] = $parameter;
        $data['agentSummary'] = $agentSummary;

        $method = $request->method();
        if ($method == 'POST') {
            $fileName = "Agent Pembayaran Rekap " . $startDate . " sd " . $endDate;
            Excel::create($fileName, function ($excel) use ($agentSummary, $fileName) {
                $excel->setTitle($fileName);
                $excel->setCreator('Report PopBox')->setCompany('PopBox Asia');
                $excel->setDescription('Report Agent Pembayaran Summary');
                $excel->sheet('summary', function ($sheet) use ($agentSummary) {
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('No');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('Agent Id');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('Type');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('Base Price');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('Admin Fee');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('Customer Price');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('Sepulsa Price');
                    });
                    $sheet->cell('H1', function ($cell) {
                        $cell->setValue('Admin Fee Partner');
                    });
                    $sheet->cell('I1', function ($cell) {
                        $cell->setValue('Bonus');
                    });

                    $cellNumberStart = 2;
                    $index = 1;
                    foreach ($agentSummary as $lockerId => $typeList) {
                        foreach ($typeList as $type => $item) {
                            $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                                $cell->setValue($index);
                            });
                            $sheet->cell("B$cellNumberStart", function ($cell) use ($lockerId) {
                                $cell->setValue($lockerId);
                            });
                            $sheet->cell("C$cellNumberStart", function ($cell) use ($type) {
                                $cell->setValue($type);
                            });
                            $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                                $cell->setValue($item->basePrice);
                            });
                            $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                                $cell->setValue($item->adminFee);
                            });
                            $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                                $cell->setValue($item->customerPrice);
                            });
                            $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                                $cell->setValue($item->sepulsaPrice);
                            });
                            $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                                $cell->setValue($item->adminFeePartner);
                            });
                            $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                                $cell->setValue($item->bonus);
                            });
                            $cellNumberStart++;
                            $index++;
                        }
                    }
                });
            })->download('xlsx');
        }

        return view('agent.finance.report-payment', $data);
    }

    /**
     * Get and Export Agent PopShop Report
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function PopshopSummary(Request $request)
    {
        // define start and end date
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $transactionList = DB::connection('popbox_agent')
            ->table('transactions')
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->leftJoin('lockers', 'lockers.id', '=', 'users.locker_id')
            ->where('transactions.status', '=', 'PAID')
            ->where('transaction_items.type', 'popshop')
            ->whereBetween('transactions.updated_at', [$startDate, $endDate])
            ->select('transaction_items.*', 'users.locker_id', 'lockers.locker_name')
            ->orderBy('transactions.created_at', 'asc')
            ->get();

        $transactionGroup = $transactionList->groupBy('locker_id');
        $agentSummary = [];
        foreach ($transactionGroup as $lockerId => $transaction) {
            $customerPrice = 0;
            $dppCustomerPrice = 0;
            $ppn = 0;
            $bonus = 0;
            $partnerPrice = 0;
            $dppPartnerPrice = 0;
            $ppnPartner = 0;
            $agentName = null;
            foreach ($transaction as $item) {
                $customerPrice = $item->price;
                $commissionId = $item->commission_schemas_id;
                $price = $item->price;
                $transactionId = $item->reference;

                $commission = CommissionSchema::calculateById($commissionId, $price, $price);
                if ($commission->isSuccess) {
                    $bonus = $commission->commission;
                }

                $agentName = $item->locker_name;

                $dppCustomerPrice = round($customerPrice / 1.1);
                $ppn = $customerPrice - $dppCustomerPrice;

                $tmp = new \stdClass();
                $tmp->agentId = $lockerId;
                $tmp->agentName = $agentName;
                $tmp->transactionId = $transactionId;
                $tmp->customerPrice = $customerPrice;
                $tmp->dppCustomerPrice = $dppCustomerPrice;
                $tmp->ppn = $ppn;
                $tmp->partnerPrice = $partnerPrice;
                $tmp->dppPartnerPrice = $dppPartnerPrice;
                $tmp->ppnPartner = $ppnPartner;
                $tmp->bonus = $bonus;
                $agentSummary[] = $tmp;
            }
        }

        $parameter = [];
        $parameter['beginDate'] = $startDate;
        $parameter['endDate'] = $endDate;

        $data = [];
        $data['parameter'] = $parameter;
        $data['agentSummary'] = $agentSummary;

        if ($request->method() == 'POST') {
            $fileName = "Agent PopShop Rekap " . $startDate . " sd " . $endDate;
            Excel::create($fileName, function ($excel) use ($agentSummary, $fileName) {
                $excel->setTitle($fileName);
                $excel->setCreator('Report PopBox')->setCompany('PopBox Asia');
                $excel->setDescription('Report Agent Summary');
                $excel->sheet('summary', function ($sheet) use ($agentSummary) {
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('No');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('Agent Id');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('Transaction Id');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('Customer Price');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('DPP Customer Price');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('PPN');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('Partner Price');
                    });
                    $sheet->cell('H1', function ($cell) {
                        $cell->setValue('DPP Partner Price');
                    });
                    $sheet->cell('I1', function ($cell) {
                        $cell->setValue('PPN Partner');
                    });
                    $sheet->cell('J1', function ($cell) {
                        $cell->setValue('Bonus');
                    });

                    $cellNumberStart = 2;
                    foreach ($agentSummary as $index => $item) {
                        $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                            $cell->setValue($index + 1);
                        });
                        $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->agentId);
                        });
                        $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->transactionId);
                        });
                        $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->customerPrice);
                        });
                        $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->dppCustomerPrice);
                        });
                        $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->ppn);
                        });
                        $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->partnerPrice);
                        });
                        $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->dppPartnerPrice);
                        });
                        $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->ppnPartner);
                        });
                        $sheet->cell("J$cellNumberStart", function ($cell) use ($item) {
                            $cell->setValue($item->bonus);
                        });
                        $cellNumberStart++;
                    }
                });
            })->download('xlsx');
        }

        return view('agent.finance.report-popshop', $data);
    }
}
