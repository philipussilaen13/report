<?php

namespace App\Http\Controllers\Agent;

use App\Http\Helpers\APIv2;
use App\Http\Helpers\ApiPopWarung;
use App\Http\Helpers\GoogleAPI;
use App\Http\Helpers\Helper;
use App\Jobs\ProcessBankTransfer;
use App\Jobs\SendFCM;
use App\Models\Agent\AgentLocker;
use App\Models\Agent\BalanceRecord;
use App\Models\Agent\CommissionSchema;
use App\Models\Agent\NotificationFCM;
use App\Models\Agent\Payment;
use App\Models\Agent\Transaction;
use App\Models\Agent\TransactionHistory;
use App\Models\Agent\TransactionItem;
use App\Models\Agent\User;
use App\Models\Popshop\DimoProduct;
use App\Models\Popshop\Order;
use App\Models\Virtual\City;
use App\Models\Virtual\Locker;
use App\Models\Virtual\Province;
use Carbon\Carbon;
use DateTime;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class TransactionController extends Controller
{
    /**
     * Get Landing
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLanding(Request $request)
    {
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        // get all parameter
        $lockerId = $request->input('locker', null);
        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $transactionId = $request->input('transactionId', null);
        $itemParam = $request->input('itemParam', null);
        $itemReference = $request->input('itemReference', null);

        /*========= create data for filter =========*/
        // $lockerDb = Locker::where('status', '<>', '0')->where('type', 'agent')->get();

        // create parameter to send to view for data processing
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // parsing data to view
        $data = [];
        $data['parameter'] = $param;
        // $data['lockerData'] = $lockerDb;

        return view('agent.transaction.landing', $data);
    }

    /**
     * Get Transaction List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList(Request $request){
        
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        // get all parameter
        $lockerId = $request->input('locker', null);
        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $typeeee = $request->input('typeeee', null);
        $transactionId = $request->input('transactionId', null);
        $itemParam = $request->input('itemParam', null);
        $itemReference = $request->input('itemReference', null);
        $p_sales_transaction = $request->input('p_sales_transaction', null);
        $p_city = $request->input('city', null);
        
        $p_data = [];
        
        if(!empty($p_sales_transaction)){
            $p_data = $this->getDataTrxWarungBySalesTransaction(trim($p_sales_transaction));
        }
        
        // get all transaction
        //DB::connection('popbox_agent')->enableQueryLog();
        $allTransaction = Transaction::with('items')
            ->withTrashed()
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->join('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'users.locker_id')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('payment_methods', 'payment_methods.id', '=', 'payments.payment_methods_id')
            ->leftjoin('popbox_virtual.cities', 'popbox_virtual.cities.id', 'popbox_virtual.lockers.cities_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('users.locker_id', $lockerId);
            })->when($status, function ($query) use ($status) {
                return $query->where('transactions.status', $status);
            })->when($type, function ($query) use ($type) {
                if (in_array('commission',$type) || in_array('reward',$type)){
                    return $query->whereIn('transactions.type',$type);
                }
                return $query->whereHas('items',function ($query) use ($type){
                    if (is_array($type)) $query->whereIn('type',$type);
                    else  $query->where('type',$type);
                });
                /*return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->whereIn('transaction_items.type', $type);*/
            })->when($typeeee, function ($query) use ($typeeee) {
                return $query->where('popbox_virtual.lockers.type', $typeeee);
            })->when($transactionId, function ($query) use ($transactionId) {
                return $query->where('reference', $transactionId);
            })->when($itemParam, function ($query) use ($itemParam) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.params', 'LIKE', "%$itemParam%");
            })->when($itemReference, function ($query) use ($itemReference) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                ->where('transaction_items.reference', $itemReference);
            })->when($p_data, function ($query) use ($p_data) {
                return $query->leftjoin('payment_non_va', 'payment_non_va.transaction_id', 'transactions.reference')
                ->where('payment_non_va.payment_id', $p_data['payment_id']);
            })->when($p_city, function ($query) use ($p_city) {
                return $query->where('popbox_virtual.cities.id', $p_city);
            })
            ->orderBy('transactions.created_at', 'desc')
            ->select('transactions.*', 'users.locker_id', 'popbox_virtual.cities.city_name')
            ->with('items')
            ->with('user.locker')
            ->paginate(15);

        $sales_transaction = [];
        
        foreach ($allTransaction as $key => $element){
            if(!empty($p_data) && (sizeof($allTransaction) == 1)
                && isset($p_data['sales_transaction'])
                && isset($p_data['locker_id'])
                && isset($p_data['trx_id'])
                && isset($p_data['payment_id']) ){
                    
                $sales_trx = $p_data;
                    
            } else {
                $sales_trx = $this->getTrxWarung($element->reference);
            }
            $sales_transaction[$element->reference] = isset($sales_trx['sales_transaction']) ? [$sales_trx['sales_transaction'],$sales_trx['trx_id'],$sales_trx['locker_id']] : '-';
        }

        /*========= create data for filter =========*/
        $lockerDb = Locker::where('status', '<>', '0')->where('type', '!=', 'locker')->get();
        $cities = City::orderByRaw('city_name ASC')->get();
        
        // create parameter to send to view for data processing
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // parsing data to view
        $data = [];
        $data['parameter'] = $param;
        $data['allTransaction'] = $allTransaction->appends($request->input());
        $data['lockerData'] = $lockerDb;
        $data['transactionId'] = $transactionId;
        $data['sales_transaction'] = $sales_transaction;
        $data['p_sales_transaction'] = $p_sales_transaction;
        $data['cities'] = $cities;
        return view('agent.transaction.list', $data);
    }

    /*-----------------------------------------AGENT TRANSACTION LIST V2----------------------------------------------------------------------*/

    public function getListV2() {

        return view('agent.transaction.list2');
    }

    public function getAjaxFilterDropdownDataTransaction() {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        /*========= create data for filter =========*/
		// $lockerDb = Locker::where('type', '!=', 'locker')->get();
        $lockerDb = Locker::get();
        $cities = City::orderByRaw('city_name ASC')->get();

        $data = [];
        $data['lockerData'] = $lockerDb;
        $data['cities'] = $cities;
        
        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getAjaxFilterTransaction(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $allTransaction = $this->getAllTransactionQuery($request, '');
        
        $resultList = [];
        foreach ($allTransaction as $item){
            $sales_trx = $this->getTrxWarung($item->reference);
            $item['sales_transaction'] = isset($sales_trx['sales_transaction']) ? [$sales_trx['sales_transaction'],$sales_trx['trx_id'],$sales_trx['locker_id']] : '-';
            $resultList[] = $item;
        }
        
        $data = [];

        $data['param'] = $request->input();
        $data['resultList'] = $resultList;
        
        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getAjaxTransactionDownload(Request $request) {

        set_time_limit(0);
        ini_set('memory_limit', '2048M');

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // Request Input
//        $listLockerId = $request->input('listlockerid', null);
        $dateRange = $request->input('daterange', null);

        $listLockerId = $this->getAllTransactionQuery($request, 'download');

        $startDate = null;
        $endDate = null;

        $startDateFilename = null;
        $endDateFilename = null;

        if (!empty($dateRange)) {
            $startDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRange, ',')->endDate;

            $startDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $startDate)->format('d M Y');
            $endDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $endDate)->format('d M Y');
        }
        
        $sales_transaction = [];
        
        foreach ($listLockerId as $key => $element){
            
            $sales_trx = $this->getTrxWarung($element->transaction_id);
            
            $sales_transaction[$element->transaction_id] = isset($sales_trx['sales_transaction']) ? [$sales_trx['sales_transaction'],$sales_trx['trx_id'],$sales_trx['locker_id']] : '-';
        }

        // Init Data Excel
        $filename = "[".date('ymd')."]"." Transaction List";

        $myFile = Excel::create($filename, function ($excel) use ($listLockerId, $sales_transaction, $startDateFilename, $endDateFilename) {
            $excel->sheet('Sheet 1', function ($sheet) use ($listLockerId, $sales_transaction, $startDateFilename, $endDateFilename) {

                $sheet->cell('B1:C1:D1:B2:C2:D2', function($cell) {

                    $cell->setFontSize(26);
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('B1:D1');
                $sheet->mergeCells('B2:D2');

                $header = $startDateFilename." - ".$endDateFilename;
                $sheet->row(1, ['', 'Transaction List']);
                $sheet->row(2, ['', $header]);

                $row = 4;
                $no_urut = 1;

                $arr_title = ['No', 'Transaction Id', 'Sales Transaction', 'Agent Name', 'User Type', 'User Status', 'Kota', 'Type', 'Status', 'Payment Method', 'Tanggal Transaksi', 'Jumlah Transaksi', 'Item'];

                $sheet->row($row, $arr_title);

                foreach ($listLockerId as $index => $item) {

                    $row++;

                    $arr = [$no_urut,
                        $item->transaction_id,
                        $sales_transaction[$item->transaction_id][0],
                        $item->agent_name,
                        $item->user_type,
                        $item->user_status,
                        $item->city_name,
                        $item->types,
                        $item->status,
                        $item->payment_method,
                        $item->created_at,
                        $item->total_price,
                        $item->items
                    ];

                    $sheet->row($row, $arr);
                    $no_urut++;

                }

            });
        })->string('xlsx');//change xlsx for the format you want, default is xls
        
        $response =  array(
            'name' => $filename, //no extention needed
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        
        return response()->json($response);

    }

    /**
     * Export Agent Performance
     * @param Request $request
     */
    public function getAjaxPerformanceTransactionDownload(Request $request){

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $startDate = null;
        $endDate = null;

        $startDateFilename = null;
        $endDateFilename = null;

        $dateRange = $request->input('dateRange', null);
        if (!empty($dateRange)) {
            $startDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRange, ',')->endDate;

            $startDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $startDate)->format('d M Y');
            $endDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $endDate)->format('d M Y');
        }

        $agentSummary = $this->getDataDownloadPerformance($startDate, $endDate);

        /*Begin Excel Process*/
//        $filename = $startDateFilename." s/d ".$endDateFilename." Agent Performance";
        $filename = "[".date('ymd')."]"." Agent Performance";

        Excel::create($filename, function ($excel) use ($agentSummary, $startDateFilename, $endDateFilename) {
            $excel->sheet('Sheet 1', function ($sheet) use ($agentSummary, $startDateFilename, $endDateFilename) {

                $sheet->cell('B1:C1:D1:B2:C2:D2', function($cell) {

                    $cell->setFontSize(26);
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('B1:D1');
                $sheet->mergeCells('B2:D2');

                $header = $startDateFilename." - ".$endDateFilename;
                $sheet->row(1, ['', 'Agent Performance']);
                $sheet->row(2, ['', $header]);

                $row = 4;
                $no_urut = 1;

                $arr_title = ['No', 'Agent ID', 'Agent Name', 'Agent Type', 'Registered By', 'Register Date', 'Last Balance',
                    'Topup Count', 'Topup Amount', 'Transaction PAID Count', 'Transaction PAID Amount', 'Reward Referral',
                    'Refund Amount', 'Commission Amount', 'Pulsa Count', 'Pulsa Amount', 'Popshop Count', 'Popshop Amount',
                    'PLN Token Count', 'PLN Token Amount', 'PLN Count', 'PLN Amount', 'Telkom Count', 'Telkom Amount',
                    'PDAM Count', 'PDAM Amount', 'BPJS Count', 'BPJS Amount'];

                $sheet->row($row, $arr_title);

                foreach ($agentSummary as $index => $item) {

                    $row++;

                    $arr = [$no_urut,
                        $item->agentId,
                        $item->agentName,
                        $item->buildingType,
                        $item->registeredBy,
                        $item->registerDate,
                        $item->lastBalance,
                        $item->topupCount,
                        $item->topupAmount,
                        $item->transactionCount,
                        $item->transactionAmount,
                        $item->rewardAmount,
                        $item->refundAmount,
                        $item->commissionAmount,
                        $item->pulsaCount,
                        $item->pulsaAmount,
                        $item->popshopCount,
                        $item->popshopAmount,
                        $item->plnTokenCount,
                        $item->plnTokenAmount,
                        $item->plnPascaCount,
                        $item->plnPascaAmount,
                        $item->telkomCount,
                        $item->telkomAmount,
                        $item->pdamCount,
                        $item->pdamAmount,
                        $item->bpjsCount,
                        $item->bpjsAmount
                    ];

                    $sheet->row($row, $arr);
                    $no_urut++;

                }

            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getAjaxNonGroupTransactionDownload(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $startDate = null;
        $endDate = null;

        $startDateFilename = null;
        $endDateFilename = null;

        $dateRange = $request->input('dateRange', null);
        if (!empty($dateRange)) {
            $startDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRange, ',')->endDate;

            $startDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $startDate)->format('d M Y');
            $endDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $endDate)->format('d M Y');
        }

        // get all parameter
        $lockerId = $request->input('locker', null);
        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $transactionId = $request->input('transactionId', null);
        $itemParam = $request->input('itemParam', null);
        $itemReference = $request->input('itemReference', null);
        // get all transaction
        //DB::connection('popbox_agent')->enableQueryLog();
        $transactionDb = Transaction::with('items')
            ->withTrashed()
            ->select('transactions.*', 'users.locker_id')
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('users.locker_id', $lockerId);
            })->when($status, function ($query) use ($status) {
                return $query->where('transactions.status', $status);
            })->when($type, function ($query) use ($type) {
                /*if (in_array('commission',$type) || in_array('reward',$type)){
                    return $query->whereIn('transactions.type',$type);
                }
                return $query->whereHas('items',function ($query) use ($type){
                    if (is_array($type)) $query->whereIn('type',$type);
                    else  $query->where('type',$type);
                });*/
                $query->addSelect('transaction_items.name','transaction_items.price','transaction_items.type as item_type');
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->whereIn('transaction_items.type', $type);
            })->when($transactionId, function ($query) use ($transactionId) {
                return $query->where('reference', $transactionId);
            })->when($itemParam, function ($query) use ($itemParam) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.params', 'LIKE', "%$itemParam%");
            })->when($itemReference, function ($query) use ($itemReference) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.reference', $itemReference);
            })
            ->orderBy('transactions.created_at', 'desc')
            ->with('items')
            ->with('user.locker')
            ->get();

        /*Begin Excel Process*/
        $tmpInput = $request->except(['dateRange','_token']);
        $tmpFilename = '';
        foreach ($tmpInput as $key => $item) {
            if (!empty($item)) {
                if (is_array($item)){
                    $tmpFilename .= $key;
                    foreach ($item as $value){
                        $tmpFilename.= " $value";
                    }
                } else $tmpFilename.= "$key $item ";
            }
        }
//        $filename = "$startDate s/d $endDate Agent Transaction $tmpFilename";
        $filename = "[".date('ymd')."]"." Agent Performance Non-Group";

        Excel::create("$filename", function ($excel) use ($transactionDb, $startDateFilename, $endDateFilename) {
            $excel->setTitle('Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Agent');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($transactionDb, $startDateFilename, $endDateFilename) {

                $sheet->cell('B1:C1:D1:B2:C2:D2', function($cell) {

                    $cell->setFontSize(26);
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('B1:D1');
                $sheet->mergeCells('B2:D2');

                $header = $startDateFilename." - ".$endDateFilename;
                $sheet->row(1, ['', 'Agent Performance Non-Group']);
                $sheet->row(2, ['', $header]);

                $sheet->cell('A4', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B4', function ($cell) {
                    $cell->setValue('Transaksi Id');
                });
                $sheet->cell('C4', function ($cell) {
                    $cell->setValue('Nama Agent');
                });
                $sheet->cell('D4', function ($cell) {
                    $cell->setValue('Tipe');
                });
                $sheet->cell('E4', function ($cell) {
                    $cell->setValue('Item');
                });
                $sheet->cell('F4', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('G4', function ($cell) {
                    $cell->setValue('Tanggal Transaksi');
                });
                $sheet->cell('H4', function ($cell) {
                    $cell->setValue('Jumlah Transaksi');
                });

                $cellNumberStart = 5;
                foreach ($transactionDb as $index => $transaction) {
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                        $cell->setValue($index + 1);
                    });
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->reference);
                    });
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($transaction) {
                        $value = "";
                        $locker = Locker::where('locker_id', $transaction->user->locker_id)->first();
                        if ($locker) $value .= $locker->locker_name;
                        /*$value.= "<br>".$transaction->user->locker_id;
                        $helper = new \PHPExcel_Helper_HTML();
                        $value = $helper->toRichTextObject($value);*/
                        $cell->setValue($value);
                    });
                    $sheet->getStyle("C$cellNumberStart")->getAlignment()->setWrapText(true);
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->type;
                        $types = [];
                        foreach ($transaction->items as $item) {
                            if (!in_array($item->type, $types)) $types[] = $item->type;
                        }
                        if (!empty($types)) {
                            $value = implode(',', $types);
                        }
                        if (isset($transaction->item_type)) $value = $transaction->item_type;
                        $cell->setValue($value);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->description;
                        /*foreach ($transaction->items as $item) {
                            $value .= $item->name . " Rp" . number_format($item->price)." | ";
                        }*/
                        /*$helper = new \PHPExcel_Helper_HTML();
                        $value = $helper->toRichTextObject($value);*/
                        if (isset($transaction->name)) $value = $transaction->name;
                        $cell->setValue($value);
                    });
                    $sheet->getStyle("E$cellNumberStart")->getAlignment()->setWrapText(true);
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->status;
                        if ($transaction->trashed()) $value = 'Dibatalkan';
                        elseif ($transaction->status == 'WAITING') $value = 'Menunggu Pembayaran';
                        elseif ($transaction->status == 'UNPAID') $value = "Menunggu Metode Pembayaran";
                        elseif ($transaction->status == 'PENDING') $value = "Menunggu Konfirmasi";
                        elseif ($transaction->status == 'EXPIRED') $value = "Lewat Masa Bayar";
                        elseif ($transaction->status == 'PAID') $value = "Telah Dibayar";
                        elseif ($transaction->status == 'REFUND') $value = 'ReFund';
                        $cell->setValue($value);
                    });
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($transaction) {
                        $value = date('d F Y H:i:s', strtotime($transaction->created_at));
                        $cell->setValue($value);
                    });
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->total_price;
                        if (isset($transaction->price)) $value = $transaction->price;
                        $cell->setValue($value);
                    });
                    $cellNumberStart++;
                }
            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    /*--------------------------------------------------------------------------------------------------------------------------------v*/
    /**
     * Get Ajax Graph and Summary
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxGraph(Request $request)
    {
        set_time_limit(300);
        ini_set('memory_limit', '20480M');
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $dayLabels = [];
        $dayCountTransaction = [];
        $daySumTransaction = [];
        $weekLabels = [];
        $weekCountTransaction = [];
        $weekSumTransaction = [];
        $monthLabels = [];
        $monthCountTransaction = [];
        $monthSumTransaction = [];
        $transactionList = [];
        $transactionCount = 0;
        $transactionAmount = 0;
        $topupAmount = 0;
        $topupCount = 0;
        $rewardAmount = 0;
        $rewardCount = 0;
        $pulsaCount = 0;
        $pulsaSum = 0;
        $paymentCount = 0;
        $paymentSum = 0;
        $popshopCount = 0;
        $popshopSum = 0;
        $deliveryCount = 0;
        $deliverySum = 0;

        $typeTransaction = ['popshop', 'pulsa', 'electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid', 'delivery'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward', 'commission','referral'];

        $typePulsa = ['pulsa'];
        $typePopShop = ['popshop'];
        $typePayment = ['electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid'];
        $typeRefund = ['refund', 'deduct'];
        $typeDelivery = ['delivery'];

        // query pulsa
        /*$pulsaTransactionDb = DB::connection('popbox_agent')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.type','purchase')
            ->where('transaction_items.type','pulsa')
            ->whereNull('transactions.deleted_at')
            ->orderBy('transactions.created_at', 'desc')
            ->select(DB::raw('COUNT(transactions.id) as count, SUM(transaction_items.price) as sum'))
            ->first();
        if ($pulsaTransactionDb){
            $pulsaSum = $pulsaTransactionDb->sum;
            $pulsaCount = $pulsaTransactionDb->count;
        }*/

        /*$shopTransactionDb = DB::connection('popbox_agent')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.type','purchase')
            ->where('transaction_items.type','popshop')
            ->whereNull('transactions.deleted_at')
            ->orderBy('transactions.created_at', 'desc')
            ->select(DB::raw('COUNT(transactions.id) as count, SUM(transaction_items.price) as sum'))
            ->first();
        if ($shopTransactionDb){
            $popshopSum = $shopTransactionDb->sum;
            $popshopCount = $shopTransactionDb->count;
        }*/

        /*$paymentTransactionDb = DB::connection('popbox_agent')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.type','payment')
            ->whereIn('transaction_items.type',$typePayment)
            ->whereNull('transactions.deleted_at')
            ->orderBy('transactions.created_at', 'desc')
            ->select(DB::raw('COUNT(transactions.id) as count, SUM(transactions.total_price) as sum'))
            ->first();
        if ($paymentTransactionDb){
            $paymentSum = $paymentTransactionDb->sum;
            $paymentCount = $paymentTransactionDb->count;
        }*/

        $deliveryTransactionDb = DB::connection('popbox_agent')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.type','delivery')
            ->whereIn('transaction_items.type',$typeDelivery)
            ->whereNull('transactions.deleted_at')
            ->orderBy('transactions.created_at', 'desc')
            ->select(DB::raw('COUNT(transactions.id) as count, SUM(transaction_items.price) as sum'))
            ->first();
        if ($deliveryTransactionDb){
            $deliverySum = $deliveryTransactionDb->sum;
            $deliveryCount = $deliveryTransactionDb->count;
        }

        $transactionDayGroupDb = DB::connection('popbox_agent')
            ->table('transactions')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->whereIn('transactions.type',['payment','purchase'])
            ->whereNull('transactions.deleted_at')
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as count'), DB::raw('SUM(total_price) as sum'))
            ->groupBy('date')
            ->get();

        foreach ($transactionDayGroupDb as $item){
            $dayCountTransaction[]=$item->count;
            $daySumTransaction[] = $item->sum;
            $dayLabels[] = $item->date;
        }

        $transactionWeekGroupDb = DB::connection('popbox_agent')
            ->table('transactions')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->whereIn('transactions.type',['payment','purchase'])
            ->whereNull('transactions.deleted_at')
            ->select(DB::raw("CONCAT(YEAR(created_at), '/', WEEK(created_at,3)) as date"), DB::raw('count(*) as count'), DB::raw('SUM(total_price) as sum'))
            ->groupBy(DB::raw('date'))
            ->get();

        foreach ($transactionWeekGroupDb as $item){
            $weekCountTransaction[] = $item->count;
            $weekSumTransaction[] = $item->sum;
            list($year,$week) = explode('/',$item->date);
            $tmpLabel = Helper::getStartAndEndDate($week,$year);
            $tmpLabel = date('j M',strtotime($tmpLabel['week_start']))." - ".date('j M',strtotime($tmpLabel['week_end']));
            $weekLabels[] = $tmpLabel;
        }

        $transactionMonthGroupDb = DB::connection('popbox_agent')
            ->table('transactions')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->whereIn('transactions.type',['payment','purchase'])
            ->whereNull('transactions.deleted_at')
            ->select(DB::raw("MONTH(created_at) as month, YEAR(created_at) as year"), DB::raw('count(*) as count'), DB::raw('SUM(total_price) as sum'))
            ->groupBy(DB::raw('YEAR(created_at) ASC,MONTH(created_at) ASC'))
            ->get();

        foreach ($transactionMonthGroupDb as $item){
            $monthCountTransaction[] = $item->count;
            $monthSumTransaction[] = $item->sum;
            $year = $item->year;
            $month = $item->month;
            $tmpLabel = Helper::getNameFromMonth($month);
            $tmpLabel = "$tmpLabel $year";
            $monthLabels[] = $tmpLabel;
        }

        // get data float
        $floatingDeposit = AgentLocker::sum('balance');
        $countActiveAgent = User::whereHas('transactions',function($query) use ($startDate,$endDate){
            $query->whereBetween('created_at',[$startDate,$endDate]);
        })->count();
        $countAllAgent = Locker::where('type','agent')->where('status','<>','0')->count();

        $paidTransactionItems = DB::connection('popbox_agent')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->orderBy('transactions.created_at', 'desc')
            ->select('transaction_items.transaction_id','transaction_items.type','transaction_items.price','transaction_items.created_at','transactions.type as trans_type','transactions.total_price')
            ->get();

        $tmpTransactionId = null;
        foreach ($paidTransactionItems as $item) {
            // for transaction summary
            if (in_array($item->type, $typeTransaction)) {
                $transactionAmount += $item->price;
                if ($item->type != 'admin_fee'){
                    $transactionList[] = $item;
                    $transactionCount++;
                }
                if ($item->type == 'pulsa'){
                    $pulsaCount++;
                    $pulsaSum += $item->price;
                } elseif (in_array($item->type,$typePayment)){
                    $paymentSum += $item->price;
                    if ($item->type != 'admin_fee') $paymentCount++;
                } elseif ($item->type == 'popshop'){
                    $popshopCount++;
                    $popshopSum += $item->price;
                }
            } elseif (in_array($item->type, $typeTopUp)) {
                $topupAmount += $item->price;
                $topupCount++;
            } elseif (in_array($item->trans_type, $typeReward)) {
                $rewardAmount += $item->total_price;
                $rewardCount++;
            }
        }

        // parse for response
        $data['dayLabels'] = $dayLabels;
        $data['dayCountTransaction'] = $dayCountTransaction;
        $data['daySumTransaction'] = $daySumTransaction;
        $data['weekLabels'] = $weekLabels;
        $data['weekCountTransaction'] = $weekCountTransaction;
        $data['weekSumTransaction'] = $weekSumTransaction;
        $data['monthLabels'] = $monthLabels;
        $data['monthCountTransaction'] = $monthCountTransaction;
        $data['monthSumTransaction'] = $monthSumTransaction;
        $data['transactionCount'] = $transactionCount;
        $data['transactionAmount'] = $transactionAmount;
        $data['topupAmount'] = $topupAmount;
        $data['topupCount'] = $topupCount;
        $data['rewardAmount'] = $rewardAmount;
        $data['rewardCount'] = $rewardCount;

        $data['pulsaCount'] = $pulsaCount;
        $data['pulsaSum'] = $pulsaSum;
        $data['paymentCount'] = $paymentCount;
        $data['paymentSum'] = $paymentSum;
        $data['popshopCount'] = $popshopCount;
        $data['popshopSum'] = $popshopSum;
        $data['deliveryCount'] = $deliveryCount;
        $data['deliverySum'] = $deliverySum;

        $data['floatingDeposit'] = $floatingDeposit;
        $data['countActiveAgent'] = $countActiveAgent;
        $data['countAllAgent'] = $countAllAgent;
        $data['isSuccess'] = true;

        return response()->json($data);
    }

    /**
     * get transaction detail
     * @param Request $request
     * @param $transactionRef
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDetail(Request $request, $transactionRef)
    {
        if (empty($transactionRef))
            return back();
        
            
        $data = [];
        // get transaction
        $transactionDb = Transaction::where('reference', $transactionRef)->withTrashed()->first();
        
        // get locker
        $locker = Locker::where('locker_id', $transactionDb->user->locker_id)->first();
        
        $data = $this->getTrxWarung($transactionDb->reference, $data);
        
        // parsing data to view
        $data['transaction'] = $transactionDb;
        $data['locker'] = $locker;
        return view('agent.transaction.detail', $data);
    }

    public function getTrxWarung($reference, $data = []){
        $paymentIdNonVa = DB::connection('popbox_agent')->table('payment_non_va')->where('transaction_id', $reference)->value('payment_id');
        if($paymentIdNonVa){
            
            $url = config('constant.popwarung.api_url').'transaction/gettransactionottopaycustomer/'.$paymentIdNonVa;
            $mresult = ApiPopWarung::callAPI('GET', $url, false);
            
            $rows = json_decode($mresult, true);
            
            if( isset($rows['payload'])
                && isset($rows['payload']['data'])
                && sizeof($rows['payload']['data']) > 0
                && isset($rows['payload']['data'][0]['reference'])
                && isset($rows['payload']['data'][0]['locker_id'])
                && isset($rows['payload']['data'][0]['id']) ) {
                    $data['sales_transaction'] = $rows['payload']['data'][0]['reference'];
                    $data['locker_id'] = $rows['payload']['data'][0]['locker_id'];
                    $data['trx_id'] = $rows['payload']['data'][0]['id'];
                }
        }
        
        return $data;
    }
    
    public function getTrxWarungBySalesTransaction($sales_transaction){
        
        $url = config('constant.popwarung.api_url').'transaction/gettransactioncustomer/'.$sales_transaction;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        
        $rows = json_decode($mresult, true);
        
        if( isset($rows['payload'])
            && isset($rows['payload']['data'])
            && sizeof($rows['payload']['data']) > 0 ) {
                return $rows['payload']['data'][0];
            }
            
        return [];
    }
    
    public function getDataTrxWarungBySalesTransaction($sales_transaction, $data = []){
        $row = $this->getTrxWarungBySalesTransaction($sales_transaction);
        
        if(!empty($row)){
            $data['payment_id'] = $row['payment_id'];
            $data['sales_transaction'] = $row['reference'];
            $data['locker_id'] = $row['locker_id'];
            $data['trx_id'] = $row['id'];
        }
        
        return $data;
    }

    public function getAjaxTrxWarung(Request $request, $transactionRef){
        
        return $this->getTrxWarung($transactionRef);
    }

    /**
     * post refund
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRefund(Request $request)
    {
        //validate
        $rules = [
            'transactionRef' => 'required'
        ];
        $this->validate($request, $rules);
        $reference = $request->input('transactionRef');
        $remarks = $request->input('remarks', null);

        DB::connection('popbox_agent')->beginTransaction();

        // get transaction
        $transactionDb = Transaction::where('reference', $reference)->first();
        if (!$transactionDb) {
            $request->session()->flash('error', 'Invalid Transaction');
            return back();
        }
        $userId = $transactionDb->users_id;
        $transactionId = $transactionDb->id;

        // get balance record
        $balanceRecordDb = BalanceRecord::where('transactions_id', $transactionDb->id)->first();
        if (!$balanceRecordDb) {
            $request->session()->flash('error', 'Invalid Balance Record');
            return back();
        }

        // current deduct deposit
        $debit = $balanceRecordDb->debit;

        // calculate commission schema
        $transactionItemDb = $transactionDb->items;
        $totalCommission = 0;
        foreach ($transactionItemDb as $item) {
            $commissionId = $item->commission_schemas_id;
            $basicPrice = $item->price;
            $publishPrice = $item->price;
            $commissionDb = CommissionSchema::calculateById($commissionId, $basicPrice, $publishPrice);
            if ($commissionDb->isSuccess) {
                $totalCommission += $commissionDb->commission;
            }
        }

        // refund amount
        $refund = 0;
        $refund = $debit - $totalCommission;

        // create refund transaction
        $refundTransactionDb = Transaction::createRefundTransaction($userId, $remarks, $reference, $refund);
        if (!$refundTransactionDb->isSuccess) {
            $request->session()->flash('error', 'Failed To create Refund Transaction');
            return back();
        }
        // change current transaction status to refund
        $transactionDb = Transaction::find($transactionId);
        $transactionDb->status = 'REFUND';
        $transactionDb->save();
        // change payment to refund
        $paymentDb = Payment::where('transactions_id', $transactionId)->first();
        if ($paymentDb) {
            $paymentDb = Payment::find($paymentDb->id);
            $paymentDb->status = 'REFUND';
            $paymentDb->save();
        }

        // insert new transaction history
        $username = Auth::user()->name;
        $historyDb = TransactionHistory::createNewHistory($transactionId, $username, 'REFUND', $remarks);
        if (!$historyDb->isSuccess) {
            $request->session()->flash('error', 'Failed To create Transaction History');
            return back();
        }

        // refund commission Transaction
        $commissionTransactionDb = Transaction::refundCommissionTransaction($transactionDb->id);

        // Refund Agent Deposit
        $agentDb = User::find($userId);
        $lockerId = $agentDb->locker_id;
        $refundTransactionId = $refundTransactionDb->transactionId;
        $balanceDb = BalanceRecord::creditDeposit($lockerId, $refund, $refundTransactionId);
        if (!$balanceDb->isSuccess) {
            $request->session()->flash('error', 'Failed To create Refund Balance');
            return back();
        }

        // create parameter for FCM Notification
        $userDb = User::find($userId);
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                $fcmTitle = "Transaksi di Refund";
                $status = 'failed';
                NotificationFCM::createTransactionNotification($transactionId,$fcmToken,$fcmTitle,$status,'Transactions',$remarks);
            }
        }
        DB::connection('popbox_agent')->commit();
        dispatch(new SendFCM());
        $request->session()->flash('success', 'Success Refund Transaction');
        return back();
    }

    /**
     * export to excel
     * @param Request $request
     */
    public function postExport(Request $request)
    {
        set_time_limit(300);
        ini_set('memory_limit', '20480M');
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        // get all parameter
        $lockerId = $request->input('locker', null);
        $status = $request->input('status', null);
        $type = $request->input('type', null);
//        $typeeee = $request->input('typeeee', 'agent,warung');
        $transactionId = $request->input('transactionId', null);
        $itemParam = $request->input('itemParam', null);
        $itemReference = $request->input('itemReference', null);
        $p_sales_transaction = $request->input('p_sales_transaction', null);
        $p_city = $request->input('city', null);
        
        $p_data = [];
        
        if(!empty($p_sales_transaction)){
            $p_data = $this->getDataTrxWarungBySalesTransaction(trim($p_sales_transaction));
        }
        
        // get all transaction
        //DB::connection('popbox_agent')->enableQueryLog();
        $transactionDb = Transaction::with('items')
            ->withTrashed()
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->join('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'users.locker_id')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('payment_methods', 'payment_methods.id', '=', 'payments.payment_methods_id')
            ->leftjoin('popbox_virtual.cities', 'popbox_virtual.cities.id', '=', 'popbox_virtual.lockers.cities_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('users.locker_id', $lockerId);
            })->when($status, function ($query) use ($status) {
                return $query->where('transactions.status', $status);
            })->when($type, function ($query) use ($type) {
                if (in_array('commission',$type) || in_array('reward',$type)){
                    return $query->whereIn('transactions.type',$type);
                }
                return $query->whereHas('items',function ($query) use ($type){
                    if (is_array($type)) $query->whereIn('type',$type);
                    else  $query->where('type',$type);
                });
                /*return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->whereIn('transaction_items.type', $type);*/
            })->when($transactionId, function ($query) use ($transactionId) {
                return $query->where('reference', $transactionId);
            })->when($itemParam, function ($query) use ($itemParam) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.params', 'LIKE', "%$itemParam%");
            })->when($itemReference, function ($query) use ($itemReference) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.reference', $itemReference);
            })->when($p_data, function ($query) use ($p_data) {
                return $query->leftjoin('payment_non_va', 'payment_non_va.transaction_id', 'transactions.reference')
                ->where('payment_non_va.payment_id', $p_data['payment_id']);
            })->when($p_city, function ($query) use ($p_city) {
                return $query->where('popbox_virtual.cities.id', $p_city);
            })
            ->orderBy('transactions.created_at', 'desc')
            ->select('transactions.*', 'users.locker_id', 'payment_methods.code', 'payment_methods.name', 'popbox_virtual.cities.city_name')
            ->with('items')
            ->with('user.locker')
            ->get();
            
        $sales_transaction = [];
        
        foreach ($transactionDb as $key => $element){
            if(!empty($p_data) && (sizeof($transactionDb) == 1)
                && isset($p_data['sales_transaction'])
                && isset($p_data['locker_id'])
                && isset($p_data['trx_id'])
                && isset($p_data['payment_id']) ){
                    
                    $sales_trx = $p_data;
                    
            } else {
                $sales_trx = $this->getTrxWarung($element->reference);
            }
            $sales_transaction[$element->reference] = isset($sales_trx['sales_transaction']) ? [$sales_trx['sales_transaction'],$sales_trx['trx_id'],$sales_trx['locker_id']] : '-';
        }

        $data = array();
        $array_index = 0;
        foreach ($transactionDb as $index => $transaction) {            
            if(count($transaction->items) > 0){
                foreach ($transaction->items as $items => $item) {
                    $valueItem = '';
                    $valueProductPrice = '';
                    $valueItemQty = '';
                    $valueTotalPrice = '';
                    if ($item->type === "popshop") {
                        $getParams = json_decode($item->params, true);
                        $amount = (isset($getParams['amount']) ? $getParams['amount'] : '');
//                         $sku = (isset($getParams['sku']) ? $getParams['sku'] : '');
//                         $dataPrice = 0;
//                         if (isset($getParams['sku'])) {
//                             $getProduct = DimoProduct::where('dimo_products.sku', $sku)->select('dimo_products.price')->orderBy('dimo_products.id', 'DESC')->first();
//                             if (!is_null($getProduct)) {
//                                 $dataPrice = $getProduct->price;
//                             }
//                         }
                        $valueItem .= $item->name;
                        $valueItemQty .= $amount;
                        $valueProductPrice .= "Rp " . number_format($item->price / $valueItemQty);
                        
                        $valueTotalPrice = "Rp " . number_format(($item->price));
                    } else {
                        $valueItem .= $item->name;
                        $valueProductPrice .= "Rp " . number_format($item->price);
                        $valueItemQty .= '1';
                        $valueTotalPrice .= "Rp " . number_format(($item->price*1));
                    }

                    $data[$array_index] = [
                        'trx_id'            => $transaction->reference,
                        'sales_trx'         => $sales_transaction[$transaction->reference] != '-' ? $sales_transaction[$transaction->reference][0] : $sales_transaction[$transaction->reference],
                        'trx_date'          => date('d F Y', strtotime($transaction->created_at)),
                        'trx_time'          => date('H:i:s', strtotime($transaction->created_at)),
                        'trx_status'        => $transaction->status,
                        'payment_method'    => $transaction->name,
                        'agent_name'        => $this->findLocker($transaction),
                        'city'              => $transaction->city_name,
                        'prod_type'         => $item->type,
                        'item'              => $valueItem,
                        'qty'               => $valueItemQty,
                        'prod_price'        => $valueProductPrice,
                        'total_trx'         => $valueTotalPrice
                    ];
                    $array_index++;
                }
            } else {
                $data[$array_index] = [
                    'trx_id'            => $transaction->reference,
                    'sales_trx'         => $sales_transaction[$transaction->reference] != '-' ? $sales_transaction[$transaction->reference][0] : $sales_transaction[$transaction->reference],
                    'trx_date'          => date('d F Y', strtotime($transaction->created_at)),
                    'trx_time'          => date('H:i:s', strtotime($transaction->created_at)),
                    'trx_status'        => $transaction->status,
                    'payment_method'    => $transaction->name,
                    'agent_name'        => $this->findLocker($transaction),
                    'city'              => $transaction->city_name,
                    'prod_type'         => $transaction->type,
                    'item'              => '-',
                    'qty'               => 1,
                    'prod_price'        => "Rp " . number_format($transaction->total_price),
                    'total_trx'         => "Rp " . number_format($transaction->total_price)
                ];

                $array_index++;
            }
        }

        /*Begin Excel Process*/
        $tmpInput = $request->except(['dateRange','_token']);
        $tmpFilename = '';
        foreach ($tmpInput as $key => $item) {
            if (!empty($item)) {
                if (is_array($item)){
                    $tmpFilename .= $key;
                    foreach ($item as $value){
                        $tmpFilename.= " $value";
                    }
                } else $tmpFilename.= "$key $item ";
            }
        }
        $filename = "$startDate s/d $endDate Agent Transaction $tmpFilename";

        Excel::create("$filename", function ($excel) use ($data) {
            $excel->setTitle('Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Agent');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($data) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Transaction Id');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Sales Transaction');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Transaction Date');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Transaction Time');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Payment Method');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Agent Name');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Kota');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Product Type');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Items');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Qty');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Product Price');
                });
                $sheet->cell('N1', function ($cell) {
                    $cell->setValue('Total Transaction');
                });
                
                $cellNumberStart = 2;
                
                foreach ($data as $value => $item){
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($cellNumberStart, $item) {
                        $cell->setValue($cellNumberStart-1);
                    });
                    
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['trx_id']);
                    });
                    
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['sales_trx']);
                    });

                    $sheet->cell("D$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['trx_date']);
                    });

                    $sheet->cell("E$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['trx_time']);
                    });

                    $sheet->cell("F$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['trx_status']);
                    });

                    $sheet->cell("G$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['payment_method']);
                    });
                    
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['agent_name']);
                    });

                    $sheet->cell("I$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['city']);
                    });
                    $sheet->getStyle("I$cellNumberStart")->getAlignment()->setWrapText(true);
                        
                    $sheet->cell("J$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['prod_type']);
                    });

                    $sheet->cell("K$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['item']);
                    });

                    $sheet->cell("L$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['qty']);
                    });
                    
                    $sheet->cell("M$cellNumberStart", function ($cell) use ($item) {
                        $cell->setValue($item['prod_price']);
                        //$cell->setAlignment('right');
                    });
                    
                    $sheet->cell("N$cellNumberStart", function ($cell) use ($item) {
                        $valueTotalPrice = '0';
                        if (!$item['total_trx']){
                            $valueTotalPrice = '0';                                
                        }
                        else{
                            $valueTotalPrice = $item['total_trx'];   
                        }
                        
                        $value = $valueTotalPrice;
                        $cell->setValue($value);
                        //$cell->setAlignment('right');
                    });
                    $cellNumberStart++;
                }
            });
        })->download('xlsx');
    }

    private function findLocker($transaction){
        $locker = Locker::where('locker_id', $transaction->user->locker_id)->first();
        if ($locker) {
            return $locker->locker_name;
        }
        return '';
    }
	
    public function postExportNonGrouping(Request $request)
    {
        set_time_limit(300);
        ini_set('memory_limit', '20480M');
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        // get all parameter
        $lockerId = $request->input('locker', null);
        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $transactionId = $request->input('transactionId', null);
        $itemParam = $request->input('itemParam', null);
        $itemReference = $request->input('itemReference', null);
        // get all transaction
        //DB::connection('popbox_agent')->enableQueryLog();
        $transactionDb = Transaction::with('items')
            ->withTrashed()
            ->select('transactions.*', 'users.locker_id')
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('users.locker_id', $lockerId);
            })->when($status, function ($query) use ($status) {
                return $query->where('transactions.status', $status);
            })->when($type, function ($query) use ($type) {
                /*if (in_array('commission',$type) || in_array('reward',$type)){
                    return $query->whereIn('transactions.type',$type);
                }
                return $query->whereHas('items',function ($query) use ($type){
                    if (is_array($type)) $query->whereIn('type',$type);
                    else  $query->where('type',$type);
                });*/
                $query->addSelect('transaction_items.name','transaction_items.price','transaction_items.type as item_type');
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->whereIn('transaction_items.type', $type);
            })->when($transactionId, function ($query) use ($transactionId) {
                return $query->where('reference', $transactionId);
            })->when($itemParam, function ($query) use ($itemParam) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.params', 'LIKE', "%$itemParam%");
            })->when($itemReference, function ($query) use ($itemReference) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.reference', $itemReference);
            })
            ->orderBy('transactions.created_at', 'desc')
            ->with('items')
            ->with('user.locker')
            ->get();

        /*Begin Excel Process*/
        $tmpInput = $request->except(['dateRange','_token']);
        $tmpFilename = '';
        foreach ($tmpInput as $key => $item) {
            if (!empty($item)) {
                if (is_array($item)){
                    $tmpFilename .= $key;
                    foreach ($item as $value){
                        $tmpFilename.= " $value";
                    }
                } else $tmpFilename.= "$key $item ";
            }
        }
        $filename = "$startDate s/d $endDate Agent Transaction $tmpFilename";

        Excel::create("$filename", function ($excel) use ($transactionDb) {
            $excel->setTitle('Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Agent');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($transactionDb) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Transaksi Id');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Nama Agent');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Tipe');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Item');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Tanggal Transaksi');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Jumlah Transaksi');
                });

                $cellNumberStart = 2;
                foreach ($transactionDb as $index => $transaction) {
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                        $cell->setValue($index + 1);
                    });
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->reference);
                    });
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($transaction) {
                        $value = "";
                        $locker = Locker::where('locker_id', $transaction->user->locker_id)->first();
                        if ($locker) $value .= $locker->locker_name;
                        /*$value.= "<br>".$transaction->user->locker_id;
                        $helper = new \PHPExcel_Helper_HTML();
                        $value = $helper->toRichTextObject($value);*/
                        $cell->setValue($value);
                    });
                    $sheet->getStyle("C$cellNumberStart")->getAlignment()->setWrapText(true);
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->type;
                        $types = [];
                        foreach ($transaction->items as $item) {
                            if (!in_array($item->type, $types)) $types[] = $item->type;
                        }
                        if (!empty($types)) {
                            $value = implode(',', $types);
                        }
                        if (isset($transaction->item_type)) $value = $transaction->item_type;
                        $cell->setValue($value);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->description;
                        /*foreach ($transaction->items as $item) {
                            $value .= $item->name . " Rp" . number_format($item->price)." | ";
                        }*/
                        /*$helper = new \PHPExcel_Helper_HTML();
                        $value = $helper->toRichTextObject($value);*/
                        if (isset($transaction->name)) $value = $transaction->name;
                        $cell->setValue($value);
                    });
                    $sheet->getStyle("E$cellNumberStart")->getAlignment()->setWrapText(true);
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->status;
                        if ($transaction->trashed()) $value = 'Dibatalkan';
                        elseif ($transaction->status == 'WAITING') $value = 'Menunggu Pembayaran';
                        elseif ($transaction->status == 'UNPAID') $value = "Menunggu Metode Pembayaran";
                        elseif ($transaction->status == 'PENDING') $value = "Menunggu Konfirmasi";
                        elseif ($transaction->status == 'EXPIRED') $value = "Lewat Masa Bayar";
                        elseif ($transaction->status == 'PAID') $value = "Telah Dibayar";
                        elseif ($transaction->status == 'REFUND') $value = 'ReFund';
                        $cell->setValue($value);
                    });
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($transaction) {
                        $value = date('d F Y H:i:s', strtotime($transaction->created_at));
                        $cell->setValue($value);
                    });
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($transaction) {
                        $value = $transaction->total_price;
                        if (isset($transaction->price)) $value = $transaction->price;
                        $cell->setValue($value);
                    });
                    $cellNumberStart++;
                }
            });
        })->download('xlsx');
    }

    /**
     * Export Agent Performance
     * @param Request $request
     */
    public function exportAgentPerformance(Request $request){
        set_time_limit(300);
        ini_set('memory_limit', '20480M');
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $agentTransaction = DB::connection('popbox_agent')
            ->table('users')
            ->leftJoin('transactions',function($join) use($startDate,$endDate){
                $join->on('transactions.users_id','=','users.id')
                    ->whereBetween('transactions.created_at',[$startDate,$endDate])
                    ->whereIn('transactions.status',['PAID'])
                    ->whereNull('transactions.deleted_at');
            })
            ->leftJoin('transaction_items','transactions.id','=','transaction_items.transaction_id')
            ->join('lockers','lockers.id','=','users.locker_id')
            ->join('popbox_virtual.lockers as lockers2','lockers.id','=','lockers2.locker_id')
            ->leftJoin('popbox_virtual.users as users2','lockers2.registered_by','=','users2.id')
            ->leftJoin('popbox_virtual.list_locker_building_types as building_types2','building_types2.id','=','lockers2.building_type_id')
            ->whereNull('lockers2.deleted_at')
            ->where('lockers2.status','<>','0')
            // ->where('lockers.id','CGK170309DHW8PM74HL')
            ->select('transactions.id','transactions.type', 'transactions.total_price',
                'lockers.locker_name','lockers.id as lockers_id','lockers.balance',
                'users2.name as registered_by','lockers2.created_at as register_date','building_types2.building_types',
                'transaction_items.type as item_type','transaction_items.price as item_price','transaction_items.id as item_id'
            )
            ->get();
        // dd($agentTransaction);
        $transactionGroup = $agentTransaction->groupBy('lockers_id');
        $typeTransaction = ['payment', 'purchase'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward','referral'];
        $agentSummary = [];
        foreach ($transactionGroup as $lockerId => $transaction) {
            $purchaseAmount = 0;
            $purchaseCount = 0;

            $topUpAmount = 0;
            $topUpCount = 0;

            $transactionAmount = 0;
            $transactionCount = 0;

            $rewardAmount = 0;
            $rewardCount = 0;

            $commissionAmount = 0;
            $commissionCount = 0;

            $pulsaAmount = 0;
            $pulsaCount = 0;
            $popshopAmount = 0;
            $popshopCount = 0;
            $plnTokenAmount = 0;
            $plnTokenCount = 0;
            $plnPascaAmount = 0;
            $plnPascaCount = 0;
            $telkomAmount = 0;
            $telkomCount = 0;
            $pdamAmount = 0;
            $pdamCount = 0;
            $bpjsAmount = 0;
            $bpjsCount = 0;


            $otherAmount = 0;
            $deductAmount = 0;
            $deductCount = 0;
            $lastBalance = 0;
            $refundAmount = 0;
            $lockerName = null;
            $registeredBy = null;
            $registerDate = null;
            $buildingType = null;

            $tmpTransactionId = null;
            foreach ($transaction as $item) {
                if (in_array($item->type, $typeTransaction)) {
                    if ($item->id != $tmpTransactionId){
                        $transactionAmount += $item->total_price;
                        $transactionCount++;

                        if ($item->item_type == 'pulsa'){
                            $pulsaAmount += $item->total_price;
                            $pulsaCount++;
                        } elseif ($item->item_type == 'popshop'){
                            $popshopAmount += $item->total_price;
                            $popshopCount++;
                        } elseif ($item->item_type == 'electricity'){
                            $plnTokenAmount += $item->total_price;
                            $plnTokenCount++;
                        } elseif ($item->item_type == 'electricity_postpaid'){
                            $plnPascaAmount += $item->total_price;
                            $plnPascaCount++;
                        } elseif ($item->item_type == 'telkom_postpaid'){
                            $telkomAmount += $item->total_price;
                            $telkomCount++;
                        } elseif ($item->item_type == 'pdam'){
                            $pdamAmount += $item->total_price;
                            $pdamCount++;
                        } elseif ($item->item_type == 'bpjs_kesehatan'){
                            $bpjsAmount += $item->total_price;
                            $bpjsCount++;
                        }
                    }
                    $tmpTransactionId = $item->id;
                }
                elseif (in_array($item->type, $typeTopUp)) {
                    if ($item->id != $tmpTransactionId) {
                        $topUpAmount += $item->item_price;
                        $topUpCount++;
                        $tmpTransactionId = $item->id;
                    }
                }
                elseif (in_array($item->type, $typeReward)) {
                    $rewardAmount += $item->total_price;
                    $rewardCount++;
                }
                elseif ($item->type == 'deduct') {
                    $deductAmount += $item->total_price;
                    $deductCount++;
                }
                elseif ($item->type == 'refund') {
                    $refundAmount += $item->total_price;
                }
                elseif ($item->type == 'commission'){
                    $commissionAmount += $item->total_price;
                    $commissionCount++;
                }

                if ($item->type == 'purchase'){
                    $purchaseCount++;
                    $purchaseAmount += $item->total_price;
                }

                $lastBalance = $item->balance;
                $lockerName = $item->locker_name;
                $registeredBy = $item->registered_by;
                $registerDate = $item->register_date;
                $buildingType = $item->building_types;
            }
            $tmp = new \stdClass();
            $tmp->agentId = $lockerId;
            $tmp->agentName = $lockerName;
            $tmp->topupAmount = $topUpAmount - $deductAmount;
            $tmp->topupCount = $topUpCount - $deductCount;
            $tmp->transactionAmount = $transactionAmount;
            $tmp->transactionCount = $transactionCount;
            $tmp->rewardAmount = $rewardAmount;
            $tmp->lastBalance = $lastBalance;
            $tmp->refundAmount = $refundAmount;
            $tmp->otherAmount = $otherAmount;
            $tmp->purchaseCount = $purchaseCount;
            $tmp->purchaseAmount = $purchaseAmount;
            $tmp->commissionAmount = $commissionAmount;
            $tmp->commissionCount = $commissionCount;
            $tmp->registeredBy = $registeredBy;
            $tmp->registerDate = $registerDate;
            $tmp->buildingType = $buildingType;
            $tmp->pulsaCount = $pulsaCount;
            $tmp->pulsaAmount = $pulsaAmount;
            $tmp->popshopCount = $popshopCount;
            $tmp->popshopAmount = $popshopAmount;
            $tmp->plnTokenCount = $plnTokenCount;
            $tmp->plnTokenAmount = $plnTokenAmount;
            $tmp->plnPascaCount =$plnPascaCount;
            $tmp->plnPascaAmount = $plnPascaAmount;
            $tmp->telkomCount = $telkomCount;
            $tmp->telkomAmount = $telkomAmount;
            $tmp->pdamCount = $pdamCount;
            $tmp->pdamAmount = $pdamAmount;
            $tmp->bpjsCount = $bpjsCount;
            $tmp->bpjsAmount = $bpjsAmount;

            $agentSummary[] = $tmp;
        }
        /*Begin Excel Process*/
        $filename = "$startDate s/d $endDate Agent Performance";

        Excel::create("$filename", function ($excel) use ($agentSummary) {
            $excel->setTitle('Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Agent');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($agentSummary) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Agent Id');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Agent Name');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Agent Type');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Registered By');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Register Date');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Last Balance');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('TopUp Count');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('TopUp Amount');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Transaction PAID Count');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Transaction PAID Amount');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Reward Referral');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Refund Amount');
                });
                $sheet->cell('N1',function ($cell){
                    $cell->setValue('Commission Amount');
                });
                $sheet->cell('O1',function ($cell){
                    $cell->setValue('Pulsa Count');
                });
                $sheet->cell('P1',function ($cell){
                    $cell->setValue('Pulsa Amount');
                });
                $sheet->cell('Q1',function ($cell){
                    $cell->setValue('PopShop Count');
                });
                $sheet->cell('R1',function ($cell){
                    $cell->setValue('PopShop Amount');
                });
                $sheet->cell('S1',function ($cell){
                    $cell->setValue('PLN Token Count');
                });
                $sheet->cell('T1',function ($cell){
                    $cell->setValue('PLN Token Amount');
                });
                $sheet->cell('U1',function ($cell){
                    $cell->setValue('PLN Count');
                });
                $sheet->cell('V1',function ($cell){
                    $cell->setValue('PLN Amount');
                });
                $sheet->cell('W1',function ($cell){
                    $cell->setValue('Telkom Count');
                });
                $sheet->cell('X1',function ($cell){
                    $cell->setValue('Telkom Amount');
                });
                $sheet->cell('Y1',function ($cell){
                    $cell->setValue('PDAM Count');
                });
                $sheet->cell('Z1',function ($cell){
                    $cell->setValue('PDAM Amount');
                });
                $sheet->cell('AA1',function ($cell){
                    $cell->setValue('BPJS Count');
                });
                $sheet->cell('AB1',function ($cell){
                    $cell->setValue('BPJS Amount');
                });

                $cellNumberStart = 2;
                foreach ($agentSummary as $index => $agent) {
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                        $cell->setValue($index + 1);
                    });
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->agentId);
                    });
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->agentName);
                    });
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->buildingType);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->registeredBy);
                    });
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->registerDate);
                    });
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->lastBalance);
                    });
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->topupCount);
                    });
                    $sheet->cell("I$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->topupAmount);
                    });
                    $sheet->cell("J$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->transactionCount);
                    });
                    $sheet->cell("K$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->transactionAmount);
                    });
                    $sheet->cell("L$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->rewardAmount);
                    });
                    $sheet->cell("M$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->refundAmount);
                    });
                    $sheet->cell("N$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->commissionAmount);
                    });
                    $sheet->cell("O$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->pulsaCount);
                    });
                    $sheet->cell("P$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->pulsaAmount);
                    });
                    $sheet->cell("Q$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->popshopCount);
                    });
                    $sheet->cell("R$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->popshopAmount);
                    });
                    $sheet->cell("S$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->plnTokenCount);
                    });
                    $sheet->cell("T$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->plnTokenAmount);
                    });
                    $sheet->cell("U$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->plnPascaCount);
                    });
                    $sheet->cell("V$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->plnPascaAmount);
                    });
                    $sheet->cell("W$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->telkomCount);
                    });
                    $sheet->cell("X$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->telkomAmount);
                    });
                    $sheet->cell("Y$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->pdamCount);
                    });
                    $sheet->cell("Z$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->pdamAmount);
                    });
                    $sheet->cell("AA$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->bpjsCount);
                    });
                    $sheet->cell("AB$cellNumberStart", function ($cell) use ($agent) {
                        $cell->setValue($agent->bpjsAmount);
                    });
                    $cellNumberStart++;
                }
            });
        })->download('xlsx');
    }

    /**
     * Confirm
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postConfirm(Request $request)
    {
        $rules = [
            'transactionRef' => 'required'
        ];
        $this->validate($request, $rules);
        $reference = $request->input('transactionRef');

        DB::connection('popbox_agent')->beginTransaction();

        // get transaction
        $transactionDb = Transaction::where('reference', $reference)->first();
        if (!$transactionDb) {
            DB::connection('popbox_agent')->rollback();
            $request->session()->flash('error', 'Invalid Transaction');
            return back();
        }

        // change to PAID
        $transactionDb = Transaction::find($transactionDb->id);
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        $paymentDb = Payment::find($transactionDb->payment->id);
        $paymentDb->status = 'PAID';
        $paymentDb->save();

        $transactionId = $transactionDb->id;
        $username = Auth::user()->name;
        $remarks = "Confirmed by $username";
        $historyDb = TransactionHistory::createNewHistory($transactionId, $username, 'PAID', $remarks);
        if (!$historyDb->isSuccess) {
            $request->session()->flash('error', 'Failed To create Transaction History');
            return back();
        }

        DB::connection('popbox_agent')->commit();

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference', $reference)->first();
        dispatch(new ProcessBankTransfer($transactionDb));

        $request->session()->flash('success', 'Success Confirm Payment');
        return back();
    }

    /**
     * Repush Transaction
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRepush(Request $request)
    {
        $rules = [
            'itemId' => 'required'
        ];
        $this->validate($request, $rules);
        $itemId = $request->input('itemId');

        $itemDb = TransactionItem::find($itemId);
        if (!$itemDb) {
            $request->session()->flash('error', 'Transaction item not found');
            return back();
        }
        // check status
        if ($itemDb->status_push != 'FAILED') {
            $request->session()->flash('error', 'Transaction Not Failed .' . $itemDb->status_push);
            return back();
        }
        if (!empty($itemDb->reference)) {
            $request->session()->flash('error', 'Already Have Reference.' . $itemDb->reference);
            return back();
        }

        // push transaction
        $transactionDb = Transaction::find($itemDb->transaction_id);
        if (!$transactionDb) {
            $request->session()->flash('error', 'Transaction Not Found');
            return back();
        }
        $status = $transactionDb->status;
        $items = $transactionDb->items;

        $userData = [];
        $userData['agentName'] = $transactionDb->user->name;
        $userData['agentEmail'] = $transactionDb->user->email;
        $userData['agentPhone'] = $transactionDb->user->phone;
        $lockerDb = Locker::where('locker_id', $transactionDb->user->locker_id)->first();
        if ($lockerDb) {
            $userData['lockerName'] = $lockerDb->locker_name;
            $userData['lockerAddress'] = $lockerDb->address;
        } else {
            $userData['lockerName'] = 'Agent PopBox';
            $userData['lockerAddress'] = 'Agent PopBox';
        }

        foreach ($items as $item) {
            if ($item->type == 'admin_fee') continue;
            // process paid transaction
            $pushStatus = 'FAILED';
            $reference = null;
            $errorMsg = null;
            $url = null;
            $params = null;
            if ($status == 'PAID') {
                if ($item->type == 'pulsa') {
                    $response = $this->submitSepulsa($item, $userData, $transactionDb);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'popshop') {
                    $response = $this->submitPopShop($item, $userData, $transactionDb);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'electricity') {
                    $response = $this->submitPLNPrePaid($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'electricity_postpaid') {
                    $response = $this->submitPLNPostPaid($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'bpjs_kesehatan') {
                    $response = $this->submitBPJS($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'pdam') {
                    $response = $this->submitPDAM($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'telkom_postpaid') {
                    $response = $this->submitTelkom($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                }
            }
            // update transaction items DB
            $transactionItemDb = TransactionItem::find($item->id);
            $transactionItemDb->status_push = $pushStatus;
            $transactionItemDb->reference = $reference;
            $transactionItemDb->save();

            if ($pushStatus == 'FAILED') {
                $environment = App::environment();
                // send email if failed
                Mail::send('email.failed-push', ['transaction' => $transactionDb, 'userData' => $userData, 'item' => $item, 'locker' => $lockerDb, 'error' => $errorMsg, 'url' => $url, 'params' => $params], function ($m) use ($transactionDb, $environment) {
                    $m->from('no-reply@popbox.asia', 'PopBox Asia');
                    $m->to('it@popbox.asia', 'IT PopBox Asia')->subject("[Agent][$environment] Failed Push Transaction : $transactionDb->reference");
                });
                $request->session()->flash('error', 'Failed to Push');
                return back();
            }
        }

        $transactionId = $transactionDb->id;
        $username = Auth::user()->name;
        $remarks = $request->input('remarks', "Repush By $username");
        $historyDb = TransactionHistory::createNewHistory($transactionId, $username, 'PAID', $remarks);

        $request->session()->flash('success', 'Success Push Transaction');
        return back();
    }

    /**
     * Send Receipt
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postReSendReceipt(Request $request){
        // validation
        $rules = [
            'transactionRef' => 'required',
            'phone' => 'nullable|numeric',
            'email' => 'nullable|email'
        ];

        // validate
        $this->validate($request,$rules);

        $transactionReference = $request->input('transactionRef');
        $phone = $request->input('phone');
        $email = $request->input('email');

        // get transaction
        $transactionDb = Transaction::where('reference',$transactionReference)->first();
        if (!$transactionDb){
            $request->session()->flash('error','Transaction Not Found');
            return back();
        }

        // get transaction item
        $transactionItems = $transactionDb->items;
        $typePayment = ['electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid'];

        $invoiceIds = [];
        foreach ($transactionItems as $transactionItem) {
            if (in_array($transactionItem->type,$typePayment)){
                if (!empty($transactionItem->reference)) $invoiceIds[] = $transactionItem->reference;
            }
        }
        if (empty($invoiceIds)){
            $request->session()->flash('error','Empty Invoice Ids');
            return back();
        }

        $successList = [];
        $failedList = [];
        // push to apiv2
        $apiV2 = new APIv2();
        foreach ($invoiceIds as $invoiceId) {
            $sendReceipt = $apiV2->postSendReceipt($invoiceId,$phone,$email);
            $message = null;
            if (empty($sendReceipt)){
                $message = "$invoiceId Failed to Push Send Receipt";
                $failedList[] = $message;
                continue;
            }
            if ($sendReceipt->response->code != 200){
                $message = "$invoiceId Failed: ".$sendReceipt->response->message;
                $failedList[] = $message;
                continue;
            }
            $message = "$invoiceId Success Send Receipt";
            $successList[] = $message;
        }
        if (!empty($failedList)){
            $errorMessage = implode(',',$failedList);
            $request->session()->flash('error',$errorMessage);
        }
        if (!empty($successList)){
            $successMessage = implode(',',$successList);
            $request->session()->flash('success',$successMessage);
        }
        return back();
    }

    public function postAddRemarks(Request $request){
        // validation
        $rules = [
            'transactionRef' => 'required'
        ];

        // validate
        $this->validate($request,$rules);

        $transactionReference = $request->input('transactionRef');
        $remarks = $request->input('remarks');

        // get transaction
        $transactionDb = Transaction::where('reference',$transactionReference)->first();
        if (!$transactionDb){
            $request->session()->flash('error','Transaction Not Found');
            return back();
        }

        // get user
        $userDb = Auth::user();
        $userName = $userDb->name;

        // update remarks
        $transactionDb = Transaction::find($transactionDb->id);
        $transactionDb->remarks = $remarks;
        $transactionDb->save();

        $transactionStatus = $transactionDb->status;
        $remarks = "Remarks : $remarks";
        // insert transaction History
        $history = TransactionHistory::createNewHistory($transactionDb->id,$userName,$transactionStatus,$remarks);

        $request->session()->flash('success','Success Add Remarks');
        return back();
    }

    /**
     * Get Transaction Report
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTransactionReport(Request $request){
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $reportType = $request->input('reportType','sales');
        $provinceId = $request->input('province',null);
        $cityId = $request->input('city',null);
        $productId = $request->input('productId',null);
        $type = $request->input('type',[]);
        $category = $request->input('category',null);
        $districtName = $request->input('districtName',null);

        // query for list
        $dayLabels = [];
        $weekLabels = [];
        $monthLabels = [];
        $listDayQtyDataSets = [];
        $listDayAmtDataSets = [];
        $listWeekQtyDataSets = [];
        $listWeekAmtDataSets = [];
        $listMonthQtyDataSets = [];
        $listMonthAmtDataSets = [];

        // get number of different dates between start and end
        $different = abs(strtotime($startDate) - strtotime($endDate));
        $different = $different/(60 * 60 *24);

        for ($i=0;$i<$different;$i++) {
            $date = date_create($startDate);
            date_add($date, date_interval_create_from_date_string("$i days"));
            $selectedDate = date_format($date, 'Y-m-d');
            $selectedWeek = date_format($date,'Y-W');
            $selectedMonth = date_format($date,'Y-m');
            $dayLabels[] = $selectedDate;
            if (!in_array($selectedWeek,$weekLabels)) $weekLabels[] = $selectedWeek;
            if (!in_array($selectedMonth,$monthLabels)) $monthLabels[] = $selectedMonth;
        }

        if ($reportType == 'sales'){
            $transactionReportDb = DB::connection('popbox_agent')
                ->table('transaction_location_reports as tlp')
                ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price', tlp.category"))
                ->when($category,function ($query) use ($category){
                    $query->where('category',$category);
                    $query->addSelect(DB::raw('tlp.type as type'));
                    $query->groupBy('tlp.type');
                })->when($type,function ($query) use ($type){
                    $query->where('tlp.type',$type);
                    $query->addSelect('tlp.name','tlp.product_id');
                    $query->groupBy('product_id');
                })->when($productId,function ($query) use ($productId,$provinceId,$cityId){
                    $query->where('product_id',$productId);
                    $query->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
                        ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
                        ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id');
                    $query->groupBy('p.id');
                    $query->addSelect(DB::raw('p.id as province_id, p.province_name'));
                    $query->when($provinceId,function ($query) use ($provinceId){
                        $query->where('c.provinces_id',$provinceId);
                        $query->groupBy('c.id');
                        $query->addSelect(DB::raw('c.id as city_id, c.city_name'));
                    });
                    $query->when($cityId,function ($query) use ($cityId){
                        $query->where('c.id',$cityId);
                        $query->groupBy('l.district_name');
                        $query->addSelect(DB::raw('c.id as cities_id, c.city_name, l.district_name, l.sub_district_name'));
                    });
                })
                ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
                ->groupBy('tlp.category')
                ->orderBy('sum','desc')
                ->get();

            // create day graph
            $transactionData = Transaction::getTransactionSalesGroup($startDate,$endDate,$category,$type,$productId,$provinceId,$cityId);
            $transactionSalesDay = $transactionData->dayData;
            $transactionSalesWeek = $transactionData->weekData;
            $transactionSalesMonth = $transactionData->monthData;

            if($productId){
                if ($cityId){
                    $tmpListDataSets = $transactionReportDb->pluck('district_name')->take(10);
                    foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                        $label = $tmpListDataSet;
                        $dataQty = [];
                        $dataAmt = [];
                        $color = Helper::getColor($index);
                        $color = "rgba($color[0],$color[1],$color[2],1)";
                        foreach ($dayLabels as $dayLabel) {
                            $tmpDay = $dayLabel;
                            $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                                return $value->district_name == $label && $value->date == $tmpDay;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listDayQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listDayAmtDataSets[] = $tmp;

                        /*=================Set Weeks Data Sets=================*/
                        $dataQty = [];
                        $dataAmt = [];
                        foreach ($weekLabels as $weekLabel){
                            $tmpWeek = $weekLabel;
                            $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                                list($dataYear,$dataWeek) = explode('-',$value->date);
                                list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                                return $value->district_name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listWeekQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listWeekAmtDataSets[] = $tmp;

                        /*====================Set Month Data Sets====================*/
                        $dataQty = [];
                        $dataAmt = [];
                        foreach ($monthLabels as $weekLabel){
                            $tmpMonth = $weekLabel;
                            $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                                list($dataYear,$dataMonth) = explode('-',$value->date);
                                list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                                return $value->district_name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listMonthQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listMonthAmtDataSets[] = $tmp;
                    }
                }
                elseif ($provinceId){
                    $tmpListDataSets = $transactionReportDb->pluck('city_name')->take(10);
                    foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                        $label = $tmpListDataSet;
                        $dataQty = [];
                        $dataAmt = [];
                        $color = Helper::getColor($index);
                        $color = "rgba($color[0],$color[1],$color[2],1)";
                        foreach ($dayLabels as $dayLabel) {
                            $tmpDay = $dayLabel;
                            $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                                return $value->city_name == $label && $value->date == $tmpDay;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listDayQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listDayAmtDataSets[] = $tmp;

                        /*=================Set Weeks Data Sets=================*/
                        $dataQty = [];
                        $dataAmt = [];
                        foreach ($weekLabels as $weekLabel){
                            $tmpWeek = $weekLabel;
                            $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                                list($dataYear,$dataWeek) = explode('-',$value->date);
                                list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                                return $value->city_name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listWeekQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listWeekAmtDataSets[] = $tmp;

                        /*====================Set Month Data Sets====================*/
                        $dataQty = [];
                        $dataAmt = [];
                        foreach ($monthLabels as $weekLabel){
                            $tmpMonth = $weekLabel;
                            $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                                list($dataYear,$dataMonth) = explode('-',$value->date);
                                list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                                return $value->city_name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listMonthQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listMonthAmtDataSets[] = $tmp;
                    }
                }
                else {
                    $tmpListDataSets = $transactionReportDb->pluck('province_name')->take(10);
                    foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                        $label = $tmpListDataSet;
                        $dataQty = [];
                        $dataAmt = [];
                        $color = Helper::getColor($index);
                        $color = "rgba($color[0],$color[1],$color[2],1)";
                        foreach ($dayLabels as $dayLabel) {
                            $tmpDay = $dayLabel;
                            $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                                return $value->province_name == $label && $value->date == $tmpDay;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listDayQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listDayAmtDataSets[] = $tmp;

                        /*=================Set Weeks Data Sets=================*/
                        $dataQty = [];
                        $dataAmt = [];
                        foreach ($weekLabels as $weekLabel){
                            $tmpWeek = $weekLabel;
                            $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                                list($dataYear,$dataWeek) = explode('-',$value->date);
                                list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                                return $value->province_name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listWeekQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listWeekAmtDataSets[] = $tmp;

                        /*====================Set Month Data Sets====================*/
                        $dataQty = [];
                        $dataAmt = [];
                        foreach ($monthLabels as $weekLabel){
                            $tmpMonth = $weekLabel;
                            $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                                list($dataYear,$dataMonth) = explode('-',$value->date);
                                list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                                return $value->province_name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                            });
                            $filtered->all();
                            $filtered = $filtered->first();
                            $qty = 0;
                            $amt = 0;
                            if ($filtered){
                                $qty = (int)$filtered->sum;
                                $amt = (int)$filtered->price;
                            }
                            $dataQty[] = $qty;
                            $dataAmt[] = $amt;
                        }
                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataQty;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = 'false';
                        $listMonthQtyDataSets[] = $tmp;

                        $tmp = [];
                        $tmp['label'] = $label;
                        $tmp['data'] = $dataAmt;
                        $tmp['borderColor'] = $color;
                        $tmp['backgroundColor'] = $color;
                        $tmp['fill'] = false;
                        $listMonthAmtDataSets[] = $tmp;
                    }
                }
            }
            elseif ($type){
                $tmpListDataSets = $transactionReportDb->pluck('name')->take(10);
                foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                    $label = $tmpListDataSet;
                    $dataQty = [];
                    $dataAmt = [];
                    $color = Helper::getColor($label);
                    $color = "rgba($color[0],$color[1],$color[2],1)";
                    foreach ($dayLabels as $dayLabel) {
                        $tmpDay = $dayLabel;
                        $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                            return $value->name == $label && $value->date == $tmpDay;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listDayQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listDayAmtDataSets[] = $tmp;

                    /*=================Set Weeks Data Sets=================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($weekLabels as $weekLabel){
                        $tmpWeek = $weekLabel;
                        $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                            list($dataYear,$dataWeek) = explode('-',$value->date);
                            list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                            return $value->name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listWeekQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listWeekAmtDataSets[] = $tmp;

                    /*====================Set Month Data Sets====================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($monthLabels as $weekLabel){
                        $tmpMonth = $weekLabel;
                        $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                            list($dataYear,$dataMonth) = explode('-',$value->date);
                            list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                            return $value->name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listMonthQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listMonthAmtDataSets[] = $tmp;
                }
            }
            elseif ($category){
                $tmpListDataSets = $transactionReportDb->pluck('type');
                foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                    $label = $tmpListDataSet;
                    $dataQty = [];
                    $dataAmt = [];
                    $color = Helper::getColor($label);
                    $color = "rgba($color[0],$color[1],$color[2],1)";
                    foreach ($dayLabels as $dayLabel) {
                        $tmpDay = $dayLabel;
                        $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                            return $value->type == $label && $value->date == $tmpDay;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listDayQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listDayAmtDataSets[] = $tmp;

                    /*=================Set Weeks Data Sets=================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($weekLabels as $weekLabel){
                        $tmpWeek = $weekLabel;
                        $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                            list($dataYear,$dataWeek) = explode('-',$value->date);
                            list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                            return $value->type == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listWeekQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listWeekAmtDataSets[] = $tmp;

                    /*====================Set Month Data Sets====================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($monthLabels as $weekLabel){
                        $tmpMonth = $weekLabel;
                        $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                            list($dataYear,$dataMonth) = explode('-',$value->date);
                            list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                            return $value->type == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listMonthQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listMonthAmtDataSets[] = $tmp;
                }
            }
            else {
                $tmpListDataSets = $transactionReportDb->pluck('category');
                foreach ($tmpListDataSets as $tmpListDataSet) {
                    $label = $tmpListDataSet;
                    $dataQty = [];
                    $dataAmt = [];
                    $color = "rgba(0,166,90,1)";
                    if ($label == 'non_digital') $color =  "rgba(60,141,188,1)";
                    foreach ($dayLabels as $dayLabel) {
                        $tmpDay = $dayLabel;
                        $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                            return $value->category == $label && $value->date == $tmpDay;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listDayQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listDayAmtDataSets[] = $tmp;

                    /*=================Set Weeks Data Sets=================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($weekLabels as $weekLabel){
                        $tmpWeek = $weekLabel;
                        $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                            list($dataYear,$dataWeek) = explode('-',$value->date);
                            list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                            return $value->category == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listWeekQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listWeekAmtDataSets[] = $tmp;

                    /*====================Set Month Data Sets====================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($monthLabels as $weekLabel){
                        $tmpMonth = $weekLabel;
                        $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                            list($dataYear,$dataMonth) = explode('-',$value->date);
                            list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                            return $value->category == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listMonthQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listMonthAmtDataSets[] = $tmp;
                }
            }
        }
        else {
            // DB::connection('popbox_agent')->enableQueryLog();
            $transactionReportDb = DB::connection('popbox_agent')
                ->table('transaction_location_reports as tlp')
                ->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
                ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
                ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id')
                ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price',p.id as province_id, p.province_name"))
                ->when($provinceId,function ($query) use ($provinceId){
                    $query->where('p.id','=',$provinceId);
                    $query->groupBy('c.id');
                    $query->addSelect(DB::raw("ANY_VALUE(c.city_name) as 'city_name',c.id as city_id ,c.city_name"));
                })->when($cityId,function ($query) use ($cityId){
                    $query->where('c.id','=',$cityId);
                    $query->groupBy('l.district_name');
                    $query->addSelect(DB::raw('l.district_name, COUNT(DISTINCT(l.id)) as number_agent'));
                })->when($districtName,function ($query) use ($districtName){
                    $query->where('l.district_name',$districtName);
                    $query->groupBy('l.locker_id');
                    $query->addSelect(DB::raw('l.sub_district_name, l.locker_name, l.locker_id'));
                })
                ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
                // ->groupBy('tlp.category')
                ->groupBy('p.id')
                ->orderBy('sum','desc')
                ->get();
            // dd($transactionReportDb);
            // dd(DB::connection('popbox_agent')->getQueryLog());

            // create day graph
            $transactionData = Transaction::getTransactionGeoGroup($startDate,$endDate,$provinceId,$cityId,$districtName);
            $transactionSalesDay = $transactionData->dayData;
            $transactionSalesWeek = $transactionData->weekData;
            $transactionSalesMonth = $transactionData->monthData;
            if (is_array($transactionSalesDay)) $transactionSalesDay = collect([]);
            if (is_array($transactionSalesWeek)) $transactionSalesWeek = collect([]);
            if (is_array($transactionSalesMonth)) $transactionSalesMonth = collect([]);

            if($districtName){
                $tmpListDataSets = $transactionReportDb->pluck('locker_name')->take(10);
                foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                    $label = $tmpListDataSet;
                    $dataQty = [];
                    $dataAmt = [];
                    $color = Helper::getColor($index);
                    $color = "rgba($color[0],$color[1],$color[2],1)";
                    foreach ($dayLabels as $dayLabel) {
                        $tmpDay = $dayLabel;
                        $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                            return $value->locker_name == $label && $value->date == $tmpDay;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listDayQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listDayAmtDataSets[] = $tmp;

                    /*=================Set Weeks Data Sets=================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($weekLabels as $weekLabel){
                        $tmpWeek = $weekLabel;
                        $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                            list($dataYear,$dataWeek) = explode('-',$value->date);
                            list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                            return $value->locker_name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listWeekQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listWeekAmtDataSets[] = $tmp;

                    /*====================Set Month Data Sets====================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($monthLabels as $weekLabel){
                        $tmpMonth = $weekLabel;
                        $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                            list($dataYear,$dataMonth) = explode('-',$value->date);
                            list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                            return $value->locker_name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listMonthQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listMonthAmtDataSets[] = $tmp;
                }
            }
            elseif ($cityId){
                $tmpListDataSets = $transactionReportDb->pluck('district_name')->take(10);
                foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                    $label = $tmpListDataSet;
                    $dataQty = [];
                    $dataAmt = [];
                    $color = Helper::getColor($label);
                    $color = "rgba($color[0],$color[1],$color[2],1)";
                    foreach ($dayLabels as $dayLabel) {
                        $tmpDay = $dayLabel;
                        $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                            return $value->district_name == $label && $value->date == $tmpDay;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listDayQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listDayAmtDataSets[] = $tmp;

                    /*=================Set Weeks Data Sets=================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($weekLabels as $weekLabel){
                        $tmpWeek = $weekLabel;
                        $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                            list($dataYear,$dataWeek) = explode('-',$value->date);
                            list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                            return $value->district_name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listWeekQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listWeekAmtDataSets[] = $tmp;

                    /*====================Set Month Data Sets====================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($monthLabels as $weekLabel){
                        $tmpMonth = $weekLabel;
                        $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                            list($dataYear,$dataMonth) = explode('-',$value->date);
                            list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                            return $value->district_name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listMonthQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listMonthAmtDataSets[] = $tmp;
                }
            }
            elseif ($provinceId){
                $tmpListDataSets = $transactionReportDb->pluck('city_name')->take(10);
                foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                    $label = $tmpListDataSet;
                    $dataQty = [];
                    $dataAmt = [];
                    $color = Helper::getColor($label);
                    $color = "rgba($color[0],$color[1],$color[2],1)";
                    foreach ($dayLabels as $dayLabel) {
                        $tmpDay = $dayLabel;
                        $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                            return $value->city_name == $label && $value->date == $tmpDay;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listDayQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listDayAmtDataSets[] = $tmp;

                    /*=================Set Weeks Data Sets=================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($weekLabels as $weekLabel){
                        $tmpWeek = $weekLabel;
                        $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                            list($dataYear,$dataWeek) = explode('-',$value->date);
                            list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                            return $value->city_name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listWeekQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listWeekAmtDataSets[] = $tmp;

                    /*====================Set Month Data Sets====================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($monthLabels as $weekLabel){
                        $tmpMonth = $weekLabel;
                        $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                            list($dataYear,$dataMonth) = explode('-',$value->date);
                            list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                            return $value->city_name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listMonthQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listMonthAmtDataSets[] = $tmp;
                }
            }
            else {
                $tmpListDataSets = $transactionReportDb->pluck('province_name')->take(10);
                foreach ($tmpListDataSets as $index => $tmpListDataSet) {
                    $label = $tmpListDataSet;
                    $dataQty = [];
                    $dataAmt = [];
                    $color = Helper::getColor($index);
                    $color = "rgba($color[0],$color[1],$color[2],1)";
                    foreach ($dayLabels as $dayLabel) {
                        $tmpDay = $dayLabel;
                        $filtered = $transactionSalesDay->filter(function ($value,$key) use ($label,$tmpDay){
                            return $value->province_name == $label && $value->date == $tmpDay;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listDayQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listDayAmtDataSets[] = $tmp;

                    /*=================Set Weeks Data Sets=================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($weekLabels as $weekLabel){
                        $tmpWeek = $weekLabel;
                        $filtered = $transactionSalesWeek->filter(function ($value,$key) use ($label,$tmpWeek){
                            list($dataYear,$dataWeek) = explode('-',$value->date);
                            list($labelYear,$labelWeek) = explode('-',$tmpWeek);

                            return $value->province_name == $label && $dataYear == $labelYear && $dataWeek == $labelWeek;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listWeekQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listWeekAmtDataSets[] = $tmp;

                    /*====================Set Month Data Sets====================*/
                    $dataQty = [];
                    $dataAmt = [];
                    foreach ($monthLabels as $weekLabel){
                        $tmpMonth = $weekLabel;
                        $filtered = $transactionSalesMonth->filter(function ($value,$key) use ($label,$tmpMonth){
                            list($dataYear,$dataMonth) = explode('-',$value->date);
                            list($labelYear,$labelMonth) = explode('-',$tmpMonth);

                            return $value->province_name == $label && $dataYear == $labelYear && $dataMonth == $labelMonth;
                        });
                        $filtered->all();
                        $filtered = $filtered->first();
                        $qty = 0;
                        $amt = 0;
                        if ($filtered){
                            $qty = (int)$filtered->sum;
                            $amt = (int)$filtered->price;
                        }
                        $dataQty[] = $qty;
                        $dataAmt[] = $amt;
                    }
                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataQty;
                    $tmp['borderColor'] = $color;
                    $tmp['fill'] = 'false';
                    $listMonthQtyDataSets[] = $tmp;

                    $tmp = [];
                    $tmp['label'] = $label;
                    $tmp['data'] = $dataAmt;
                    $tmp['borderColor'] = $color;
                    $tmp['backgroundColor'] = $color;
                    $tmp['fill'] = false;
                    $listMonthAmtDataSets[] = $tmp;
                }
            }
        }

        $totalQuantity = 0;
        $totalAmount = 0;
        foreach ($transactionReportDb as $item) {
            $totalQuantity += $item->sum;
            $totalAmount += $item->price;
        }

        $dayLabels = [];
        $weekLabels = [];
        $monthLabels = [];
        for ($i=0;$i<$different;$i++) {
            $date = date_create($startDate);
            date_add($date, date_interval_create_from_date_string("$i days"));
            $selectedDate = date_format($date, 'Y-m-d');
            $selectedWeek = date_format($date,'Y-W');
            $selectedMonth = date_format($date,'Y-m');
            $dayLabels[] = $selectedDate;

            list($year,$week) = explode('-',$selectedWeek);
            $tmpLabel = Helper::getStartAndEndDate($week,$year);
            $tmpLabel = date('j M',strtotime($tmpLabel['week_start']))." - ".date('j M',strtotime($tmpLabel['week_end']));
            $selectedWeek = $tmpLabel;
            if (!in_array($selectedWeek,$weekLabels)) $weekLabels[] = $selectedWeek;

            $selectedMonth = date('Y M',strtotime($selectedMonth));
            if (!in_array($selectedMonth,$monthLabels)) $monthLabels[] = $selectedMonth;
        }


        // create pagination
        $paginateTransactionReport = $this->paginate($transactionReportDb,20)->setPath('/agent/transaction/report');

        // get data for view
        $provincesList = Province::get();
        $citiesList = City::get();
        $typeList = [
            'popshop' => 'Grocery Shop',
            'pulsa' => 'Mobile Pulsa',
            'electricity' => 'Electricity Token',
            'electricity_postpaid' => 'Electricity PostPaid',
            'bpjs_kesehatan' => 'BPJS Kesehatan',
            'pdam' => 'PDAM',
            'telkom_postpaid' => 'Telkom PostPaid'
        ];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['reportType'] = $reportType;
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";
        $param['dateEncoded'] = date('Y/m/d',strtotime($startDate))." - ".date('Y/m/d',strtotime($endDate));

        // parse data to view
        $data = [];
        $data['transactionReportDb'] = $paginateTransactionReport->appends($request->input());
        $data['parameter'] = $param;
        $data['typeList'] = $typeList;
        $data['totalAmount'] = $totalAmount;
        $data['totalQuantity'] = $totalQuantity;
        $data['dayLabels'] = $dayLabels;
        $data['weekLabels'] = $weekLabels;
        $data['monthLabels'] = $monthLabels;
        $data['listDayQtyDataSets'] = $listDayQtyDataSets;
        $data['listDayAmtDataSets'] = $listDayAmtDataSets;
        $data['listWeekQtyDataSets'] = $listWeekQtyDataSets;
        $data['listWeekAmtDataSets'] = $listWeekAmtDataSets;
        $data['listMonthQtyDataSets'] = $listMonthQtyDataSets;
        $data['listMonthAmtDataSets'] = $listMonthAmtDataSets;

        return view('agent.transaction.report',$data);
    }

    public function postChangeDeliveryStatus(Request $request){
        // validation
        $rules = [
            'transactionRef' => 'required',
            'transactionItemId' => 'required',
            'deliveryStatus' => 'required|in:on_process,delivered,undelivered'
        ];

        // validate
        $this->validate($request,$rules);

        $deliveryCode = [
            'on_process' => 2,
            'delivered' => 1,
            'undelivered' => 3
        ];

        $transactionReference = $request->input('transactionRef');
        $transactionItemId = $request->input('transactionItemId');
        $deliveryStatus = $request->input('deliveryStatus');
        $itemName = $request->input('itemName',null);

        // get transaction
        $transactionItemDb = TransactionItem::with('transaction')
            ->where('id',$transactionItemId)
            ->whereHas('transaction',function ($query) use ($transactionReference){
                $query->where('reference',$transactionReference);
            })->first();

        if (!$transactionItemDb){
            $request->session()->flash('error','Invalid Transaction');
            return back();
        }
        $transactionId = $transactionItemDb->transaction->id;
        $itemReference = $transactionItemDb->reference;

        // find popshop order on popbox DB
        $orderDb = Order::where('invoice_id',$itemReference)->first();
        if (!$orderDb){
            $request->session()->flash('error','Invalid Order PopShop Reference');
            return back();
        }
        $orderId = $orderDb->id;

        DB::connection('popbox_agent')->beginTransaction();
        DB::connection('popbox_db')->beginTransaction();

        // update delivery on transaction items
        $transactionItemDb = TransactionItem::find($transactionItemId);
        $transactionItemDb->delivery_status = $deliveryStatus;
        $transactionItemDb->save();

        // update delivery on order POPBOX
        $orderDb = Order::find($orderId);
        $orderDb->deliver_status = $deliveryCode[$deliveryStatus];
        $orderDb->save();

        // get user
        $userDb = Auth::user();
        $userName = $userDb->name;

        $transactionStatus = $transactionItemDb->transaction->status;
        $remarks = "Change Delivery Status for Transaction Item : $itemReference, $itemName";
        // insert transaction History
        $history = TransactionHistory::createNewHistory($transactionId,$userName,$transactionStatus,$remarks);


        DB::connection('popbox_agent')->commit();
        DB::connection('popbox_db')->commit();

        $request->session()->flash('success','Success Change Delivery Status');
        return back();
    }

    public function getAjaxChangeAllDeliveryStatus(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $transactionRef = $request->input('transaction_ref', null);
        $transactionStatus = $request->input('transaction_status', null);

        $transactionId = DB::connection('popbox_agent')->table('transactions')
            ->where('transactions.reference', $transactionRef)
            ->first()
            ->id;

        // get user
        $userDb = Auth::user();
        $userName = $userDb->name;

        DB::beginTransaction();

        // parse data to view
        $data = [];

        $transactionItem = DB::connection('popbox_agent')->table('transaction_items')
            ->where('transaction_id', $transactionId)
            ->update(['delivery_status' => $transactionStatus]);

        if ($transactionItem) {
            $transactionItems = DB::connection('popbox_agent')->table('transaction_items')
                ->where('transaction_id', $transactionId)
                ->get();

            foreach ($transactionItems as $transactionItemm) {
                $remarks = "Change Delivery Status for Transaction Item : $transactionItemm->reference, $transactionItemm->name";
                $history = TransactionHistory::createNewHistory($transactionId,$userName,$transactionStatus,$remarks);
            }

            $data['transaction_ref'] = $transactionRef;
            $data['transaction_id'] = $transactionId;
            $data['transaction_status'] = $transactionStatus;
            $data['items'] = $transactionItem;

            $response->isSuccess = true;

            DB::commit();
        } else {
            DB::rollback();
            $response->errorMsg = 'Failed Update Delivery Status';
        }

        $response->data = $data;
        return response()->json($response);

    }

    /* ====================================================
     * ================= PRIVATE FUNCTION =================
     * ====================================================
     */

    /**
     * Submit for Paid Transaction PopShop
     * @param $item
     * @param $userData
     * @param $transactionDb
     * @return \stdClass
     */
    private function submitPopShop($item, $userData, $transactionDb)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $totalAmount = $item->price;
            $productInfo = $paramsDb->sku . '|' . $item->name . '|' . $paramsDb->amount . '|' . $totalAmount;
            $customerInfo = $userData['agentName'] . '|' . $userData['agentEmail'] . '|' . $userData['agentPhone'];
            $purchaseInfo = $userData['lockerName'] . '|||deposit agent|' . $transactionDb->updated_at;
            $deliveryAddress = $userData['lockerName'] . "-" . $userData['lockerAddress'];

            $apiV2 = new APIv2();
            $result = $apiV2->submitPopShop($productInfo, $customerInfo, $purchaseInfo, $deliveryAddress);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'ordershop/submit';
            $param = [];
            $param['token'] = $token;
            $param['product_info'] = $productInfo;
            $param['customer_info'] = $customerInfo;
            $param['purchase_info'] = $purchaseInfo;
            $param['delivery_address'] = $deliveryAddress;

            if (empty($result)) {
                $message = 'Failed to get Submit Order Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit for Paid Transaction Sepulsa
     * @param $item
     * @param $userData
     * @param $transactionDb
     * @return \stdClass
     */
    private function submitSepulsa($item, $userData, $transactionDb)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productType = null;
            if ($item->type == 'pulsa') {
                $productType = 'mobile';
            }
            $productId = $paramsDb->product_id;
            $productAmount = $item->price;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitSepulsa($phone, $productType, $productId, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = $productType;
            $param['product_id'] = $productId;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit PLN PrePaid
     * @param $item
     * @return \stdClass
     */
    private function submitPLNPrePaid($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productId = $paramsDb->product_id;
            $productAmount = $item->price;
            $meterNumber = $paramsDb->meter_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitPLNPrePaid($phone, $productId, $productAmount, $meterNumber, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'electricity';
            $param['product_id'] = $productId;
            $param['product_amount'] = $productAmount;
            $param['meter_number'] = $meterNumber;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit PLN Post Paid
     * @param $item
     * @return \stdClass
     */
    private function submitPLNPostPaid($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $meterNumber = $paramsDb->meter_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitPLNPostPaid($phone, $productAmount, $meterNumber, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'electricity_postpaid';
            $param['product_amount'] = $productAmount;
            $param['meter_number'] = $meterNumber;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit BPJS
     * @param $item
     * @return \stdClass
     */
    private function submitBPJS($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $bpjsNumber = $paramsDb->bpjs_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitBPJS($phone, $bpjsNumber, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'bpjs_kesehatan';
            $param['bpjs_number'] = $bpjsNumber;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit PDAM
     * @param $item
     * @return \stdClass
     */
    private function submitPDAM($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $pdamNumber = $paramsDb->pdam_number;
            $operatorCode = $paramsDb->operator_code;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitPDAM($phone, $pdamNumber, $operatorCode, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'pdam';
            $param['pdam_number'] = $pdamNumber;
            $param['operator_code'] = $operatorCode;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit Telkom
     * @param $item
     * @return \stdClass
     */
    private function submitTelkom($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $telkomNumber = $paramsDb->telkom_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitTelkom($phone, $telkomNumber, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'telkom_postpaid';
            $param['telkom_number'] = $telkomNumber;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Paginate
     * @param $items
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    private function paginate($items,$perPage=20){
        //Get current page form url e.g. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $currentPageItems = $items->slice(($currentPage -1) * $perPage,$perPage);

        return new LengthAwarePaginator($currentPageItems,count($items),$perPage);
    }

    /**
     * Reverse Geocode
     * @param $provinceName
     * @param null $cityName
     * @param null $districtName
     * @param null $subDistrict
     * @return \stdClass
     */
    private function reverseGeocode($provinceName,$cityName=null,$districtName=null,$subDistrict=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->latitude = null;
        $response->longitude = null;
        $response->address = null;

        // generate address first
        $address = '';
        if (!empty($subDistrict)) $address.="$subDistrict,";
        if (!empty($districtName)) $address.= "$districtName,";
        if (!empty($cityName)) $address.="$cityName,";
        $address.="$provinceName";
        $response->address = $address;

        $googleApi = new GoogleAPI();
        $reverseByAddress = $googleApi->reverseGeocodeAddress($address);
        if (empty($reverseByAddress)){
            return $response;
        }
        if ($reverseByAddress->status != 'OK'){
            return $response;
        }
        // get the first result
        if (empty($reverseByAddress->results)){
            return $response;
        }
        $results = $reverseByAddress->results;
        $results = $results[0];
        $geometry = $results->geometry->location;
        $latitude = $geometry->lat;
        $longitude = $geometry->lng;

        $response->isSuccess = true;
        $response->latitude = $latitude;
        $response->longitude = $longitude;
        return $response;
    }

    private function checkAgentType($locker_id) {

        $result = '';

        $virtualLocker = DB::connection('popbox_virtual')
            ->table('lockers')
            ->where('locker_id', '=', $locker_id)
            ->first();

        $result = $virtualLocker->type;
        return $result;
    }

    private function getAllTransactionQuery($request, $type) {

        $transactionStartDate = null;
        $transactionEndDate = null;

        // get all parameter
        $type_agent_warung = $request->input('type_agent_warung', null);
        $lockerId = $request->input('store_agentid_name', null);
        $status = $request->input('status', null);

        $dateRange = $request->input('daterange_transaction', null);
        $transactionId = $request->input('transaction_id', null);
        $sales_transaction = $request->input('sales_transaction', null);
        $city_id = $request->input('city_id', null);
        $type_transaction = $request->input('type_transaction', null);
        $itemReference = $request->input('item_reference', null);
        $itemParam = $request->input('item_param', null);

        if (!empty($dateRange)) {
            $transactionStartDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $transactionEndDate = Helper::formatDateRange($dateRange, ',')->endDate;
        }
        
        $p_data = [];
        
        if(!empty($sales_transaction)){
            $p_data = $this->getDataTrxWarungBySalesTransaction(trim($sales_transaction));
        }
        
        // get all transaction
        //DB::connection('popbox_agent')->enableQueryLog();
        $allTransaction = Transaction::with('items')
            ->withTrashed()
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->join('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'users.locker_id')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('payment_methods', 'payment_methods.id', '=', 'payments.payment_methods_id')
            ->leftjoin('popbox_virtual.cities', 'popbox_virtual.cities.id', 'popbox_virtual.lockers.cities_id')
            ->leftjoin('lockers', 'popbox_agent.lockers.id', '=', 'users.locker_id')
            ->when($dateRange, function ($query) use ($transactionStartDate, $transactionEndDate) {
                return $query->whereBetween('transactions.created_at', [$transactionStartDate, $transactionEndDate]);
            })->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('users.locker_id', $lockerId);
            })->when($status, function ($query) use ($status) {
                return $query->where('transactions.status', $status);
            })->when($type_transaction, function ($query) use ($type_transaction) {
                if (in_array('commission',$type_transaction) || in_array('reward',$type_transaction)){
                    return $query->whereIn('transactions.type',$type_transaction);
                }
                return $query->whereHas('items',function ($query) use ($type_transaction){
                    if (is_array($type_transaction)) $query->whereIn('type',$type_transaction);
                    else  $query->where('type',$type_transaction);
                });
                /*return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->whereIn('transaction_items.type', $type);*/
            })->when($type_agent_warung, function ($query) use ($type_agent_warung) {
                return $query->where('popbox_virtual.lockers.type', $type_agent_warung);
            })->when($transactionId, function ($query) use ($transactionId) {
                return $query->where('reference', $transactionId);
            })->when($itemParam, function ($query) use ($itemParam) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.params', 'LIKE', "%$itemParam%");
            })->when($itemReference, function ($query) use ($itemReference) {
                return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
                    ->where('transaction_items.reference', $itemReference);
            })->when($p_data, function ($query) use ($p_data) {
                return $query->leftjoin('payment_non_va', 'payment_non_va.transaction_id', 'transactions.reference')
                ->where('payment_non_va.payment_id', $p_data['payment_id']);
            })->when($city_id, function ($query) use ($city_id) {
                return $query->where('popbox_virtual.cities.id', $city_id);
            })
            ->orderBy('transactions.created_at', 'desc')
            ->select('transactions.*', 'users.locker_id', 'payment_methods.code', 'payment_methods.name', 'popbox_virtual.cities.city_name', 
                'popbox_virtual.lockers.type as user_type')
            ->with('items')
            ->with('user.locker')
            ->get();

        if ($type == 'download') {

            $listDownload = [];

            foreach ($allTransaction as $item) {

                $type = '';
                if (sizeof($item->items) > 0) {
                    $type = $this->getTypesDownload($item->items);
                } else {
                    $type = $item->type;
                }

                $items = '';
                if (sizeof($item->items) > 0) {
                    $items = $this->getItemsDownload($item->items);
                }

                $result = new \stdClass();
                $result->transaction_id = $item->reference;
                $result->agent_name = $item->user->locker->locker_name;
                $result->user_type = $item->user_type;
                $result->user_status = $item->user->locker->status_verification;
                $result->city_name = $item->city_name;
                $result->types = $type;
                $result->status = $item->status;
                $result->payment_method = $item->code ? $item->code : '-';
                $result->created_at = $item->created_at->toDateTimeString();
                $result->total_price = $item->total_price;
                $result->items = $items;
                $listDownload[] = $result;
            }

            return $listDownload;
        } else {
            return $allTransaction;
        }
    }

    private function getTypesDownload($items) {
        $type = '';

        if ($items[0]->type == 'popshop') {
            return 'popshop';
        } else {
            foreach ($items as $item) {
                $str = '';
                $str .= $item->type;
                $str .= ',';
                $type .= $str;
            }
        }

        return $type;
    }

    private function getItemsDownload($data) {
        $items = '';

        foreach ($data as $value) {
//            dd($value->price);
            $str = '';
            $str .= $value->name;
            $str .= ' --> ';
            $str .= $value->price;
            $str .= ' || ';
            $items .= $str;
        }

        return $items;
    }

    private function getDataDownloadPerformance($startDate, $endDate) {

        $agentTransaction = DB::connection('popbox_agent')
            ->table('users')
            ->leftJoin('transactions',function($join) use($startDate,$endDate){
                $join->on('transactions.users_id','=','users.id')
                    ->whereBetween('transactions.created_at',[$startDate,$endDate])
                    ->whereIn('transactions.status',['PAID'])
                    ->whereNull('transactions.deleted_at');
            })
            ->leftJoin('transaction_items','transactions.id','=','transaction_items.transaction_id')
            ->join('lockers','lockers.id','=','users.locker_id')
            ->join('popbox_virtual.lockers as lockers2','lockers.id','=','lockers2.locker_id')
            ->leftJoin('popbox_virtual.users as users2','lockers2.registered_by','=','users2.id')
            ->leftJoin('popbox_virtual.list_locker_building_types as building_types2','building_types2.id','=','lockers2.building_type_id')
            ->whereNull('lockers2.deleted_at')
            ->where('lockers2.status','<>','0')
            // ->where('lockers.id','CGK170309DHW8PM74HL')
            ->select('transactions.id','transactions.type', 'transactions.total_price',
                'lockers.locker_name','lockers.id as lockers_id','lockers.balance',
                'users2.name as registered_by','lockers2.created_at as register_date','building_types2.building_types',
                'transaction_items.type as item_type','transaction_items.price as item_price','transaction_items.id as item_id'
            )
            ->get();
        // dd($agentTransaction);
        $transactionGroup = $agentTransaction->groupBy('lockers_id');
        $typeTransaction = ['payment', 'purchase'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward','referral'];
        $agentSummary = [];
        foreach ($transactionGroup as $lockerId => $transaction) {
            $purchaseAmount = 0;
            $purchaseCount = 0;

            $topUpAmount = 0;
            $topUpCount = 0;

            $transactionAmount = 0;
            $transactionCount = 0;

            $rewardAmount = 0;
            $rewardCount = 0;

            $commissionAmount = 0;
            $commissionCount = 0;

            $pulsaAmount = 0;
            $pulsaCount = 0;
            $popshopAmount = 0;
            $popshopCount = 0;
            $plnTokenAmount = 0;
            $plnTokenCount = 0;
            $plnPascaAmount = 0;
            $plnPascaCount = 0;
            $telkomAmount = 0;
            $telkomCount = 0;
            $pdamAmount = 0;
            $pdamCount = 0;
            $bpjsAmount = 0;
            $bpjsCount = 0;


            $otherAmount = 0;
            $deductAmount = 0;
            $deductCount = 0;
            $lastBalance = 0;
            $refundAmount = 0;
            $lockerName = null;
            $registeredBy = null;
            $registerDate = null;
            $buildingType = null;

            $tmpTransactionId = null;
            foreach ($transaction as $item) {
                if (in_array($item->type, $typeTransaction)) {
                    if ($item->id != $tmpTransactionId){
                        $transactionAmount += $item->total_price;
                        $transactionCount++;

                        if ($item->item_type == 'pulsa'){
                            $pulsaAmount += $item->total_price;
                            $pulsaCount++;
                        } elseif ($item->item_type == 'popshop'){
                            $popshopAmount += $item->total_price;
                            $popshopCount++;
                        } elseif ($item->item_type == 'electricity'){
                            $plnTokenAmount += $item->total_price;
                            $plnTokenCount++;
                        } elseif ($item->item_type == 'electricity_postpaid'){
                            $plnPascaAmount += $item->total_price;
                            $plnPascaCount++;
                        } elseif ($item->item_type == 'telkom_postpaid'){
                            $telkomAmount += $item->total_price;
                            $telkomCount++;
                        } elseif ($item->item_type == 'pdam'){
                            $pdamAmount += $item->total_price;
                            $pdamCount++;
                        } elseif ($item->item_type == 'bpjs_kesehatan'){
                            $bpjsAmount += $item->total_price;
                            $bpjsCount++;
                        }
                    }
                    $tmpTransactionId = $item->id;
                }
                elseif (in_array($item->type, $typeTopUp)) {
                    if ($item->id != $tmpTransactionId) {
                        $topUpAmount += $item->item_price;
                        $topUpCount++;
                        $tmpTransactionId = $item->id;
                    }
                }
                elseif (in_array($item->type, $typeReward)) {
                    $rewardAmount += $item->total_price;
                    $rewardCount++;
                }
                elseif ($item->type == 'deduct') {
                    $deductAmount += $item->total_price;
                    $deductCount++;
                }
                elseif ($item->type == 'refund') {
                    $refundAmount += $item->total_price;
                }
                elseif ($item->type == 'commission'){
                    $commissionAmount += $item->total_price;
                    $commissionCount++;
                }

                if ($item->type == 'purchase'){
                    $purchaseCount++;
                    $purchaseAmount += $item->total_price;
                }

                $lastBalance = $item->balance;
                $lockerName = $item->locker_name;
                $registeredBy = $item->registered_by;
                $registerDate = $item->register_date;
                $buildingType = $item->building_types;
            }
            $tmp = new \stdClass();
            $tmp->agentId = $lockerId;
            $tmp->agentName = $lockerName;
            $tmp->topupAmount = $topUpAmount - $deductAmount;
            $tmp->topupCount = $topUpCount - $deductCount;
            $tmp->transactionAmount = $transactionAmount;
            $tmp->transactionCount = $transactionCount;
            $tmp->rewardAmount = $rewardAmount;
            $tmp->lastBalance = $lastBalance;
            $tmp->refundAmount = $refundAmount;
            $tmp->otherAmount = $otherAmount;
            $tmp->purchaseCount = $purchaseCount;
            $tmp->purchaseAmount = $purchaseAmount;
            $tmp->commissionAmount = $commissionAmount;
            $tmp->commissionCount = $commissionCount;
            $tmp->registeredBy = $registeredBy;
            $tmp->registerDate = $registerDate;
            $tmp->buildingType = $buildingType;
            $tmp->pulsaCount = $pulsaCount;
            $tmp->pulsaAmount = $pulsaAmount;
            $tmp->popshopCount = $popshopCount;
            $tmp->popshopAmount = $popshopAmount;
            $tmp->plnTokenCount = $plnTokenCount;
            $tmp->plnTokenAmount = $plnTokenAmount;
            $tmp->plnPascaCount =$plnPascaCount;
            $tmp->plnPascaAmount = $plnPascaAmount;
            $tmp->telkomCount = $telkomCount;
            $tmp->telkomAmount = $telkomAmount;
            $tmp->pdamCount = $pdamCount;
            $tmp->pdamAmount = $pdamAmount;
            $tmp->bpjsCount = $bpjsCount;
            $tmp->bpjsAmount = $bpjsAmount;

            $agentSummary[] = $tmp;
        }

        return $agentSummary;

    }
	
    public function getTransactionWarung(Request $request)
    {       
        return view('agent.transaction.warung');
    }	
	
	public function getListTransactionWarung(Request $request) {
        $userid = $request->input('userid', 'all');
        $userid = empty($userid) ? 'all' : $userid;

        $startDate = $request->input('datetransactionstart', '');
        $endDate = $request->input('datetransactionend', '');

        $url = config('constant.popwarung.api_url').'transaction/getlisttransactioncustomer/'.$userid.'/'.$startDate.'/'.$endDate;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;		
	}
	
    public function detailTransactionWarung(Request $request, $id, $lockerid){
        $lockerDb = Locker::where('locker_id', $lockerid)->first();
        if (!$lockerDb) {
            $request->session()->flash('error', 'Locker ID not found');
            return back();
        }

        $data['transactionid'] = $id;
        $data['locker'] = $lockerDb;
        return view('agent.transaction.detailwarung', $data);
    }
	
	public function getDetailTransactionWarung(Request $request)
	{
		$transactionid = $request->input('transactionid', '');
        $url = config('constant.popwarung.api_url').'transaction/getdetailtransactioncustomer/'.$transactionid;
        $mresult = ApiPopWarung::callAPI('GET', $url, false);
        return $mresult;				
	}

    public function getListMasterWarung(Request $request) {
        $keyword = $request->input('search','');

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $lockerDb = Locker::where('status', '<>', '0')
            ->where('type', '!=', 'locker')
            ->where("locker_name", 'like', '%'.strtoupper($keyword).'%')
            ->get();

        $data = [];
        $data['lockerData'] = $lockerDb;

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }
}
