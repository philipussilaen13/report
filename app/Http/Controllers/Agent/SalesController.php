<?php

namespace App\Http\Controllers\Agent;

use App\Models\Agent\AgentLocker;
use App\Models\Agent\BalanceRecord;
use App\Models\Agent\Payment;
use App\Models\Agent\PaymentTransfer;
use App\Models\Agent\Transaction;
use App\Models\Agent\TransactionHistory;
use App\Models\Agent\User;
use App\Models\Virtual\Locker;
use App\Models\Virtual\SalesAgent;
use App\Models\Virtual\SalesTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SalesController extends Controller
{
    /**
     * get Add Sales Page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAddSalesAgent(Request $request)
    {
        // get sales user based on group
        $salesDb = SalesAgent::with(['user'])->get();

        // get user
        $userDashboardDb = \App\User::whereHas('group', function ($query) {
            $query->whereIn('name', ['sales', 'bd', 'finance']);
        })->get();

        // parse data to view
        $data = [];
        $data['salesDb'] = $salesDb;
        $data['userDashboardDb'] = $userDashboardDb;

        return view('agent.sales.add-sales', $data);
    }

    /**
     * Add Sales Agent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAddSalesAgent(Request $request)
    {
        $userDashboardId = $request->input('user');
        $deposit = $request->input('deposit', 0);

        if (!is_numeric($deposit)) {
            $request->session()->flash('error', 'Invalid Deposit Value');
            return back();
        }

        // get dashboard User
        $userDb = \App\User::find($userDashboardId);
        if (!$userDb) {
            $request->session()->flash('error', 'Invalid User');
            return back();
        }

        DB::connection('popbox_virtual')->beginTransaction();
        DB::beginTransaction();

        // get user Virtual
        $userVirtualDb = \App\Models\Virtual\User::where('email', $userDb->email)->first();
        if (!$userVirtualDb) {
            // insert new user on Virtual DB
            # get group
            $groupName = $userDb->group->name;
            // get groupDB on Virtual
            $groupVirtualDb = DB::connection('popbox_virtual')
                ->table('groups')
                ->where('name', $groupName)
                ->first();
            if (!$groupVirtualDb) {
                // insert New Group
                $groupVirtualDb = DB::connection('popbox_virtual')
                    ->insert(['name' => $groupName]);
            }
            $groupVirtualId = $groupVirtualDb->id;
            // insert new user
            $userVirtualDb = new \App\Models\Virtual\User();
            $userVirtualDb->name = $userDb->name;
            $userVirtualDb->phone = $userDb->phone;
            $userVirtualDb->email = $userDb->email;
            $userVirtualDb->username = $userDb->email;
            $userVirtualDb->password = $userDb->password;
            $userVirtualDb->groups_id = $groupVirtualId;
            $userVirtualDb->companies_id = 1;
            $userVirtualDb->save();
        }

        // check user on sales agent
        $salesDb = SalesAgent::where('user_id', $userVirtualDb->id)->first();
        if ($salesDb) {
            $request->session()->flash('error', 'Sales Agent Already Exist');
            return back();
        }


        // add sales agent
        $salesDb = new SalesAgent();
        $salesDb->user_id = $userVirtualDb->id;
        $salesDb->balance = 0;
        $salesDb->credit_limit = $deposit;
        $salesDb->status = 1;
        $salesDb->save();

        if (!empty($deposit)) {
            // create transaction
            $salesTransactionDb = new SalesTransaction();
            $result = $salesTransactionDb->createNewSalesTransaction($salesDb->id, 'sales-topup', $deposit, 'PAID');
            if ($result->isSuccess) {
                // topup sales agent
                $result = $salesDb->topUpSales($salesDb->id, $deposit);
                if (!$result->isSuccess) {
                    $request->session()->flash('error', 'Failed topup');
                    DB::connection('popbox_virtual')->rollback();
                    return back();
                }
            }
        }

        DB::connection('popbox_virtual')->commit();
        DB::commit();

        $request->session()->flash('success', 'Success Add Sales Agent');
        return back();
    }

    /**
     * Get Agent Top Up Page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAgentTopup(Request $request)
    {
        // get based on group
        $userDb = Auth::user();
        $virtualUserDb = DB::connection('popbox_virtual')
            ->table('users')
            ->where('email', $userDb->email)
            ->first();
        $groupName = $userDb->group->name;
        if ($groupName == 'sales') {
            $lockerDb = Locker::where('registered_by', $virtualUserDb->id)->get();
        } else {
            $lockerDb = Locker::get();
        }

        // get sales data
        $salesDb = SalesAgent::where('user_id', $virtualUserDb->id)->first();

        if (!$salesDb) abort(404);

        // parsing data to view
        $data = [];
        $data['agents'] = $lockerDb;
        $data['salesDb'] = $salesDb;
        return view('agent.sales.topup-agent', $data);
    }

    /**
     * Post Agent Top Up
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAgentTopup(Request $request)
    {
        $lockerId = $request->input('lockerId');
        $amount = $request->input('amount', 0);
        $remarks = $request->input('remarks', null);
        $transactionType = 'topup';
        $method = 'cash';

        // validation balance
        $userDb = Auth::user();
        $virtualUserDb = DB::connection('popbox_virtual')
            ->table('users')
            ->where('email', $userDb->email)
            ->first();
        $salesDb = SalesAgent::where('user_id', $virtualUserDb->id)->first();
        $salesId = $salesDb->id;

        if ($salesDb->balance < $amount) {
            $request->session()->flash('error', 'Insufficient Balance');
            return back();
        }

        // get agent user
        $agentUserDb = User::where('locker_id', $lockerId)->first();
        $userId = $agentUserDb->id;

        // get locker name
        $lockerDb = Locker::where('locker_id', $agentUserDb->locker_id)->first();

        DB::connection('popbox_virtual')->beginTransaction();
        DB::connection('popbox_agent')->beginTransaction();

        // create transaction Sales
        $salesTransactionDb = new SalesTransaction();
        $created = $salesTransactionDb->createNewSalesTransaction($salesId, 'agent-topup', $amount, 'UNPAID');
        if (!$created->isSuccess) {
            DB::connection('popbox_agent')->rollback();
            DB::connection('popbox_virtual')->rollback();
            $request->session()->flash('error', $created->errorMsg);
            return back();
        }
        $salesTransactionId = $created->transactionId;
        $salesTransactionDb = SalesTransaction::find($salesTransactionId);

        if (empty($remarks)) {
            $remarks = "Top Up Cash by " . $userDb->name . " Ref:" . $salesTransactionDb->transaction_id;
        }

        $creditTransactionDb = Transaction::createCreditTransaction($transactionType, $userId, $remarks, $amount);
        if (!$creditTransactionDb->isSuccess) {
            $request->session()->flash('error', 'Failed Transaction');
            DB::connection('popbox_agent')->rollback();
            DB::connection('popbox_virtual')->rollback();
            return back();
        }
        $transactionId = $creditTransactionDb->transactionId;
        $transactionRef = $creditTransactionDb->transactionRef;
        if ($transactionType == 'topup') {
            $paymentDb = Payment::addPayment($transactionRef, $method, 'topup');
            if (!$paymentDb->isSuccess) {
                DB::connection('popbox_agent')->rollback();
                DB::connection('popbox_virtual')->rollback();
                $message = $paymentDb->errorMsg;
                $request->session()->flash('error', $message);
                return back();
            }
        }

        $balanceRecordDb = BalanceRecord::creditDeposit($lockerId, $amount, $transactionId);
        if (!$balanceRecordDb->isSuccess) {
            DB::connection('popbox_agent')->rollback();
            DB::connection('popbox_virtual')->rollback();
            $request->session()->flash('error', 'Failed Credit Transaction');
            return back();
        }

        // change to PAID
        $transactionDb = Transaction::find($transactionId);
        $transactionDb->status = 'PAID';
        $transactionDb->save();
        if ($transactionType == 'topup') {
            $paymentDb = Payment::find($transactionDb->payment->id);
            $paymentDb->status = 'PAID';
            $paymentDb->save();
        }

        $transactionId = $transactionDb->id;
        $username = $userDb->name;
        $remarks = "Confirmed by $username";
        $historyDb = TransactionHistory::createNewHistory($transactionId, $username, 'PAID', $remarks);
        if (!$historyDb->isSuccess) {
            DB::connection('popbox_agent')->rollback();
            DB::connection('popbox_virtual')->rollback();
            $request->session()->flash('error', 'Failed To create Transaction History');
            return back();
        }

        // deduct sales
        $salesDb = new SalesAgent();
        $deduct = $salesDb->deductSales($salesId, $amount);
        if (!$deduct->isSuccess) {
            DB::connection('popbox_agent')->rollback();
            DB::connection('popbox_virtual')->rollback();
            $request->session()->flash('error', $deduct->errorMsg);
            return back();
        }
        $salesTransactionDb->status = 'PAID';
        $salesTransactionDb->description = "Top Up to Agent $lockerDb->locker_name Rp $amount Ref : $transactionRef.";
        $salesTransactionDb->save();


        DB::connection('popbox_agent')->commit();
        DB::connection('popbox_virtual')->commit();
        $request->session()->flash('success', 'Success Credit Transaction');
        return back();
    }

    /**
     * Ajax Get Agent Data
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAgentData(Request $request)
    {
        // create default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $lockerId = $request->input('lockerId', null);

        if (empty($lockerId)) {
            $response->errorMsg = 'Agent Empty';
            return response()->json($response);
        }
        $agentUserDb = User::where('locker_id', $lockerId)->first();
        if (!$agentUserDb) {
            $response->errorMsg = 'Agent User not Found';
            return response()->json($response);
        }
        $lockerDb = Locker::where('locker_id', $lockerId)->first();
        $lastTransactionDb = Transaction::where('users_id', $agentUserDb->id)
            ->where('type', 'topup')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        $agentLockerDb = AgentLocker::find($lockerId);

        // get agent Detail
        $data = [];
        $data['agentUser'] = $agentUserDb;
        $data['locker'] = $lockerDb;
        $data['transaction'] = $lastTransactionDb;
        $data['agentLocker'] = $agentLockerDb;
        $data['lockerId'] = $lockerId;
        $view = view('agent.sales._agent-detail', $data)->render();

        $response->isSuccess = true;
        $response->view = $view;
        $response->data = $data;
        return response()->json($response);
    }

    /**
     * Get Transaction and Request Top Up
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRequestTopUp()
    {
        // get sales data
        $userDb = Auth::user();
        $userVirtualDb = DB::connection('popbox_virtual')
            ->table('users')
            ->where('email', $userDb->email)
            ->first();
        $salesDb = SalesAgent::where('user_id', $userVirtualDb->id)->first();

        if (!$salesDb) abort(404);

        $transactionSalesDb = SalesTransaction::where('sales_agent_id', $salesDb->id)->get();

        $data = [];
        $data['salesDb'] = $salesDb;
        $data['transactionSalesDb'] = $transactionSalesDb;

        return view('agent.sales.topup-request', $data);
    }

    /**
     * sales get request top up
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRequestTopUp(Request $request)
    {
        $amount = $request->input('amount');
        $bankDestination = $request->input('bank');

        if (empty($amount) || empty($bankDestination)) {
            $request->session()->flash('error', 'Amount and Bank cannot be empty');
            return back();
        }
        if (!is_numeric($amount)) {
            $request->session()->flash('error', 'Amount must be Number');
            return back();
        }

        // save to DB
        $userDb = Auth::user();
        $userVirtualDb = DB::connection('popbox_virtual')
            ->table('users')
            ->where('email', $userDb->email)
            ->first();
        $salesDb = SalesAgent::where('user_id', $userVirtualDb->id)->first();
        if (!$salesDb) {
            $request->session()->flash('error', 'Sales not found');
            return back();
        }

        $type = 'sales-topup';
        $remarks = "Sales Request Top Up $amount to $bankDestination";

        DB::connection('popbox_virtual')->beginTransaction();

        $salesTransactionDb = new SalesTransaction();
        $result = $salesTransactionDb->createNewSalesTransaction($salesDb->id, $type, $amount, 'PENDING', $remarks);
        if (!$result->isSuccess) {
            DB::connection('popbox_virtual')->rollback();
            $request->session()->flash('error', $result->errorMsg);
            return back();
        }
        $transactionId = $result->transactionId;
        $salesTransactionDb = SalesTransaction::find($transactionId);

        // save file
        $file = $request->file('imgTransfer');
        $fileName = $salesTransactionDb->transaction_id . ".jpg";
        // move file
        $request->file('imgTransfer')->move(public_path() . '/files/sales/', $fileName);

        // parse data to email
        $data = [];
        $data['salesName'] = $userDb->name;
        $data['topupAmount'] = $amount;
        $data['bank'] = $bankDestination;
        $data['transactionRef'] = $salesTransactionDb->transaction_id;

        $environment = App::environment();
        if ($environment == 'production') {
            // dian@popbox.asia , finance@popbox.asia
           Mail::send('email.sales.topup-pending', ['data' => $data], function ($m) use ($environment, $salesTransactionDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                // $m->to('lidya@popbox.asia', 'Finance PopBox Asia')->subject("[Agent][$environment] Sales Agent Top Up Pending : $salesTransactionDb->transaction_id");
                $m->to(['lidya@popbox.asia','dian@popbox.asia','finance@popbox.asia'], 'Finance PopBox Asia')->subject("[Agent][$environment] Sales Agent Top Up Pending : $salesTransactionDb->transaction_id");
                $m->bcc('sigit@popbox.asia', 'IT PopBox Asia')->subject("[Agent][$environment] Sales Agent Top Up Pending : $salesTransactionDb->transaction_id");
            });
        } else {
           Mail::send('email.sales.topup-pending', ['data' => $data], function ($m) use ($environment, $salesTransactionDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                $m->to('arief@popbox.asia', 'IT PopBox Asia')->subject("[Agent][$environment] Sales Agent Top Up Pending : $salesTransactionDb->transaction_id");
            });
        }

        DB::connection('popbox_virtual')->commit();

        $request->session()->flash('success', 'Success Request TopUp. Please Wait for Finance team to confirmed');
        return back();
    }
}
