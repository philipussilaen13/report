<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helper;
use App\Http\Helpers\SanitizeHelper;
use App\Models\Agent\Payment;
use App\Models\Agent\Transaction;
use App\Models\Agent\TransactionItem;
use App\Models\Virtual\Locker;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CodController extends Controller
{
    public function index(Request $request){
        $transaction_status = [
            ['id' => 'ALL',     'name' => 'ALL'],
            ['id' => 'OPEN',    'name' => 'OPEN'],
            ['id' => 'CLOSED',  'name' => 'CLOSED'],
        ];
        
        $status_open = '';
        $status_closed = '';
        
        return view('agent.cod.list', compact('transaction_status', 'status_open', 'status_closed'));
    }
    
    public function list(Request $request){
        
        $input = $request->input();
        $dateRange = $request->input('daterange_transaction', null);
        
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $startDate = substr(Helper::formatDateRange($dateRange, ',')->startDate, 0, 10);
            $endDate = substr(Helper::formatDateRange($dateRange, ',')->endDate, 0, 10);
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";
        
        // $lockerDb = Locker::where('status', '<>', '0')->where('type', '!=', 'locker')->orderBy('locker_name', 'asc')->get();
        
        $lockerId = $request->input('locker', '');
        $status = $request->input('transactionstatus', 'all');
        $transactionId = $request->input('transactionid', '');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 15);
        $type = 'popshop';
        
        $rows = Transaction::with('items','histories')
            ->withTrashed()
            ->join('users', 'users.id', '=', 'transactions.users_id')
            ->join('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'users.locker_id')
            ->join('payments', 'payments.transactions_id', 'transactions.id')
            ->join('payment_methods', 'payment_methods.id', 'payments.payment_methods_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('payment_methods.code', 'cash')
            ->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('users.locker_id', $lockerId);
            })->when($status, function ($query) use ($status) {
                if($status === 'CLOSED') {
                    return $query->where('transactions.status', 'PAID');
                } elseif($status === 'OPEN') {
                    return $query->whereIn('transactions.status', ['UNPAID', 'PENDING', 'PAID', 'WAITING', 'EXPIRED', 'REFUND'])
                    ->whereHas('items',function ($query) use ($status){
                        $query->whereNull('cod_status')->orWhere('cod_status', 'not_yet_delivery');
                    });
                }
                return $query->whereIn('transactions.status', ['UNPAID', 'PENDING', 'PAID', 'WAITING', 'EXPIRED', 'REFUND']);
            })->when($type, function ($query) use ($type) {
//                  if (in_array('commission',$type) || in_array('reward',$type)){
//                      return $query->whereIn('transactions.type',$type);
//                  }
                 return $query->whereHas('items',function ($query) use ($type){
                     if (is_array($type)) $query->whereIn('type',$type);
                     else  $query->where('type',$type);
             });
                /*return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')->whereIn('transaction_items.type', $type);*/
//          })->when($typeeee, function ($query) use ($typeeee) {
//                  return $query->where('popbox_virtual.lockers.type', $typeeee);
            })->when($transactionId, function ($query) use ($transactionId) {
                return $query->where('reference', $transactionId);
//          })->when($itemParam, function ($query) use ($itemParam) {
//                  return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
//                      ->where('transaction_items.params', 'LIKE', "%$itemParam%");
//          })->when($itemReference, function ($query) use ($itemReference) {
//                  return $query->join('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
//                      ->where('transaction_items.reference', $itemReference);
        })->orderBy('transactions.created_at', 'desc')
        ->select('transactions.*', 'users.locker_id')
        ->with('items')
        ->with('user.locker');
        
        $count = $rows->get();
        $record = $rows->offset($start)->limit($limit)->get();;
        
        $status_open = 0;
        $status_closed = 0;
        
        foreach ($count as $row){
            if($row['status'] === 'PAID'){
                $status_closed++;
            } else {
                $status_open++;
            }
        }
        
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";
        
        // parsing data to view
        $data = [];
        $data['recordsTotal'] = sizeof($count);
        $data['parameter'] = $param;
        $data['allTransaction'] = SanitizeHelper::cleansingNull($record, $start);
//         $data['lockerData'] = $lockerDb;
        $data['transactionId'] = $transactionId;
        $data['status_open'] = $status_open;
        $data['status_closed'] = $status_closed;
        
        return $data;
    }
    
    public function show(Request $request, $id, $lockerid) {
//         $data = array();
        
        $statuses = [
            [
                'id' => 'delivered',
                'name' => 'Sudah Diterima'
            ],
            [
                'id' => 'partially_delivered',
                'name' => 'Diterima Sebagian'
            ],
            [
                'id' => 'cancelled',
                'name' => 'Batal'
            ]
        ];
        
        $transaction = Transaction::withTrashed()
        ->join('users', 'users.id', '=', 'transactions.users_id')
        ->select('transactions.*', 'users.locker_id', 'users.name')->where('transactions.id', $id)->get();
        
        $status_item = 'delivered';
        $status = [ 'delivered', 'partially', 'canceled'
        ];
        foreach ($transaction[0]['items'] as $item){
            if($item['cod_status'] === null || $item['cod_status'] === '' || in_array($item['cod_status'], $status)) {
                $status_item = $item['cod_status'];
                break;
            }
        }
        $history_size = sizeof($transaction[0]['histories']);
        $last_updated_by = $transaction[0]['histories'][$history_size - 1]['user'];
        $last_updated = $transaction[0]['histories'][$history_size - 1]['created_at'];
        
//         $data['trx_id'] = $id;
//         $data['status_item'] = $status_item;
//         $data['locker_id'] = $lockerid;
//         $data['transaction'] = $transaction;
        
        return view('agent.cod.detail', compact('id', 'locker_id', 'transaction', 'status_item', 'statuses', 'last_updated_by', 'last_updated'));
    }
    
    public function detail(Request $request) {
        
        $input = $request->input();
        $trx_id = $request->get('transactionid');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 15);
        
        $rows = TransactionItem::where('transaction_id', $trx_id)
                ->select(
                    DB::Raw(
                        'id, transaction_id, commission_schemas_id, commission_amount, name, 
                        type, picture, params, price,remarks, created_at, updated_at, reference, sn_status, serial_number, delivery_status, 
                        IFNULL( `transaction_items`.`cod_status` , "not_yet_delivery" ) as cod_status,
                        CASE WHEN `transaction_items`.`cod_status` != "not_yet_delivery" THEN "disabled" END as status_d '
                ));
        
        $count = $rows->get();
        $record = $rows->offset($start)->limit($limit)->get();;
        
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        
        $data = array();
        $data['parameter'] = $param;
        $data['recordsTotal'] = sizeof($count);
        $data['transactionItems'] = $record;
        
        return $data;
    }
    
    public function update(Request $request){
        
        $items = $request->input('items', []);
        //$transactionId = $request->input('transactionId', '');
        $transaction_reference = $request->input('transactionReference', '');
        //$lockerId = $request->input('lockerId', '');
        
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        $allItemsUpdated = true;
        try{
            $transactionDb = Transaction::where('reference', $transaction_reference)->first();
            if (!$transactionDb){
                return response()->json(["error" => 'Invalid Transaction Reference'], 500);
            }
//             if ($transactionDb->status=='PAID'){
//                 return response()->json(["error" => 'Transaction Already Paid'], 500);
//             }
            if ($transactionDb->status=='CANCELLED'){
                return response()->json(["error" => 'Transaction Already Cancelled'], 500);
            }
            
            if ($transactionDb->status=='EXPIRED'){
                return response()->json(["error" => 'Transaction Already Expired'], 500);
            }
            
            if ($transactionDb->status == 'REFUND') {
                return response()->json(["error" => 'Transaction Already Refunded'], 500);
            }
            
            foreach ($items as $item){
                $payment = Payment::updateTransactionItemStatus($transactionDb->id, $item, Auth::user()->name);
                if (!$payment->isSuccess) {
                    
                    $allItemsUpdated = false;
                    \Log::debug($payment->errorMsg);
                    throw new \Exception($payment->errorMsg);
                }
            }
            
            if($allItemsUpdated){
                // update transaction
                $transactionItems = TransactionItem::where('transaction_id', $transactionDb->id)->whereNull('cod_status')->orWhere('cod_status', 'not_yet_delivery')->count();
                
                if($transactionItems === 0){
                    $transactionDb = Transaction::updateStatusTransaction($transactionDb->id,'PAID');
                    if (!$transactionDb->isSuccess){
                        \Log::debug($transactionDb->errorMsg);
                        throw new \Exception($transactionDb->errorMsg);
                    }
                }
            }
        } catch (\Exception $e){
            \Log::debug($e->getMessage());
            return response()->json(["error" => $e->getMessage()], 500);
        }
        $response->data = ['status' => 'Updated DONE'];
        $response->isSuccess = true;
        
        return response()->json($response);
    }
}