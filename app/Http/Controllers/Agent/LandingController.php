<?php

namespace App\Http\Controllers\Agent;

use App\Http\Helpers\Helper;
use App\Models\Agent\Transaction;
use App\Models\Popshop\DimoProduct;
use App\Models\Popshop\DimoProductCategoryName;
use App\Models\Virtual\BuildingType;
use App\Models\Virtual\City;
use App\Models\Virtual\Locker;
use App\Models\Virtual\LockerService;
use App\Models\Virtual\Province;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LandingController extends Controller
{

    private $online = 'online';
    private $offline = 'offline';
    private $today = 'today';
    private $lastSevenDays = 'lastSevenDays';
    private $lastThirtyDays = 'lastThirtyDays';
    private $thisMonth = 'month';

    /**
     * Landing Agent
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLanding(Request $request){
        // get all locker list
        $lockerDb = Locker::where('type','agent')->get();

        $totalAgent = 0;
        $totalThisWeek = 0;
        $totalThisMonth = 0;
        $totalWaiting = 0;

        $startThisMonth = date('Y-m-1')." 00:00:00";
        $endThisMonth = date('Y-m-t')." 23:59:59";
        $startThisWeek = date('Y-m-d',strtotime('last monday'))." 00:00:00";
        $endThisWeek = date('Y-m-d',strtotime('this sunday'))." 23:59:59";

        foreach ($lockerDb as $locker){
            if ($locker->created_at >= $startThisWeek && $locker->created_at <= $endThisWeek) $totalThisWeek++;
            if ($locker->created_at >= $startThisMonth && $locker->created_at <= $endThisMonth) $totalThisMonth++;
            if ($locker->status == 0) $totalWaiting++;
            else $totalAgent++;
        }

        // get data for view
        $listLockerBuilding = BuildingType::get();
        // list province
        $listProvinces = Province::get();
        // list city
        $listCities = City::get();

        // parsing data to view
        $data = [];
        $data['totalAgent'] = $totalAgent;
        $data['totalWaiting'] = $totalWaiting;
        $data['totalThisMonth'] = $totalThisMonth;
        $data['totalThisWeek'] = $totalThisWeek;
        $data['startMonth'] = $startThisMonth;
        $data['endMonth'] = $endThisMonth;
        $data['startWeek'] = $startThisWeek;
        $data['endWeek'] = $endThisWeek;
        $data['listLockerBuilding'] = $listLockerBuilding;
        $data['listProvinces'] = $listProvinces;
        $data['listCities'] = $listCities;

        return view('agent.landing',$data);
    }

    /**
     * Ajax Get Transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxGetTransaction(Request $request){
        // set default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $beginWeekDate = date('Y-m-d',strtotime('this week monday'));
        $endWeekDate = date('Y-m-d',strtotime('this week sunday'));

        $paidTransactionItems = Transaction::leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$beginWeekDate, $endWeekDate])
            ->where('transactions.status', 'PAID')
            ->orderBy('transactions.created_at', 'desc')
            ->select('transaction_items.*','transactions.type as trans_type','transactions.total_price')
            ->get();

        $transactionList = [];
        $transactionCount = 0;
        $transactionAmount = 0;
        $topupAmount = 0;
        $topupCount = 0;
        $rewardAmount = 0;
        $rewardCount = 0;
        $pulsaCount = 0;
        $pulsaSum = 0;
        $paymentCount = 0;
        $paymentSum = 0;
        $popshopCount = 0;
        $popshopSum = 0;
        $deliveryCount = 0;
        $deliverySum = 0;
        $purchaseTransactionCount = 0;
        $purchaseTransactionSum = 0;
        $paymentTransactionCount= 0;
        $paymentTransactionSum = 0;
        $commissionCount= 0;
        $commissionAmount= 0;

        $typeTransaction = ['popshop', 'pulsa', 'electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid', 'delivery'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward', 'commission','referral'];

        $typePulsa = ['pulsa'];
        $typePopShop = ['popshop'];
        $typePayment = ['electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid'];
        $typeRefund = ['refund', 'deduct'];
        $typeDelivery = ['delivery'];

        foreach ($paidTransactionItems as $item) {
            // for transaction summary
            if (in_array($item->type, $typeTransaction)) {
                $transactionAmount += $item->price;
                if ($item->type != 'admin_fee'){
                    $transactionList[] = $item;
                    $transactionCount++;
                }
            } elseif (in_array($item->type, $typeTopUp)) {
                $topupAmount += $item->price;
                $topupCount++;
            } elseif (in_array($item->trans_type, $typeReward)) {
                $rewardAmount += $item->total_price;
                $rewardCount++;
            }

            // for product summary
            if (in_array($item->type, $typePulsa)) {
                $pulsaCount++;
                $pulsaSum += $item->price;
            } elseif (in_array($item->type, $typePopShop)) {
                $popshopCount++;
                $popshopSum += $item->price;
            } elseif (in_array($item->type, $typePayment)) {
                $paymentCount++;
                $paymentSum += $item->price;
            } elseif (in_array($item->type, $typeDelivery)) {
                $deliveryCount++;
                $deliverySum += $item->price;
            }
        }

        // parse for response
        $data['transactionCount'] = $transactionCount;
        $data['transactionAmount'] = $transactionAmount;
        $data['topupAmount'] = $topupAmount;
        $data['topupCount'] = $topupCount;
        $data['rewardAmount'] = $rewardAmount;
        $data['rewardCount'] = $rewardCount;

        $data['pulsaCount'] = $pulsaCount;
        $data['pulsaSum'] = $pulsaSum;
        $data['paymentCount'] = $paymentCount;
        $data['paymentSum'] = $paymentSum;
        $data['popshopCount'] = $popshopCount;
        $data['popshopSum'] = $popshopSum;
        $data['deliveryCount'] = $deliveryCount;
        $data['deliverySum'] = $deliverySum;
        $data['beginWeekDate'] = $beginWeekDate;
        $data['endWeekDate'] = $endWeekDate;
        $response->isSuccess = true;
        $response->data = $data;

        return response()->json($response);
    }

    public function ajaxGetProduct(Request $request){
        // get all top product
        $sepulsaTransactionDb = DB::connection('popbox_db')->table('tb_sepulsa_transaction AS tst')
            ->where('status','success')
            ->select(DB::raw("tst.product_description, COUNT(tst.id) as 'count'"))
            ->groupBy('tst.product_id')
            ->orderBy('count','desc')
            ->get();
        dd($sepulsaTransactionDb);
    }


    /*------------------------------------------------TOPSALES------------------------------------------*/

    public function getLanding2() {
        return view('agent.landing2');
    }

    public function ajaxGetTopSellingOffline() {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // categories
        $categoriesDb = DimoProductCategoryName::with('products')
            ->where('category_name','LIKE',"%Grocery%")
            ->get();

        $countSku = 0;
        foreach ($categoriesDb as $item) {
            $countSku += count($item->products);
        }

        // count top product
        $topProduct = $this->getProductBasedOnDate('offline','lastSevenDays');

//        dd($topProduct);

        $productsDb = DimoProduct::with('categories')
            ->whereHas('categories',function ($query){
                $query->where('category_name','LIKE',"%Grocery%");
            })
            ->orderBy('id','desc')
            ->paginate(20);

        $data = [];
        $data['categoriesDb'] = $categoriesDb;
        $data['productsDb'] = $productsDb;
        $data['topProduct'] = $topProduct;
        $data['countSku'] = $countSku;

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    /**
     * Get Sepulsa Transaction List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ajaxGetTopSellingOnline(){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        /*Get Top Product*/
        $topProductDb = $this->getProductBasedOnDate('online','lastSevenDays');

        $categeryProductDb = DB::connection('popbox_db')
            ->table('tb_sepulsa_product as tspl')
            ->where('tspl.type', '!=', 'multi')
            ->groupBy('tspl.operator')
            ->get();

        $countProductDb = count($categeryProductDb);

        /*Get for graph*/
//        $graphTransactionDb = DB::connection('popbox_db')
//            ->table('tb_sepulsa_transaction as tst')
//            ->leftJoin('tb_sepulsa_product as tsp','tst.product_id','=','tsp.product_id')
//            ->whereBetween('tst.created_at',[$startDate,$endDate])
//            ->where('rc','00')
//            ->select('tst.*','tsp.operator')
//            ->get();

        // parse data to view
        $data = [];
        $data['topProduct'] = $topProductDb;
        $data['countProductDb'] = $countProductDb;

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function ajaxGetFloatingMoney() {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $floatingMoney = DB::connection('popbox_agent')
            ->table('lockers')
            ->where('balance', '!=', 0)
            ->select('balance')
            ->get();

        $total = 0;
        foreach ($floatingMoney as $item) {
            $total += (int)$item->balance;
        }

        // parse data to view
        $data = [];
        $data['floating_money'] = $total;

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function ajaxGetTopSellingAgent() {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $agentDb = DB::connection('popbox_virtual')
            ->table('lockers')
            ->where('type', '!=', 'locker')
            ->whereNull('deleted_at')
            ->get();
        $totalAgent = count($agentDb);

        $topProduct = $this->getProductBasedOnDate('agent', 'lastSevenDays');

//        dd($topProduct);

        // parse data to view
        $data = [];
        $data['totalAgent'] = $totalAgent;
        $data['topProduct'] = $topProduct;


        $response->data = $data;
        $response->isSuccess = true;
        return response()->json($response);
    }

    public function ajaxGetFilter(Request $request) {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $type = $request->input('type');
        $time = $request->input('time');

        $date = $this->getStartAndEndDate($time);

        if ($type === 'todayTransactions') {
            $data = $this->getProductBasedOnDate($type, $time);
        } else {
            // count top product
            $topProduct = $this->getProductBasedOnDate($type, $time);
            // parse data to view
            $data = [];
            $data['topProduct'] = $topProduct;
            $data['startDate'] = $date->startDate;
            $data['endDate'] = $date->endDate;
        }

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    /*------------------------------------------------USERINSIGHT------------------------------------------*/
    public function getUserInsight() {
        return view('agent.insight.insight');
    }

    public function getInsightAjaxFilter(Request $request) {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $type = $request->input('type');
        $time = $request->input('time');

//        dd($type);

        $date = $this->getStartAndEndDate($time);

        $data = [];
        $countAgent = 0;
        $countWarung = 0;

        $filterResult = $this->getFilterLockerType($type, $time);

        $lockerDb = $filterResult['lockerDb'];
        $chartDb = $filterResult['chartDb'];

        if ($type == 'registeredDate') {

            foreach ($lockerDb as $value) {
                if ($value->type == 'agent') {
                    $countAgent +=1;
                } else if ($value->type == 'warung') {
                    $countWarung +=1;
                }
            }

        } else if ($type == 'transactionDate') {

            foreach ($lockerDb as $value) {
                if ($value->type == 'agent') {
                    $countAgent++;
                } else if ($value->type == 'warung') {
                    $countWarung++;
                }
            }

            $arr_datee = [];

            foreach ($chartDb as $value) {
                $arr_datee[] = $value->created_at;
            }
            $arr_date = array_unique($arr_datee);

            $arr_data = [];
            foreach ($arr_date as $keyDate => $date) {
                $dataCount = new \stdClass();
                $countAgentt = 0;
                $countWarungg = 0;
                foreach ($chartDb as $keyValue => $value) {
                    if ($value->created_at == $date) {
                        if ($value->type == 'agent') {
                            $countAgentt += 1;
                        } else if ($value->type == 'warung') {
                            $countWarungg += 1;
                        }
                    }
                }
                $dataCount->date = $date;
                $dataCount->agent = $countAgentt;
                $dataCount->warung = $countWarungg;
                $arr_data[] = $dataCount;
            }

            $chartDb = $arr_data;
        } else if ($type == 'transactionCount') {

            $countAgentAmount = 0;
            $countWarungAmount = 0;

            foreach ($lockerDb as $item) {
                $countAgent += (int)$item->agent_transactions;
                $countWarung += (int)$item->warung_transactions;

                $countAgentAmount += (int)$item->total_price_agent;
                $countWarungAmount += (int)$item->total_price_warung;

                $data['amountAll'] = $countAgentAmount + $countWarungAmount;
                $data['amountAgent'] = $countAgentAmount;
                $data['amountWarung'] = $countWarungAmount;
            }
        }

        $data['countAll'] = $countAgent + $countWarung;
        $data['countAgent'] = $countAgent;
        $data['countWarung'] = $countWarung;
        $data['chart'] = $chartDb;
        $data['startDate'] = $date->startDate;
        $data['endDate'] = $date->endDate;

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getInsightAllType() {

    }


    /*------------------------------------------------MAP------------------------------------------*/

    /**
     * Ajax Get Map Filtering
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMapFilter(Request $request){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $buildingType = $request->input('buildingType',null);
        $provinceId = $request->input('province',null);
        $cityId = $request->input('city',null);

        DB::connection('popbox_virtual')->enableQueryLog();

        $lockerDb = Locker::where('type','=','agent')
            ->where('status','<>',0)
            ->when($buildingType,function($query) use($buildingType){
                return $query->where('building_type_id',$buildingType);
            })->when($provinceId,function ($query) use ($provinceId){
                return $query->join('cities','cities.id','=','lockers.cities_id')
                    ->join('provinces','provinces.id','=','cities.provinces_id')
                    ->where('provinces.id',$provinceId);
            })->when($cityId,function ($query) use ($cityId){
                return $query->where('cities_id',$cityId);
            })
            ->get();

        $locations = [];
        foreach ($lockerDb as $locker){
            $tmp = new \stdClass();
            $tmp->locker_id = $locker->locker_id;
            $tmp->locker_name = $locker->locker_name;
            $tmp->address = $locker->address;
            $tmp->operational_hour = $locker->open_hour." - ".$locker->close_hour;
            $tmp->status = $locker->status;
            $tmp->lat = (float)$locker->latitude;
            $tmp->lng = (float)$locker->longitude;
            $mapIcon = 1;
            // if ($locker->status == 1) $mapIcon = 1;
            // elseif ($locker->status == 2) $mapIcon = 2;
            $tmp->map_icon = $mapIcon;
            $locations[] = $tmp;
        }

        $responseDb = DB::connection('popbox_virtual')->getQueryLog();

        $response->data = $locations;
        $response->query = $responseDb;

        $response->isSuccess = true;
        return response()->json($response);
    }


    /*------------------------------------------------PRIVATE FUNCTION------------------------------------------*/

    private function getStartAndEndDate($time) {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        $response->startDate = null;
        $response->endDate = null;

        if ($time == 'today') {
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d');
        } else if ($time == 'lastSevenDays') {
            $startDate = date('Y-m-d', strtotime("-7 days"));
            $endDate = date('Y-m-d');
        } else if ($time == 'lastThirtyDays') {
            $startDate = date('Y-m-d', strtotime("-30 days"));
            $endDate = date('Y-m-d');
        } else if ($time == 'month') {
            $startDate = date('Y-m-01');
            $endDate = date('Y-m-d');
        } else if ($time == 'allTime') {
            $startDate = null;
            $endDate = null;
        }

        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $response->startDate = $startDate;
        $response->endDate = $endDate;

        $response->isSuccess = true;
        return $response;
    }

    private function getLabelTimeForGraph($time) {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        $response->date = null;

        if ($time == 'today') {
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d');
        } else if ($time == 'lastSevenDays') {
            $date = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        } else if ($time == 'lastThirtyDays') {
            $startDate = date('Y-m-d', strtotime("-30 days"));
            $endDate = date('Y-m-d');
        } else if ($time == 'month') {
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d');
        } else if ($time == 'allTime') {
            $startDate = null;
            $endDate = null;
        }

        $response->startDate = $startDate;
        $response->endDate = $endDate;

        $response->isSuccess = true;
        return $response;
    }

    private function getProductBasedOnDate($type, $time) {

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');

        $date = "";
        if ($time != 'allTime') {
//            dd($time);
            $date = $this->getStartAndEndDate($time);
        }

        if ($type == 'offline') {

            // count top product offline
            $topProductDb = DB::connection('popbox_db')
                ->table('dimo_products as dp')
                ->leftJoin('order_products as op','op.sku', '=', 'dp.sku')
                ->leftJoin('orders as o','o.invoice_id', '=', 'op.invoice_id')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('o.check_out_date',[$date->startDate,$date->endDate]);
//                    $query->whereDate('o.check_out_date',[$date->startDate,$date->endDate]);
                })
                ->select(DB::raw('dp.sku, dp.name, SUM(op.quantity) as sum, SUM(op.price) as total_order'))
                ->groupBy('dp.sku')
                ->orderBy('sum','desc')
                ->take(50)
                ->get();

//            if ($time != 'allTime') {
//                $result = $topProductDb->take(50)->get()->toArray();
//            } else {
//                $result = $topProductDb->get()->toArray();
//            }

            return $topProductDb;

        } else if ($type == 'online') {

            /*Get Top Product*/
            $topProductDb = DB::connection('popbox_db')
                ->table('tb_sepulsa_product as tsp')
                ->leftJoin('tb_sepulsa_transaction as tst','tst.product_id', '=', 'tsp.product_id')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('tst.created_at',[$date->startDate,$date->endDate]);
//                    $query->whereDate('tst.created_at',[$date->startDate,$date->endDate]);
                })
                ->select(DB::raw('tsp.operator, tsp.label,tsp.product_id, count(tst.id) as \'count_transaction\', sum(tst.product_amount) as \'total_transaction\''))
                ->groupBy('tsp.product_id')
                ->orderBy('count_transaction','DESC')
                ->take(50)
                ->get();

//            if ($time != 'allTime') {
//                $result = $topProductDb->take(50)->get()->toArray();
//            } else {
//                $result = $topProductDb->get()->toArray();
//            }

            return $topProductDb;

        } else if ($type == 'agent') {

            // agent buying offline
            $topProductDb = DB::connection('popbox_agent')
                ->table('transactions')
                ->leftJoin('users', 'users.id', '=', 'transactions.users_id')
                ->leftJoin('lockers', 'lockers.id', '=', 'users.locker_id')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('transactions.created_at',[$date->startDate,$date->endDate]);
//                    $datee = DB::raw('DATE_FORMAT(transactions.created_at, "%Y-%m-%d")');
//                    $query->whereDate('transactions.created_at',[$date->startDate,$date->endDate]);
                })
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('users.id, users.name, lockers.locker_name,
                                    lockers.balance as floating_money,
                                    sum( IF(transactions.cart_id IS NULL, 1, 0)) as digital,
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0)) as nondigital,
                                    sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) AS digital_price,
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0)) AS nondigital_price,
                                    (sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0))) AS total_price,
                                    (sum( IF(transactions.cart_id IS NULL, 1, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0))) AS total_jumlah'))
                ->groupBy('transactions.users_id')
                ->orderBy('total_jumlah', 'desc')
                ->take(50)
                ->get();

//            if ($time != 'allTime') {
//                $result = $topProductDb->take(50)->get()->toArray();
//            } else {
//                $result = $topProductDb->get()->toArray();
//            }

            return $topProductDb;

        } else if ($type === 'chartOfflineOnlineSales') {

            // chart offline online sales
            $topSalesDb = DB::connection('popbox_agent')
                ->table('transactions')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('transactions.created_at',[$date->startDate,$date->endDate]);
//                    $datee = DB::raw('DATE_FORMAT(transactions.created_at, "%Y-%m-%d")');
//                    $query->whereDate('transactions.created_at',[$date->startDate,$date->endDate]);
                })
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('date(transactions.created_at) as created_at,
                                    sum( IF(transactions.cart_id IS NULL, 1, 0)) as digital,
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0)) as nondigital,
                                    sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) AS digital_price,
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0)) AS nondigital_price,
                                    (sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0))) AS total_price,
                                    (sum( IF(transactions.cart_id IS NULL, 1, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0))) AS total_jumlah'))
                ->groupBy(DB::raw('date(transactions.created_at)'))
                ->orderBy(DB::raw('date(transactions.created_at)'))
                ->get();
            return $topSalesDb;

        } else if ($type === 'todayTransactions') {

            // get today and yesterday transactions
            $topSalesDb = DB::connection('popbox_agent')
                ->table('transactions')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('transactions.created_at',[$date->startDate,$date->endDate]);
//                    $datee = DB::raw('DATE_FORMAT(transactions.created_at, "%Y-%m-%d")');
//                    $query->whereDate('transactions.created_at',[$date->startDate,$date->endDate]);
                })
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('date(transactions.created_at) as created_at,
                                    sum( IF(transactions.cart_id IS NULL, 1, 0)) as digital,
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0)) as nondigital,
                                    sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) AS digital_price,
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0)) AS nondigital_price,
                                    (sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0))) AS total_price,
                                    (sum( IF(transactions.cart_id IS NULL, 1, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0))) AS total_jumlah'))
                ->groupBy(DB::raw('date(transactions.created_at)'))
                ->orderBy(DB::raw('date(transactions.created_at)'))
                ->get();

            // get balance agent now
            $floatingMoney = DB::connection('popbox_agent')
                ->table('lockers')
                ->where('balance', '!=', 0)
                ->select('balance')
                ->get();

            $total = 0;
            foreach ($floatingMoney as $item) {
                $total += (int)$item->balance;
            }


            $dateTopupToday = $this->getStartAndEndDate('today');
            // get topup amount today
            $topupTodayAmount = DB::connection('popbox_agent')
                ->table('transactions')
                ->where('transactions.type', '=', 'topup')
                ->where('transactions.status', '=', 'paid')
                ->whereBetween('transactions.created_at',[$dateTopupToday->startDate,$dateTopupToday->endDate])
                ->select('transactions.total_price')
                ->get();

            $data = [];
            $data['topProduct'] = $topSalesDb;
            $data['floating_money'] = $total;
            $data['topup_amount'] = $topupTodayAmount->sum('total_price');
            $data['startDate'] = $date->startDate;
            $data['endDate'] = $date->endDate;

            return $data;
        }
    }

    private function getFilterLockerType($type, $time) {

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');

        $date = "";
        if ($time != 'allTime') {
//            dd($time);
            $date = $this->getStartAndEndDate($time);
        }

        if ($type == 'registeredDate') {

            $lockerDb = Locker::when($time != 'allTime', function ($query) use ($date) {
                $query->whereBetween('created_at',[$date->startDate,$date->endDate]);
            })
                ->whereNotIn('type', ['locker'])
                ->get();

            $chartDb = Locker::when($time != 'allTime', function ($query) use ($date) {
                $query->whereBetween('created_at',[$date->startDate,$date->endDate]);
            })
                ->whereNotIn('type', ['locker'])
                ->select(DB::raw('date(lockers.created_at) as date_registered,
                                    SUM(IF(lockers.type = "agent", 1, 0)) as total_agent,
                                    SUM(IF(lockers.type = "warung", 1, 0)) as total_warung,
                                    COUNT(lockers.id) as total_registered'))
                ->groupBy(DB::raw('date(lockers.created_at)'))
                ->get();

            $data = [];
            $data['lockerDb'] = $lockerDb;
            $data['chartDb'] = $chartDb;

            return $data;

        } else if ($type == 'transactionDate') {

//            $transactionsDb = DB::connection('popbox_agent')
//                ->table('transactions')
//                ->leftJoin('popbox_agent.users', 'popbox_agent.users.id', 'popbox_agent.transactions.users_id')
//                ->leftJoin('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'users.locker_id')
//                ->when($time != 'allTime', function ($query) use ($date) {
//                    $query->whereBetween('transactions.created_at',[$date->startDate,$date->endDate]);
////                    $datee = DB::raw('DATE_FORMAT(transactions.created_at, "%Y-%m-%d")');
////                    $query->whereDate('transactions.created_at',[$date->startDate,$date->endDate]);
//                })
//                ->whereIn('transactions.type', ['purchase', 'payment'])
//                ->where('transactions.status', '=', 'paid')
//                ->whereNull('transactions.deleted_at')
//                ->select(DB::raw('SUM(IF(popbox_virtual.lockers.type = "agent", 1, 0)) AS agent_transactions,
//                                    SUM(IF(popbox_virtual.lockers.type = "warung", 1, 0)) AS warung_transactions,
//                                    COUNT(transactions.users_id) AS total_transactions,
//                                    SUM(IF(popbox_virtual.lockers.type = "agent", transactions.total_price, 0)) AS total_price_agent,
//                                    SUM(IF(popbox_virtual.lockers.type = "warung", transactions.total_price, 0)) AS total_price_warung,
//                                        SUM(transactions.total_price) AS total_price,
//                                        date(transactions.created_at) AS created_at'))
//                ->groupBy(DB::raw('date(transactions.created_at)'))
//                ->orderBy(DB::raw('date(transactions.created_at)'))
//                ->get();
//
//            dd($transactionsDb);

            $transactionsDbCount = DB::connection('popbox_agent')
                ->table('transactions')
                ->leftJoin('popbox_agent.users', 'popbox_agent.users.id', 'popbox_agent.transactions.users_id')
                ->leftJoin('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'popbox_agent.users.locker_id')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('transactions.created_at',[$date->startDate,$date->endDate]);
//                    $datee = DB::raw('DATE_FORMAT(transactions.created_at, "%Y-%m-%d")');
//                    $query->whereDate('transactions.created_at',[$date->startDate,$date->endDate]);
                })
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('popbox_virtual.lockers.type,
                                    popbox_virtual.lockers.locker_name'))
                ->groupBy(DB::raw('popbox_virtual.lockers.type, popbox_virtual.lockers.locker_name'))
                ->get();

            $transactionsDbV2Chart = DB::connection('popbox_agent')
                ->table('transactions')
                ->leftJoin('popbox_agent.users', 'popbox_agent.users.id', 'popbox_agent.transactions.users_id')
                ->leftJoin('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'popbox_agent.users.locker_id')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('transactions.created_at',[$date->startDate,$date->endDate]);
//                    $datee = DB::raw('DATE_FORMAT(transactions.created_at, "%Y-%m-%d")');
//                    $query->whereDate('transactions.created_at',[$date->startDate,$date->endDate]);
                })
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('date(transactions.created_at) as created_at,
                                    popbox_virtual.lockers.type,
                                    popbox_virtual.lockers.locker_name'))
                ->groupBy(DB::raw('date(transactions.created_at), popbox_virtual.lockers.type, popbox_virtual.lockers.locker_name'))
                ->get();

            $data = [];
            $data['lockerDb'] = $transactionsDbCount;
            $data['chartDb'] = $transactionsDbV2Chart;

            return $data;
        } else if ($type == 'transactionCount') {

            $transactionsDb = DB::connection('popbox_agent')
                ->table('transactions')
                ->leftJoin('popbox_agent.users', 'popbox_agent.users.id', 'popbox_agent.transactions.users_id')
                ->leftJoin('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'users.locker_id')
                ->when($time != 'allTime', function ($query) use ($date) {
                    $query->whereBetween('transactions.created_at',[$date->startDate,$date->endDate]);
//                    $datee = DB::raw('DATE_FORMAT(transactions.created_at, "%Y-%m-%d")');
//                    $query->whereDate('transactions.created_at',[$date->startDate,$date->endDate]);
                })
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('SUM(IF(popbox_virtual.lockers.type = "agent", 1, 0)) AS agent_transactions,
                                    SUM(IF(popbox_virtual.lockers.type = "warung", 1, 0)) AS warung_transactions,
                                    COUNT(transactions.users_id) AS total_transactions,
                                    SUM(IF(popbox_virtual.lockers.type = "agent", transactions.total_price, 0)) AS total_price_agent,
                                    SUM(IF(popbox_virtual.lockers.type = "warung", transactions.total_price, 0)) AS total_price_warung,
                                        SUM(transactions.total_price) AS total_price,
                                        date(transactions.created_at) AS created_at'))
                ->groupBy(DB::raw('date(transactions.created_at)'))
                ->orderBy(DB::raw('date(transactions.created_at)'))
                ->get();

            $data = [];
            $data['lockerDb'] = $transactionsDb;
            $data['chartDb'] = $transactionsDb;

            return $data;
        }
    }
}
