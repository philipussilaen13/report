<?php

namespace App\Http\Controllers\Agent;

use App\Http\Helpers\ApiPopExpress;
use App\Http\Helpers\Helper;
use App\Jobs\SendFCM;
use App\Models\Agent\AgentLocker;
use App\Models\Agent\BalanceRecord;
use App\Models\Agent\LockerVAOpen;
use App\Models\Agent\NotificationFCM;
use App\Models\Agent\Transaction;
use App\Models\Agent\TransactionItem;
use App\Models\Agent\User;
use App\Models\Virtual\BuildingType;
use App\Models\Virtual\City;
use App\Models\Virtual\ListLockerService;
use App\Models\Virtual\Locker;
use App\Models\Virtual\LockerHistory;
use App\Models\Virtual\Province;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Helpers\HelperApi;

class AgentController extends Controller
{
    /**
     * get agent landing dashboard
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLanding(Request $request)
    {

        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = null;
        $endDate = null;
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0])) . " 00:00:00";
            $endDate = date('Y-m-d', strtotime($tmpDate[1])) . " 23:59:59";
        }

        $status = $request->input('status', null);
        $province = $request->input('province', null);
        $city = $request->input('city', null);
        $type = $request->input('typeee', 'agent,warung');
        $lockerName = $request->input('lockerName', null);
        $lockerId = $request->input('lockerId', null);
        $registeredBy = $request->input('registeredBy',null);

        $lockerList = [];
        if (!empty($status)){
            $agentLocker = AgentLocker::where('status_verification',$status)->get();
            foreach ($agentLocker as $item){
                $lockerList[] = $item->id;
            }
        }

        $lockerDb = Locker::with(['operational_time', 'city','city.province', 'services','registeredBy'])
            ->when($province, function ($query) use ($province) {
                return $query->whereHas('city.province',function ($query) use ($province){
                   $query->where('provinces_id',$province);
                });
            })->when($city, function ($query) use ($city) {
                return $query->where('cities_id', $city);
            })->when($type, function ($query) use ($type) {
//                return $query->where('type', $type);
                return $query->whereIn('type', explode(',', $type));
            })->when($lockerName, function ($query) use ($lockerName) {
                return $query->where('locker_name', 'LIKE', "%$lockerName%");
            })->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('locker_id', 'LIKE', $lockerId);
            })->when($startDate, function ($query) use ($startDate, $endDate) {
                return $query->whereBetween('created_at', [$startDate, $endDate]);
            })->when($registeredBy,function ($query) use($registeredBy){
                if ($registeredBy == 'empty') return $query->whereNull('registered_by');
                else return $query->where('registered_by',$registeredBy);
            })->when($status,function ($query) use ($lockerList){
                $query->whereIn('locker_id',$lockerList);
            })
            ->paginate(30);

        // create parameter to send to view for data processing
        $param = [];
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }

        // get city
        $cities = City::get();

        // parse province
        $provinces = Province::get();

        // get user registered by
        $registeredUser = [];
        $registeredByUserDb = Locker::leftJoin('users','users.id','=','lockers.registered_by')
            ->where('lockers.type','agent')
            ->select(DB::connection('popbox_virtual')->raw('DISTINCT(registered_by),users.id, users.name'))
            ->get();
        foreach ($registeredByUserDb as $item) {
            $tmp = new \stdClass();
            $tmp->id = $item->id;
            $tmp->name = $item->name;
            if (empty($item->id)){
                $tmp->id = 'empty';
                $tmp->name = '(Kosong)';
            }
            $registeredUser[] = $tmp;
        }
        // parsing data to view
        $data = [];
        $data['parameter'] = $param;
        $data['lockerDb'] = $lockerDb->appends($request->input());
        $data['agentLockerDb'] = $lockerDb->appends($request->input());
        $data['provinces'] = $provinces;
        $data['cities'] = $cities;
        $data['registeredBy'] = $registeredUser;

        // chart
        return view('agent.list.index', $data);
    }

    /**
     * Post Export Excel
     * @param Request $request
     */
    public function postExportExcel(Request $request){
        set_time_limit(300);
        ini_set('memory_limit', '20480M');

        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = null;
        $endDate = null;
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0])) . " 00:00:00";
            $endDate = date('Y-m-d', strtotime($tmpDate[1])) . " 23:59:59";
        }

        $status = $request->input('status', null);
        $province = $request->input('province', null);
        $city = $request->input('city', null);
        $type = $request->input('type', 'agent,warung');
        $lockerName = $request->input('lockerName', null);
        $lockerId = $request->input('lockerId', null);
        $registeredBy = $request->input('registeredBy',null);

        $lockerList = [];
        if (!empty($status)){
            $agentLockerDb = AgentLocker::where('status_verification',$status)->get();
            foreach ($agentLockerDb as $item){
                $lockerList[] = $item->id;
            }
        } else {
            $agentLockerDb = AgentLocker::get();
        }

        $lockerDb = Locker::with(['operational_time', 'city','city.province', 'services','registeredBy'])
            ->when($province, function ($query) use ($province) {
                return $query->whereHas('city.province',function ($query) use ($province){
                    $query->where('provinces_id',$province);
                });
            })->when($city, function ($query) use ($city) {
                return $query->where('cities_id', $city);
            })->when($type, function ($query) use ($type) {
                if ($type.contains(',')) {
                    return $query->whereIn('type', explode(',', $type));
                }
                return $query->where('type', $type);
            })->when($lockerName, function ($query) use ($lockerName) {
                return $query->where('locker_name', 'LIKE', "%$lockerName%");
            })->when($lockerId, function ($query) use ($lockerId) {
                return $query->where('locker_id', 'LIKE', $lockerId);
            })->when($startDate, function ($query) use ($startDate, $endDate) {
                return $query->whereBetween('created_at', [$startDate, $endDate]);
            })->when($registeredBy,function ($query) use($registeredBy){
                if ($registeredBy == 'empty') return $query->whereNull('registered_by');
                else return $query->where('registered_by',$registeredBy);
            })->when($status,function ($query) use ($lockerList){
                $query->whereIn('locker_id',$lockerList);
            })->get();

        $listLockerId = [];
        foreach ($lockerDb as $item) {
            if (!in_array($item->locker_id,$listLockerId)) $listLockerId[] = $item->locker_id;
        }

        // get user
        $userDb = User::with('locker')
            ->whereIn('locker_id',$listLockerId)
            ->groupBy('locker_id')
            ->get();

        $nowDate = date('Y-m-d');
        $input = $request->except('_token');
        $filter = "";
        foreach ($input as $key => $item) {
            if (!empty($item)){
                $filter .= "$key-$item ";
            }
        }

        $filename = "Agent List per $nowDate $filter";

        Excel::create("$filename", function ($excel) use ($lockerDb,$userDb,$agentLockerDb) {
            $excel->setTitle('Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Agent');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($lockerDb,$userDb,$agentLockerDb) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Agent Id');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Agent Name');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('User Name');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Phone');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Email');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Last Balance');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Address');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Address Details');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Province');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('City');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('District');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Sub-District');
                });
                $sheet->cell('N1', function ($cell) {
                    $cell->setValue('Registered-By');
                });
                $sheet->cell('O1', function ($cell) {
                    $cell->setValue('Service');
                });
                $sheet->cell('P1', function ($cell) {
                    $cell->setValue('Remarks');
                });
                $sheet->cell('Q1',function ($cell){
                    $cell->setValue('Register Date');
                });
                $sheet->cell('R1',function ($cell){
                    $cell->setValue('Latitude, Longitude');
                });
                $sheet->cell('S1',function ($cell){
                    $cell->setValue('Status');
                });

                $cellNumberStart = 2;
                foreach ($lockerDb as $index => $locker) {
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                        $cell->setValue($index + 1);
                    });
                    $agentId = $locker->locker_id;
                    $agentName = $locker->locker_name;
                    $userData = $userDb->where('locker_id',$agentId)->first();
                    $userName = "";
                    $userPhone = "";
                    $userEmail = "";
                    $lastBalance = 0;
                    if ($userData){
                        $userName = $userData->name;
                        $userPhone = $userData->phone;
                        $userEmail = $userData->email;
                        $lastBalance = $userData->locker->balance;
                    }

                    $status = '';
                    $agentLockerData = $agentLockerDb->where('id',$agentId)->first();
                    if ($agentLockerData){
                        $status = $agentLockerData->status_verification;
                    }

                    $address = $locker->address;
                    $addressDetail = $locker->detail_address;
                    $province = $locker->city->province->province_name;
                    $city = $locker->city->city_name;
                    $district = $locker->district_name;
                    $subDistrict = $locker->sub_district_name;
                    $registeredBy = "";
                    if (!empty($locker->registeredBy)) $registeredBy = $locker->registeredBy->name;
                    $services = "";
                    if (!empty($locker->services)){
                        $listActiveServices = $locker->services;
                        foreach ($listActiveServices as $listActiveService) {
                            if (empty($services)) $services = $listActiveService->name;
                            else $services .= ", ".$listActiveService->name;
                        }
                    }
                    $remarks = $locker->remarks;
                    $registerDate = date('Y-m-d H:i:s',strtotime($locker->created_at));
                    $latitude = $locker->latitude;
                    $longitude = $locker->longitude;

                    $sheet->cell("B$cellNumberStart", function ($cell) use ($agentId) {
                        $cell->setValue($agentId);
                    });
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($agentName) {
                        $cell->setValue($agentName);
                    });
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($userName) {
                        $cell->setValue($userName);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($userPhone) {
                        $cell->setValue($userPhone);
                    });
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($userEmail) {
                        $cell->setValue($userEmail);
                    });
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($lastBalance) {
                        $cell->setValue($lastBalance);
                    });
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($address) {
                        $cell->setValue($address);
                    });
                    $sheet->cell("I$cellNumberStart", function ($cell) use ($addressDetail) {
                        $cell->setValue($addressDetail);
                    });
                    $sheet->cell("J$cellNumberStart", function ($cell) use ($province) {
                        $cell->setValue($province);
                    });
                    $sheet->cell("K$cellNumberStart", function ($cell) use ($city) {
                        $cell->setValue($city);
                    });
                    $sheet->cell("L$cellNumberStart", function ($cell) use ($district) {
                        $cell->setValue($district);
                    });
                    $sheet->cell("M$cellNumberStart", function ($cell) use ($subDistrict) {
                        $cell->setValue($subDistrict);
                    });
                    $sheet->cell("N$cellNumberStart", function ($cell) use ($registeredBy) {
                        $cell->setValue($registeredBy);
                    });
                    $sheet->cell("O$cellNumberStart", function ($cell) use ($services) {
                        $cell->setValue($services);
                    });
                    $sheet->cell("P$cellNumberStart", function ($cell) use ($remarks) {
                        $cell->setValue($remarks);
                    });
                    $sheet->cell("Q$cellNumberStart", function ($cell) use ($registerDate) {
                        $cell->setValue($registerDate);
                    });
                    $sheet->cell("R$cellNumberStart", function ($cell) use ($latitude,$longitude) {
                        $cell->setValue("$latitude,$longitude");
                    });
                    $sheet->cell("S$cellNumberStart", function ($cell) use ($status) {
                        $cell->setValue($status);
                    });
                    $cellNumberStart++;
                }
            });
        })->download('xlsx');
    }

    /**
     * Get Ajax Summary Agent
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxSummary()
    {
        $lockerDb = Locker::where('type','agent')
            ->get();

        $waitingCounter = 0;
        $registeredCounter = 0;

        foreach ($lockerDb as $item) {
            if ($item->status == 0) $waitingCounter++;
            else $registeredCounter++;
        }

        $data['isSuccess'] = true;
        $data['waitingCounter'] = $waitingCounter;
        $data['registeredCounter'] = $registeredCounter;

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getAjaxGraph(Request $request)
    {
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1') . " 00:00:00";
        $endDate = date('Y-m-t') . " 23:59:59";
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0])) . " 00:00:00";
            $endDate = date('Y-m-d', strtotime($tmpDate[1])) . " 23:59:59";
        }

        $lockerDb = Locker::whereBetween('created_at', [$startDate, $endDate])
            ->where('status','<>','0')
            ->where('type','agent')
            ->get();

        /*--------------------create chart--------------------*/
        // get date range
        $periodDate = new \DatePeriod(
            new \DateTime($startDate),
            new \DateInterval('P1D'),
            new \DateTime($endDate)
        );

        // create graphs
        $graphTransaction = collect($lockerDb);
        // order by created date asc
        $sorted = $graphTransaction->sortBy('created_at');
        $graphTransaction = $sorted->values();
        $dayData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('z');
        });
        $weekData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('W');
        });
        $monthData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('n');
        });

        /*========= create graph based on days =========*/
        $dayLabels = [];
        foreach ($periodDate as $date) {
            $dayLabels[] = $date->format('j M y');
        }

        // create data for day graph
        $dayCountRegisterAgent = [];
        foreach ($dayLabels as $index => $date) {
            $count = 0;
            $tmpDayOfYear = date('z', strtotime($date));
            if (isset($dayData[$tmpDayOfYear])) {
                $count = count($dayData[$tmpDayOfYear]);
            }
            $dayCountRegisterAgent[] = $count;
        }

        /*========= create graph based on week =========*/
        $begin = (int)date('W', strtotime("first day of $startDate"));
        $end = (int)date('W', strtotime("last day of $endDate"));
        $weeksThisMonth = range($begin, $end);
        $weekLabels = [];
        // create label based on week
        foreach ($weeksThisMonth as $index => $item) {
            $range = Helper::getStartAndEndDate($item, date('Y'));
            $string = date('d', strtotime($range['week_start'])) . "-" . date('d', strtotime($range['week_end'])) . " " . date('M', strtotime($range['week_end']));
            if (date('M', strtotime($range['week_start'])) != date('M', strtotime($range['week_end']))) {
                $string = date('d', strtotime($range['week_start'])) . date('M', strtotime($range['week_start'])) . "-" . date('d', strtotime($range['week_end'])) . " " . date('M', strtotime($range['week_end']));
            }
            $weekLabels[] = $string;
        }
        // create data for month graph
        $weekCountRegisterAgent = [];
        foreach ($weeksThisMonth as $index => $item) {
            $count = 0;
            if (isset($weekData[$item])) {
                $count = count($weekData[$item]);
            }
            $weekCountRegisterAgent[] = $count;
        }

        /*========= create graph based on month =========*/
        // create label based on month
        $monthLabels = [];
        foreach ($monthData as $index => $item) {
            $tmpDate = Helper::getDateFromMonth($index);
            $monthLabels[] = $tmpDate->format('M');
        }
        // create data for month graph
        $monthCountTransaction = [];
        foreach ($monthLabels as $index => $date) {
            $count = 0;
            $tmpMonthOfYear = date('n', strtotime($date));
            if (isset($monthData[$tmpMonthOfYear])) {
                $count = count($monthData[$tmpMonthOfYear]);
            }
            $monthCountTransaction[] = $count;
        }

        $data['isSuccess'] = true;
        $data['dayLabels'] = $dayLabels;
        $data['dayData'] = $dayCountRegisterAgent;
        $data['weekLabels'] = $weekLabels;
        $data['weekData'] = $weekCountRegisterAgent;
        $data['monthLabels'] = $monthLabels;
        $data['monthData'] = $monthCountTransaction;

        return response()->json($data);
    }

    /**
     * @param $lockerId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getDashboardAgentDetail($lockerId, Request $request)
    {
        $lockerDb = Locker::where('locker_id', $lockerId)
            ->first();
        if (!$lockerDb) {
            $request->session()->flash('error', 'Locker ID not found');
            return back();
        }
        // get agent balance
        $lockerBalance = AgentLocker::find($lockerId);
        $balance = $lockerBalance->balance;
        $express = null;
        if (!empty($lockerBalance->express_branch_id)){
            $express = new \stdClass();
            $express->branch_id = $lockerBalance->express_branch_id;
            $express->agent_code = $lockerBalance->express_agent_code;
        }

        // get user Data
        $agentId = [];
        $agentDb = User::where('locker_id', $lockerId)->get();
        foreach ($agentDb as $agent) {
            $agentId[] = $agent->id;
        }

        // get agent transaction
        $startDate = date('Y-m-1') . " 00:00:00";
        $endDate = date('Y-m-t') . " 23:59:59";

        $thisMonthTransactionDb = Transaction::leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->whereIn('users_id', $agentId)
            ->orderBy('transactions.created_at', 'desc')
            ->select('transaction_items.*','transactions.type as trans_type','transactions.total_price')
            ->get();

        $transactionList = [];
        $transactionCount = 0;
        $transactionAmount = 0;
        $topupAmount = 0;
        $rewardAmount = 0;
        $pulsaCount = 0;
        $paymentCount = 0;
        $popshopCount = 0;
        $deliveryCount = 0;

        $typeTransaction = ['popshop', 'pulsa', 'electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid', 'delivery'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward', 'commission','referral'];

        $typePulsa = ['pulsa'];
        $typePopShop = ['popshop'];
        $typePayment = ['electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid'];
        $typeRefund = ['refund', 'deduct'];
        $typeDelivery = ['delivery'];

        foreach ($thisMonthTransactionDb as $item) {
            // for transaction summary
            if (in_array($item->type, $typeTransaction)) {
                $transactionCount++;
                $transactionAmount += $item->price;
                $transactionList[] = $item;
            } elseif (in_array($item->type, $typeTopUp)) {
                $topupAmount += $item->price;
            } elseif (in_array($item->trans_type, $typeReward)) {
                $rewardAmount += $item->total_price;
            }

            // for product summary
            if (in_array($item->type, $typePulsa)) {
                $pulsaCount++;
            } elseif (in_array($item->type, $typePopShop)) {
                $popshopCount++;
            } elseif (in_array($item->type, $typePayment)) {
                $paymentCount++;
            } elseif (in_array($item->type, $typeDelivery)) {
                $deliveryCount++;
            }
        }

        // create graphs
        // get date range
        $periodDate = new \DatePeriod(
            new \DateTime($startDate),
            new \DateInterval('P1D'),
            new \DateTime($endDate)
        );

        $graphTransaction = collect($transactionList);
        // order by created date asc
        $sorted = $graphTransaction->sortBy('created_at');
        $graphTransaction = $sorted->values();
        $dayData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('z');
        });
        $weekData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('W');
        });
        $monthData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('n');
        });

        /*========= create graph based on days =========*/
        $dayLabels = [];
        foreach ($periodDate as $date) {
            $dayLabels[] = $date->format('j M y');
        }

        // create data for day graph
        $dayCountTransaction = [];
        $daySumTransaction = [];
        foreach ($dayLabels as $index => $date) {
            $count = 0;
            $sum = 0;
            $tmpDayOfYear = date('z', strtotime($date));
            if (isset($dayData[$tmpDayOfYear])) {
                $count = count($dayData[$tmpDayOfYear]);
                $sum = $dayData[$tmpDayOfYear]->sum('total_price');
            }
            $dayCountTransaction[] = $count;
            $daySumTransaction[] = $sum;
        }

        /*========= create graph based on week =========*/
        $begin = (int)date('W', strtotime("first day of $startDate"));
        $end = (int)date('W', strtotime("last day of $endDate"));
        $weeksThisMonth = range($begin, $end);
        $weekLabels = [];
        // create label based on week
        foreach ($weeksThisMonth as $index => $item) {
            $range = Helper::getStartAndEndDate($item, date('Y'));
            $string = date('d', strtotime($range['week_start'])) . "-" . date('d', strtotime($range['week_end'])) . " " . date('M', strtotime($range['week_end']));
            if (date('M', strtotime($range['week_start'])) != date('M', strtotime($range['week_end']))) {
                $string = date('d', strtotime($range['week_start'])) . date('M', strtotime($range['week_start'])) . "-" . date('d', strtotime($range['week_end'])) . " " . date('M', strtotime($range['week_end']));
            }
            $weekLabels[] = $string;
        }
        // create data for month graph
        $weekCountTransaction = [];
        $weekSumTransaction = [];
        foreach ($weeksThisMonth as $index => $item) {
            $count = 0;
            $sum = 0;
            if (isset($weekData[$item])) {
                $count = count($weekData[$item]);
                $sum = $weekData[$item]->sum('total_price');
            }
            $weekCountTransaction[] = $count;
            $weekSumTransaction[] = $sum;
        }

        /*========= create graph based on month =========*/
        $monthLabels = [];
        foreach ($monthData as $index => $item) {
            $tmpDate = Helper::getDateFromMonth($index);
            $monthLabels[] = $tmpDate->format('M');
        }

        // create data for month graph
        $monthCountTransaction = [];
        $monthSumTransaction = [];
        foreach ($monthLabels as $index => $date) {
            $count = 0;
            $sum = 0;
            $tmpMonthOfYear = date('n', strtotime($date));
            if (isset($monthData[$tmpMonthOfYear])) {
                $count = count($monthData[$tmpMonthOfYear]);
                $sum = $monthData[$tmpMonthOfYear]->sum('total_price');
            }
            $monthCountTransaction[] = $count;
            $monthSumTransaction[] = $sum;
        }

        // get province data
        $provinceDb = Province::get();
        // get city data
        $cityDb = City::get();
        // get service
        $serviceDb = ListLockerService::get();
        // get building type data
        $listBuildingTypes = BuildingType::get();
        // get user who could registered
        $userRegisteredBy = \App\User::whereHas('group', function ($query) {
            $query->whereIn('name', ['sales', 'bd']);
        })->get();

        // parsing data to view
        $data = [];
        $data['dayLabels'] = $dayLabels;
        $data['dayCountTransaction'] = $dayCountTransaction;
        $data['daySumTransaction'] = $daySumTransaction;
        $data['weekLabels'] = $weekLabels;
        $data['weekCountTransaction'] = $weekCountTransaction;
        $data['weekSumTransaction'] = $weekSumTransaction;
        $data['monthLabels'] = $monthLabels;
        $data['monthCountTransaction'] = $monthCountTransaction;
        $data['monthSumTransaction'] = $monthSumTransaction;
        $data['transactionCount'] = $transactionCount;
        $data['transactionAmount'] = $transactionAmount;
        $data['topupAmount'] = $topupAmount;
        $data['rewardAmount'] = $rewardAmount;

        $data['pulsaCount'] = $pulsaCount;
        $data['paymentCount'] = $paymentCount;
        $data['popshopCount'] = $popshopCount;
        $data['deliveryCount'] = $deliveryCount;
        $data['lockerData'] = $lockerDb;
        $data['agentData'] = $agentDb;
        $data['balance'] = $balance;
        $data['provinces'] = $provinceDb;
        $data['cities'] = $cityDb;
        $data['services'] = $serviceDb;
        $data['express'] = $express;
        $data['listBuildingTypes'] = $listBuildingTypes;
        $data['userRegisteredBy'] = $userRegisteredBy;
        $data['lockerBalance'] = $lockerBalance;

        return view('agent.list.detail', $data);
    }

    /**
     * activate agent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postActivateAgent(Request $request)
    {
        // validate input
        $rule = [
            'lockerId' => 'required'
        ];
        $this->validate($request, $rule);

        $lockerId = $request->input('lockerId');
        $topUpAmount = $request->input('topupAmount', 0);
        $input = $request->input();
        $registeredBy = null;
        if (!empty($input['registered_by'])){
            $userReportDb = \App\User::find($input['registered_by']);
            if (!$userReportDb){
                $request->session()->flash('error','Invalid User Registered By');
                return back();
            }
            $userVirtual = \App\Models\Virtual\User::where('email',$userReportDb->email)->first();
            if (!$userVirtual){
                $request->session()->flash('error','Invalid User Registered By on Virtual Locker');
                return back();
            }
            $registeredBy = $userVirtual->id;
            $input['registered_by'] = $userVirtual->id;
        }

        // find on agent lockers
        $agentLockerDb = AgentLocker::where('id', $lockerId)->first();
        if (!$agentLockerDb) {
            $agentLockerDb = new AgentLocker();
            $agentLockerDb->id = $lockerId;
            $agentLockerDb->balance = 0;
            $agentLockerDb->save();
        }
        DB::beginTransaction();
        DB::connection('popbox_agent')->beginTransaction();

        // activate agent and locker
        $lockerDB = Locker::where('locker_id', $lockerId)->first();
        $lockerDB = Locker::find($lockerDB->id);
        $lockerDB->status = 2;
        $lockerDB->activated_by = Auth::user()->name;
        $lockerDB->remarks = $request->input('remarks', null);
        $lockerDB->registered_by = $registeredBy;
        $lockerDB->save();

        $agentUserDb = User::where('locker_id', $lockerId)->get();
        foreach ($agentUserDb as $item) {
            $tmp = User::find($item->id);
            $tmp->status = "2";
            $tmp->save();
        }

        // enable agent locker
        $agentLockerDb = AgentLocker::find($lockerId);
        $agentLockerDb->status_verification = 'enable';
        if (empty($agentLockerDb->pin_verification)){
            $pin = Helper::generateRandomString(6);
            $agentLockerDb->pin_verification = $pin;
        }
        $agentLockerDb->save();

        $input['locker_id'] = $lockerId;
        // update on locker data
        $lockerDB = Locker::updateLocker($input);
        if (!$lockerDB->isSuccess) {
            DB::rollback();
            DB::connection('popbox_agent')->rollback();
            $request->session()->flash('error', 'Gagal Aktivasi');
            return back();
        }

        $remarks = $request->input('remarks');
        $status = 'UPDATE';
        $user = Auth::user()->name;
        $lockerDb = Locker::where('locker_id', $lockerId)->first();
        $lockerId = $lockerDb->id;
        $history = LockerHistory::createNewHistory($lockerId, $user, $status, $remarks);

        DB::connection('popbox_agent')->commit();
        DB::commit();

        $commandError = '';
        // create va open
        $lockerVAOpen = LockerVAOpen::createVAOpen($lockerDb->locker_id);
        if (!empty($lockerVAOpen->successCreate)) $commandError .= implode(',',$lockerVAOpen->successCreate);
        if (!empty($lockerVAOpen->failedCreate))$commandError .= implode(',',$lockerVAOpen->failedCreate);

        $request->session()->flash('success','Sukses Aktivasi '.$commandError);
        return back();
    }

    /**
     * Post Update Agent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdateAgent(Request $request)
    {
        // validate input
        $rule = [
            'lockerId' => 'required'
        ];
        $this->validate($request, $rule);

        $lockerId = $request->input('lockerId');

        DB::beginTransaction();
        $input = $request->input();
        $input['locker_id'] = $lockerId;

        if (!empty($input['registered_by'])){
            $userReportDb = \App\User::find($input['registered_by']);
            if (!$userReportDb){
                $request->session()->flash('error','Invalid User Registered By');
                return back();
            }
            $userVirtual = \App\Models\Virtual\User::where('email',$userReportDb->email)->first();
            if (!$userVirtual){
                $request->session()->flash('error','Invalid User Registered By on Virtual Locker');
                return back();
            }
            $input['registered_by'] = $userVirtual->id;
        }

        // update on locker data
        $lockerDB = Locker::updateLocker($input);
        if (!$lockerDB->isSuccess) {
            DB::rollback();
            $request->session()->flash('error', 'Gagal Update');
            return back();
        }
        $remarks = $request->input('remarks');
        $status = 'UPDATE';
        $user = Auth::user()->name;
        $lockerDb = Locker::where('locker_id', $lockerId)->first();
        $lockerId = $lockerDb->id;
        $history = LockerHistory::createNewHistory($lockerId, $user, $status, $remarks);
        DB::commit();

        $request->session()->flash('success', 'Sukses Update');
        return back();
    }

    /**
     * Post Activate Agent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postActivateExpress(Request $request){
        $rule = [
            'lockerId' => 'required'
        ];
        $this->validate($request, $rule);

        $lockerId = $request->input('lockerId');

        // get locker
        $lockerDb = Locker::where('locker_id',$lockerId)->first();
        if (!$lockerDb){
            $request->session()->flash('error','Locker Not Found');
            return back();
        }

        // get agent Locker
        $agentLockerDb = AgentLocker::find($lockerId);
        $agentUserDb = User::where('locker_id',$lockerId)->first();

        $agentName = $agentLockerDb->locker_name;
        $agentUser = $agentUserDb->name;
        $agentPhone = $agentUserDb->phone;
        $agentEmail = $agentUserDb->email;
        $lockerCity = $lockerDb->city->city_name;
        $lockerProvince = $lockerDb->city->province->province_name;
        $lockerAddress = $lockerDb->address;
        $lockerLongitude = $lockerDb->longitude;
        $lockerLatitude =  $lockerDb->latitude;

        $apiPopExpress = new ApiPopExpress();
        $getCity = $apiPopExpress->getCityOrigin();
        if (empty($getCity)){
            $message = 'Failed Get City';
            $request->session()->flash('error',$message);
            return back();
        }
        if ($getCity->response->code !=200){
            $message = $getCity->response->message;
            $request->session()->flash('error',$message);
            return back();
        }
        $cityOrigin = $getCity->data->origins;
        $origin = null;
        foreach ($cityOrigin as $item) {
            $tmpName = strtoupper($item->name);
            if (strpos($lockerCity,$tmpName) !== false) {
                $origin = $item->name;
            }
        }
        if (empty($origin)){
            $request->session()->flash('error','Out of Coverage');
            return back();
        }

        // submit to PopExpress
        $submit = $apiPopExpress->registerAgent($agentName,$lockerProvince,$lockerCity,$origin,$agentPhone,$lockerAddress,$agentUser,$agentEmail,$lockerLatitude,$lockerLongitude);
        if (empty($submit)){
            $request->session()->flash('error','Failed Submit PopExpress');
            return back();
        }
        if ($submit->response->code != 200){
            $message = $submit->response->message;
            $request->session()->flash('error',$message);
            return back();
        }
        $data = $submit->data;
        $branchId = $data->branch_id;
        $branchCode = $data->agent_code;

        // update agent locker
        $agentLockerDb = AgentLocker::find($agentLockerDb->id);
        $agentLockerDb->express_branch_id = $branchId;
        $agentLockerDb->express_agent_code = $branchCode;
        $agentLockerDb->save();

        $request->session()->flash('success','Success Activate PopExpress');
        return back();
    }

    /**
     * Change Status Agent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangeStatusAgent(Request $request){
        $status = $request->input('status');
        $lockerId = $request->input('lockerId');
        $successListAgent = [];
        $failedListAgent = [];

        $idPicture = $request->file('idPicture');
        $idStatus = $request->input('idStatus');
        
        $idSelfiePicture = $request->file('idSelfiePicture');
        $idSelfieStatus = $request->input('selfieStatus');
        
        $storePhoto = $request->file('storePhoto');
        $storeStatus = $request->input('storeStatus');
        
        $npwpPicture = $request->file('npwpPhoto');
        $npwpStatus = $request->input('npwpStatus');
        
        $registeredBy = $request->input('registered_by');
        $input = $request->input();


        // find locker
        $lockerDb = AgentLocker::with('users')
            ->whereIn('id',$lockerId)
            ->get();

        // set status
        if ($status == 'DISABLE'){
            $statusNumber = 3;
            $verificationStatus = 'disable';
        } elseif ($status == 'ENABLE'){
            $statusNumber = 2;
            $verificationStatus = 'enable';
        } elseif ($status == 'VERIFIED'){
            $statusNumber = 2;
            $verificationStatus = 'verified';
        } else {
            $request->session()->flash('error','Unknown Status');
            return back();
        }
        $fcmNotification = new NotificationFCM();
        foreach ($lockerDb as $item) {
            DB::connection('popbox_agent')->beginTransaction();
            DB::connection('popbox_virtual')->beginTransaction();
            // update user
            $userData = User::where('locker_id',$item->id)->get();
            foreach ($userData as $user){
                $userDb = User::find($user->id);
                $userDb->status = $statusNumber;
                $userDb->save();

                // release pending transaction
                if ($verificationStatus == 'verified'){
                    $transaction = Transaction::releasePendingTransaction($user->id,$item->id);
                    if (!$transaction->isSuccess){
                        $failedListAgent[] = "Released pending transaction of ".$item->locker_name."-".$userData->name;
                        DB::connection('popbox_agent')->rollback();
                        continue;
                    }
                }

            }

            // update locker
            $agentLockerDb = AgentLocker::find($item->id);
            if ($agentLockerDb){
                $agentLockerDb->status_verification = $verificationStatus;
                $agentLockerDb->save();
            }

            // save picture
            $ftpHost = env('SERVER_AGENT_IP');
            $ftpUser = env('SERVER_AGENT_USER');
            $ftpPass = env('SERVER_AGENT_PASS');
            
            $api = new HelperApi();
            
            if (!empty($idPicture[$item->id])){
                $userData = User::where('locker_id',$item->id)->first();
                if ($userData){
                    $userData = User::find($userData->id);
                    $fileName = $userData->username.'-'.date('Ymd').'.jpg';
                    $idFile = $idPicture[$item->id];
//                     $remotePath = env('AGENT_PATH')."agent/id/$fileName";

//                     $connection = ssh2_connect($ftpHost,22);
//                     if ($connection === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_auth_password($connection,$ftpUser,$ftpPass);
//                     if ($state === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_scp_send($connection,$idFile,$remotePath,0777);
//                     if ($state === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }
                    $subFolder = "idPict";
                    $result = $api->postStoreFile($fileName, base64_encode(file_get_contents($idFile)), $subFolder);
                    if($result->response->code > 200){
                        $failedListAgent[] = "Save ID pict of ".$item->locker_name."-".$userData->name;
                        DB::connection('popbox_agent')->rollback();
                        continue;
                    } 
                    $userData->id_picture = $fileName;
                    $userData->save();
                }
            }

            if (!empty($idSelfiePicture[$item->id])){
                $userData = User::where('locker_id',$item->id)->first();
                if ($userData){
                    $userData = User::find($userData->id);
                    $fileName = $userData->username.'-'.date('Ymd').'.jpg';
                    $idFile = $idSelfiePicture[$item->id];
//                     $remotePath = env('AGENT_PATH')."agent/id_selfie/$fileName";

//                     $connection = ssh2_connect($ftpHost,22);
//                     if ($connection === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_auth_password($connection,$ftpUser,$ftpPass);
//                     if ($state === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_scp_send($connection,$idFile,$remotePath,0777);
//                     if ($state === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }

                    
                    $subFolder = "selfiePict";
                    $result = $api->postStoreFile($fileName, base64_encode(file_get_contents($idFile)), $subFolder);
                    if($result->response->code > 200){
                        $failedListAgent[] = "Save Selfie pict of ".$item->locker_name."-".$userData->name;
                        DB::connection('popbox_agent')->rollback();
                        continue;
                    } 
                    $userData->id_selfie = $fileName;
                    $userData->save();
                }
            }

            if (!empty($npwpPicture[$item->id])){
                $userData = User::where('locker_id',$item->id)->first();
                if ($userData){
                    $userData = User::find($userData->id);
                    $fileName = $userData->username.'-'.date('Ymd').'.jpg';
                    $idFile = $npwpPicture[$item->id];
//                     $remotePath = env('AGENT_PATH')."agent/npwp/$fileName";

//                     $connection = ssh2_connect($ftpHost,22);
//                     if ($connection === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_auth_password($connection,$ftpUser,$ftpPass);
//                     if ($state === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_scp_send($connection,$idFile,$remotePath,0777);
//                     if ($state === false){
//                         $failedListAgent[] = $item->locker_name."-".$userData->name;
//                         DB::connection('popbox_agent')->rollback();
//                         continue;
//                     }
                    
                    
                    $subFolder = "npwpPict";
                    $result = $api->postStoreFile($fileName, base64_encode(file_get_contents($idFile)), $subFolder);
                    if($result->response->code > 200){
                        $failedListAgent[] = "Save NPWP pict of ".$item->locker_name."-".$userData->name;
                        DB::connection('popbox_agent')->rollback();
                        continue;
                    } 
                    $userData->npwp_picture = $fileName;
                    $userData->save();
                }
            }

            if (!empty($storePhoto[$item->id])){
                $lockerData = Locker::where('locker_id',$item->id)->first();
                if ($lockerData){
                    $lockerData = Locker::find($lockerData->id);
                    $fileName = $lockerData->locker_id.'.jpg';
                    $idFile = $storePhoto[$item->id];
//                     $remotePath = env('VLOCKER_PATH')."img/locker/$fileName";

//                     $connection = ssh2_connect($ftpHost,22);
//                     if ($connection === false){
//                         $failedListAgent[] = "Save Store pict of ".$item->locker_name;
//                         DB::connection('popbox_agent')->rollback();
//                         DB::connection('popbox_virtual')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_auth_password($connection,$ftpUser,$ftpPass);
//                     if ($state === false){
//                         $failedListAgent[] = "Save Store pict of ".$item->locker_name;
//                         DB::connection('popbox_agent')->rollback();
//                         DB::connection('popbox_virtual')->rollback();
//                         continue;
//                     }

//                     $state = ssh2_scp_send($connection,$idFile,$remotePath,0677);
//                     if ($state === false){
//                         $failedListAgent[] = "Save Store pict of ".$item->locker_name;
//                         DB::connection('popbox_agent')->rollback();
//                         DB::connection('popbox_virtual')->rollback();
//                         continue;
//                     }
                    $subFolder = "storePict";
                    $result = $api->postStoreFile($fileName, base64_encode(file_get_contents($idFile)), $subFolder);
                    if($result->response->code > 200){
                        $failedListAgent[] = "Save Store pict of ".$item->locker_name."-".$userData->name;
                        DB::connection('popbox_agent')->rollback();
                        continue;
                    } 
                    $lockerData->picture = $fileName;
                    $lockerData->save();
                }
            }

            $lockerData = AgentLocker::find($item->id);
            $currentStatus = $lockerData->status_verification;
            if (!empty($idStatus[$item->id])){
                $lockerData->id_verification = $idStatus[$item->id];
            }
            if (!empty($idSelfieStatus[$item->id])){
                $lockerData->selfie_verification = $idSelfieStatus[$item->id];
            }

            if (!empty($storeStatus[$item->id])){
                $lockerData->store_verification = $storeStatus[$item->id];
            }
            if (!empty($npwpStatus[$item->id])){
                $lockerData->npwp_verification = $npwpStatus[$item->id];
            }

            $lockerData->save();

            $userData = User::where('locker_id',$item->id)->first();
            $successListAgent[] = $item->locker_name."-".$userData->name;

            $bodyNotification = null;
            $imageUrl = null;
            $description = null;
            $agentName = $agentLockerDb->locker_name;
            if ($status == 'DISABLE'){
                $bodyNotification = "Maaf!<br /> Akun $agentName <b>dalam pengawasan</b>. <br /> Silakan hubungi CS untuk keterangan lebih lanjut. <br /><br /> Terima kasih";
                $imageUrl = url("img/agent/acc_pending.png");
                $description = "Akun Dalam Pengawasan";
            } elseif ($status == 'ENABLE'){
                $bodyNotification = "Maaf!<br /> Akun $agentName <b>belum lolos verifikasi</b>. <br /> Silakan cek akun saya untuk informasi lebih lengkap. Silakan lengkapi data kembali. <br /><br /> Terima kasih";
                $imageUrl = url("img/agent/acc_rejected.png");
                $description = "Akun Gagal Verifikasi";
            } elseif ($status == 'VERIFIED'){
                $bodyNotification = "Selamat!<br />Akun $agentName sudah <b>TERVERIFIKASI</b>";
                $imageUrl = url("img/agent/acc_verified.png");
                $description = "Akun Terverifikasi";
            }

            $fcm = $userData->gcm_token;
            $lockerId = $userData->locker_id;
            $title = "Status Verifikasi";
            // create parameter push
            $data = [];
            $data['id'] = (int)"9".rand(10,99);
            $data['type'] = 'transaction';
            $data['timestamp'] = date('Y-m-d H:i:s');
            $data['description'] = $bodyNotification;
            $data['reference'] = "";
            $data['locker_id'] = $lockerId;
            $data['img_url'] = $imageUrl;

            $insert = $fcmNotification->insertNew('dashboard',$fcm,'android',$title,$description,$data);

            if (!empty($input['registered_by'])){
                $userReportDb = \App\User::find($input['registered_by']);
                if (!$userReportDb){
                    $failedListAgent[] = $item->locker_name." - Invalid User Registered By";
                    DB::connection('popbox_agent')->rollback();
                    DB::connection('popbox_virtual')->rollback();
                    $request->session()->flash('error','Invalid User Registered By');
                    continue;
                }
                $userVirtual = \App\Models\Virtual\User::where('email',$userReportDb->email)->first();
                if (!$userVirtual){
                    $failedListAgent[] = $item->locker_name." - Invalid User Registered By on Virtual Locker";
                    DB::connection('popbox_agent')->rollback();
                    DB::connection('popbox_virtual')->rollback();
                    $request->session()->flash('error','Invalid User Registered By on Virtual Locker');
                    continue;
                }
                $registeredBy = $userVirtual->id;

                // change locker registered_by
                $lockerData = Locker::where('locker_id',$item->id)->first();
                $lockerData = Locker::find($lockerData->id);
                $lockerData->registered_by = $registeredBy;
                $lockerData->save();
            }


            DB::connection('popbox_agent')->commit();
            DB::connection('popbox_virtual')->commit();
        }
        if (!empty($successListAgent)){
            $successListAgent = implode(",",$successListAgent);
            $message = "Success change: $status. Agent List: $successListAgent";
            $request->session()->flash('success',$message);
        }
        if (!empty($failedListAgent)){
            $failedListAgent = implode(",",$failedListAgent);
            $message = "Failed change: $status. Agent List: $failedListAgent";
            $request->session()->flash('error',$message);
        }

        // dispatch job
        SendFCM::dispatch();

        return back();
    }


    /*--------------------------------------------------------USER LIST V2------------------------------------------------------------*/

    public function getLandingV2() {
        return view('agent.list.index2');
    }

    public function getAjaxAllType(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $lockerDb = self::userList($request);

        $data = [];
        if (!empty($lockerDb)) {
            // formatting result of query to desired output
            $resultList = [];
            foreach ($lockerDb as $item) {
                $resultList[] = $this->formatResult($item);
            }

            $data['param'] = $request->input();
            $data['resultList'] = $resultList;

            $response->isSuccess = true;
        }

        $response->data = $data;
        return response()->json($response);
    }

    public function getAjaxAllTypeV2(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $transactionStartDate = null;
        $transactionEndDate = null;

        $registeredStartDate = null;
        $registeredEndDate = null;

//        dd($request->input());

        $firstOpeningPage = $request->input('firstOpeningPage', null);
        $type = $request->input('type', 'empty');
        $status = $request->input('status', null);
        $sales = $request->input('sales', 'empty');
        $province = $request->input('province', null);
        $city = $request->input('city', null);
        $name = $request->input('name', null);
        $agentid = $request->input('agentid', null);
        $dateRangeTransaction = $request->input('daterange_transaction', null);
        $dateRangeRegistered = $request->input('daterange_registered', null);

        $draw = $request->input('draw', 0);
        $start = $request->input('start', null);
        $limit = $request->input('limit', null);

        if (!empty($dateRangeTransaction)) {
            $transactionStartDate = Helper::formatDateRange($dateRangeTransaction, ',')->startDate;
            $transactionEndDate = Helper::formatDateRange($dateRangeTransaction, ',')->endDate;
        }

        if (!empty($dateRangeRegistered)) {
            $registeredStartDate = Helper::formatDateRange($dateRangeRegistered, ',')->startDate;
            $registeredEndDate = Helper::formatDateRange($dateRangeRegistered, ',')->endDate;
        }

        $lockerList = [];
        if (!empty($status)){
            $agentLockerStatus = AgentLocker::where('status_verification',$status)->get();
            foreach ($agentLockerStatus as $item){
                $lockerList[] = $item->id;
            }
        }

        $transactionList = [];
        if (!empty($dateRangeTransaction)) {
            $agentLockerTransaction = DB::connection('popbox_agent')
                ->table('transactions')
                ->leftJoin('users', 'users.id', 'transactions.users_id')
                ->leftJoin('lockers', 'lockers.id', 'users.locker_id')
                ->whereBetween('transactions.created_at',[$transactionStartDate,$transactionEndDate])
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('lockers.id, lockers.locker_name,
                                    GROUP_CONCAT(DISTINCT date(transactions.created_at) SEPARATOR ",") as transaction_date,
                                    (sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0))) AS total_price,
                                    (sum( IF(transactions.cart_id IS NULL, 1, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0))) AS total_jumlah'

                ))
                ->groupBy('transactions.users_id', 'lockers.locker_name')
                ->orderBy('locker_name','asc')
                ->get();
            foreach ($agentLockerTransaction as $item) {
                $transactionList[] = $item->id;
            }
        }

        $querylockerDb = Locker::with(['user', 'operational_time', 'city','city.province', 'services','registeredBy'])
            ->when($province, function ($query) use ($province) {
                return $query->whereHas('city.province',function ($query) use ($province){
                    $query->where('provinces_id',$province);
                });
            })->when($city, function ($query) use ($city) {
                return $query->where('cities_id', $city);
            })->when($type, function ($query) use ($type) {
                if ($type == 'empty') {
                    return $query->where('type', '!=', 'locker')->orWhere('type', '=', null);
                }
                return $query->where('type', $type);
//                    return $query->whereIn('type', explode(',', $type));
            })->when($name, function ($query) use ($name) {
                return $query->where('locker_name', 'LIKE', "%$name%");
            })->when($agentid, function ($query) use ($agentid) {
                return $query->where('locker_id', 'LIKE', $agentid);
            })->when($dateRangeRegistered, function ($query) use ($registeredStartDate, $registeredEndDate) {
                return $query->whereBetween('popbox_virtual.lockers.created_at', [$registeredStartDate, $registeredEndDate]);
            })->when($sales,function ($query) use($sales){
                if ($sales == 'empty') {
                    return $query->whereNull('registered_by');
                } else {
                    return $query->where('registered_by', $sales);
                }
            })
            ->when($status,function ($query) use ($lockerList){
                $query->whereIn('popbox_virtual.lockers.locker_id',$lockerList);
            })->when($dateRangeTransaction,function ($query) use ($transactionList){
                $query->whereIn('popbox_virtual.lockers.locker_id',$transactionList);
            })
            ->orderBy('locker_name', 'asc');

//        dd($querylockerDb->select('lockers.locker_id as id')->first());

        $dbTotal = clone $querylockerDb;
        $total = $dbTotal->select(DB::raw("count(lockers.id) as total"))->first();

        $lockerDbCount = $total->total;
        $lockerDb = $querylockerDb->offset($start)->limit($limit)->get();
//        $lockerDb = $querylockerDb->offset($start)->limit($limit)->select('lockers.locker_id as id', 'lockers.locker_name as name',
//                                        'lockers.type', 'agentLocker.balance', 'city.city_name', 'agentLocker.status_verification',
//                                            'registeredBy.name', 'lockers.created_at')->get();



        $data = [];
        if (!empty($lockerDb)) {
            // formatting result of query to desired output
            $resultList = [];
            foreach ($lockerDb as $item) {
                $resultList[] = $this->formatResult($item);
            }

            $data['param'] = $request->input();
            $data['draw'] = $draw;
            $data['lockerDbCount'] = $lockerDbCount;
            $data['resultList'] = $resultList;

            $response->isSuccess = true;
        }

        $response->data = $data;
        return response()->json($response);
    }

    public function getAjaxDataStatusVerification() {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $statusVerDb = AgentLocker::whereNotNull('status_verification');

        $totalData = sizeof($statusVerDb->get());

        $totalDisable = sizeof(AgentLocker::whereNotNull('status_verification')->where('status_verification', '=', 'disable')->get());
        $totalDisablePercentage = round(($totalDisable / $totalData) * 100);

        $totalEnable = sizeof(AgentLocker::whereNotNull('status_verification')->where('status_verification', '=', 'enable')->get());
        $totalEnablePercentage = round(($totalEnable / $totalData) * 100);

        $totalPending = sizeof(AgentLocker::whereNotNull('status_verification')->where('status_verification', '=', 'pending')->get());
        $totalPendingPercentage = round(($totalPending / $totalData) * 100);

        $totalVerified = sizeof(AgentLocker::whereNotNull('status_verification')->where('status_verification', '=', 'verified')->get());
        $totalVerifiedPercentage = round(($totalVerified / $totalData) * 100);


        $data = [];
        $data['total_data'] = $totalData;
        $data['total_disable'] = $totalDisable;
        $data['total_disable_percentage'] = $totalDisablePercentage;
        $data['total_enable'] = $totalEnable;
        $data['total_enable_percentage'] = $totalEnablePercentage;
        $data['total_pending'] = $totalPending;
        $data['total_pending_percentage'] = $totalPendingPercentage;
        $data['total_verified'] = $totalVerified;
        $data['total_verified_percentage'] = $totalVerifiedPercentage;


        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getAjaxFilterDropdownDataUser() {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $typeDb = Locker::where('lockers.type', '!=', 'locker')
            ->groupBy('lockers.type')
            ->select('lockers.type')
            ->get();

        $statusDb = AgentLocker::whereNotNull('lockers.status_verification')
            ->groupBy('lockers.status_verification')
            ->select('lockers.status_verification')
            ->get();

        // get user registered by
        $registeredUser = [];
        $registeredByUserDb = Locker::leftJoin('users','users.id','=','lockers.registered_by')
            ->where('lockers.type','agent')
            ->select(DB::connection('popbox_virtual')->raw('DISTINCT(registered_by),users.id, users.name'))
            ->get();
        foreach ($registeredByUserDb as $item) {
            $tmp = new \stdClass();
            $tmp->id = $item->id;
            $tmp->name = $item->name;
            if (empty($item->id)){
                $tmp->id = 'empty';
                $tmp->name = '(Kosong)';
            }
            $registeredUser[] = $tmp;
        }

        // get city
        $cities = City::select('cities.id', 'cities.city_name', 'cities.provinces_id')
            ->get();

        // parse province
        $provinces = Province::select('provinces.id', 'provinces.province_name', 'provinces.countries_id')
            ->get();

        $data = [];
        $data['type'] = $typeDb;
        $data['status'] = $statusDb;
        $data['sales'] = $registeredUser;
        $data['province'] = $provinces;
        $data['city'] = $cities;

//        dd($typeDb);

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getAjaxUserDataDownload(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // Request Input
        // Query Data
        $lockerDb = self::userList($request, 'download');
        $locker_service = DB::connection('popbox_virtual')->select('select l.locker_id, lls.name 
                        from lockers l
                        left join locker_services ls on l.id = ls.lockers_id
                        left join list_locker_services lls on ls.list_locker_services_id = lls.id
                        order by lls.name
        ');
        
//        dd($lockerDb);

        // Init Data Excel
        $filename = "[".date('ymd')."]"." User List";

        Excel::create($filename, function ($excel) use($lockerDb, $locker_service) {
            $excel->sheet('Sheet1', function ($sheet) use($lockerDb, $locker_service){
                $row = 1;
                $no_urut = 1;

                $arr_title = ['No', 'Agent Id', 'Agent Name', 'User Name', 'Phone', 'Email', 'Last Balance', 'Address',
                    'Address Detail', 'Province', 'City', 'District', 'Sub-District', 'Registered By', 'Service', 'Remarks',
                    'Register Date', 'Latitude, Longitude', 'Status', 'Type'];

                $sheet->row($row, $arr_title);

                foreach ($lockerDb as $index => $item) {

                    $row++;

                    if (isset($item->name)) {
                        $username = $item->name;
                    } else {
                        $username = '';
                    }

                    if (isset($item->phone)) {
                        $userphone = $item->phone;
                    } else {
                        $userphone = '';
                    }

                    if (isset($item->email)) {
                        $useremail = $item->email;
                    } else {
                        $useremail = '';
                    }

                    if (isset($item->register_name)) {
                        $registered_by = $item->register_name;
                    } else {
                        $registered_by = '';
                    }

                    $services = "";
                    if (!empty($locker_service)){
                        foreach ($locker_service as $service) {
                            if($service->locker_id == $item->locker_id){
                                if (empty($services)) { 
                                    $services = $service->name;
                                } else { 
                                    $services .= ", ".$service->name;
                                }
                            }
                        }
                    }

                    $arr = [$no_urut,
                        $item->locker_id,
                        $item->locker_name,
                        $username,
                        $userphone,
                        $useremail,
                        $item->balance,
                        $item->address,
                        $item->detail_address,
                        $item->province_name,
                        $item->city_name,
                        $item->district_name,
                        $item->sub_district_name,
                        $registered_by,
                        $services,
                        $item->remarks,
                        $item->created_at,
                        $item->latitude.','.$item->longitude,
                        isset($item->status_verification) ? $item->status_verification : '',
                        $item->type
                    ];

                    $sheet->row($row, $arr);
                    $no_urut++;
                }

            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getAjaxAllUserDataDownload(Request $request) {
        
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $header = [
            ["column"  => "A", "name" => "No",          "field" => "no",                 'align' => 'right'],
            ["column"  => "B", "name" => "Locker ID",   "field" => "locker_id",   'align' => 'left'],
            ["column"  => "C", "name" => "Locker Name", "field" => "locker_name",   'align' => 'left'],
            ["column"  => "D", "name" => "Type",        "field" => "type",          'align' => 'left'],
            ["column"  => "E", "name" => "Status",      "field" => "status",        'align' => 'left'],
            ["column"  => "F", "name" => "City",        "field" => "city",          'align' => 'left'],
        ];
        
        $result = DB::connection('popbox_agent')->select('
                    select distinct l.locker_id, l.locker_name, l.type, lk.status_verification, c.city_name
                    from 
                    	users u
                    	join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    	join popbox_virtual.cities c on l.cities_id = c.id
                    	join popbox_agent.lockers lk on lk.id = l.locker_id
                    where                     
                    	l.`deleted_at` is null
                    order by l.locker_name asc
                ');
        
        $rows = [];
        foreach ($result as $i => $r){
            $row = (array) $r;
            $rows[] = [
                'no'            => $i+1,
                'locker_id'     => $row['locker_id'],
                'locker_name'   => $row['locker_name'],
                'type'          => ucwords($row['type']),
                'status'        => ucwords($row['status_verification']),
                'city'          => ucwords($row['city_name']),
            ];
        }
        
        $filename = "List of User";
        $response = Helper::rendering_excel($filename, $rows, $header);
        
        return response()->json($response);
    }
    
    public function download(Request $request) {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            echo "File not found";
        }
    }

    /*--------------------------------------------------------LIST DETAIL------------------------------------------------------------*/

    public function getChangeTypeAgent(Request $request) {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $locker_id = $request->input('locker_id');
        $userId = Auth::user()->id;

        $popboxVirtual = DB::connection('popbox_virtual')
            ->table('lockers')
            ->where('locker_id', '=', $locker_id)
            ->update([
                'type' => 'warung',
                'dashboard_userid' => $userId,
                'converted_at' => date('Y-m-d H:i:s')]);

        if ($popboxVirtual) {
            $response->isSuccess = true;
        } else {
            $response->isSuccess = false;
        }

        // parse data to view
        $data = [];
        $data['lockerId'] = $locker_id;


        $response->data = $data;
        return response()->json($response);
    }

    public function getDetailSales(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $lockerId = $request->input('locker_id', null);
//        $dateRangeTransaction = '2018-07-01,2018-08-31';
        $dateRangeTransaction = $request->input('date_range', null);
        if (!empty($dateRangeTransaction)) {
            $transactionStartDate = Helper::formatDateRange($dateRangeTransaction, ',')->startDate;
            $transactionEndDate = Helper::formatDateRange($dateRangeTransaction, ',')->endDate;
        }

        $query1 = Transaction::leftJoin('users', 'users.id', '=', 'transactions.users_id')
            ->leftJoin('lockers', 'lockers.id', '=', 'users.locker_id')
            ->where('lockers.id', $lockerId)
            ->where('transactions.status', 'PAID')
            ->whereNull('transactions.deleted_at')
            ->whereBetween('transactions.created_at', [$transactionStartDate, $transactionEndDate])
            ->select(DB::raw('users.id, users.name,
                                sum( IF(transactions.type = "commission", 1, 0)) AS commission_count,
                                sum( IF(transactions.type = "deduct", 1, 0)) AS deduct_count,
                                sum( IF(transactions.type = "delivery", 1, 0)) AS delivery_count,
                                sum( IF(transactions.type = "payment", 1, 0)) AS payment_count,
                                sum( IF(transactions.type = "purchase" && transactions.cart_id IS NULL, 1, 0)) AS digital_count,
                                sum( IF(transactions.type = "purchase" && transactions.cart_id IS NOT NULL, 1, 0)) AS nondigital_count,
                                sum( IF(transactions.type = "referral", 1, 0)) AS referral_count,
                                sum( IF(transactions.type = "refund", 1, 0)) AS refund_count,
                                sum( IF(transactions.type = "reward", 1, 0)) AS reward_count,
                                sum( IF(transactions.type = "topup", 1, 0)) AS topup_count,
                                sum( IF(transactions.type = "commission", transactions.total_price, 0)) AS commission_amount,
                                sum( IF(transactions.type = "deduct", transactions.total_price, 0)) AS deduct_amount,
                                sum( IF(transactions.type = "delivery", transactions.total_price, 0)) AS delivery_amount,
                                sum( IF(transactions.type = "payment", transactions.total_price, 0)) AS payment_amount,
                                sum( IF(transactions.type = "purchase" && transactions.cart_id IS NULL, transactions.total_price, 0)) AS digital_amount,
                                sum( IF(transactions.type = "purchase" && transactions.cart_id IS NOT NULL, transactions.total_price, 0)) AS nondigital_amount,
                                sum( IF(transactions.type = "referral", transactions.total_price, 0)) AS referral_amount,
                                sum( IF(transactions.type = "refund", transactions.total_price, 0)) AS refund_amount,
                                sum( IF(transactions.type = "reward", transactions.total_price, 0)) AS reward_amount,
                                sum( IF(transactions.type = "topup", transactions.total_price, 0)) AS topup_amount'));

        $query2 = clone $query1;
        $query3 = clone $query1;
        $query4 = clone $query1;
        $query5 = clone $query1;

        $dailyData = $query1->addSelect(DB::raw('date(transactions.created_at) as date'))
            ->groupBy(DB::raw('date'))
            ->get();

        $weeklyData = $query2->addSelect(DB::raw('CONCAT(YEAR(transactions.created_at), "-", WEEK(transactions.created_at, 3)) as date'))
            ->groupBy(DB::raw('date'))
            ->get();

        $monthlyData = $query3->addSelect(DB::raw('CONCAT(YEAR(transactions.created_at), "-", MONTH(transactions.created_at)) as date'))
            ->groupBy(DB::raw('date'))
            ->get();

        $totalData = $query4->groupBy('transactions.users_id')
            ->get();

        $transactionAmount = 0;
        $transactionCount = 0;

        $topupAmount = 0;
        $commissionAmount = 0;
        $pulsaAmount = 0;
        $paymentAmount = 0;
        $popshopAmount = 0;

        $topupCount = 0;
        $commissionCount = 0;
        $pulsaCount = 0;
        $paymentCount = 0;
        $popshopCount = 0;

        foreach ($monthlyData as $datum) {
            $transactionAmount += $datum->digital_amount;
            $transactionAmount += $datum->nondigital_amount;
            $transactionAmount += $datum->payment_amount;
            $transactionAmount += $datum->delivery_amount;

            $transactionCount += $datum->digital_count;
            $transactionCount += $datum->nondigital_count;
            $transactionCount += $datum->payment_count;
            $transactionCount += $datum->delivery_count;

            $topupAmount += $datum->topup_amount;
            $commissionAmount += $datum->commission_amount;
            $commissionAmount += $datum->referral_amount;
            $commissionAmount += $datum->reward_amount;
            $pulsaAmount += $datum->digital_amount;
            $paymentAmount += $datum->payment_amount;
            $popshopAmount += $datum->nondigital_amount;

            $topupCount += $datum->topup_count;
            $commissionCount += $datum->commission_count;
            $commissionCount += $datum->referral_count;
            $commissionCount += $datum->reward_count;
            $pulsaCount += $datum->digital_count;
            $paymentCount += $datum->payment_count;
            $popshopCount += $datum->nondigital_count;
        }

        // parse data to view
        $data = [];
        $data['param'] = $request->input();
        $data['dailyDataSalesDb'] = $dailyData;
        $data['weeklyDataSalesDb'] = $weeklyData;
        $data['monthlyDataSalesDb'] = $monthlyData;
        $data['totalData'] = $totalData;
        $data['transactionAmount'] = $transactionAmount;
        $data['transactionCount'] = $transactionCount;
        $data['topupAmount'] = $topupAmount;
        $data['commissionAmount'] = $commissionAmount;
        $data['pulsaAmount'] = $pulsaAmount;
        $data['paymentAmount'] = $paymentAmount;
        $data['popshopAmount'] = $popshopAmount;
        $data['topupCount'] = $topupCount;
        $data['commissionCount'] = $commissionCount;
        $data['pulsaCount'] = $pulsaCount;
        $data['paymentCount'] = $paymentCount;
        $data['popshopCount'] = $popshopCount;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }
    
    public function populateUser(Request $request){
        $lockers = collect(self::populate_user($request))->map(function($item) {
            return ['locker_id' => $item->locker_id, 'locker_name' => $item->locker_name];
        });
        
        return $lockers;
    }

    /*--------------------------------------------------------PRIVATE FUNCTION------------------------------------------------------------*/

    private function userList($request, $req_type = ''){
        
        $transactionStartDate = null;
        $transactionEndDate = null;
        
        $registeredStartDate = null;
        $registeredEndDate = null;
        
        $add_selected_column = '';
        
        $type = $request->input('type');
        $status = $request->input('status', null);
        $sales = $request->input('sales');
        $province = $request->input('province', null);
        $city = $request->input('city', null);
        $name = $request->input('name', null);
        $agentid = $request->input('agentid', null);
        $dateRangeTransaction = $request->input('daterange_transaction', null);
        $dateRangeRegistered = $request->input('daterange_registered', null);
        
        $params = [
                    'join_transactions_table'       => "",
                    'where_city_id'                 => "",
                    'where_province_id'             => "",
                    'where_locker_id'               => "",
                    'where_locker_name'             => "",
                    'where_status_verification'     => "",
                    'where_lockers_created_at'      => "",
                    'where_transaction_created_at'  => "",
        ];
        
        if (!empty($dateRangeTransaction)) {
            $transactionStartDate = Helper::formatDateRange($dateRangeTransaction, ',')->startDate;
            $transactionEndDate = Helper::formatDateRange($dateRangeTransaction, ',')->endDate;
            
            $params['where_transaction_created_at'] = "and t.created_at between '".$transactionStartDate."' and '".$transactionEndDate."' and t.status = 'PAID' and t.type in ('purchase', 'payment')";
            $params['join_transactions_table'] = "join popbox_agent.`transactions` t on t.`users_id` = u.id";
        }
        
        if (!empty($dateRangeRegistered)) {
            $registeredStartDate = Helper::formatDateRange($dateRangeRegistered, ',')->startDate;
            $registeredEndDate = Helper::formatDateRange($dateRangeRegistered, ',')->endDate;
            
            $params['where_lockers_created_at'] = " and l.created_at between '".$registeredStartDate."' and '".$registeredEndDate."'";
        }
        
        if (!empty($status)){
            $params['where_status_verification'] = " and LCASE(lk.status_verification) = '".strtolower($status)."'";
        }
        
        if(!empty($type) && $type != 'all'){
            $params['where_user_type'] = " and l.type = '".$type."'";
        } else {
            $params['where_user_type'] = "";
        }
        
        if (empty($sales)) {
            $params['where_registered_by'] = "";
        } else {
            $params['where_registered_by'] = " and l.registered_by = $sales";
        }
        
        if(!empty($province)){
            $params['where_province_id'] = " and c.provinces_id = $province";
        }
        
        if(!empty($city)){
            $params['where_city_id'] = " and c.id = $city";
        }
        
        if(!empty($agentid)){
            $params['where_locker_id'] = " and l.locker_id LIKE '%$agentid%'";
        }
        
        if(!empty($name)){
            $params['where_locker_name'] = " and l.locker_name LIKE '%$name%'";
        }
        
        if($req_type == 'download'){
            $add_selected_column = ', u.name, u.phone, u.email';
        }
        
        $locker = DB::connection('popbox_agent')->select(self::query_user($params, $add_selected_column));
        
        return $locker;
    }
    
    private function populate_user($request){
        $keyword = $request->input('search','');
        
        $params = [
            'where_locker_id'   => "",
        ];
        
        if(!empty($keyword)){
            $params['where_locker_id'] = " and l.locker_id LIKE '%$keyword%' OR l.locker_name LIKE '%$keyword%'";
        }
        
        $locker = DB::connection('popbox_agent')->select(self::query_user($params));
        
        return $locker;
    }
    
    private function query_user($params, $add_selected_column = ''){
        
        $whereCon = '';
        $joinTable = '';
        if(array_has($params, 'where_status_verification')) {
            $whereCon .= $params['where_status_verification'];
        }
        if(array_has($params, 'where_user_type')) {
            $whereCon .= $params['where_user_type'];
        }
        if(array_has($params, 'where_registered_by')) {
            $whereCon .= $params['where_registered_by'];
        }
        if(array_has($params, 'where_city_id')) {
            $whereCon .= $params['where_city_id'];
        }
        if(array_has($params, 'where_province_id')) {
            $whereCon .= $params['where_province_id'];
        }
        if(array_has($params, 'where_locker_id')) {
            $whereCon .= $params['where_locker_id'];
        }
        if(array_has($params, 'where_locker_name')) {
            $whereCon .= $params['where_locker_name'];
        }
        if(array_has($params, 'where_transaction_created_at')) {
            $whereCon .= $params['where_transaction_created_at'];
        }
        if(array_has($params, 'where_lockers_created_at')) {
            $whereCon .= $params['where_lockers_created_at'];
        }
        if(array_has($params, 'join_transactions_table')){
            $joinTable .= $params['join_transactions_table'];
        }
        
        return "
                select distinct l.*, LCASE(lk.status_verification) as 'status_verification', c.city_name, p.province_name, reg.name as 'register_name', IFNULL(lk.balance, 0) as 'balance' $add_selected_column
                from
                    popbox_virtual.lockers l
                    left join users u on u.locker_id = l.locker_id
                    left join popbox_virtual.cities c on c.id = l.cities_id
                    left join popbox_virtual.provinces p on p.id = c.provinces_id
                    left join popbox_virtual.users reg on reg.id = l.registered_by
                    left join popbox_agent.lockers lk on lk.id = l.locker_id
                    ".$joinTable."
                where
                    l.`deleted_at` is null $whereCon
                    order by l.locker_name asc";
    }
    
    private function getFilterListUser($request, $download) {

        $transactionStartDate = null;
        $transactionEndDate = null;

        $registeredStartDate = null;
        $registeredEndDate = null;

        $firstOpeningPage = $request->input('firstOpeningPage', null);
        $type = $request->input('type', 'empty');
        $status = $request->input('status', null);
        $sales = $request->input('sales', 'empty');
        $province = $request->input('province', null);
        $city = $request->input('city', null);
        $name = $request->input('name', null);
        $agentid = $request->input('agentid', null);
        $dateRangeTransaction = $request->input('daterange_transaction', null);
        $dateRangeRegistered = $request->input('daterange_registered', null);

        if (!empty($dateRangeTransaction)) {
            $transactionStartDate = Helper::formatDateRange($dateRangeTransaction, ',')->startDate;
            $transactionEndDate = Helper::formatDateRange($dateRangeTransaction, ',')->endDate;
        }

        if (!empty($dateRangeRegistered)) {
            $registeredStartDate = Helper::formatDateRange($dateRangeRegistered, ',')->startDate;
            $registeredEndDate = Helper::formatDateRange($dateRangeRegistered, ',')->endDate;
        }

        $lockerList = [];
        if (!empty($status)){
            $agentLockerStatus = AgentLocker::where('status_verification',$status)->get();
            foreach ($agentLockerStatus as $item){
                $lockerList[] = $item->id;
            }
        }

        $transactionList = [];
        if (!empty($dateRangeTransaction)) {
            $agentLockerTransaction = DB::connection('popbox_agent')
                ->table('transactions')
                ->leftJoin('users', 'users.id', 'transactions.users_id')
                ->leftJoin('lockers', 'lockers.id', 'users.locker_id')
                ->whereBetween('transactions.created_at',[$transactionStartDate,$transactionEndDate])
                ->whereIn('transactions.type', ['purchase', 'payment'])
                ->where('transactions.status', '=', 'paid')
                ->whereNull('transactions.deleted_at')
                ->select(DB::raw('lockers.id, lockers.locker_name,
                                    GROUP_CONCAT(DISTINCT date(transactions.created_at) SEPARATOR ",") as transaction_date,
                                    (sum( IF(transactions.cart_id IS NULL, transactions.total_price, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, transactions.total_price, 0))) AS total_price,
                                    (sum( IF(transactions.cart_id IS NULL, 1, 0)) +
                                    sum( IF(transactions.cart_id IS NOT NULL, 1, 0))) AS total_jumlah'

                ))
                ->groupBy('transactions.users_id', 'lockers.locker_name')
                ->orderBy('locker_name','asc')
                ->get();
            foreach ($agentLockerTransaction as $item) {
                $transactionList[] = $item->id;
            }
        }

        if ($firstOpeningPage == 'true') {
            $lockerDb = Locker::with(['user', 'operational_time', 'city','city.province', 'services','registeredBy'])
            ->when($province, function ($query) use ($province) {
                return $query->whereHas('city.province',function ($query) use ($province){
                    $query->where('provinces_id',$province);
                });
            })->when($city, function ($query) use ($city) {
                return $query->where('cities_id', $city);
            })->when($type, function ($query) use ($type) {
                if ($type == 'empty') {
                    return $query->where('type', '!=', 'locker')->orWhere('type', '=', null);
                }
                return $query->where('type', $type);
            })->when($name, function ($query) use ($name) {
                return $query->where('locker_name', 'LIKE', "%$name%");
            })->when($agentid, function ($query) use ($agentid) {
                return $query->where('locker_id', 'LIKE', $agentid);
            })->when($dateRangeRegistered, function ($query) use ($registeredStartDate, $registeredEndDate) {
                return $query->whereBetween('popbox_virtual.lockers.created_at', [$registeredStartDate, $registeredEndDate]);
            })->when($sales,function ($query) use($sales){
                if ($sales == 'empty') {
                    return $query->whereNull('registered_by');
                } else {
                    return $query->where('registered_by', $sales);
                }
            })
            ->when($status,function ($query) use ($lockerList){
                $query->whereIn('popbox_virtual.lockers.locker_id',$lockerList);
            })->when($dateRangeTransaction,function ($query) use ($transactionList){
                $query->whereIn('popbox_virtual.lockers.locker_id',$transactionList);
            })
            // ->whereIn('type', ['agent','warung'])
            ->orderBy('locker_name', 'asc')
            // ->limit(25)
            ->get();
        } else {
            
            if ($download == 'download') {

                $lockerDb = Locker::with(['user', 'operational_time', 'city','city.province', 'services','registeredBy'])
                    ->leftJoin('popbox_agent.users', 'popbox_agent.users.locker_id', '=', 'popbox_virtual.lockers.locker_id')
                    ->when($province, function ($query) use ($province) {
                        return $query->whereHas('city.province',function ($query) use ($province){
                            $query->where('provinces_id',$province);
                        });
                    })->when($city, function ($query) use ($city) {
                        return $query->where('cities_id', $city);
                    })->when($type, function ($query) use ($type) {
                        if ($type == 'empty') {
                            return $query->where('type', '!=', 'locker')->orWhere('type', '=', null);
                        }
                        return $query->where('type', $type);
//                    return $query->whereIn('type', explode(',', $type));
                    })->when($name, function ($query) use ($name) {
                        return $query->where('locker_name', 'LIKE', "%$name%");
                    })->when($agentid, function ($query) use ($agentid) {
                        return $query->where('locker_id', 'LIKE', $agentid);
                    })->when($dateRangeRegistered, function ($query) use ($registeredStartDate, $registeredEndDate) {
                        return $query->whereBetween('popbox_virtual.lockers.created_at', [$registeredStartDate, $registeredEndDate]);
                    })->when($sales,function ($query) use($sales){
                        if ($sales == 'empty') {
                            return $query->whereNull('registered_by');
                        } else {
                            return $query->where('registered_by', $sales);
                        }
                    })
                    ->when($status,function ($query) use ($lockerList){
                        $query->whereIn('popbox_virtual.lockers.locker_id',$lockerList);
                    })->when($dateRangeTransaction,function ($query) use ($transactionList){
                        $query->whereIn('popbox_virtual.lockers.locker_id',$transactionList);
                    })
                    // ->whereIn('type', ['agent','warung'])
                    ->orderBy('locker_name', 'asc')
                    ->get();

            } else {

                $lockerDb = Locker::with(['user', 'operational_time', 'city','city.province', 'services','registeredBy'])
                    ->when($province, function ($query) use ($province) {
                        return $query->whereHas('city.province',function ($query) use ($province){
                            $query->where('provinces_id',$province);
                        });
                    })->when($city, function ($query) use ($city) {
                        return $query->where('cities_id', $city);
                    })->when($type, function ($query) use ($type) {
                        if ($type == 'empty') {
                            return $query->where('type', '!=', 'locker')->orWhere('type', '=', null);
                        }
                        return $query->where('type', $type);
//                    return $query->whereIn('type', explode(',', $type));
                    })->when($name, function ($query) use ($name) {
                        return $query->where('locker_name', 'LIKE', "%$name%");
                    })->when($agentid, function ($query) use ($agentid) {
                        return $query->where('locker_id', 'LIKE', $agentid);
                    })->when($dateRangeRegistered, function ($query) use ($registeredStartDate, $registeredEndDate) {
                        return $query->whereBetween('popbox_virtual.lockers.created_at', [$registeredStartDate, $registeredEndDate]);
                    })->when($sales,function ($query) use($sales){
                        if ($sales == 'empty') {
                            return $query->whereNull('registered_by');
                        } else {
                            return $query->where('registered_by', $sales);
                        }
                    })
                    ->when($status,function ($query) use ($lockerList){
                        $query->whereIn('popbox_virtual.lockers.locker_id',$lockerList);
                    })->when($dateRangeTransaction,function ($query) use ($transactionList){
                        $query->whereIn('popbox_virtual.lockers.locker_id',$transactionList);
                    })
                    // ->whereIn('type', ['agent','warung'])
                    ->orderBy('locker_name', 'asc')
                    ->get();

            }
        }

        return $lockerDb;

    }

    private function formatResult($value) {
        
        $result = new \stdClass();

        if (isset($value->locker_id)) {
            $result->id = $value->locker_id;
        } else {
            $result->id = '';
        }

        if (isset($value->locker_name)) {
            $result->name = $value->locker_name;
        } else {
            $result->name = '';
        }

        if (isset($value->type)) {
            $result->type = $value->type;
        } else {
            $result->type = '';
        }

        if (isset($value->balance)) {
            $result->deposit = $value->balance;
        } else {
            $result->deposit = '';
        }

        if (isset($value->city_name)) {
            $result->city = $value->city_name;
        } else {
            $result->city = '';
        }

        if (isset($value->status_verification)) {
            $result->status = $value->status_verification;
        } else {
            $result->status = '';
        }

        if (isset($value->register_name)) {
            $result->registeredby = $value->register_name;
        } else {
            $result->registeredby = '';
        }

        if (isset($value->created_at)) {
            $result->registeredDate = date('d M Y', strtotime($value->created_at));
//             $result->registeredDate = $value->created_at->format('d M Y');
        } else {
            $result->registeredDate = '';
        }

        return $result;
    }

    private function ContainsNumbers($String){
        return preg_match('/\\d/', $String) > 0;
    }
}
