<?php

namespace App\Http\Controllers;

use App\Models\Agent\AgentLocker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LogAccessController extends Controller
{
    public function getAgentAccess(Request $request){
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get log 3 days ago. *default
        $startDate = date('Y-m-d',strtotime('-3 days'));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $url = $request->input('url',null);
        $lockerId = $request->input('lockerId',null);
        $requestInput = $request->input('request',null);

        $logAccess = DB::connection('popbox_agent')
            ->table('logs')
            ->join('users','users.id','=','logs.users_id')
            ->leftJoin('lockers','lockers.id','=','users.locker_id')
            ->when($url,function ($query) use ($url){
                return $query->where('url','LIKE',"%$url%");
            })->when($lockerId,function ($query) use ($lockerId){
                return $query->where('users.locker_id','=',$lockerId);
            })->when($requestInput,function ($query) use($requestInput){
                return $query->where('request','LIKE',"%$requestInput%");
            })->select('lockers.id','lockers.locker_name','users.name','logs.url','logs.ip','logs.request','logs.response','logs.created_at')
            ->orderBy('logs.id','desc')
            ->paginate(10);

        // get data for form search
        $lockerDb = AgentLocker::get();


        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // parse data to view
        $data = [];
        $data['logDb'] = $logAccess->appends($request->input());
        $data['lockerDb'] = $lockerDb;
        $data['param'] = $param;

        return view('logs.agent',$data);
    }
}
