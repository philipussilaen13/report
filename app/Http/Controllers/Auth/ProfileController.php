<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Get Change Password Page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChangePassword(Request $request){
        return view('auth.password-change');
    }

    /**
     * Post Change Password
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangePassword(Request $request){
        $rules = [
            'oldPassword' => 'required',
            'password' => 'required|min:6|string',
            'rePassword' => 'required|same:password',
        ];

        $this->validate($request,$rules);

        $oldPassword = $request->input('oldPassword');
        $newPassword = $request->input('password');

        // get user data
        $userDb = Auth::user();

        $currentPasswordHash = $userDb->password;

        // validate old password
        $check = Hash::check($oldPassword,$currentPasswordHash);
        if (!$check){
            $request->session()->flash('error','Invalid Old Password');
            return back();
        }

        // change password
        $userDb = User::find($userDb->id);
        $userDb->password = Hash::make($newPassword);
        $userDb->save();

        $request->session()->flash('success','Success Change Password');
        return back();
    }
}
