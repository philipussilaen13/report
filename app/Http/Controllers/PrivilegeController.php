<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Module;
use App\Models\Privilege;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Expr\AssignOp\Mod;

class PrivilegeController extends Controller
{
    /**
     * Get Group Management Page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getGroup(Request $request){
        // get group data
        $groupDb = new Group();
        $groupData = $groupDb->get();

        // parse to view
        $data = [];
        $data['groupData'] = $groupData;
        return view('privileges.group',$data);
    }

    /**
     * Create and Update Group
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postGroup(Request $request){
        $groupName = $request->input('groupName',null);
        $groupId = $request->input('groupId',null);

        if (empty($groupName)){
            $request->session()->flash('error','Group name cant be empty');
            return back();
        }

        // check if group name is exist
        $groupDb = new Group();
        $checkName = $groupDb->where('name',$groupName)->first();

        if ($checkName && empty($groupId)){
            $request->session()->flash('error','Group Name Already Exist');
            return back();
        }

        if (empty($groupId)){
            // insert to db group
            $groupDb->name = $groupName;
            $groupDb->save();
            $request->session()->flash('success','Success Create New Group : '.$groupName);
        } else {
            // update DB
            $groupDb = Group::find($groupId);
            $groupDb->name = $groupName;
            $groupDb->save();
            $request->session()->flash('success','Success Update Group '.$groupName);
        }
        return back();
    }

    /**
     * Get Module List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getModule(Request $request){
        // get module from route
        $routeList = Route::getRoutes();
        $registeredRoutes = [];
        foreach ($routeList as $item) {
            if (!in_array($item->uri,$registeredRoutes)) $registeredRoutes[] = $item->uri;
        }
        $routeLists = [];
        foreach ($registeredRoutes as $route) {
            if (strpos($route, 'ajax') !== false) continue;
            if (strpos($route, 'Ajax') !== false) continue;
            elseif (strpos($route, '_debugbar') !== false) continue;
            $tmp = new \stdClass();
            $tmp->id = null;
            $tmp->group = null;
            $tmp->name = $route;
            $tmp->description = null;
            $tmp->active = null;
            // set group
            $exploded = explode('/',$route);
            $group = $exploded[0];
            if (empty($group)) $group = 'landing';
            $tmp->group = $group;
            // check on DB
            $moduleDb = Module::where('name',$route)->first();
            if (!$moduleDb){
                $moduleDb = new Module();
                $moduleDb->module_group = $group;
                $moduleDb->name = $route;
                $moduleDb->description = null;
                $moduleDb->active = 1;
                $moduleDb->save();
            }
            $tmp->id = $moduleDb->id;
            $tmp->name = $moduleDb->name;
            $tmp->description = $moduleDb->description;
            $tmp->status = $moduleDb->active;
            $routeLists[] = $tmp;
        }

        // get module not in registered route
        $moduleDb = Module::whereNotIn('name',$registeredRoutes)->get();
        foreach ($moduleDb as $item){
            // delete the module and privilege
            $moduleId = $item->id;
            $deletePrivilege = Privilege::where('module_id',$moduleId)->delete();

            // delete model
            $deleteModule = Module::where('id',$moduleId)->delete();
        }

        // parse data to view
        $data = [];
        $data['routeLists'] = $routeLists;

        return view('privileges.module',$data);
    }

    /**
     * Create and Update Module
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postModule(Request $request){
        $moduleId = $request->input('moduleId',null);
        $moduleName = $request->input('moduleName');
        $moduleDescription = $request->input('moduleDescription');

        // check param
        if (empty($moduleName) || empty($moduleDescription)){
            $request->session()->flash('error','Module Name or Module Description cant be empty');
            return back();
        }

        // check module name on DB
        $check = Module::where('name',$moduleName)->first();

        if (empty($moduleId) && $check){
            $request->session()->flash('error','Module Name already Exist');
            return back();
        }

        if (empty($moduleId)){
            // insert to DB
            $data = new Module();
            $data->module_group = 'dashboard';
            $data->name = $moduleName;
            $data->description = $moduleDescription;
            $data->active = 1;
            $data->save();

            $request->session()->flash('success','Success Add Module '.$moduleName);
        } else {
            // update DB
            $data = Module::find($moduleId);
            $data->description = $moduleDescription;
            $data->save();
            $request->session()->flash('success','Success Update Module '.$moduleName);
        }
        return back();
    }

    /**
     * Get Privileges Page and Group Privilege
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPrivilege(Request $request){
        // get Group List
        $groupDb = new Group();
        $groupData = $groupDb->get();

        $groupId = $request->input('groupId',null);

        $moduleList = [];
        $selectedGroup = null;
        if (!empty($groupId)){
            $selectedGroup = Group::find($groupId);
            $moduleDb = Module::orderBy('name','asc')->get();
            foreach ($moduleDb as $module){
                $tmp = new \stdClass();
                $tmp->id = $module->id;
                $tmp->module_group = $module->module_group;
                $tmp->module_name = $module->name;
                $tmp->description = $module->description;
                $tmp->isChecked = false;
                // check available on privileges
                $check = Privilege::where('group_id',$groupId)->where('module_id',$module->id)->first();
                if ($check) $tmp->isChecked = true;
                $moduleList[] = $tmp;
            }
        }

        // parse data to view
        $data['groupData'] = $groupData;
        $data['moduleList'] = $moduleList;
        $data['selectedGroup'] = $selectedGroup;

        return view('privileges.privilege',$data);
    }

    /**
     * Post Privileges based on Group
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPrivilege(Request $request){
        $groupId = $request->input('groupId');
        $newPrivileges = $request->input('modules');

        // delete all Privileges
        Privilege::where('group_id',$groupId)->delete();

        // insert new Privileges
        foreach ($newPrivileges as $item) {
            $db = new Privilege();
            $db->group_id = $groupId;
            $db->module_id = $item;
            $db->save();
        }

        $request->session()->flash('success','Success Change Privilege');
        return back();
    }

    /**
     * Get User List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUser(Request $request){
        // get group list
        $groupDb = Group::orderBy('name','asc')->get();

        // filtering data function
        $searchName = $request->input('search-name',null);
        $searchPhone = $request->input('search-phone',null);
        $searchEmail = $request->input('search-email',null);
        $searchGroup = $request->input('search-group',null);

        // get user list
        $userDb = User::when($searchName,function ($query) use ($searchName){
            return $query->where('name','LIKE',"%$searchName%");
        })->when($searchPhone,function ($query) use ($searchPhone){
            return $query->where('phone',$searchPhone);
        })->when($searchEmail,function ($query) use ($searchEmail){
            return $query->where('email',$searchEmail);
        })->when($searchGroup,function ($query) use ($searchGroup){
            return $query->where('group_id',$searchGroup);
        })->get();

        // parsing data to view
        $data = [];
        $data['users'] = $userDb;
        $data['groups'] = $groupDb;
        return view('privileges.user',$data);
    }

    /**
     * Create and Update User
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUser(Request $request){
        $userId = $request->input('userId',null);

        if (empty($userId)){
            $userDb = new User();
            $userDb->name = $request->input('name');
            $userDb->phone = $request->input('phone');
            $userDb->username = $request->input('phone');
            $userDb->email = $request->input('email');
            $userDb->password = Hash::make($request->input('password'));
            $userDb->group_id = $request->input('group');
            $userDb->status = $request->input('status','enable');
            $userDb->save();
            $request->session()->flash('success','Success Add New User '.$request->input('name'));
        } else {
            $userDb = User::find($userId);
            $userDb->name = $request->input('name');
            $userDb->phone = $request->input('phone');
            $userDb->username = $request->input('phone');
            $userDb->email = $request->input('email');
            $userDb->password = Hash::make($request->input('password'));
            $userDb->group_id = $request->input('group');
            $userDb->status = $request->input('status','enable');
            $userDb->save();
            $request->session()->flash('success','Success Update User '.$request->input('name'));
        }

        return back();
    }

    /*-------------------------------------------------PRIVILEGE V2-----------------------------------------------------------------------*/

    public function getLanding() {

        return view('privileges.privilege2');
    }

    public function getPrivilegesGroupList() {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $listGroupDB = Group::whereNotIn('groups.id', [1,2,3,4])->get();

        $groupModuleDB = Group::leftJoin('privileges', 'privileges.group_id', '=', 'groups.id')
            ->leftJoin('modules', 'modules.id', '=', 'privileges.module_id')
            ->whereNull('privileges.deleted_at')
            ->whereNotIn('groups.id', [1,2,3,4])
            ->select('groups.id as group_id', 'groups.name as group_name', 'privileges.module_id', 'modules.module_group', 'modules.name', 'modules.description')
            ->orderBy('groups.id', 'asc')
            ->orderBy('module_group', 'asc')
            ->get();

        $modulesDB = Module::whereNull('deleted_at')
            ->orderBy('module_group', 'asc')
            ->get();

        $privilegesDB = Privilege::whereNull('deleted_at')->get();

        $data = [];
        $data['listGroupDB'] = $listGroupDB;
        $data['groupModuleDB'] = $groupModuleDB;
        $data['modulesDB'] = $modulesDB;
        $data['privilegesDB'] = $privilegesDB;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }
}
