<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Group;
use App\Models\Popexpress\Account;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\Customer;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Employee;
use App\Models\UserGroup;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class AccountController extends Controller {

    protected $accessAdd = false;
    protected $accessExport = false;
    protected $allowedGroups = [
        'add' => ['popexpress_admin', 'popexpress_finance'],
        'export' => ['popexpress_admin', 'popexpress_finance', 'popexpress_operation_head', 'popexpress_sales'],
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userid = Auth::user()->id;
            $allowed = $this->allowedGroups['add'];
            $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                ->where('user_groups.deleted_at', null)
                ->where('user_groups.user_id', $userid)
                ->where(function($query) use ($allowed){
                    foreach ($allowed as $allow) {
                        $query->orWhere('groups.name', '=', $allow);
                    }
                })
                ->first();
            $this->accessAdd = (!is_null($userGroups) ? true : false);

            $allowedExports = $this->allowedGroups['export'];
            $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                ->where('user_groups.deleted_at', null)
                ->where('user_groups.user_id', $userid)
                ->where(function($query) use ($allowedExports){
                    foreach ($allowedExports as $allow) {
                        $query->orWhere('groups.name', '=', $allow);
                    }
                })
                ->first();
            $this->accessExport = (!is_null($userGroups) ? true : false);

            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $code = request('code');
        $email = request('email');
        $name = request('name');
        $phone = request('phone');
        $sales = request('sales');
        $type = request('customer_type');
        $suspend = request('suspend');
        $branch = request('branch_id');
        $account_name = request('account_name');

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->where('groups.name', 'popexpress_sales')
            ->where('user_groups.deleted_at', null)
            ->pluck('users.name', 'users.id')
            ->toArray();

        $branches = Branch::where('deleted_at', null)->select('id', DB::raw("CONCAT(code, ' - ', name) as name"))->get();

        $suspends = [0 => 'No', 1 => 'Yes'];
        $types = ['social_seller' => 'Social Seller', 'corporate' => 'Corporate', 'marketplace' => 'Marketplace'];

        $getAccounts = Account::leftJoin('customers', 'customers.id', '=', 'accounts.customer_id')
            ->when($sales, function ($query) use ($sales) {
                return $query->where('customers.sales', '=', $sales);
            })
            ->when($type, function ($query) use ($type) {
                return $query->where('customers.customer_type', '=', $type);
            })
            ->when($suspend, function ($query) use ($suspend) {
                return $query->where('customers.suspend', '=', $suspend);
            })
            ->when($branch, function ($query) use ($branch) {
                return $query->where('accounts.branch_id', '=', $branch);
            })
            ->when($code, function ($query) use ($code) {
                return $query->where('customers.code', 'LIKE', "%$code%");
            })
            ->when($account_name, function ($query) use ($account_name) {
                return $query->where('accounts.account_name', 'LIKE', "%$account_name%");
            })
            ->pluck('customers.user_id')->toArray();

        $getUsers = User::whereIn('users.id', $getAccounts)
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('users.name', 'LIKE', "%$name%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.username',
                'users.phone',
                'users.email'
            )
            ->get();

        $users = [];
        $userlist = [];

        foreach ($getUsers as $getUser) {

            $users[$getUser->report_users_id] = [
                'report_users_id' => $getUser->report_users_id,
                'name' => $getUser->name,
                'username' => $getUser->username,
                'phone' => $getUser->phone,
                'email' => $getUser->email
            ];
            $userlist[] = $getUser->report_users_id;

        }

        $accounts = Account::leftjoin('customers', 'customers.id', '=', 'accounts.customer_id')
            ->leftjoin('employees', 'employees.id', '=', 'customers.sales')
            ->where('accounts.deleted_at', null)
            ->select(
                DB::raw('accounts.id as id'),
                DB::raw('accounts.branch_id as branch_id'),
                DB::raw('accounts.all_branch as all_branch'),
                DB::raw('accounts.account_name as account_name'),
                DB::raw('customers.user_id as report_users_id'),
                DB::raw('customers.id as customers_id'),
                DB::raw('customers.code as code'),
                DB::raw("employees.user_id as employees_user_id"),
                DB::raw("customers.credit_limit as credit_limit"),
                DB::raw("IF(customers.suspend = 1, '(SUSPEND)', '') as is_suspend")
            )
            ->whereIn('customers.user_id', $userlist)
            ->orderBy('customers.code')
            ->paginate(20);

        $accessAdd = $this->accessAdd;
        $accessReport = $this->accessExport;

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->where('groups.name', 'popexpress_sales')
            ->where('user_groups.deleted_at', null)
            ->where('users.deleted_at', null)
            ->pluck('users.name', 'users.id')
            ->toArray();

        $listSales = [];
        foreach ($getSales as $key => $getSale) {
            $listSales[] = $key;
        }

        $salesEmployees = Employee::whereIn('user_id', $listSales)
            ->where('deleted_at', null)
            ->select('user_id', 'id')
            ->get();

        $sales = [];
        foreach ($salesEmployees as $salesEmployee) {
            $sales[] = [
                'id' => $salesEmployee->id,
                'name' => $getSales[$salesEmployee->user_id]
            ];
        }

        $customers = Customer::where('deleted_at', null)->select('id', 'code', 'user_id')->get();
        $customerLists = Customer::where('deleted_at', null)->pluck('user_id')->toArray();
        $customerUsers = User::where('deleted_at', null)->whereIn('id', $customerLists)->pluck('name', 'id')->toArray();

        $custs = [];
        foreach ($customers as $customer) {
            $custs[] = [
                'id' => $customer->id,
                'name' => $customer->code.' - '.$customerUsers[$customer->user_id]
            ];
        }

        return view('popexpress.accounts.index', compact('accounts', 'accessAdd', 'accessReport', 'users', 'getSales', 'sales', 'types', 'branches', 'suspends', 'custs'));
    }

    public function store(Request $request)
    {
        if(!$this->accessAdd) {
            abort('404');
        }

        $id = request('id');
        $delete = request('delete');

        $account_name = request('account_name');
        $customer_id = request('customer_id');
        $branch_id = request('branch_id');
        $all_branch = request('all_branch');
        $timestamp = date('Y-m-d H:i:s');

        if(empty($id)) {

            $this->validate(request(), [
                'account_name' => 'required',
                'customer_id' => 'required',
                'branch_id' => 'required'
            ],[
                'account_name.required' => 'Code diperlukan.',
                'customer_id.required' => 'Name diperlukan.',
                'branch_id.required' => 'Username diperlukan.'
            ]);

            DB::beginTransaction();

            $account = new Account();
            $account->account_name = $account_name;
            $account->customer_id = $customer_id;
            $account->branch_id = $branch_id;
            $account->all_branch = (!empty($all_branch) ? $all_branch : 0);
            $account->server_timestamp = $timestamp;
            if($account->save()) {
                DB::commit();
                $data = $account->getAttributes();
                $key = $account->id;
                $module = 'accounts';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            } else {
                DB::rollBack();
            }

            session()->flash('success', 'Data customer telah berhasil disimpan.');

        } else {

            $account = Account::where('id', '=', $id)->first();

            if($delete == '1') {
                DB::beginTransaction();
                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                $deleteAccount = Account::where('deleted_at', null)->where('id', $id)->update($dataDelete);
                if($deleteAccount){
                    DB::commit();
                    $key = $id;
                    $module = 'accounts';
                    $type = 'delete';
                    $jsonBefore = json_encode($account);
                    $jsonAfter = json_encode($dataDelete);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data telah dihapus.');
                } else {
                    DB::rollBack();
                }

            } else {

                $this->validate(request(), [
                    'account_name' => 'required',
                    'customer_id' => 'required',
                    'branch_id' => 'required'
                ],[
                    'account_name.required' => 'Code diperlukan.',
                    'customer_id.required' => 'Name diperlukan.',
                    'branch_id.required' => 'Username diperlukan.'
                ]);

                DB::beginTransaction();

                $account = Account::where('id', $id)->first();
                $account->account_name = $account_name;
                $account->customer_id = $customer_id;
                $account->branch_id = $branch_id;
                $account->all_branch = (!empty($all_branch) ? $all_branch : 0);
                $account->server_timestamp = $timestamp;

                if($account->save()) {
                    DB::commit();
                    $key = $id;
                    $module = 'accounts';
                    $type = 'edit';
                    $jsonBefore = json_encode($account);
                    $jsonAfter = json_encode($account);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data accounts telah berhasil diperbarui.');
                } else {
                    DB::rollBack();
                    session()->flash('error', 'Data accounts gagal diperbarui.');
                }
            }
        }

        return redirect('/popexpress/accounts');

    }

    public function export(Request $request)
    {
        if(!$this->accessExport) {
            abort('404');
        }

        $code = request('code');
        $email = request('email');
        $name = request('name');
        $phone = request('phone');
        $sales = request('sales');
        $type = request('customer_type');
        $suspend = request('suspend');
        $branch = request('branch_id');
        $account_name = request('account_name');

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->where('groups.name', 'popexpress_sales')
            ->where('user_groups.deleted_at', null)
            ->pluck('users.name', 'users.id')
            ->toArray();

        $branches = Branch::where('deleted_at', null)->select('id', DB::raw("CONCAT(code, ' - ', name) as name"))->get();

        $suspends = [0 => 'No', 1 => 'Yes'];
        $types = ['social_seller' => 'Social Seller', 'corporate' => 'Corporate', 'marketplace' => 'Marketplace'];

        $getAccounts = Account::leftJoin('customers', 'customers.id', '=', 'accounts.customer_id')
            ->when($sales, function ($query) use ($sales) {
                return $query->where('customers.sales', '=', $sales);
            })
            ->when($type, function ($query) use ($type) {
                return $query->where('customers.customer_type', '=', $type);
            })
            ->when($suspend, function ($query) use ($suspend) {
                return $query->where('customers.suspend', '=', $suspend);
            })
            ->when($branch, function ($query) use ($branch) {
                return $query->where('accounts.branch_id', '=', $branch);
            })
            ->when($code, function ($query) use ($code) {
                return $query->where('customers.code', 'LIKE', "%$code%");
            })
            ->when($account_name, function ($query) use ($account_name) {
                return $query->where('accounts.account_name', 'LIKE', "%$account_name%");
            })
            ->pluck('customers.user_id')->toArray();

        $getUsers = User::whereIn('users.id', $getAccounts)
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('users.name', 'LIKE', "%$name%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.username',
                'users.phone',
                'users.email'
            )
            ->get();

        $users = [];
        $userlist = [];

        foreach ($getUsers as $getUser) {

            $users[$getUser->report_users_id] = [
                'report_users_id' => $getUser->report_users_id,
                'name' => $getUser->name,
                'username' => $getUser->username,
                'phone' => $getUser->phone,
                'email' => $getUser->email
            ];
            $userlist[] = $getUser->report_users_id;

        }

        $accounts = Account::leftjoin('customers', 'customers.id', '=', 'accounts.customer_id')
            ->leftjoin('employees', 'employees.id', '=', 'customers.sales')
            ->leftJoin('destinations', 'destinations.id', '=', 'customers.pickup_location')
            ->leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->where('accounts.deleted_at', null)
            ->select(
                DB::raw('accounts.id as id'),
                DB::raw('accounts.branch_id as branch_id'),
                DB::raw('accounts.all_branch as all_branch'),
                DB::raw('accounts.account_name as account_name'),
                DB::raw('customers.user_id as report_users_id'),
                DB::raw('customers.id as customers_id'),
                DB::raw('customers.customer_type as customer_type'),
                DB::raw('customers.terms_of_payment as terms_of_payment'),
                DB::raw('customers.code as code'),
                DB::raw('customers.remark as remark'),
                DB::raw('customers.billing_address as billing_address'),
                DB::raw('customers.pickup_address as pickup_address'),
                DB::raw("CONCAT(origins.name, ' - ', destinations.airport_code, ', ', destinations.airport_code, ', ', destinations.detail_code, ', ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS destinations_name"),
                DB::raw("employees.user_id as employees_user_id"),
                DB::raw("customers.credit_limit as credit_limit"),
                DB::raw("IF(customers.suspend = 1, 'Yes', 'No') as is_suspend")
            )
            ->whereIn('customers.user_id', $userlist)
            ->orderBy('customers.code')
            ->get();


        $filename = "accounts_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($accounts, $users, $getSales, $types){
            $excel->sheet('Sheet1', function($sheet) use($accounts, $users, $getSales, $types){
                $row = 1;
                $arr_title = ['code','name','account_name','username','email','phone','credit_limit','suspend','terms_of_payment', 'sales','last_transaction','customer_type','remark','billing_address','pickup_address','pickup_location'];

                $sheet->row($row, $arr_title);

                foreach ( $accounts as $index => $item) {
                    $row++;
                    $arr = [
                        $item->code,
                        $users[$item->report_users_id]['name'],
                        $item->account_name,
                        $users[$item->report_users_id]['username'],
                        $users[$item->report_users_id]['email'],
                        $users[$item->report_users_id]['phone'],
                        intval($item->credit_limit),
                        $item->is_suspend,
                        $item->terms_of_payment,
                        isset($getSales[$item->employees_user_id]) ? $getSales[$item->employees_user_id] : '',
                        '',
                        $types[$item->customer_type],
                        $item->remark,
                        $item->billing_address,
                        $item->pickup_address,
                        $item->destinations_name,
                    ];

                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

}