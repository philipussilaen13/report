<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Account;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\Customer;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Employee;
use App\Models\Popexpress\Pickup;
use App\Models\Popexpress\PickupDetail;
use App\Models\Popexpress\PickupTask;
use App\Models\UserGroup;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class PickupController extends Controller
{

    public function index(Request $request)
    {
        $pickup_no = request('pickup_no');
        $branch_id = request('branch_id');
        $pickup_status = request('pickup_status');
        $customer_id = request('customer_id');
        $account_id = request('account_id');
        $start_date = request('start_date');
        $end_date = request('end_date');

        $start = substr($start_date, 6, 4).'-'.substr($start_date, 3, 2).'-'.substr($start_date, 0, 2);
        $end = substr($end_date, 6, 4).'-'.substr($end_date, 3, 2).'-'.substr($end_date, 0, 2);

        $users = User::where('deleted_at', null)->pluck('name', 'id')->toArray();

        $pickups = Pickup::leftJoin('accounts', 'accounts.id', '=', 'pickups.account_id')
            ->leftJoin('customers', 'customers.id', '=', 'pickups.customer_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'pickups.pickup_location')
            ->leftJoin('branches', 'branches.id', '=', 'pickups.branch_id')
            ->where('pickups.deleted_at', null)
            ->select(
                'pickups.*',
                'pickups.id as pickup_id',
                'accounts.account_name as account_name',
                'customers.user_id as customer_user_id',
                DB::raw("CONCAT(destinations.county, ' - ', destinations.district) AS destination"),
                DB::raw("CONCAT(branches.code, ' - ', branches.name) AS branch_name")
            )
            ->when($pickup_no, function ($query) use ($pickup_no) {
                return $query->where('pickups.pickup_no', 'LIKE', "%$pickup_no%");
            })
            ->when($branch_id, function ($query) use ($branch_id) {
                return $query->where('pickups.branch_id', '=', $branch_id);
            })
            ->when($pickup_status, function ($query) use ($pickup_status) {
                return $query->where('pickups.pickup_status', '=', $pickup_status);
            })
            ->when($customer_id, function ($query) use ($customer_id) {
                return $query->where('pickups.customer_id', '=', $customer_id);
            })
            ->when($account_id, function ($query) use ($account_id) {
                return $query->where('pickups.account_id', '=', $account_id);
            })
            ->when($start_date, function ($query) use ($start) {
                return $query->whereDate('pickups.created_at', '>=', $start);
            })
            ->when($end_date, function ($query) use ($end) {
                return $query->whereDate('pickups.created_at', '<=', $end);
            })
            ->orderBy('pickups.created_at', 'desc')
            ->paginate(20);

        foreach($pickups as $key => $data){
            if($data->created_at != ""){
                $pickups[$key]->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('Y-m-d H:i:s').CommonPopExpressHelper::getTimezoneText();
            }
        }

        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();

        $customers = Customer::where('deleted_at', null)->pluck('id', 'user_id')->toArray();

        $filterCustomers = array_keys($customers);
        $idsCustomers = array_values($customers);
        $usersFilter = User::whereIn('id', $filterCustomers)->orderBy('name')->select('name', 'id')->get();
        $accounts = Account::where('deleted_at', null)->orderBy('account_name')->get();
        $statuses = ['requested' => 'warning', 'assigned' => 'warning', 'picked' => 'success', 'pending' => 'success', 'processing' => 'success', 'closed' => 'primary', 'canceled' => 'danger'];
        $allStatus = array_keys($statuses);

        return view('popexpress.pickups.index', compact('users', 'pickups', 'branches', 'customers', 'statuses', 'accounts', 'allStatus', 'usersFilter'));

    }

    public function create()
    {
        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();
        $accounts = Account::where('deleted_at', null)->orderBy('account_name')->get();
        $types = ["non-cash", "cash", "locker", "popsend-address", "popsend-locker"];
        $times = ["09.00-18.00", "18.00-21.00", "all-day"];
        $sources = [ "web", "whatsapp", "line", "email", "sms", "phone", "api"];
        $locations = Destination::where('deleted_at', null)->orderBy('district')->get();

        return view('popexpress.pickups.create', compact('branches', 'accounts', 'types', 'times', 'sources', 'locations'));
    }

    public function store(Request $request)
    {
        $id = request('pickupid');

        $branch_id = request('branch_id');
        $pickup_time = request('pickup_time');
        $pickup_location = request('pickup_location');
        $pickup_address = request('pickup_address');
        $expected_total_items = request('expected_total_items');
        $account_id = request('account_id');
        $pickup_type = request('pickup_type');
        $pickup_source = request('pickup_source');
        $pickup_alternative_address = request('pickup_alternative_address');
        $pickup_remarks = request('pickup_remarks');
        $pickup_latitude = request('pickup_latitude');
        $pickup_longitude = request('pickup_longitude');
        $pickup_status = request('pickup_status');
        $pickup_customer_name = request('pickup_customer_name');
        $pickup_customer_email = request('pickup_customer_email');
        $pickup_customer_phone = request('pickup_customer_phone');

        if(empty($id)) {

            $this->validate(request(), [
                'branch_id' => 'required',
                'pickup_time' => 'required',
                'pickup_location' => 'required',
                'pickup_address' => 'required',
                'account_id' => 'required',
                'pickup_type' => 'required',
                'pickup_source' => 'required',
                'pickup_customer_name' => 'required',
                'pickup_customer_email' => 'required',
                'pickup_customer_phone' => 'required',
            ], [
                'branch_id.required' => 'Branch diperlukan.',
                'pickup_time.required' => 'Pickup time diperlukan.',
                'pickup_location.required' => 'Pickup location diperlukan.',
                'pickup_address.required' => 'Pickup address diperlukan.',
                'account_id.required' => 'Account diperlukan.',
                'pickup_type.required' => 'Pickup type diperlukan.',
                'pickup_source.required' => 'Pickup source diperlukan.',
                'pickup_customer_name.required' => 'Customer name diperlukan.',
                'pickup_customer_email.required' => 'Customer email diperlukan.',
                'pickup_customer_phone.required' => 'Customer phone diperlukan.',
            ]);

            $account = Account::where('id', $account_id)->first();

            $pickup = new Pickup();
            $pickup->id = CommonPopExpressHelper::generatePickupId();
            $pickup->branch_id = $branch_id;
            $pickup->pickup_time = $pickup_time;
            $pickup->pickup_location = $pickup_location;
            $pickup->pickup_address = $pickup_address;
            $pickup->customer_id = $account->customer_id;
            $pickup->account_id = $account_id;
            $pickup->pickup_type = $pickup_type;
            $pickup->pickup_source = $pickup_source;
            $pickup->pickup_status = 'requested';
            $pickup->pickup_alternative_address = $pickup_alternative_address;
            $pickup->pickup_remarks = $pickup_remarks;
            $pickup->pickup_customer_name = $pickup_customer_name;
            $pickup->pickup_customer_email = $pickup_customer_email;
            $pickup->pickup_customer_phone = $pickup_customer_phone;
            $pickup->expected_total_items = (empty($expected_total_items) ? 0 : $expected_total_items);
            $pickup->pickup_no = CommonPopExpressHelper::generatePickupNo();
            $pickup->server_timestamp = date('Y-m-d H:i:s');

            if($pickup->save()) {
                $data = $pickup->getAttributes();
                $key = $pickup->id;
                $module = 'pickups';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data pickup telah berhasil disimpan.');

        } else {

            $this->validate(request(), [
                'branch_id' => 'required',
                'pickup_time' => 'required',
                'pickup_location' => 'required',
                'pickup_address' => 'required',
                'account_id' => 'required',
                'pickup_type' => 'required',
                'pickup_source' => 'required',
                'pickup_customer_name' => 'required',
                'pickup_customer_email' => 'required',
                'pickup_customer_phone' => 'required',
            ], [
                'branch_id.required' => 'Branch diperlukan.',
                'pickup_time.required' => 'Pickup time diperlukan.',
                'pickup_location.required' => 'Pickup location diperlukan.',
                'pickup_address.required' => 'Pickup address diperlukan.',
                'account_id.required' => 'Account diperlukan.',
                'pickup_type.required' => 'Pickup type diperlukan.',
                'pickup_source.required' => 'Pickup source diperlukan.',
                'pickup_customer_name.required' => 'Customer name diperlukan.',
                'pickup_customer_email.required' => 'Customer email diperlukan.',
                'pickup_customer_phone.required' => 'Customer phone diperlukan.',
            ]);

            $account = Account::where('id', $account_id)->first();

            $pickup = Pickup::where('id', $id)->first();

            $pickup->branch_id = $branch_id;
            $pickup->pickup_time = $pickup_time;
            $pickup->pickup_location = $pickup_location;
            $pickup->pickup_address = $pickup_address;
            $pickup->customer_id = $account->customer_id;
            $pickup->account_id = $account_id;
            $pickup->pickup_type = $pickup_type;
            $pickup->pickup_source = $pickup_source;
            $pickup->pickup_latitude = $pickup_latitude;
            $pickup->pickup_longitude = $pickup_longitude;
            $pickup->pickup_status = $pickup_status;
            $pickup->pickup_alternative_address = $pickup_alternative_address;
            $pickup->pickup_remarks = $pickup_remarks;
            $pickup->pickup_customer_name = $pickup_customer_name;
            $pickup->pickup_customer_email = $pickup_customer_email;
            $pickup->pickup_customer_phone = $pickup_customer_phone;
            $pickup->expected_total_items = (empty($expected_total_items) ? 0 : $expected_total_items);
            $pickup->server_timestamp = date('Y-m-d H:i:s');

            if($pickup->save()) {
                $data = $pickup->getAttributes();
                $key = $id;
                $module = 'pickups';
                $type = 'edit';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data pickup telah berhasil disimpan.');

        }

        return redirect('/popexpress/pickups');
    }

    function getLocation(Request $request)
    {
        $type = request('type');

        if($type == "locker" || $type == "popsend-locker") {
            $is_locker = 1;
        } else {
            $is_locker = 0;
        }

        $locations = Destination::where('deleted_at', null)->where('is_locker', $is_locker)->orderBy('district')->get()->toArray();

        return response()->json($locations);

    }

    function view(Request $request, $id) {

        $pickup = Pickup::where('deleted_at', null)->select('pickups.id as pickupid', 'pickups.*')->where('id', $id)->first();
        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();
        $accounts = Account::where('deleted_at', null)->orderBy('account_name')->get();
        $types = ["non-cash", "cash", "locker", "popsend-address", "popsend-locker"];
        $times = ["09.00-18.00", "18.00-21.00", "all-day"];
        $sources = [ "web", "whatsapp", "line", "email", "sms", "phone", "api"];
        if($pickup->pickup_type == "locker" || $pickup->pickup_type == "popsend-locker") {
            $is_locker = 1;
        } else {
            $is_locker = 0;
        }
        $locations = Destination::where('deleted_at', null)->where('is_locker', $is_locker)->orderBy('district')->get();
        $statusesPickups = ['requested', 'assigned', 'picked', 'pending', 'processing', 'closed', 'canceled'];

        $awb = request('awb');
        $merchant_awb = request('merchant_awb');
        $third_party_awb = request('third_party_awb');
        $reseller = request('reseller');
        $recipient = request('recipient');
        $recipient_telephone = request('recipient_telephone');
        $recipient_email = request('recipient_email');
        $in_hub_date_start = request('hub_from');
        $in_hub_date_end = request('hub_to');
        $latest_status = request('latest_status');
        $hub_destination_id = request('hub_destination_id');
        $recipient_address = request('recipient_address');
        $postcode = request('postcode');
        $in_hub = request('in_hub');

        $start = substr($in_hub_date_start, 6, 4).'-'.substr($in_hub_date_start, 3, 2).'-'.substr($in_hub_date_start, 0, 2);
        $end = substr($in_hub_date_end, 6, 4).'-'.substr($in_hub_date_end, 3, 2).'-'.substr($in_hub_date_end, 0, 2);


        $pickupDetails = PickupDetail::leftJoin('pickups', 'pickups.id', '=', 'pickup_details.pickup_id')
            ->leftJoin(DB::raw('destinations as hub'), 'hub.id', '=', 'pickup_details.hub_destination_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'pickup_details.web_destination_id')
            ->leftJoin('branches', 'branches.id', '=', 'pickup_details.branch_id')
            ->where('pickup_details.deleted_at', null)
            ->where('pickup_details.pickup_id', $id)
            ->select(
                'pickup_details.*',
                'pickup_details.id as pickupid',
                'pickup_details.pickup_id as idpickup',
                'pickups.pickup_no as pickup_no',
                DB::raw("IF(pickup_details.in_hub = 0, CONCAT(destinations.detail_code, ' - ', destinations.district, ', ', destinations.county), CONCAT(hub.detail_code, ' - ', hub.district, ', ', hub.county)) AS destination"),
                DB::raw("DATE_FORMAT(pickup_details.in_hub_date, '%d %b %Y') AS hub_date")
            )
            ->when($awb, function ($query) use ($awb) {
                return $query->where('pickup_details.awb', 'LIKE', "%$awb%");
            })
            ->when($merchant_awb, function ($query) use ($merchant_awb) {
                return $query->where('pickup_details.merchant_awb', 'LIKE', "%$merchant_awb%");
            })
            ->when($third_party_awb, function ($query) use ($third_party_awb) {
                return $query->where('pickup_details.third_party_awb', 'LIKE',"%$third_party_awb%");
            })
            ->when($reseller, function ($query) use ($reseller) {
                return $query->where('pickup_details.reseller', 'LIKE', "%$reseller%");
            })
            ->when($recipient, function ($query) use ($recipient) {
                return $query->where('pickup_details.recipient', 'LIKE', "%$recipient%");
            })
            ->when($recipient_telephone, function ($query) use ($recipient_telephone) {
                return $query->where('pickup_details.recipient_telephone', 'LIKE', "%$recipient_telephone%");
            })
            ->when($recipient_email, function ($query) use ($recipient_email) {
                return $query->where('pickup_details.recipient_email', 'LIKE', "%$recipient_email%");
            })
            ->when($postcode, function ($query) use ($postcode) {
                return $query->where('pickup_details.postcode', 'LIKE', "%$postcode%");
            })
            ->when($recipient_address, function ($query) use ($recipient_address) {
                return $query->where('pickup_details.recipient_address', 'LIKE', "%$recipient_address%");
            })
            ->when($latest_status, function ($query) use ($latest_status) {
                return $query->where('pickups.pickup_status', '=', $latest_status);
            })
            ->when($hub_destination_id, function ($query) use ($hub_destination_id) {
                return $query->where('pickup_details.hub_destination_id', '=', $hub_destination_id);
            })
            ->when($in_hub != '', function ($query) use ($in_hub) {
                return $query->where('pickup_details.in_hub', '=', $in_hub);
            })
            ->when($in_hub_date_start, function ($query) use ($start) {
                return $query->whereDate('pickup_details.in_hub_date', '>=', $start);
            })
            ->when($in_hub_date_end, function ($query) use ($end) {
                return $query->whereDate('pickup_details.in_hub_date', '<=', $end);
            })
            ->orderBy('pickup_details.created_at', 'desc')
            ->get();

        $destinationsDetails = Destination::where('deleted_at', null)->orderBy('district')->get();


        $statusDetails = ['new' => 'success','in hub' => 'success','consolidate' => 'success','deconsolidate' => 'success','delivery' => 'success','void' => 'danger','done' => 'primary'];

        $pickups = Pickup::where('deleted_at', null)->whereIn('pickup_status', ['requested','assigned','pending','processing'])->select('id as pickupid', 'pickup_no')->orderBy('pickup_no')->get();
        $destinations = Destination::where('deleted_at', null)->orderBy('district')->get();
        $statuses = ['new' => 'success','in hub' => 'success','consolidate' => 'success','deconsolidate' => 'success','delivery' => 'success','void' => 'danger','done' => 'primary'];


        $pickup_no = request('pickup_no');
        $courier_id = request('courier_id');
        $created_from = request('created_from');
        $created_to = request('created_to');

        $start = substr($created_from, 6, 4).'-'.substr($created_from, 3, 2).'-'.substr($created_from, 0, 2);
        $end = substr($created_to, 6, 4).'-'.substr($created_to, 3, 2).'-'.substr($created_to, 0, 2);

        $pickupTasks = PickupTask::leftJoin('pickups', 'pickups.id', '=', 'pickup_tasks.pickup_id')
            ->leftJoin(DB::raw('employees as courier'), 'courier.id', '=', 'pickup_tasks.courier_id')
            ->leftJoin('employees', 'employees.id', '=', 'pickup_tasks.assignee_id')
            ->where('pickup_tasks.pickup_id', $id)
            ->where('pickup_tasks.deleted_at', null)
            ->select(
                'pickup_tasks.*',
                'pickup_tasks.id as pickuptaskid',
                'pickups.pickup_no as pickup_no',
                'pickups.pickup_status as pickup_status',
                'courier.user_id as courier_user_id',
                'employees.user_id as employee_user_id',
                DB::raw("DATE_FORMAT(pickup_tasks.start_date, '%d %b %Y %H:%m') AS start_date"),
                DB::raw("DATE_FORMAT(pickup_tasks.cancel_date, '%d %b %Y %H:%m') AS cancel_date"),
                DB::raw("DATE_FORMAT(pickup_tasks.pickup_date, '%d %b %Y %H:%m') AS pickup_date"),
                DB::raw("DATE_FORMAT(pickup_tasks.created_at, '%d %b %Y %H:%m') AS created")
            )
            ->when($pickup_no, function ($query) use ($pickup_no) {
                return $query->where('pickups.pickup_no', 'LIKE', "%$pickup_no%");
            })
            ->when($courier_id, function ($query) use ($courier_id) {
                return $query->where('courier.user_id', '=', $courier_id);
            })
            ->when($created_from, function ($query) use ($start) {
                return $query->whereDate('pickup_tasks.created_at', '>=', $start);
            })
            ->when($created_to, function ($query) use ($end) {
                return $query->whereDate('pickup_tasks.created_at', '<=', $end);
            })
            ->orderBy('pickup_tasks.created_at', 'desc')
            ->get();

        $getEmployees = Employee::where('deleted_at', null)->pluck('user_id')->toArray();
        $users = User::where('deleted_at', null)->whereIn('id', $getEmployees)->pluck('name', 'id')->toArray();
        $cancelTypes = ['unfulfillment' => 'danger','bad_address' => 'danger','customer_cancel'=> 'info','reassign'=> 'warning','not_available'=> 'success'];
        $cancelDatas = ['unfulfillment' => 'Tugas tidak bisa dipenuhi','bad_address' => 'Alamat tidak ditemukan','customer_cancel'=> 'Pelanggan membatalkan','reassign'=> 'Ditugaskan kembali','not_available'=> 'Pelanggan tidak ada'];
        $pickups = Pickup::where('deleted_at', null)->whereIn('pickup_status', ['requested','pending'])->select('id as pickupid', 'pickup_no')->orderBy('pickup_no')->get();
        $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->leftJoin('users', 'users.id', '=', 'user_groups.user_id')
            ->where('user_groups.deleted_at', null)
            ->whereIn('user_id', $getEmployees)
            ->where('groups.name', 'popexpress_courier')
            ->pluck('users.name', 'users.id')
            ->toArray();

        return view('popexpress.pickups.view', compact('pickup', 'branches', 'accounts', 'types', 'times', 'sources', 'locations', 'pickup', 'statusesPickups', 'pickupDetails', 'statusDetails', 'pickupTasks', 'users', 'cancelTypes', 'pickups', 'userGroups', 'cancelDatas', 'pickups', 'destinations', 'statuses', 'destinationsDetails'));

    }

    function getCustomer(Request $request) {

        $account = request('account');

        $customer = Account::leftJoin('customers', 'customers.id', '=', 'accounts.customer_id')->where('accounts.id', $account)->select('customers.id', 'customers.user_id')->first();

        $user = [];
        if(!is_null($customer)) {
            $user = User::where('id', $customer->user_id)->select('name', 'email', 'phone')->first()->toArray();
        }

        return response()->json($user);


    }

}