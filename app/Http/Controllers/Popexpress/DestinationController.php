<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popbox\LockerLocation;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\BranchPickup;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Origin;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;

class DestinationController extends Controller {

    public function index(Request $request)
    {
        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $is_locker = request('is_locker');
        $locker_id = request('locker_id');
        $county = request('county');
        $district = request('district');

        $destinations = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->where('destinations.deleted_at', null)
            ->select('destinations.*', 'origins.name as origin_name')
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('destinations.origin_id', '=', $origin_id);
            })
            ->when($locker_id, function ($query) use ($locker_id) {
                return $query->where('destinations.locker_id', '=', $locker_id);
            })
            ->when($is_locker != '', function ($query) use ($is_locker) {
                return $query->where('destinations.is_locker', '=', $is_locker);
            })
            ->when($airport_code, function ($query) use ($airport_code) {
                return $query->where('destinations.airport_code', 'LIKE', "%$airport_code%");
            })
            ->when($detail_code, function ($query) use ($detail_code) {
                return $query->where('destinations.detail_code', 'LIKE', "%$detail_code%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('destinations.province', 'LIKE', "%$province%");
            })
            ->when($county, function ($query) use ($county) {
                return $query->where('destinations.county', 'LIKE', "%$county%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('destinations.district', 'LIKE', "%$district%");
            })
            ->paginate(20);

        $lockers = [1 => 'yes', 0 => 'no'];
        $origins = Origin::where('deleted_at', null)->get();

        $lockerNames = LockerLocation::where('locker_locations.name', 'NOT LIKE', '%PopBox @%')
            ->select(
                DB::raw('locker_locations.locker_id'),
                DB::raw("CONCAT('Popbox @ ',locker_locations.name) AS name")
            )
            ->get()->toArray();

        $lockerNames = array_map(function($locker) {
            return array(
                'id' => $locker['locker_id'],
                'name' => $locker['name']
            );
        }, $lockerNames);

        $ArrLockerNames = json_encode($lockerNames);

        return view('popexpress.destinations.index', compact('destinations', 'lockers', 'origins', 'ArrLockerNames'));
    }

    public function store(Request $request)
    {
        $id = request('id');
        $delete = request('delete');
        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $is_locker = request('is_locker');
        $locker_name = request('locker_name');
        $locker_id = request('locker_id');
        $county = request('county');
        $district = request('district');


        if(empty($id)) {
            $this->validate(request(), [
                'airport_code' => 'required',
                'province' => 'required',
                'is_locker' => 'required',
                'county' => 'required',
                'district' => 'required'
            ],[
                'airport_code.required' => 'Airport code diperlukan.',
                'province.required' => 'Province diperlukan.',
                'is_locker.required' => 'Locker diperlukan.',
                'county.required' => 'County diperlukan.',
                'district.required' => 'District diperlukan.',
            ]);

            if(!empty($locker_id)) {
                if(Destination::where('locker_id', $locker_id)->first()){
                    return redirect('/popexpress/destinations')->withInput()->withErrors([
                        'message' => 'Locker Name sudah terdaftar.'
                    ]);
                }
            }

            if($is_locker == 0) {
                if(empty($detail_code)) {
                    return redirect('/popexpress/destinations')->withInput()->withErrors([
                        'message' => 'Detail code diperlukan.'
                    ]);
                }
            }

            if(Destination::where('detail_code', $detail_code)->first()){
                return redirect('/popexpress/destinations')->withInput()->withErrors([
                    'message' => 'Detail code sudah terdaftar.'
                ]);
            }

            $dataDestination = [
                'origin_id' => $origin_id,
                'airport_code' => $airport_code,
                'detail_code' => ($is_locker == 1 ? $locker_name : $detail_code),
                'province' => $province,
                'is_locker' => $is_locker,
                'locker_id' => ($is_locker == 1 ? $locker_id : null),
                'county' => $county,
                'district' => $district,
                'server_timestamp' => date('Y-m-d H:i:s')
            ];

            $destination = Destination::create($dataDestination);

            if($destination) {
                $data = $destination->getAttributes();
                $key = $destination->id;
                $module = 'destinations';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data destination telah berhasil disimpan.');
        } else {
            $destination = Destination::where('id', '=', $id)->first();
            $old = $destination->getAttributes();

            if($delete == '1') {
                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                $deleteOrigin = Destination::where('deleted_at', null)->where('id', $id)->update($dataDelete);
                if($deleteOrigin){
                    $key = $id;
                    $module = 'destinations';
                    $type = 'delete';
                    $jsonBefore = json_encode($old);
                    $jsonAfter = json_encode($dataDelete);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data telah dihapus.');
                }
            } else {
                $this->validate(request(), [
                    'airport_code' => 'required',
                    'province' => 'required',
                    'is_locker' => 'required',
                    'county' => 'required',
                    'district' => 'required'
                ],[
                    'airport_code.required' => 'Airport code diperlukan.',
                    'province.required' => 'Province diperlukan.',
                    'is_locker.required' => 'Locker diperlukan.',
                    'county.required' => 'County diperlukan.',
                    'district.required' => 'District diperlukan.',
                ]);

                if(!empty($locker_id)) {
                    if(Destination::where('locker_id', $locker_id)->where('id', '!=', $id)->first()){
                        return redirect('/popexpress/destinations')->withInput()->withErrors([
                            'message' => 'Locker Name sudah terdaftar.'
                        ]);
                    }
                }

                if($is_locker == 0) {
                    if(empty($detail_code)) {
                        return redirect('/popexpress/destinations')->withInput()->withErrors([
                            'message' => 'Detail code diperlukan.'
                        ]);
                    }
                }

                if(Destination::where('detail_code', $detail_code)->where('id', '!=', $id)->first()){
                    return redirect('/popexpress/destinations')->withInput()->withErrors([
                        'message' => 'Detail code sudah terdaftar.'
                    ]);
                }

                $destination->origin_id = $origin_id;
                $destination->airport_code = $airport_code;
                $destination->detail_code = ($is_locker == 1 ? $locker_name : $detail_code);
                $destination->province = $province;
                $destination->is_locker = $is_locker;
                $destination->locker_id = ($is_locker == 1 ? $locker_id : null);
                $destination->county = $county;
                $destination->district = $district;
                $destination->server_timestamp = date("Y-m-d H:i:s");
                $destination->save();
                $new = $destination->getAttributes();
                $diff = array_diff_assoc($old, $new);

                if($destination) {
                    $key = $id;
                    $module = 'destinations';
                    $type = 'edit';
                    $diffBefore = array_diff_assoc($old, $new);
                    $diffAfter = array_diff_assoc($new, $old);
                    $jsonBefore = json_encode($diffBefore);
                    $jsonAfter = json_encode($diffAfter);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                }
                session()->flash('success', 'Data destination telah berhasil diperbarui.');
            }
        }

        return redirect('/popexpress/destinations');
    }

    public function export(Request $request)
    {
        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $is_locker = request('is_locker');
        $locker_id = request('locker_id');
        $county = request('county');
        $district = request('district');

        $destinations = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->where('destinations.deleted_at', null)
            ->select('destinations.*', 'origins.name as origin_name')
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('destinations.origin_id', '=', $origin_id);
            })
            ->when($locker_id, function ($query) use ($locker_id) {
                return $query->where('destinations.locker_id', '=', $locker_id);
            })
            ->when($is_locker != '', function ($query) use ($is_locker) {
                return $query->where('destinations.is_locker', '=', $is_locker);
            })
            ->when($airport_code, function ($query) use ($airport_code) {
                return $query->where('destinations.airport_code', 'LIKE', "%$airport_code%");
            })
            ->when($detail_code, function ($query) use ($detail_code) {
                return $query->where('destinations.detail_code', 'LIKE', "%$detail_code%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('destinations.province', 'LIKE', "%$province%");
            })
            ->when($county, function ($query) use ($county) {
                return $query->where('destinations.county', 'LIKE', "%$county%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('destinations.district', 'LIKE', "%$district%");
            })->get();

        $filename = "destinations_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($destinations){
            $excel->sheet('Sheet1', function($sheet) use($destinations){
                $row = 1;

                $arr_title = ['origin','airport_code','detail_code','province','county','district','latitude','longitude'];
                $sheet->row($row, $arr_title);

                foreach ( $destinations as $index => $item) {
                    $row++;
                    $arr = [
                        $item->origin_name,
                        $item->airport_code,
                        $item->detail_code,
                        $item->province,
                        $item->county,
                        $item->district,
                        $item->latitude,
                        $item->longitude
                    ];
                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function exportLocker(Request $request)
    {

        $lokerLists = LockerLocation::pluck('locker_id')->toArray();
        $getDestinations = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->select('destinations.*', 'origins.name as origin_name')
            ->whereIn('locker_id', $lokerLists)
            ->where('is_locker', 1)
            ->get();


        $destinations = [];
        foreach ($getDestinations as $getDestination) {
            $destinations[$getDestination->locker_id] = [
                'origin_name' => $getDestination->origin_name,
                'airport_code' => $getDestination->airport_code,
                'detail_code' => $getDestination->detail_code,
                'province' => $getDestination->province,
                'county' => $getDestination->county,
                'district' => $getDestination->district,
                'latitude' => $getDestination->latitude,
                'longitude' => $getDestination->longitude,
            ];
        }

        $lockerNames = LockerLocation::where('locker_locations.name', 'NOT LIKE', '%PopBox @%')
            ->select(
                DB::raw('locker_id'),
                DB::raw('name'),
                DB::raw('address'),
                DB::raw('address_2')
            )
            ->orderBy('locker_locations.name')
            ->get();

        $filename = "popbox_locker_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($lockerNames, $destinations){
            $excel->sheet('Sheet1', function($sheet) use($lockerNames, $destinations){
                $row = 1;

                $arr_title = ['popbox_locker_id','popbox_name','popbox_address','popbox_address_2','origin','airport_code','detail_code','province','county','district','latitude','longitude'];
                $sheet->row($row, $arr_title);

                foreach ( $lockerNames as $index => $item) {
                    $row++;
                    $arr = [
                        $item->locker_id,
                        $item->name,
                        $item->address,
                        $item->address_2,
                        isset($destinations[$item->locker_id]['origin_name']) ? $destinations[$item->locker_id]['origin_name'] : '',
                        isset($destinations[$item->locker_id]['airport_code']) ? $destinations[$item->locker_id]['airport_code'] : '',
                        isset($destinations[$item->locker_id]['detail_code']) ? $destinations[$item->locker_id]['detail_code'] : '',
                        isset($destinations[$item->locker_id]['province']) ? $destinations[$item->locker_id]['province'] : '',
                        isset($destinations[$item->locker_id]['county']) ? $destinations[$item->locker_id]['county'] : '',
                        isset($destinations[$item->locker_id]['district']) ? $destinations[$item->locker_id]['district'] : '',
                        isset($destinations[$item->locker_id]['latitude']) ? $destinations[$item->locker_id]['latitude'] : '',
                        isset($destinations[$item->locker_id]['longitude']) ? $destinations[$item->locker_id]['longitude'] : '',
                    ];
                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function template(Request $request)
    {
        $destinations = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->where('destinations.detail_code', 'NOT LIKE', '%PopBox @%')
            ->where('destinations.deleted_at', null)
            ->select('destinations.*', 'origins.name as origin_name')->take(10)->get();

        $filename = "destinations_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($destinations){
            $excel->sheet('Sheet1', function($sheet) use($destinations){
                $row = 1;

                $arr_title = ['origin','airport_code','detail_code','province','county','district','latitude','longitude'];
                $sheet->row($row, $arr_title);

                foreach ( $destinations as $index => $item) {
                    $row++;
                    $arr = [
                        $item->origin_name,
                        $item->airport_code,
                        $item->detail_code,
                        $item->province,
                        $item->county,
                        $item->district,
                        $item->latitude,
                        $item->longitude
                    ];
                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function download(Request $request)
    {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            echo "File not found";
        }
    }

    public function import()
    {
        return view('popexpress.destinations.import');
    }

    public function importData(Request $request){

        $file = request('file_upload');

        $fileExt = $file->getClientOriginalName();
        $arr_ext = explode('.', $fileExt);
        $real_ext = $arr_ext[count($arr_ext)-1];

        if($file) {
            if (!in_array($real_ext, array("xlsx", "xls"))) {
                return redirect('/popexpress/destinations/import')->withInput()->withErrors([
                    'message' => 'Hanya excel file .xls & .xlsx yang diperbolehkan untuk diupload.'
                ]);
            }

            $records = Excel::load($file->path())->get()->toArray();

            $origins = Origin::where('deleted_at', null)->select('name', 'id')->get();
            $originValues = [];
            $originList = [];
            foreach ($origins as $origin) {
                $originValues[$origin->id] = strtoupper($origin->name);
                $originList[] = strtoupper($origin->name);
            }

            $total = count($records);
            $new = 0;
            $update = 0;
            $skip = 0;
            $row = '';
            foreach ($records as $key => $record)
            {
                $number = $key+1;
                if(empty($record['detail_code']) || empty($record['airport_code']) || empty($record['province']) || empty($record['county']) || empty($record['district'])) {
                    $row .= ($row == '' ? $number : ', '.$number);
                    $skip++;
                    continue;
                }

                if (strpos($record['detail_code'], 'Popbox @') !== false) {
                    $row .= ($row == '' ? $number : ', '.$number);
                    $skip++;
                    continue;
                }

                if(!in_array(strtoupper($record['origin']), $originList) && !is_null($record['origin'])) {
                    $row .= ($row == '' ? $number : ', '.$number);
                    $skip++;
                    continue;
                }

                $checkDestination = Destination::where('detail_code', $record['detail_code'])->count();
                $origin_id = array_search(strtoupper($record['origin']), $originValues);

                if($checkDestination > 0) {
                    $updateDestination = Destination::where('detail_code', $record['detail_code'])
                        ->update(
                            [
                                'origin_id' => ($origin_id !== false ? $origin_id : null),
                                'airport_code' => $record['airport_code'],
                                'province' => $record['province'],
                                'county' => $record['county'],
                                'district' => $record['district'],
                                'latitude' => $record['latitude'],
                                'longitude' => $record['longitude'],
                                'server_timestamp' => date('Y-m-d H:i:s')
                            ]
                        );
                    $update++;
                } else {
                    $dataDestination = [
                        'origin_id' => ($origin_id !== false ? $origin_id : null),
                        'airport_code' => $record['airport_code'],
                        'detail_code' => $record['detail_code'],
                        'province' => $record['province'],
                        'county' => $record['county'],
                        'district' => $record['district'],
                        'latitude' => $record['latitude'],
                        'longitude' => $record['longitude'],
                        'server_timestamp' => date('Y-m-d H:i:s')
                    ];
                    $destination = Destination::create($dataDestination);
                    $new++;
                }
            }

            $data['total'] = $total;
            $data['new'] = $new;
            $data['update'] = $update;
            $data['skip'] = $skip;
            $data['row'] = $row;

            if($total > 0) {
                $key = ' ';
                $module = 'destinations';
                $type = 'general';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }

            return view('popexpress.destinations.import', $data);

        }

    }

    public function detail($destinationId)
    {
        $destination = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->select('destinations.*', 'origins.name as origin_name')
            ->where('destinations.id', $destinationId)
            ->whereNull('destinations.deleted_at')
            ->first();

        $branchPickups = BranchPickup::leftJoin('branches', 'branches.id', '=', 'branches_pickups.branch_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'branches.location')
            ->where('branches_pickups.deleted_at', null)
            ->where('branches_pickups.destination_id', '=', $destinationId)
            ->select(
                'branches_pickups.*',
                'branches_pickups.id as branches_pickup_id',
                'branches.*',
                DB::raw("CONCAT(branches.code, ' - ', branches.name) as branch_name"),
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS description")
            )
            ->orderBy('branches_pickups.branch_id')
            ->get();

        $branches = Branch::where('deleted_at', null)->get();
        $origins = Origin::where('deleted_at', null)->get();

        $lockerNames = LockerLocation::where('locker_locations.name', 'NOT LIKE', '%PopBox @%')
            ->select(
                DB::raw('locker_locations.locker_id'),
                DB::raw("CONCAT('Popbox @ ',locker_locations.name) AS name")
            )
            ->get()->toArray();

        $lockerNames = array_map(function($locker) {
            return array(
                'id' => $locker['locker_id'],
                'name' => $locker['name']
            );
        }, $lockerNames);

        $ArrLockerNames = json_encode($lockerNames);

        if($destination){
            if(!$destination->latitude){
                $destination->latitude = 0;
            }
            if(!$destination->longitude){
                $destination->longitude = 0;
            }

            $place = str_replace(" ", "+", $destination->district.', '.$destination->county.', '.$destination->province);

            return view('popexpress.destinations.detail', compact('destination', 'place', 'branchPickups', 'branches', 'origins', 'ArrLockerNames'));
        }else{
            return abort(404);
        }
    }

    public function addBranchPickup(Request $request)
    {

        $branch_id = request('branch_id');
        $destination_id = request('destination_id');

        $return = false;
        $check = BranchPickup::where('deleted_at', null)->where('branch_id', $branch_id)->where('destination_id', $destination_id)->first();
        if(is_null($check)) {
            $branchPickup = BranchPickup::create([
                'branch_id' => $branch_id,
                'destination_id' => $destination_id,
                'server_timestamp' => date('Y-m-d H:i:s')
            ]);

            if($branchPickup) {
                $data = $branchPickup->getAttributes();
                $key = $branchPickup->id;
                $module = 'branches_pickups';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);

                $return = true;
            }
        }

        return response()->json($return);

    }

    public function deleteBranchPickup(Request $request)
    {

        $id = request('id');
        $return = false;

        $branchPickup = BranchPickup::where('id', '=', $id)->first();
        $old = $branchPickup->getAttributes();
        $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
        $deleteBranch = BranchPickup::where('deleted_at', null)->where('id', $id)->update($dataDelete);
        if($deleteBranch){
            $key = $id;
            $module = 'branches_pickups';
            $type = 'delete';
            $jsonBefore = json_encode($old);
            $jsonAfter = json_encode($dataDelete);
            $remark = null;
            CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            $return = true;
        }

        return response()->json($return);

    }

    public function load(Request $request)
    {
        $destinationId = request('destination_id');

        $branchPickups = BranchPickup::leftJoin('branches', 'branches.id', '=', 'branches_pickups.branch_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'branches.location')
            ->where('branches_pickups.deleted_at', null)
            ->where('branches_pickups.destination_id', '=', $destinationId)
            ->select(
                'branches_pickups.*',
                'branches_pickups.id as branches_pickup_id',
                'branches.*',
                DB::raw("CONCAT(branches.code, ' - ', branches.name) as branch_name"),
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS description")
            )
            ->orderBy('branches_pickups.branch_id')
            ->get()
            ->toArray();

        return response()->json($branchPickups);

    }

    public function updateDestination(Request $request)
    {

        $id = request('id');
        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $is_locker = request('is_locker');
        $locker_name = request('locker_name');
        $locker_id = request('locker_id');
        $county = request('county');
        $district = request('district');
        $latitude = request('latitude');
        $longitude = request('longitude');

        if(!empty($id)) {
            $this->validate(request(), [
                'airport_code' => 'required',
                'province' => 'required',
                'is_locker' => 'required',
                'county' => 'required',
                'district' => 'required'
            ],[
                'airport_code.required' => 'Airport code diperlukan.',
                'province.required' => 'Province diperlukan.',
                'is_locker.required' => 'Locker diperlukan.',
                'county.required' => 'County diperlukan.',
                'district.required' => 'District diperlukan.',
            ]);

            if(!empty($locker_id)) {
                if(Destination::where('locker_id', $locker_id)->where('id', '!=', $id)->first()){
                    $result = [
                        'status' => 'error',
                        'message' => 'Locker Name sudah terdaftar.'
                    ];
                    return response()->json($result);
                }
            }

            if($is_locker == 0) {
                if(empty($detail_code)) {
                    $result = [
                        'status' => 'error',
                        'message' => 'Detail code diperlukan.'
                    ];
                    return response()->json($result);
                }
            }

            if(Destination::where('detail_code', $detail_code)->where('id', '!=', $id)->first()){
                $result = [
                    'status' => 'error',
                    'message' => 'Detail code sudah terdaftar.'
                ];
                return response()->json($result);
            }

            $destination = Destination::where('id', '=', $id)->first();
            $old = $destination->getAttributes();

            $destination->origin_id = $origin_id;
            $destination->airport_code = $airport_code;
            $destination->detail_code = ($is_locker == 1 ? $locker_name : $detail_code);
            $destination->province = $province;
            $destination->is_locker = $is_locker;
            $destination->locker_id = ($is_locker == 1 ? $locker_id : null);
            $destination->county = $county;
            $destination->district = $district;
            $destination->latitude = $latitude;
            $destination->longitude = $longitude;
            $destination->server_timestamp = date("Y-m-d H:i:s");
            $destination->save();
            $new = $destination->getAttributes();
            $diff = array_diff_assoc($old, $new);

            if($destination) {
                $key = $id;
                $module = 'destinations';
                $type = 'edit';
                $diffBefore = array_diff_assoc($old, $new);
                $diffAfter = array_diff_assoc($new, $old);
                $jsonBefore = json_encode($diffBefore);
                $jsonAfter = json_encode($diffAfter);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }

            $origin = Origin::where('id', $destination->origin_id)->select('name')->first();

            $result = [
                'status' => 'success',
                'message' => 'success',
                'data' => [
                    'origin_id' => $destination->origin_id,
                    'origin' => (isset($origin->name) ? $origin->name : ''),
                    'airport_code' => $destination->airport_code,
                    'detail_code' => $destination->detail_code,
                    'province' => $destination->province,
                    'is_locker' => $destination->is_locker,
                    'county' => $destination->county,
                    'district' => $destination->district,
                    'latitude' => is_null($destination->latitude) ? 0 : $destination->latitude,
                    'longitude' => is_null($destination->longitude) ? 0 : $destination->longitude,
                ]
            ];

        } else {
            $result = [
                'status' => 'error',
                'message' => 'ID kosong.'
            ];
        }

        return response()->json($result);

    }

}