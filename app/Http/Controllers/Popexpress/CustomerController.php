<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Group;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\Customer;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Employee;
use App\Models\UserGroup;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller {

    protected $accessAdd = false;
    protected $accessExport = false;
    protected $allowedGroups = [
        'add' => ['popexpress_admin', 'popexpress_finance'],
        'export' => ['popexpress_admin', 'popexpress_finance', 'popexpress_operation_head', 'popexpress_sales'],
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userid = Auth::user()->id;
            $allowed = $this->allowedGroups['add'];
            $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                ->where('user_groups.deleted_at', null)
                ->where('user_groups.user_id', $userid)
                ->where(function($query) use ($allowed){
                    foreach ($allowed as $allow) {
                        $query->orWhere('groups.name', '=', $allow);
                    }
                })
                ->first();
            $this->accessAdd = (!is_null($userGroups) ? true : false);

            $allowedExports = $this->allowedGroups['export'];
            $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                ->where('user_groups.deleted_at', null)
                ->where('user_groups.user_id', $userid)
                ->where(function($query) use ($allowedExports){
                    foreach ($allowedExports as $allow) {
                        $query->orWhere('groups.name', '=', $allow);
                    }
                })
                ->first();
            $this->accessExport = (!is_null($userGroups) ? true : false);

            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $code = request('code');
        $email = request('email');
        $name = request('name');
        $phone = request('phone');
        $sales = request('sales');
        $type = request('customer_type');
        $suspend = request('suspend');

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
                    ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                    ->where('groups.name', 'popexpress_sales')
                    ->where('user_groups.deleted_at', null)
                    ->pluck('users.name', 'users.id')
                    ->toArray();

        $suspends = [0 => 'No', 1 => 'Yes'];
        $types = [ 'social_seller', 'corporate', 'marketplace'];

        $getCustomers = Customer::when($sales, function ($query) use ($sales) {
                return $query->where('customers.sales', '=', $sales);
            })
            ->when($type, function ($query) use ($type) {
                return $query->where('customers.customer_type', '=', $type);
            })
            ->when($suspend, function ($query) use ($suspend) {
                return $query->where('customers.suspend', '=', $suspend);
            })
            ->when($code, function ($query) use ($code) {
                return $query->where('customers.code', 'LIKE', "%$code%");
            })
            ->pluck('user_id')->toArray();

        $getUsers = User::whereIn('users.id', $getCustomers)
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('users.name', 'LIKE', "%$name%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.username',
                'users.phone',
                'users.email'
            )
            ->get();

        $users = [];
        $userlist = [];

        foreach ($getUsers as $getUser) {

            $users[$getUser->report_users_id] = [
                'report_users_id' => $getUser->report_users_id,
                'name' => $getUser->name,
                'username' => $getUser->username,
                'phone' => $getUser->phone,
                'email' => $getUser->email
            ];
            $userlist[] = $getUser->report_users_id;

        }

        $customers = Customer::leftjoin('employees', 'employees.id', '=', 'customers.sales')
            ->where('customers.deleted_at', null)
            ->select(
                DB::raw('customers.user_id as report_users_id'),
                DB::raw('customers.id as customers_id'),
                DB::raw('customers.code as code'),
                DB::raw("employees.user_id as employees_user_id"),
                DB::raw("customers.credit_limit as credit_limit"),
                DB::raw("IF(customers.suspend = 1, '(SUSPEND)', '') as is_suspend")
            )
            ->whereIn('customers.user_id', $userlist)
            ->orderBy('customers.code')
            ->paginate(20);

        $accessAdd = $this->accessAdd;
        $accessReport = $this->accessExport;

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->where('groups.name', 'popexpress_sales')
            ->where('user_groups.deleted_at', null)
            ->where('users.deleted_at', null)
            ->pluck('users.name', 'users.id')
            ->toArray();

        $listSales = [];
        foreach ($getSales as $key => $getSale) {
            $listSales[] = $key;
        }

        $salesEmployees = Employee::whereIn('user_id', $listSales)
            ->where('deleted_at', null)
            ->select('user_id', 'id')
            ->get();

        $sales = [];
        foreach ($salesEmployees as $salesEmployee) {
            $sales[] = [
                'id' => $salesEmployee->id,
                'name' => $getSales[$salesEmployee->user_id]
            ];
        }

        $types = ['social_seller' => 'Social Seller', 'corporate' => 'Corporate', 'marketplace' => 'Marketplace'];

        return view('popexpress.customers.index', compact('customers', 'accessAdd', 'accessReport', 'users', 'getSales', 'sales', 'types'));
    }

    public function create()
    {
        if(!$this->accessAdd) {
            abort('404');
        }
        $statuses = ['disable', 'enable'];
        $types = ['social_seller' => 'Social Seller', 'corporate' => 'Corporate', 'marketplace' => 'Marketplace'];
        $top = [1, 7, 14, 30];

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->where('groups.name', 'popexpress_sales')
            ->where('user_groups.deleted_at', null)
            ->where('users.deleted_at', null)
            ->pluck('users.name', 'users.id')
            ->toArray();

        $listSales = [];
        foreach ($getSales as $key => $getSale) {
            $listSales[] = $key;
        }

        $salesEmployees = Employee::whereIn('user_id', $listSales)
            ->where('deleted_at', null)
            ->select('user_id', 'id')
            ->get();

        $sales = [];
        foreach ($salesEmployees as $salesEmployee) {
            $sales[] = [
                'id' => $salesEmployee->id,
                'name' => $getSales[$salesEmployee->user_id]
            ];
        }

        $destinations = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
                        ->where('destinations.deleted_at', null)
                        ->select(
                            'destinations.id',
                            DB::raw("CONCAT(IF(origins.name is not null,origins.name, ''), ' - ', destinations.airport_code, ', ', destinations.airport_code, ', ', destinations.detail_code, ', ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS name")
                        )
                        ->get();

        return view('popexpress.customers.create', compact('statuses', 'sales', 'types', 'top', 'destinations'));
    }

    public function edit($id, $user_id)
    {
        $statuses = ['disable', 'enable'];
        $types = ['social_seller' => 'Social Seller', 'corporate' => 'Corporate', 'marketplace' => 'Marketplace'];
        $top = [1, 7, 14, 30];

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->where('groups.name', 'popexpress_sales')
            ->where('user_groups.deleted_at', null)
            ->where('users.deleted_at', null)
            ->pluck('users.name', 'users.id')
            ->toArray();

        $listSales = [];
        foreach ($getSales as $key => $getSale) {
            $listSales[] = $key;
        }

        $salesEmployees = Employee::whereIn('user_id', $listSales)
            ->where('deleted_at', null)
            ->select('user_id', 'id')
            ->get();

        $sales = [];
        foreach ($salesEmployees as $salesEmployee) {
            $sales[] = [
                'id' => $salesEmployee->id,
                'name' => $getSales[$salesEmployee->user_id]
            ];
        }

        $destinations = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->where('destinations.deleted_at', null)
            ->select(
                'destinations.id',
                DB::raw("CONCAT(IF(origins.name is not null,origins.name, ''), ' - ', destinations.airport_code, ', ', destinations.airport_code, ', ', destinations.detail_code, ', ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS name")
            )
            ->get();

        $accessAdd = $this->accessAdd;

        $customer = Customer::leftJoin('destinations', 'destinations.id', '=', 'customers.pickup_location')
                    ->leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
                    ->where('customers.deleted_at', null)
                    ->where('customers.id', $id)
                    ->select(
                        'customers.*',
                        DB::raw("CONCAT(origins.name, ' - ', destinations.airport_code, ', ', destinations.airport_code, ', ', destinations.detail_code, ', ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS destinations_name")
                    )
                    ->first();

        $user = User::where('deleted_at', null)->where('id', $user_id)->first();

        $join_date = date('d/m/Y', strtotime($customer->join_date));

        return view('popexpress.customers.edit', compact('statuses', 'sales', 'types', 'top', 'destinations', 'accessAdd', 'customer', 'user', 'join_date'));

    }

    public function store(Request $request)
    {
        if(!$this->accessAdd) {
            abort('404');
        }

        $id = request('id');
        $delete = request('delete');

        $code = request('code');
        $name = request('name');
        $email = request('email');
        $phone = request('phone');
        $username = request('username');
        $password = request('password');
        $status = request('status');
        $id_line = request('id_line');
        $id_instagram = request('id_instagram');
        $pickup_location = request('pickup_location');

        $pickup_latitude = request('pickup_latitude');
        $pickup_longitude = request('pickup_longitude');
        $terms_of_payment = request('terms_of_payment');
        $tax = request('tax');
        $join_date = request('join_date');
        $customer_type = request('customer_type');
        $billing_address = request('billing_address');
        $pickup_address = request('pickup_address');
        $sales = request('sales');
        $credit_limit = request('credit_limit');
        $suspend = request('suspend');
        $weight_rounding = request('weight_rounding');
        $remark = request('remark');
        $timestamp = date('Y-m-d H:i:s');

        $join_date = substr($join_date, 6, 4).'-'.substr($join_date, 3, 2).'-'.substr($join_date, 0, 2);


        if(empty($id)) {

            $this->validate(request(), [
                'code' => 'required',
                'name' => 'required',
                'password' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'terms_of_payment' => 'required',
                'join_date' => 'required',
                'billing_address' => 'required',
                'status' => 'required',
                'credit_limit' => 'required',
                'customer_type' => 'required',
            ],[
                'code.required' => 'Code diperlukan.',
                'name.required' => 'Name diperlukan.',
                'password.required' => 'Password diperlukan.',
                'email.required' => 'Email diperlukan.',
                'phone.required' => 'Phone diperlukan.',
                'terms_of_payment.required' => 'Terms of payment diperlukan.',
                'join_date.required' => 'Join date diperlukan.',
                'billing_address.required' => 'Billing Address diperlukan.',
                'status.required' => 'Status diperlukan.',
                'credit_limit.required' => 'Credit Limit diperlukan.',
                'customer_type.required' => 'Customer Type diperlukan.',
            ]);

            if(Customer::where('code', $code)->first()){
                return redirect('/popexpress/customers/create')->withInput()->withErrors([
                    'message' => 'Code sudah terdaftar.'
                ]);
            }

            if(User::where('email', $email)->first()){
                return redirect('/popexpress/customers/create')->withInput()->withErrors([
                    'message' => 'Email sudah terdaftar.'
                ]);
            }

            if(User::where('phone', $phone)->first()){
                return redirect('/popexpress/customers/create')->withInput()->withErrors([
                    'message' => 'Phone sudah terdaftar.'
                ]);
            }

            if(!empty($username)) {
                if(User::where('username', $username)->first()){
                    return redirect('/popexpress/customers/create')->withInput()->withErrors([
                        'message' => 'Username sudah terdaftar.'
                    ]);
                }
            }

            if(!empty($pickup_location)) {
                if(empty($pickup_address)) {
                    return redirect('/popexpress/customers/create')->withInput()->withErrors([
                        'message' => 'Pickup address harus terisi jika pickup location diisi. Sebaliknya juga.'
                    ]);
                }
            }

            if(!empty($pickup_address)) {
                if(empty($pickup_location)) {
                    return redirect('/popexpress/customers/create')->withInput()->withErrors([
                        'message' => 'Pickup location harus terisi jika pickup address diisi. Sebaliknya juga.'
                    ]);
                }
            }

            DB::beginTransaction();

            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->phone = $phone;
            $user->password = bcrypt($password);
            $user->status = $status;
            $user->group_id = 7;
            $user->company_id = 1;
            $user->username = $username;
            $user->popexpress_api_key = CommonPopExpressHelper::generateRandomString(10, false, true, true);
            $user->server_timestamp = $timestamp;
            $user->save();

            $user_id = $user->id;

            $group = Group::where('name', 'popexpress_customer')->first();
            $user_group = new UserGroup();
            $user_group->user_id = $user_id;
            $user_group->group_id = $group->id;
            $user_group->server_timestamp = $timestamp;
            $user_group->save();

            $customer = new Customer();
            $customer->code = $code;
            $customer->user_id = $user_id;
            $customer->tax = (is_null($tax) ? 0 : 1);
            $customer->id_line = $id_line;
            $customer->id_instagram = $id_instagram;
            $customer->pickup_location = $pickup_location;
            $customer->pickup_latitude = $pickup_latitude;
            $customer->pickup_longitude = $pickup_longitude;
            $customer->terms_of_payment = $terms_of_payment;
            $customer->join_date = $join_date;
            $customer->customer_type = $customer_type;
            $customer->billing_address = $billing_address;
            $customer->pickup_address = $pickup_address;
            $customer->sales = $sales;
            $customer->credit_limit = $credit_limit;
            $customer->suspend = (!empty($suspend) ? $suspend : 0);
            $customer->weight_rounding = $weight_rounding;
            $customer->remark = $remark;
            $customer->server_timestamp = $timestamp;
            $customer->save();

            if($customer->save()) {
                DB::commit();

                $data = $customer->getAttributes();
                $key = $customer->id;
                $module = 'customers';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            } else {
                DB::rollBack();
            }

            session()->flash('success', 'Data customer telah berhasil disimpan.');

        } else {

            $customer = Customer::where('id', '=', $id)->first();

            if($delete == '1') {
                DB::beginTransaction();

                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                $deleteCustomer = Customer::where('deleted_at', null)->where('id', $id)->update($dataDelete);

                $userGroups = UserGroup::where('user_id', $customer->user_id)->where('deleted_at', null)->get();

                UserGroup::where('deleted_at', null)->where('user_id', $customer->user_id)->update($dataDelete);


                $deleteUser = User::where('deleted_at', null)->where('id', $customer->user_id)->update($dataDelete);

                if($deleteUser){
                    DB::commit();
                    $key = $id;
                    $module = 'customers';
                    $type = 'delete';
                    $jsonBefore = json_encode($customer);
                    $jsonAfter = json_encode($dataDelete);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data telah dihapus.');
                } else {
                    DB::rollBack();
                }

            } else {

                $this->validate(request(), [
                    'code' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                    'phone' => 'required',
                    'terms_of_payment' => 'required',
                    'join_date' => 'required',
                    'billing_address' => 'required',
                    'status' => 'required',
                    'credit_limit' => 'required',
                    'customer_type' => 'required',
                ],[
                    'code.required' => 'Code diperlukan.',
                    'name.required' => 'Name diperlukan.',
                    'email.required' => 'Email diperlukan.',
                    'phone.required' => 'Phone diperlukan.',
                    'terms_of_payment.required' => 'Terms of payment diperlukan.',
                    'join_date.required' => 'Join date diperlukan.',
                    'billing_address.required' => 'Billing Address diperlukan.',
                    'status.required' => 'Status diperlukan.',
                    'credit_limit.required' => 'Credit Limit diperlukan.',
                    'customer_type.required' => 'Customer Type diperlukan.',
                ]);

                if(Customer::where('code', $code)->where('id', '!=', $id)->first()){
                    return redirect('/popexpress/customers/edit/'.$customer->id.'/'.$customer->user_id)->withInput()->withErrors([
                        'message' => 'Code sudah terdaftar.'
                    ]);
                }

                if(User::where('email', $email)->where('id', '!=', $customer->user_id)->first()){
                    return redirect('/popexpress/customers/edit/'.$customer->id.'/'.$customer->user_id)->withInput()->withErrors([
                        'message' => 'Email sudah terdaftar.'
                    ]);
                }

                if(User::where('phone', $phone)->where('id', '!=', $customer->user_id)->first()){
                    return redirect('/popexpress/customers/edit/'.$customer->id.'/'.$customer->user_id)->withInput()->withErrors([
                        'message' => 'Phone sudah terdaftar.'
                    ]);
                }

                if(!empty($username)) {
                    if(User::where('username', $username)->where('id', '!=', $customer->user_id)->first()){
                        return redirect('/popexpress/customers/edit/'.$customer->id.'/'.$customer->user_id)->withInput()->withErrors([
                            'message' => 'Username sudah terdaftar.'
                        ]);
                    }
                }

                if(!empty($pickup_location)) {
                    if(empty($pickup_address)) {
                        return redirect('/popexpress/customers/edit/'.$customer->id.'/'.$customer->user_id)->withInput()->withErrors([
                            'message' => 'Pickup address harus terisi jika pickup location diisi. Sebaliknya juga.'
                        ]);
                    }
                }

                if(!empty($pickup_address)) {
                    if(empty($pickup_location)) {
                        return redirect('/popexpress/customers/edit/'.$customer->id.'/'.$customer->user_id)->withInput()->withErrors([
                            'message' => 'Pickup location harus terisi jika pickup address diisi. Sebaliknya juga.'
                        ]);
                    }
                }

                DB::beginTransaction();

                $customer = Customer::where('id', $id)->first();
                $customer->code = $code;
                $customer->tax = (is_null($tax) ? 0 : 1);
                $customer->id_line = $id_line;
                $customer->id_instagram = $id_instagram;
                $customer->pickup_location = $pickup_location;
                $customer->pickup_latitude = $pickup_latitude;
                $customer->pickup_longitude = $pickup_longitude;
                $customer->terms_of_payment = $terms_of_payment;
                $customer->join_date = $join_date;
                $customer->customer_type = $customer_type;
                $customer->billing_address = $billing_address;
                $customer->pickup_address = $pickup_address;
                $customer->sales = $sales;
                $customer->credit_limit = $credit_limit;
                $customer->suspend = (!empty($suspend) ? $suspend : 0);
                $customer->weight_rounding = $weight_rounding;
                $customer->remark = $remark;
                $customer->server_timestamp = $timestamp;
                $customer->save();

                $user_id = $customer->user_id;

                $user = User::where('id', $user_id)->first();
                $user->name = $name;
                $user->email = $email;
                $user->phone = $phone;
                $user->status = $status;
                $user->username = $username;
                $user->server_timestamp = $timestamp;

                if($user->save()) {
                    DB::commit();
                    $key = $id;
                    $module = 'customers';
                    $type = 'edit';
                    $jsonBefore = json_encode($customer);
                    $jsonAfter = json_encode($customer);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data customer telah berhasil diperbarui.');
                } else {
                    DB::rollBack();
                    session()->flash('error', 'Data customer gagal diperbarui.');
                }
            }
        }

        return redirect('/popexpress/customers');

    }

    public function export(Request $request)
    {
        if(!$this->accessExport) {
            abort('404');
        }

        $code = request('code');
        $email = request('email');
        $name = request('name');
        $phone = request('phone');
        $sales = request('sales');
        $type = request('customer_type');
        $suspend = request('suspend');

        $getSales = User::leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
            ->where('groups.name', 'popexpress_sales')
            ->where('user_groups.deleted_at', null)
            ->pluck('users.name', 'users.id')
            ->toArray();

        $types = ['social_seller' => 'Social Seller', 'corporate' => 'Corporate', 'marketplace' => 'Marketplace'];

        $getCustomers = Customer::when($sales, function ($query) use ($sales) {
            return $query->where('customers.sales', '=', $sales);
        })
            ->when($type, function ($query) use ($type) {
                return $query->where('customers.customer_type', '=', $type);
            })
            ->when($suspend, function ($query) use ($suspend) {
                return $query->where('customers.suspend', '=', $suspend);
            })
            ->when($code, function ($query) use ($code) {
                return $query->where('customers.code', 'LIKE', "%$code%");
            })
            ->pluck('user_id')->toArray();

        $getUsers = User::whereIn('users.id', $getCustomers)
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('users.name', 'LIKE', "%$name%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.username',
                'users.phone',
                'users.email'
            )
            ->get();

        $users = [];
        $userlist = [];

        foreach ($getUsers as $getUser) {

            $users[$getUser->report_users_id] = [
                'report_users_id' => $getUser->report_users_id,
                'name' => $getUser->name,
                'username' => $getUser->username,
                'phone' => $getUser->phone,
                'email' => $getUser->email
            ];
            $userlist[] = $getUser->report_users_id;

        }

        $customers = Customer::leftjoin('employees', 'employees.id', '=', 'customers.sales')
            ->leftJoin('destinations', 'destinations.id', '=', 'customers.pickup_location')
            ->leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->where('customers.deleted_at', null)
            ->select(
                DB::raw('customers.user_id as report_users_id'),
                DB::raw('customers.id as customers_id'),
                DB::raw('customers.code as code'),
                DB::raw('customers.terms_of_payment as terms_of_payment'),
                DB::raw('customers.customer_type as customer_type'),
                DB::raw('customers.remark as remark'),
                DB::raw('customers.billing_address as billing_address'),
                DB::raw('customers.pickup_address as pickup_address'),
                DB::raw("employees.user_id as employees_user_id"),
                DB::raw("customers.credit_limit as credit_limit"),
                DB::raw("IF(customers.suspend = 1, 'Yes', 'No') as is_suspend"),
                DB::raw("CONCAT(origins.name, ' - ', destinations.airport_code, ', ', destinations.airport_code, ', ', destinations.detail_code, ', ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS destinations_name"),
                DB::raw("customers.weight_rounding")
            )
            ->whereIn('customers.user_id', $userlist)
            ->orderBy('customers.code')
            ->get();


        $filename = "customers_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($customers, $users, $getSales, $types){
            $excel->sheet('Sheet1', function($sheet) use($customers, $users, $getSales, $types){
                $row = 1;
                $arr_title = ['code','name','username','email','phone','credit_limit','suspend','terms_of_payment', 'sales','last_transaction','customer_type','remark','billing_address','pickup_address','pickup_location', 'weight_rounding'];

                $sheet->row($row, $arr_title);

                foreach ( $customers as $index => $item) {
                    $row++;
                    $arr = [
                        $item->code,
                        $users[$item->report_users_id]['name'],
                        $users[$item->report_users_id]['username'],
                        $users[$item->report_users_id]['email'],
                        $users[$item->report_users_id]['phone'],
                        intval($item->credit_limit),
                        $item->is_suspend,
                        $item->terms_of_payment,
                        isset($getSales[$item->employees_user_id]) ? $getSales[$item->employees_user_id] : '',
                        '',
                        $types[$item->customer_type],
                        $item->remark,
                        $item->billing_address,
                        $item->pickup_address,
                        $item->destinations_name,
                        $item->weight_rounding,
                    ];

                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function changePassword(Request $request)
    {
        if (!$this->accessAdd) {
            abort('404');
        }
        $id = request('id');
        $user_id = request('user_id');
        $password = request('password');
        $confirm_password = request('confirm_password');

        $this->validate(request(), [
            'password' => 'required',
            'confirm_password' => 'required'
        ],[
            'password.required' => 'New password diperlukan.',
            'confirm_password.required' => 'Confirm password diperlukan.'
        ]);

        if($password != $confirm_password) {
            return redirect('/popexpress/customers/edit/'.$id.'/'.$user_id)->withErrors([
                'message' => 'Password tidak sama.'
            ]);
        }

        $user = User::where('id', $id)->first();
        $user->password = bcrypt($password);
        if($user->save()) {
            $data = $user->getAttributes();
            $key = $id;
            $module = 'customers';
            $type = 'edit';
            $jsonBefore = null;
            $jsonAfter = json_encode($data);
            $remark = null;
            CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);

            session()->flash('success', 'Password berhasil diperbarui.');
            return redirect('/popexpress/customers/edit/'.$id);
        } else {
            session()->flash('error', 'Password gagal diperbarui.');
            return redirect('/popexpress/customers/edit/'.$id)->withInput()->withErrors([
                'message' => 'Password gagal diperbarui.'
            ]);
        }
    }

    public function transfer()
    {
        if(!$this->accessAdd) {
            abort('404');
        }

        $email = request('email');
        $phone = request('phone');

        $getCustomers = Customer::where('deleted_at', null)->pluck('user_id')->toArray();
        $getUsers = User::where('users.deleted_at', null)
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.phone',
                'users.email'
            )
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->paginate(20);

        return view('popexpress.customers.transfer', compact('getCustomers', 'getUsers'));

    }

    public function editTransfer($id)
    {
        if(!$this->access) {
            abort('404');
        }

        $user = User::where('users.deleted_at', null)
            ->where('users.id', $id)
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.phone',
                'users.email',
                DB::raw("(select GROUP_CONCAT(groups.name) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupuser"),
                DB::raw("(select GROUP_CONCAT(groups.id) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupiduser")
            )
            ->first();

        $customer = Customer::where('user_id', $id)->first();

        $groups = Group::select('id', 'name')->where('deleted_at', null)->where('name', 'LIKE', 'popexpress_%')->get();
        $branches = Branch::where('deleted_at', null)->get();

        return view('popexpress.customers.edit_transfer', compact('user', 'groups', 'branches', 'employee'));

    }

    public function storeTransfer($id)
    {
        if(!$this->accessAdd) {
            abort('404');
        }

        $timestamp = date('Y-m-d H:i:s');

        $user = User::where('users.deleted_at', null)
            ->where('users.id', $id)
            ->select(
                'users.id',
                'users.name',
                'users.phone',
                'users.email'
            )
            ->first();

        DB::beginTransaction();

        $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
        $group = Group::where('deleted_at', null)->where('name', 'popexpress_customer')->first();
        UserGroup::where('deleted_at', null)->where('user_id', $user->id)->update($dataDelete);

        $user_group = new UserGroup();
        $user_group->user_id = $id;
        $user_group->group_id = $group->id;
        $user_group->server_timestamp = $timestamp;
        $user_group->save();

        $customer = Customer::where('user_id', $user->id)->first();
        if(is_null($customer)) {
            $customer = new Customer();
            $customer->user_id = $user->id;
            $customer->code = CommonPopExpressHelper::generateRandomString(5, true, false, true);
            $customer->customer_type = 'social_seller';
            $customer->terms_of_payment = '30';
            $customer->credit_limit = 500000;
            $customer->pickup_address = ' ';
            $customer->pickup_location = 0;
            $customer->tax = 0;
            $customer->suspend = 0;
            $customer->join_date = $timestamp;
            $customer->server_timestamp = $timestamp;

            if($customer->save()) {
                DB::commit();
                $key = $id;
                $module = 'customers';
                $type = 'add';
                $jsonBefore = json_encode($customer);
                $jsonAfter = json_encode($customer);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                session()->flash('success', 'Data transfer user telah berhasil disimpan.');
            } else {
                DB::rollBack();
                session()->flash('error', 'Data transfer user gagal disimpan.');
            }
        }

        return redirect('/popexpress/customers/transfer');

    }

}