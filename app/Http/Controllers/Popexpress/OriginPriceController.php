<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Models\Popexpress\InternationalCode;
use App\Models\Popexpress\Origin;
use App\Models\Popexpress\Price;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class OriginPriceController extends Controller
{

    public function price(Origin $origin)
    {
        return view('popexpress.origin_price.price', compact('origin'));
    }

    public function priceGrid(Origin $origin, Request $request)
    {
        $code = request('code');
        $kota = request('kota');
        $kecamatan = request('kecamatan');

        $prices = Price::leftjoin('origins', 'prices.origin_id', '=', 'origins.id')
            ->leftjoin('international_codes', 'prices.destination_id', '=', 'international_codes.id')
            ->where('prices.origin_id', $origin->id)
            ->select(
                'prices.*',
                'international_codes.airport_code',
                'international_codes.detail_code',
                'international_codes.province',
                'international_codes.county',
                'international_codes.district'
            )
            ->when($code, function ($query) use ($code) {
                return $query->where('international_codes.detail_code', 'LIKE', "%$code%");
            })
            ->when($kota, function ($query) use ($kota) {
                return $query->where('international_codes.county', 'LIKE', "%$kota%");
            })
            ->when($kecamatan, function ($query) use ($kecamatan) {
                return $query->where('international_codes.district', 'LIKE', "%$kecamatan%");
            });

        $length = request('length');
        $start = request('start');
        $draw = request('draw');
        $order = request('order');
        $columns = request('columns');
        $sortColumn = $columns[$order[0]['column']]['name'];
        $sortDirection = $order[0]['dir'];

        if($sortColumn == '')
        {
            $sortColumn = 'international_codes.district';
            $sortDirection = 'asc';
        }

        $countTotal = $prices->count();
        $output['draw'] = $draw;
        $output['start'] = $start;
        $output['length'] = $length;
        $output['recordsTotal'] = $countTotal;
        $output['recordsFiltered'] = $countTotal;
        $output['data'] = $prices->offset($start)->limit($length)->orderBy($sortColumn, $sortDirection)->get();

        return response()->json($output);
    }

    public function createPrice(Origin $origin)
    {

        $ArrEstimasiAwal = [
            ['code'=>1 , 'name'=> '1'],
            ['code'=>2 , 'name'=> '2'],
            ['code'=>3 , 'name'=> '3'],
            ['code'=>4 , 'name'=> '4'],
            ['code'=>5 , 'name'=> '5'],
            ['code'=>6 , 'name'=> '6'],
            ['code'=>7 , 'name'=> '7'],
            ['code'=>8 , 'name'=> '8'],
            ['code'=>9 , 'name'=> '9'],
            ['code'=>10 , 'name'=> '10']
        ];

        $ArrEstimasiAkhir = [
            ['code'=>1 , 'name'=> '1'],
            ['code'=>2 , 'name'=> '2'],
            ['code'=>3 , 'name'=> '3'],
            ['code'=>4 , 'name'=> '4'],
            ['code'=>5 , 'name'=> '5'],
            ['code'=>6 , 'name'=> '6'],
            ['code'=>7 , 'name'=> '7'],
            ['code'=>8 , 'name'=> '8'],
            ['code'=>9 , 'name'=> '9'],
            ['code'=>10 , 'name'=> 'Lebih']
        ];

        $ListTujuanKecamatan = InternationalCode::addselect('international_codes.id AS id')
            ->addselect(DB::raw("CONCAT(international_codes.province,', ',international_codes.county,', ',international_codes.district) AS name"))
            ->wherenull('international_codes.deleted_at')
            ->get()->toArray();

        $ArrTujuanKecamatan= json_encode($ListTujuanKecamatan);
        return view('popexpress.origin_price.create_price', compact('breadcrumbs', 'origin', 'ArrEstimasiAwal', 'ArrEstimasiAkhir','ArrTujuanKecamatan'));
    }

    public function storePrice(Request $request)
    {
        $response = ['success'=>false , 'duplicate'=>false,  'message'=>'' ];

        $origin_id = $request->origin_id;
        $tujuan_id = $request->tujuan_id;
        $price_oneday = $request->price_oneday;
        $price_regular = $request->price_regular;
        $estimasi_awal = $request->estimasi_awal;
        $estimasi_akhir =  $request->estimasi_akhir==10 ? 0 : $request->estimasi_akhir ;


        $Exist = Price::select('*')
            ->where('prices.origin_id',$origin_id)
            ->where('prices.destination_id',$tujuan_id)
            ->first();

        if($Exist && $request->update == 0){
            $response['success'] = true;
            $response['duplicate'] = true;
            $response['message'] = 'Harga tujuan sudah terdaftar, Apakah akan diperbaharui ?';

            return response()->json($response);
        }

        if($request->update == 0)
        {
            $Arr = [
                'origin_id' =>$origin_id,
                'destination_id' => $tujuan_id,
                'regular' => $price_regular,
                'estimation_early' => $estimasi_awal,
                'estimation_late' => $estimasi_akhir,
                'one_day' => $price_oneday,
                'created_at' => date('Y-m-d H:i:s'),
                'server_timestamp' => date('Y-m-d H:i:s')
            ];

            DB::connection('pop_express')->statement('SET FOREIGN_KEY_CHECKS=0');
            $Insert = DB::table('prices')->insert($Arr);
            DB::connection('pop_express')->statement('SET FOREIGN_KEY_CHECKS=1');
            $Exist = Price::select('*')
                ->where('prices.origin_id',$origin_id)
                ->where('prices.destination_id',$tujuan_id)
                ->first();

            if($Exist){
                $response['duplicate'] = false;
                $response['success'] = true;
                session()->flash('success','Harga berhasil dibuat');
                return response()->json($response);
            }
        }
        else
        {

            $Arr = [
                'regular' => $price_regular,
                'estimation_early' => $estimasi_awal,
                'estimation_late' => $estimasi_akhir,
                'one_day' => $price_oneday,
                'updated_at' => date('Y-m-d H:i:s'),
                'server_timestamp' => date('Y-m-d H:i:s')
            ];
            DB::connection('pop_express')->statement('SET FOREIGN_KEY_CHECKS=0');
            $Update = Price::where('prices.origin_id',$origin_id)
                ->where('prices.destination_id',$tujuan_id)
                ->update($Arr);
            DB::connection('pop_express')->statement('SET FOREIGN_KEY_CHECKS=1');

            $response['success'] = true;

            session()->flash('success','Harga berhasil diperbaharui');
            return response()->json($response);
        }
    }

    public function excelPrice(Request $request)
    {
        $code = request('code');
        $kota = request('kota');
        $kecamatan = request('kecamatan');
        $origin = request('origin_id');

        $prices = Price::leftjoin('origins', 'prices.origin_id', '=', 'origins.id')
            ->leftjoin('international_codes', 'prices.destination_id', '=', 'international_codes.id')
            ->where('prices.origin_id', $origin)
            ->select(
                'prices.*',
                'international_codes.airport_code',
                'international_codes.detail_code',
                'international_codes.province',
                'international_codes.county',
                'international_codes.district'
            )
            ->when($code, function ($query) use ($code) {
                return $query->where('international_codes.detail_code', 'LIKE', "%$code%");
            })
            ->when($kota, function ($query) use ($kota) {
                return $query->where('international_codes.county', 'LIKE', "%$kota%");
            })
            ->when($kecamatan, function ($query) use ($kecamatan) {
                return $query->where('international_codes.district', 'LIKE', "%$kecamatan%");
            });

        $tb_prices = $prices->get();

        if(count($tb_prices)<=0)
        {
            $output['success'] =  false;
            $output['message'] =  'Tidak terdapat data untuk diunduh';
            return response()->json($output);
        }

        $filename = "price_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($tb_prices){
            $excel->sheet('Sheet1', function($sheet) use($tb_prices){
                $row = 1;
                $arr_title = ['ID','KODE','KODE_DETAIL','KOTA','KECAMATAN','ONE_DAY','REGULAR','ESTIMASI_AWAL','ESTIMASI_AKHIR'];
                $sheet->row($row, $arr_title);

                foreach ( $tb_prices as $index => $item) {
                    $row++;
                    $arr = [
                        $item->id,
                        $item->airport_code,
                        $item->detail_code,
                        $item->county,
                        $item->district,
                        $item->one_day,
                        $item->regular,
                        $item->estimation_early,
                        $item->estimation_late
                    ];
                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function download(Request $request)
    {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            echo "File not found";
        }
    }

    public function updatePriceGrid(Request $request)
    {
        $output = ['success' => false, 'error' => false, 'message' => 'Permintaan anda tidak dapat diproses. Mohon dicoba lagi.'];
        if($price = Price::find(request('id'))){
            $error = "";

            $editor_regular = request('editor_regular');
            $editor_one_day = request('editor_one_day');
            $editor_estimation_early = request('editor_estimation_early');
            $editor_estimation_late = request('editor_estimation_late');

            if($editor_regular == ""){
                $error .= 'Harga regular diperlukan.<br>';
            }
            if($editor_one_day == ""){
                $error .= 'Harga one day diperlukan.<br>';
            }
            if($editor_estimation_early == ""){
                $error .= 'Estimasi awal diperlukan.<br>';
            }
            if($editor_estimation_late == ""){
                $error .= 'Estimasi akhir diperlukan.<br>';
            }

            if($error != ""){
                $output['message'] = $error;
                $output['error'] = true;
                return response()->json($output);
            }

            $price->regular = $editor_regular;
            $price->one_day = $editor_one_day;
            $price->estimation_early = $editor_estimation_early;
            $price->estimation_late = $editor_estimation_late;
            $price->server_timestamp = date("Y-m-d H:i:s");
            $price->save();

            $output['success'] = true;
            $output['message'] = 'Data telah diperbarui.';
        }
        return response()->json($output);
    }

    public function indexUpload(Origin $origin)
    {
        $RecordError = [];
        return view('popexpress.origin_price.upload_price', compact('origin','RecordError'));
    }

    public function downloadTemplate(Request $request)
    {
        $output = ['success' => true, 'root' => [], 'total' => 0];

        $filename = "template_".time().strtolower(str_random(6));
        Excel::create($filename, function($excel){
            $excel->sheet('Sheet1', function($sheet){
                $row = 1;
                $arr_title = ['destination_id','regular','one_day','estimasi'];
                $sheet->row($row++, $arr_title);

                $arr = ['AMQ10000','58000','0',' 2 - 3'];
                $sheet->row($row++, $arr);
                $arr = ['AMQ10005','58000','0',' 2 - 3'];
                $sheet->row($row, $arr);

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);

        $file = request('file');
        if(file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(false);
        }else{
            echo "File not found";
        }
    }

    public function uploadPrice(Request $request)
    {
        ini_set('max_execution_time', 600); //10 Minutes
        ini_set('set_time_limit', 600);
        ini_set('memory_limit','512M');

        $origin_id = $request->origin_id ;
        $file = request('file_harga');

        $fileExt = $file->getClientOriginalName();
        $arr_ext = explode('.', $fileExt);
        $real_ext = $arr_ext[count($arr_ext)-1];

        if($file) {
            if (!in_array($real_ext, array("xlsx", "xls"))) {
                return redirect('/popexpress/origin/price/upload/' . $origin_id)->withInput()->withErrors([
                    'message' => 'Hanya excel file .xlsx yang diperbolehkan untuk diupload.'
                ]);
            }

            $tableName = "temp_origin_price_" . Auth::user()->id;
            Schema::connection('pop_express')->dropIfExists($tableName);
            Schema::connection('pop_express')->create($tableName, function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->string('origin_id')->nullable();
                $table->string('destination_id')->nullable();
                $table->string('detail_code')->nullable();
                $table->string('one_day')->nullable();
                $table->string('regular')->nullable();
                $table->string('estimasi')->nullable();
                $table->string('estimasi_early')->nullable();
                $table->string('estimasi_late')->nullable();
                $table->datetime('tanggal_upload')->nullable();
            });

            $records = Excel::load($file->path())->get()->toArray();

            foreach ($records as $record)
            {
                $arr_estimasi = explode('-',str_replace("'",'',str_replace(' ','',trim($record['estimasi']))));
                $estimasi_early = isset($arr_estimasi[0]) ? $arr_estimasi[0] : 0;
                $estimasi_late = isset($arr_estimasi[1]) ? $arr_estimasi[1] : 0;

                $arr = [
                    'origin_id'=>$origin_id,
                    'destination_id'=>0,
                    'detail_code'=>$record['destination_id'],
                    'one_day'=>$record['one_day'],
                    'regular'=>$record['regular'],
                    'estimasi'=>$record['estimasi'],
                    'estimasi_early'=>$estimasi_early,
                    'estimasi_late'=>$estimasi_late,
                    'tanggal_upload'=> date('Y-m-d H:i:s')
                ];
                DB::connection('pop_express')->table($tableName)->insert($arr);
            }



            //CHECK International Code
            $RecordError = DB::connection('pop_express')->table($tableName.' AS a')
                ->leftjoin('international_codes AS b',function($join)  {
                    $join->on('b.detail_code', '=', 'a.detail_code');
                })
                ->whereNull('b.id')
                ->select('a.*')
                ->get();

            //UPDATE DESTINATION ID
            $Statement = "UPDATE ".$tableName." AS a
                          LEFT JOIN international_codes AS b ON a.detail_code=b.detail_code
                          SET a.destination_id = b.id
                          WHERE b.id IS NOT NULL";
            $Updated = DB::connection('pop_express')->statement($Statement);

            //UPDATE
            $Statement = "  UPDATE ".$tableName." AS a
                            LEFT JOIN prices AS b ON a.destination_id=b.destination_id AND a.origin_id=b.origin_id
                            SET
                            b.regular = a.regular, b.estimation_early = a.estimasi_early,
                            b.estimation_late = a.estimasi_late, b.one_day = a.one_day,
                            b.updated_at = '".date('Y-m-d H:i:s')."',
                            b.server_timestamp = '".date('Y-m-d H:i:s')."'
                            WHERE b.id IS NOT NULL";
            $Updated = DB::connection('pop_express')->statement($Statement);

            //INSERT
            DB::connection('pop_express')->statement('SET FOREIGN_KEY_CHECKS=0');
            $Statement = "INSERT INTO prices (origin_id,destination_id,regular,estimation_early,estimation_late,one_day,created_at, server_timestamp)
                          SELECT a.origin_id,a.destination_id,a.regular,a.estimasi_early,a.estimasi_late,a.one_day,'".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."'
                          FROM ".$tableName." AS a
                          LEFT JOIN prices AS b ON a.destination_id=b.destination_id AND a.origin_id=b.origin_id
                          LEFT JOIN international_codes AS c ON a.destination_id=c.id
                          WHERE c.id IS NOT NULL AND b.id IS NULL";
            $Inserted = DB::connection('pop_express')->statement($Statement);
            DB::connection('pop_express')->statement('SET FOREIGN_KEY_CHECKS=1');


            if(count($RecordError)>0)
            {
                $origin = Origin::select('*')
                    ->where('origins.id',$origin_id)
                    ->first();

                return view('popexpress.origin_price.upload_price', compact('RecordError','origin_id','origin'));
            }
            else{
                return redirect('/popexpress/origin/'.$origin_id.'/price')->with(['success' => 'Total '.count($records).' Data price berhasil diupload']);
            }
        }

    }

}