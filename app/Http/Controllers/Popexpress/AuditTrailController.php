<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Models\Popexpress\AuditTrail;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class AuditTrailController extends Controller {

    public function index(Request $request)
    {
        $key = request('key');
        $module = request('module');
        $type = request('type');
        $dateRange = request('created_at');
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $reportUsers = User::pluck('name', 'id')->toArray();

        $auditTrails = AuditTrail::select('audit_trails.*')
            ->when($key, function ($query) use ($key) {
                return $query->where('audit_trails.key', 'LIKE', "%$key%");
            })
            ->when($startDate, function ($query) use ($startDate,$endDate) {
                return $query->whereBetween('audit_trails.created_at', [$startDate,$endDate]);
            })
            ->when($module, function ($query) use ($module) {
                return $query->where('audit_trails.module', 'LIKE', "%$module%");
            })
            ->when($type, function ($query) use ($type) {
                return $query->where('audit_trails.type', '=', $type);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $types = ['general', 'add', 'edit', 'delete'];

        return view('popexpress.audit_trails.index', compact('auditTrails', 'types', 'reportUsers'));
    }
}