<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popbox\LockerLocation;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\BranchPickup;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Origin;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class BranchController extends Controller {

    public function index(Request $request)
    {
        $code = request('code');
        $type = request('type');
        $name = request('name');
        $location = request('location');

        $branches = Branch::leftJoin('destinations', 'destinations.id', '=', 'branches.location')
            ->where('branches.deleted_at', null)
            ->select(
                'branches.*',
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS destination")
            )
            ->when($code, function ($query) use ($code) {
                return $query->where('branches.code', 'LIKE', "%$code%");
            })
            ->when($type, function ($query) use ($type) {
                return $query->where('branches.type', '=', $type);
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('branches.name', 'LIKE', "%$name%");
            })
            ->when($location, function ($query) use ($location) {
                return $query->where('branches.location', '=', $location);
            })
            ->paginate(20);

        $timezones = ['+14:00', '+13:00', '+12:45', '+12:00', '+11:00', '+10:30', '+10:00', '+09:30', '+09:00', '+08:45', '+08:30', '+08:00', '+07:00', '+06:30', '+06:00', '+05:45', '+05:30', '+05:00', '+04:30', '+04:00', '+03:00', '+02:00', '+01:00', '00:00', '-01:00', '-02:00', '-02:30', '-03:00', '-04:00', '-05:00', '-06:00', '-07:00', '-08:00', '-09:00', '-09:30', '-10:00', '-11:00', '-12:00'];
        $destinationNames = Destination::where('destinations.deleted_at', null)
            ->select(
                DB::raw('destinations.id'),
                DB::raw("CONCAT(destinations.detail_code, ' - ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS name")
            )
            ->get();

        $types = ['hub','subhub','droppoint','popagent','selfservice','3pl'];

        foreach($branches as $key => $data){
            if($data->last_sync != ""){
                $branches[$key]->last_sync = Carbon::createFromFormat('Y-m-d H:i:s', $data->last_sync)->format('d/m/Y H:i:s').CommonPopExpressHelper::getTimezoneText();
            }
        }

        return view('popexpress.branches.index', compact('branches', 'timezones', 'destinationNames', 'types'));
    }

    public function store(Request $request)
    {
        $id = request('id');
        $delete = request('delete');
        $code = request('code');
        $name = request('name');
        $latitude = request('latitude');
        $longitude = request('longitude');
        $type = request('type');
        $location = request('location');
        $timezone = request('timezone');
        $address = request('address');

        if(empty($id)) {


            $this->validate(request(), [
                'code' => 'required',
                'name' => 'required',
                'type' => 'required',
                'location' => 'required',
                'timezone' => 'required',
                'address' => 'required'
            ],[
                'code.required' => 'Code diperlukan.',
                'name.required' => 'Name diperlukan.',
                'type.required' => 'Type diperlukan.',
                'location.required' => 'Location diperlukan.',
                'timezone.required' => 'Timezone diperlukan.',
                'address.required' => 'Address diperlukan.',
            ]);

            if(Branch::where('code', $code)->where('id', '!=', $id)->first()){
                return redirect('/popexpress/branches')->withInput()->withErrors([
                    'message' => 'Code sudah terdaftar.'
                ]);
            }

            $dataBranch = [
                'code' => $code,
                'name' => $name,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'type' => $type,
                'location' => $location,
                'timezone' => $timezone,
                'address' => $address,
                'server_timestamp' => date('Y-m-d H:i:s')
            ];

            $branch = Branch::create($dataBranch);

            if($branch) {
                $data = $branch->getAttributes();
                $key = $branch->id;
                $module = 'branches';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data branch telah berhasil disimpan.');
        } else {
            $branch = Branch::where('id', '=', $id)->first();
            $old = $branch->getAttributes();

            if($delete == '1') {
                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                $deleteBranch = Branch::where('deleted_at', null)->where('id', $id)->update($dataDelete);
                if($deleteBranch){
                    $key = $id;
                    $module = 'branches';
                    $type = 'delete';
                    $jsonBefore = json_encode($old);
                    $jsonAfter = json_encode($dataDelete);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data telah dihapus.');
                }
            } else {
                $this->validate(request(), [
                    'code' => 'required',
                    'name' => 'required',
                    'type' => 'required',
                    'location' => 'required',
                    'timezone' => 'required',
                    'address' => 'required'
                ],[
                    'code.required' => 'Code diperlukan.',
                    'name.required' => 'Name diperlukan.',
                    'type.required' => 'Type diperlukan.',
                    'location.required' => 'Location diperlukan.',
                    'timezone.required' => 'Timezone diperlukan.',
                    'address.required' => 'Address diperlukan.',
                ]);

                if(Branch::where('code', $code)->where('id', '!=', $id)->first()){
                    return redirect('/popexpress/branches')->withInput()->withErrors([
                        'message' => 'Code sudah terdaftar.'
                    ]);
                }

                $branch->code = $code;
                $branch->name = $name;
                $branch->latitude = $latitude;
                $branch->longitude = $longitude;
                $branch->type = $type;
                $branch->location = $location;
                $branch->timezone = $timezone;
                $branch->address = $address;
                $branch->server_timestamp = date("Y-m-d H:i:s");
                $branch->save();
                $new = $branch->getAttributes();
                $diff = array_diff_assoc($old, $new);

                if($branch) {
                    $key = $id;
                    $module = 'branches';
                    $type = 'edit';
                    $jsonBefore = json_encode($old);
                    $jsonAfter = json_encode($diff);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                }
                session()->flash('success', 'Data branch telah berhasil diperbarui.');
            }
        }

        return redirect('/popexpress/branches');
    }

    public function export(Request $request)
    {
        $code = request('code');
        $type = request('type');
        $name = request('name');
        $location = request('location');

        $branches = Branch::leftJoin('destinations', 'destinations.id', '=', 'branches.location')
            ->where('branches.deleted_at', null)
            ->select(
                'branches.*',
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS destination")
            )
            ->when($code, function ($query) use ($code) {
                return $query->where('branches.code', 'LIKE', "%$code%");
            })
            ->when($type, function ($query) use ($type) {
                return $query->where('branches.type', '=', $type);
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('branches.name', 'LIKE', "%$name%");
            })
            ->when($location, function ($query) use ($location) {
                return $query->where('branches.location', '=', $location);
            })->get();

        foreach($branches as $key => $data){
            if($data->last_sync != ""){
                $branches[$key]->last_sync = Carbon::createFromFormat('Y-m-d H:i:s', $data->last_sync)->format('d/m/Y H:i:s').CommonPopExpressHelper::getTimezoneText();
            }
        }

        $filename = "branches_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($branches){
            $excel->sheet('Sheet1', function($sheet) use($branches){
                $row = 1;

                $arr_title = ['code','name','type','location','address','latitude','longitude','timezone', 'last_sync'];
                $sheet->row($row, $arr_title);

                foreach ( $branches as $index => $item) {
                    $row++;
                    $arr = [
                        $item->code,
                        $item->name,
                        strtoupper($item->type),
                        $item->destination,
                        $item->address,
                        $item->latitude,
                        $item->longitude,
                        $item->timezone,
                        $item->last_sync
                    ];
                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function download(Request $request)
    {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            echo "File not found";
        }
    }

    public function detail($branchid)
    {
        $branch = Branch::leftJoin('destinations', 'destinations.id', '=', 'branches.location')
            ->where('branches.deleted_at', null)
            ->where('branches.id', $branchid)
            ->select(
                'branches.*',
                'destinations.district',
                'destinations.county',
                'destinations.province',
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.province, ', ', destinations.county, ', ', destinations.district)) AS destination")
            )
            ->first();

        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $is_locker = request('is_locker');
        $locker_id = request('locker_id');
        $county = request('county');
        $district = request('district');

        $destinations = Destination::leftJoin('origins', 'origins.id', '=', 'destinations.origin_id')
            ->where('destinations.deleted_at', null)
            ->select('destinations.*', 'origins.name as origin_name')
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('destinations.origin_id', '=', $origin_id);
            })
            ->when($locker_id, function ($query) use ($locker_id) {
                return $query->where('destinations.locker_id', '=', $locker_id);
            })
            ->when($is_locker != '', function ($query) use ($is_locker) {
                return $query->where('destinations.is_locker', '=', $is_locker);
            })
            ->when($airport_code, function ($query) use ($airport_code) {
                return $query->where('destinations.airport_code', 'LIKE', "%$airport_code%");
            })
            ->when($detail_code, function ($query) use ($detail_code) {
                return $query->where('destinations.detail_code', 'LIKE', "%$detail_code%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('destinations.province', 'LIKE', "%$province%");
            })
            ->when($county, function ($query) use ($county) {
                return $query->where('destinations.county', 'LIKE', "%$county%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('destinations.district', 'LIKE', "%$district%");
            })
            ->paginate(20);


        $timezones = ['+14:00', '+13:00', '+12:45', '+12:00', '+11:00', '+10:30', '+10:00', '+09:30', '+09:00', '+08:45', '+08:30', '+08:00', '+07:00', '+06:30', '+06:00', '+05:45', '+05:30', '+05:00', '+04:30', '+04:00', '+03:00', '+02:00', '+01:00', '00:00', '-01:00', '-02:00', '-02:30', '-03:00', '-04:00', '-05:00', '-06:00', '-07:00', '-08:00', '-09:00', '-09:30', '-10:00', '-11:00', '-12:00'];
        $destinationNames = Destination::where('destinations.deleted_at', null)
            ->select(
                DB::raw('destinations.id'),
                DB::raw("CONCAT(destinations.detail_code, ' - ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS name")
            )
            ->get();

        $types = ['hub','subhub','droppoint','popagent','selfservice','3pl'];

        $origins = Origin::where('deleted_at', null)->get();

        $lockerNames = LockerLocation::where('locker_locations.name', 'NOT LIKE', '%PopBox @%')
            ->select(
                DB::raw('locker_locations.locker_id'),
                DB::raw("CONCAT('Popbox @ ',locker_locations.name) AS name")
            )
            ->get();

        $listDestinationPickups = BranchPickup::where('deleted_at', null)->where('branch_id', $branchid)->pluck('destination_id')->toArray();
        $listDestinationPickups = json_encode($listDestinationPickups);
        if($branch){
            if(!$branch->latitude){
                $branch->latitude = 0;
            }
            if(!$branch->longitude){
                $branch->longitude = 0;
            }

            if(!is_null($branch->last_sync)) {
                $branch->last_sync = Carbon::createFromFormat('Y-m-d H:i:s', $branch->last_sync)->format('d/m/Y H:i:s').CommonPopExpressHelper::getTimezoneText();
            }

            $place = str_replace(" ", "+", $branch->district.', '.$branch->county.', '.$branch->province);

            return view('popexpress.branches.detail', compact('branch', 'destinationNames', 'place', 'branchPickups', 'timezones', 'types', 'destinations', 'origins', 'lockerNames', 'listDestinationPickups'));
        }else{
            return abort(404);
        }
    }

    public function updateBranch(Request $request)
    {

        $id = request('id');
        $code = request('code');
        $name = request('name');
        $type = request('type');
        $address = request('address');
        $latitude = request('latitude');
        $longitude = request('longitude');
        $timezone = request('timezone');
        $location = request('location');

        if(!empty($id)) {
            $this->validate(request(), [
                'code' => 'required',
                'name' => 'required',
                'type' => 'required',
                'location' => 'required',
                'timezone' => 'required',
                'address' => 'required'
            ],[
                'code.required' => 'Code diperlukan.',
                'name.required' => 'Name diperlukan.',
                'type.required' => 'Type diperlukan.',
                'location.required' => 'Location diperlukan.',
                'timezone.required' => 'Timezone diperlukan.',
                'address.required' => 'Address diperlukan.',
            ]);

            if(Branch::where('code', $code)->where('id', '!=', $id)->first()){
                $result = [
                    'status' => 'error',
                    'message' => 'Code sudah terdaftar.'
                ];
                return response()->json($result);
            }


            $branch = Branch::where('id', '=', $id)->first();
            $old = $branch->getAttributes();

            $branch->code = $code;
            $branch->name = $name;
            $branch->latitude = $latitude;
            $branch->longitude = $longitude;
            $branch->type = $type;
            $branch->location = $location;
            $branch->timezone = $timezone;
            $branch->address = $address;
            $branch->server_timestamp = date("Y-m-d H:i:s");
            $branch->save();
            $new = $branch->getAttributes();

            if($branch) {
                $key = $id;
                $module = 'branches';
                $type = 'edit';
                $diffBefore = array_diff_assoc($old, $new);
                $diffAfter = array_diff_assoc($new, $old);
                $jsonBefore = json_encode($diffBefore);
                $jsonAfter = json_encode($diffAfter);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }

            $destination = Destination::where('id', $branch->location)->first();
            $destinationPlace = $destination->detail_code.' - '.$destination->province.', '.$destination->county.', '.$destination->district;

            if(!is_null($branch->last_sync)) {
                $branch->last_sync = Carbon::createFromFormat('Y-m-d H:i:s', $branch->last_sync)->format('d/m/Y H:i:s').CommonPopExpressHelper::getTimezoneText();
            }

            $result = [
                'status' => 'success',
                'message' => 'success',
                'data' => [
                    'code' => $branch->code,
                    'name' => $branch->name,
                    'type' => $branch->type,
                    'address' => $branch->address,
                    'latitude' => is_null($branch->latitude) ? 0 : $branch->latitude,
                    'longitude' => is_null($branch->longitude) ? 0 : $branch->longitude,
                    'timezone' => $branch->timezone,
                    'location' => $branch->location,
                    'destination' => $destinationPlace,
                    'last_sync' => $branch->last_sync
                ]
            ];

        } else {
            $result = [
                'status' => 'error',
                'message' => 'ID kosong.'
            ];
        }

        return response()->json($result);

    }

    public function load(Request $request)
    {
        $branchId = request('branch_id');

        $branchPickups = BranchPickup::leftJoin('branches', 'branches.id', '=', 'branches_pickups.branch_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'branches.location')
            ->where('branches_pickups.deleted_at', null)
            ->where('branches_pickups.branch_id', '=', $branchId)
            ->select(
                'branches_pickups.*',
                'branches_pickups.id as branches_pickup_id',
                'branches.*',
                DB::raw("CONCAT(branches.code, ' - ', branches.name) as branch_name"),
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS description")
            )
            ->orderBy('branches_pickups.branch_id')
            ->get()
            ->toArray();

        return response()->json($branchPickups);

    }

    public function grid(Request $request)
    {

        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $is_locker = request('is_locker');
        $locker_id = request('locker_id');
        $county = request('county');
        $district = request('district');

        $branches = Destination::leftjoin('origins','origins.id','=','destinations.origin_id')
            ->select(
                'destinations.*',
                'origins.name as origin_name'
            )
            ->where('destinations.deleted_at', null)
            ->select('destinations.*', 'origins.name as origin_name')
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('destinations.origin_id', '=', $origin_id);
            })
            ->when($locker_id, function ($query) use ($locker_id) {
                return $query->where('destinations.locker_id', '=', $locker_id);
            })
            ->when($is_locker != '', function ($query) use ($is_locker) {
                return $query->where('destinations.is_locker', '=', $is_locker);
            })
            ->when($airport_code, function ($query) use ($airport_code) {
                return $query->where('destinations.airport_code', 'LIKE', "%$airport_code%");
            })
            ->when($detail_code, function ($query) use ($detail_code) {
                return $query->where('destinations.detail_code', 'LIKE', "%$detail_code%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('destinations.province', 'LIKE', "%$province%");
            })
            ->when($county, function ($query) use ($county) {
                return $query->where('destinations.county', 'LIKE', "%$county%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('destinations.district', 'LIKE', "%$district%");
            });

        $length = request('length');
        $start = request('start');
        $draw = request('draw');
        $order = request('order');
        $columns = request('columns');
        $sortColumn = $columns[$order[0]['column']]['name'];
        $sortDirection = $order[0]['dir'];

        if($sortColumn == '')
        {
            $sortColumn = 'destinations.detail_code';
            $sortDirection = 'asc';
        }

        $countTotal = $branches->count();
        $output['draw'] = $draw;
        $output['start'] = $start;
        $output['length'] = $length;
        $output['recordsTotal'] = $countTotal;
        $output['recordsFiltered'] = $countTotal;
        $output['data'] = $branches->take($length)->skip($start)->orderBy($sortColumn, $sortDirection)->get();

        return response()->json($output);
    }

    public function addBranchPickup(Request $request)
    {

        $branch_id = request('branch_id');
        $list_destinations = request('list_destination');
        $destinations = json_decode($list_destinations);
        $exist = 0;
        $add = '';
        $delete = '';

        if(!empty($branch_id)) {

            foreach ($destinations as $destination) {

                $check = BranchPickup::where('deleted_at', null)
                    ->where('branch_id', $branch_id)
                    ->where('destination_id', $destination->id)
                    ->first();

                $codeDestination = Destination::where('id', $destination->id)->select('detail_code')->first();

                if($destination->value == 1) {
                    if(is_null($check)) {
                        $branchPickup = BranchPickup::create([
                            'branch_id' => $branch_id,
                            'destination_id' => $destination->id,
                            'server_timestamp' => date('Y-m-d H:i:s')
                        ]);
                        if($branchPickup) {
                            $add .= $codeDestination->detail_code.', ';
                            $data = $branchPickup->getAttributes();
                            $key = $branchPickup->id;
                            $module = 'branches_pickups';
                            $type = 'add';
                            $jsonBefore = null;
                            $jsonAfter = json_encode($data);
                            $remark = null;
                            CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                        }
                    }
                } else {
                    if(!is_null($check)) {
                        $branchPickup = BranchPickup::where('id', '=', $check->id)->first();
                        $old = $branchPickup->getAttributes();

                        $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                        $deleteBranchPickup = BranchPickup::where('deleted_at', null)->where('id', $check->id)->update($dataDelete);
                        if($deleteBranchPickup){
                            $delete .= $codeDestination->detail_code.', ';
                            $key = $check->id;
                            $module = 'branches_pickups';
                            $type = 'delete';
                            $jsonBefore = json_encode($old);
                            $jsonAfter = json_encode($dataDelete);
                            $remark = null;
                            CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                        }
                    }
                }
            }

            $listDestinationPickups = BranchPickup::where('deleted_at', null)->where('branch_id', $branch_id)->pluck('destination_id')->toArray();
            $listDestinationPickups = json_encode($listDestinationPickups);

            $result = [
                'status' => 'success',
                'message' => ($add != '' ? $add.' berhasil disimpan.<br>' : '').($delete != '' ? $delete.' berhasil dihapus.' : ''),
                'list' => $listDestinationPickups
            ];

        } else {
            $result = [
                'status' => 'error',
                'message' => 'ID kosong.'
            ];
        }

        return response()->json($result);

    }

}