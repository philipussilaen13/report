<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\BranchPickup;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Origin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class BranchPickupController extends Controller {

    public function index(Request $request)
    {
        $branch_id = request('branch_id');
        $destination_id = request('destination_id');

        $branchPickups = BranchPickup::leftJoin('branches', 'branches.id', '=', 'branches_pickups.branch_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'branches_pickups.destination_id')
            ->where('branches_pickups.deleted_at', null)
            ->select(
                'branches_pickups.*',
                DB::raw("CONCAT(branches.code, ' - ', branches.name) as branch_name"),
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS description")
            )
            ->when($branch_id, function ($query) use ($branch_id) {
                return $query->where('branches_pickups.branch_id', '=', $branch_id);
            })
            ->when($destination_id, function ($query) use ($destination_id) {
                return $query->where('branches_pickups.destination_id', '=', $destination_id);
            })
            ->orderBy('branches_pickups.branch_id')
            ->paginate(20);

        $branches = Branch::where('deleted_at', null)->get();
        $destinations = Destination::where('destinations.deleted_at', null)
            ->select(
                DB::raw('destinations.id'),
                DB::raw("CONCAT(destinations.detail_code, ' - ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS name")
            )
            ->get();

        return view('popexpress.branches_pickups.index', compact('branchPickups', 'branches', 'destinations'));
    }

    public function store(Request $request)
    {
        $id = request('id');
        $branch_id = request('branch_id');
        $destination_id = request('destination_id');

        if(empty($id)) {
            $this->validate(request(), [
                'branch_id' => 'required',
                'destination_id' => 'required'
            ],[
                'branch_id.required' => 'Branch diperlukan.',
                'destination_id.required' => 'Destination diperlukan.'
            ]);

            if(BranchPickup::where('branch_id', $branch_id)->where('destination_id', $destination_id)->first()){
                return redirect('/popexpress/branches_pickups')->withInput()->withErrors([
                    'message' => 'Nama branch dan destination telah terdaftar. Silakan input nama lain.'
                ]);
            }

            $branchPickup = BranchPickup::create([
                'branch_id' => $branch_id,
                'destination_id' => $destination_id,
                'server_timestamp' => date('Y-m-d H:i:s')
            ]);

            if($branchPickup) {
                $data = $branchPickup->getAttributes();
                $key = $branchPickup->id;
                $module = 'branches_pickups';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }

            session()->flash('success', 'Data branch pickup telah berhasil disimpan.');
        } else {
            $branchPickup = BranchPickup::where('id', '=', $id)->first();
            $old = $branchPickup->getAttributes();

            $this->validate(request(), [
                'branch_id' => 'required',
                'destination_id' => 'required'
            ],[
                'branch_id.required' => 'Branch diperlukan.',
                'destination_id.required' => 'Destination diperlukan.'
            ]);

            if(BranchPickup::where('branch_id', $branch_id)->where('destination_id', $destination_id)->where('id', '!=', $id)->first()){
                return redirect('/popexpress/branches_pickups')->withInput()->withErrors([
                    'message' => 'Nama branch dan destination telah terdaftar. Silakan input nama lain.'
                ]);
            }

            $branchPickup->branch_id = $branch_id;
            $branchPickup->destination_id = $destination_id;
            $branchPickup->server_timestamp = date("Y-m-d H:i:s");
            $branchPickup->save();
            $new = $branchPickup->getAttributes();
            $diff = array_diff_assoc($old, $new);

            if($branchPickup) {
                $key = $id;
                $module = 'branches_pickups';
                $type = 'edit';
                $jsonBefore = json_encode($old);
                $jsonAfter = json_encode($diff);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data branch pickup telah berhasil diperbarui.');
        }

        return redirect('/popexpress/branches_pickups');
    }
}