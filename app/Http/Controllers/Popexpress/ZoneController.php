<?php

namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Models\Popexpress\InternationalCode;
use App\Models\Popexpress\ThirdParty;
use App\Models\Popexpress\Zone;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;

class ZoneController extends Controller
{

    private $ArrayZone = [
        ['code' => 'A', 'name' => 'A'],
        ['code' => 'B', 'name' => 'B'],
        ['code' => 'C', 'name' => 'C'],
        ['code' => 'D', 'name' => 'D']
    ];

    public function index()
    {
        $thirdparties = ThirdParty::select('id', 'code')->get()->sortBy('code');
        $extJSArray = [];
        $tmp = $thirdparties->toArray();
        foreach($tmp as $data){
            array_push($extJSArray, ['name' => $data['code'], 'val' => $data['code']]);
        }
        return view('popexpress.zone.index', compact('extJSArray'));
    }

    public function grid(Request $request)
    {
        $code = request('code');
        $codedetail = request('codedetail');
        $province = request('province');
        $city = request('city');
        $district = request('district');

        $zones = Zone::join('international_codes','international_codes.id','=','zones.international_code_id')
            ->leftjoin(DB::raw("third_parties as tpb"),'zones.one_day_handler','=',DB::raw("tpb.id"))
            ->leftjoin(DB::raw("third_parties as tpa"),'zones.regular_handler','=',DB::raw("tpa.id"))
            ->select(
                'zones.id as id',
                'zones.zone as zone',
                'tpa.code as tpacode',
                'tpb.code as tpbcode',
                'international_codes.airport_code',
                'international_codes.detail_code',
                'international_codes.latitude',
                'international_codes.province',
                'international_codes.county',
                'international_codes.district'
            )
            ->when($code, function ($query) use ($code) {
                return $query->where('international_codes.airport_code', 'LIKE', "%$code%");
            })
            ->when($codedetail, function ($query) use ($codedetail) {
                return $query->where('international_codes.detail_code', 'LIKE', "%$codedetail%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('international_codes.province', 'LIKE', "%$province%");
            })
            ->when($city, function ($query) use ($city) {
                return $query->where('international_codes.county', 'LIKE', "%$city%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('international_codes.district', 'LIKE', "%$district%");
            });

        $length = request('length');
        $start = request('start');
        $draw = request('draw');
        $order = request('order');
        $columns = request('columns');
        $sortColumn = $columns[$order[0]['column']]['name'];
        $sortDirection = $order[0]['dir'];

        if($sortColumn == '')
        {
            $sortColumn = 'international_codes.detail_code';
            $sortDirection = 'asc';
        }

        $countTotal = $zones->count();
        $output['draw'] = $draw;
        $output['start'] = $start;
        $output['length'] = $length;
        $output['recordsTotal'] = $countTotal;
        $output['recordsFiltered'] = $countTotal;
        $output['data'] = $zones->offset($start)->limit($length)->orderBy($sortColumn, $sortDirection)->get();

        return response()->json($output);
    }

    public function destination($destinationId)
    {
        $destination = InternationalCode::select('*')->where('id', $destinationId)->whereNull('deleted_at')->first();
        if($destination){
            if(!$destination->latitude){
                $destination->latitude = 0;
            }
            if(!$destination->longitude){
                $destination->longitude = 0;
            }
            return view('popexpress.zone.destination', compact('destination'));
        }else{
            return abort(404);
        }
    }

    public function destinationUpdate($destinationId, Request $request)
    {   $destination = InternationalCode::select('*')->where('id', $destinationId)->whereNull('deleted_at')->first();
        if($destination){
            $latitude = 0;
            if(request('latitude')){
                $latitude = request('latitude');
            }
            $longitude = 0;
            if(request('longitude')){
                $longitude = request('longitude');
            }
            $coverageSize = 0;
            if(request('coverage')){
                $coverageSize = request('coverage');
            }
            if($latitude == 0){
                $latitude = null;
            }
            if($longitude == 0){
                $longitude = null;
            }

            $updateData = [
                "latitude" => $latitude,
                "longitude" => $longitude,
                "coverage_size" => $coverageSize,
                "server_timestamp" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ];
            InternationalCode::where('id', $destination->id)->update($updateData);
            session()->flash('success', 'Data destinasi telah berhasil diperbarui.');
            return redirect('/popexpress/zone/destination/'.$destination->id);
        }else{
            return abort(404);
        }
    }

    public function createZone(Request $request)
    {
        $ListTujuanKecamatan = InternationalCode::addselect('international_codes.id AS id')
            ->addselect(DB::raw("CONCAT(international_codes.province,', ',international_codes.county,', ',international_codes.district) AS name"))
            ->wherenull('international_codes.deleted_at')
            ->get()->toArray();

        $ArrTujuanKecamatan= json_encode($ListTujuanKecamatan);

        $ListThirdParties = ThirdParty::addSelect('third_parties.id as id')
            ->addSelect('third_parties.code as name')
            ->wherenull('third_parties.deleted_at')
            ->orderBy('third_parties.id','ASC')
            ->get();

        $ListZona = $this->ArrayZone;

        return view('popexpress.zone.create', compact('ArrTujuanKecamatan','ListThirdParties','ListZona'));
    }

    public function storeZone(Request $request)
    {
        $response = ['success' => false, 'duplicate' => false, 'message' => ''];

        $DestinationID = request('tujuan_id');
        $Zone = request('zona');
        $requler_handler = request('regular');
        $oneday_handler = request('oneday');
        $isupdate = request('update');

        if ($isupdate == 0) {
            $zone_exist = Zone::where('zones.international_code_id', $DestinationID)->first();

            if ($zone_exist) {
                $response['success'] = true;
                $response['duplicate'] = true;
                $response['message'] = 'Zona sudah terdaftar, Apakah akan diperbaharui ?';

                return response()->json($response);
            }

            $Arr_Statement = [
                'international_code_id' => $DestinationID,
                'zone' => $Zone,
                'regular_handler' => $requler_handler,
                'one_day_handler' => $oneday_handler,
                'created_at' => date('Y-m-d H:i:s'),
                'server_timestamp' => date('Y-m-d H:i:s')
            ];

            $Zone_Insert = Zone::insert($Arr_Statement);
            $zone_exist = Zone::where('zones.international_code_id', $DestinationID)->first();

            if ($zone_exist) {
                $response['duplicate'] = false;
                $response['success'] = true;
                session()->flash('success', 'Zona berhasil dibuat');
                return response()->json($response);
            }
        } else {
            $Zone_Update = Zone::where('international_code_id', $DestinationID)
                ->update(
                    [
                        'zone' => $Zone,
                        'regular_handler' => $requler_handler,
                        'one_day_handler' => $oneday_handler,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'server_timestamp' => date('Y-m-d H:i:s')
                    ]
                );

            $zone_exist = Zone::where('zones.international_code_id', $DestinationID)->first();

            if ($zone_exist) {
                $response['duplicate'] = false;
                $response['success'] = true;
                session()->flash('success', 'Zona berhasil diperbaharui');
                return response()->json($response);
            }
        }

    }


    public function excelZone(Request $request)
    {
        $output = ['success' => true, 'root' => [], 'total' => 0];


        $code = request('code');
        $codedetail = request('codedetail');
        $province = request('province');
        $city = request('city');
        $district = request('district');

        $zones = Zone::join('international_codes','international_codes.id','=','zones.international_code_id')
            ->leftjoin(DB::raw("third_parties as tpb"),'zones.one_day_handler','=',DB::raw("tpb.id"))
            ->leftjoin(DB::raw("third_parties as tpa"),'zones.regular_handler','=',DB::raw("tpa.id"))
            ->select(
                'zones.id as id',
                'zones.zone as zone',
                'tpa.code as tpacode',
                'tpb.code as tpbcode',
                'international_codes.airport_code',
                'international_codes.detail_code',
                'international_codes.latitude',
                'international_codes.province',
                'international_codes.county',
                'international_codes.district'
            )
            ->when($code, function ($query) use ($code) {
                return $query->where('international_codes.airport_code', 'LIKE', "%$code%");
            })
            ->when($codedetail, function ($query) use ($codedetail) {
                return $query->where('international_codes.detail_code', 'LIKE', "%$codedetail%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('international_codes.province', 'LIKE', "%$province%");
            })
            ->when($city, function ($query) use ($city) {
                return $query->where('international_codes.county', 'LIKE', "%$city%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('international_codes.district', 'LIKE', "%$district%");
            });

        $tb_zona = $zones->get();

        $filename = "zona_" . time() . strtolower(str_random(6));

        Excel::create($filename, function ($excel) use ($tb_zona) {
            $excel->sheet('Sheet1', function ($sheet) use ($tb_zona) {
                $row = 1;
                $no_urut = 1;
                $arr_title = ['NO', 'KODE', 'KODE DETIL', 'PROVINSI', 'KABUPATEN', 'KECAMATAN', 'ZONA', 'REGULAR', 'ONE DAY'];
                $sheet->row($row, $arr_title);

                foreach ($tb_zona as $index => $item) {
                    $row++;
                    $arr = [
                        $no_urut,
                        $item->airport_code,
                        $item->detail_code,
                        $item->province,
                        $item->county,
                        $item->district,
                        $item->zone,
                        $item->tpacode,
                        $item->tpbcode
                    ];
                    $sheet->row($row, $arr);
                    $no_urut++;
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename . ".xlsx";
        $output['success'] = true;

        return response()->json($output);
    }

    public function download(Request $request)
    {
        $file = request('file');
        if ($file != "" && file_exists(storage_path("/app") . "/" . $file)) {
            return response()->download(storage_path("/app") . "/" . $file)->deleteFileAfterSend(true);
        } else {
            echo "File not found";
        }
    }

    public function uploadIndex(Request $request)
    {
        $RecordError = [];
        return view('popexpress.zone.upload', compact('RecordError'));
    }

    public function uploadZone(Request $request){
        ini_set('max_execution_time', 600); //10 Minutes
        ini_set('set_time_limit', 600);
        ini_set('memory_limit','512M');

        $file = request('file_upload');

        $fileExt = $file->getClientOriginalName();
        $arr_ext = explode('.', $fileExt);
        $real_ext = $arr_ext[count($arr_ext)-1];

        if($file) {
            if (!in_array($real_ext, array("xlsx", "xls"))) {
                return redirect('/zone/upload')->withInput()->withErrors([
                    'message' => 'Hanya excel file .xlsx yang diperbolehkan untuk diupload.'
                ]);
            }

            $tableName = "temp_zone_" . Auth::user()->id;
            Schema::connection('pop_express')->dropIfExists($tableName);
            Schema::connection('pop_express')->create($tableName, function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->string('kode')->nullable();
                $table->string('destination_id')->nullable()->default(0);
                $table->string('kode_detil')->nullable();
                $table->string('provinsi')->nullable();
                $table->string('kabupaten')->nullable();
                $table->string('kecamatan')->nullable();
                $table->string('zona')->nullable();
                $table->string('regular_id')->nullable()->default(0);
                $table->string('regular')->nullable();
                $table->string('oneday_id')->nullable()->default(0);
                $table->string('oneday')->nullable();
            });

            $records = Excel::load($file->path())->get()->toArray();

            foreach ($records as $record)
            {
                $arr = [
                    'kode'=>$record['kode'],
                    'kode_detil'=>$record['kode_detil'],
                    'provinsi'=>$record['provinsi'],
                    'kabupaten'=>$record['kabupaten'],
                    'kecamatan'=>$record['kecamatan'],
                    'zona'=>$record['zona'],
                    'regular'=>$record['regular'],
                    'oneday'=>$record['one_day']
                ];
                DB::connection('pop_express')->table($tableName)->insert($arr);
            }


            //CHECK International Code
            $RecordError = DB::connection('pop_express')->table($tableName.' AS a')
                ->leftjoin('international_codes AS b',function($join)  {
                    $join->on('b.detail_code', '=', 'a.kode_detil')
                        ->on('b.airport_code', '=', 'a.kode' );
                })
                ->whereNull('b.id')
                ->select('a.*')
                ->get();

            //UPDATE DESTINATION ID
            $Statement = "UPDATE ".$tableName." AS a
                          LEFT JOIN international_codes AS b ON a.kode=b.airport_code AND a.kode_detil=b.detail_code
                          SET a.destination_id = b.id
                          WHERE b.id IS NOT NULL";
            $Updated = DB::connection('pop_express')->statement($Statement);

            //UPDATE REGULAR ID
            $Statement = "UPDATE ".$tableName." AS a
                          LEFT JOIN third_parties AS b ON UPPER(TRIM(a.regular))=UPPER(TRIM(b.code)) AND b.deleted_at IS NULL
                          SET a.regular_id = b.id
                          WHERE b.id IS NOT NULL ";
            $Updated = DB::connection('pop_express')->statement($Statement);

            //UPDATE ONEDAY ID
            $Statement = "UPDATE ".$tableName." AS a
                          LEFT JOIN third_parties AS b ON UPPER(TRIM(a.oneday))=UPPER(TRIM(b.code)) AND b.deleted_at IS NULL
                          SET a.oneday_id = b.id
                          WHERE b.id IS NOT NULL ";
            $Updated = DB::connection('pop_express')->statement($Statement);

            //UPDATE REGULAR ID
            $Statement = "  UPDATE ".$tableName." AS a
                            LEFT JOIN zones AS b ON a.destination_id =b.international_code_id 
                            SET
                            b.zone = a.zona,
                            b.one_day_handler = a.oneday_id,
                            b.regular_handler = a.regular_id,
                            b.updated_at = '".date('Y-m-d H:i:s')."',
                            b.server_timestamp = '".date('Y-m-d H:i:s')."'
                            WHERE b.id IS NOT NULL";
            $Updated = DB::connection('pop_express')->statement($Statement);

            //INSERT
            $Statement = "INSERT INTO zones (international_code_id,zone,regular_handler,one_day_handler,created_at, server_timestamp) 
                          SELECT a.destination_id,a.zona,a.regular_id,a.oneday_id,'".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."' 
                          FROM ".$tableName." AS a
                          LEFT JOIN zones AS b ON a.destination_id=b.international_code_id 
                          LEFT JOIN international_codes AS c ON a.destination_id=c.id
                          WHERE c.id IS NOT NULL AND b.id IS NULL ";
            $Inserted = DB::connection('pop_express')->statement($Statement);

            if (count($RecordError) > 0) {
                return view('popexpress.zone.upload', compact('breadcrumbs','RecordError'));
            } else {
                return redirect('/popexpress/zone')->with(['success' => 'Total '.count($records).' Data berhasil diupload dan diperbaharui']);
            }

        }

    }

}