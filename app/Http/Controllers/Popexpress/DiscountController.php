<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Account;
use App\Models\Popexpress\Discount;
use App\Models\Popexpress\Origin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class DiscountController extends Controller {

    public function index(Request $request)
    {
        $account_id = request('account_id');
        $origin_id = request('origin_id');
        $start_date = request('start_date');
        $end_date = request('end_date');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $county = request('county');
        $district = request('district');
        $is_locker = request('is_locker');
        $override = request('override');

        $start = substr($start_date, 6, 4).'-'.substr($start_date, 3, 2).'-'.substr($start_date, 0, 2);
        $end = substr($end_date, 6, 4).'-'.substr($end_date, 3, 2).'-'.substr($end_date, 0, 2);


        $discounts = Discount::leftJoin('accounts', 'accounts.id', '=', 'discounts.account_id')
            ->leftJoin('origins', 'origins.id', '=', 'discounts.origin_id')
            ->where('discounts.deleted_at', null)
            ->when($account_id, function ($query) use ($account_id) {
                return $query->where('discounts.account_id', '=', $account_id);
            })
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('discounts.origin_id', '=', $origin_id);
            })
            ->when($start_date, function ($query) use ($start) {
                return $query->whereDate('discounts.start_date', '=', $start);
            })
            ->when($end_date, function ($query) use ($end) {
                return $query->whereDate('discounts.end_date', '=', $end);
            })
            ->when($airport_code, function ($query) use ($airport_code) {
                return $query->where('discounts.airport_code', 'LIKE', "%$airport_code%");
            })
            ->when($detail_code, function ($query) use ($detail_code) {
                return $query->where('discounts.detail_code', 'LIKE', "%$detail_code%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('discounts.province', 'LIKE', "%$province%");
            })
            ->when($county, function ($query) use ($county) {
                return $query->where('discounts.county', 'LIKE', "%$county%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('discounts.district', 'LIKE', "%$district%");
            })
            ->when($is_locker, function ($query) use ($is_locker) {
                return $query->where('discounts.is_locker', '=', $is_locker);
            })
            ->when($override, function ($query) use ($override) {
                return $query->where('discounts.override', '=', $override);
            })
            ->select(
                'discounts.*',
                'accounts.account_name as account_name',
                'origins.name as origin_name',
                DB::raw("IF(discounts.is_locker = 1, 'Yes', 'No') as locker"),
                DB::raw("IF(discounts.override = 1, 'Yes', '-') as override"),
                DB::raw("DATE_FORMAT(discounts.start_date, '%d %b %Y') AS start"),
                DB::raw("DATE_FORMAT(discounts.end_date, '%d %b %Y') AS end"),
                DB::raw("DATE_FORMAT(discounts.start_date, '%d/%m/%Y') AS start_edit"),
                DB::raw("DATE_FORMAT(discounts.end_date, '%d/%m/%Y') AS end_edit"),
                DB::raw("DATE_FORMAT(discounts.created_at, '%d-%m-%Y %H:%i:%s') AS created")
            )
            ->paginate(20);


        $accounts = Account::where('deleted_at', null)->pluck('account_name', 'id')->toArray();
        $types = ['value','percentage'];
        $origins = Origin::where('deleted_at', null)->pluck('name', 'id')->toArray();

        return view('popexpress.discounts.index', compact('accounts', 'types', 'origins', 'discounts'));

    }

    public function store(Request $request)
    {
        $id = request('id');
        $delete = request('delete');

        $account_id = request('account_id');
        $origin_id = request('origin_id');
        $start_date = request('start_date');
        $end_date = request('end_date');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $county = request('county');
        $district = request('district');
        $is_locker = request('is_locker');
        $override = request('override');
        $type = request('type');
        $value = request('value');

        $start = substr($start_date, 6, 4).'-'.substr($start_date, 3, 2).'-'.substr($start_date, 0, 2);
        $end = substr($end_date, 6, 4).'-'.substr($end_date, 3, 2).'-'.substr($end_date, 0, 2);

        if(strtotime($start) > strtotime($end))
        {
            return redirect('/popexpress/discounts')->withInput()->withErrors([
                'message' => 'End date tidak boleh lebih awal dari start date.'
            ]);
        }

        if($type == "percentage" && intval($value) > 100)
        {
            return redirect('/popexpress/discounts')->withInput()->withErrors([
                'message' => 'Percentage tidak bisa lebih dari 100'
            ]);
        }

        if(empty($id)) {

            $this->validate(request(), [
                'account_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'type' => 'required',
                'value' => 'required|numeric|min:1',
            ],[
                'account_id.required' => 'Account diperlukan.',
                'start_date.required' => 'Start date diperlukan.',
                'end_date.required' => 'End date diperlukan.',
                'type.required' => 'Type diperlukan.',
                'value.required' => 'Value diperlukan.',
                'value.numeric' => 'Value harus angka.',
                'value.min' => 'Minimal value 1.',
            ]);

            $discount = new Discount();
            $discount->account_id = $account_id;
            $discount->origin_id = (!empty($origin_id) ? $origin_id : 0);
            $discount->start_date = $start;
            $discount->end_date = $end;
            $discount->airport_code = $airport_code;
            $discount->detail_code = $detail_code;
            $discount->province = $province;
            $discount->county = $county;
            $discount->district = $district;
            $discount->is_locker = (!empty($is_locker) ? $is_locker : 0);
            $discount->override = (!empty($override) ? $override : 0);
            $discount->type = $type;
            $discount->value = $value;
            $discount->server_timestamp = date("Y-m-d H:i:s");

            if($discount->save()) {
                $data = $discount->getAttributes();
                $key = $discount->id;
                $module = 'discounts';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data discounts telah berhasil disimpan.');
        } else {
            $discount = Discount::where('id', '=', $id)->first();
            $old = $discount->getAttributes();

            $this->validate(request(), [
                'account_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'type' => 'required',
                'value' => 'required|numeric|min:1',
            ],[
                'account_id.required' => 'Account diperlukan.',
                'start_date.required' => 'Start date diperlukan.',
                'end_date.required' => 'End date diperlukan.',
                'type.required' => 'Type diperlukan.',
                'value.required' => 'Value diperlukan.',
                'value.numeric' => 'Value harus angka.',
                'value.min' => 'Value tidak boleh 0.',
            ]);

            $discount->account_id = $account_id;
            $discount->origin_id = $origin_id;
            $discount->start_date = $start;
            $discount->end_date = $end;
            $discount->airport_code = $airport_code;
            $discount->detail_code = $detail_code;
            $discount->province = $province;
            $discount->county = $county;
            $discount->district = $district;
            $discount->is_locker = (!empty($is_locker) ? $is_locker : 0);
            $discount->override = (!empty($override) ? $override : 0);
            $discount->type = $type;
            $discount->value = $value;
            $discount->server_timestamp = date("Y-m-d H:i:s");
            $discount->save();
            $new = $discount->getAttributes();
            $diff = array_diff_assoc($old, $new);

            if($discount) {
                $key = $id;
                $module = 'discounts';
                $type = 'edit';
                $jsonBefore = json_encode($old);
                $jsonAfter = json_encode($diff);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data discounts telah berhasil diperbarui.');

        }

        return redirect('/popexpress/discounts');
    }

}