<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\Route;
use App\Models\UserGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class RouteController extends Controller
{

    protected $accessAdd = false;
    protected $allowedGroups = [
        'add' => ['popexpress_admin', 'popexpress_operation_head']
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userid = Auth::user()->id;
            $allowed = $this->allowedGroups['add'];
            $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                ->where('user_groups.deleted_at', null)
                ->where('user_groups.user_id', $userid)
                ->where(function($query) use ($allowed){
                    foreach ($allowed as $allow) {
                        $query->orWhere('groups.name', '=', $allow);
                    }
                })
                ->first();
            $this->accessAdd = (!is_null($userGroups) ? true : false);

            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $branch_id = request('branch_id');
        $branch_destination_id = request('branch_destination_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $county = request('county');
        $district = request('district');
        $is_locker = request('is_locker');

        if(is_null($branch_id) && is_null($branch_destination_id)) {
            $routes = [];
        } else {
            $routes = Route::leftJoin('branches', 'branches.id', '=', 'routes.firstmile_branch_id')
                ->where('routes.deleted_at', null)
                ->select(
                    'routes.*',
                    'branches.name as branch_name'
                )
                ->when($branch_id, function ($query) use ($branch_id) {
                    return $query->where('routes.firstmile_branch_id', '=', $branch_id);
                })
                ->when($branch_destination_id, function ($query) use ($branch_destination_id) {
                    return $query->where(function($query) use ($branch_destination_id) {
                        $query->where('routes.routing', 'LIKE', "%,$branch_destination_id]")->orWhere('routes.routing', 'LIKE', "[$branch_destination_id]");
                    });
                })
                ->when($airport_code, function ($query) use ($airport_code) {
                    return $query->where('routes.airport_code', 'LIKE', "%$airport_code%");
                })
                ->when($detail_code, function ($query) use ($detail_code) {
                    return $query->where('routes.detail_code', 'LIKE', "%$detail_code%");
                })
                ->when($province, function ($query) use ($province) {
                    return $query->where('routes.province', 'LIKE', "%$province%");
                })
                ->when($county, function ($query) use ($county) {
                    return $query->where('routes.county', 'LIKE', "%$county%");
                })
                ->when($district, function ($query) use ($district) {
                    return $query->where('routes.district', 'LIKE', "%$district%");
                })
                ->when($is_locker, function ($query) use ($is_locker) {
                    return $query->where('routes.is_locker', '=', $is_locker);
                })
                ->paginate(20);
        }

        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();
        $branchList = [];
        foreach ($branches as $branch) {
            $branchList[$branch->id] = [
                'id' => $branch->id,
                'code' => $branch->code,
                'name' => $branch->name,
                'label' => $branch->code.' - '.$branch->name,
                'type' => $branch->type,
            ];
        }

        $accessAdd = $this->accessAdd;

        return view('popexpress.routes.index', compact('routes', 'branches', 'branchList', 'accessAdd'));

    }

    public function create()
    {
        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();
        return view('popexpress.routes.create', compact('branches'));
    }

    public function edit($id)
    {
        $route = Route::find($id);
        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();
        $branchList = [];
        foreach ($branches as $branch) {
            $branchList[$branch->id] = [
                'id' => $branch->id,
                'code' => $branch->code,
                'name' => $branch->name,
                'label' => $branch->code.' - '.$branch->name,
                'type' => $branch->type,
            ];
        }
        $routing = ltrim($route->routing, '[');
        $routing = rtrim($routing, ']');
        $routes = explode(',', $routing);
        return view('popexpress.routes.edit', compact('branches', 'route', 'routing', 'branchList', 'routes'));
    }

    public function store(Request $request)
    {
        $id = request('id');
        $delete = request('delete');

        $firstmile_branch_id = request('firstmile_branch_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $county = request('county');
        $district = request('district');
        $is_locker = request('is_locker');
        $routing = request('routing');

        if(empty($id)) {

            $this->validate(request(), [
                'firstmile_branch_id' => 'required',
                'routing' => 'required',
            ], [
                'firstmile_branch_id.required' => 'Origin branch diperlukan.',
                'routing.required' => 'Routing diperlukan.'
            ]);

            if(empty($airport_code) && empty($detail_code) && empty($province) && empty($county) && empty($district)) {
                return redirect('/popexpress/routes/create')->withInput()->withErrors([
                    'message' => 'Airport code, detail code, province, county, district salah satu harus terisi.'
                ]);
            }

            $route = new Route();
            $route->firstmile_branch_id = $firstmile_branch_id;
            $route->airport_code = $airport_code;
            $route->detail_code = $detail_code;
            $route->province = $province;
            $route->county = $county;
            $route->district = $district;
            $route->is_locker = ($is_locker == '1' ? 1 : 0);
            $route->routing = '['.rtrim($routing, ',').']';
            $route->server_timestamp = date('Y-m-d H:i:s');
            if($route->save()) {
                $data = $route->getAttributes();
                $key = $route->id;
                $module = 'routes';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data routes telah berhasil disimpan.');

        } else {

            $this->validate(request(), [
                'firstmile_branch_id' => 'required',
                'routing' => 'required',
            ], [
                'firstmile_branch_id.required' => 'Origin branch diperlukan.',
                'routing.required' => 'Routing diperlukan.'
            ]);

            if(empty($airport_code) && empty($detail_code) && empty($province) && empty($county) && empty($district)) {
                return redirect('/popexpress/routes/create')->withInput()->withErrors([
                    'message' => 'Airport code, detail code, province, county, district salah satu harus terisi.'
                ]);
            }

            $route = Route::where('id', $id)->first();
            $route->firstmile_branch_id = $firstmile_branch_id;
            $route->airport_code = $airport_code;
            $route->detail_code = $detail_code;
            $route->province = $province;
            $route->county = $county;
            $route->district = $district;
            $route->is_locker = ($is_locker == '1' ? 1 : 0);
            $route->routing = '['.rtrim($routing, ',').']';
            $route->server_timestamp = date('Y-m-d H:i:s');
            if($route->save()) {
                $data = $route->getAttributes();
                $key = $id;
                $module = 'routes';
                $type = 'edit';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data routes telah berhasil disimpan.');

        }

        $routing = rtrim($routing, ',');
        $lastRoute = explode(',', $routing);
        $branch_destination_id = end($lastRoute);

        return redirect()->action('Popexpress\RouteController@index',
            [
                'branch_id' => $firstmile_branch_id,
                'airport_code'=> $airport_code,
                'province' => $province,
                'district' => $district,
                'branch_destination_id' => $branch_destination_id,
                'detail_code' => $detail_code,
                'county' => $county
            ]
        );
    }

}