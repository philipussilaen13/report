<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Account;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\Customer;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Pickup;
use App\Models\Popexpress\PickupDetail;
use App\Models\Popexpress\Price;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;

class PickupDetailController extends Controller
{

    public function index(Request $request)
    {
        $awb = request('awb');
        $merchant_awb = request('merchant_awb');
        $third_party_awb = request('third_party_awb');
        $reseller = request('reseller');
        $recipient = request('recipient');
        $recipient_telephone = request('recipient_telephone');
        $recipient_email = request('recipient_email');
        $in_hub_date_start = request('hub_from');
        $in_hub_date_end = request('hub_to');
        $latest_status = request('latest_status');
        $hub_destination_id = request('hub_destination_id');
        $recipient_address = request('recipient_address');
        $postcode = request('postcode');
        $in_hub = request('in_hub');

        $start = substr($in_hub_date_start, 6, 4).'-'.substr($in_hub_date_start, 3, 2).'-'.substr($in_hub_date_start, 0, 2);
        $end = substr($in_hub_date_end, 6, 4).'-'.substr($in_hub_date_end, 3, 2).'-'.substr($in_hub_date_end, 0, 2);

        $pickupDetails = PickupDetail::leftJoin('pickups', 'pickups.id', '=', 'pickup_details.pickup_id')
            ->leftJoin(DB::raw('destinations as hub'), 'hub.id', '=', 'pickup_details.hub_destination_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'pickup_details.web_destination_id')
            ->leftJoin('branches', 'branches.id', '=', 'pickup_details.branch_id')
            ->where('pickup_details.deleted_at', null)
            ->select(
                'pickup_details.*',
                'pickup_details.id as pickupid',
                'pickup_details.pickup_id as idpickup',
                'pickups.pickup_no as pickup_no',
                DB::raw("IF(pickup_details.in_hub = 0, CONCAT(destinations.detail_code, ' - ', destinations.district, ', ', destinations.county), CONCAT(hub.detail_code, ' - ', hub.district, ', ', hub.county)) AS destination"),
                DB::raw("DATE_FORMAT(pickup_details.in_hub_date, '%d %b %Y') AS hub_date")
            )
            ->when($awb, function ($query) use ($awb) {
                return $query->where('pickup_details.awb', 'LIKE', "%$awb%");
            })
            ->when($merchant_awb, function ($query) use ($merchant_awb) {
                return $query->where('pickup_details.merchant_awb', 'LIKE', "%$merchant_awb%");
            })
            ->when($third_party_awb, function ($query) use ($third_party_awb) {
                return $query->where('pickup_details.third_party_awb', 'LIKE',"%$third_party_awb%");
            })
            ->when($reseller, function ($query) use ($reseller) {
                return $query->where('pickup_details.reseller', 'LIKE', "%$reseller%");
            })
            ->when($recipient, function ($query) use ($recipient) {
                return $query->where('pickup_details.recipient', 'LIKE', "%$recipient%");
            })
            ->when($recipient_telephone, function ($query) use ($recipient_telephone) {
                return $query->where('pickup_details.recipient_telephone', 'LIKE', "%$recipient_telephone%");
            })
            ->when($recipient_email, function ($query) use ($recipient_email) {
                return $query->where('pickup_details.recipient_email', 'LIKE', "%$recipient_email%");
            })
            ->when($postcode, function ($query) use ($postcode) {
                return $query->where('pickup_details.postcode', 'LIKE', "%$postcode%");
            })
            ->when($recipient_address, function ($query) use ($recipient_address) {
                return $query->where('pickup_details.recipient_address', 'LIKE', "%$recipient_address%");
            })
            ->when($latest_status, function ($query) use ($latest_status) {
                return $query->where('pickup_details.latest_status', '=', $latest_status);
            })
            ->when($hub_destination_id, function ($query) use ($hub_destination_id) {
                return $query->where('pickup_details.hub_destination_id', '=', $hub_destination_id);
            })
            ->when($in_hub != '', function ($query) use ($in_hub) {
                return $query->where('pickup_details.in_hub', '=', $in_hub);
            })
            ->when($in_hub_date_start, function ($query) use ($start) {
                return $query->whereDate('pickup_details.in_hub_date', '>=', $start);
            })
            ->when($in_hub_date_end, function ($query) use ($end) {
                return $query->whereDate('pickup_details.in_hub_date', '<=', $end);
            })
            ->orderBy('pickup_details.created_at', 'desc')
            ->paginate(20);

        $pickups = Pickup::where('deleted_at', null)->whereIn('pickup_status', ['requested','assigned','pending','processing'])->select('id as pickupid', 'pickup_no')->orderBy('pickup_no')->get();
        $destinations = Destination::where('deleted_at', null)->orderBy('district')->get();
        $statuses = ['new' => 'success','in hub' => 'success','consolidate' => 'success','deconsolidate' => 'success','delivery' => 'success','void' => 'danger','done' => 'primary'];

        return view('popexpress.pickup_details.index', compact('pickupDetails', 'pickups', 'destinations', 'statuses'));

    }

    public function create()
    {
        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();
        $accounts = Account::where('deleted_at', null)->orderBy('account_name')->get();
        $types = ["non-cash", "cash", "locker", "popsend-address", "popsend-locker"];
        $times = ["09.00-18.00", "18.00-21.00", "all-day"];
        $sources = [ "web", "whatsapp", "line", "email", "sms", "phone", "api"];
        $locations = Destination::where('deleted_at', null)->orderBy('district')->get();

        return view('popexpress.pickups.create', compact('branches', 'accounts', 'types', 'times', 'sources', 'locations'));
    }

    public function store(Request $request)
    {
        $id = request('id');

        $pickup_id = request('pickup_id');
        $customer_weight_rounding = request('customer_weight_rounding');
        $reseller = request('reseller');
        $recipient = request('recipient');
        $recipient_telephone = request('recipient_telephone');
        $recipient_email = request('recipient_email');
        $recipient_address = request('recipient_address');
        $postcode = request('postcode');
        $web_destination_id = request('web_destination_id');
        $hub_destination_id = request('web_destination_id');
        $price_regular = request('price_regular');
        $price_oneday = request('price_oneday');
        $web_weight = request('web_weight');
        $web_length = request('web_length');
        $web_width = request('web_width');
        $web_height = request('web_height');
        $merchant_awb = request('merchant_awb');
        $third_party_awb = request('third_party_awb');
        $description = request('description');
        $remarks = request('remarks');
        $reg_item_price = request('hub_item_price');
        $reg_rounded_weight = request('hub_rounded_weight');
        $reg_price_per_kg = request('hub_price_per_kg');
        $reg_insurance_price = request('hub_insurance_price');
        $reg_total_price = request('hub_total_price');
        $web_item_price = request('web_item_price');
        $web_rounded_weight = request('web_rounded_weight');
        $web_price_per_kg = request('web_price_per_kg');
        $web_insurance_price = request('web_insurance_price');
        $web_total_price = request('web_total_price');
        $web_service_type = request('web_service_type');
        $typeData = request('type');

        if(empty($id)) {

            $this->validate(request(), [
                'pickup_id' => 'required',
                'customer_weight_rounding' => 'required',
                'recipient' => 'required',
                'recipient_telephone' => 'required',
                'recipient_address' => 'required',
                'postcode' => 'required',
                'web_destination_id' => 'required',
                'price_regular' => 'required',
                'price_oneday' => 'required',
                'hub_rounded_weight' => 'required',
                'hub_price_per_kg' => 'required',
                'hub_total_price' => 'required',
                'web_rounded_weight' => 'required',
                'web_price_per_kg' => 'required',
                'web_total_price' => 'required',
            ], [
                'pickup_id' => 'Pickup no diperlukan.',
                'customer_weight_rounding' => 'Weight rounding diperlukan.',
                'recipient' => 'Recipient diperlukan.',
                'recipient_telephone' => 'Phone diperlukan.',
                'recipient_address' => 'Address diperlukan.',
                'postcode' => 'Postcode diperlukan.',
                'web_destination_id' => 'Destination diperlukan.',
                'price_regular' => 'Regular price diperlukan.',
                'price_oneday' => 'Oneday price diperlukan.',
                'hub_rounded_weight' => 'Hub rounded weight diperlukan.',
                'hub_price_per_kg' => 'Hub price per kg diperlukan.',
                'hub_total_price' => 'Hub total price diperlukan.',
                'web_rounded_weight' => 'Web rounded weight diperlukan.',
                'web_price_per_kg' => 'Web price per kg diperlukan.',
                'web_total_price' => 'Web total price diperlukan.',
            ]);

            if(empty($web_weight)){
                if(empty($web_length) || empty($web_width) || empty($web_height)) {
                    return redirect('/popexpress/pickups/list_pickup_details')->withInput()->withErrors([
                        'message' => 'Weight diperlukan.'
                    ]);
                }
            }

            $pickup = Pickup::where('id', $pickup_id)->first();

            $pickupDetail = new PickupDetail();
            $pickupDetail->id = CommonPopExpressHelper::generatePickupId();
            $pickupDetail->pickup_id = $pickup_id;
            $pickupDetail->branch_id = $pickup->branch_id;
            $pickupDetail->awb = $pickup_id;
            $pickupDetail->merchant_awb = $merchant_awb;
            $pickupDetail->third_party_awb = $third_party_awb;
            $pickupDetail->reseller = $reseller;
            $pickupDetail->recipient = $recipient;
            $pickupDetail->recipient_telephone = $recipient_telephone;
            $pickupDetail->recipient_email = $recipient_email;
            $pickupDetail->recipient_address = $recipient_address;
            $pickupDetail->postcode = $postcode;
            $pickupDetail->web_destination_id = $web_destination_id;
            $pickupDetail->hub_destination_id = $hub_destination_id;
            $pickupDetail->description = $description;
            $pickupDetail->remarks = $remarks;
            $pickupDetail->web_weight = $web_weight;
            $pickupDetail->web_length = $web_length;
            $pickupDetail->web_width = $web_width;
            $pickupDetail->web_height = $web_height;
            $pickupDetail->web_rounded_weight = $web_rounded_weight;
            $pickupDetail->web_service_type = $web_service_type;
            $pickupDetail->hub_service_type = $web_service_type;
            $pickupDetail->hub_weight = $web_weight;
            $pickupDetail->hub_length = $web_length;
            $pickupDetail->hub_width = $web_width;
            $pickupDetail->hub_height = $web_height;
            $pickupDetail->hub_price_per_kg = preg_replace("/[^0-9]/", '', $web_price_per_kg);
            $pickupDetail->hub_total_price = preg_replace("/[^0-9]/", '', $web_total_price);
            $pickupDetail->hub_discount_price = 0;
            $pickupDetail->hub_item_price = preg_replace("/[^0-9]/", '', $web_item_price);
            $pickupDetail->hub_insurance_price = preg_replace("/[^0-9]/", '', $web_insurance_price);
            $pickupDetail->hub_nett_price = preg_replace("/[^0-9]/", '', $web_total_price);
            $pickupDetail->web_price_per_kg = preg_replace("/[^0-9]/", '', $web_price_per_kg);
            $pickupDetail->web_total_price = preg_replace("/[^0-9]/", '', $web_total_price);
            $pickupDetail->web_discount_price = 0;
            $pickupDetail->web_item_price = preg_replace("/[^0-9]/", '', $web_item_price);
            $pickupDetail->web_insurance_price = preg_replace("/[^0-9]/", '', $web_insurance_price);
            $pickupDetail->web_nett_price = preg_replace("/[^0-9]/", '', $web_total_price);
            $pickupDetail->latest_status = 'new';
            $pickupDetail->server_timestamp = date('Y-m-d H:i:s');

            if($pickupDetail->save()) {
                $data = $pickupDetail->getAttributes();
                $key = $pickupDetail->id;
                $module = 'pickup_details';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data pickup details telah berhasil disimpan.');

        } else {

            $this->validate(request(), [
                'id' => 'required',
                'pickup_id' => 'required',
                'recipient' => 'required',
                'recipient_telephone' => 'required',
                'recipient_address' => 'required',
                'postcode' => 'required',
                'web_destination_id' => 'required',
                'web_rounded_weight' => 'required',
                'web_price_per_kg' => 'required',
                'web_total_price' => 'required',
            ], [
                'id' => 'Pickup Detail Id diperlukan.',
                'pickup_id' => 'Pickup no diperlukan.',
                'recipient' => 'Recipient diperlukan.',
                'recipient_telephone' => 'Phone diperlukan.',
                'recipient_address' => 'Address diperlukan.',
                'postcode' => 'Postcode diperlukan.',
                'web_destination_id' => 'Destination diperlukan.',
                'web_rounded_weight' => 'Web rounded weight diperlukan.',
                'web_price_per_kg' => 'Web price per kg diperlukan.',
                'web_total_price' => 'Web total price diperlukan.',
            ]);

            $pickupDetail = PickupDetail::where('id', $id)->first();
            $old = $pickupDetail->getAttributes();

            $pickup = Pickup::where('id', $pickup_id)->first();
            $pickupDetail->pickup_id = $pickup_id;
            $pickupDetail->branch_id = $pickup->branch_id;
            $pickupDetail->merchant_awb = $merchant_awb;
            $pickupDetail->third_party_awb = $third_party_awb;
            $pickupDetail->reseller = $reseller;
            $pickupDetail->recipient = $recipient;
            $pickupDetail->recipient_telephone = $recipient_telephone;
            $pickupDetail->recipient_email = $recipient_email;
            $pickupDetail->recipient_address = $recipient_address;
            $pickupDetail->postcode = $postcode;
            $pickupDetail->web_destination_id = $web_destination_id;
            $pickupDetail->description = $description;
            $pickupDetail->remarks = $remarks;
            $pickupDetail->web_weight = $web_weight;
            $pickupDetail->web_length = $web_length;
            $pickupDetail->web_width = $web_width;
            $pickupDetail->web_height = $web_height;
            $pickupDetail->web_rounded_weight = $web_rounded_weight;
            $pickupDetail->web_service_type = $web_service_type;
            $pickupDetail->web_price_per_kg = preg_replace("/[^0-9]/", '', $web_price_per_kg);
            $pickupDetail->web_total_price = preg_replace("/[^0-9]/", '', $web_total_price);
            $pickupDetail->web_discount_price = preg_replace("/[^0-9]/", '', $web_price_per_kg);
            $pickupDetail->web_item_price = preg_replace("/[^0-9]/", '', $web_item_price);
            $pickupDetail->web_insurance_price = preg_replace("/[^0-9]/", '', $web_insurance_price);
            $pickupDetail->web_nett_price = preg_replace("/[^0-9]/", '', $web_total_price);
            $pickupDetail->updated_at = date('Y-m-d H:i:s');
            $pickupDetail->server_timestamp = date('Y-m-d H:i:s');

            if($pickupDetail->save()) {
                $data = $pickupDetail->getAttributes();
                $key = $pickupDetail->id;
                $module = 'pickup_details';
                $type = 'edit';
                $diffBefore = array_diff_assoc($old, $data);
                $diffAfter = array_diff_assoc($data, $old);
                $jsonBefore = json_encode($diffBefore);
                $jsonAfter = json_encode($diffAfter);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data pickup details telah berhasil disimpan.');

        }

        if($typeData == "view") {
            return redirect('/popexpress/pickups/view/'.$pickup_id);
        } else {
            return redirect('/popexpress/pickups/list_pickup_details');
        }

    }

    function getLocation(Request $request)
    {
        $type = request('type');

        if($type == "locker" || $type == "popsend-locker") {
            $is_locker = 1;
        } else {
            $is_locker = 0;
        }

        $locations = Destination::where('deleted_at', null)->where('is_locker', $is_locker)->orderBy('district')->get()->toArray();

        return response()->json($locations);

    }

    function view($id) {

        $pickup = Pickup::where('deleted_at', null)->select('pickups.id as pickupid', 'pickups.*')->where('id', $id)->first();
        $branches = Branch::where('deleted_at', null)->orderBy('name')->get();
        $accounts = Account::where('deleted_at', null)->orderBy('account_name')->get();
        $types = ["non-cash", "cash", "locker", "popsend-address", "popsend-locker"];
        $times = ["09.00-18.00", "18.00-21.00", "all-day"];
        $sources = [ "web", "whatsapp", "line", "email", "sms", "phone", "api"];
        if($pickup->pickup_type == "locker" || $pickup->pickup_type == "popsend-locker") {
            $is_locker = 1;
        } else {
            $is_locker = 0;
        }
        $locations = Destination::where('deleted_at', null)->where('is_locker', $is_locker)->orderBy('district')->get();
        $statuses = ['requested', 'assigned', 'picked', 'pending', 'processing', 'closed', 'canceled'];

        return view('popexpress.pickups.view', compact('pickup', 'branches', 'accounts', 'types', 'times', 'sources', 'locations', 'pickup', 'statuses'));

    }

    function get_destination_price(Request $request) {

        $destination_id = request('destinationid');
        $pickup_id = request('pickupid');

        $price = Price::leftJoin('destinations', 'destinations.id', '=', 'prices.destination_id')
            ->where('prices.deleted_at', null)
            ->where('prices.destination_id', $destination_id)
            ->select(
                'prices.*',
                'destinations.airport_code',
                'destinations.detail_code',
                'destinations.province',
                'destinations.county',
                'destinations.district',
                'destinations.is_locker'
            )
            ->orderBy('prices.id', 'desc')
            ->first();

        if(!is_null($price)) {
            $pickup = Pickup::where('id',$pickup_id)->select('id', 'account_id')->first();
            $destination = Destination::where('id', $destination_id)->select('id', 'origin_id')->first();
            $destinations = [
                'id' => $price->destination_id,
                'airport_code' => $price->airport_code,
                'detail_code' => $price->detail_code,
                'province' => $price->province,
                'county' => $price->county,
                'district' => $price->district,
                'is_locker' => $price->is_locker,
                'price_id' => $price->id,
            ];
            $afterDiscount = CommonPopExpressHelper::getDiscount($pickup->account_id, $destination->origin_id, $destinations);
            $data = ['regular' => intval($afterDiscount['regular']), 'one_day' => intval($afterDiscount['one'])];
        } else {
            $data = ['regular' => 0, 'one_day' => 0];
        }
        return response()->json($data);

    }

    function get_account(Request $request) {

        $pickup_id = request('pickupid');

        $pickup = Pickup::leftJoin('customers', 'customers.id', '=', 'pickups.customer_id')
            ->where('pickups.id',$pickup_id)
            ->select('pickups.id', 'pickups.account_id', 'pickups.customer_id', 'customers.weight_rounding')
            ->first();

        $weight_rounding = (!is_null($pickup) ? $pickup->weight_rounding : 0);

        return response()->json($weight_rounding);

    }

}