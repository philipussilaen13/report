<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 16/06/18
 * Time: 02.51
 */

// please refer to this article : https://medium.com/@Miqubel/mastering-firebase-notifications-36a3ffe57c41
namespace App\Http\Controllers;


use Illuminate\Http\Request;

class NotificationController extends Controller {

    public function pushNotificationToSubscriberFCM($notif, $param, $fields) {

        #API access key from Google API's Console
        define( 'API_ACCESS_KEY', 'AAAAUYtjWQ0:APA91bGwW1TobozTaxsGAKILcoWzQEJC2hnw2o43wNzdc0z4tC5DoeRmE_UxB_O-Wt8OV90ZWf9ToY9ziBF6L6LPP9FWT9T92VYcfwboi8r19lwqrf1ahu9Lx3vc-vwa3GqIeAh7bUzACfT9JIXSq_xcGbdHZAhkZA' );

        // Angga Dev
        $angga_dev = 'eJ9kETrAxZA:APA91bFqjSnp0DvnnsFlO3HzvIJtiG6AS2O0DYrlbwdoIUiHR7Yty7pXBEcCmY3OgVCQxua0aT37w11u55Q1TLBYaQsmFfYVYxb2BiV2kkwGeoV1lHTu7YTRPo1-Jv8H15wznQn-XDr8';
//        $topicDev = '/topics/topicDevelopment';

//        $to = $topicDev;

//        $fields = array (
//            'to'		=> $to,
//            'notification'	=> $notif,
//            'data' => $param
//        );

        $headers = array (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

//        return ApiHelper::buildResponse(200, 'Success', json_decode($result, false));
        return $result;
    }

    public function sendPushArticle($article) {

        $topicDev = '/topics/topicDevelopment';
        $to = $topicDev;

        #prep the bundle
        $notification_body = array (
            'title'	=> $article->promo_content->notification_title,
            'body' 	=> $article->promo_content->notification_subtitle,
            'icon'	=> 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $data_body = array (
            'id'=> $article->id,
            'timestamp'=> '',
            'img_url'=> '',
            'type'=> 'article'
        );

        $fields = array (
            'to'		=> $to,
            'notification'	=> $notification_body,
            'data' => $data_body
        );

        $push = new NotificationController();
        $response = $push->pushNotificationToSubscriberFCM($notification_body, $data_body, $fields);

        return $response;
    }

    public function sendPushNotification(Request $request) {
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $input = $request->input();
        $fcmDestination = $request->input('topic', null);
        $title = $request->input('title', null);
        $body = $request->input('body', null);
        $type = $request->input('type', null);

        if ($fcmDestination == 'development') {
            $to = '/topics/topicDevelopment';
        } else if ($fcmDestination == 'indonesia') {
            $to = '/topics/topicIndonesia';
        } else if ($fcmDestination == 'malaysia') {
            $to = '/topics/topicMalaysia';

        }else if ($fcmDestination == 'development-android') {
            $to = '/topics/topicDevelopmentAndroid';
        } else if ($fcmDestination == 'indonesia-android') {
            $to = '/topics/topicIndonesiaAndroid';
        } else if ($fcmDestination == 'malaysia-android') {
            $to = '/topics/topicMalaysiaAndroid';
        }

        #prep the bundle
        $notification_body = array (
            'title'	=> $title,
            'body' 	=> $body,
            'icon'	=> 'myicon', /*Default Icon*/
            'sound' => 'mySound' /*Default sound*/
        );

        if($type == 'ios'){
            // IOS - skipped the newer version android app 2.1.2-50
            $data_body = array (
                'id'=> 114,
                'timestamp'=> '',
                'img_url'=> ''
            );
            
            $fields = array (
                'to'	=> $to,
                'notification' => $notification_body,
                'data' => $data_body
            );

        }else{
            // Payload for ANDROID, contain type to open specific screen
            $data_body = array (
                'title'	=> $title,
                'body' 	=> $body,
                'id'=> 114,
                'timestamp'=> '',
                'img_url'=> '',
                'type'=> $type
            );
            
            $fields = array (
                'to' => $to,
                'data' => $data_body
            );
        }

        $responsee = NotificationController::pushNotificationToSubscriberFCM($notification_body, $data_body, $fields);
        // parse data to view
        $data = [];
        $data['response'] = $responsee;

        $response->data = $data;
        $response->isSuccess = true;
        return response()->json($response);
    }
}