<?php

namespace App\Http\Controllers\PopApps;

use App\Http\Helpers\ApiPayment;
use App\Http\Helpers\Helper;
use App\Models\PopApps\AvailableParameter;
use App\Models\PopApps\Campaign;
use App\Models\PopApps\CampaignVoucher;
use App\Models\PopApps\City;
use App\Models\PopApps\Country;
use App\Models\PopApps\Province;
use App\Models\PopApps\ReferralCampaign;
use App\Models\PopApps\User;
use App\Models\Popbox\BuildingType;
use App\Models\Popbox\LockerLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MarketingController extends Controller
{
    /**
     * @param null $type
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getHelperRule($type = null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->view = null;

        if (empty($type)){
            $response->errorMsg = 'Empty Type';
            return response()->json($response);
        }

        switch ($type){
            case 'origin_country':
            case 'origin_locker_country':
                // get available param
                $countryList = Country::get();
                $data['country'] = $countryList;
                $view = view('popapps.marketing.ajax.country',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'origin_province':
            case 'origin_locker_province':
                // get available param
                $province = Province::get();
                $data['province'] = $province;
                $view = view('popapps.marketing.ajax.province',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'origin_city':
            case 'origin_locker_city':
                // get available param
                $city = City::get();
                $data['city'] = $city;
                $view = view('popapps.marketing.ajax.city',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'origin_locker_name':
                $lockerName = LockerLocation::get();
                $data['locker'] = $lockerName;
                $view = view('popapps.marketing.ajax.locker-name',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'origin_locker_category':
                $buildingType = BuildingType::get();
                $data['buildingType'] = $buildingType;
                $view = view('popapps.marketing.ajax.locker-building-type',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'pickup_type':
                $pickupType = ['locker','address'];
                $data['data'] = $pickupType;
                $view = view('popapps.marketing.ajax.pickup-type',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'topup_method' :
                // get payment list
                $param = [];
                $paymentAPI = new ApiPayment();
                $result = $paymentAPI->getAvailableMethod($param);

                if (!empty($result) && $result->response->code==200){
                    $methodDb = $result->data;
                    $data['data'] = $methodDb;

                    $view = view('popapps.marketing.ajax.topup-method',$data)->render();
                    $response->isSuccess = true;
                    $response->view = $view;
                } else {
                    $response->errorMsg = 'Failed to Get Payment Method';
                }
                break;
            case 'transaction_month' :
                $monthList = [
                    '01' => 'January',
                    '02' => 'February',
                    '03' => 'March',
                    '04' => 'April',
                    '05' => 'May',
                    '06' => 'June',
                    '07' => 'July',
                    '08' => 'August',
                    '09' => 'September',
                    '10' => 'October',
                    '11' => 'November',
                    '12' => 'December'
                ];
                $view = view('popapps.marketing.ajax.transaction-month',compact('monthList'))->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'transaction_day' :
                $dayList = [
                    '1' => 'Monday',
                    '2' => 'Tuesday',
                    '3' => 'Wednesday',
                    '4' => 'Thursday',
                    '5' => 'Friday',
                    '6' => 'Saturday',
                    '7' => 'Sunday',
                ];
                $view = view('popapps.marketing.ajax.transaction-day',compact('dayList'))->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'transaction_year' :
                $currentYear = date('Y');
                $yearList = [];
                for ($i=0;$i<10;$i++){
                    $yearList[] = $currentYear+$i;
                }

                $view = view('popapps.marketing.ajax.transaction-year',compact('yearList'))->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'transaction_type':
                $pickupType = ['popsend','popsafe'];
                $data['data'] = $pickupType;
                $view = view('popapps.marketing.ajax.transaction-type',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            default :
                $response->errorMsg = 'Failed. Undefined Type';
                break;
        }

        return response()->json($response);
    }

    /**
     * Get Campaign Management
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCampaign(Request $request){
        // get all campaign DB
        $campaignDb = Campaign::orderBy('created_at','desc')->paginate(20);

        $nowDate = date('Y-m-d H:i:s');

        // get list for calendar
        $calendarCampaignDb = Campaign::where('status','1')
            ->where('end_time','>',$nowDate)
            ->orderBy('priority','DESC')
            ->orderBy('created_at','DESC')
            ->get();

        $calendarList = [];
        foreach ($calendarCampaignDb as $item) {
            $tmp = [];
            $tmp['name'] = $item->name;
            $tmp['start_date'] = date('Y-m-d H:i:s',strtotime($item->start_time));
            $tmp['end_date'] = date('Y-m-d H:i:s',strtotime($item->end_time));
            $color = Helper::getColor($item->id);
            $tmp['color'] = "#".$color[0].$color[1].$color[2];
            $calendarList[] = $tmp;
        }

        // parse data to view
        $data = [];
        $data['campaigns'] = $campaignDb;
        $data['calendarList'] = $calendarList;

        return view('popapps.marketing.campaign',$data);
    }

    /**
     * Get Form Add Campaign
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAddCampaign(Request $request){
        // set parameter data
        $statusList  = ['1'=>'active','0'=>'disabled'];
        $promoType = ['potongan','deposit'];
        $typeList = ['percent','fixed'];
        $categoryList = ['fixed','cutback'];
        $limitType = ['all','member','voucher'];
        $voucherRequired = [
            '0' => 'Not Required',
            '1' => 'Required Voucher/Promo Code'
        ];

        // parse data to view
        $data = [];
        $data['statusList'] = $statusList;
        $data['promoType'] = $promoType;
        $data['typeList'] = $typeList;
        $data['categoryList'] = $categoryList;
        $data['limitType'] = $limitType;
        $data['voucherRequired'] = $voucherRequired;

        return view('popapps.marketing.campaign-add',$data);
    }

    /**
     * Get Form Add Campaign
     * @param Request $request
     * @param $campaignId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditCampaign(Request $request,$campaignId){
        $campaignDb = Campaign::with('parameters')->find($campaignId);
        if (!$campaignDb){
            $request->session()->flash('error','Invalid Campaign');
            return back();
        }

        // set parameter data
        $statusList  = ['disabled', 'enabled'];
        $promoType = ['potongan','deposit'];
        $typeList = ['percent','fixed'];
        $categoryList = ['fixed','cutback'];
        $limitType = ['all','member','voucher'];
        $voucherRequired = [
            '0' => 'Not Required',
            '1' => 'Required Voucher/Promo Code'
        ];

        // get all available param
        $availableParamDb = AvailableParameter::get();
        $availableParamList = [];
        $paramCategory = [];
        foreach ($availableParamDb as $param){
            if (!isset($paramCategory[$param->param_name]))
            {
                $i = count($paramCategory);
                $paramCategory[$param->param_name] = $i;
            }
            $i = $paramCategory[$param->param_name];

            $availableParamList[$i]['paramCategory'] = utf8_encode($param->param_name);
            $availableParamList[$i]['paramName'][] = array("id"=>$param->id,"name" => $param->name, "description" => ucfirst(utf8_encode($param->description)), "type"=> $param->type);
        }
        $operatorList = array('<'=>'Less','<='=>'Less or Same','='=>'Same','>='=>'More or Same','>'=>'More','between'=>'Between','exist'=>'Exist','except'=>'Not Exist');

        // parse data to view
        $data = [];
        $data['statusList'] = $statusList;
        $data['promoType'] = $promoType;
        $data['typeList'] = $typeList;
        $data['categoryList'] = $categoryList;
        $data['limitType'] = $limitType;
        $data['voucherRequired'] = $voucherRequired;
        $data['campaignDb'] = $campaignDb;
        $data['availableParam'] = $availableParamList;
        $data['operatorList'] = $operatorList;

        return view('popapps.marketing.campaign-add',$data);
    }

    /**
     * Get Ajax Rule Form View
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getAjaxRule(Request $request){
        $newRuleCount = $request->input('newRuleCount',1);
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->view = null;

        // get all available param
        $availableParamDb = AvailableParameter::orderBy('param_name','asc')->get();
        $availableParamList = [];
        $paramCategory = [];
        foreach ($availableParamDb as $param){
            if (!isset($paramCategory[$param->param_name]))
            {
                $i = count($paramCategory);
                $paramCategory[$param->param_name] = $i;
            }
            $i = $paramCategory[$param->param_name];

            $availableParamList[$i]['paramCategory'] = utf8_encode($param->param_name);
            $availableParamList[$i]['paramName'][] = array("id"=>$param->id,"name" => $param->name, "description" => ucfirst(utf8_encode($param->description)), "type"=> $param->type);
        }
        $operatorList = array('<'=>'Less','<='=>'Less or Same','='=>'Same','>='=>'More or Same','>'=>'More','between'=>'Between','exist'=>'Exist','except'=>'Not Exist');

        // parse data
        $data = [];
        $data['availableParam'] = $availableParamList;
        $data['operatorList'] = $operatorList;
        $data['newRuleCount'] = $newRuleCount;

        $view = view('popapps.marketing.ajax.campaign-add-rule',$data)->render();

        $response->isSuccess = true;
        $response->view = $view;

        return response()->json($response);
    }

    /**
     * Add Campaign
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAddCampaign(Request $request){
        $input = $request->input();

        // create validation
        $rules = [
            'name' => 'required',
            'description' => 'nullable',
            'startDate' => 'required|date|before:endDate',
            'endDate' => 'required|date|after:startDate',
            'status' => 'required|in:1,0',
            'priority' => 'required|numeric',
            'promoType' => 'required|in:potongan,deposit',
            'type' => 'required|in:percent,fixed',
            'amount' => 'required|numeric',
            'category' => 'required_if:promoType,transaction|in:fixed,cutback',
            'voucherRequired' => 'required|in:0,1',
            'limitType' => 'required|in:all,member,voucher',
            'limitUsage' => 'nullable|numeric',
            'minAmount' => 'nullable|numeric',
            'maxAmount' => 'nullable|numeric'
        ];
        $this->validate($request,$rules);

        $voucherRequired = $request->input('voucherRequired');

        DB::connection('popsend')->beginTransaction();

        // insert campaign and parameter
        $insertCampaign = Campaign::saveData($request);
        if (!$insertCampaign->isSuccess){
            DB::connection('popbox_agent')->rollback();
            $request->session()->flash('error',$insertCampaign->errorMsg);
            return back();
        }
        $campaignId = $insertCampaign->campaignId;

        DB::connection('popsend')->commit();

        if ($voucherRequired == 0){
            $request->session()->flash('success','Success Add Campaign');
            return redirect('popapps/marketing/campaign');
        }

        return redirect("popapps/marketing/campaign/addVoucher/$campaignId");
    }

    /**
     * Get Voucher Management Page
     * @param Request $request
     * @param null $campaignId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getAddVoucher(Request $request, $campaignId=null){
        if (empty($campaignId)){
            $request->session()->flash('error','Empty Campaign ID');
            return redirect('agent/marketing/campaign');
        }
        $campaignData = Campaign::find($campaignId);
        if (empty($campaignData)){
            $request->session()->flash('error','Campaign Not Found');
            return redirect('agent/marketing/campaign');
        }
        // get all voucher list
        $voucherList = CampaignVoucher::where('campaign_id',$campaignId)->get();

        foreach ($voucherList as $item) {

            $voucherId = $item->id;
            $countVoucherUsage = DB::connection('popsend')->table('campaign_usages')
                ->where('voucher_id', '=', $voucherId)
                ->count();
            $item->voucher_usage = $countVoucherUsage;
        }

        // parse data
        $data = [];
        $data['campaignData'] = $campaignData;
        $data['voucherList'] = $voucherList;

        return view('popapps.marketing.campaign-voucher',$data);
    }

    /**
     * Post Add Voucher
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAddVoucher(Request $request){
        $campaignId = $request->input('campaignId');
        $format = $request->input('format',null);
        $amountGenerated = $request->input('generated',1);
        $formatLength = $request->input('format_length',4);

        $voucherCodeArray = [];
        // generated Voucher Code
        for ($i=0;$i<$amountGenerated;$i++){
            $isExist = true;
            while ($isExist){
                $code = $this->generateVoucherCode($format,$formatLength,$i+1);
                $check = in_array($code,$voucherCodeArray);
                if (!$check) $isExist = false;
            }
            $voucherCodeArray[] = $code;
        }

        CampaignVoucher::saveVoucher($campaignId,$voucherCodeArray);

        $request->session()->flash('success','Success Add Voucher');
        return back();
    }

    /**
     * Get Referral Management
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getReferral(){
        // get referral campaign
        $referralCampaignDb = ReferralCampaign::orderBy('updated_at','desc')
            ->orderBy('status','desc')
            ->paginate(10);

        $userList = User::get();

        // available rule
        $availableType = ['topup','register'];
        $availableOperator = ['>','>=','=','<','<='];

        $data = [];
        $data['availableType'] = $availableType;
        $data['availableOperator'] = $availableOperator;
        $data['referralCampaignDb'] = $referralCampaignDb;
        $data['userList'] = $userList;

        return view('popapps.marketing.referral',$data);
    }

    /**
     * Post Referral Management
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postReferral(Request $request){
        $id = $request->input('id',null);
        $name = $request->input('name');
        $activeDate = $request->input('activeDate');
        $type = $request->input('type');
        $rule = $request->input('rule');
        $ruleValue = $request->input('rule-value');
        $fromAmount = $request->input('fromAmount');
        $toAmount = $request->input('toAmount');
        $code = $request->input('code',null);
        $expired = $request->input('expired');
        $status = $request->input('status');
        $description = $request->input('description');
        $descriptionToShared = $request->input('descriptionToShared');
        // $limit = $request->input('limit',0);

        DB::connection('popsend')->beginTransaction();

        // explode date
        $tmpDate = explode('-',$activeDate);
        $startDate = date('Y-m-d',strtotime($tmpDate[0]))." 00:00:00";
        $endDate = date('Y-m-d ',strtotime($tmpDate[1])). "23:59:59";

        // save to DB
        if (empty($id)){
            $dataDb = new ReferralCampaign();
        } else {
            $dataDb = ReferralCampaign::find($id);
        }
        $dataDb->campaign_name = $name;
        $dataDb->description = $description;
        $dataDb->description_to_shared = $descriptionToShared;
        $dataDb->type = $type;
        // $dataDb->limit = $limit;
        // rule
        if ($type == 'topup' && !empty($ruleValue)) {
            $tmpRule[$rule] = $ruleValue;
            $dataDb->rule = json_encode($tmpRule);
        }
        $dataDb->code = $code;
        $dataDb->start_date = $startDate;
        $dataDb->end_date = $endDate;
        $dataDb->status = $status;
        $dataDb->from_amount = $fromAmount;
        $dataDb->to_amount = $toAmount;
        $dataDb->expired = $expired;
        $dataDb->save();


        DB::connection('popsend')->commit();

        $request->session()->flash('success','Success');
        return back();
    }

    /**
     * Get Referral Transaction
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getReferralTransaction(Request $request){
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $code = $request->input('code',null);
        $fromAgent = $request->input('fromAgent',null);
        $toAgent = $request->input('toAgent',null);
        $type = $request->input('type',null);
        $status  =$request->input('status',null);

        // get referral transaction
        $referralTransactionDb = ReferralTransaction::with(['referralCampaign','fromAgent','toAgent'])
            ->whereBetween('created_at',[$startDate,$endDate])
            ->when($code,function($query) use($code){
                return $query->where('code','LIKE',"%$code%");
            })->when($fromAgent,function ($query) use($fromAgent){
                return $query->where('from_locker_id',$fromAgent);
            })->when($toAgent,function($query) use ($toAgent){
                return $query->where('to_locker_id',$toAgent);
            })->when($type,function ($query) use($type){
                return $query->where('type',$type);
            })->when($status,function($query) use($status){
                return $query->where('status',$status);
            })
            ->orderBy('created_at','desc')
            ->paginate(15);

        /*get all agent for from agent*/
        $fromLockerDb = AgentLocker::whereNotNull('referral_code')
            ->get();

        /*get all locker for to agent*/
        $toLockerDb = AgentLocker::get();

        // set type list
        $typeList = ['topup','register'];

        // set status list
        $statusList = ['PENDING','USED','FAILED'];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        $data = [];
        $data['referralTransactionDb'] = $referralTransactionDb;
        $data['parameter'] = $param;
        $data['fromLockerDb'] = $fromLockerDb;
        $data['toLockerDb'] = $toLockerDb;
        $data['statusList'] = $statusList;
        $data['typeList'] = $typeList;

        return view('agent.marketing.referral-transaction',$data);
    }

    /*======================== Private Function ========================*/
    /**
     * Generate Voucher Code
     * @param string $format
     * @param int $formatLength
     * @param int $count
     * @return string
     */
    private function generateVoucherCode($format='',$formatLength = 4,$count=1){
        $stringArr = explode('-',$format);
        $code = '';
        if (empty($formatLength)) $formatLength = 4;
        foreach ($stringArr as $item) {
            switch ($item){
                case "{ordernum}" :
                    $formatValue=sprintf("%0".$formatLength."d", $count);
                    $code .= $formatValue;
                    break;
                case "{randomnum}" :
                    $formatValue = $this->generateRandomNumber($formatLength);
                    $code .= $formatValue;
                    break;
                case "{randomalphanum}" :
                    $formatValue = $this->generateRandomAlphaNum($formatLength);
                    $code .= $formatValue;
                    break;
                default :
                    $code .= $item;
            }
        }
        return $code;
    }

    /**
     * Generate Random Number
     * @param int $length
     * @return string
     */
    private function generateRandomNumber($length = 4){
        $characters = '0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($j = 0; $j < $length; $j++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }

    /**
     * Generate Random Alpha Number
     * @param int $length
     * @return string
     */
    private function generateRandomAlphaNum($length = 4){
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ';
        $string = '';
        $max = strlen($characters) - 1;
        for ($k = 0; $k < $length; $k++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
}
