<?php

namespace App\Http\Controllers\PopApps;

use App\Models\PopApps\Delivery;
use App\Models\PopApps\DeliveryDestination;
use App\Models\PopApps\User;
use App\Models\PopApps\Voucher;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DeliveryController extends Controller
{
    /**
     * Get Delivery List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDeliveryList(Request $request){

        $groupName = Auth::user()->group->name;
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $deliveryStatus = $request->input('status',null);
        $userId = $request->input('user',null);
        $invoiceId = $request->input('invoiceId',null);
        $country = $request->input('country',null);
        $promocode = $request->input('promoCode',null);

        $voucherId = null;
        if (!empty($promocode)) {
            $voucherDb = Voucher::where('code', $promocode)->first();
            if ($voucherDb) {
                $voucherId = $voucherDb->id;
            }
        }

        $deliveryDb = Delivery::with([
                'user',
                'transaction' => function ($query){
                    $query->where('transaction_type','delivery');
                },
                'campaign_usage',
                'transaction.campaign'
            ])
            ->whereBetween('deliveries.created_at',[$startDate,$endDate])
            ->when($invoiceId,function($query) use ($invoiceId, $groupName){
                $query->where('invoice_code',$invoiceId)
                    ->when($groupName == 'pepito',function ($query) use ($groupName){
                        $query->whereRaw("origin_name LIKE '%pepito%'")
                            ->orWhereRaw("origin_name LIKE '%popular%'");
                    })
                    ->orWhereHas('destinations',function ($query) use ($invoiceId){
                       $query->where('invoice_sub_code',$invoiceId);
                    });
            })
            ->when($deliveryStatus,function ($query) use ($deliveryStatus){
                $query->where('status',$deliveryStatus);
            })
            ->when($userId,function ($query) use ($userId){
                $query->where('user_id',$userId);
            })
            ->when($country,function ($query) use ($country){
                $query->whereHas('user',function ($query) use ($country){
                    $query->where('country',$country);
                });
            })
            ->when($voucherId, function ($query) use ($voucherId){
                $query->whereHas('transaction', function ($query) use ($voucherId){
                    $query->whereHas('campaign', function ($query) use ($voucherId){
                        return $query->where('voucher_id', $voucherId);
                    });
                });
            })
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
            })
            ->orderBy('created_at','desc')
            ->paginate(20);

        // create count
        $countCreated = Delivery::whereBetween('created_at',[$startDate,$endDate])
            ->where('status','CREATED')
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
            })
            ->count();

        $countProcess = Delivery::whereBetween('created_at',[$startDate,$endDate])
            ->where('status','ON_PROCESS')
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
            })
            ->count();

        $countComplete = Delivery::whereBetween('created_at',[$startDate,$endDate])
            ->where('status','COMPLETED')
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
            })
            ->count();

        $countCancel = Delivery::whereBetween('created_at',[$startDate,$endDate])
            ->where('status','CANCEL')
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
            })
            ->count();

        $countDestination = Delivery::join('delivery_destinations', 'deliveries.id', '=', 'delivery_destinations.delivery_id')
            ->whereBetween('deliveries.created_at',[$startDate,$endDate])
            ->where('deliveries.invoice_code',$invoiceId)
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
            })
            ->count();

        foreach ($deliveryDb as $item) {
            $countDest = DeliveryDestination::where('delivery_destinations.delivery_id', '=', $item->id)
                ->count();

            $country = User::where('id', '=', $item->user_id)
                ->first();

            $item->count_destination = $countDest;
            $item->country = $country->country;
        }

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // create for filtering list
        $deliveryStatusList = ['CREATED','COMPLETED','CANCEL','ON_PROCESS'];
        if($groupName == 'pepito') {
            $countryList = [
                'ID' => 'Indonesia'
            ];
        } else {
            $countryList = [
                'ID' => 'Indonesia',
                'MY' => 'Malaysia'
            ];
        }

        // parse view
        $data = [];
        $data['deliveryDb'] = $deliveryDb->appends($request->input());
        $data['parameter'] = $param;
        $data['deliveryStatusList'] = $deliveryStatusList;
        $data['countryList'] = $countryList;
        $data['countCreated'] = $countCreated;
        $data['countProcess'] = $countProcess;
        $data['countComplete'] = $countComplete;
        $data['countCancel'] = $countCancel;
        $data['countDestination'] = $countDestination;

        return view('popapps.delivery.list',$data);
    }

    public function DownloadExcel(Request $request){

        $groupName = Auth::user()->group->name;
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $deliveryStatus = $request->input('status',null);
        $userId = $request->input('user',null);
        $invoiceId = $request->input('invoiceId',null);
        $country = $request->input('country',null);

        $deliveryDb = Delivery::with([
                'user',
                'transaction' => function ($query){
                    $query->where('transaction_type','delivery');
                },
                'campaign_usage',
                'transaction.campaign',
                'destinations'
            ])
            ->whereBetween('created_at',[$startDate,$endDate])
            ->when($invoiceId,function($query) use ($invoiceId, $groupName){
                $query->where('invoice_code',$invoiceId)
                    ->when($groupName == 'pepito',function ($query) use ($groupName){
                        $query->whereRaw("origin_name LIKE '%pepito%'")
                            ->orWhereRaw("origin_name LIKE '%popular%'");
                    })
                    ->orWhereHas('destinations',function ($query) use ($invoiceId){
                       $query->where('invoice_sub_code',$invoiceId);
                    });
            })
            ->when($deliveryStatus,function ($query) use ($deliveryStatus){
                $query->where('status',$deliveryStatus);
            })
            ->when($userId,function ($query) use ($userId){
                $query->where('user_id',$userId);
            })
            ->when($country,function ($query) use ($country){
                $query->whereHas('user',function ($query) use ($country){
                    $query->where('country',$country);
                });
            })
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
            })
            ->orderBy('created_at','desc')
            ->get();
        

        foreach ($deliveryDb as $item) {
            $countDest = DeliveryDestination::where('delivery_destinations.delivery_id', '=', $item->id)
                ->count();

            $country = User::where('id', '=', $item->user_id)
                ->first();

            $item->count_destination = $countDest;
            $item->country = $country->country;
            $item->voucher_code = "";

            if (!is_null($item->transaction->campaign)){
                $campaignUsageDb = $item->transaction->campaign;
                $item->voucher_code = $campaignUsageDb->voucher->code;
            } 
        }

        $nowDate = date('YmdHis');
        $filename = "Delivery List - $nowDate";

        Excel::create($filename, function($excel) use($deliveryDb){
            $excel->sheet('Penerima', function($sheet) use($deliveryDb){
                $row = 1;
                $sheet->row($row, ["No","Country","Invoice Master","Invoice ID","Status","User Name", "User Phone", "User Email","Pickup Type ","Pickup Name","Destination Count","Transaction Date","Price","Promo Amount","Paid Amount","Promo Code"]);
    
                $row++;
                foreach ($deliveryDb as $key => $value)
                {
//                    dd($value->destinations[0]->invoice_sub_code);
                    $destinationList = '';
                    if (sizeof($value->destinations) > 1) {
                        foreach ($value->destinations as $destination) {
                            $destinationList .= $destination->invoice_sub_code.',';
                        }
                        $destinationList = rtrim($destinationList,", ");
                    } else {
                        $destinationList = $value->destinations[0]->invoice_sub_code;
                    }

//                    dd($destinationList);

                    $nourut= $key+1;
                    $sheet->row($row, [
                        $nourut,
                        $value->country,
                        $value->invoice_code,
                        $destinationList,
                        $value->status,
                        $value->user->name,
                        $value->user->phone,
                        $value->user->email,
                        ($value->pickup_type == 'locker' ? 'Locker' : $value->pickup_type),
                        $value->origin_name,
                        $value->count_destination,
                        $value->created_at,
                        $value->transaction->total_price,
                        $value->transaction->promo_amount,
                        $value->transaction->paid_amount,
                        $value->voucher_code
                    ]);
                    $row++;
                }

            });
        })->store('xlsx', storage_path('/app'));

        return response()->download(storage_path('/app').'/'.$filename.".xlsx");
    }
}