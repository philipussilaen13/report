<?php

namespace App\Http\Controllers\PopApps;

use App\Models\PopApps\BalanceRecord;
use App\Models\PopApps\Transaction;
use App\Models\PopApps\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FinanceController extends Controller
{
    /**
     * Get User Detail for Credit / Debit Module
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function ajaxUserDetail(Request $request){
        // create default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $memberId = $request->input('memberId',null);
        $type = $request->input('type',null);

        if (empty($memberId) || empty($type)){
            $response->errorMsg = 'Empty Member Id or Empty Type';
            return response()->json($response);
        }

        // get users Db
        $usersDb = User::where('member_id',$memberId)->first();
        if (!$usersDb){
            $response->errorMsg = 'Users Not Found';
            return response()->json($response);
        }
        $userId = $usersDb->id;

        // get last transaction
        if ($type == 'credit') {
            $transactionType = ['topup', 'refund'];
            $lastTransactionDb = Transaction::with('user')
                ->where('user_id',$userId)
                ->whereIn('transaction_type',['topup','refund'])
                ->orderBy('created_at','desc')
                ->take(5)
                ->get();
        } else {
            $transactionType = ['deduct'];
            $lastTransactionDb = Transaction::with('user')
                ->where('user_id',$userId)
                ->orderBy('created_at','desc')
                ->take(5)
                ->get();
        }

        // parse data to view
        $data = [];
        $data['users'] = $usersDb;
        $data['transaction'] = $lastTransactionDb;
        $data['transactionType'] = $transactionType;
        $data['type'] = $type;

        $view = view('popapps.finance._ajaxUser',$data)->render();

        $response->isSuccess = true;
        $response->view = $view;
        $response->data = $data;
        return response()->json($response);
    }

    /**
     * Get Credit Manual
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreditManual(Request $request)
    {
        // get agent locker Only
        $usersDb = User::get();

        // parsing data to view
        $data = [];
        $data['users'] = $usersDb;
        return view('popapps.finance.credit', $data);
    }

    /**
     * Post Credit / Add Manual
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreditManual(Request $request){
        // create rules
        $rules = [
            'memberId' => 'required',
            'type' => 'required|in:topup,refund',
            'amount' => 'required|numeric',
            'remark' => 'required'
        ];
        $this->validate($request, $rules);

        $creditAmount = $request->input('amount',0);
        $memberId = $request->input('memberId');
        $transactionType = $request->input('type');
        $remarks = $request->input('remark');

        // get users
        $usersDb = User::where('member_id',$memberId)->first();
        if (!$usersDb){
            $request->session()->flash('error','Invalid Users');
            return back();
        }
        $userId = $usersDb->id;

        DB::connection('popsend')->beginTransaction();

        // create transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createTransaction($transactionType,$userId,$remarks,$creditAmount);
        if (!$createTransaction->isSuccess){
            DB::connection('popsend')->rollback();
            $request->session()->flash('error',$createTransaction->errorMsg);
            return back();
        }
        $transactionId = $createTransaction->transactionId;

        // credit balance
        $balanceModel =  new BalanceRecord();
        $credit = $balanceModel->creditDeposit($userId,$creditAmount,$transactionId);
        if (!$credit->isSuccess){
            DB::connection('popsend')->rollback();
            $request->session()->flash('error',$credit->errorMsg);
            return back();
        }

        DB::connection('popsend')->commit();
        $request->session()->flash('success',"Success Credit $creditAmount for $usersDb->name ($usersDb->phone)");
        return back();
    }

    /**
     * Get Debit Manual
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDebitManual(Request $request)
    {
        // get agent locker Only
        $usersDb = User::get();

        // parsing data to view
        $data = [];
        $data['users'] = $usersDb;
        return view('popapps.finance.debit', $data);
    }

    /**
     * Post Debit Manual
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDebitManual(Request $request){
        // create rules
        $rules = [
            'memberId' => 'required',
            'type' => 'required|in:deduct',
            'amount' => 'required|numeric',
            'remark' => 'required'
        ];
        $this->validate($request, $rules);

        $creditAmount = $request->input('amount',0);
        $memberId = $request->input('memberId');
        $transactionType = $request->input('type');
        $remarks = $request->input('remark');

        // get users
        $usersDb = User::where('member_id',$memberId)->first();
        if (!$usersDb){
            $request->session()->flash('error','Invalid Users');
            return back();
        }
        $userId = $usersDb->id;

        DB::connection('popsend')->beginTransaction();

        // create transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createTransaction($transactionType,$userId,$remarks,$creditAmount);
        if (!$createTransaction->isSuccess){
            DB::connection('popsend')->rollback();
            $request->session()->flash('error',$createTransaction->errorMsg);
            return back();
        }
        $transactionId = $createTransaction->transactionId;

        // credit balance
        $balanceModel =  new BalanceRecord();
        $credit = $balanceModel->debitDeposit($userId,$creditAmount,$transactionId);
        if (!$credit->isSuccess){
            DB::connection('popsend')->rollback();
            $request->session()->flash('error',$credit->errorMsg);
            return back();
        }

        DB::connection('popsend')->commit();
        $request->session()->flash('success',"Success Credit $creditAmount for $usersDb->name ($usersDb->phone)");
        return back();
    }
}
