<?php

namespace App\Http\Controllers\PopApps;

use App\Http\Helpers\Helper;
use App\Models\PopApps\FreeGift;
use App\Models\PopApps\FreeGiftCategory;
use App\Models\PopApps\FreeGiftQuestion;
use App\Models\PopApps\Choise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;

class FreeGiftController extends Controller
{
    /**
     * Get Free Gift Usage Data
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getListUsage(Request $request){
        // get category
        $categoryDb = FreeGiftCategory::with(['freeGifts','questions'])
            ->orderBy('id','desc')
            ->paginate(5);
        $data = [];
        $data['categoryDb'] = $categoryDb;

        return view('popapps.freegift.usage',$data);
    }

    /**
     * Get Free Gift Transaction Data
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
     
    public function getListTransaction(Request $request){
        $transactionDb = FreeGift::orderBy('id', 'asc')->paginate(10);
        $data = [];
        $data['transactionDb'] = $transactionDb;
        return view('popapps.freegift.transaction',$data);
    }
    
    /**
     * Get Free Gift Transaction Data
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
     
    public function listQuestion($categoryId, $sort='asc'){
        // // // $transactionDb = FreeGift::orderBy('id', 'asc')->paginate(10);
        $listQuestion = FreeGiftQuestion::orderBy('id', $sort)->where('category_id', $categoryId)->paginate(10);
        $data = [];
        $data['listQuestion'] = $listQuestion;
        $data['category_id'] = $categoryId;
        return view('popapps.freegift.question',$data);
    }
    
    public function updatestatusTransaction(Request $request) {
        $input = $request->all();

        $select = FreeGift::where('id', $input['transactionId'])->where('invoice_id', $input['invoiceId'])->get();
        if($select->count()) {
            $update = FreeGift::where('id', $input['transactionId'])->where('invoice_id', $input['invoiceId'])->update(['status'=>$input['status']]);
        }
        return back();
    }
    
    /**
     * Get Free Gift Category Data
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
     
    public function getListCategory($sort='asc'){
        $categoryDb = FreeGiftCategory::orderBy('id', $sort)->paginate(10);
        $data = [];
        $data['categoryDb'] = $categoryDb;
        return view('popapps.freegift.category',$data);
    }
    
    /**
     * Add Question Data And Answer(Choise)
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
     
    public function storeQuestion(Request $request){
        $input = $request->all();
        $error = [];
        $msg = '';
        $data_question = [];
        if(!$input['question'])                             $error[] = 'Question must be filled';
        if(!$input['question_type'])                        $error[] = 'Question type must be selected';
        if(isset($input['answer'])) {
            $n=1;
            foreach($input['answer'] as $answer) {
                if(!$answer)                                $error[] = 'Answer '.$n.' must be filled';
                $n++;
            }
            if(count($input['answer'])<2)                   $error[] = "Answer's count must be at least 2 answers";
        } else {
                                                            $error[] = 'Answer must be created';
        }
        $countFreeGiftQuestionCode = FreeGiftQuestion::where('question_code', $input['question_code'])->count();
        if($countFreeGiftQuestionCode)                      $error[] = 'Question Code already exist';
        if(!$error) {
            $country = $request->input('country');
            $avaliable_country = '[';
            if($country) {
                foreach($country as $countries) {
                    $avaliable_country .= '"'.$countries.'",';
                }
                $avaliable_country = substr($avaliable_country, 0, -1);
                $avaliable_country .= ']';
            } else $avaliable_country = '["",""]';
            try {
                $free_gift_question = new FreeGiftQuestion;
                $free_gift_question->category_id = $request->category_id;
                $free_gift_question->question = $request->question;
                $free_gift_question->question_code = $request->question_code;
                $free_gift_question->question_type = $request->question_type;
                $free_gift_question->avaliable_country = $avaliable_country;
                $free_gift_question->save();
                
                foreach($input['answer'] as $answer) {
                    $data_question[] = ['question_id'=>$free_gift_question->id, 'text'=>$answer];
                }
                Choise::insert($data_question);
            } catch ( \Illuminate\Database\QueryException $e) {
                dump($e->errorInfo);
            }
        } else {
            $errors = '';
            foreach($error as $err) {
                $errors .= $err.'<br />';
            }
            $msg = $errors;
        }
        exit($msg);
    }

    /**
     * Add Free Gift Category Data
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function storeCategory(Request $request){
        $input = $request->all();
        $str_extension = '';
        $error = [];
        $msg = '';
        if($request->hasFile('icon')) {
            $icon = $request->file('icon');
            $size = $request->file('icon')->getClientSize();
            $ext = $icon->getClientOriginalExtension();
            $extensions = ["jpeg", "jpg", "gif", "png"];
            if($extensions) {
                foreach($extensions as $extension) {
                    $str_extension .= $extension.',';
                }
                $str_extension = substr($str_extension, 0, -1);
            }
            if($size < 0 || $size > 204800)                 $error[] = 'File maximum size not permitted (lower than 200KB)';
            if (!in_array(strtolower($ext), $extensions))   $error[] = 'File extension not permitted (Must be one of '.$str_extension.')';
        }

        if(empty($input['title']))                          $error[] = 'Category Name must be filled';
        if(empty($input['category_id'])) {
            $statusData = 'add';
            $countFreeGiftCategory = FreeGiftCategory::where('category_code', $input['category_code'])->count();
            if(empty($input['category_code']))              $error[] = 'Category Code must be filled';
            else {
                if($countFreeGiftCategory)                  $error[] = 'Category Code already exist';                                           
            }
        } else {
            $statusData = 'edit';
            $FreeGiftCategory = FreeGiftCategory::findOrFail($input['category_id']);
            if(!$FreeGiftCategory)                          $error[] = 'Category not exist. Please refresh the page and check again...';                
        }
        if(!$error) {
            $country = $request->input('country');
            $input['avaliable_country'] = '[';
            if($country) {
                foreach($country as $countries) {
                    $input['avaliable_country'] .= '"'.$countries.'",';
                }
                $input['avaliable_country'] = substr($input['avaliable_country'], 0, -1);
                $input['avaliable_country'] .= ']';
            } else $input['avaliable_country'] = '["",""]';

            if($statusData == 'add') {
                if($request->hasFile('icon')) {
                    if($request->file('icon')->isValid()) {
                        $icon_name = date('YmdHis').".$ext";
                        $upload_path = 'iconupload';
                        $request->file('icon')->move($upload_path, $icon_name);
                        $input['icon'] = $icon_name;
                    }
                }
                $category = FreeGiftCategory::create($input);
            } else {
                
                if($request->hasFile('icon')) {
					$exist = Storage::disk('icon')->exists($FreeGiftCategory->icon);
					if(isset($FreeGiftCategory->icon) && $exist) {
						$delete = Storage::disk('icon')->delete($FreeGiftCategory->icon);
					}
                    if($request->file('icon')->isValid()) {
                        $icon_name = date('YmdHis').".$ext";
                        $upload_path = 'iconupload';
                        $request->file('icon')->move($upload_path, $icon_name);
                        $input['icon'] = $icon_name;
                    }
                }
                $FreeGiftCategory->update($input);
            }
        } else {
            $errors = '';
            foreach($error as $err) {
                $errors .= $err.'<br />';
            }
            $msg = $errors;
        }
        exit($msg);
    }

    /**
     * Get Category Detail
     * @param Request $request
     * @param $categoryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCategoryDetail(Request $request,$categoryId){
        if (empty($categoryId)){
            $request->session()->flash('error','Empty Category');
            return back();
        }

        // get category
        $categoryDb = FreeGiftCategory::find($categoryId);
        if (!$categoryDb){
            $request->session()->flash('error','Category Not Found');
            return back();
        }

        $freeGiftDb = $categoryDb->freeGifts;

        // count respondent
        $countRespondent = count($freeGiftDb);

        // list unique location
        $uniqueLocation = $freeGiftDb->unique('locker_name');
        $countUniqueLocation = count($uniqueLocation);

        // groupBy Gender
        $groupGender = $freeGiftDb->groupBy('gender');
        $maleCount = $femaleCount = 0;
        if (isset($groupGender['M'])){
            $maleCount = count($groupGender['M']);
        }
        if (isset($groupGender['F'])){
            $femaleCount = count($groupGender['F']);
        }
        // create data for graph gender
        $genderGraph = new \stdClass();
        $genderGraph->label = ['Male','Female'];
        $genderGraph->data = [$maleCount,$femaleCount];
        $genderGraph->color = ['#FF0000','#130CE8'];

        // Group By Age
        $groupAge = $freeGiftDb->groupBy('birtd_date');
        $ageGraph = new \stdClass();
        $ageGraph->label = [];
        $ageGraph->data = [];
        $ageGraph->color = [];
        $colorNumber = 100;
        foreach ($groupAge as $key => $item) {
            $ageGraph->label[] = $key;
            $ageGraph->data[] = count($groupAge[$key]);
            $color = Helper::getColor($colorNumber);
            $ageGraph->color[] = "rgb($color[0],$color[1],$color[2])";
            $colorNumber += 25;
        }

        // graph for question
        $answerQuestionDb = FreeGift::join('free_gift_answers as fga','fga.free_gift_id','=','free_gifts.id')
            ->join('free_gift_answer_details as fgad','fgad.free_gift_answer_id','=','fga.id')
            ->join('choises as c','c.id','=','fgad.choice_id')
            ->join('questions as q','q.id','=','fga.question_id')
            ->join('free_gift_categories as fgc','fgc.category_code','=','free_gifts.merchandise_code')
            ->select(DB::raw('fgc.category_code, fgc.title,q.question, c.text'))
            ->where('free_gifts.merchandise_code',$categoryDb->category_code)
            ->get();

        $groupByQuestion = $answerQuestionDb->groupBy('question');

        $graphQuestionList = [];
        $index = 1;
        foreach ($groupByQuestion as $question => $answer){
            $tmp = new \stdClass();
            $tmp->question = $question;
            $tmp->index = $index;
            $tmp->graphData = null;
            $groupByAnswer = $answer->groupBy('text');

            $graphData = new \stdClass();
            $graphData->label = [];
            $graphData->data = [];
            $graphData->color = [];
            $colorNumber = 10;
            foreach ($groupByAnswer as $key => $item) {
                $graphData->label[] = $key;
                $graphData->data[] = count($groupByAnswer[$key]);
                $color = Helper::getColor($colorNumber);
                $graphData->color[] = "rgb($color[0],$color[1],$color[2])";
                $colorNumber += 25;
            }
            $tmp->graphData = $graphData;
            $graphQuestionList[] = $tmp;
            $index++;
        }

        $data = [];
        $data['categoryDb'] = $categoryDb;
        $data['countRespondent'] = $countRespondent;
        $data['countUniqueLocation'] = $countUniqueLocation;
        $data['genderGraph'] = $genderGraph;
        $data['ageGraph'] = $ageGraph;
        $data['graphQuestionList'] = collect($graphQuestionList);
        // dd($graphQuestionList);
        return view('popapps.freegift.category-detail',$data);
    }

    /**
     * Get Update Category
     * @param Request $request
     * @param $categoryId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getUpdateCategory(Request $request, $categoryId){
        if (empty($categoryId)){
            $request->session()->flash('error','Empty Category');
            return back();
        }

        // get category
        $categoryDb = FreeGiftCategory::find($categoryId);
        if (!$categoryDb){
            $request->session()->flash('error','Category Not Found');
            return back();
        }

        $countryList = ['ID','MY'];

        $data = [];
        $data['categoryDb'] = $categoryDb;
        $data['countryList'] = $countryList;

        return view('popapps.freegift.form-update-category',$data);
    }

    /*----------------------------------------LANDING----------------------------------------*/

    public function getLanding() {
        return view('popapps.freegift.list');
    }

    public function getFreeGiftListUser(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $transaction_date = $request->input('transaction_date', null);
        $category = $request->input('freegift_category', null);
        $orderNumber = $request->input('order_number', null);
        $userId = $request->input('user_id', null);

        $startDate = null;
        $endDate = null;
        if (!empty($transaction_date)) {
            $startDate = Helper::formatDateRange($transaction_date, ',')->startDate;
            $endDate = Helper::formatDateRange($transaction_date, ',')->endDate;
        }

        $freeGiftCategoryDb = DB::connection('popsend')->table('free_gift_categories')->get();

        $freeGiftUserDb = DB::connection('popsend')->table('free_gifts')
            ->join('users', 'users.id', '=', 'free_gifts.user_id')
            ->join('free_gift_categories', 'free_gift_categories.category_code', '=', 'free_gifts.merchandise_code')
            ->when($transaction_date, function ($query) use ($startDate, $endDate){
                $query->whereBetween('free_gifts.created_at', [$startDate, $endDate]);
            })
            ->when($category, function ($query) use ($category){
                $query->where('free_gift_categories.id', $category);
            })
            ->when($orderNumber, function ($query) use ($orderNumber){
                $query->where('free_gifts.invoice_id', $orderNumber);
            })
            ->when($userId, function ($query) use ($userId){
                $query->where('free_gifts.user_id', $userId);
            })
            ->select('free_gifts.id', 'free_gifts.invoice_id', 'users.member_id', 'users.name', 'users.phone', 'free_gifts.created_at', 'free_gift_categories.title',
                'free_gifts.status', 'free_gifts.locker_name')
            ->orderBy('free_gifts.created_at', 'desc')
            ->get();


        $data = [];
        $data['param'] = $request->input();
        $data['resultList'] = $freeGiftUserDb;
        $data['freeGiftCategoryDb'] = $freeGiftCategoryDb;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }
}
