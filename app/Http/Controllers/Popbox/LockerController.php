<?php

namespace App\Http\Controllers\Popbox;

use App\Http\Helpers\ApiProjectX;
use App\Http\Helpers\Helper;
use App\Http\Helpers\CustomeLocker;
use App\Models\Company;
use App\Models\Group;
use App\Models\NewLocker\Box;
use App\Models\NewLocker\Express;
use App\Models\NewLocker\Users;
use App\Models\Popbox\BuildingType;
use App\Models\Popbox\LockerActivities;
use App\Models\Popbox\LockerLocation;
use App\Models\Agent\AgentLocker;
use App\Models\Virtual\Locker;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class LockerController extends Controller
{
    /**
     * Landing
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function landing(Request $request){
        $input = $request->input();
        // get all locker data
        $boxDb = Box::join('tb_newlocker_machinestat','tb_newlocker_machinestat.locker_id','=','tb_newlocker_box.id')
            ->where('deleteFlag','=','0')->where('tb_newlocker_box.activationFlag', '=', '1')
            ->get();

        // create status counter
        $allLockerCounter = 0;
        $onlineCounter = 0;
        $offlineCounter = 0;
        $offlineList = [];

        foreach ($boxDb as $locker){
            if ($locker->conn_status == 0){
                $offlineCounter++;
                $offlineList[] = $locker;
            }
            elseif ($locker->conn_status == 1) {
                $onlineCounter++;
            }
            $allLockerCounter++;
        }

        // get building type data
        $buildingTypeDb = BuildingType::get();

        $lockerId = $request->input('lockerId');
        $lockerType = $request->input('lockerType');
        $dateRange = $request->input('dateRange');

        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        // count date diff
        $beginDiffDate = strtotime($startDate);
        $endDiffDate = strtotime($endDate);
        $diffDate = $endDiffDate - $beginDiffDate;
        $numberOfDays = floor($diffDate / (60 * 60 *24));

        // get all locker box
        $validLockerId = [];
        foreach ($boxDb as $item){
            $validLockerId[] = $item->id;
        }

        $lockerActivitiesAll = DB::connection('popbox_db')
            ->table('locker_activities_all')
            ->join('locker_locations','locker_locations.name','=','locker_activities_all.locker_name')
            ->leftJoin('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->leftJoin('districts','districts.id','=','locker_locations.district_id')
            ->leftJoin('provinces','provinces.id','=','districts.province_id')
            ->whereIn('locker_locations.locker_id',$validLockerId)
            ->when($lockerId,function($query) use ($lockerId){
                return $query->where('locker_locations.locker_id',$lockerId);
            })->when($lockerType,function ($query) use($lockerType){
                return $query->where('locker_locations.building_type_id',$lockerType);
            })->whereBetween('locker_activities_all.storetime',[$startDate,$endDate])
            ->groupBy('locker_activities_all.locker_name')
            ->select(
                DB::raw('locker_activities_all.locker_name, 
                    locker_locations.locker_id,
                    districts.district,provinces.province, 
                    buildingtypes.building_type,
                    count(locker_activities_all.id) as delivery')
            )
            ->paginate(10);

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // parse param to view
        $data = [];
        $data['onlineCounter'] = $onlineCounter;
        $data['offlineCounter'] = $offlineCounter;
        $data['allCounter'] = $allLockerCounter;
        $data['lockerLocationDb'] =$boxDb;
        $data['buildingTypeDb'] = $buildingTypeDb;
        $data['lockerActivitiesAll'] = $lockerActivitiesAll->appends($request->input());
        $data['parameter'] = $param;
        $data['numberOfDays'] = $numberOfDays;

        return view('popbox.landing',$data);
    }

    /**
     * Get Locker Performance page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLockerPerformance(Request $request){
        $input = $request->input();
        // get locker location data
        $lockerLocationDb = LockerLocation::get();
        // get building type data
        $buildingTypeDb = BuildingType::get();

        $lockerId = $request->input('lockerId');
        $lockerType = $request->input('lockerType');
        $dateRange = $request->input('dateRange');

        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $lockerActivitesAll = DB::connection('popbox_db')
            ->table('locker_activities_all')
            ->leftJoin('locker_locations','locker_locations.name','=','locker_activities_all.locker_name')
            ->leftJoin('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->leftJoin('districts','districts.id','=','locker_locations.district_id')
            ->leftJoin('provinces','provinces.id','=','districts.province_id')
            ->when($lockerId,function($query) use ($lockerId){
                return $query->where('locker_locations.locker_id',$lockerId);
            })->when($lockerType,function ($query) use($lockerType){
                return $query->where('locker_locations.building_type_id',$lockerType);
            })->whereBetween('locker_activities_all.storetime',[$startDate,$endDate])
            ->groupBy('locker_activities_all.locker_name')
            ->select(
                DB::raw('locker_activities_all.locker_name, 
                    districts.district,provinces.province, 
                    buildingtypes.building_type,
                    count(locker_activities_all.id) as delivery , 
                    ROUND((count(locker_activities_all.id)/(DATEDIFF(CURRENT_DATE(), locker_locations.activation_date))),2) as avg_daily_usage')
            )
            ->paginate(10);

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        $data = [];
        $data['lockerLocationDb'] =$lockerLocationDb;
        $data['buildingTypeDb'] = $buildingTypeDb;
        $data['lockerActivitiesAll'] = $lockerActivitesAll->appends($request->input());
        $data['parameter'] = $param;

        return view('popbox.locker.performance',$data);
    }

    /**
     * Ajax Locker Performance
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxLockerPerformance(Request $request){
        // get data for locker activities
        $input = $request->input();

        $data = new \stdClass();
        $data->isSuccess = false;
        $data->errorMsg = null;
        $data->input = $input;

        $lockerId = $request->input('lockerId');
        $lockerType = $request->input('lockerType');
        $dateRange = $request->input('dateRange');

        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $lockerActivitesAll = DB::connection('popbox_db')
            ->table('locker_activities_all')
            ->leftJoin('locker_locations','locker_locations.name','=','locker_activities_all.locker_name')
            ->when($lockerId,function($query) use ($lockerId){
                return $query->where('locker_locations.locker_id',$lockerId);
            })->when($lockerType,function ($query) use($lockerType){
                return $query->where('locker_locations.building_type_id',$lockerType);
            })->whereBetween('locker_activities_all.storetime',[$startDate,$endDate])
            ->select('locker_activities_all.id','locker_activities_all.barcode','locker_locations.locker_id','locker_activities_all.storetime')
            ->get();

        $totalParcel = 0;


        /*-- create graph --*/
        $graphTransaction = collect($lockerActivitesAll);
        // order by created date asc
        $sorted = $graphTransaction->sortBy('storetime');
        $graphTransaction = $sorted->values();
        $dayData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->storetime)->format('z');
        });
        $weekData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->storetime)->format('W');
        });
        $monthData = $graphTransaction->groupBy(function ($date) {
            return Carbon::parse($date->storetime)->format('n');
        });

        /*========= create graph based on days =========*/
        $dayLabels = [];
        foreach ($dayData as $index => $item) {
            $tmpDate = Helper::getDateFromDay($index, date('Y'));
            $dayLabels[] = date('j M y', strtotime($tmpDate->format('j M y')));
        }
        // create data for day graph
        $dayCountPerformance = [];
        foreach ($dayLabels as $index => $date) {
            $count = 0;
            $tmpDayOfYear = date('z', strtotime($date));
            if (isset($dayData[$tmpDayOfYear])) {
                $count = count($dayData[$tmpDayOfYear]);
            }
            $dayCountPerformance[] = $count;
        }

        /*========= create graph based on week =========*/
        $begin = (int)date('W', strtotime("first day of $startDate"));
        $end = (int)date('W', strtotime("last day of $endDate"));
        $weeksThisMonth = range($begin, $end);
        $weekLabels = [];
        // create label based on week
        foreach ($weeksThisMonth as $index => $item) {
            $range = Helper::getStartAndEndDate($item, date('Y'));
            $string = date('d', strtotime($range['week_start'])) . "-" . date('d', strtotime($range['week_end'])) . " " . date('M', strtotime($range['week_end']));
            if (date('M', strtotime($range['week_start'])) != date('M', strtotime($range['week_end']))) {
                $string = date('d', strtotime($range['week_start'])) . date('M', strtotime($range['week_start'])) . "-" . date('d', strtotime($range['week_end'])) . " " . date('M', strtotime($range['week_end']));
            }
            $weekLabels[] = $string;
        }
        // create data for month graph
        $weekCountPerformance = [];
        foreach ($weeksThisMonth as $index => $item) {
            $count = 0;
            if (isset($weekData[$item])) {
                $count = count($weekData[$item]);
            }
            $weekCountPerformance[] = $count;
        }

        /*========= create graph based on month =========*/
        $monthLabels = [];
        foreach ($monthData as $index => $item) {
            $tmpDate = Helper::getDateFromMonth($index);
            $monthLabels[] = $tmpDate->format('M');
        }

        // create data for month graph
        $monthCountPerformance = [];
        foreach ($monthLabels as $index => $date) {
            $count = 0;
            $tmpMonthOfYear = date('n', strtotime($date));
            if (isset($monthData[$tmpMonthOfYear])) {
                $count = count($monthData[$tmpMonthOfYear]);
            }
            $monthCountPerformance[] = $count;
        }

        $data->isSuccess = true;
        $data->dayLabels = $dayLabels;
        $data->dayCountPerformance = $dayCountPerformance;
        $data->weekLabels = $weekLabels;
        $data->weekCountPerformance = $weekCountPerformance;
        $data->monthLabels = $monthLabels;
        $data->monthCountPerformance = $monthCountPerformance;

        return response()->json($data);
    }

    /**
     * Get Locker Location
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLocation(Request $request){
        $input = $request->input();

        $name = $request->input('name',null);
        $type = $request->input('type',null);
        $country = $request->input('country',null);
        $status = $request->input('status');


        // CHECK USER
        $customeLocker = new CustomeLocker;
        $user = Auth::user()->group->groupname;
        $groupCompany = $customeLocker->getCompany($user);
        // dd($user, $groupCompany);
        // get from box where delete flag 0
        $boxDb = Box::leftJoin('tb_newlocker_machinestat','tb_newlocker_machinestat.locker_id','=','tb_newlocker_box.id')
            ->where('deleteFlag',0)
            ->get();

        $boxLockerIdList = [];
        foreach ($boxDb as $item) {
            $boxLockerIdList[] = $item->id;
        }

        // get based on locker location
        $lockerLocationDb = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->join('countries','countries.id','=','provinces.country_id')
            ->join('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->whereIn('locker_id',$boxLockerIdList)
            ->get();

         // get based on locker location
         $countProvinceDistrict = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->whereIn('locker_id',$boxLockerIdList)
            ->select(DB::raw('COUNT(DISTINCT(district)) as total_district'), DB::raw('COUNT(DISTINCT(districts.province_id)) as total_province'))
            ->get()->toArray();

        $lockerList = [];
        foreach ($boxDb as $boxLocker){
            $tmp = new \stdClass();
            $tmp->lockerId = $boxLocker->id;
            $tmp->name = $boxLocker->name;
            $tmp->type = null;
            $tmp->country = null;
            $tmp->address = null;
            $tmp->city = null;
            $tmp->operationalHour = null;
            $tmp->capacity = null;
            $tmp->status = $boxLocker->conn_status;

                // search on locker location DB
            $filtered = $lockerLocationDb->where('locker_id',$boxLocker->id);
            $filtered = $filtered->first();
            if ($filtered){
                $lockerName = $filtered->name;
                $lockerName = str_replace('PopBox @ ','',$lockerName);
                $tmp->name = $lockerName;
                $tmp->type =  $filtered->building_type;
                $tmp->country = $filtered->country;
                $tmp->address = $filtered->address;
                $tmp->city = $filtered->district.",".$filtered->province;
                $tmp->operationalHour = $filtered->operational_hours;
                $tmp->capacity = $filtered->locker_capacity;
            }

            $lockerList[] = $tmp;
        }

        // create pagination
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $lockerPagination = collect($lockerList);

        // query name
        if (!empty($name)){
            $filtered = $lockerPagination->filter(function($value,$key) use($name){
               $isFind = false;
                if (stripos($value->name, $name) !== false) {
                    $isFind = true;
                }
                return $isFind;
            });
            $lockerPagination = $filtered;
        }

        // query type
        if (!empty($type)){
            $filtered = $lockerPagination->filter(function($value,$key) use($type){
              return $value->type == $type;
            });
            $lockerPagination = $filtered;
        }

        // query country
        if (!empty($country)){
            $filtered = $lockerPagination->filter(function($value,$key) use($country){
                return $value->country == $country;
            });
            $lockerPagination = $filtered;
        }

        if (!is_null($status)){
            $filtered = $lockerPagination->filter(function($value,$key) use($status){
                return $value->status == $status;
            });
            $lockerPagination = $filtered;
        }

        // sort locker data by name
        $sorted = $lockerPagination->sortBy(function($item,$key){
            return $item->name;
        });
        $lockerPagination = $sorted->values()->all();
        $lockerPagination = collect($lockerPagination);
        $perPage = 15;

        $currentPageSearchResults = $lockerPagination->slice(($currentPage-1)* $perPage,$perPage)->all();
        $lockerList = new LengthAwarePaginator($currentPageSearchResults,count($lockerPagination),$perPage,$currentPage,['path' => $request->url(), 'query' => $request->query()]);

        /*===========*/
        // get building type list
        $buildingTypeDb = BuildingType::get();

        $countryList = ['Indonesia','Malaysia'];

        $statusList = [
            '1' => 'Online',
            '0' => 'Offline'
        ];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }

        $compartmen = 0;
        $location = [];
        foreach ($lockerList as $key => $value) {
            $compartmen += $value->capacity;
            $location[] = $value->name;
        }

        $data = [];
        $data['parameter'] = $param;
        $data['lockerList'] = $lockerList;
        $data['buildingTypeList'] = $buildingTypeDb;
        $data['countryList'] = $countryList;
        $data['statusList'] = $statusList;
        $data['compartmen'] = $compartmen;
        $data['city'] = $countProvinceDistrict[0]['total_district'];
        $data['location'] = count($location);
        $data['province'] = $countProvinceDistrict[0]['total_province'];
        $data['user'] = $user;
        $data['groupUser'] = ['APXID'];

        return view('popbox.locker.list',$data);
    }

    public function CustomeLockerLocation(Request $request)
    {
        $input = $request->input();

        $name = $request->input('name',null);
        $type = $request->input('type',null);
        $country = $request->input('country',null);
        $status = $request->input('status');


        // CHECK USER
        $customeLocker = new CustomeLocker;
        $user = Auth::user()->group->groupname;
        $groupCompany = $customeLocker->getCompany($user);

        $dataExpress = Express::select('box_id')->distinct('box_id')->where('groupName', 'AP_EXPRESS')->get();        
        
        // $lockerID = [];
        // foreach ($dataExpress as $key => $value) {
        //     $lockerID[] = $value->box_id;
        // }

        $lockerID = [
            '3404703e47ac8cb1ca645f87bee3c0b3',
            '92457d6a5e86c3c2c09e3b4898eee9c6'
        ];

        // get from box where delete flag 0
        $boxDb = Box::leftJoin('tb_newlocker_machinestat','tb_newlocker_machinestat.locker_id','=','tb_newlocker_box.id')      ->whereIn('id', $lockerID)
            ->where('deleteFlag',0)
            ->get();
        
        $boxLockerIdList = [];
        foreach ($boxDb as $item) {
            $boxLockerIdList[] = $item->id;
        }

        // get based on locker location
        $lockerLocationDb = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->join('countries','countries.id','=','provinces.country_id')
            ->join('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->whereIn('locker_id',$boxLockerIdList)
            ->get();

        // dd($boxDb);

        // get based on locker location
        $countProvinceDistrict = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->whereIn('locker_id',$boxLockerIdList)
            ->select(DB::raw('COUNT(DISTINCT(district)) as total_district'), DB::raw('COUNT(DISTINCT(districts.province_id)) as total_province'))
            ->get()->toArray();

        $lockerList = [];
        foreach ($boxDb as $boxLocker){
            $tmp = new \stdClass();
            $tmp->lockerId = $boxLocker->id;
            $tmp->name = $boxLocker->name;
            $tmp->type = null;
            $tmp->country = null;
            $tmp->address = null;
            $tmp->city = null;
            $tmp->operationalHour = null;
            $tmp->capacity = null;
            $tmp->status = $boxLocker->conn_status;
            // dd($boxLocker->i);
                // search on locker location DB
            $filtered = $lockerLocationDb->where('locker_id',$boxLocker->id);

            $filtered = $filtered->first();
            if ($filtered){
                $lockerName = $filtered->name;
                $lockerName = str_replace('PopBox @ ','',$lockerName);
                $tmp->name = $lockerName;
                $tmp->type =  $filtered->building_type;
                $tmp->country = $filtered->country;
                $tmp->address = $filtered->address;
                $tmp->city = $filtered->district.",".$filtered->province;
                $tmp->operationalHour = $filtered->operational_hours;
                $tmp->capacity = $filtered->locker_capacity;
            }

            $lockerList[] = $tmp;
        }

        // create pagination
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $lockerPagination = collect($lockerList);

        // query name
        if (!empty($name)){
            $filtered = $lockerPagination->filter(function($value,$key) use($name){
               $isFind = false;
                if (stripos($value->name, $name) !== false) {
                    $isFind = true;
                }
                return $isFind;
            });
            $lockerPagination = $filtered;
        }

        // query type
        if (!empty($type)){
            $filtered = $lockerPagination->filter(function($value,$key) use($type){
              return $value->type == $type;
            });
            $lockerPagination = $filtered;
        }

        // query country
        if (!empty($country)){
            $filtered = $lockerPagination->filter(function($value,$key) use($country){
                return $value->country == $country;
            });
            $lockerPagination = $filtered;
        }

        if (!is_null($status)){
            $filtered = $lockerPagination->filter(function($value,$key) use($status){
                return $value->status == $status;
            });
            $lockerPagination = $filtered;
        }

        // sort locker data by name
        $sorted = $lockerPagination->sortBy(function($item,$key){
            return $item->name;
        });
        $lockerPagination = $sorted->values()->all();
        $lockerPagination = collect($lockerPagination);
        $perPage = 15;

        $currentPageSearchResults = $lockerPagination->slice(($currentPage-1)* $perPage,$perPage)->all();
        $lockerList = new LengthAwarePaginator($currentPageSearchResults,count($lockerPagination),$perPage,$currentPage,['path' => $request->url(), 'query' => $request->query()]);

        /*===========*/
        // get building type list
        $buildingTypeDb = BuildingType::get();

        $countryList = ['Indonesia','Malaysia'];

        $statusList = [
            '1' => 'Online',
            '0' => 'Offline'
        ];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }

        $compartmen = 0;
        $location = [];
        foreach ($lockerList as $key => $value) {
            $compartmen += $value->capacity;
            $location[] = $value->name;
        }

        $data = [];
        $data['parameter'] = $param;
        $data['lockerList'] = $lockerList;
        $data['buildingTypeList'] = $buildingTypeDb;
        $data['countryList'] = $countryList;
        $data['statusList'] = $statusList;
        $data['compartmen'] = $compartmen;
        $data['city'] = $countProvinceDistrict[0]['total_district'];
        $data['location'] = count($location);
        $data['province'] = $countProvinceDistrict[0]['total_province'];
        $data['user'] = $user;
        $data['groupUser'] = ['APXID'];

        return view('popbox.locker.list',$data);
    }

    /**
     * Get Delivery Activity
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDeliveryActivity(Request $request){
        $input = $request->input();

        $userDb = Auth::user();
        $groupName = $userDb->group->name;
        $companyId = null;
        if ($groupName == '3pl_admin'){
            // get company ID
            $companyId = $userDb->company->locker_company_id;
        }

        $dateRange = $request->input('dateRange');
        $lockerId = $request->input('locker');
        $expressType = $request->input('expressType');
        $parcelNumber = $request->input('parcelNumber');
        $pin = $request->input('pin');
        $phone = $request->input('phone');
        $status = $request->input('status');


        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $unixStartDate = strtotime($startDate) * 1000;
        $unixEndDate = strtotime($endDate) * 1000;

        $unixNowDateTime = time()*1000;

        // get data express from newlocker
        $expressData = DB::connection('newlocker_db')
            ->table('tb_newlocker_express as tne')
            ->leftJoin('tb_newlocker_company as tnc','tnc.id_company','=','tne.logisticsCompany_id')
            ->leftJoin('tb_newlocker_box as tnb','tnb.id','=','tne.box_id')
            ->leftJoin('tb_newlocker_mouth as tnm','tnm.id_mouth','=','tne.mouth_id')
            ->leftjoin('tb_newlocker_mouthtype as tnmt','tnmt.id_mouthtype','=','tnm.mouthType_id')
            ->whereBetween('tne.storetime',[$unixStartDate,$unixEndDate])
            ->when($companyId,function($query) use($companyId){
                return $query->where('tne.logisticsCompany_id',$companyId);
            })->when($lockerId,function($query) use($lockerId){
                return $query->where('tnb.id',$lockerId);
            })->when($expressType, function ($query) use ($expressType){
                return $query->where('tne.expressType',$expressType);
            })->when($parcelNumber, function ($query) use ($parcelNumber){
                return $query->where('tne.expressNumber','LIKE',"%$parcelNumber%");
            })->when($pin, function ($query) use ($pin){
                return $query->where('validateCode',$pin);
            })->when($phone,function ($query) use ($phone){
                return $query->where('tne.takeUserPhoneNumber',$phone);
            })->when($status, function ($query) use ($status,$unixNowDateTime){
                if ($status == 'OVERDUE') {
                    return $query->where('tne.status','IN_STORE')
                        ->where('tne.overdueTime','<',$unixNowDateTime);
                }
                else return $query->where('tne.status',$status);
            })
            // ->where('tne.expressType','COURIER_STORE')
            // ->where('tne.status','<>','IMPORTED')
            ->orderBy('tne.storetime','desc')
            ->select('tne.id','tne.expressType','tne.expressNumber','tne.customerStoreNumber','tne.takeUserPhoneNumber','tne.status','tne.groupName','tne.overdueTime','tne.storeTime',
                'tne.takeTime','tne.storeUserPhoneNumber','tne.validateCode',
                'tnc.company_name','tnc.company_type',
                'tnb.name as locker_name',
                'tnm.number AS locker_number',
                'tnmt.name AS locker_size')
            ->paginate(20);

        /*Get Data for Summary*/
        // count OVERDUE parcel
        $countOverdue = DB::connection('newlocker_db')
            ->table('tb_newlocker_express as tne')
            ->whereBetween('tne.storetime',[$unixStartDate,$unixEndDate])
            ->where('tne.status','IN_STORE')
            ->where('tne.overdueTime','<',$unixNowDateTime)
            ->when($companyId,function($query) use($companyId){
                return $query->where('tne.logisticsCompany_id',$companyId);
            })
            ->count();

        // count IN_STORE
        $countInStore = DB::connection('newlocker_db')
            ->table('tb_newlocker_express as tne')
            ->whereBetween('tne.storetime',[$unixStartDate,$unixEndDate])
            ->where('tne.status','IN_STORE')
            ->when($companyId,function($query) use($companyId){
                return $query->where('tne.logisticsCompany_id',$companyId);
            })
            ->count();

        // count CUSTOMER_TAKEN
        $countCustomerTaken = DB::connection('newlocker_db')
            ->table('tb_newlocker_express as tne')
            ->whereBetween('tne.storetime',[$unixStartDate,$unixEndDate])
            ->where('tne.status','CUSTOMER_TAKEN')
            ->when($companyId,function($query) use($companyId){
                return $query->where('tne.logisticsCompany_id',$companyId);
            })
            ->count();

        /*Set parameter for filtering*/
        // get locker list based on locker location on newlocker
        $lockerList = Box::where('deleteFlag',0)->get();

        // type list
        $typeList = [
            'COURIER_STORE' => 'LAST MILE',
            'CUSTOMER_REJECT' => 'RETURN',
            'CUSTOMER_STORE' => 'POPSEND'
        ];

        $statusList = [
            'CUSTOMER_TAKEN' => 'CUSTOMER TAKEN',
            'COURIER_TAKEN' => 'COURIER TAKEN',
            'IN_STORE' => 'IN STORE',
            'IMPORTED' => 'IMPORTED',
            'OPERATOR_TAKEN' => 'OPERATOR TAKEN',
            'OVERDUE' => 'OVERDUE'
        ];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        $data = [];
        $data['lockerActivitiesDb'] = $expressData->appends($request->input());
        $data['parameter'] = $param;
        $data['lockerList'] = $lockerList;
        $data['typeList'] = $typeList;
        $data['statusList'] = $statusList;
        $data['countOverdue'] = $countOverdue;
        $data['countInStore'] = $countInStore;
        $data['countCustomerTaken'] = $countCustomerTaken;

        return view('popbox.locker.delivery.activities',$data);
    }

    /**
     * Get Delivery Detail
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDeliveryDetailActivity(Request $request){
        $parcelId = $request->input('parcelId',null);
        if (empty($parcelId)){
            return back();
        }

        $expressData = DB::connection('newlocker_db')
            ->table('tb_newlocker_express as tne')
            ->leftJoin('tb_newlocker_company as tnc','tnc.id_company','=','tne.logisticsCompany_id')
            ->leftJoin('tb_newlocker_box as tnb','tnb.id','=','tne.box_id')
            ->leftJoin('tb_newlocker_mouth as tnm','tnm.id_mouth','=','tne.mouth_id')
            ->leftjoin('tb_newlocker_mouthtype as tnmt','tnmt.id_mouthtype','=','tnm.mouthType_id')
            ->where('tne.id',$parcelId)
            ->select('tne.id','tne.expressType','tne.expressNumber','tne.customerStoreNumber','tne.takeUserPhoneNumber','tne.status','tne.groupName','tne.overdueTime','tne.storeTime',
                'tne.takeTime','tne.storeUserPhoneNumber','tne.validateCode', 'tne.storeUser_id', 'tne.takeUser_id',
                'tnc.company_name as companyName','tnc.company_type as companyType',
                'tnb.name as lockerName',
                'tnm.number AS lockerNumber',
                'tnmt.name AS lockerSize')
            ->first();

        if (!$expressData){
            return back();
        }

        $smsList = [];

        // get SMS History
        $apiProx = new ApiProjectX();
        $getSms = $apiProx->getSMSHistoryByParcelId($parcelId);
        if (!empty($getSms)){
            if ($getSms->response->code == 200){
                $smsList = $getSms->data;
            }
        }

        $data = [];
        $data['expressData'] = $expressData;
        $data['smsList'] = $smsList;

        return view('popbox.locker.delivery.detail',$data);
    }

    /**
     * Post Change Phone
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangePhone(Request $request){
        // create validation
        $rules = [
            'phone' => 'required|numeric|digits_between:10,14',
            'parcelId' => 'required'
        ];

        $this->validate($request,$rules);

        $parcelId = $request->input('parcelId');
        $phone = $request->input('phone');

        // DB::connection('popbox_db')->beginTransaction();

        /*$lockerActivitiesDb = LockerActivities::where('id',$parcelId)->first();
        if (!$lockerActivitiesDb){
            $request->session()->flash('error','Parcel Not Found on Locker Activities');
            return back();
        }*/

        // push to prox
        $apiProx = new ApiProjectX();
        $changePhone = $apiProx->changePhoneNumber($parcelId,$phone);
        if (empty($changePhone)){
            $request->session()->flash('error','Failed Change Phone');
            return back();
        }
        if ($changePhone->response->code!=200){
            $request->session()->flash('error',$changePhone->response->message);
            return back();
        }

        // change on locker activities
        /*$lockerActivitiesDb = LockerActivities::find($parcelId);
        $lockerActivitiesDb->phone = $phone;
        $lockerActivitiesDb->save();*/

        // DB::connection('popbox_db')->commit();

        $request->session()->flash('success','Success Update Phone');
        return back();
    }

    /**
     * Re Send SMS
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postResendSMS(Request $request){
        // validate
        $rules = [
            'parcelId' => 'required'
        ];

        $this->validate($request,$rules);

        $parcelId = $request->input('parcelId');

        // push to prox
        $apiProx = new ApiProjectX();
        $resendSMS = $apiProx->reSendSMSbyParcelId($parcelId);
        if (empty($resendSMS)){
            $request->session()->flash('error','Failed Re Send SMS');
            return back();
        }
        if ($resendSMS->response->code != 200){
            $request->session()->flash('error',$resendSMS->response->message);
            return back();
        }

        $request->session()->flash('success','Success Re Send SMS');
        return back();
    }

    /**
     * Post Change Overdue
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangeOverdue(Request $request){
        $rules = [
            'parcelId' => 'required',
            'overdueDate' => 'required|date'
        ];

        $this->validate($request,$rules);

        $parcelId = $request->input('parcelId');
        $overdueDate = $request->input('overdueDate');

        // check on locker activities
        /*$lockerActivitiesDb = LockerActivities::find($parcelId);
        if (!$lockerActivitiesDb){
            $request->session()->flash('error','Parcel Id Not Found on Locker Activities');
            return back();
        }*/

        // generate unix milisecond overdue date time
        $overdueDate = date('Y-m-d 23:00:00',strtotime($overdueDate));
        $unixOverdueDateTime = strtotime($overdueDate) * 1000;

        // post to new locker prox
        $apiProx = new ApiProjectX();
        $changeOverdue = $apiProx->changeOverdueDateTime($parcelId,$unixOverdueDateTime);

        if (empty($changeOverdue)){
            $request->session()->flash('error','Failed Change Overdue');
            return back();
        }
        if ($changeOverdue->response->code != 200){
            $request->session()->flash('error',$changeOverdue->response->message);
            return back();
        }

        // change on locker activities
        /*$lockerActivitiesDb->overduetime = $overdueDate;
        $lockerActivitiesDb->save();*/

        $request->session()->flash('success','Success Change Overdue');
        return back();
    }

    /**
     * Open Door Locker
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postOpenDoor(Request $request){
        // validate
        $rules = [
            'parcelId' => 'required'
        ];

        $this->validate($request,$rules);

        $parcelId = $request->input('parcelId');

        // push to prox
        $apiProx = new ApiProjectX();
        $openRemote = $apiProx->remoteUnlockByExpress($parcelId);
        if (empty($openRemote)){
            $request->session()->flash('error','Failed Open Remote');
            return back();
        }
        if ($openRemote->response->code != 200){
            $request->session()->flash('error',$openRemote->response->message);
            return back();
        }

        $request->session()->flash('success','Success Open Remote');
        return back();
    }

    /**
     * Get Users Locker
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsers(Request $request){
        $userDb = Auth::user();
        $groupName = $userDb->group->name;
        $companyId = null;
        if ($groupName == '3pl_admin'){
            // get company ID
            $companyId = $userDb->company->locker_company_id;
        }

        $loginName = $request->input('loginName',null);
        $displayName = $request->input('displayName',null);
        $companyName = $request->input('companyName',null);
        $companyType = $request->input('companyType',null);
        $status = $request->input('status',null);

        $statusText = null;
        if (!is_null($status)){
            if ($status == 0){
                $statusText = 'active';
            } elseif ($status == 1) $statusText = 'disabled';
        }
        // get user list
        $userDb = Users::leftJoin('tb_newlocker_company','tb_newlocker_company.id_company','=','tb_newlocker_user.id_company')
            ->when($companyId,function($query) use ($companyId){
                return $query->where('tb_newlocker_user.id_company',$companyId);
            })->when($loginName,function($query) use($loginName){
                return $query->where('username','LIKE',"%$loginName%");
            })->when($displayName, function ($query) use($displayName){
                return $query->where('displayname','LIKE',"%$displayName%");
            })->when($companyName,function ($query) use($companyName){
                return $query->where('tb_newlocker_company.company_name','=',$companyName);
            })->when($companyType, function ($query) use($companyType){
                return $query->where('tb_newlocker_company.company_type','LIKE',"%$companyType%");
            })->when($statusText,function($query) use($status){
                return $query->where('tb_newlocker_user.deleteFlag',$status);
            })->orderBy('tb_newlocker_user.last_update','desc')
            ->select('tb_newlocker_user.*','tb_newlocker_company.company_name','tb_newlocker_company.company_type')
            ->paginate(20);

        // get available company
        $companyTypeList = DB::connection('newlocker_db')
            ->table('tb_newlocker_company')
            ->select(DB::raw('DISTINCT(company_type)'))
            ->get();

        // get available company group
        $companyGroupList =  DB::connection('newlocker_db')
            ->table('tb_newlocker_company')
            ->where('level','<=',2)
            ->where('company_type','LOGISTICS_COMPANY')
            ->select(DB::raw('DISTINCT(company_name), id_company'))
            ->get();

        $userType = [
            'admin' => 'Locker User & Dashboard Admin',
            'user' => 'Locker User'
        ];

        $userStatus = [
            '1' => 'Disabled',
            '0' => 'Active'
        ];

        $data = [];
        $data['userDb'] = $userDb;
        $data['companyTypeList'] = $companyTypeList;
        $data['companyGroupList'] = $companyGroupList;
        $data['userType'] = $userType;
        $data['userStatus'] = $userStatus;

        return view('popbox.locker.delivery.users',$data);
    }

    /**
     * Post Users Locker
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUsers(Request $request){
        $userId = $request->input('userId',null);
        $nowDateTime = date('Y-m-d H:i:s');
        if (empty($userId)){
            $rules = [
                'loginName' => 'required|regex:/^[0-9A-Za-z\-_]+$/',
                'displayName' => 'required',
                'password' => 'required|min:6|string',
                'rePassword' => 'required|same:password',
                'idNumber' => 'required|alpha_num',
                'userType' => 'required|in:admin,user',
                'companyId' => 'required',
                'email' => 'required_if:userType,admin'
            ];

            $this->validate($request,$rules);
            $loginName = $request->input('loginName');
            $displayName = $request->input('displayName');
            $passwordText = strtoupper($request->input('password'));
            $companyLockerId = $request->input('companyId');
            $idNumber = $request->input('idNumber');
            $userType = $request->input('userType');
            $userEmail = $request->input('email',null);
            $deleteFlag = $request->input('status',0);

            // check company ID on newlocker
            $companyLockerDb = DB::connection('newlocker_db')
                ->table('tb_newlocker_company')
                ->where('id_company',$companyLockerId)
                ->first();

            if (!$companyLockerDb){
                $request->session()->flash('error','Invalid Company Id');
                return back();
            }
            $companyName = $companyLockerDb->company_name;
            $companyAddress = $companyLockerDb->company_address;

            DB::connection('newlocker_db')->beginTransaction(); # transaction on newlocker
            DB::beginTransaction(); #transaction on report DB

            // generate param
            $tmpUserId = $companyLockerId.(time()*1000);
            $userId = \hash('haval128,5',$tmpUserId);
            $tmpPassword = $passwordText."_POPBOX_NEWLOCKER_SALT";
            $password = \hash('sha256',$tmpPassword);

            $userRole = 'LOGISTICS_COMPANY_USER';
            if ($userType == 'admin') $userRole = 'LOGISTICS_COMPANY_ADMIN';

            // insert to DB locker
            $lockerUser = new Users();
            $lockerUser->id_user = $userId;
            $lockerUser->username = $loginName;
            $lockerUser->displayname = $displayName;
            $lockerUser->phone = $passwordText;
            $lockerUser->password = $password;
            $lockerUser->groupname = $companyName;
            $lockerUser->role = $userRole;
            $lockerUser->id_company = $companyLockerId;
            $lockerUser->user_type = $companyName;
            $lockerUser->idcard_no = $idNumber;
            $lockerUser->last_update = $nowDateTime;
            $lockerUser->deleteFlag = $deleteFlag;
            $lockerUser->save();

            // insert into db report
            if ($userType == 'admin'){
                // check if group available
                $groupDb = Group::where('name','3pl_admin')->first();
                if (!$groupDb){
                    DB::connection('newlocker_db')->rollback();
                    DB::rollback();
                    $request->session()->flash('error','Group Not Found');
                    return back();
                }

                // check on company report
                $companyDb = Company::where('locker_company_id',$companyLockerId)->first();
                if (!$companyDb){
                    // insert new company
                    $companyDb = new Company();
                    $companyDb->name = $companyName;
                    $companyDb->address = $companyAddress;
                    $companyDb->locker_company_id = $companyLockerId;
                    $companyDb->save();
                }
                // insert into users report
                $reportUserDb = new User();
                $reportUserDb->name = $displayName;
                $reportUserDb->email = $userEmail;
                $reportUserDb->phone = null;
                $reportUserDb->password = Hash::make($request->input('password'));
                $reportUserDb->status = 'enable';
                $reportUserDb->group_id = $groupDb->id;
                $reportUserDb->company_id = $companyDb->id;
                $reportUserDb->locker_user_id = $userId;
                $reportUserDb->save();
            }

            $request->session()->flash('success','Success Insert New User');
        }
        else  {
            // create validation
            $rules = [
                'loginName' => 'required|alpha_num',
                'displayName' => 'required',
                'password' => 'nullable|min:6|string',
                'rePassword' => 'nullable|same:password',
                'idNumber' => 'required|alpha_num'
            ];

            $this->validate($request,$rules);

            $userId = $request->input('userId',null);
            $loginName = $request->input('loginName');
            $displayName = $request->input('displayName');
            $passwordText = $request->input('password',null);
            $companyType = $request->input('companyType');
            $idNumber = $request->input('idNumber');
            $deleteFlag = $request->input('status',0);
            $userType = $request->input('userType');

            if (!empty($passwordText)){
                $tmpPassword = strtoupper($passwordText)."_POPBOX_NEWLOCKER_SALT";
                $password = \hash('sha256',$tmpPassword);
            }

            $userRole = 'LOGISTICS_COMPANY_USER';
            if ($userType == 'admin') $userRole = 'LOGISTICS_COMPANY_ADMIN';

            // update
            $lockerUserDb = Users::where('id_user',$userId)->first();
            if (empty($lockerUserDb)){
                $request->session()->flash('error','Invalid Users');
                return back();
            }

            $lockerUser = Users::find($userId);
            $lockerUser->username = $loginName;
            $lockerUser->displayname = $displayName;
            if (!empty($passwordText)) $lockerUser->phone = $passwordText;
            if (!empty($passwordText)) $lockerUser->password = $password;
            $lockerUser->role = $userRole;
            $lockerUser->idcard_no = $idNumber;
            $lockerUser->last_update = $nowDateTime;
            $lockerUser->deleteFlag = $deleteFlag;
            $lockerUser->save();

            $userType = $lockerUser->role;
            if ($userType == 'LOGISTICS_COMPANY_ADMIN'){
                $reportUserDb = User::where('locker_user_id',$userId)->first();
                if (!$reportUserDb){
                    $request->session()->flash('error','Dashboard User Not Found');
                    return back();
                }
                $reportUserDb = User::find($reportUserDb->id);
                if (!empty($passwordText)) $reportUserDb->password = Hash::make($passwordText);
                $reportUserDb->save();
            }

            $request->session()->flash('success','Success Update Users');
        }

        DB::connection('newlocker_db')->commit();
        DB::commit();

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPickupList(Request $request){
        // get based on company ID
        $userDb = Auth::user();
        $companyId = null;
        if (!empty($userDb->company)){
            $companyDb = $userDb->company;
            $companyId = $companyDb->locker_company_id;
        }

        $expressDb = Express::with(['company','box','userStore'])
            ->where('status','=','IMPORTED')
            ->where('expressType','=','COURIER_STORE')
            ->when($companyId,function ($query) use($companyId){
                return $query->where('logisticsCompany_id',$companyId);
            })
            ->orderBy('importTime','desc')
            ->orderBy('storeTime','desc')
            ->paginate(20);

        $data = [];
        $data['expressDb'] = $expressDb;

        return view('popbox.locker.delivery.express-upload',$data);
    }

    /**
     * Download Example
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadExample(Request $request){
        $fileUrl = public_path()."/files/express_template.xls";
        return response()->download($fileUrl);
    }

    /**
     * Post Express
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postExpress(Request $request){
        $formType = $request->input('formType',null);

        // get user company
        $userDb = Auth::user();
        if (empty($userDb->company)){
            $request->session()->flash('error','User Not Have Company');
            return back();
        }
        $companyDb = $userDb->company;
        $companyId = $companyDb->locker_company_id;
        if (empty($companyId)){
            $request->session()->flash('error','Empty Company Id');
            return back();
        }

        $companyLockerDb = \App\Models\NewLocker\Company::find($companyId);
        if (!$companyLockerDb){
            $request->session()->flash('error','Invalid Company');
            return back();
        }
        $companyName = $companyLockerDb->company_name;

        if (empty($formType)) {
            // create validation
            $rules = [
                'orderNumber' => 'required|string|min:6',
                'customerPhone' => 'required|numeric|digits_between:10,15',
                'customerEmail' => 'nullable|email',
            ];

            $this->validate($request,$rules);

            // validate locker on box
            $orderNumber = $request->input('orderNumber');
            $customerPhone = $request->input('customerPhone');
            $customerEmail = $request->input('customerEmail',null);

            // create variable for parameter input to DB
            // create ID
            $unixNowMillisecond = time() * 1000;
            $expressId = $unixNowMillisecond."_POPBOX_NEWLOCKER_SALT";
            $expressId = hash('haval128,5',$expressId);

            DB::connection('newlocker_db')->beginTransaction();

            // insert to DB
            $expressDb = new Express();
            $expressDb->id = $expressId;
            $expressDb->expressType = 'COURIER_STORE';
            $expressDb->expressNumber = $orderNumber;
            $expressDb->takeUserPhoneNumber = $customerPhone;
            $expressDb->status = 'IMPORTED';
            $expressDb->groupName = $companyName;
            $expressDb->importTime = $unixNowMillisecond;
            $expressDb->lastModifiedTime = $unixNowMillisecond;
            $expressDb->logisticsCompany_id = $companyId;
            $expressDb->save();

            DB::connection('newlocker_db')->commit();

            $request->session()->flash('success','Success Add Parcel '.$orderNumber);
        } else {
            // create validation
            $rules = [
                'orderNumber.*' => 'required|string|min:6',
                'phone.*' => 'required|numeric|digits_between:10,15',
            ];

            $validator = Validator::make($request->all(),$rules);

            if ($validator->fails()){
                return redirect('popbox/locker/delivery/pickup')
                    ->withErrors($validator);
            }

            DB::connection('newlocker_db')->beginTransaction();

            // validate locker on box
            $orderNumberList = $request->input('orderNumber');
            $customerPhoneList = $request->input('phone');

            foreach ($orderNumberList as $index => $orderNumber){
                // create variable for parameter input to DB
                // create ID
                $unixNowMillisecond = time() * 1000;
                $random = rand(1,99);
                $expressId = $unixNowMillisecond.$random.$companyName;
                $expressId = hash('haval128,5',$expressId);

                $customerPhone = $customerPhoneList[$index];

                // insert to DB
                $expressDb = new Express();
                $expressDb->id = $expressId;
                $expressDb->expressType = 'COURIER_STORE';
                $expressDb->expressNumber = $orderNumber;
                $expressDb->takeUserPhoneNumber = $customerPhone;
                $expressDb->status = 'IMPORTED';
                $expressDb->groupName = $companyName;
                $expressDb->importTime = $unixNowMillisecond;
                $expressDb->lastModifiedTime = $unixNowMillisecond;
                $expressDb->logisticsCompany_id = $companyId;
                $expressDb->save();
            }

            DB::connection('newlocker_db')->commit();

            $request->session()->flash('success','Success Add Parcel ');
        }
        return redirect('popbox/locker/delivery/pickup');
    }

    /**
     * Post Excel Example
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postExcelExpress(Request $request){
        $input = $request->file();
        // create rules
        $rules = [
            'excel'=>'file'
        ];

        $this->validate($request,$rules);

        $path = $request->file('excel')->getRealPath();

        // get excel data
        $data = Excel::load($path,function ($reader){})->get();

        $orderList = [];

        if (!empty($data) && $data->count()){
            foreach ($data as $item) {
                $order = new \stdClass();

                $phone = null;
                $phoneValidate = false;
                $orderNumber = null;
                $orderNumberValidate = false;

                if (!empty($item->order_no)) {
                    $orderNumber = $item->order_no;
                    if (preg_match('/^[0-9A-Za-z.\-_]+$/',$orderNumber)) $orderNumberValidate = true;
                }
                if (!empty($item->phone)) {
                    $phone = $item->phone;
                    if (is_numeric($phone)) $phoneValidate = true;
                }

                $order->orderNumber = $orderNumber;
                $order->orderNumberValidate = $orderNumberValidate;
                $order->phone = $phone;
                $order->phoneValidate = $phoneValidate;
                $orderList[] = $order;
            }
        }
        $data = [];
        $data['orderList'] = $orderList;

        return view('popbox.locker.delivery.express-preview',$data);
    }

    public function getPaymentTransactions(Request $request)
    {
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        // get all parameter
        $lockerId = $request->input('locker', null);
        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $transactionId = $request->input('transactionId', null);
        $itemParam = $request->input('itemParam', null);
        $itemReference = $request->input('itemReference', null);

        // create parameter to send to view for data processing
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // parsing data to view
        $data = [];
        $data['parameter'] = $param;
        // $data['lockerData'] = $lockerDb;

        return view('payment.summary_payment', $data);
    }

    public function getAjaxGraph(Request $request)
    {
        set_time_limit(300);
        ini_set('memory_limit', '20480M');
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $dayLabels = [];
        $emoneyDay = [];
        $ottoDay = [];
        $bniDay = [];
        $tcashDay = [];
        $gopayDay = [];
        $emoneyWeek = [];
        $ottoWeek = [];
        $bniWeek = [];
        $tcashWeek = [];
        $gopayWeek = [];
        $emoneyMonthly = [];
        $ottoMonthly = [];
        $bniMonthly = [];
        $tcashMonthly = [];
        $gopayMonthly = [];
        $dayCountTransaction = [];
        $daySumTransaction = [];
        $weekLabels = [];
        $weekCountTransaction = [];
        $weekSumTransaction = [];
        $weekPaymentMethod = [];
        $monthLabels = [];
        $monthCountTransaction = [];
        $monthSumTransaction = [];
        $monthPaymentMethod = [];
        $transactionList = [];
        $transactionCount = 0;
        $transactionAmount = 0;
        $topupAmount = 0;
        $topupCount = 0;
        $rewardAmount = 0;
        $rewardCount = 0;
        $pulsaCount = 0;
        $pulsaSum = 0;
        $paymentCount = 0;
        $paymentSum = 0;
        $popshopCount = 0;
        $popshopSum = 0;
        $deliveryCount = 0;
        $deliverySum = 0;

        $emoneyDayCount = [];
        $ottoDayCount = [];
        $tcashDayCount = [];
        $bniDayCount = [];
        $gopayDayCount = [];

        $emoneyWeekCount = [];
        $ottoWeekCount = [];
        $tcashWeekCount = [];
        $bniWeekCount = [];
        $gopayWeekCount = [];

        $emoneyMonthlyCount = [];
        $ottoMonthlyCount = [];
        $tcashMonthlyCount = [];
        $bniMonthlyCount = [];
        $gopayMonthlyCount = [];

        $dataTableDaily = [];
        $dataTableWeekly = [];
        $dataTableMonthly = [];

        $parcelDayCount = [];
        $parcelWeeklyCount = [];
        $parcelMonthlyCount = [];

        $typeTransaction = ['popshop', 'pulsa', 'electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid', 'delivery'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward', 'commission','referral'];

        $typePulsa = ['pulsa'];
        $typePopShop = ['popshop'];
        $typePayment = ['electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid'];
        $typeRefund = ['refund', 'deduct'];
        $typeDelivery = ['delivery'];

        $transactionsDb = DB::connection('popsend')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.transaction_type','popsafe')
            ->where('transactions.client_id_companies','!=','001')
            ->orderBy('transactions.created_at', 'desc')
            ->select(DB::raw('COUNT(transactions.id) as count, SUM(transaction_items.price) as sum'))
            ->first();
        
        if ($transactionsDb){
            $deliverySum = $transactionsDb->sum;
            $deliveryCount = $transactionsDb->count;
        }

        $transactionDayGroupDb = DB::connection('popsend')
            ->table('transactions')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftjoin('popsafes', 'popsafes.id', '=', 'transaction_items.item_reference')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.client_id_companies','!=','001')
            ->whereIn('popsafes.locker_name',['DPS 01','DPS 02'])
            ->whereIn('transactions.transaction_type',['popsafe'])
            ->select(DB::raw('DATE(transactions.created_at) as date'), DB::raw('count(*) as count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.amount END) as emoney_payment, COUNT(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.id END) as emoney_count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.amount END) as otto_payment, COUNT(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.id END) as otto_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.amount END) as tcash_payment, COUNT(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.id END) as tcash_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.amount END) as gopay_payment, COUNT(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.id END) as gopay_count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.amount END) as bni_payment, COUNT(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.id END) as bni_count'), DB::raw('COUNT(distinct(popsafes.invoice_code)) as popsafe_invoice'))
            ->groupBy('date')
            ->get();

        foreach ($transactionDayGroupDb as $item){
            $dayCountTransaction[]=$item->count;
            $daySumTransaction[] = $item->sum;
            $dayLabels[] = $item->date;
            $parcelDayCount[] = $item->popsafe_invoice;

            $emoneyDay[] = ($item->emoney_payment == null) ? 0 : (int)$item->emoney_payment;
            $emoneyDayCount[] = ($item->emoney_count == null) ? 0 : (int)$item->emoney_count;

            $ottoDay[] = ($item->otto_payment == null) ? 0 : (int)$item->otto_payment;
            $ottoDayCount[] = ($item->otto_count == null) ? 0 : (int)$item->otto_count;

            $tcashDay[] = ($item->tcash_payment == null) ? 0 : (int)$item->tcash_payment;
            $tcashDayCount[] = ($item->tcash_count == null) ? 0 : (int)$item->tcash_count;

            $bniDay[] = ($item->bni_payment == null) ? 0 : (int)$item->bni_payment;
            $bniDayCount[] = ($item->bni_count == null) ? 0 : (int)$item->bni_count;

            $gopayDay[] = ($item->gopay_payment == null) ? 0 : (int)$item->gopay_payment;
            $gopayDayCount[] = ($item->gopay_count == null) ? 0 : (int)$item->gopay_count;
        }

        $transactionWeekGroupDb = DB::connection('popsend')
            ->table('transactions')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftjoin('popsafes', 'popsafes.id', '=', 'transaction_items.item_reference')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.client_id_companies','!=','001')
            ->whereIn('popsafes.locker_name',['DPS 01','DPS 02'])
            ->whereIn('transactions.transaction_type',['popsafe'])
            ->select(DB::raw("CONCAT(YEAR(transactions.created_at), '/', WEEK(transactions.created_at,3)) as date"), DB::raw('count(*) as count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.amount END) as emoney_payment, COUNT(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.id END) as emoney_count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.amount END) as otto_payment, COUNT(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.id END) as otto_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.amount END) as tcash_payment, COUNT(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.id END) as tcash_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.amount END) as gopay_payment, COUNT(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.id END) as gopay_count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.amount END) as bni_payment, COUNT(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.id END) as bni_count'), DB::raw('COUNT(distinct(popsafes.invoice_code)) as popsafe_invoice'))
            ->groupBy(DB::raw('date'))
            ->get();

        foreach ($transactionWeekGroupDb as $item){
            $weekCountTransaction[] = $item->count;
            $weekSumTransaction[] = $item->sum;
            list($year,$week) = explode('/',$item->date);
            $tmpLabel = Helper::getStartAndEndDate($week,$year);
            $tmpLabel = date('j M',strtotime($tmpLabel['week_start']))." - ".date('j M',strtotime($tmpLabel['week_end']));
            $weekLabels[] = $tmpLabel;
            // $weekPaymentMethod[] = $item->payment_method;
            $emoneyWeek[] = ($item->emoney_payment == null) ? 0 : (int)$item->emoney_payment;
            $emoneyWeekCount[] = ($item->emoney_count == null) ? 0 : (int)$item->emoney_count;
           
            $ottoWeek[] = ($item->otto_payment == null) ? 0 : (int)$item->otto_payment;
            $ottoWeekCount[] = ($item->otto_count == null) ? 0 : (int)$item->otto_count;
           
            $tcashWeek[] = ($item->tcash_payment == null) ? 0 : (int)$item->tcash_payment;
            $tcashWeekCount[] = ($item->tcash_count == null) ? 0 : (int)$item->tcash_count;

            $bniWeek[] = ($item->bni_payment == null) ? 0 : (int)$item->bni_payment;
            $bniWeekCount[] = ($item->bni_count == null) ? 0 : (int)$item->bni_count;

            $gopayWeek[] = ($item->gopay_payment == null) ? 0 : (int)$item->gopay_payment;
            $gopayWeekCount[] = ($item->gopay_count == null) ? 0 : (int)$item->gopay_count;

            $parcelWeeklyCount[] = $item->popsafe_invoice;
        }

        $transactionMonthGroupDb = DB::connection('popsend')
            ->table('transactions')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftjoin('popsafes', 'popsafes.id', '=', 'transaction_items.item_reference')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.client_id_companies','!=','001')
            ->whereIn('popsafes.locker_name',['DPS 01','DPS 02'])
            ->whereIn('transactions.transaction_type',['popsafe'])
            ->select(DB::raw("MONTH(transactions.created_at) as month, YEAR(transactions.created_at) as year"), DB::raw('count(*) as count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.amount END) as emoney_payment, COUNT(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.id END) as emoney_count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.amount END) as otto_payment, COUNT(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.id END) as otto_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.amount END) as tcash_payment, COUNT(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.id END) as tcash_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.amount END) as gopay_payment, COUNT(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.id END) as gopay_count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.amount END) as bni_payment, COUNT(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.id END) as bni_count'), DB::raw('COUNT(distinct(popsafes.invoice_code)) as popsafe_invoice'))
            ->groupBy(DB::raw('YEAR(transactions.created_at) ASC,MONTH(transactions.created_at) ASC'))
            ->get();

        foreach ($transactionMonthGroupDb as $item){
            $monthCountTransaction[] = $item->count;
            $monthSumTransaction[] = $item->sum;
            $year = $item->year;
            $month = $item->month;
            $tmpLabel = Helper::getNameFromMonth($month);
            $tmpLabel = "$tmpLabel $year";
            $monthLabels[] = $tmpLabel;
            // $monthPaymentMethod[] = $item->payment_method;
            $emoneyMonthly[] = ($item->emoney_payment == null) ? 0 : (int)$item->emoney_payment;
            $emoneyMonthlyCount[] = ($item->emoney_count == null) ? 0 : (int)$item->emoney_count;

            $ottoMonthly[] = ($item->otto_payment == null) ? 0 : (int)$item->otto_payment;
            $ottoMonthlyCount[] = ($item->otto_count == null) ? 0 : (int)$item->otto_count;
            
            $tcashMonthly[] = ($item->tcash_payment == null) ? 0 : (int)$item->tcash_payment;
            $tcashMonthlyCount[] = ($item->tcash_count == null) ? 0 : (int)$item->tcash_count;
           
            $bniMonthly[] = ($item->bni_payment == null) ? 0 : (int)$item->bni_payment;
            $bniMonthlyCount[] = ($item->bni_count == null) ? 0 : (int)$item->bni_count;

            $gopayMonthly[] = ($item->gopay_payment == null) ? 0 : (int)$item->gopay_payment;
            $gopayMonthlyCount[] = ($item->gopay_count == null) ? 0 : (int)$item->gopay_count;

            $parcelMonthlyCount[] = $item->popsafe_invoice;
        }

        // get data float
        $floatingDeposit = 0;
        $countActiveAgent = 0;
        $countAllAgent = 0;

        $paidTransactionItems = DB::connection('popsend')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->orderBy('transactions.created_at', 'desc')
            ->select('transaction_items.transaction_id','transaction_items.price','transaction_items.created_at','transactions.transaction_type as trans_type','transactions.total_price')
            ->get();

        $rewardCount = 0;
        $rewardCount = 0;
        $transactionAmount = 0;
        $transactionCount = 0;
        $pulsaCount = 0;
        $pulsaSum = 0;
        $paymentSum = 0;
        $paymentCount = 0;
        $topupAmount = 0;
        $topupCount = 0;

        foreach ($dayLabels as $key => $value) {
            $dataTableDaily[] = [
                'periode' => $value,
                'totalParcel' => $parcelDayCount[$key],
                'totalTrx' => $bniDayCount[$key] + $emoneyDayCount[$key] + $tcashDayCount[$key] + $ottoDayCount[$key] + $gopayDayCount[$key],
                'bni' => $bniDay[$key],
                'emoney' => $emoneyDay[$key],
                'tcash' => $tcashDay[$key],
                'ottopay' => $ottoDay[$key],
                'gopay' => $gopayDay[$key],
                'total' => $bniDay[$key] + $emoneyDay[$key] + $tcashDay[$key] + $ottoDay[$key] + $gopayDay[$key]

            ];
        }

        foreach ($weekLabels as $key => $value) {
            $dataTableWeekly[] = [
                'periode' => $value,
                'totalParcel' => $parcelWeeklyCount[$key],
                'totalTrx' => $bniWeekCount[$key] + $emoneyWeekCount[$key] + $tcashWeekCount[$key] + $ottoWeekCount[$key] + $gopayWeekCount[$key],
                'bni' => $bniWeek[$key],
                'emoney' => $emoneyWeek[$key],
                'tcash' => $tcashWeek[$key],
                'ottopay' => $ottoWeek[$key],
                'gopay' => $gopayWeek[$key],
                'total' => $bniWeek[$key] + $emoneyWeek[$key] + $tcashWeek[$key] + $ottoWeek[$key] + $gopayWeek[$key]

            ];
        }
        
        foreach ($monthLabels as $key => $value) {
            $dataTableMonthly[] = [
                'periode' => $value,
                'totalParcel' => $parcelMonthlyCount[$key],
                'totalTrx' => $bniMonthlyCount[$key] + $emoneyMonthlyCount[$key] + $tcashMonthlyCount[$key] + $ottoMonthlyCount[$key] + $gopayMonthlyCount[$key],
                'bni' => $bniMonthly[$key],
                'emoney' => $emoneyMonthly[$key],
                'tcash' => $tcashMonthly[$key],
                'ottopay' => $ottoMonthly[$key],
                'gopay' => $gopayMonthly[$key],
                'total' => $bniMonthly[$key] + $emoneyMonthly[$key] + $tcashMonthly[$key] + $ottoMonthly[$key] + $gopayMonthly[$key]

            ];
        }

        // parse for response
        $data['dayLabels'] = $dayLabels;
        $data['dayCountTransaction'] = $dayCountTransaction;
        $data['daySumTransaction'] = $daySumTransaction;
       
        $data['weekLabels'] = $weekLabels;
        $data['weekCountTransaction'] = $weekCountTransaction;
        $data['weekSumTransaction'] = $weekSumTransaction;
        $data['weekMethodPayment'] = $weekPaymentMethod;
       
        $data['monthLabels'] = $monthLabels;
        $data['monthCountTransaction'] = $monthCountTransaction;
        $data['monthSumTransaction'] = $monthSumTransaction;
        $data['monthPaymentMethod'] = $monthPaymentMethod;
    
        $data['transactionCount'] = $transactionCount;
        $data['transactionAmount'] = $transactionAmount;
        $data['topupAmount'] = $topupAmount;
        $data['topupCount'] = $topupCount;
        $data['rewardAmount'] = $rewardAmount;
        $data['rewardCount'] = $rewardCount;

        $data['pulsaCount'] = $pulsaCount;
        $data['pulsaSum'] = $pulsaSum;
        $data['paymentCount'] = $paymentCount;
        $data['paymentSum'] = $paymentSum;
        $data['popshopCount'] = $popshopCount;
        $data['popshopSum'] = $popshopSum;
        $data['deliveryCount'] = $deliveryCount;
        $data['deliverySum'] = $deliverySum;

        $data['emoneyDay'] = $emoneyDay;
        $data['ottoDay'] = $ottoDay;
        $data['tcashDay'] = $tcashDay;
        $data['bniDay'] = $bniDay;
        $data['gopayDay'] = $gopayDay;

        $data['emoneyDayCount'] = $emoneyDayCount;
        $data['ottoDayCount'] = $ottoDayCount;
        $data['tcashDayCount'] = $tcashDayCount;
        $data['bniDayCount'] = $bniDayCount;
        $data['gopayDayCount'] = $gopayDayCount;

        $data['emoneyWeek'] = $emoneyWeek;
        $data['ottoWeek'] = $ottoWeek;
        $data['tcashWeek'] = $tcashWeek;
        $data['bniWeek'] = $bniWeek;
        $data['gopayWeek'] = $gopayWeek;

        $data['emoneyWeekCount'] = $emoneyWeekCount;
        $data['ottoWeekCount'] = $ottoWeekCount;
        $data['tcashWeekCount'] = $tcashWeekCount;
        $data['bniWeekCount'] = $bniWeekCount;
        $data['gopayWeekCount'] = $gopayWeekCount;

        $data['emoneyMonthly'] = $emoneyMonthly;
        $data['ottoMonthly'] = $ottoMonthly;
        $data['tcashMonthly'] = $tcashMonthly;
        $data['bniMonthly'] = $bniMonthly;
        $data['gopayMonthly'] = $gopayMonthly;

        $data['emoneyMonthlyCount'] = $emoneyMonthlyCount;
        $data['ottoMonthlyCount'] = $ottoMonthlyCount;
        $data['tcashMonthlyCount'] = $tcashMonthlyCount;
        $data['bniMonthlyCount'] = $bniMonthlyCount;
        $data['gopayMonthlyCount'] = $gopayMonthlyCount;

        $data['floatingDeposit'] = $floatingDeposit;
        $data['countActiveAgent'] = $countActiveAgent;
        $data['countAllAgent'] = $countAllAgent;

        // DATATABLES
        $data['dataTableDaily'] = $dataTableDaily;
        $data['dataTableWeekly'] = $dataTableWeekly;
        $data['dataTableMonthly'] = $dataTableMonthly;

        $data['isSuccess'] = true;

        return response()->json($data);
    }
}
