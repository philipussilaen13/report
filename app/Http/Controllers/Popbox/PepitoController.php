<?php

namespace App\Http\Controllers\Popbox;

use App\Http\Helpers\Helper;
use App\Models\Popbox\SepulsaTransaction;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PepitoController extends Controller
{
    
    public function list(Request $request){        
        return view('popbox.pepito.list');
    }
    
    /**
     * Get Sepulsa Transaction List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTransaction(Request $request){
        
        $populatedLocation = $this->populateLocation();
        
        $locker_ids = collect($populatedLocation)->pluck('name');
        
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";
        
        $productType = $request->input('type',null);
        $transactionStatus = $request->input('status',null);
        $locker_id = $request->input('location_selected', null);        
        $invoiceId = $request->input('invoice_id',null);
        
        if($locker_id){
            $locker_ids = [$locker_id];
        }
        
        // get product type
        $productListDb = SepulsaTransaction::select(DB::raw('DISTINCT(product_type)'))
        ->pluck('product_type');
        // get Transaction List
        $transactionDb = DB::connection('popbox_db')
        ->table('tb_sepulsa_transaction as tst')
        ->leftJoin('tb_sepulsa_product as tsp','tst.product_id','=','tsp.product_id')
        ->whereBetween('tst.created_at',[$startDate,$endDate])
        ->when($productType,function ($query) use($productType){
            return $query->where('product_type',$productType);
        })->when($transactionStatus, function ($query) use($transactionStatus){
            return $query->where('tst.status',$transactionStatus);
        })->when($invoiceId,function ($query) use ($invoiceId){
            return $query->where('invoice_id',$invoiceId);
        })->when($locker_ids, function($query) use ($locker_ids){
            return $query->whereIn('location', $locker_ids);
        })->orderBy('id','desc')
        ->select('tst.*','tsp.operator')
        ->paginate(15);
        
        $otherPayment = ['bpjs_kesehatan','pdam','telkom_postpaid'];
        foreach ($transactionDb as $item){
            $sepulsaPrice = 0;
            $popboxPrice = 0;
            if ($item->product_type == 'mobile'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount;
            } elseif ($item->product_type == 'electricity'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount + 2500;
            } elseif ($item->product_type == 'electricity_postpaid'){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            } elseif (in_array($item->product_type,$otherPayment)){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            }
            
            $item->sepulsa_price = $sepulsaPrice;
            $item->popbox_price = $popboxPrice;
        }
        
        /*Get Top Product*/
        $topProductDb = DB::connection('popbox_db')
        ->table('tb_sepulsa_product as tsp')
        ->leftJoin('tb_sepulsa_transaction as tst','tst.product_id', '=', 'tsp.product_id')
        ->whereBetween('tst.created_at',[$startDate,$endDate])
        ->when($locker_ids, function($query) use ($locker_ids){
            return $query->whereIn('location', $locker_ids);
        })
        ->select(DB::raw('tsp.operator, tsp.label,tsp.product_id, count(tst.id) as \'count_transaction\', sum(tst.product_amount) as \'total_transaction\''))
        ->groupBy('tsp.product_id')
        ->orderBy('count_transaction','DESC')
        ->get();
        
        /*Get for graph*/
        $graphTransactionDb = DB::connection('popbox_db')
        ->table('tb_sepulsa_transaction as tst')
        ->leftJoin('tb_sepulsa_product as tsp','tst.product_id','=','tsp.product_id')
        ->whereBetween('tst.created_at',[$startDate,$endDate])
        ->where('rc','00')
        ->when($locker_ids, function($query) use ($locker_ids){
            return $query->whereIn('location', $locker_ids);
        })->select('tst.*','tsp.operator')
        ->get();
        
        $dayData = $graphTransactionDb->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('z');
        });
            
        $dayLabels = [];
        foreach ($dayData as $index => $item) {
            $tmpDate = Helper::getDateFromDay($index, date('Y'));
            $dayLabels[] = date('j M y', strtotime($tmpDate->format('j M y')));
        }
        // create data for day graph
        $dayCountTransaction = [];
        $daySumTransaction = [];
        foreach ($dayLabels as $index => $date) {
            $count = 0;
            $sum = 0;
            $tmpDayOfYear = date('z', strtotime($date));
            if (isset($dayData[$tmpDayOfYear])) {
                $count = count($dayData[$tmpDayOfYear]);
                $sum = $dayData[$tmpDayOfYear]->sum('product_amount');
            }
            $dayCountTransaction[] = $count;
            $daySumTransaction[] = $sum;
        }
        
        $transactionStatusList = ['success','failed'];
        
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";
        
        // parse data to view
        $data = [];
        $data['transactionDb'] = $transactionDb->appends($request->input());
        $data['productList'] = $productListDb;
        $data['transactionStatusList'] = $transactionStatusList;
        $data['location'] = $populatedLocation;
        $data['param'] = $param;
        $data['topProductDb'] = $topProductDb;
        $data['dayLabel'] = $dayLabels;
        $data['dayCountTransaction'] = $dayCountTransaction;
        $data['daySumTransaction'] = $daySumTransaction;
        
        return view('popbox.pepito.list',$data);
    }

    /**
     * Post Export Transaction
     * @param Request $request
     */
    public function postExportSepulsaTransaction(Request $request){
        set_time_limit(300);
        ini_set('memory_limit', '-1');
        
        $populatedLocation = $this->populateLocation();
        
        $locker_ids = collect($populatedLocation)->pluck('name');

        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $productType = $request->input('type',null);
        $transactionStatus = $request->input('status',null);
        $locker_id = $request->input('location_selected',null);
        $invoiceId = $request->input('invoice_id',null);
        if($locker_id){
            $locker_ids = [$locker_id];
        }

        // get Transaction List
        $transactionDb = DB::connection('popbox_db')
            ->table('tb_sepulsa_transaction as tst')
            ->leftJoin('tb_sepulsa_product as tsp','tst.product_id','=','tsp.product_id')
            ->whereBetween('tst.created_at',[$startDate,$endDate])
            ->when($productType,function ($query) use($productType){
                return $query->where('product_type',$productType);
            })->when($transactionStatus, function ($query) use($transactionStatus){
                return $query->where('tst.status',$transactionStatus);
            })->when($locker_ids, function($query) use ($locker_ids){
                return $query->whereIn('location', $locker_ids);
            })->when($invoiceId,function ($query) use ($invoiceId){
                return $query->where('invoice_id',$invoiceId);
            })->orderBy('id','desc')
            ->select('tst.*','tsp.operator')
            ->get();
        $otherPayment = ['bpjs_kesehatan','pdam','telkom_postpaid'];
        foreach ($transactionDb as $item){
            $sepulsaPrice = 0;
            $popboxPrice = 0;
            if ($item->product_type == 'mobile'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount;
            } elseif ($item->product_type == 'electricity'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount + 2500;
            } elseif ($item->product_type == 'electricity_postpaid'){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            } elseif (in_array($item->product_type,$otherPayment)){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            }

            $item->sepulsa_price = $sepulsaPrice;
            $item->popbox_price = $popboxPrice;
        }

        /*Begin Excel Process*/
        $filename = "$startDate s/d $endDate Pepito Transaction";

        Excel::create("$filename", function ($excel) use ($transactionDb) {
            $excel->setTitle('Pepito Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Asia');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($transactionDb) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Invoice ID');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Customer Data');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Product Type');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Product Description');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Location');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('PopBox Price');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Transaction Date');
                });
//                 $sheet->cell('J1', function ($cell) {
//                     $cell->setValue('Sepulsa Price');
//                 });
//                 $sheet->cell('K1', function ($cell) {
//                     $cell->setValue('PopBox Price');
//                 });
//                 $sheet->cell('L1', function ($cell) {
//                     $cell->setValue('Transaction Date');
//                 });

                $cellNumberStart = 2;
                foreach ($transactionDb as $index => $transaction) {
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                        $cell->setValue($index + 1);
                    });
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->invoice_id);
                    });
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->cust_phone." - ".$transaction->cust_email);
                    });
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->product_type);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->product_description);
                    });
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->location);
                    });
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->status);
                    });
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue(number_format($transaction->popbox_price));
                    });
                    $sheet->cell("I$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->created_at);
                    });
//                     $sheet->cell("J$cellNumberStart", function ($cell) use ($transaction) {
//                         $cell->setValue($transaction->sepulsa_price);
//                     });
//                     $sheet->cell("K$cellNumberStart", function ($cell) use ($transaction) {
//                         $cell->setValue($transaction->popbox_price);
//                     });
//                     $sheet->cell("L$cellNumberStart", function ($cell) use ($transaction) {
//                         $cell->setValue($transaction->created_at);
//                     });
                    $cellNumberStart++;
                }
            });
        })->download('xlsx');
    }

    private function populateLocation() {
        
        $location = array();
        
        $location[] = [
            'locker_id' => 'cf087a8f9012f0157968c99e8bc2dde3',
            'name'      => 'Pepito Express Seminyak'
        ];
        
        $location[] = [
            'locker_id' => '8bb22c4d88e25a12a66b004ffb7ce40c',
            'name'      => 'Pepito Express Karang Mas'
        ];
        
        $location[] = [
            'locker_id' => 'f1d0cc1578097d051695eb78ab1860dd',
            'name'      => 'Pepito Express Pecatu'
        ];
        
        $location[] = [
            'locker_id' => 'cb2cf6dc6bedbb738888c6a6590b12c5',
            'name'      => 'Pepito Express Siligita'
        ];
        
        $location[] = [
            'locker_id' => '0a1e744bf4c415984ca08a140ccd57b7',
            'name'      => 'Pepito Express Umalas'
        ];
        
        $location[] = [
            'locker_id' => '537d61faa9ec72a90add145ec1e93318',
            'name'      => 'Pepito Market Andong'
        ];
        
        $location[] = [
            'locker_id' => '4e317ac59774e1d5e271ad0a22ef540e',
            'name'      => 'Pepito Market Canggu'
        ];
        
        $location[] = [
            'locker_id' => 'b2d54ac408be9b156b0cbcf2ad2f7972',
            'name'      => 'Pepito Market Echo Beach - Canggu'
        ];
        
        $location[] = [
            'locker_id' => '2d9f0c491dffefb2feca1801417fc3b5',
            'name'      => 'Pepito Market Nusa Dua'
        ];
        
        $location[] = [
            'locker_id' => 'd905998ff34ef5f76e55894ab19a5d10',
            'name'      => 'Pepito Market Pererenan'
        ];
        
        $location[] = [
            'locker_id' => 'f244aa0daf1d71c670d3cfdcb5e9edc4',
            'name'      => 'Pepito Market Petitenget'
        ];
        
        $location[] = [
            'locker_id' => 'db4852468b610618f091cc75abc6e454',
            'name'      => 'Pepito Market Tebongkang'
        ];
        
        $location[] = [
            'locker_id' => '21844ffd0aa9f8be00fbe6bb08a599d0',
            'name'      => 'Pepito Market Tuban'
        ];
        
        $location[] = [
            'locker_id' => 'eb90dc4f8443c6b4dc1f0a0127da9417',
            'name'      => 'Pepito Market Uluwatu'
        ];
        
        $location[] = [
            'locker_id' => '694c4aefe6c5333ceece349c4035a2d1',
            'name'      => 'Popular Express Sanur Bali'
        ];
        
        $location[] = [
            'locker_id' => 'b510dc5147f5be80f9de705c21d9a0d4',
            'name'      => 'Popular Market Deli Canggu Bali'
        ];
        
        $location[] = [
            'locker_id' => '06815e03e50b0d08c0966f1d19f9ed10',
            'name'      => 'Popular Market Hayam Wuruk'
        ];
        
        $location[] = [
            'locker_id' => 'ffeaf8fc9ba48b8ce2659a15a55c0e67',
            'name'      => 'Popular Market Sanur Intaran Bali'
        ];
        
        return $location;
    }
}
