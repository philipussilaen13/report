<?php

namespace App\Http\Controllers\Popbox;

use App\Http\Helpers\ApiSepulsa;
use App\Http\Helpers\Helper;
use App\Models\Popbox\LockerLocation;
use App\Models\Popbox\SepulsaLocation;
use App\Models\Popbox\SepulsaProduct;
use App\Models\Popbox\SepulsaTransaction;
use App\Models\Virtual\Locker;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class SepulsaController extends Controller
{
    /**
     * Get Sepulsa Product List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProduct(Request $request)
    {
        //get All Sepulsa Product
        $sepulsaProductDb = SepulsaProduct::orderBy('type', 'asc')->get();
        $openCount = 0;
        $openManualCount = 0;
        $closeCount = 0;
        $closeManualCount = 0;

        $filtered = $sepulsaProductDb->where('status', 'OPEN');
        $filtered->all();
        $openCount = $filtered->count();

        $filtered = $sepulsaProductDb->where('status', 'MANUAL_OPEN');
        $filtered->all();
        $openManualCount = $filtered->count();

        $filtered = $sepulsaProductDb->where('status', 'CLOSED');
        $filtered->all();
        $closeCount = $filtered->count();

        $filtered = $sepulsaProductDb->where('status', 'MANUAL_CLOSE');
        $filtered->all();
        $closeManualCount = $filtered->count();

        $typeList = [];
        $operatorList = [];
        foreach ($sepulsaProductDb as $item) {
            if (!in_array($item->type, $typeList)) $typeList[] = $item->type;
            if (!in_array($item->operator, $operatorList)) $operatorList[] = $item->operator;
        }

        $statusList = ['OPEN', 'MANUAL_OPEN', 'CLOSED', 'MANUAL_CLOSE', 'PERMANENT_CLOSE'];

        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $paketData = ($request->input('paket_data', null) === "0") ? '-1' : $request->input('paket_data', null);

        // $isData = 0;
        $operator = $request->input('operator', null);
        $label = $request->input('label', null);
        $productId = $request->input('product_id', null);

        $sepulsaProductDb = SepulsaProduct::when($status, function ($query) use ($status) {
            return $query->where('status', $status);
        })->when($type, function ($query) use ($type) {
            return $query->where('type', $type);
        })->when($operator, function ($query) use ($operator) {
            return $query->where('operator', $operator);
        })->when($paketData, function ($query) use ($paketData) {
            if ($paketData == '-1') {
                return $query->where([['type', '=' ,'mobile'], ['is_packet_data', '=', '0']]);
            } else {
                return $query->where([['type', '=' ,'mobile'], ['is_packet_data', '=',$paketData]]);                
            }
            
        })->when($label, function ($query) use ($label) {
            return $query->where('label', 'LIKE', "%$label%");
        })->when($productId, function ($query) use ($productId) {
            if ($productId.contains(',')) {
                return $query->whereIn('product_id', explode(',', $productId));
            }
            return $query->where('product_id', $productId);
        })->orderBy('type', 'asc')
            ->orderByRaw('CAST(product_id AS UNSIGNED)')
            ->paginate(10);

        $groupProduct = SepulsaProduct::groupBy('type')
            ->where('status','<>','PERMANENT_CLOSE')
            ->select(DB::raw('COUNT(product_id) as count, type'))
            ->get();

        $param = [];
        foreach ($request->input() as $key => $item) {
            $param[$key] = $item;
        }
        $param['paket_data'] = $paketData;

        // parse data to view
        $data = [];
        $data['products'] = $sepulsaProductDb;
        $data['groupProduct'] = $groupProduct;
        $data['openCount'] = $openCount;
        $data['openManualCount'] = $openManualCount;
        $data['closeCount'] = $closeCount;
        $data['closeManualCount'] = $closeManualCount;
        $data['typeList'] = $typeList;
        $data['operatorList'] = $operatorList;
        $data['status'] = $statusList;
        $data['param'] = $param;

        return view('popbox.sepulsa.product', $data);
    }

    /**
     * update sepulsa product
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSepulsaProduct(Request $request)
    {
        $productId = $request->input('productId', null);
        $label = $request->input('label');
        $description = $request->input('description');
        $remarks = $request->input('remarks');
        $status = $request->input('status');
        $publishPrice = $request->input('publishPrice');

        // if not empty product id, then update
        if (!empty($productId)) {
            $sepulsaDb = SepulsaProduct::where('product_id', $productId)->first();
            $sepulsaDb->label = $label;
            $sepulsaDb->description = $description;
            $sepulsaDb->remarks = $remarks;
            $sepulsaDb->status = $status;
            $sepulsaDb->publish_price = $publishPrice;
            $sepulsaDb->save();
        }

        $request->session()->flash('success', "Success Update $label");
        return back();
    }

    /**
     * Sepulsa Update Status
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getSepulsaUpdateStatus(Request $request){
        // post to sepulsa
        $url = "https://buddy.sepulsa.id/open_api/product_list/view.json";
        $param = [];

        $sepulsaApi = new ApiSepulsa();
        $getApi = $sepulsaApi->get_sepulsa($url,$param);
        if (empty($getApi)){
            $request->session()->flash('error','Failed to Update');
            return back();
        }
        $data = $getApi['data'];
        foreach ($data as $item) {
            // check if exist on db
            $sepulsaProductDb = DB::connection('popbox_db')->table('tb_sepulsa_product')->where('product_id',$item['product_id'])->first();
            if (!$sepulsaProductDb) continue;
            $numberStatus = $item['status'];
            $status = 'OPEN';
            if ($numberStatus == 0) $status = 'CLOSED';
            if ($numberStatus == 1) $status = 'OPEN';
            if ($sepulsaProductDb->status == 'OPEN' || $sepulsaProductDb->status == 'CLOSED'){
                // update status open and closed only, if manual_close or manual_open, IGNORE
                if ($sepulsaProductDb->status != $status){
                    echo "$sepulsaProductDb->label change to $status\n";
                }
                $productDb = DB::connection('popbox_db')->table('tb_sepulsa_product')
                    ->where('id',$sepulsaProductDb->id)
                    ->update(['status'=>$status,'updated_at'=>date('Y-m-d H:i:s')]);
            }
        }
        $request->session()->flash('success','Success Update Sepulsa');
        return back();
    }

    public function postCloseProduct(Request $request){
        $status = $request->input('status');
        $productIds = $request->input('product_id',null);
        if (empty($productIds)){
            $request->session()->flash('error','Product Id Not Seleceted');
            return back();
        }
        $success = '';
        foreach ($productIds as $productId) {
            $sepulsaDb = SepulsaProduct::where('product_id', $productId)->first();
            $sepulsaDb->status = $status;
            $sepulsaDb->save();

            $success .= $sepulsaDb->label;
        }
        $request->session()->flash('success',"Success $status Product: $success");
        return back();
    }

    /**
     * get Sepulsa Location
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSepulsaLocation(Request $request)
    {
        // get sepulsa location
        $type = $request->input('type', 'agent');
        $name = $request->input('name', null);
        $sepulsaLocationDb = SepulsaLocation::when($type, function ($query) use ($type) {
            return $query->where('type', $type);
        })->when($name, function ($query) use ($name) {
            return $query->where('name', 'LIKE', "%name%");
        })->get();

        // get locker location
        $lockerDb = LockerLocation::join('districts', 'districts.id', '=', 'locker_locations.district_id')
            ->leftJoin('locker_services','locker_services.locker_id','=','locker_locations.locker_id')
            ->where('locker_services.service','=','emoney')
            ->groupBy('locker_locations.locker_id')
            ->get();

        $sepulsaLocationNameList = [];
        foreach ($sepulsaLocationDb as $item) {
            $sepulsaLocationNameList[] = $item->name;
        }

        $lockerVirtualDb = Locker::with('city')->whereIn('locker_name',$sepulsaLocationNameList)->get();

        $tmpLockerDb = $lockerDb->toArray();
        foreach ($sepulsaLocationDb as $item) {
            $address = null;
            $nearestLocker = null;
            $cityName = null;
            $lockerId = null;
            $lockerData = null;
            foreach ($lockerVirtualDb as $lockerDatum) {
                if ($lockerDatum->locker_name == $item->name){
                    $address = $lockerDatum->address;
                    $cityName = $lockerDatum->city->city_name;
                    $lockerId = $lockerDatum->locker_id;
                    $lockerData = $lockerDatum;
                }
            }
            if ($item->type == 'agent') {
                if (empty($lockerData->latitude)) {
                    continue;
                }
                $ref = [$lockerData->latitude, $lockerData->longitude];
                $distances = array_map(function ($locker) use ($ref) {
                    $a = [$locker['latitude'], $locker['longitude']];
                    return Helper::calculateDistance($a, $ref);
                }, $tmpLockerDb);
                asort($distances);
                $tmpNearest = $tmpLockerDb[key($distances)];
                $nearestLocker = $tmpNearest['name'];
            }
            $item->lockerId = $lockerId;
            $item->address = $address;
            $item->cityName = $cityName;
            $item->nearestLocker = $nearestLocker;
        }

        // create pagination
        $perPage = 15;
        $page = $request->input('page', 1);

        $paginator = new LengthAwarePaginator($sepulsaLocationDb->forPage($page, $perPage), $sepulsaLocationDb->count(), $perPage, $page);
        $paginator->withPath('');
        $paginator->appends($request->except('page'));

        $typeList = ['agent', 'locker'];
        $param = $request->except('page');
        // pass data to view
        $data = [];
        $data['location'] = $paginator;
        $data['typeList'] = $typeList;
        $data['param'] = $param;
        $data['lockerDb'] = $lockerDb;
        return view('popbox.sepulsa.location', $data);
    }

    /**
     * update sepulsa location
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSepulsaLocation(Request $request)
    {
        $locationId = $request->input('form-id', null);
        if (empty($locationId)) {
            $request->session()->flash('error', 'Empty Location Id');
            return back();
        }
        $locationDb = SepulsaLocation::find($locationId);
        if (empty($locationDb)) {
            $request->session()->flash('error', 'Location not Found');
            return back();
        }
        // get locker db
        $alias_locker = $request->input('form-locker_alias', null);
        $lockerDb = LockerLocation::join('districts', 'districts.id', '=', 'locker_locations.district_id')->where('name', $alias_locker)->first();
        if (!$lockerDb) {
            $request->session()->flash('error', 'Locker Not Found');
            return back();
        }
        // update sepulsa location
        $locationDb->alias_locker = $lockerDb->name;
        $locationDb->alias_city = $lockerDb->district;
        $locationDb->save();

        $request->session()->flash('success', 'Success Update Location');
        return back();
    }

    /**
     * Update Agent Location
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdateLocation(Request $request){
        $sepulsaLocationDb = SepulsaLocation::where('type','agent')
            ->whereNull('alias_locker')
            ->take(120)
            ->get();

        // get locker location
        $lockerDb = LockerLocation::join('districts', 'districts.id', '=', 'locker_locations.district_id')
            ->leftJoin('locker_services','locker_services.locker_id','=','locker_locations.locker_id')
            ->where('locker_services.service','=','emoney')
            ->groupBy('locker_locations.locker_id')
            ->get();

        $sepulsaLocationNameList = [];
        foreach ($sepulsaLocationDb as $item) {
            $sepulsaLocationNameList[] = $item->name;
        }

        $lockerVirtualDb = Locker::with('city')->whereIn('locker_name',$sepulsaLocationNameList)->get();

        $tmpLockerDb = $lockerDb->toArray();
        foreach ($sepulsaLocationDb as $item) {
            $address = null;
            $nearestLocker = null;
            $cityName = null;
            $lockerId = null;
            $lockerData = null;
            foreach ($lockerVirtualDb as $lockerDatum) {
                if ($lockerDatum->locker_name == $item->name){
                    $address = $lockerDatum->address;
                    $cityName = $lockerDatum->city->city_name;
                    $lockerId = $lockerDatum->locker_id;
                    $lockerData = $lockerDatum;
                }
            }
            if ($item->type == 'agent') {
                if (empty($lockerData->latitude)) {
                    continue;
                }
                $ref = [$lockerData->latitude, $lockerData->longitude];
                $distances = array_map(function ($locker) use ($ref) {
                    $a = [$locker['latitude'], $locker['longitude']];
                    return Helper::calculateDistance($a, $ref);
                }, $tmpLockerDb);
                asort($distances);
                $tmpNearest = $tmpLockerDb[key($distances)];
                $nearestLocker = $tmpNearest['name'];
            }
            $item->lockerId = $lockerId;
            $item->address = $address;
            $item->cityName = $cityName;
            $item->nearestLocker = $nearestLocker;
        }

        $successList = [];
        $failedList = [];

        foreach ($sepulsaLocationDb as $item) {
            $locationId = $item->id;
            if (empty($locationId)) {
                $failedList[] = "empty Location";
                continue;
            }
            $locationDb = SepulsaLocation::find($locationId);
            if (empty($locationDb)) {
                $failedList[] = "Sepulsa Location not found";
                continue;
            }
            // get locker db
            $alias_locker = $item->nearestLocker;
            $lockerDb = LockerLocation::join('districts', 'districts.id', '=', 'locker_locations.district_id')->where('name', $alias_locker)->first();
            if (!$lockerDb) {
                $failedList[] = "Locker Db Not Found";
                continue;
            }
            // update sepulsa location
            $locationDb->alias_locker = $lockerDb->name;
            $locationDb->alias_city = $lockerDb->district;
            $locationDb->save();

            $successList[] = $item;
        }

        if (empty($successList)){
            $request->session()->flash('error','Failed Update');
            return back();
        }
        $countSuccess = count($successList);
        $countFailed = count($failedList);

        $request->session()->flash('error',"Failed Update $countFailed Location");
        $request->session()->flash('success',"Success Update $countSuccess Location");
        return back();
    }

    public function completeData (Request $request){
        // get sepulsa prefix
        $sepulsaLocation = DB::connection('popbox_db')
            ->table('tb_sepulsa_location')
            ->where('type','agent')
            ->get();

        $sepulsaList = [];
        foreach ($sepulsaLocation as $item) {
            $sepulsaList[] = $item->name;
        }

        // get reference transaction
        $lockerDb = Locker::with('city')
            ->whereNotIn('locker_name',$sepulsaList)
            ->where('type','agent')
            ->where('status','<>','0')
            ->get();
        foreach ($lockerDb as $item) {
            $agentName = $item->locker_name;
            $words = explode(" ", $agentName);
            $acronym = "A";
            if (!empty($words)){
                foreach ($words as $w) {
                    if (!empty($w[0]))
                        $acronym .= $w[0];
                }
            } else {
                $acronym .= "new";
            }

            if (strlen($acronym)>5){
                $acronym = substr($acronym,0,5);
            }
            $locationName = $agentName;
            $locationType = 'agent';
            // check based on name
            $checkName = DB::connection('popbox_db')->table('tb_sepulsa_location')->where('name',$locationName)->first();
            if (!$checkName){
                $check = DB::connection('popbox_db')->table('tb_sepulsa_location')->where('code','LIKE',"%$acronym%")->get();
                if (count($check)>=1){
                    $number = count($check) + 1;
                    $acronym.= $number;
                }
            }
            $shortLocationCode = $acronym;

            $check = DB::connection('popbox_db')->table('tb_sepulsa_location')->where('code',$shortLocationCode)->first();
            if (!$check){
                // insert into table tb_sepulsa_location
                $sepulsaLocationDb = [];
                $sepulsaLocationDb['code'] = $shortLocationCode;
                $sepulsaLocationDb['type'] = $locationType;
                $sepulsaLocationDb['name'] = $locationName;
                $sepulsaLocationDb['created_at'] = date('Y-m-d H:i:s');
                DB::connection('popbox_db')->table('tb_sepulsa_location')->insert($sepulsaLocationDb);
            }
        }
        $request->session()->flash('success','Success Add Agent Location');
        return back();
    }

    /**
     * Get Sepulsa Transaction List
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSepulsaTransaction(Request $request){
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $productType = $request->input('type',null);
        $transactionStatus = $request->input('status',null);
        $rcStatus = $request->input('rc_status',null);
        $invoiceId = $request->input('invoice_id',null);

        // get product type
        $productListDb = SepulsaTransaction::select(DB::raw('DISTINCT(product_type)'))
            ->pluck('product_type');
        // get Transaction List
        $transactionDb = DB::connection('popbox_db')
            ->table('tb_sepulsa_transaction as tst')
            ->leftJoin('tb_sepulsa_product as tsp','tst.product_id','=','tsp.product_id')
            ->whereBetween('tst.created_at',[$startDate,$endDate])
            ->when($productType,function ($query) use($productType){
                return $query->where('product_type',$productType);
            })->when($transactionStatus, function ($query) use($transactionStatus){
                return $query->where('tst.status',$transactionStatus);
            })->when($rcStatus,function ($query) use($rcStatus){
                return $query->where('rc',$rcStatus);
            })->when($invoiceId,function ($query) use ($invoiceId){
                return $query->where('invoice_id',$invoiceId);
            })->orderBy('id','desc')
            ->select('tst.*','tsp.operator')
            ->paginate(15);

        $otherPayment = ['bpjs_kesehatan','pdam','telkom_postpaid'];
        foreach ($transactionDb as $item){
            $sepulsaPrice = 0;
            $popboxPrice = 0;
            if ($item->product_type == 'mobile'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount;
            } elseif ($item->product_type == 'electricity'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount + 2500;
            } elseif ($item->product_type == 'electricity_postpaid'){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            } elseif (in_array($item->product_type,$otherPayment)){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            }

            $item->sepulsa_price = $sepulsaPrice;
            $item->popbox_price = $popboxPrice;
        }

        /*Get Top Product*/
        $topProductDb = DB::connection('popbox_db')
            ->table('tb_sepulsa_product as tsp')
            ->leftJoin('tb_sepulsa_transaction as tst','tst.product_id', '=', 'tsp.product_id')
            ->whereBetween('tst.created_at',[$startDate,$endDate])
            ->select(DB::raw('tsp.operator, tsp.label,tsp.product_id, count(tst.id) as \'count_transaction\', sum(tst.product_amount) as \'total_transaction\''))
            ->groupBy('tsp.product_id')
            ->orderBy('count_transaction','DESC')
            ->get();

        /*Get for graph*/
        $graphTransactionDb = DB::connection('popbox_db')
            ->table('tb_sepulsa_transaction as tst')
            ->leftJoin('tb_sepulsa_product as tsp','tst.product_id','=','tsp.product_id')
            ->whereBetween('tst.created_at',[$startDate,$endDate])
            ->where('rc','00')
            ->select('tst.*','tsp.operator')
            ->get();

        $dayData = $graphTransactionDb->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('z');
        });

        $dayLabels = [];
        foreach ($dayData as $index => $item) {
            $tmpDate = Helper::getDateFromDay($index, date('Y'));
            $dayLabels[] = date('j M y', strtotime($tmpDate->format('j M y')));
        }
        // create data for day graph
        $dayCountTransaction = [];
        $daySumTransaction = [];
        foreach ($dayLabels as $index => $date) {
            $count = 0;
            $sum = 0;
            $tmpDayOfYear = date('z', strtotime($date));
            if (isset($dayData[$tmpDayOfYear])) {
                $count = count($dayData[$tmpDayOfYear]);
                $sum = $dayData[$tmpDayOfYear]->sum('product_amount');
            }
            $dayCountTransaction[] = $count;
            $daySumTransaction[] = $sum;
        }

        $transactionStatusList = ['success','failed'];
        $rcList = ['00','10','20','21','22','23','24','25','50','98','99'];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // parse data to view
        $data = [];
        $data['transactionDb'] = $transactionDb->appends($request->input());
        $data['productList'] = $productListDb;
        $data['transactionStatusList'] = $transactionStatusList;
        $data['rcList'] = $rcList;
        $data['param'] = $param;
        $data['topProductDb'] = $topProductDb;
        $data['dayLabel'] = $dayLabels;
        $data['dayCountTransaction'] = $dayCountTransaction;
        $data['daySumTransaction'] = $daySumTransaction;

        return view('popbox.sepulsa.transaction',$data);
    }

    /**
     * Post Export Transaction
     * @param Request $request
     */
    public function postExportSepulsaTransaction(Request $request){
        set_time_limit(300);
        ini_set('memory_limit', '20480M');

        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $productType = $request->input('type',null);
        $transactionStatus = $request->input('status',null);
        $rcStatus = $request->input('rc_status',null);
        $invoiceId = $request->input('invoice_id',null);

        // get Transaction List
        $transactionDb = DB::connection('popbox_db')
            ->table('tb_sepulsa_transaction as tst')
            ->leftJoin('tb_sepulsa_product as tsp','tst.product_id','=','tsp.product_id')
            ->whereBetween('tst.created_at',[$startDate,$endDate])
            ->when($productType,function ($query) use($productType){
                return $query->where('product_type',$productType);
            })->when($transactionStatus, function ($query) use($transactionStatus){
                return $query->where('tst.status',$transactionStatus);
            })->when($rcStatus,function ($query) use($rcStatus){
                return $query->where('rc',$rcStatus);
            })->when($invoiceId,function ($query) use ($invoiceId){
                return $query->where('invoice_id',$invoiceId);
            })->orderBy('id','desc')
            ->select('tst.*','tsp.operator')
            ->get();
        $otherPayment = ['bpjs_kesehatan','pdam','telkom_postpaid'];
        foreach ($transactionDb as $item){
            $sepulsaPrice = 0;
            $popboxPrice = 0;
            if ($item->product_type == 'mobile'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount;
            } elseif ($item->product_type == 'electricity'){
                $sepulsaPrice = $item->product_price;
                $popboxPrice = $item->product_amount + 2500;
            } elseif ($item->product_type == 'electricity_postpaid'){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            } elseif (in_array($item->product_type,$otherPayment)){
                $sepulsaPrice = $item->product_price + $item->product_amount;
                $popboxPrice = $item->product_amount + 2500;
            }

            $item->sepulsa_price = $sepulsaPrice;
            $item->popbox_price = $popboxPrice;
        }

        /*Begin Excel Process*/
        $filename = "$startDate s/d $endDate Sepulsa Transaction";

        Excel::create("$filename", function ($excel) use ($transactionDb) {
            $excel->setTitle('Sepulsa Transaction');
            $excel->setCreator('Dashboard Agent')->setCompany('PopBox Asia');
            $excel->setDescription('transaction file');
            $excel->sheet('transaction', function ($sheet) use ($transactionDb) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('No');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Customer Phone');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Product Type');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Product Description');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Location');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('RC');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Transaction ID');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Order ID');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Sepulsa Price');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('PopBox Price');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Transaction Date');
                });

                $cellNumberStart = 2;
                foreach ($transactionDb as $index => $transaction) {
                    $sheet->cell("A$cellNumberStart", function ($cell) use ($index) {
                        $cell->setValue($index + 1);
                    });
                    $sheet->cell("B$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->cust_phone);
                    });
                    $sheet->cell("C$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->product_type);
                    });
                    $sheet->cell("D$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->product_description);
                    });
                    $sheet->cell("E$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->location);
                    });
                    $sheet->cell("F$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->status);
                    });
                    $sheet->cell("G$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->rc);
                    });
                    $sheet->cell("H$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->transaction_id);
                    });
                    $sheet->cell("I$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->invoice_id);
                    });
                    $sheet->cell("J$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->sepulsa_price);
                    });
                    $sheet->cell("K$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->popbox_price);
                    });
                    $sheet->cell("L$cellNumberStart", function ($cell) use ($transaction) {
                        $cell->setValue($transaction->created_at);
                    });
                    $cellNumberStart++;
                }
            });
        })->download('xlsx');
    }

    /*------------------------------------------DOWNLOAD PRODUCT SEPULSA----------------------------------------------------------*/

    public function getSepulsaProductDownload(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $operator = $request->input('operator', null);
        $label = $request->input('label', null);
        $productId = $request->input('product_id', null);

        //get All Sepulsa Product
        $sepulsaProductDb = SepulsaProduct::when($status, function ($query) use ($status) {
            return $query->where('status', $status);
        })->when($type, function ($query) use ($type) {
            return $query->where('type', $type);
        })->when($operator, function ($query) use ($operator) {
            return $query->where('operator', $operator);
        })->when($label, function ($query) use ($label) {
            return $query->where('label', 'LIKE', "%$label%");
        })->when($productId, function ($query) use ($productId) {
            if ($productId.contains(',')) {
                return $query->whereIn('product_id', explode(',', $productId));
            }
            return $query->where('product_id', $productId);
        })->orderBy('operator', 'asc')
//            ->orderByRaw('CAST(product_id AS UNSIGNED)')
        ->get();

        // Init Data Excel
        $filename = "[".date('ymd')."]"." Sepulsa Product";

        Excel::create($filename, function ($excel) use($sepulsaProductDb) {
            $excel->sheet('Sheet1', function ($sheet) use($sepulsaProductDb){
                $row = 1;
                $no_urut = 1;

                $arr_title = ['No', 'Product Id', 'Tipe', 'Operator', 'Product Names', 'Description', 'Nominal/Value',
                    'Sepulsa Price', 'Customer Price', 'Margin Price', 'Status'];

                $sheet->row($row, $arr_title);

                foreach ($sepulsaProductDb as $index => $item) {

                    $row++;

                    $marginPrice = 0;
                    $marginPrice = $item->publish_price - $item->price;

                    $arr = [$no_urut,
                        $item->product_id,
                        $item->type,
                        $item->operator,
                        $item->label,
                        $item->description,
                        $item->denom,
                        $item->price,
                        $item->publish_price,
                        $marginPrice,
                        $item->status
                    ];

                    $sheet->row($row, $arr);
                    $no_urut++;
                }

            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);

//        $param = [];
//        foreach ($request->input() as $key => $item) {
//            $param[$key] = $item;
//        }
//
//        $data = [];
//        $data['param'] = $param;
//        $data['sepulsa'] = $sepulsaProductDb;
//
//        $response->data = $data;
//
//        $response->isSuccess = true;
//        return response()->json($response);
    }
}
