<?php

namespace App\Http\Controllers\Popbox;

use App\Models\Popshop\DimoProduct;
use App\Models\Popshop\DimoProductCategoryName;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PopshopController extends Controller
{
    public function getProduct(){
        $startDate = '2017-05-01 00:00:00';
        $endDate = date('Y-m-d H:i:s');

        // categories
        $categoriesDb = DimoProductCategoryName::with('products')
            ->where('category_name','LIKE',"%Grocery%")
            ->get();

        // count top product
        $topProduct = DB::connection('popbox_db')
            ->table('dimo_products as dp')
            ->leftJoin('order_products as op','op.sku', '=', 'dp.sku')
            ->leftJoin('orders as o','o.invoice_id', '=', 'op.invoice_id')
            ->whereBetween('o.check_out_date',[$startDate,$endDate])
            ->select(DB::raw('dp.sku, dp.name, SUM(op.quantity) as sum, SUM(op.price) as total_order'))
            ->groupBy('dp.sku')
            ->orderBy('sum','desc')
            ->take(5)
            ->get();

        $productsDb = DimoProduct::with('categories')
            ->whereHas('categories',function ($query){
                $query->where('category_name','LIKE',"%Grocery%");
            })
            ->orderBy('id','desc')
            ->paginate(20);

        $data = [];
        $data['categoriesDb'] = $categoriesDb;
        $data['productsDb'] = $productsDb;
        $data['topProduct'] = $topProduct;

        return view('popbox.popshop.product',$data);
    }

    public function getProductCategories() {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // categories
        $categoriesDb = DimoProductCategoryName::with('products')
            ->where('category_name','LIKE',"%Grocery%")
            ->groupBy('category_name')
            ->select('category_name')
            ->get();

        $productsDb = DimoProduct::with('categories')
            ->whereHas('categories',function ($query){
                $query->where('category_name','LIKE',"%Grocery%");
            })
            ->get();

        foreach ($categoriesDb as $item) {
            unset($item->products);
        }

        $data = [];
        $data['categoriesDb'] = $categoriesDb;
        $data['productsDb'] = $productsDb;

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getProductList(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $sku = $request->input('sku', null);
        $name = $request->input('name', null);
        $category = $request->input('category', null);

        $productsDb = DimoProduct::with('categories')
            ->when($sku, function ($query) use ($sku){
                $query->where('sku', $sku);
            })->when($name, function ($query) use ($name){
                $query->where('name', 'LIKE', '%'.$name.'%');
            })->when($category, function ($query) use ($category){
                $query->whereHas('categories',function ($query) use ($category){
                    $query->whereIn('category_name', $category);
                });
            }, function ($query){
//                $query->where('category_name','LIKE',"%Grocery%");
                $query->whereHas('categories',function ($query){
                    $query->where('category_name','LIKE',"%Grocery%");
                });
            })->get();

        $data = [];
        $data['param'] = $request->input();
        $data['productsDb'] = $productsDb;

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getDownloadProductList(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $sku = $request->input('sku', null);
        $name = $request->input('name', null);
        $category = $request->input('category', null);

        $productsDb = DimoProduct::with('categories')
            ->when($sku, function ($query) use ($sku){
                $query->where('sku', $sku);
            })->when($name, function ($query) use ($name){
                $query->where('name', 'LIKE', '%'.$name.'%');
            })->when($category, function ($query) use ($category){
                $query->whereHas('categories',function ($query) use ($category){
                    $query->whereIn('category_name', $category);
                });
            }, function ($query){
                $query->whereHas('categories',function ($query){
                    $query->where('category_name','LIKE',"%Grocery%");
                });
            })
            ->get();

        // Init Data Excel
        $filename = "[".date('ymd')."]"." Product List";

        Excel::create($filename, function ($excel) use ($productsDb){
            $excel->sheet('Sheet 1', function ($sheet) use ($productsDb){

                $row = 1;
                $no_urut = 1;

                $arr_title = ['No', 'SKU', 'Product Name', 'Category', 'Price', 'Discount'];

                $sheet->row($row, $arr_title);

                foreach ($productsDb as $index => $item) {
                    $row++;

                    $arr = [$no_urut,
                        $item->sku,
                        $item->name,
                        $item['categories'][0]->category_name,
                        $item->price,
                        $item->discount
                        ];

                    $sheet->row($row, $arr);
                    $no_urut++;
                }


            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";

        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }
}
