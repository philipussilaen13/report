<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Express extends Model
{
    const UPDATED_AT = 'last_update';
    const CREATED_AT = 'last_update';

    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_express';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;


    /*Relationship*/
    public function company(){
        return $this->belongsTo(Company::class,'logisticsCompany_id','id_company');
    }

    public function userStore(){
        return $this->belongsTo(Users::class,'storeUser_id','id_user');
    }

    public function box(){
        return $this->belongsTo(Box::class,'box_id','id');
    }
}
