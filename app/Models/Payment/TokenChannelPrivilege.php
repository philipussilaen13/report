<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class TokenChannelPrivilege extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'token_channel_privileges';
}
