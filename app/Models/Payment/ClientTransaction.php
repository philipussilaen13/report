<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class ClientTransaction extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'client_transactions';

    /*Relationship*/
    public function paymentChannel(){
        return $this->belongsTo(PaymentChannel::class,'payment_channels_id','id');
    }

    public function companyAccessToken(){
        return $this->belongsTo(CompanyAccessToken::class,'company_access_tokens_id','id');
    }

    public function dokuTransaction(){
        return $this->hasOne(DokuTransaction::class,'client_transactions_id','id');
    }

    public function bniTransaction(){
        return $this->hasOne(BNITransaction::class,'client_transactions_id','id');
    }
}
