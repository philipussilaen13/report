<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class PaymentVendor extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'payment_vendors';
}
