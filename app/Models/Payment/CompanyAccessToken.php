<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class CompanyAccessToken extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'company_access_tokens';

    /*Relationship*/
    /**
     * Has Many Transaction
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientTransactions(){
        return $this->hasMany(ClientTransaction::class);
    }

    public function channels(){
        return $this->belongsToMany(PaymentChannel::class,'token_channel_privileges','company_access_tokens_id','payment_channels_id')->withPivot('status','id');
    }
}
