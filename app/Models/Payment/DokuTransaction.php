<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class DokuTransaction extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'doku_transactions';
}
