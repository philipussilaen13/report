<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Privilege extends Model
{
    use SoftDeletes;
    protected $table = 'privileges';

    /**
     * check privileges based on username
     * @param $email
     * @param $module
     * @return \stdClass
     */
    public static function checkPrivileges($email, $module)
    {
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get userDb
        $userDb = User::where('email', $email)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Found';
            return $response;
        }
        // get groupId
        $groupId = $userDb->group->id;

        // get moduleDb
        $moduleDb = Module::where('name', $module)->first();
        if (!$moduleDb){
            $response->errorMsg = 'Module Not Found';
            return $response;
        }
        //get moduleId
        $moduleId = $moduleDb->id;

        // check privileges
        $check = self::where('group_id', $groupId)->where('module_id', $moduleId)->first();
        if (!$check){
            $response->errorMsg = 'Unauthorized';
            return $response;
        }

        $response->isSuccess = true;
        $response->userId = $userDb->id;
        $response->moduleId = $moduleId;
        $response->groupId = $groupId;
        return $response;
    }

    /**
     * check has access to module or not
     * @param $module
     * @return bool
     */
    public static function hasAccess($module){
        $hasAccess = false;
        if (!Auth::check()){
            return $hasAccess;
        }
        //group
        $user = Auth::user();
        $groupId = $user->group_id;
        $groupName = $user->group->name;
        if ($groupName == 'superadmin'){
            $hasAccess = true;
            return $hasAccess;
        }

        if ($groupName == 'admin'){
            if (!is_array($module)){
                if (strpos($module, 'privileges') === false) {
                    // The word was NOT found
                    $hasAccess = true;
                    return $hasAccess;
                } else {
                    $hasAccess = false;
                    return $hasAccess;
                }
            }
            else {
                foreach ($module as $item) {
                    if (strpos($item, 'privileges') === false) {
                        // The word was NOT found
                        $hasAccess = true;
                        return $hasAccess;
                    } else {
                        $hasAccess = false;
                        return $hasAccess;
                    }
                }
            }
        }

        if (is_array($module)) {
            foreach ($module as $item) {
                $moduleDb = Module::where('name', $item)->first();
                if ($moduleDb) {
                    // check privileges
                    $privilegesDb = self::where('group_id', $groupId)->where('module_id', $moduleDb->id)->first();
                    if ($privilegesDb) {
                        $hasAccess = true;
                        return $hasAccess;
                    }
                }
            }
            return $hasAccess;
        }

        $moduleDb = Module::where('name', $module)->first();
        if (!$moduleDb){
            return $hasAccess;
        }
        // check privileges
        $privilegesDb = self::where('group_id', $groupId)->where('module_id', $moduleDb->id)->first();

        if ($privilegesDb) $hasAccess = true;
        return $hasAccess;
    }
}
