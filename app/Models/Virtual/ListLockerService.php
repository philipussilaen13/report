<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class ListLockerService extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'list_locker_services';
}
