<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    // set table
    protected $connection = 'popbox_virtual';
    protected $table = 'countries';

    /*Relationship*/
    public function province()
    {
        return $this->hasMany(Province::class, 'countries_id');
    }
}
