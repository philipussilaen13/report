<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class LockerHistory extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_histories';

    /**
     * @param $lockerId
     * @param $user
     * @param string $action
     * @param null $remark
     * @return \stdClass
     */
    public static function createNewHistory($lockerId, $user, $action = 'UPDATE', $remark = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $data = new self();
        $data->locker_id = $lockerId;
        $data->user = $user;
        $data->action = $action;
        $data->remarks = $remark;
        $data->save();

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function locker()
    {
        return $this->belongsTo(Locker::class, 'locker_id', 'id');
    }
}
