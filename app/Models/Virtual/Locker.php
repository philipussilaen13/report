<?php

namespace App\Models\Virtual;

use App\Models\Agent\AgentLocker;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Locker extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_virtual';
    protected $table = 'lockers';

    /**
     * list locker
     * filter available : country_name, province_name, city_name, open_hour, close_hour, address
     * limit / take
     * @param $input
     * @return mixed
     */
    public static function listLocker($input)
    {
        // get all locker
        $countryName = empty($input['country_name']) ? null : $input['country_name'];
        $provinceName = empty($input['province_name']) ? null : $input['province_name'];
        $cityName = empty($input['city_name']) ? null : $input['city_name'];
        $lockerId = empty($input['locker_id']) ? null : $input['locker_id'];
        $type = empty($input['type']) ? null : $input['type'];
        $address = empty($input['address']) ? null : $input['address'];
        $status = empty($input['status']) ? null : $input['status'];
        $limit = empty($input['limit']) ? null : $input['limit'];
        $order = empty($input['order']) ? 'locker_name' : $input['order'];

        $lockerDb = self::whereHas('city', function ($query) use ($countryName) {
            $query->whereHas('province', function ($query) use ($countryName) {
                $query->whereHas('country', function ($query) use ($countryName) {
                    if ($countryName) $query->where('country_name', 'LIKE', "%$countryName%");
                });
            });
        })->whereHas('city', function ($query) use ($provinceName) {
            $query->whereHas('province', function ($query) use ($provinceName) {
                if ($provinceName) $query->where('province_name', 'LIKE', "%$provinceName%");
            });
        })->whereHas('city', function ($query) use ($cityName) {
            if ($cityName) $query->where('city_name', 'LIKE', "%$cityName%");
        })->when($lockerId, function ($query) use ($lockerId) {
            return $query->where('locker_id', $lockerId);
        })->when($type, function ($query) use ($type) {
            return $query->where('type', $type);
        })->when($address, function ($query) use ($address) {
            return $query->where('address', 'LIKE', "%$address%");
        })->when($status, function ($query) use ($status) {
            return $query->where('status', $status);
        })->when($limit, function ($query) use ($limit) {
            return $query->take($limit);
        })->orderBy($order, 'asc')->get();

        return $lockerDb;
    }

    /**
     * update locker
     * @param $input
     * @return \stdClass
     */
    public static function updateLocker($input)
    {
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->id = null;
        $response->lockerId = null;

        $lockerDb = self::where('locker_id', $input['locker_id'])->first();
        if (!$lockerDb) {
            $response->errorMsg = 'Invalid Locker. Locker Not Found';
            return $response;
        }
        $id = $lockerDb->id;

        // update to DB process
        $lockerDb = self::find($id);

        if (!empty($input['building_type'])) {
            $lockerDb->building_type_id = $input['building_type'];
        }

        if (!empty($input['detail_address'])) {
            $lockerDb->detail_address = $input['detail_address'];
        }
        if (!empty($input['picture'])) {
            // saving picture
            $filename = $input['locker_id'] . '.jpg';
            $path = public_path() . "/img/locker/$filename";
            $img = base64_decode($input['picture']);
            $saving = file_put_contents($path, $img);
            if (!$saving) {
                $response->errorMsg = "Failed to Save Picture";
                return $response;
            }
            $lockerDb->picture = $filename;
        }
        if (!empty($input['address'])) {
            $lockerDb->address = $input['address'];
        }
        if (!empty($input['city'])) {
            $lockerDb->cities_id = $input['city'];
        }
        if (!empty($input['registered_by'])) {
            $lockerDb->registered_by = $input['registered_by'];
        }
        if (!empty($input['remarks'])) {
            $lockerDb->remarks = $input['remarks'];
        }

        $lockerDb->save();
        $response->id = $id;
        $response->lockerId = $lockerDb->locker_id;

        // validate days, open_hour, close hour
        if (!empty($input['days'])) {
            $days = $input['days'];
            if (!is_array($days)) {
                $response->errorMsg = 'Days must be array';
                return $response;
            }
            // validate and generate 7 items (if not) for days, open_hour, close_hour
            if (count($days) < 7) {
                $diff = 7 - count($days); #different between 7 items and current items in days array
                $end = count($days); #get last item in days array
                for ($i = 0; $i < $diff; $i++) {
                    $days[] = $end + $i;
                }
            }
        }
        if (!empty($input['open_hour'])) {
            $openHour = $input['open_hour'];
            if (!is_array($openHour)) {
                $response->errorMsg = 'Open Hour must be array';
                return $response;
            }
            if (count($openHour) < 7) {
                $diff = 7 - count($openHour); #different between 7 items and current items in open hour array
                for ($i = 0; $i < $diff; $i++) {
                    $openHour[] = null;
                }
            }
        }
        if (!empty($input['close_hour'])) {
            $closeHour = $input['close_hour'];
            if (!is_array($closeHour)) {
                $response->errorMsg = 'Close Hour must be array';
                return $response;
            }
            if (count($closeHour) < 7) {
                $diff = 7 - count($closeHour); #different between 7 items and current items in close hour array
                for ($i = 0; $i < $diff; $i++) {
                    $closeHour[] = null;
                }
            }
        }
        if (!empty($input['days']) && !empty($input['open_hour']) && !empty($input['close_hour'])) {
            // update to table locker operational
            foreach ($days as $index => $item) {
                // check if available on db
                $operationDb = LockerOperationalTime::where('lockers_id', $id)->where('day', $item)->first();
                if (!$operationDb) {
                    // if not exist insert new
                    $data = new LockerOperationalTime();
                    $data->lockers_id = $id;
                    $data->day = $item;
                    $data->open_hour = $openHour[$index];
                    $data->close_hour = $closeHour[$index];
                    $data->save();
                } else {
                    $data = LockerOperationalTime::find($operationDb->id);
                    $data->open_hour = $openHour[$index];
                    $data->close_hour = $closeHour[$index];
                    $data->save();
                }
            }
        }
        if (!empty($input['services'])) {
            $services = $input['services'];
            if (!is_array($services)) {
                $response->errorMsg = 'Services must be array';
                return $response;
            }
            $servicesDb = ListLockerService::whereIn('code', $services)->get();
            if ($servicesDb->count() == 0) {
                $response->errorMsg = "Invalid / No Selected Service";
                return $response;
            }

            DB::connection('popbox_virtual')->table('locker_services')->where('lockers_id', $id)->delete();

            // insert into locker_service
            foreach ($servicesDb as $item) {
                $data = new LockerService();
                $data->list_locker_services_id = $item->id;
                $data->lockers_id = $id;
                $data->save();
            }
        } else {
            DB::connection('popbox_virtual')->table('locker_services')->where('lockers_id', $id)->delete();
        }

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function city()
    {
        return $this->belongsTo(City::class, 'cities_id', 'id');
    }

    public function lockerStatus()
    {
        return $this->hasMany(LockerStatus::class, 'id', 'lockers_id');
    }

    public function operational_time()
    {
        return $this->hasMany(LockerOperationalTime::class, 'lockers_id', 'id');
    }

    public function services()
    {
        return $this->belongsToMany(ListLockerService::class, 'locker_services', 'lockers_id', 'list_locker_services_id');
    }

    public function buildingType()
    {
        return $this->hasOne(BuildingType::class, 'id', 'building_type_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'lockers_id', 'id');
    }

    public function registeredBy()
    {
        return $this->hasOne(User::class, 'id', 'registered_by');
    }

    public function agentLocker(){
        return $this->hasOne(AgentLocker::class,'id','locker_id');
    }

}
