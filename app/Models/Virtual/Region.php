<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'regions';
    use SoftDeletes;    
}
