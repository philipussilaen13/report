<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class LockerStatus extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_status';

    /*Relationship*/
    public function lockerStatusDetail()
    {
        return $this->hasMany(LockerStatusDetail::class, 'id', 'locker_status_id');
    }

    public function locker()
    {
        return $this->belongsTo(Locker::class, 'lockers_id', 'id');
    }
}
