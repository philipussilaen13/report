<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class LockerStatusDetail extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_status_details';

    /*Relationship*/
    public function lockerStatus()
    {
        return $this->belongsTo(LockerStatus::class, 'locker_status_id', 'id');
    }
}
