<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class BuildingType extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'list_locker_building_types';
}
