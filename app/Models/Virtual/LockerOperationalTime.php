<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class LockerOperationalTime extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_operational_times';
}
