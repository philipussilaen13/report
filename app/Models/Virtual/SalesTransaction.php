<?php

namespace App\Models\Virtual;

use App\Http\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;

class SalesTransaction extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'sales_transactions';

    /**
     * Create sales transaction
     * @param $salesId
     * @param string $type
     * @param int $amount
     * @param string $status
     * @return \stdClass
     */
    public function createNewSalesTransaction($salesId, $type = 'agent-topup', $amount = 0, $status = 'PAID', $remarks = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        if ($amount == 0) {
            $response->errorMsg = "Amount Cannot 0";
            return $response;
        }

        // create transaction ID
        $prefix = 'AT';
        if ($type == 'sales-topup') $prefix = 'ST';
        $transactionId = $prefix . date('ymdhi') . Helper::generateRandomString(3);

        // save to DB
        $salesTransactionDb = new self();
        $salesTransactionDb->sales_agent_id = $salesId;
        $salesTransactionDb->type = $type;
        $salesTransactionDb->transaction_id = $transactionId;
        $salesTransactionDb->description = $remarks;
        $salesTransactionDb->status = $status;
        $salesTransactionDb->transaction_date = date('Y-m-d');
        $salesTransactionDb->amount = $amount;
        $salesTransactionDb->save();

        $response->isSuccess = true;
        $response->transactionId = $salesTransactionDb->id;

        return $response;
    }

    /*relationship*/
    public function sales()
    {
        return $this->belongsTo(SalesAgent::class, 'sales_agent_id', 'id');
    }
}
