<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class LockerService extends Model
{
    // set table
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_services';
}
