<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class SalesAgent extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'sales_agents';

    /**
     * Top Up Sales
     * @param $salesId
     * @param int $amount
     * @return \stdClass
     */
    public function topUpSales($salesId, $amount = 0)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $salesDb = self::find($salesId);
        if (!$salesDb) {
            $response->errorMsg = 'Sales Not Found';
            return $response;
        }

        $lastBalance = $salesDb->balance; # get last balance
        $newBalance = $lastBalance + $amount;

        $currentLimit = $salesDb->credit_limit;
        if ($newBalance > $currentLimit) {
            $response->errorMsg = 'New Balance cant more than current Limit';
            return $response;
        }

        $salesDb->balance = $newBalance;
        $salesDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Deduct Sales
     * @param $salesId
     * @param int $amount
     * @return \stdClass
     */
    public function deductSales($salesId, $amount = 0)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $salesDb = self::find($salesId);
        if (!$salesDb) {
            $response->errorMsg = 'Sales Not Found';
            return $response;
        }

        $lastBalance = $salesDb->balance; # get last balance
        if ($lastBalance < $amount) {
            $response->errorMsg = 'Insufficient  Balance';
            return $response;
        }

        $newBalance = $lastBalance - $amount;

        $salesDb->balance = $newBalance;
        $salesDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
