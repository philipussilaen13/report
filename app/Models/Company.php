<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $table = 'companies';

    public function company(){
        return $this->hasOne(Transaction::class,'client_id_companies','client_id');
    }

}
