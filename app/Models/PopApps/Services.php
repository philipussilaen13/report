<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 15/06/18
 * Time: 22.18
 */

namespace App\Models\PopApps;


use Illuminate\Database\Eloquent\Model;

class Services extends Model {

    protected $connection = 'popsend';
    protected $table = 'popbox_services';

    public function getServices() {

        $services = self::all();

        return $services;
    }
}