<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class FreeGiftCategory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'free_gift_categories';

    protected $fillable = ['title', 'category_code', 'icon', 'avaliable_country'];

    /*Relationship*/
    public function freeGifts(){
        return $this->hasMany(FreeGift::class,'merchandise_code', 'category_code');
    }

    public function questions(){
        return $this->hasMany(FreeGiftQuestion::class, 'category_id');
    }
}
