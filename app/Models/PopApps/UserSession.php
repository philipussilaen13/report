<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 25/06/18
 * Time: 11.57
 */

namespace App\Models\PopApps;


use Illuminate\Database\Eloquent\Model;

class UserSession extends Model {

    protected $connection = 'popsend';
    protected $table = 'user_sessions';

    public function getLatestSessionId($userId) {
        $r = null;
        if (empty($userId)) {
            return $r;
        }

        $sessionId = self::where('user_id', '4')
            ->orderBy('last_activity', 'desc')
            ->select('session_id')
            ->first();

        if (empty($sessionId)) {
            return $r;
        }

        return $sessionId;
    }
}