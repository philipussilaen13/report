<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'popsend';
    protected $table = 'users';

    /*Relationship*/
    public function transactions(){
        return $this->hasMany(Transaction::class);
    }

    public function balanceRecords(){
        return $this->hasMany(BalanceRecord::class);
    }
}
