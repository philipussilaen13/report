<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class MessageServiceDetail extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'message_service_detail';
}
