<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageService extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'message_service';
    use SoftDeletes;
}
