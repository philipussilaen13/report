<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class CampaignUsage extends Model
{
    protected $connection = 'popsend';
    protected $table = 'campaign_usages';

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }

    public function voucher(){
        return $this->belongsTo(CampaignVoucher::class);
    }

    public function voucher_code(){
        return $this->belongsTo(Voucher::class);
    }
}
