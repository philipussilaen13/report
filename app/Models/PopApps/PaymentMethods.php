<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class PaymentMethods extends Model
{
    protected $connection = 'popsend';
    protected $table = 'payment_methods';
}
