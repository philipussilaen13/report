<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 15/06/18
 * Time: 22.28
 */

namespace App\Models\PopApps\Article;


use Illuminate\Database\Eloquent\Model;

class Articles extends Model {

    protected $connection = 'popsend';
    protected $table = 'articles';

    public function saveData($data) {

        $dateRange = $data->date_range;
        $startDate = '';
        $endDate = '';

        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }

        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";


        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $fileImageLockerPath = '';
        $fileImageWebPath = '';
        $fileImageMobilePath = '';

        if (!empty($data->image_locker)) {
            $fileImageLockerPath = $data->image_locker->storeAs('images', time(). '-' .$data->title. '-LOCKER-IMAGE'.'.jpg', 'store_images_article');
        }

        if (!empty($data->image_web)) {
            $fileImageWebPath = $data->image_web->storeAs('images', time(). '-' .$data->title. '-WEB-IMAGE'.'.jpg', 'store_images_article');
        }

        if (!empty($data->image_mobile)) {
            $fileImageMobilePath = $data->image_mobile->storeAs('images', time(). '-' .$data->title. '-MOBILE-IMAGE'.'.jpg', 'store_images_article');
        }

        $article = new self();

        $article->notification_title = $data->notif_title;
        $article->notification_subtitle = $data->notif_subtitle;
        $article->title = $data->title;
        $article->subtitle = $data->subtitle;
        $article->content = $data->content;
        $article->promo_code = '';
        $article->other_desc = $data->other_desc;
        $article->image_locker = $fileImageLockerPath;
        $article->image_web = $fileImageWebPath;
        $article->image_mobile = $fileImageMobilePath;
        $article->article_service_id = $data->service;
        $article->article_type_id = $data->type;
        $article->article_status_id = $data->status;
        $article->article_priority = '1';
        $article->article_country_id = $data->country;
        $article->start_date = $startDate;
        $article->end_date = $endDate;
        $article->save();

        $response->isSuccess = true;
        $response->data = $article;
        return $response;
    }

    public function updateData($data) {

        $article = self::where('id', $data->id)
            ->update(
                [
                    '' => $data
                ]
            );


    }

    public function updateStatusArticle($id, $status) {

        $article = self::where('id', '=', $id)
            ->update(['article_status_id' => $status]);

        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $response->isSuccess = true;
        $response->data = $article;
        return $response;
    }

    public function getArticle() {

        $article = self::join('countries', 'countries.id', '=', 'articles.article_country_id')
            ->join('article_status', 'article_status.id', '=', 'articles.article_status_id')
            ->join('article_types', 'article_types.id', '=', 'articles.article_type_id')
            ->join('popbox_services', 'popbox_services.id', '=', 'articles.article_service_id')
            ->orderBy('articles.id', 'asc')
            ->select('articles.*', 'countries.country_name', 'article_status.name as status', 'article_types.name as types', 'popbox_services.name as services')
            ->get();

        $result = $this->formatResult($article);

        return $result;
    }

    public function getArticleById($id) {
        $r = null;
        if (empty($id)) {
            return $r;
        }

        $article = self::join('countries', 'countries.id', '=', 'articles.article_country_id')
            ->join('article_status', 'article_status.id', '=', 'articles.article_status_id')
            ->join('article_types', 'article_types.id', '=', 'articles.article_type_id')
            ->join('popbox_services', 'popbox_services.id', '=', 'articles.article_service_id')
            ->where('articles.id', '=', $id)
            ->select('articles.*', 'countries.country_name', 'article_status.name as status', 'article_types.name as types', 'popbox_services.name as services')
            ->first();

        if (empty($article)) {
            return $r;
        }

        $result = $this->formatResult($article);

        return $result;
    }

    private function formatResult($article) {
        $result = null;

        if(is_a($article, 'Illuminate\Database\Eloquent\Collection')) {
            $articleList = [];

            foreach ($article as $item) {
                $tmp = $this->formatStdClass($item);
                $articleList[] = $tmp;
            }

            $result = $articleList;

        } else {
            $tmp = $this->formatStdClass($article);
            $result = $tmp;
        }

        return $result;
    }

    private function formatStdClass($item) {

        $urlForImage = env('URL_PATH');

        $tmp = new \stdClass();
        $tmp->id = $item->id;
        $tmp->priority = $item->article_priority;

        $articleContent = new \stdClass();
        $articleContent->notification_title = $item->notification_title;
        $articleContent->notification_subtitle = $item->notification_subtitle;
        $articleContent->title = $item->title;
        $articleContent->subtitle = $item->subtitle;
        $articleContent->content = $item->content;
        $articleContent->article_code = $item->promo_code;
        $articleContent->other_desc = $item->other_desc;

        $articleImage = new \stdClass();
        $articleImage->image_locker = $urlForImage.$item->image_locker;
        $articleImage->image_web = $urlForImage.$item->image_web;
        $articleImage->image_mobile = $urlForImage.$item->image_mobile;

        $articleDetail = new \stdClass();
        $articleDetail->service = $item->service;
        $articleDetail->status = $item->status;
        $articleDetail->type = $item->types;
        $articleDetail->country = $item->country_name;
        $articleDetail->start_date = $item->start_date;
        $articleDetail->end_date = $item->end_date;

        $tmp->article_content = $articleContent;
        $tmp->article_image = $articleImage;
        $tmp->article_detail = $articleDetail;

        return $tmp;
    }
}