<?php
namespace App\Models\PopApps\Article;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 15/06/18
 * Time: 22.13
 */
class ArticleStatus extends Model {

    protected $connection = 'popsend';
    protected $table = 'article_status';

    public function getStatus() {

        $status = self::all();

        return $status;
    }
}