<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'payment_transaction_histories';
}
