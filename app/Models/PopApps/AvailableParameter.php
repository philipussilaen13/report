<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class AvailableParameter extends Model
{
    protected $connection = 'popsend';
    protected $table = 'available_parameters';
}
