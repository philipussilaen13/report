<?php

namespace App\Models\PopApps;

use App\Http\Helpers\ApiExpressPopSend;
use Illuminate\Database\Eloquent\Model;

class DeliveryDestination extends Model
{
    protected $connection = 'popsend';
    protected $table = 'delivery_destinations';

    /**
     * re Push Express
     * @param $deliveryId
     * @return \stdClass
     */
    public static function rePushExpress($deliveryId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $dataDb = self::find($deliveryId);
        $expressUrl = $dataDb->express_url;
        $expressParam = json_decode($dataDb->express_parameter);
        if (empty($expressUrl) || empty($expressParam)){
            $response->errorMsg = 'Empty Express URL or Parameter';
            return $response;
        }

        // call library
        $apiExpress = new ApiExpressPopSend();
        $rePush = $apiExpress->rePushPickup($expressUrl,$expressParam);
        if (empty($rePush)){
            $response->errorMsg = 'Failed Push Pickup';
            return $response;
        }

        if ($rePush->response->code != 200){
            $response->errorMsg = $rePush->response->message;
            return $response;
        }

        $expressNumber = $rePush->data->pickup_request_number;

        $dataDb->pickup_number = $expressNumber;
        $dataDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function histories(){
        return $this->hasMany(DeliveryDestinationHistory::class);
    }
}
