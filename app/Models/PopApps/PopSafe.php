<?php

namespace App\Models\PopApps;

use App\Http\Helpers\ApiProjectX;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PopSafe extends Model
{
    protected $connection = 'popsend';
    protected $table = 'popsafes';

    /**
     * Cancel Order
     * @param $transactionId
     * @param null $remarks
     * @return \stdClass
     */
    public function cancelOrder($transactionId,$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $transactionDb = Transaction::where('transaction_type','popsafe')->where('id',$transactionId)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Invalid Transaction PopSafe';
            return $response;
        }

        $popSafeId = $transactionDb->transaction_id_reference;
        // get popsafe DB
        $popSafeDb = self::find($popSafeId);
        if (!$popSafeDb){
            $response->errorMsg = 'PopSafe Transaction Not Found';
            return $response;
        }

        $status = $popSafeDb->status;
        if ($status != 'CREATED'){
            $response->errorMsg = 'PopSafe Status are not CREATED. Status: '.$status;
            return $response;
        }
        $expressId = $popSafeDb->express_id;
        if (empty($expressId)){
            $response->errorMsg = "Prox Express ID not Found";
            return $response;
        }

        // change status to cancel
        $popSafeDb->status = 'CANCEL';
        $popSafeDb->save();

        $userName = Auth::user()->name;
        $user = "PopBox $userName";

        // insert history
        $popSafeHistoryModel = new PopSafeHistory();
        $insertHistory = $popSafeHistoryModel->insertPopSafeHistory($popSafeDb->id,'CANCEL',$user,$remarks);

        // cancel to Prox
        $apiProx = new ApiProjectX();
        $deleteImported = $apiProx->deleteImport($expressId);
        if (empty($deleteImported)){
            $response->errorMsg= 'Failed to Delete to Prox';
            return $response;
        }

        if ($deleteImported->response->code!=200){
            $response->errorMsg = $deleteImported->response->message;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Extend PopSafe
     * @param $invoiceCode
     * @param boolean $isAuto
     * @return \stdClass
     */
    public function extendPopSafe($invoiceCode,$isAuto = false){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->popSafeId = null;

        // get popsafe db
        $popSafeDb = self::where('invoice_code',$invoiceCode)->first();
        if (!$popSafeDb){
            $response->errorMsg = 'PopSafe Invoice Not Found';
            return $response;
        }
        $status = $popSafeDb->status;
        $expiredTime = $popSafeDb->expired_time;
        $autoExtend = $popSafeDb->auto_extend;

        // check expired / overdue time
        $nowTime = time();
        if ($nowTime < strtotime($expiredTime)){
            $response->errorMsg = 'PopSafes not EXPIRED';
            return $response;
        }
        // check if auto extend
        if (!$isAuto){
            if (!empty($autoExtend)){
                $response->errorMsg = 'Auto Extend Already Enabled';
                return $response;
            }
        }

        // add expired time
        $newExpired = new \DateTime($expiredTime);
        $newExpired->modify("+24 hours");
        $newExpired = $newExpired->format("Y-m-d H:i:s");

        // update expired on popsafe
        $popSafeDb = self::find($popSafeDb->id);
        $popSafeDb->status = 'IN STORE';
        $popSafeDb->expired_time = $newExpired;
        $popSafeDb->last_extended_datetime = date('Y-m-d H:i:s');
        $numberOfExtend = $popSafeDb->number_of_extend;
        $popSafeDb->number_of_extend = $numberOfExtend+1;
        $popSafeDb->save();

        $response->isSuccess = true;
        $response->popSafeId = $popSafeDb->id;

        // insert history
        $user = 'User';
        $remarks = 'Extend by User';
        if ($autoExtend){
            $user = 'System';
            $remarks = 'Auto Extend';
        }
        $insertHistory = PopsafeHistory::insertPopsafeHistory($popSafeDb->id,'IN STORE',$user,$remarks);

        return $response;
    }

    /**
     * Get List Of Order PopSafe
     * @param $userId
     * @param null $invoiceId
     * @return \stdClass
     */
    public static function getList($userId, $invoiceId=null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $historyDb = self::where('user_id','=',$userId)
            ->select('id', 'locker_name','locker_address','locker_address_detail','operational_hours','locker_number','code_pin','latitude','longitude','locker_size','transaction_date','cancellation_time',
                'expired_time','invoice_code','status','notes','total_price','item_photo')
            ->when($invoiceId,function ($query) use ($invoiceId){
                $query->where('invoice_code',$invoiceId);
            })
            ->orderby('created_at','DESC')
            ->paginate(10);

        if ($historyDb->isEmpty()){
            $response->isSuccess = true;
            $response->errorMsg = "You don't have any transactions";
            return $response;
        }
        $nowTime = time();
        foreach ($historyDb as $item) {
            $isCancel = 1;
            $isUse = 1;
            $isCollect = 0;
            $isExpired = 0;
            $status = $item->status;
            $imageName = $item->item_photo;
            $paid_price = 0;
            $promo_price = 0;

            // get promo details
            $promoUsed = DB::table('transactions')
                ->select('paid_amount', 'promo_amount', 'id')
                ->where('transaction_id_reference', $item->id)
                ->where('transaction_type', 'popsafe')
                ->first();

            if (!empty($promoUsed)) {
                $paid_price = (int)$promoUsed->paid_amount;
                $promo_price = (int)$promoUsed->promo_amount;
            }

            if ($item->status == 'CREATED'){
                // check cancellation time
                if ($nowTime > strtotime($item->cancellation_time)) $isCancel = 1;
                // check expired time
                if ($nowTime > strtotime($item->expired_time)){
                    $isUse = 0;
                    $isCancel = 0;
                    $isExpired = 0;
                }
            } elseif ($item->status == 'IN STORE'){
                $isCancel = 0;
                $isCollect = 1;
                // check expired time
                if ($nowTime > strtotime($item->expired_time)) {
                    $isUse = 0;
                    $isCollect = 0;
                    $status = 'OVERDUE';
                    $isExpired = 0;
                }
            } elseif ($item->status == 'COMPLETE'){
                $isCancel = 0;
            } elseif ($item->status == 'OVERDUE' || $item->status == 'EXPIRED' || $item->status == 'CANCEL'){
                $isUse = 0;
                $isCollect = 0;
                $isCancel = 0;
                $isExpired = 0;
            }

            if (!empty($imageName)){
                $imageName = url($imageName);
            }

            $item->is_cancel = $isCancel;
            $item->is_expired = $isUse;
            $item->is_collect = $isCollect;
            $item->is_expired = $isExpired;
            $item->status = $status;
            $item->item_photo = $imageName;
            $item->paid_price = $paid_price;
            $item->promo_price = $promo_price;
        }

        $response->isSuccess = true;
        $response->pagination = (int)ceil($historyDb->total()/$historyDb->perPage());
        $response->data = $historyDb->items();

        return $response;
    }

    /*Relationship*/
    public function histories(){
        return $this->hasMany(PopSafeHistory::class,'popsafe_id','id');
    }

    public static function getPinByInvoiceCode($invoice_code)
    {
        $data = self::where('invoice_code', '=', $invoice_code)
        ->pluck('code_pin');

        return $data;
    }
}
