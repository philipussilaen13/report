<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PickupDetail extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'pickup_details';
    protected $primaryKey = 'id';
    public $timestamps = true;
    use SoftDeletes;
}
