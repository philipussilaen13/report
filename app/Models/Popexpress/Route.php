<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'routes';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = true;
    use SoftDeletes;
}
