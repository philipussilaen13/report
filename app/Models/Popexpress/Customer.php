<?php

namespace App\Models\Popexpress;

use App\Account;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'customers';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = true;

    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function reference()
    {
        return $this->belongsTo(Customer::class, 'customer_reference');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, 'customer_id');
    }

    public static function listSelectbox()
    {
        return Customer::join('users', 'users.id', '=', 'customers.user_id')->addSelect('customers.id')->addSelect('customers.customer_code')->addSelect('users.name')->where('users.active', 1)->orderBy('users.name', 'asc')->get();
    }
}
