<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;

class EmployeeBranch extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'employee_branch';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
}
