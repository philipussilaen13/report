<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'branches';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = true;
    use SoftDeletes;
}
