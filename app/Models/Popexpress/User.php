<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
