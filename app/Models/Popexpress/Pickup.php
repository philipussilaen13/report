<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pickup extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'pickups';
    protected $primaryKey = 'id';
    public $timestamps = true;
    use SoftDeletes;
}
