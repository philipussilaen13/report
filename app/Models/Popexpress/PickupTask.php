<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PickupTask extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'pickup_tasks';
    protected $primaryKey = 'id';
    public $timestamps = true;
    use SoftDeletes;
}
