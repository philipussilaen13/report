<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'audit_trails';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = false;
}
