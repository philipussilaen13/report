<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'cities';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
}
