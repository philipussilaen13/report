<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'discounts';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = true;
    use SoftDeletes;
}
