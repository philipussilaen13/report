<?php

namespace App\Models\Warung;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReceiptBatch extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'receipt_batch';

    public static function getListReceiptBatch($receiptid) {        
        $popboxReportDb = env('DB_DATABASE', 'forge');

        $receiptBatchDb = self::where('receipt_batch.receipt_id', $receiptid)
            ->select(
                DB::raw("
                    receipt_batch.id as receipt_batch_id, receipt_batch.receipt_id, receipt_batch.batch_no,
                    receipt_batch.status_batch as status_batch_id,
                    case 
                        when receipt_batch.status_batch = 1 then 'Belum Dikonfirmasi'
                        when receipt_batch.status_batch = 2 then 'Terima Sebagian'
                        when receipt_batch.status_batch = 3 then 'Terima Semua'
                        when receipt_batch.status_batch = 4 then 'Tolak Semua'
                        else null
                        end as status_batch,
                    receipt_batch.users_id,
                    date_format(receipt_batch.created_at, '%d-%m-%Y') as receipt_create_batch_date,
                    date_format(receipt_batch.updated_at, '%d-%m-%Y') as receipt_batch_date                
                ")
            )
            ->get();
        
        return $receiptBatchDb;
    }

    public static function getReceiptBatchByBatchNo($batchNo, $transactionItemID) {
        $receiptBatchDb = self::where('receipt_batch.batch_no', $batchNo)
            ->where('receipt_items.transaction_items_id', $transactionItemID)
            ->leftJoin('receipt_items', 'receipt_batch.id', '=', 'receipt_items.receipt_batch_id')
            ->select( 
                DB::raw("
                    receipt_items.id as receipt_items_id, receipt_items.gap_order
                ")
            )->first();
        return $receiptBatchDb;
    }

    public static function insert($params) {        
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        $batchNo = null;

        if( empty($params['receipt_id'])) {
            $response->isSuccess = false;
            $response->errorMsg = 'Receipt ID is null';
            return $response;
        }

        $receiptBatchDb = self::where('receipt_id', $params['receipt_id'])
            ->select(
                DB::raw("
                    IFNULL(MAX(CAST(SUBSTRING_INDEX(batch_no,'-',-1) AS UNSIGNED)),0)+1 AS batch_no
                ")
            )
            ->first();
        if( !empty($receiptBatchDb->batch_no) ) {
            $batchNo = 'BATCH-'.$receiptBatchDb->batch_no;
        }

        $mresult = new self();
        $mresult->receipt_id = $params['receipt_id'];
        $mresult->batch_no = $batchNo;
        $mresult->status_batch = $params['status_batch'];
        $mresult->users_id = $params['users_id'];
        $mresult->created_at = date('Y-m-d H:i:s');
        $mresult->updated_at = null;
        $mresult->save();

        $response->isSuccess = true;
        $response->data = [
            'batch_id' => $mresult->id
        ];
        return $response;
    }
}
