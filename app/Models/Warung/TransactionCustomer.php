<?php

namespace App\Models\Warung;

use Illuminate\Database\Eloquent\Model;

class TransactionCustomer extends Model
{
    protected $connection = 'product_service';
    protected $table = 'transaction_customer';
}
