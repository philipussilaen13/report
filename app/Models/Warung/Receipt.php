<?php

namespace App\Models\Warung;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_agent';
    protected $table = 'receipt';

    public static function getListReceipt($params) {        
        $virtualDb = env('DB_VIRTUAL_DATABASE', 'forge');

        $start = (int) $params['start'];               
        $limit = (int) $params['limit'];               

        $result = [];
        $receiptDb = self::leftJoin('transactions', 'receipt.transaction_id', '=', 'transactions.id')
            ->leftJoin($virtualDb.'.lockers', 'receipt.locker_id', '=', $virtualDb.'.lockers.locker_id')
            ->select(
                DB::raw("
                    receipt.id as receipt_id, receipt.transaction_id, transactions.users_id, receipt.locker_id, receipt.reference,
                    receipt.status_receipt as status_receipt_id,
                    case 
                        when receipt.status_receipt = 1 then 'Belum Dikonfirmasi'
                        when receipt.status_receipt = 2 then 'Terima Sebagian'
                        when receipt.status_receipt = 3 then 'Sudah Terima Semua Barang'
                        else null
                    end as status_receipt,
                    date_format(receipt.updated_at, '%d-%m-%Y') as receipt_date,
                    date_format(transactions.updated_at, '%d-%m-%Y') as transaction_date,
                    lockers.locker_name            
                ")
            );

        if( !empty($params['statusid'])) {
            $receiptDb = $receiptDb->where('receipt.status_receipt', $params['statusid']);
        }

        if( !empty($params['lockerid'])) {
            $receiptDb = $receiptDb->where('receipt.locker_id', $params['lockerid']);
        }

        if( !empty($params['reference'])) {
            $receiptDb = $receiptDb->where('receipt.reference', 'like', '%' . $params['reference'] . '%');
        }

        if( !empty($params['start_date']) && !empty($params['end_date'])) {
            $receiptDb = $receiptDb->whereRaw("date(transactions.updated_at) between date(?) and date(?)", [$params['start_date'], $params['end_date']]);
        }

        $result['count'] = $receiptDb->count();
        
        $receiptDb = $receiptDb->take($limit)->skip($start)->orderBy('receipt.updated_at', 'desc');
        
        $result['data'] = $receiptDb->get();

        return $result;
    }

    public static function getReceiptByID($receiptid) {
        $virtualDb = env('DB_VIRTUAL_DATABASE', 'forge');

        $receiptDb = self::leftJoin('transactions', 'receipt.transaction_id', '=', 'transactions.id')
            ->leftJoin($virtualDb.'.lockers', 'receipt.locker_id', '=', $virtualDb.'.lockers.locker_id')
            ->where('receipt.id', $receiptid)
            ->select(
                DB::raw("
                    receipt.id as receipt_id, receipt.transaction_id, transactions.users_id, receipt.locker_id, receipt.reference,
                    receipt.status_receipt as status_receipt_id,
                    case 
                        when receipt.status_receipt = 1 then 'Belum Dikonfirmasi'
                        when receipt.status_receipt = 2 then 'Terima Sebagian'
                        when receipt.status_receipt = 3 then 'Sudah Terima Semua Barang'
                        else null
                    end as status_receipt,
                    date_format(receipt.updated_at, '%d-%m-%Y') as receipt_date,
                    date_format(transactions.updated_at, '%d-%m-%Y') as transaction_date,
                    lockers.locker_name            
                ")
            )
            ->first();
        
        return $receiptDb;
    }

    public static function getReceiptSuggestDelivery($receiptid) {
        $receiptDb = DB::connection('popbox_agent')
            ->select("
                SELECT a.id, c.id AS transaction_items_id, IFNULL(CAST(JSON_EXTRACT(c.params, '$.amount') AS UNSIGNED),0) AS qty_order, 
                    IFNULL(d.qty_receipt,0) AS qty_receipt,
                    IFNULL(e.qty_delivery,0) AS qty_delivery,
                    CASE 
                        WHEN (IFNULL(CAST(JSON_EXTRACT(c.params, '$.amount') AS UNSIGNED),0) - IFNULL(d.qty_receipt,0)) = 0 THEN 0
                        ELSE 
                            CASE 
                                WHEN IFNULL(e.qty_delivery,0) = 0 THEN IFNULL(CAST(JSON_EXTRACT(c.params, '$.amount') AS UNSIGNED),0) - IFNULL(d.qty_receipt,0)
                                ELSE 
                                    IFNULL(CAST(JSON_EXTRACT(c.params, '$.amount') AS UNSIGNED),0) - IFNULL(e.qty_delivery,0) - IFNULL(d.qty_receipt,0)
                            END
                    END AS gap_qty,
                    c.name, c.params  	
                FROM receipt a
                LEFT JOIN transactions b ON a.transaction_id = b.id
                LEFT JOIN transaction_items c ON b.id = c.transaction_id
                LEFT JOIN (
                    SELECT a.id AS receiptid, c.transaction_items_id, 
                        IFNULL(SUM(c.qty_receipt),0) AS qty_receipt
                    FROM receipt a
                    LEFT JOIN receipt_batch b ON b.receipt_id = a.id
                    LEFT JOIN receipt_items c ON c.receipt_batch_id = b.id
                    WHERE b.status_batch IN('2','3')
                    GROUP BY a.id, c.transaction_items_id
                ) d ON c.id = d.transaction_items_id
                LEFT JOIN (
                    SELECT a.id AS receiptid, c.transaction_items_id, 
                    IFNULL(SUM(c.qty_delivery),0) AS qty_delivery
                    FROM receipt a
                    LEFT JOIN receipt_batch b ON b.receipt_id = a.id
                    LEFT JOIN receipt_items c ON c.receipt_batch_id = b.id
                    WHERE b.status_batch = 1
                    GROUP BY a.id, c.transaction_items_id
                ) e ON c.id = e.transaction_items_id
                WHERE a.id = ?        
            ", [$receiptid]);
        return $receiptDb;
    }

    public static function getSummaryDelivery() {
        $receiptDb = collect(DB::connection('popbox_agent')
            ->select("
                select sum( case when status_receipt = 1 then 1 else 0 end ) as status_unprocess,
                    sum( case when status_receipt = 2 then 1 else 0 end ) as status_partial,
                    sum( case when status_receipt = 3 then 1 else 0 end ) as status_completed
                from receipt
                where deleted_at is null            
            "));
        return $receiptDb->first();
    }

    /*Relationship*/
    public function itemBatch() {
        return $this->hasMany(ReceiptBatch::class,'receipt_id','id');
    }
}
