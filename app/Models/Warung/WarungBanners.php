<?php

namespace App\Models\Warung;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarungBanners extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'banners';
    use SoftDeletes;
}
