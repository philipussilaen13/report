<?php

namespace App\Models\Warung;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionReportConfiguration extends Model
{
    use SoftDeletes;
    
    protected $connection = 'popbox_agent';
    protected $table = 'report_configuration_jobs';
    protected $dates = ['deleted_at'];
}
