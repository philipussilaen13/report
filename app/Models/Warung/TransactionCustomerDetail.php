<?php

namespace App\Models\Warung;

use Illuminate\Database\Eloquent\Model;

class TransactionCustomerDetail extends Model
{
    protected $connection = 'product_service';
    protected $table = 'transaction_customer_detail';
}
