<?php

namespace App\Models\Warung;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReceiptItem extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'receipt_items';

    public static function getListReceiptItem($receiptid, $batchid) {        
        $receiptItemDb = DB::connection('popbox_agent')->select("
            select a.id as receipt_item_id, a.transaction_items_id,
                a.qty_delivery, a.qty_receipt, b.name, b.type, b.params,
                ifnull(d.gap_order, ifnull(cast(json_extract(b.params, '$.amount') as unsigned),0)) as totorder,
                ifnull(d.gap_order, ifnull(cast(json_extract(b.params, '$.amount') as unsigned),0)) - ifnull(a.qty_receipt,0) as gaporder
            from receipt_items a
            left join transaction_items b on a.transaction_items_id = b.id
            left join receipt_batch c on a.receipt_batch_id = c.id
            left join (
                select a.receipt_batch_id, a.receipt_id, b.transaction_items_id, b.gap_order, a.rank
                from (
                    select a.id as receipt_batch_id, a.receipt_id, a.batch_no, @rank := @rank + 1 AS rank
                    from receipt_batch a, (SELECT @rank := 0) rk
                    where a.receipt_id = ?
                    order by a.id asc
                ) a
                left join receipt_items b on a.receipt_batch_id = b.receipt_batch_id
            ) d on a.transaction_items_id = d.transaction_items_id and d.rank = (cast(substring(c.batch_no, 7) as unsigned) - 1)
            where a.receipt_batch_id = ?            
        ", [$receiptid, $batchid]);

        return $receiptItemDb;
    }

    public static function insert($params) {        
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        if( empty($params['receipt_batch_id'])) {
            $response->isSuccess = false;
            $response->errorMsg = 'field receipt_batch_id is null';
            return $response;
        }

        $mresult = new self();
        $mresult->receipt_batch_id = $params['receipt_batch_id'];
        $mresult->transaction_items_id = $params['transaction_items_id'];
        $mresult->qty_delivery = $params['qty_delivery'];
        $mresult->qty_receipt = $params['qty_receipt'];
        $mresult->created_at = date('Y-m-d H:i:s');
        $mresult->updated_at = date('Y-m-d H:i:s');
        $mresult->save();

        $response->isSuccess = true;
        return $response;
    }
}
