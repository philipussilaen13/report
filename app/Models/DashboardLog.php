<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class DashboardLog extends Model
{
    protected $table = 'dashboard_logs';

    /*============ Public Function ============*/
    /**
     * insert log with request
     * @param $userId
     * @param $moduleId
     * @param $request
     * @return mixed
     */
    public static function insertRequest($userId,$moduleId,Request $request){
        $logDb = new self();
        $logDb->module_id = $moduleId;
        $logDb->user_id = $userId;
        $logDb->session_id = $request->session()->getId();
        $logDb->ip = $request->ip();
        $logDb->device = $request->header('User-Agent');
        $logDb->request = json_encode($request->input());
        $logDb->save();

        return $logDb->id;
    }

    /**
     * update previous request with response
     * @param $logId
     * @param $response
     */
    public static function updateResponse($logId,$response){
        $logDb = self::find($logId);
        $logDb->response = $response->content();
        $logDb->save();
        return;
    }
    /*========== End Public Function ==========*/
}
