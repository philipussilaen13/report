<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class SepulsaLocation extends Model
{
    // set table
    protected $connection = 'popbox_db';
    protected $table = 'tb_sepulsa_location';
    public $timestamps = false;
}
