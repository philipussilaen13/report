<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class BuildingType extends Model
{
    // set connection and table
    protected $connection = 'popbox_db';
    protected $table = 'buildingtypes';
}
