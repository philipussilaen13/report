<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class LockerActivities extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_activities';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
}
