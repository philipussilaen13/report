<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class SepulsaTransaction extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_sepulsa_transaction';
}
