<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommissionRule extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_agent';
    protected $table = 'commission_rules';

    /*Relationship*/
    public function parameter(){
        return $this->belongsTo(AvailableParameter::class,'available_parameter_id','id');
    }
}
