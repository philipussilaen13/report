<?php

namespace App\Models\Agent;

use App\Http\Helpers\ApiPayment;
use Illuminate\Database\Eloquent\Model;

class LockerVAOpen extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'locker_va_open';

    public static function createVAOpen($lockerId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->successCreate = null;
        $response->failedCreate = null;
        $availableOpen = ['bni_va','doku_bca_va'];

        // get locker ID
        $agentLockerDb = AgentLocker::where('id',$lockerId)->first();
        if (!$agentLockerDb){
            $failedCreate[] = "$lockerId NotFound";
            $response->failedCreate = $failedCreate;
            return $response;
        }
        $lockerName = $agentLockerDb->locker_name;

        $successCreate = [];
        $failedCreate = [];

        foreach ($availableOpen as $openVa){
            $lockerVaOpenDb = self::where('lockers_id',$lockerId)->where('va_type',$openVa)->first();
            if ($lockerVaOpenDb) {
                $successCreate[] = "$lockerId already have $openVa";
                continue;
            }
            // get user phone
            $userDb = User::where('locker_id',$lockerId)
                ->where('status','<>','0')
                ->first();
            if (!$userDb) {
                $failedCreate[] = "$lockerId doesn't have user / not active";
                continue;
            }
            // get phone
            $phone = $userDb->phone;
            $vaNumber = substr($phone,-10);
            $expiredYears = 50;

            $method = null;
            if ($openVa == 'bni_va') $method = 'BNI-VA';
            elseif ($openVa == 'doku_bca_va') $method = 'DOKU-BCA';

            $param = [];
            $param['method_code'] = $method;
            $param['amount'] = '0';
            $param['billing_type'] = 'open';
            $param['transaction_id'] = "$lockerId-$method";
            $param['customer_name'] = "PT POPBOX ASIA - $lockerName";
            $param['description'] = 'Top Up Open '.$lockerName;
            $param['virtual_account'] = $vaNumber;
            $param['datetime_expired'] = date('Y-m-d H:i:s',strtotime("+$expiredYears years"));

            // post to API
            $apiV2 = new ApiPayment();
            $result =  $apiV2->createPayment($param);

            echo "Push to API Payment \n";

            if (empty($result)){
                $failedCreate[] = "Failed Create $openVa";
                continue;
            }
            if ($result->response->code!=200){
                $message = $result->response->message;
                $failedCreate[] = "Failed Create $openVa : $message";
                continue;
            }
            $data = $result->data[0];

            $vaNumber = $data->virtual_account_number;
            $expiredDate = $data->datetime_expired;
            $paymentId = $data->payment_id;

            $lockerVAData = new self();
            $lockerVAData->lockers_id = $lockerId;
            $lockerVAData->va_type = $openVa;
            $lockerVAData->va_number = $vaNumber;
            $lockerVAData->payment_id = $paymentId;
            $lockerVAData->expired_date = $expiredDate;
            $lockerVAData->save();

            $successCreate[] = "Success Create VA Open $openVa";
        }

        $response->successCreate = $successCreate;
        $response->failedCreate = $failedCreate;
        $response->isSuccess = true;
        return $response;
    }
}
