<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class ReferralCampaign extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'referral_campaigns';
}
