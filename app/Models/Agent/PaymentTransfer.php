<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class PaymentTransfer extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'payment_transfers';

    /**
     * get Available Bank
     * @return array
     */
    public static function getAvailableBanks()
    {
        $availableBanks = [];
        $tmp = new \stdClass();
        $tmp->id = "BCA1";
        $tmp->bank_name = 'Bank Central Asia (BCA)';
        $tmp->bank_code = 'BCA';
        $tmp->bank_account = '5260-35-8822';
        $tmp->bank_user = 'PT. Popbox Asia Services';
        $tmp->branch = 'Grand Slipi Tower, Jakarta';
        $availableBanks[] = $tmp;

        $tmp = new \stdClass();
        $tmp->id = "MANDIRI1";
        $tmp->bank_name = 'Bank Mandiri';
        $tmp->bank_code = 'MANDIRI';
        $tmp->bank_account = '165-000-91-2222-8';
        $tmp->bank_user = 'PT. Popbox Asia Services';
        $tmp->branch = 'Grand Slipi Tower, Jakarta';
        $availableBanks[] = $tmp;
        return $availableBanks;
    }

    /**
     * Create Bank Transfer
     * @param $paymentId
     * @param $bankId
     * @param null $senderName
     * @param null $senderBank
     * @return \stdClass
     */
    public static function createPaymentTransfer($paymentId, $bankId, $senderName = null, $senderBank = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get available banks
        $selectedBank = null;
        foreach (self::getAvailableBanks() as $bank) {
            if ($bank->id == $bankId) {
                $selectedBank = $bank;
            }
        }
        if (empty($selectedBank)) {
            $response->errorMsg = "Invalid Bank Id";
            return $response;
        }

        $destinationBank = $selectedBank->bank_name;
        $destinationAccount = $selectedBank->bank_account;

        // insert to database
        $paymentDb = new self();
        $paymentDb->payments_id = $paymentId;
        $paymentDb->destination_bank = $destinationBank;
        $paymentDb->destination_account = $destinationAccount;
        $paymentDb->sender_bank = $senderBank;
        $paymentDb->sender_name = $senderName;
        $paymentDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Add Payment
     * @param $transactionRef
     * @param $method
     * @param string $module
     * @return \stdClass
     */
    public static function addPayment($transactionRef, $method, $module = 'topup')
    {
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // check transaction
        $transactionDb = Transaction::where('reference', $transactionRef)->first();
        if (!$transactionDb) {
            $response->errorMsg = 'Invalid Transaction';
            return $response;
        }

        $module = $transactionDb->type;
        if (empty($module)) $module = 'topup';

        // check method
        $methodDb = PaymentMethod::join('payment_available_module', 'payment_available_module.payment_methods_id', 'payment_methods.id')
            ->where('module', $module)
            ->where('code', $method)
            ->first();

        if (!$methodDb) {
            $response->errorMsg = 'Invalid Method Payment';
            return $response;
        }

        // check on DB payment
        $checkDb = self::where('transactions_id', $transactionDb->id)->first();
        if ($checkDb) {
            $response->errorMsg = 'Transaction Payment Already Exist';
            return $response;
        }

        // set time limit
        $timeLimit = date('Y-m-d H:i:s', strtotime("+3 hours"));
        if ($method == 'deposit') {
            $timeLimit = date('Y-m-d H:i:s', strtotime("+1 days"));
        } elseif ($method == 'transfer') {
            $timeLimit = date('Y-m-d H:i:s', strtotime("+1 days"));
        } elseif ($method == 'doku_va') {
            $timeLimit = date('Y-m-d H:i:s', strtotime("+12 hours"));
        }

        // insert into payment
        $data = new self();
        $data->payment_methods_id = $methodDb->payment_methods_id;
        $data->transactions_id = $transactionDb->id;
        $data->amount = $transactionDb->total_price;
        $data->status = 'WAITING';
        $data->time_limit = $timeLimit;
        $data->save();

        // update transaction into waiting
        $transactionUpdate = Transaction::updateStatusTransaction($transactionDb->id, 'WAITING');
        if (!$transactionUpdate->isSuccess) {
            $response->errorMsg = 'Invalid Status';
            return $response;
        }

        $response->isSuccess = true;
        $response->paymentId = $data->id;
        return $response;
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'payments_id', 'id');
    }
}
