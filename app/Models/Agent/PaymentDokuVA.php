<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class PaymentDokuVA extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'payment_doku_va';

    /*Relationship*/
    public function payment()
    {
        return $this->hasOne(Payment::class, 'payments_id', 'id');
    }
}
