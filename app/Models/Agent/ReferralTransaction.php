<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class ReferralTransaction extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'referral_transactions';

    /*Relationship*/
    public function referralCampaign(){
        return $this->belongsTo(ReferralCampaign::class);
    }

    public function fromAgent(){
        return $this->belongsTo(AgentLocker::class,'from_locker_id','id');
    }

    public function toAgent(){
        return $this->belongsTo(AgentLocker::class,'to_locker_id','id');
    }
}
