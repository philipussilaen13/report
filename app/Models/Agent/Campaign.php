<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Campaign extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_agent';
    protected $table = 'campaigns';

    /**
     * Save Data
     * @param Request $request
     * @return \stdClass
     */
    public static function saveData(Request $request){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get all input
        $campaignId = $request->input('campaignId',null);
        $name = $request->input('name');
        $description = $request->input('description',null);
        $startDate = date('Y-m-d',strtotime($request->input('startDate')))." 00:00:00";
        $endDate = date('Y-m-d',strtotime($request->input('endDate')))." 23:59:59";
        $status = $request->input('status');
        $priority = $request->input('priority');
        $promoType = $request->input('promoType');
        $type = $request->input('type');
        $amount = $request->input('amount');
        $category = $request->input('category','cutback');
        $voucherRequired = $request->input('voucherRequired');
        $limitType = $request->input('limitType');
        $limitUsage = $request->input('limitUsage',null);
        $minAmount = $request->input('minAmount',null);
        $maxAmount = $request->input('maxAmount',null);
        $availableParameterId = $request->input('parameterId',[]);
        $operatorParameter = $request->input('operator',[]);
        $valueParameter = $request->input('value',[]);

        // if percentage not more than 100
        if ($type == 'percent'){
            if ($amount > 100){
               $response->errorMsg = 'Amount Percent cant larger than 100';
               return $response;
            }
        }

        // save to DB
        if (empty($campaignId)){
            $campaignDb = new self();
        } else {
            $campaignDb = self::find($campaignId);
        }

        $campaignDb->name = $name;
        $campaignDb->description = $description;
        $campaignDb->start_time = $startDate;
        $campaignDb->end_time = $endDate;
        $campaignDb->status = $status;
        $campaignDb->promo_type = $promoType;
        $campaignDb->type = $type;
        $campaignDb->category = $category;
        $campaignDb->voucher_required = $voucherRequired;
        $campaignDb->campaign_limit_type = $limitType;
        $campaignDb->priority = $priority;
        $campaignDb->amount = $amount;
        if (!empty($maxAmount)) {
            $campaignDb->max_amount = $maxAmount;
        }
        if (!empty($minAmount)){
            $campaignDb->min_amount = $minAmount;
        }
        if (!empty($limitUsage)){
            $campaignDb->limit_usage = $limitUsage;
        }
        $campaignDb->approved_by = null;
        $campaignDb->approval_status = 'pending';
        $campaignDb->save();

        $campaignId = $campaignDb->id;

        // save campaign Param
        $saveCampaignParameter = CampaignParameter::saveParameter($campaignId,$availableParameterId,$operatorParameter,$valueParameter);
        if (!$saveCampaignParameter){
            $response->errorMsg = 'Failed Save Campaign parameter';
            return $response;
        }

        $response->isSuccess = true;
        $response->campaignId = $campaignId;
        return $response;
    }

    /*Relationship*/
    public function vouchers(){
        return $this->hasMany(CampaignVoucher::class);
    }

    public function parameters(){
        return $this->hasMany(CampaignParameter::class);
    }
}
