<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Version extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'version';

    public static function getListVersion($start, $limit)
    {
        $query = self::take($limit)->skip($start)->orderBy('updated_at', 'desc')->get();

        $no = 1; $data = [];
        foreach($query as $r) {
            $r['no'] = $no;

            $data[] = $r;
            $no++;
        }

        $result = [
            'data' => $data,
            'count' => self::count()
        ];
        return $result;
    }

    public static function checkVersion($version, $id, $act)
    {
        if($act == 'add') {
            $mresultVersion = Version::where('version', '=', $version)
                ->count();
        }
        else {
            $mresultVersion = Version::where('version', '=', $version)
                -> where('id', '<>',  $id)
                ->count();
        }
        return $mresultVersion;
    }

    public static function insertVersion($version)
    {
        DB::beginTransaction();
        $data = new self();
        $data->version = $version;
        $data->created_at = date('Y-m-d H:i:s');
        $data->updated_at = date('Y-m-d H:i:s');
        $data->created_by = null;
        $data->updated_by = null;
        $data->save();
        DB::commit();
        return $data;
    }

    public static function updateVersion($id, $version)
    {
        DB::beginTransaction();
        $data = self::where('id', $id)->update([
            'version' => $version,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => null,
        ]);
        DB::commit();
        return $data;
    }
}
