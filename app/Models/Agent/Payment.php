<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'payments';

    public static function addPayment($transactionRef, $method, $module = 'topup')
    {
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // check transaction
        $transactionDb = Transaction::where('reference', $transactionRef)->first();
        if (!$transactionDb) {
            $response->errorMsg = 'Invalid Transaction';
            return $response;
        }

        $module = $transactionDb->type;
        if (empty($module)) $module = 'topup';

        // check method
        $methodDb = PaymentMethod::join('payment_available_module', 'payment_available_module.payment_methods_id', 'payment_methods.id')
            ->where('module', $module)
            ->where('code', $method)
            ->first();

        if (!$methodDb) {
            $response->errorMsg = 'Invalid Method Payment';
            return $response;
        }

        // check on DB payment
        $checkDb = self::where('transactions_id', $transactionDb->id)->first();
        if ($checkDb) {
            $response->errorMsg = 'Transaction Payment Already Exist';
            return $response;
        }

        // set time limit
        $timeLimit = date('Y-m-d H:i:s', strtotime("+3 hours"));
        if ($method == 'deposit') {
            $timeLimit = date('Y-m-d H:i:s', strtotime("+1 days"));
        } elseif ($method == 'transfer') {
            $timeLimit = date('Y-m-d H:i:s', strtotime("+1 days"));
        } elseif ($method == 'doku_va') {
            $timeLimit = date('Y-m-d H:i:s', strtotime("+12 hours"));
        }

        // insert into payment
        $data = new self();
        $data->payment_methods_id = $methodDb->payment_methods_id;
        $data->transactions_id = $transactionDb->id;
        $data->amount = $transactionDb->total_price;
        $data->status = 'WAITING';
        $data->time_limit = $timeLimit;
        $data->save();

        // update transaction into waiting
        $transactionUpdate = Transaction::updateStatusTransaction($transactionDb->id, 'WAITING');
        if (!$transactionUpdate->isSuccess) {
            $response->errorMsg = 'Invalid Status';
            return $response;
        }

        $response->isSuccess = true;
        $response->paymentId = $data->id;
        return $response;
    }
    
    
    
    /**
     * pay payment
     * @param $transactionRef
     * @param $username
     * @return \stdClass
     */
    public static function updateTransactionItemStatus($transaction_id, $item, $user){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        $paymentDb = self::where('transactions_id',$transaction_id)->first();
        if (!$paymentDb){
            $response->errorMsg = 'Transaction Not Available in Payment';
            \Log::debug($response->errorMsg);
            return $response;
        }
        
        // check time limit payment
//         $timeLimit = strtotime($paymentDb->time_limit);
//         $now = time();
//         if ($now > $timeLimit){
//             $response->errorMsg = 'Transaction Already Expired';
//             return $response;
//         }
        
        // update status to paid
        $data = self::find($paymentDb->id);
        $data->status = 'PAID';
        $data->save();
        
        $transaction_item = TransactionItem::find($item['id']);
        $transaction_item->cod_status = $item['status'];
        $transaction_item->status_push = 'COMPLETED';
        $transaction_item->save();
        
        $remarks = 'Update COD status for item with item id '.$item['id'];
        $historyDb = TransactionHistory::createNewHistory($transaction_id, $user, 'PAID', $remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            \Log::debug($response->errorMsg);
            return $response;
        }
        
        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function method()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_methods_id', 'id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transactions_id', 'id');
    }

    public function transfer()
    {
        return $this->belongsTo(PaymentTransfer::class, 'payments_id', 'id');
    }
}
