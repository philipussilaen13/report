<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    // set connection
    protected $connection = 'popbox_agent';
    protected $table = 'transaction_histories';
//    protected $fillable = ['transaction_id', 'user', 'status', 'remarks'];

    /**
     * create new history
     * @param $transactionId
     * @param $user
     * @param $status
     * @param null $remark
     * @return \stdClass
     */
    public static function createNewHistory($transactionId, $user, $status, $remark = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $data = new self();
        $data->transactions_id = $transactionId;
        $data->user = $user;
        $data->status = $status;
        $data->remarks = $remark;
        $data->save();

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id', 'id');
    }

}
