<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PendingTransaction extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_agent';
    protected $table = 'pending_transactions';
}
