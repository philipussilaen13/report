<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class BalanceRecord extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'balance_records';

    /**
     * deduct deposit
     * @param $lockerId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public static function deductDeposit($lockerId, $amount, $transactionId)
    {
        // generate standard response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get lockerId Deposit
        $lockerDb = AgentLocker::find($lockerId);
        if (!$lockerDb) {
            $response->errorMsg = 'Invalid Locker';
            return $response;
        }
        $currentBalance = $lockerDb->balance;
        if ($currentBalance < $amount) {
            $response->errorMsg = 'Insufficient Deposit';
            return $response;
        }
        // deduct deposit
        $newBalance = $currentBalance - $amount;

        // update to locker balance
        $lockerDb->balance = $newBalance;
        $lockerDb->save();

        // insert into balance record
        $credit = 0;
        $debit = $amount;
        $balanceRecordDb = self::insertNewRecord($lockerId, $transactionId, $credit, $debit, $newBalance);

        $response->isSuccess = true;
        return $response;
    }

    /**
     * credit / top up deposit
     * @param $lockerId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public static function creditDeposit($lockerId, $amount, $transactionId)
    {
        // generate standard response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get lockerId Deposit
        $lockerDb = AgentLocker::find($lockerId);
        if (!$lockerDb) {
            $response->errorMsg = 'Invalid Locker';
            return $response;
        }
        $currentBalance = $lockerDb->balance;
        // credit deposit
        $newBalance = $currentBalance + $amount;

        // update to locker balance
        $lockerDb->balance = $newBalance;
        $lockerDb->save();

        // insert into balance record
        $credit = $amount;
        $debit = 0;
        $balanceRecordDb = self::insertNewRecord($lockerId, $transactionId, $credit, $debit, $newBalance);

        $response->isSuccess = true;
        return $response;
    }

    /**
     * insert new record
     * @param $lockerId
     * @param $transactionId
     * @param int $credit
     * @param int $debet
     * @param $balance
     */
    public static function insertNewRecord($lockerId, $transactionId, $credit = 0, $debit = 0, $balance)
    {
        $data = new self();
        $data->lockers_id = $lockerId;
        $data->transactions_id = $transactionId;
        $data->credit = $credit;
        $data->debit = $debit;
        $data->balance = $balance;
        $data->date = date('Y-m-d');
        $data->save();

        return;
    }
}
