<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $connection = 'popbox_agent';
    protected $primaryKey = 'id';
    protected $table = 'articles';
}
