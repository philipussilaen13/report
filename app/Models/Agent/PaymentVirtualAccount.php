<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class PaymentVirtualAccount extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'payment_virtual_accounts';
}
