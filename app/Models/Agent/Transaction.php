<?php

namespace App\Models\Agent;

use App\Http\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_agent';
    protected $table = 'transactions';

    /**
     * create refund transaction
     * @param $userId
     * @param $remarks
     * @param $reference
     * @param $refund
     * @return \stdClass
     */
    public static function createRefundTransaction($userId, $remarks, $reference, $refund)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        $transactionDb = new Transaction();
        $transactionDb->users_id = $userId;
        // create transaction reference
        $transactionReference = 'TRX-' . $userId . date('ymdhi') . Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = 'refund';
        $transactionDb->cart_id = null;
        $transactionDb->description = "Refund  $remarks from PopBox " . Auth::user()->name;
        // create total price
        $transactionDb->total_price = $refund;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;
        // insert transaction items
        $itemsDb = new TransactionItem();
        $itemsDb->transaction_id = $transactionId;
        $itemsDb->name = "Refund Transaction From $reference";
        $itemsDb->type = 'refund';
        $params = [];
        $params['users'] = Auth::user()->name;
        $itemsDb->params = json_encode($params);
        $itemsDb->price = $refund;
        $itemsDb->save();

        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        return $response;
    }

    /**
     * create credit transaction
     * @param $type
     * @param $userId
     * @param $remarks
     * @param $amount
     * @return \stdClass
     */
    public static function createCreditTransaction($type, $userId, $remarks, $amount)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionRef = null;

        $transactionDb = new Transaction();
        $transactionDb->users_id = $userId;
        // create transaction reference
        $transactionReference = 'TRX-' . $userId . date('ymdhi') . Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = $type;
        $transactionDb->cart_id = null;
        $transactionDb->description = "$remarks Action from : " . Auth::user()->name;
        // create total price
        $transactionDb->total_price = $amount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $tmpRemarks = substr($remarks, 0, 190);
        $transactionId = $transactionDb->id;
        // insert transaction items
        $itemsDb = new TransactionItem();
        $itemsDb->transaction_id = $transactionId;
        $itemsDb->name = "$tmpRemarks Action from : " . Auth::user()->name;
        $itemsDb->type = $type;
        $params = [];
        $params['users'] = Auth::user()->name;
        $itemsDb->params = json_encode($params);
        $itemsDb->price = $amount;
        $itemsDb->save();

        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        return $response;
    }

    /**
     * @param $type
     * @param $userId
     * @param $remarks
     * @param $amount
     * @return \stdClass
     */
    public static function createDebitTransaction($type, $userId, $remarks, $amount)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        $transactionDb = new Transaction();
        $transactionDb->users_id = $userId;
        // create transaction reference
        $transactionReference = 'TRX-' . $userId . date('ymdhi') . Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = $type;
        $transactionDb->cart_id = null;
        $transactionDb->description = "$remarks Action from : " . Auth::user()->name;
        // create total price
        $transactionDb->total_price = $amount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;
        $tmpRemarks = substr($remarks, 0, 190);
        // insert transaction items
        $itemsDb = new TransactionItem();
        $itemsDb->transaction_id = $transactionId;
        $itemsDb->name = "$tmpRemarks Action from : " . Auth::user()->name;
        $itemsDb->type = $type;
        $params = [];
        $params['users'] = Auth::user()->name;
        $itemsDb->params = json_encode($params);
        $itemsDb->price = $amount;
        $itemsDb->save();

        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        return $response;
    }

    /**
     * update status transaction
     * @param $transactionId
     * @param $status
     * @return \stdClass
     */
    public static function updateStatusTransaction($transactionId, $status)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $statusList = ['UNPAID', 'WAITING', 'PAID', 'CANCELED'];
        $status = strtoupper($status);
        if (!in_array($status, $statusList)) {
            $response->errorMsg = 'Invalid Status';
            return $response;
        }

        // update to DB
        $data = self::find($transactionId);
        $data->status = $status;
        $data->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Refund Commission Transaction
     * @param $transactionId
     * @return \stdClass
     */
    public static function refundCommissionTransaction($transactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get commission transaction with reference
        $transactionDb = Transaction::where('type','commission')
            ->where('transaction_id_reference',$transactionId)
            ->first();
        if (!$transactionDb){
            $response->errorMsg = 'Commission Transaction Not Found';
            return $response;
        }

        $transactionDb = Transaction::find($transactionDb->id);
        $transactionDb->status = 'REFUND';
        $transactionDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Release Pending Transaction
     * @param $userId
     * @param $lockerId
     * @return \stdClass
     */
    public static function releasePendingTransaction($userId,$lockerId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get pending transactions
        $pendingTransactionDb = PendingTransaction::where('user_id',$userId)->where('status','PENDING')->get();

        foreach ($pendingTransactionDb as $item) {
            $id = $item->id;
            $userId = $item->user_id;
            $type = $item->type;
            $reference = $item->reference;
            $transactionIdReference = $item->transaction_id_reference;
            $description = $item->description;
            $totalPrice = $item->total_price;
            $status = $item->status;
            $expiredDate  = $item->expired_date;

            $pendingDb = PendingTransaction::find($id);
            // check expired date
            $nowDateTime = date('Y-m-d H:i:s');
            if (strtotime($expiredDate) < strtotime($nowDateTime)) {
                $pendingDb->status = 'EXPIRED';
                $pendingDb->save();
                continue;
            }

            // create transaction based on type
            if ($type == 'commission'){
                $commissionTransaction = new Transaction();
                $commissionTransaction->users_id = $userId;
                $commissionTransaction->transaction_id_reference = $transactionIdReference;
                $commissionTransaction->type = 'commission';
                $commissionTransaction->reference = $reference;
                $commissionTransaction->description = $description;
                $commissionTransaction->total_price = $totalPrice;
                $commissionTransaction->status = 'PAID';
                $commissionTransaction->save();

                // top up deposit
                $depositDb = BalanceRecord::creditDeposit($lockerId,$totalPrice,$commissionTransaction->id);
                if (!$depositDb->isSuccess){
                    $response->errorMsg = $depositDb->errorMsg;
                    return $response;
                }
            }
            elseif ($type == 'referral') {
                // save to DB
                $transactionDb = new self();
                $transactionDb->users_id =$userId;
                // create transaction reference
                $transactionDb->reference = $reference;
                $transactionDb->type = 'referral';
                $transactionDb->cart_id = null;
                $transactionDb->description = $description;

                // create total price
                $transactionDb->total_price = $totalPrice;
                $transactionDb->status = 'PAID';
                $transactionDb->save();

                // get transaction Id for transaction item db
                // insert initial amount topup
                $tmp = number_format($totalPrice);
                $transactionId = $transactionDb->id;
                $items = [];
                $item = new \stdClass();
                $item->type = 'referral';
                $item->price = $totalPrice;
                $item->name = "Top Up Referral $tmp";
                $item->picture = null;
                $item->params = null;
                $items[] = $item;

                $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userId);
                if (!$itemDb->isSuccess){
                    $response->errorMsg = $itemDb->errorMsg;
                    return $response;
                }

                // insert to history
                $userDb = User::find($userId);
                $user = $userDb->name;
                $remarks = "Create TopUp Transaction $totalPrice $description";
                $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'PAID',$remarks);
                if (!$historyDb->isSuccess){
                    $response->errorMsg = "Failed Create Transaction History";
                    return $response;
                }

                // find another referral transaction
                $referralPending = PendingTransaction::where('type','referral')
                    ->where('id',$transactionIdReference)
                    ->first();
                if ($referralPending){
                    $referralPending = PendingTransaction::find($referralPending->id);
                    // save to DB
                    $transactionDb = new self();
                    $transactionDb->users_id =$referralPending->user_id;
                    // create transaction reference
                    $transactionDb->reference = $referralPending->reference;
                    $transactionDb->type = 'referral';
                    $transactionDb->cart_id = null;
                    $transactionDb->description = $referralPending->description;

                    // create total price
                    $transactionDb->total_price = $referralPending->total_price;
                    $transactionDb->status = 'PAID';
                    $transactionDb->save();

                    // get transaction Id for transaction item db
                    // insert initial amount topup
                    $tmp = number_format($referralPending->total_price);
                    $transactionId = $transactionDb->id;
                    $items = [];
                    $item = new \stdClass();
                    $item->type = 'referral';
                    $item->price = $referralPending->total_price;
                    $item->name = "Top Up Referral $tmp";
                    $item->picture = null;
                    $item->params = null;
                    $items[] = $item;

                    $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$referralPending->user_id);
                    if (!$itemDb->isSuccess){
                        $response->errorMsg = $itemDb->errorMsg;
                        return $response;
                    }

                    // insert to history
                    $userDb = User::find($referralPending->user_id);
                    $user = $userDb->name;
                    $remarks = "Create TopUp Transaction $totalPrice $description";
                    $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'PAID',$remarks);
                    if (!$historyDb->isSuccess){
                        $response->errorMsg = "Failed Create Transaction History";
                        return $response;
                    }
                    $referralPending->status = 'USED';
                    $referralPending->save();
                }
            }
            else {
                $response->errorMsg = 'Invalid Type';
                return $response;
            }

            $pendingDb->status = 'USED';
            $pendingDb->save();
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Get Transaction Report Sales Group
     * @param $startDate
     * @param $endDate
     * @param null $category
     * @param null $type
     * @param null $productId
     * @param null $provinceId
     * @param null $cityId
     * @return \stdClass
     */
    public static function getTransactionSalesGroup($startDate,$endDate,$category=null,$type=null,$productId=null,$provinceId = null,$cityId = null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->dayData = [];
        $response->weekData = [];
        $response->monthData = [];


        $dayData = DB::connection('popbox_agent')
            ->table('transaction_location_reports as tlp')
            ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price', tlp.category, DATE(transaction_datetime) as date"))
            ->when($category,function ($query) use ($category){
                $query->where('category',$category);
                $query->addSelect(DB::raw('tlp.type as type'));
                $query->groupBy('tlp.type');
            })->when($type,function ($query) use ($type){
                $query->where('tlp.type',$type);
                $query->addSelect('tlp.name','tlp.product_id');
                $query->groupBy('product_id');
            })->when($productId,function ($query) use ($productId,$provinceId,$cityId){
                $query->where('product_id',$productId);
                $query->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
                    ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
                    ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id');
                $query->groupBy('p.id');
                $query->addSelect(DB::raw('p.id as province_id, p.province_name'));
                $query->when($provinceId,function ($query) use ($provinceId){
                    $query->where('c.provinces_id',$provinceId);
                    $query->groupBy('c.id');
                    $query->addSelect(DB::raw('c.id as cities_id, c.city_name'));
                });
                $query->when($cityId,function ($query) use ($cityId){
                    $query->where('c.id',$cityId);
                    $query->groupBy('l.district_name');
                    $query->addSelect(DB::raw('c.id as cities_id, c.city_name, l.district_name, l.sub_district_name'));
                });
            })
            ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
            ->groupBy('date')
            ->groupBy('tlp.category')
            ->orderBy('date','desc')
            ->get();
        $response->dayData = $dayData;

        $weekData = DB::connection('popbox_agent')
            ->table('transaction_location_reports as tlp')
            ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price', tlp.category, CONCAT(YEAR(transaction_datetime), '-', WEEK(transaction_datetime,3)) as date"))
            ->when($category,function ($query) use ($category){
                $query->where('category',$category);
                $query->addSelect(DB::raw('tlp.type as type'));
                $query->groupBy('tlp.type');
            })->when($type,function ($query) use ($type){
                $query->where('tlp.type',$type);
                $query->addSelect('tlp.name','tlp.product_id');
                $query->groupBy('product_id');
            })->when($productId,function ($query) use ($productId,$provinceId,$cityId){
                $query->where('product_id',$productId);
                $query->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
                    ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
                    ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id');
                $query->groupBy('p.id');
                $query->addSelect(DB::raw('p.id as province_id, p.province_name'));
                $query->when($provinceId,function ($query) use ($provinceId){
                    $query->where('c.provinces_id',$provinceId);
                    $query->groupBy('c.id');
                    $query->addSelect(DB::raw('c.id as cities_id, c.city_name'));
                });
                $query->when($cityId,function ($query) use ($cityId){
                    $query->where('c.id',$cityId);
                    $query->groupBy('l.district_name');
                    $query->addSelect(DB::raw('c.id as cities_id, c.city_name, l.district_name, l.sub_district_name'));
                });
            })
            ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
            ->groupBy('date')
            ->groupBy('tlp.category')
            ->orderBy('date','asc')
            ->get();
        $response->weekData = $weekData;

        $monthData = DB::connection('popbox_agent')
            ->table('transaction_location_reports as tlp')
            ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price', tlp.category, CONCAT(YEAR(transaction_datetime), '-', MONTH(transaction_datetime)) as date"))
            ->when($category,function ($query) use ($category){
                $query->where('category',$category);
                $query->addSelect(DB::raw('tlp.type as type'));
                $query->groupBy('tlp.type');
            })->when($type,function ($query) use ($type){
                $query->where('tlp.type',$type);
                $query->addSelect('tlp.name','tlp.product_id');
                $query->groupBy('product_id');
            })->when($productId,function ($query) use ($productId,$provinceId,$cityId){
                $query->where('product_id',$productId);
                $query->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
                    ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
                    ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id');
                $query->groupBy('p.id');
                $query->addSelect(DB::raw('p.id as province_id, p.province_name'));
                $query->when($provinceId,function ($query) use ($provinceId){
                    $query->where('c.provinces_id',$provinceId);
                    $query->groupBy('c.id');
                    $query->addSelect(DB::raw('c.id as cities_id, c.city_name'));
                });
                $query->when($cityId,function ($query) use ($cityId){
                    $query->where('c.id',$cityId);
                    $query->groupBy('l.district_name');
                    $query->addSelect(DB::raw('c.id as cities_id, c.city_name, l.district_name, l.sub_district_name'));
                });
            })
            ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
            ->groupBy('date')
            ->groupBy('tlp.category')
            ->orderBy('date','asc')
            ->get();
        $response->monthData = $monthData;

        $response->isSuccess = true;
        return $response;
    }

    public static function getTransactionGeoGroup($startDate,$endDate,$provinceId=null,$cityId=null,$districtName=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->dayData = [];
        $response->weekData = [];
        $response->monthData = [];

        $dayData = DB::connection('popbox_agent')
            ->table('transaction_location_reports as tlp')
            ->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
            ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
            ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id')
            ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price',p.id as province_id, p.province_name, DATE(transaction_datetime) as date"))
            ->when($provinceId,function ($query) use ($provinceId){
                $query->where('p.id','=',$provinceId);
                $query->groupBy('c.id');
                $query->addSelect(DB::raw("ANY_VALUE(c.city_name) as 'city_name',c.id as city_id ,c.city_name"));
            })->when($cityId,function ($query) use ($cityId){
                $query->where('c.id','=',$cityId);
                $query->groupBy('l.district_name');
                $query->addSelect(DB::raw('l.district_name, COUNT(DISTINCT(l.id)) as number_agent'));
            })->when($districtName,function ($query) use ($districtName){
                $query->where('l.district_name',$districtName);
                $query->groupBy('l.locker_id');
                $query->addSelect(DB::raw('l.sub_district_name, l.locker_name, l.locker_id'));
            })
            ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
            ->groupBy('date')
            ->groupBy('p.id')
            ->orderBy('date','asc')
            ->get();
        $response->dayData = $dayData;

        $weekData = DB::connection('popbox_agent')
            ->table('transaction_location_reports as tlp')
            ->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
            ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
            ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id')
            ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price',p.id as province_id, p.province_name, CONCAT(YEAR(transaction_datetime), '-', WEEK(transaction_datetime,3)) as date"))
            ->when($provinceId,function ($query) use ($provinceId){
                $query->where('p.id','=',$provinceId);
                $query->groupBy('c.id');
                $query->addSelect(DB::raw("ANY_VALUE(c.city_name) as 'city_name',c.id as city_id ,c.city_name"));
            })->when($cityId,function ($query) use ($cityId){
                $query->where('c.id','=',$cityId);
                $query->groupBy('l.district_name');
                $query->addSelect(DB::raw('l.district_name, COUNT(DISTINCT(l.id)) as number_agent'));
            })->when($districtName,function ($query) use ($districtName){
                $query->where('l.district_name',$districtName);
                $query->groupBy('l.locker_id');
                $query->addSelect(DB::raw('l.sub_district_name, l.locker_name, l.locker_id'));
            })
            ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
            ->groupBy('date')
            ->groupBy('p.id')
            ->orderBy('date','asc')
            ->get();
        $response->weekData = $weekData;

        $monthData = DB::connection('popbox_agent')
            ->table('transaction_location_reports as tlp')
            ->join('popbox_virtual.lockers as l','l.locker_id','=','tlp.locker_id')
            ->join('popbox_virtual.cities as c','c.id','=','l.cities_id')
            ->join('popbox_virtual.provinces as p','p.id','=','c.provinces_id')
            ->select(DB::raw("SUM(tlp.quantity) as 'sum', SUM(tlp.price) as 'price',p.id as province_id, p.province_name, CONCAT(YEAR(transaction_datetime), '-', MONTH(transaction_datetime)) as date"))
            ->when($provinceId,function ($query) use ($provinceId){
                $query->where('p.id','=',$provinceId);
                $query->groupBy('c.id');
                $query->addSelect(DB::raw("ANY_VALUE(c.city_name) as 'city_name',c.id as city_id ,c.city_name"));
            })->when($cityId,function ($query) use ($cityId){
                $query->where('c.id','=',$cityId);
                $query->groupBy('l.district_name');
                $query->addSelect(DB::raw('l.district_name, COUNT(DISTINCT(l.id)) as number_agent'));
            })->when($districtName,function ($query) use ($districtName){
                $query->where('l.district_name',$districtName);
                $query->groupBy('l.locker_id');
                $query->addSelect(DB::raw('l.sub_district_name, l.locker_name, l.locker_id'));
            })
            ->whereBetween('tlp.transaction_datetime',[$startDate,$endDate])
            ->groupBy('date')
            ->groupBy('p.id')
            ->orderBy('date','asc')
            ->get();
        $response->monthData = $monthData;

        $response->isSuccess = true;
        return $response;
    }

    /*===============================================Relationship===============================================*/

    public function items(){
        return $this->hasMany(TransactionItem::class,'transaction_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'users_id','id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'transactions_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(TransactionHistory::class, 'transactions_id', 'id');
    }
}
