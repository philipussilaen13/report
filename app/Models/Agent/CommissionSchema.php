<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class CommissionSchema extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'commission_schemas';

    /**
     * calculate schema by schema Id
     * @param $commissionId
     * @param $basicPrice
     * @param $publishPrice
     * @param int $profit
     * @return \stdClass
     */
    public static function calculateById($commissionId, $basicPrice, $publishPrice, $profit = 0)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->commission = 0;

        // find commission schema
        $schemaDb = self::find($commissionId);
        if (!$schemaDb) {
            $response->errorMsg = 'Invalid Commission Schema Id';
            return $response;
        }
        $schemaType = $schemaDb->type;
        $schemaValue = $schemaDb->values;
        $price = $publishPrice;
        if (!empty($rule)) {
            if ($rule == 'publish') $price = $publishPrice;
            if ($rule == 'basic') $price = $basicPrice;
            if ($rule == 'profit') $price = $profit;
        }

        $amount = self::calculate($schemaType, $schemaValue, $price);
        $response->commission = $amount;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Calculate Schema by ID Schema and Price Array
     * @param $commissionId
     * @param array $publishPrice
     * @return \stdClass
     */
    public static function calculateByIdArrPrice($commissionId, $publishPrice = [])
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->commission = 0;

        // find commission schema
        $schemaDb = self::find($commissionId);
        if (!$schemaDb) {
            $response->errorMsg = 'Invalid Commission Schema Id';
            return $response;
        }
        $schemaType = $schemaDb->type;
        $schemaValue = $schemaDb->values;
        $price = $publishPrice;

        $totalCommission = 0;
        foreach ($price as $item) {
            $amount = self::calculate($schemaType, $schemaValue, $item);
            $totalCommission += $amount;
        }

        $response->commission = $totalCommission;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Calculate based on type
     * @param $type
     * @param $values
     * @param $price
     * @return int
     */
    private static function calculate($type, $values, $price)
    {
        $amount = 0;
        // if type percent
        if ($type == 'percent') {
            $amount = $price * $values / 100;
        } elseif ($type == 'fixed') {
            $amount = $values;
        }
        return $amount;
    }

    /*Relationship*/
    public function commissionRules(){
        return $this->hasMany(CommissionRule::class);
    }
}
