<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OutlineReport extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'outline_report';
}
