<?php

namespace App\Models\Popshop;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'orders';
    public $timestamps = false;
}
