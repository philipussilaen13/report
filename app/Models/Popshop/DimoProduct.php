<?php

namespace App\Models\Popshop;

use Illuminate\Database\Eloquent\Model;

class DimoProduct extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'dimo_products';
    protected $primaryKey = 'sku';
    public $incrementing = false;

    public function orders(){
        return $this->hasManyThrough(Order::class,OrderProduct::class,'sku','invoice_id','sku','invoice_id');
    }

    public function categories(){
        return $this->belongsToMany(DimoProductCategoryName::class,'dimo_product_category','product_sku','id_category');
    }
}
