<?php

namespace App\Models\Popshop;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'order_products';
}
