<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DailyTransactionWarung extends Mailable
{
    use Queueable, SerializesModels;
    private $locker_name = '';
    private $fileNameTransactionDigital = '';
    private $fileNameTransactionNonDigital = '';
    private $transactionDate = '';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($lockerName, $transactionDate, $fileNameTransactionDigital, $fileNameTransactionNonDigital)
    {
        //
        $this->locker_name = $lockerName;
        $this->transactionDate = $transactionDate;
        $this->fileNameTransactionDigital = $fileNameTransactionDigital;
        $this->fileNameTransactionNonDigital = $fileNameTransactionNonDigital;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['date'] = $this->transactionDate;
        $data['agent_name'] = $this->locker_name;

        return $this->from('no-reply@popbox.asia')
            ->cc(['greta@popbox.asia', 'william.yaputra@popbox.asia'])
            ->bcc('it@popbox.asia')
            ->subject('Data transaksi '.$this->locker_name.' Tanggal '.$data['date'])
            ->attach('storage/exports/'.$this->fileNameTransactionDigital.'.xls')
            ->attach('storage/exports/'.$this->fileNameTransactionNonDigital.'.xls')
            ->view('email.warung.dailytransactionwarung', $data);
    }
}
