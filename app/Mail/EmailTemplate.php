<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailTemplate extends Mailable
{
    use Queueable, SerializesModels;
    public $subject_email;
    public $fileName_email;
    public $receipt_email;
    public $cc_email;
    public $bcc_email;
    public $template_email;
    public $from_email = 'no-reply@popbox.asia';
    public $data_email;
    public $attach_file_path;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $fileName, $cc, $bcc, $template, $from, $data, $attach_file_path)
    {
        $this->subject_email = $subject;
        $this->fileName_email = $fileName;
        $this->cc_email = $cc;
        $this->bcc_email = $bcc;
        $this->template_email = $template;
        $this->from_email = $from;
        $this->data_email= $data;
        $this->attach_file_path = $attach_file_path;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $form = $this->from($this->from_email)->subject($this->subject_email);
        
        if($this->template_email){
            $form = $form->view($this->template_email, $this->data_email);
        }
        
        if($this->attach_file_path){
            if(is_array($this->fileName_email)){
                foreach ($this->fileName_email as $fileName){
                    $form = $form->attach(storage_path($this->attach_file_path).$fileName.'.xls');
                }
            } else {
                $form = $form->attach(storage_path($this->attach_file_path).$this->fileName_email.'.xls');
            }
        }
        
        if($this->cc_email){
            $form = $form->cc($this->cc_email);
        }
        
        if($this->bcc_email){
            $form = $form->bcc($this->bcc_email);
        }
        
        return $form;
    }
}
