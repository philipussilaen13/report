<?php

namespace App\Console\Commands;

use App\Http\Controllers\Warung\TransactionController;
use App\Http\Helpers\Helper;
use App\Models\Agent\Transaction;
use App\Models\Warung\TransactionReportConfiguration;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailTemplate;

class TransactionWarungReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sending:report';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send report Transaction Warung';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $rows = TransactionReportConfiguration::whereNotNull('warung_id')
                    ->whereNull('deleted_at')
                    ->whereNotNull('receiver')
                    ->where('is_active', '=', '1')
                    ->where('email_sent', '=', '0')
                    ->where('send_hour', '=', date('H').':00')->get();
            foreach ($rows as $row){
                if($row->period == 'daily'){     // hour of send email
                    
                    $date = date('Y-m-d', strtotime(date('Y-m-d') . "-1 days")).','.date('Y-m-d', strtotime(date('Y-m-d') . "-1 days"));
                    self::create($row->warung_id, $row->receiver, $date, $row->period, $row->id);
                    
                } else if($row->period == 'weekly' 
                    && !empty($row->send_date)              // date of send can not be empty
                    && date('d-m-Y') == $row->send_date){   // date of send email
                    
                    $date = date('Y-m-d',strtotime(date_format(date_create($row->send_date), 'Y-m-d') . "-7 days")).','.date('Y-m-d',strtotime(date_format(date_create($row->send_date), 'Y-m-d') . "-1 days"));
                    self::create($row->warung_id, $row->receiver, $date, $row->period, $row->id);
                    
                } else if($row->period == 'monthly'
//                     || (!empty($row->send_date) && date('d-m-Y') == $row->send_date)    // date of send is not empty
                    && date('d-m-Y') == date('01-m-Y')){
                    
                    $date = date('Y-m-d',strtotime("first day of last month")).','.date('Y-m-d',strtotime("last day of last month"));
                    self::create($row->warung_id, $row->receiver, $date, $row->period, $row->id);
                }
            }
            
            if(date('H:i') == '23:00'){
                Helper::removeReportFile('email_attach_file');
//                 Helper::delete_file(storage_path('files/xls'), 3600*12*1); // delete file that older than 12 hours
                $rows = DB::connection('popbox_agent')->update('update report_configuration_jobs set email_sent = 0 where email_sent = ?', [1]);
            }
        } catch (\Exception $e){
            Log::error($e->getMessage());
            Log::error($e->getTrace());
        }
    }
    
    private function create($id_warung, $receipt, $date, $report_type, $row_id){
        
        $startDate = '';
        $endDate = '';
        $lockerName = Helper::findLocker($id_warung);
        $file_path = 'files/xls';
        
        if (!empty($date)) {
            $startDate = Helper::formatDateRange($date, ',')->startDate;
            $endDate = Helper::formatDateRange($date, ',')->endDate;
        }
        
        $data = [];
        $data['date'] = date_format(date_create($startDate), 'd-m-Y');
        $data['agent_name'] = $lockerName;
        $data['report_date'] = date_format(date_create($startDate), 'd-m-Y') == date_format(date_create($endDate), 'd-m-Y') ?
        date_format(date_create($endDate), 'd-m-Y') : // if same date
        date_format(date_create($startDate), 'd-m-Y').' s/d '.date_format(date_create($endDate), 'd-m-Y'); // if date range
        
        $fileName = [
            'Transaksi_Non_Digital' => 'Transaksi_Non_Digital_'.$lockerName.'_'.str_replace('s/d', '-', $data['report_date']),
            'Transaksi_Digital'     => 'Transaksi_Digital_'.$lockerName.'_'.str_replace('s/d', '-', $data['report_date']),
        ];
        
        $param = [
            'id_warung' => $id_warung,
            'startDate' => $startDate,
            'endDate'   => $endDate,
            'filename'  => $fileName,
            'file_path' => $file_path
        ];
        
        $mail_param = [
            'subject'       => 'Transaction report '.$lockerName,
            'fileName'      => $fileName,
            'receipt'       => $receipt,
            'cc'            => '',
            'bcc'           => '',
            'template'      => 'email.warung.transactionwarung',
            'locker_id'     => $id_warung,
            'locker_name'   => $lockerName,
            'data'          => $data,
            'file_path'     => $file_path
        ];
        
        if(!is_file(storage_path('files/xls').'/'.$param['filename']['Transaksi_Non_Digital'].'.xls')){
            Log::debug('Create file with name '.$param['filename']['Transaksi_Non_Digital'].'.xls');
            $this->generate_transaction_non_digital($param);
        }
        if(!is_file(storage_path('files/xls').'/'.$param['filename']['Transaksi_Digital'].'.xls')){
            Log::debug('Create file with name '.$param['filename']['Transaksi_Digital'].'.xls');
            $this->generate_transaction_digital($param);
        }
        
        if(is_file(storage_path('files/xls').'/'.$param['filename']['Transaksi_Non_Digital'].'.xls') 
            && is_file(storage_path('files/xls').'/'.$param['filename']['Transaksi_Digital'].'.xls')){
            
            $this->sending_mail($mail_param, $report_type, $row_id);
        }
    }
    
    private function sending_mail($mail_param, $report_type, $row_id){
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        try{
            Mail::to($mail_param['receipt'])->send(
                new EmailTemplate(
                    $mail_param['subject'],
                    $mail_param['fileName'],
                    $mail_param['cc'],
                    $mail_param['bcc'],
                    $mail_param['template'],
                    'no-reply@popbox.asia',
                    $mail_param['data'],
                    $mail_param['file_path'] . '/'
                )
            );
            
            if( count(Mail::failures()) > 0 ) {
                Log::info('Failed to send Report Daily Transaction Warung ('.$mail_param['locker_id'].' - '.$mail_param['locker_name'].') to '.$mail_param['receipt']);
            }
            else {
                $obj = TransactionReportConfiguration::find($row_id);
                $obj->email_sent = 1;
                $obj->server_timestamp = date('Y-m-d H:i:s');
                if($report_type == 'weekly'){
                    $obj->send_date = date('d-m-Y', strtotime(date('d-m-Y') . "+7 days"));
                }
                $obj->save();
            }
        } catch (\Exception $e){
            Log::error($e->getMessage());
            Log::error($e->getTrace());
        }
    }
    
    private function generate_transaction_non_digital($param) {
        
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $lockerID = empty($param['id_warung']) ? 'all' : $param['id_warung'];
        $object = new TransactionController();
        $payload = $object->getData($lockerID, substr($param['startDate'], 0, 10), substr($param['endDate'], 0, 10), 'all', 'all', 'all');
        
        Helper::rendering_excel($param['filename']['Transaksi_Non_Digital'], $payload, TransactionController::non_digital_headers(), $param['file_path']);
    }
    
    private function generate_transaction_digital($param)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $mresult = [];
        $payload = [];
        try{
            $mresult = Transaction::leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->leftJoin('users', 'transactions.users_id', '=', 'users.id')
            ->leftJoin('popbox_virtual.lockers', 'users.locker_id', '=', 'popbox_virtual.lockers.locker_id')
            ->whereNull('transactions.deleted_at')
            ->whereNotIn('transaction_items.type', ['popshop'])
            ->whereBetween('transactions.created_at', [$param['startDate'], $param['endDate']])
            ->where('popbox_virtual.lockers.locker_id', '=', $param['id_warung'])
            ->select(
                'transactions.id', 'transactions.reference', 'transactions.created_at', 'transactions.status', 'users.locker_id',
                'transaction_items.name', 'transaction_items.params',
                'popbox_virtual.lockers.locker_name', 'transaction_items.type', 'transaction_items.price', 'transactions.total_price'
                )
                ->get();
            
                foreach ($mresult as $index => $item){
                    $payload[] = [
                        "no"                => $index+1,
                        "reference"         => $item->reference,
                        "time_transaction"  => date('d-m-Y', strtotime($item->created_at)),
                        "date_transaction"  => date('H:i:s', strtotime($item->created_at)),
                        "status"            => $item->status,
                        "locker_name"       => $item->locker_name,
                        "type"              => $item->type,
                        "name"              => $item->name,
                        "qty"               => 1,
                        "price"             => $item->price,
                        "total_trx"         => $item->total_price,
                    ];
                }
        } catch (\Exception $e){
            Log::error($e->getMessage());
            Log::error($e->getTrace());
        }
        
        Helper::rendering_excel($param['filename']['Transaksi_Digital'], $payload, self::digital_headers(), $param['file_path']);
    }
    
    private function digital_headers(){
        $column_01 = ["column"  => "A", "name" => "No",                 "field" => "no",                'align' => 'right'];
        $column_02 = ["column"  => "B", "name" => "Transaction ID",     "field" => "reference",         'align' => 'left'];
        $column_03 = ["column"  => "C", "name" => "Transaction Date",   "field" => "time_transaction",  'align' => 'center'];
        $column_04 = ["column"  => "D", "name" => "Transaction Time",   "field" => "date_transaction",  'align' => 'center'];
        $column_05 = ["column"  => "E", "name" => "Status",             "field" => "status",            'align' => 'center'];
        $column_06 = ["column"  => "F", "name" => "Warung Name",        "field" => "locker_name",       'align' => 'left'];
        $column_07 = ["column"  => "G", "name" => "Product Type",       "field" => "type",              'align' => 'center'];
        $column_08 = ["column"  => "H", "name" => "Items",              "field" => "name",              'align' => 'left'];
        $column_09 = ["column"  => "I", "name" => "Qty",                "field" => "qty",               'align' => 'right'];
        $column_10 = ["column"  => "J", "name" => "Product Price",      "field" => "price",             'align' => 'right', 'column_format' => 'Rp #,##0.00_-'];
        $column_11 = ["column"  => "K", "name" => "Total Transaction",  "field" => "total_trx",         'align' => 'right', 'column_format' => 'Rp #,##0.00_-'];
        
        $header = [
            $column_01, $column_02, $column_03, $column_04, $column_05, $column_06, $column_07, $column_08, $column_09, $column_10, $column_11,
        ];
        
        return $header;
    }
}
