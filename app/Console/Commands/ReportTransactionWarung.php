<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Warung\TransactionController;
use App\Mail\DailyTransactionWarung;
use App\Models\Agent\User;
use Log;

class ReportTransactionWarung extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:transactionwarung';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report Dialy Transaction Warung Kimono';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mresult = DB::connection('popbox_agent') 
            ->table('users')
            ->leftJoin('popbox_virtual.lockers', 'users.locker_id', '=', 'popbox_virtual.lockers.locker_id')
            ->leftJoin('popbox_virtual.cities', 'popbox_virtual.lockers.cities_id', '=', 'popbox_virtual.cities.id')
            ->leftJoin('popbox_virtual.regions', 'popbox_virtual.cities.regions_id', '=', 'popbox_virtual.regions.id')
            ->where('popbox_virtual.lockers.type', '=', 'warung')
            ->where('popbox_virtual.regions.region_code', 'like', 'KIMONU%')
            ->select('users.id', 'users.email', 'users.locker_id', 'popbox_virtual.lockers.locker_name')
            ->get();

        $date = date("Y-m-d", strtotime(date("Y-m-d") . ' -1 day'));
        $currentDate = date("Y-m-d", strtotime(date("Y-m-d") . ' -1 day'));

        $transaction = new TransactionController();
        foreach($mresult as $r) {
            $lockerName = strtoupper(preg_replace('/\s+/', '_', $r->locker_name));

            $transaction->locker_id = $r->locker_id;
            $transaction->transactionDate = $date;
            $transaction->fileNameTransactionDigital = 'TRANSAKSI_DIGITAL_'.$lockerName.'_'.$currentDate;
            $transaction->fileNameTransactionNonDigital = 'TRANSAKSI_NONDIGITAL_'.$lockerName.'_'.$currentDate;
            $mTransactionDigital = $transaction->DailyTransactionDigital();    
            $mTransactionNonDigital = $transaction->DailyTransactionNonDigital();    

            Mail::to('sigit@popbox.asia')->send(new DailyTransactionWarung($r->locker_name, $date, $transaction->fileNameTransactionDigital, $transaction->fileNameTransactionNonDigital));
            // Mail::to($r->email)->send(new DailyTransactionWarung($r->locker_name, $currentDate, $transaction->fileNameTransactionDigital, $transaction->fileNameTransactionNonDigital));
            if( count(Mail::failures()) > 0 ) {
                Log::info('Report Daily Transaction Warung ('.$r->locker_id.') ' . $currentDate . ' has been failure');    
            }
            else {
                Log::info('Report Daily Transaction Warung ('.$r->locker_id.') ' . $currentDate . ' has been success');    
            }
        }
        self::RemoveFileDailyReport();
    }

    public function RemoveFileDailyReport()
    {
        $currentDate = date("Y-m-d");

        // Remove file daily transaction at 3 days ago
        $fileDate = date("Y-m-d", strtotime($currentDate . "-3 days"));
        $directory = '';
        $files = Storage::disk('exports')->allFiles($directory);

        foreach($files as $r) {
            $fileExt = pathinfo(storage_path('exports').'/'.$r, PATHINFO_EXTENSION);
            if(in_array($fileExt, ['xls','xlsx'])) {
                $fileName = pathinfo($r, PATHINFO_FILENAME);
                $expFileName = explode('_', $fileName);
                if(date($expFileName[2]) <= date($fileDate)) {
                    $res = Storage::disk('exports')->delete($r);
                    if($res) {
                        Log::info('Remove Report Daily Transaction Warung ' . $r . ' has been success');                            
                    }
                    else {
                        Log::info('Remove Report Daily Transaction Warung ' . $r . ' has been failure');                            
                    }
                }
            }
        }
    }
}
