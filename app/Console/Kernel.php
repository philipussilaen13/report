<?php

namespace App\Console;

use App\Console\Commands\RouteUpdate;
use App\Console\Commands\ReportTransactionWarung;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\TransactionWarungReport;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        RouteUpdate::class,
        ReportTransactionWarung::class,
        TransactionWarungReport::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('routes:update')->everyMinute();
        $schedule->command('sending:report')->hourly();
        $schedule->command('outline:report')->dailyAt(config('constant.outline_report.time_implemented'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
