<?php

namespace App\Jobs;

use App\Http\Helpers\APIv2;
use App\Models\Agent\BalanceRecord;
use App\Models\Agent\Transaction;
use App\Models\Agent\TransactionItem;
use App\Models\Virtual\Locker;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessBankTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $uniqueId = null;
    protected $headers = [
        'Content-Type: application/json'
    ];
    protected $is_post = 0;
    protected $transactions = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Transaction $transactions)
    {
        $this->transactions = $transactions;
        $this->uniqueId = uniqid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $transactionDb = $this->transactions;
        $status = $transactionDb->status;
        $items = $transactionDb->items;

        $userData = [];
        $userData['agentName'] = $transactionDb->user->name;
        $userData['agentEmail'] = $transactionDb->user->email;
        $userData['agentPhone'] = $transactionDb->user->phone;
        $lockerDb = Locker::where('locker_id', $transactionDb->user->locker_id)->first();
        if ($lockerDb) {
            $userData['lockerName'] = $lockerDb->locker_name;
            $userData['lockerAddress'] = $lockerDb->address;
        } else {
            $userData['lockerName'] = 'Agent PopBox';
            $userData['lockerAddress'] = 'Agent PopBox';
        }

        // if top up transaction
        if ($transactionDb->type == 'topup') {
            $this->submitTopUp($transactionDb);
        } else {
            foreach ($items as $item) {
                // process paid transaction
                $pushStatus = 'FAILED';
                $reference = null;
                if ($status == 'PAID') {
                    if ($item->type == 'pulsa') {
                        $response = $this->submitSepulsa($item, $userData, $transactionDb);
                        if ($response->isSuccess) {
                            $pushStatus = 'COMPLETED';
                            $reference = $response->reference;
                        }
                    } elseif ($item->type == 'popshop') {
                        $response = $this->submitPopShop($item, $userData, $transactionDb);
                        if ($response->isSuccess) {
                            $pushStatus = 'COMPLETED';
                            $reference = $response->reference;
                        }
                    }
                }
                // update transaction items DB
                $transactionItemDb = TransactionItem::find($item->id);
                $transactionItemDb->status_push = $pushStatus;
                $transactionItemDb->reference = $reference;
                $transactionItemDb->save();
            }
        }

    }

    /**
     * Submit for Paid Transaction PopShop
     * @param $item
     * @param $userData
     * @param $transactionDb
     * @return \stdClass
     */
    private function submitPopShop($item, $userData, $transactionDb)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $totalAmount = $item->price;
            $productInfo = $paramsDb->sku . '|' . $item->name . '|' . $paramsDb->amount . '|' . $totalAmount;
            $customerInfo = $userData['agentName'] . '|' . $userData['agentEmail'] . '|' . $userData['agentPhone'];
            $purchaseInfo = $userData['lockerName'] . '|||deposit agent|' . $transactionDb->updated_at;
            $deliveryAddress = $userData['lockerName'] . "-" . $userData['lockerAddress'];

            $apiV2 = new APIv2();
            $result = $apiV2->submitPopShop($productInfo, $customerInfo, $purchaseInfo, $deliveryAddress);

            if (empty($result)) {
                $message = 'Failed to get Submit Order Product';
                $response->errorMsg = $message;
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                return $response;
            }
            $reference = $result->data->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit for Paid Transaction Sepulsa
     * @param $item
     * @param $userData
     * @param $transactionDb
     * @return \stdClass
     */
    private function submitSepulsa($item, $userData, $transactionDb)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productType = null;
            if ($item->type == 'pulsa') {
                $productType = 'mobile';
            }
            $productId = $paramsDb->product_id;
            $productAmount = $item->price;

            $apiV2 = new APIv2();
            $result = $apiV2->submitSepulsa($phone, $productType, $productId, $productAmount);

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit Top Up
     * @param $transactionDb
     * @return \stdClass
     */
    private function submitTopUp($transactionDb)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;

        $transactionId = $transactionDb->id;
        $totalTopUp = $transactionDb->total_price;
        $lockerId = $transactionDb->user->locker_id;

        $agentBalanceRecordDb = BalanceRecord::creditDeposit($lockerId, $totalTopUp, $transactionId);
        if ($agentBalanceRecordDb->isSuccess) $response->isSuccess = true;
        return $response;
    }

    /**
     * logging
     * @param $msg
     */
    private function log($msg)
    {
        $uniqueId = $this->uniqueId;
        $msg = "SMS $uniqueId $msg\n";
        $f = fopen(storage_path() . '/logs/apps/' . date('Y.m.d.') . 'log', 'a');
        fwrite($f, date('H:i:s') . " $msg");
        fclose($f);
        return;
    }
}
