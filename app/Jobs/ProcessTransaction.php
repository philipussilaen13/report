<?php

namespace App\Jobs;

use App\Http\Helpers\APIv2;
use App\Models\Agent\Transaction;
use App\Models\Agent\TransactionItem;
use App\Models\Virtual\Locker;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class ProcessTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $uniqueId = null;
    protected $headers = [
        'Content-Type: application/json'
    ];
    protected $is_post = 0;
    protected $transactions = null;

    /**
     * ProcessTransaction constructor.
     * @param AgentTransaction $transactions
     */
    public function __construct(Transaction $transactions)
    {
        $this->transactions = $transactions;
        $this->uniqueId = uniqid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $transactionDb = $this->transactions;
        $status = $transactionDb->status;
        $items = $transactionDb->items;

        $userData = [];
        $userData['agentName'] = $transactionDb->user->name;
        $userData['agentEmail'] = $transactionDb->user->email;
        $userData['agentPhone'] = $transactionDb->user->phone;
        $lockerDb = Locker::where('locker_id', $transactionDb->user->locker_id)->first();
        if ($lockerDb) {
            $userData['lockerName'] = $lockerDb->locker_name;
            $userData['lockerAddress'] = $lockerDb->address;
        } else {
            $userData['lockerName'] = 'Agent PopBox';
            $userData['lockerAddress'] = 'Agent PopBox';
        }

        foreach ($items as $item) {
            if ($item->type == 'admin_fee') continue;
            // process paid transaction
            $pushStatus = 'FAILED';
            $reference = null;
            $errorMsg = null;
            $url = null;
            $params = null;
            if ($status == 'PAID') {
                if ($item->type == 'pulsa') {
                    $response = $this->submitSepulsa($item, $userData, $transactionDb);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'popshop') {
                    $response = $this->submitPopShop($item, $userData, $transactionDb);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'electricity') {
                    $response = $this->submitPLNPrePaid($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'electricity_postpaid') {
                    $response = $this->submitPLNPostPaid($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'bpjs_kesehatan') {
                    $response = $this->submitBPJS($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'pdam') {
                    $response = $this->submitPDAM($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                } elseif ($item->type == 'telkom_postpaid') {
                    $response = $this->submitTelkom($item);
                    if ($response->isSuccess) {
                        $pushStatus = 'COMPLETED';
                        $reference = $response->reference;
                    }
                    $errorMsg = $response->errorMsg;
                    $url = $response->url;
                    $params = $response->params;
                }
            }
            // update transaction items DB
            $transactionItemDb = TransactionItem::find($item->id);
            $transactionItemDb->status_push = $pushStatus;
            $transactionItemDb->reference = $reference;
            $transactionItemDb->save();

            if ($pushStatus == 'FAILED') {
                $environment = App::environment();
                // send email if failed
                Mail::send('email.failed-push', ['transaction' => $transactionDb, 'userData' => $userData, 'item' => $item, 'locker' => $lockerDb, 'error' => $errorMsg, 'url' => $url, 'params' => $params], function ($m) use ($transactionDb, $environment) {
                    $m->from('no-reply@popbox.asia', 'PopBox Asia');
                    $m->to('it@popbox.asia', 'IT PopBox Asia')->subject("[Agent][$environment] Failed Push Transaction : $transactionDb->reference");
                });
            }
        }
    }

    /**
     * Submit for Paid Transaction PopShop
     * @param $item
     * @param $userData
     * @param $transactionDb
     * @return \stdClass
     */
    private function submitPopShop($item, $userData, $transactionDb)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $totalAmount = $item->price;
            $productInfo = $paramsDb->sku . '|' . $item->name . '|' . $paramsDb->amount . '|' . $totalAmount;
            $customerInfo = $userData['agentName'] . '|' . $userData['agentEmail'] . '|' . $userData['agentPhone'];
            $purchaseInfo = $userData['lockerName'] . '|||deposit agent|' . $transactionDb->updated_at;
            $deliveryAddress = $userData['lockerName'] . "-" . $userData['lockerAddress'];

            $apiV2 = new APIv2();
            $result = $apiV2->submitPopShop($productInfo, $customerInfo, $purchaseInfo, $deliveryAddress);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'ordershop/submit';
            $param = [];
            $param['token'] = $token;
            $param['product_info'] = $productInfo;
            $param['customer_info'] = $customerInfo;
            $param['purchase_info'] = $purchaseInfo;
            $param['delivery_address'] = $deliveryAddress;

            if (empty($result)) {
                $message = 'Failed to get Submit Order Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit for Paid Transaction Sepulsa
     * @param $item
     * @param $userData
     * @param $transactionDb
     * @return \stdClass
     */
    private function submitSepulsa($item, $userData, $transactionDb)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productType = null;
            if ($item->type == 'pulsa') {
                $productType = 'mobile';
            }
            $productId = $paramsDb->product_id;
            $productAmount = $item->price;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitSepulsa($phone, $productType, $productId, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = $productType;
            $param['product_id'] = $productId;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit PLN PrePaid
     * @param $item
     * @return \stdClass
     */
    private function submitPLNPrePaid($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productId = $paramsDb->product_id;
            $productAmount = $item->price;
            $meterNumber = $paramsDb->meter_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitPLNPrePaid($phone, $productId, $productAmount, $meterNumber, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'electricity';
            $param['product_id'] = $productId;
            $param['product_amount'] = $productAmount;
            $param['meter_number'] = $meterNumber;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit PLN Post Paid
     * @param $item
     * @return \stdClass
     */
    private function submitPLNPostPaid($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $meterNumber = $paramsDb->meter_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitPLNPostPaid($phone, $productAmount, $meterNumber, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'electricity_postpaid';
            $param['product_amount'] = $productAmount;
            $param['meter_number'] = $meterNumber;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit BPJS
     * @param $item
     * @return \stdClass
     */
    private function submitBPJS($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $bpjsNumber = $paramsDb->bpjs_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitBPJS($phone, $bpjsNumber, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'bpjs_kesehatan';
            $param['bpjs_number'] = $bpjsNumber;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit PDAM
     * @param $item
     * @return \stdClass
     */
    private function submitPDAM($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $pdamNumber = $paramsDb->pdam_number;
            $operatorCode = $paramsDb->operator_code;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitPDAM($phone, $pdamNumber, $operatorCode, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'pdam';
            $param['pdam_number'] = $pdamNumber;
            $param['operator_code'] = $operatorCode;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * Submit Telkom
     * @param $item
     * @return \stdClass
     */
    private function submitTelkom($item)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->reference = null;
        $response->params = null;
        $response->url = null;

        $paramsDb = $item->params;
        if (!empty($paramsDb)) {
            $paramsDb = json_decode($paramsDb);
            $phone = $paramsDb->phone;
            $productAmount = $item->price;
            $telkomNumber = $paramsDb->telkom_number;
            $agentName = null;
            if (!empty($paramsDb->agent_name)) {
                $agentName = $paramsDb->agent_name;
            }
            $customerEmail = null;
            if (!empty($paramsDb->customer_email)) {
                $customerEmail = $paramsDb->customer_email;
            }

            $apiV2 = new APIv2();
            $result = $apiV2->submitTelkom($phone, $telkomNumber, $productAmount, $agentName, $customerEmail);

            $host = env('API_URL');
            $token = env('API_TOKEN');

            $url = $host . '/' . 'service/sepulsa/postTransaction';
            $param = [];
            $param['token'] = $token;
            $param['phone'] = $phone;
            $param['product_type'] = 'telkom_postpaid';
            $param['telkom_number'] = $telkomNumber;
            $param['product_amount'] = $productAmount;
            $param['agent_name'] = $agentName;
            $param['customer_email'] = $customerEmail;

            if (empty($result)) {
                $message = 'Failed to get Post Transaction Product';
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            if ($result->response->code != 200) {
                $message = $result->response->message;
                $response->errorMsg = $message;
                $response->url = $url;
                $response->params = json_encode($param);
                return $response;
            }
            $reference = $result->data[0]->invoice_id;
            $response->reference = $reference;
            $response->isSuccess = true;
        }
        return $response;
    }

    /**
     * logging
     * @param $msg
     */
    private function log($msg)
    {
        $uniqueId = $this->uniqueId;
        $msg = "SMS $uniqueId $msg\n";
        $f = fopen(storage_path() . '/logs/app/' . date('Y.m.d.') . 'log', 'a');
        fwrite($f, date('H:i:s') . " $msg");
        fclose($f);
        return;
    }
}
